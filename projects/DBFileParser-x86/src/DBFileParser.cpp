//============================================================================
// Name        : DBFileParser.cpp
// Author      : doron
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <unordered_map>
#include <string>
#include <algorithm>
#include <vector>
#include <cctype>
#include <iostream>
#include <fstream>
#include <algorithm>
//#define SUB_KEY_ENABLE

using namespace std;

unordered_map<string,string> DB;

struct DBfile
{
	string data;
};

struct DBfile DBFile[100];

class DB_Value
{
	int fieldsNum;
	string Value;
	unordered_map<string, string> Table;
};

DB_Value cDB_Value;
class DBFileUtil
{
public:
	int parsingDBFile(string LineForPars);
	int paresingNestaetData(string key_forsubkey, string LineForPars);
	int loadDBfile(char *filename);
	string getValue( string key);
	int InsertValue( string key ,string val);
	int SaveDBToFile(string key ,string val);
	string InterfaceCmd(string key ,string val, int nested ,int GetSet);//GetSet = 0 get 1 = set
	int InitSystem(void);

private:
};
DBFileUtil dbutil;

int DBFileUtil::SaveDBToFile(string key_val ,string val)
{
	string sLine, stemp;
	int found;
	int start=0,stop = 0;
	ifstream myFileIn("nms.db");
	ofstream myDileOut("temp_nms.db" );

	if(!myFileIn || !myDileOut)
	{
		cout << "Error opening files!" << endl;
		return 1;
	}
	cout << "Debug :"<< "  key :" << key_val << "  val :" <<val  << endl;

	while( getline(myFileIn,sLine))
	{
		//cout << " Debug :"<< sLine << endl;
		if( sLine.empty() )
		{
			myDileOut << sLine; // Ignore empty lines
		}
		else
		{
			if(sLine.find_first_of('[') == 0)
			{
				if(sLine.find("[END]") == 0)
				{
					cout << " Debug :"<< sLine << endl;// Handle failure.
					myDileOut << sLine;
					myDileOut.close();
					myFileIn.close();
					if (0 != std::rename("temp_nms.db", "nms.db"))
					{
						cout << " Debug :"<< sLine << endl;// Handle failure.
					}
					return 1;
				}
			   start = sLine.find_first_of('[') + 1;
			   //cout << " Debug :"<< sLine << endl;
			   found = sLine.find_first_of(key_val.c_str());
			   if (found == 1 && stop ==0)
			   {

				   sLine = "["+key_val+"]" + "{"+val+"}"+"\n";
				   cout << "Debug :"<< "  key :" << key_val << "  get line :" <<stemp  << endl;
				   stop = 1;
			   }
			}
			myDileOut << sLine;
		}


	}
	myDileOut.close();
	myFileIn.close();
//	if (0 != std::rename("temp_nms.db", "nms.db"))
//	{
//		cout << " Debug :"<< sLine << endl;// Handle failure.
//	}
return 1;
}

int DBFileUtil::InsertValue( string key ,string val)
{
	pair<string,string> tmp (key,val);
	DB.insert (tmp);

}

string DBFileUtil::getValue( string key)
{
	unordered_map<string,string>::const_iterator got = DB.find (key);

	if ( got == DB.end() )
		return   "not found";
	else
		return  got->second;
}

string DBFileUtil::InterfaceCmd(string key ,string val, int nested ,int GetSet)
{
	if(GetSet)
	{
		dbutil.InsertValue( key, val );
	}
	else
	{
		return (dbutil.getValue(key));
	}
}


int DBFileUtil::loadDBfile(char *filename)
{
	string sLine;
	ifstream myInputFile(filename, ios::in);
	if( !myInputFile )
	{
	   //sError = "File SYS_CONFIG_FILE could not be opened";
	   return 0;//sError; // ERROR
	}
	while( getline(myInputFile,sLine) )
	{
	   if( sLine.empty() ); // Ignore empty lines
	   else
	   {
		   dbutil.parsingDBFile(sLine);
	   }
	}
	return 1;
}
int DBFileUtil::paresingNestaetData(string Key, string LineForPars)
{
	int start=0,end=0 , lest_end = 0, stop = 0;
	int number_off_sub_key = 1;
	string sLine, sValue, sKey, sTemp;
	char key[30];
	char value[200];
	size_t found ;
	//cout << "Debug :"<< LineForPars << endl;
	   /***************************************/
	   //find sub key and value
	   found = LineForPars.find("[",end+1,1);
	   stop = LineForPars.find_last_of("}");
	   if (found!=std::string::npos)//find nested key and value
	   {
		   //example data nestet
		   //[POE_P00]{EN[1] PRIO[3] MAX_PWR_MWATT[60000] PORT_TYPE[4PAIR] DESCR[----]}
		   //01234567890123456789012345678901234567890123456789001234567890
		   //insart data to table
		   while(1)
		   {
			   //clean value for next step
			   memset(key,0,30);
			   memset(value,0,200);
			   if (number_off_sub_key == 1)
			   {
				   number_off_sub_key++;
				   start = LineForPars.find_first_of('{') + 1;//find the next firset key
				   LineForPars.copy(key,found - start  ,start);

				   end = LineForPars.find("]",found,1) -1;
				   lest_end = end + 1;
				   if (end != std::string::npos)
				   {
					  LineForPars.copy(value,end  - found  ,found + 1);
				   }
			   }
			   else//next key
			   {
				   number_off_sub_key++;
				   start = lest_end ;
				   end = LineForPars.find("[",start + 1,1);
				   if (end != std::string::npos)
				   {
					   LineForPars.copy(key,(end - start)-2  ,start + 2);
				   }
				   lest_end = end;
				   end = LineForPars.find("]",lest_end ,1);
				   if (end != std::string::npos)
				   {
					  LineForPars.copy(value,(end  - lest_end) -1 ,lest_end + 1);
				   }
				   lest_end = end;


			   }
			   //cout << "Mster key :"<< Key<< "  Sub key :"<< key << " value:"<< value<< endl;
			   if (stop == end + 1)
			   {
				   number_off_sub_key = 1;
				   break;
			   }

		   }

	   }
	   return 1;
}

int DBFileUtil::parsingDBFile(string LineForPars)
{
	int number_of_line = 0,start=0,end=0 ;
	string sLine, sValue, sKey, sTemp,key_forsubkey;
	char key[30];
	char value[200];
	//cout << "Debug :"<< LineForPars << endl;
	   if(LineForPars.find_first_of('[') == 0)
	   {

		   if(LineForPars.find("[END]") == 0)
		   {
			 return number_of_line;
		   }
		   /***************************************/
		   //get key
		   memset(key,0,30);
		   memset(value,0,200);
		   start = LineForPars.find_first_of('[') + 1;
		   end = LineForPars.find_first_of(']') ;
		   LineForPars.copy(key,end - start,start );
#ifdef SUB_KEY_ENABLE
		   dbutil.InsertValue( (string)key, (string)value );
		   key_forsubkey = (string)key;
		   cout << "firest key :"<< key <<  endl;
		   found = LineForPars.find("[",end+1,1);
		   if (found!=std::string::npos)//find nested key and value
		   {
			   dbutil.paresingNestaetData( key_forsubkey, LineForPars);
		   }
		   else
		   {
			   //get value
			   memset(value,0,200);
			   start = LineForPars.find_first_of('{') + 1;
			   end = LineForPars.find_first_of('}') ;
			   LineForPars.copy(value,end - start  ,start);
			   /***************************************/
			   cout << "Set key :"<< key << " value:"<< value<< endl;
			   //insart data to table
			   dbutil.InsertValue( (string)key, (string)value );
			   /***************************************/
			   //for test get the value
			   (string)value = dbutil.getValue((string)key);
			   cout << "Get key :"<< key << " value:"<< value<< endl;
		   }
		   /***************************************/

#else //SUB_KEY_ENABLE

		   {
			   //get value
			   memset(value,0,200);
			   start = LineForPars.find_first_of('{') + 1;
			   end = LineForPars.find_first_of('}') ;
			   LineForPars.copy(value,end - start  ,start);
			   /***************************************/
			   //cout << "Set key :"<< key << " value:"<< value<< endl;
			   //insart data to table
			   dbutil.InsertValue( (string)key, (string)value );
			   /***************************************/
			   //for test get the value
			   (string)value = dbutil.getValue((string)key);
		   //cout << "Get key :"<< key << " value:"<< value<< endl;
		   }
#endif //SUB_KEY_ENABLE
	   }
	   return 1;
}


int DBFileUtil::InitSystem(void)
{
	string sval;
	string stemp;
	int ret=0;
	//setup system from the DB in memory
	//set IPV4_DHCP
	sval =dbutil.getValue("IPV4_DHCP_EN");
//	ret = sval.find_first_of("1");
//	ret = sval.find_first_of("0");
	cout << "IPV4_DHCP_EN value:"<< sval<< endl;

	if (sval.find_first_of("1")==0)
	{
		system("/var/start_dhcp.sh");
	}
    //set ip addres
	sval =dbutil.getValue("IPV4_ADDR");
	stemp = "/var/set_ip_addr.sh " + sval;
	cout << "IPV4_ADDR value:"<< stemp<< endl;
	system(const_cast<char  *>(stemp.c_str()));
	//set ip mask
	sval =dbutil.getValue("IPV4_MASK");
	stemp = "/var/set_ip_mask.sh " + sval;
	cout << "IPV4_MASK value:"<< stemp<< endl;
	system(const_cast<char  *>(stemp.c_str()));
	//IPV4_DGW
	//IPV6_DHCP_EN
	sval =dbutil.getValue("IPV6_DHCP_EN");
	if (sval.find_first_of("1")==0)
	{
		sval =dbutil.getValue("IPV6_ADDR");
		stemp = "/var/set_ipv6.sh " + sval;
		stemp = stemp + "/" + dbutil.getValue("IPV6_PREFIX");
		cout << "IPV6_ADDR value:"<< stemp<< endl;
		system(const_cast<char  *>(stemp.c_str()));
	}
	//telnet enable
	sval =dbutil.getValue("TELNET_EN");
	if (sval.find_first_of("1")==0)
	{
		stemp = "/var/start_telnet.sh ";
		system(const_cast<char  *>(stemp.c_str()));
	}
	//ssh
	sval =dbutil.getValue("SSH_EN");
	if (sval.find_first_of("1")==0)
	{
		stemp = "/var/start_ssh.sh ";
		system(const_cast<char  *>(stemp.c_str()));
	}
	//set ntp
	sval =dbutil.getValue("IP_NTP");
	stemp = "/var/start_ntp.sh " + sval;
	system(const_cast<char  *>(stemp.c_str()));
	//set snmp
	sval =dbutil.getValue("SNMPV2_EN");//SNMPV3_EN?
	if (sval.find_first_of("1")==0)
	{
		stemp = "/var/start_snmp.sh ";
		system(const_cast<char  *>(stemp.c_str()));
	}
	//start web
	sval =dbutil.getValue("HTTPS_EN");
	if (sval.find_first_of("1")==0)
	{
		stemp = "/var/start_web.sh ";
		system(const_cast<char  *>(stemp.c_str()));
	}

/*
[IPV6_DGW]{}

[IP_DNS0]{}
[IP_DNS1]{}

[IP_HOST_NAME]{My-Device}
[IP_NTP]{64.90.182.55}
[IP_SYSLOG0]{}
[IP_SYSLOG1]{}

[USER_NAME]{admin}
[USER_PASS]{"peeuW,0}


[SNMPV2_EN]{0}
[SNMPV3_EN]{0}
[HTTPS_EN]{0}//web
 *
 * */
	return 1;
}

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}


int main(int argc, char * argv[])
{
	int var;
    string val;
	dbutil.loadDBfile("nms.db");//init from restart read the DB from the file system
	//dbutil.InitSystem();
    if(cmdOptionExists(argv, argv+argc, "-h"))
    {
    	cout << " ****** Help For DB parser  ******" << endl;
    	cout << "DBFileParser -h" << endl; //this help
    	cout << "If no argumant for read the DB and init the System" << endl;
    	cout << " ****** For Command Line ******* " << endl;
    	cout << "DBFileParser -set [key] [value]" << endl;
    	cout << "DBFileParser -get [key]" << endl;
    	cout << "DBFileParser -init" << endl;
    }
    else
    {
    	 if(cmdOptionExists(argv, argv+argc, "-set"))
    	 {

			for (var = 3; var < argc; ++var)
			{

				//cout << "arg: " << argv[var] << endl;
				val = val + argv[var];
			}
			dbutil.InsertValue(argv[2],val);
			dbutil.SaveDBToFile( argv[2] , val);
			cout << "Set key :"<< argv[2] << " value:"<< val<< endl;
    	 }
    	 else
    	 {
    		 if(cmdOptionExists(argv, argv+argc, "-get"))
    		 {
    			 val = argv[2];
    			 val = dbutil.getValue(val);
    			 cout << "Set key :"<< argv[2] << " value:"<< val<< endl;
    			 return 0;
    		 }
    		 else
    		 {
    			 if(cmdOptionExists(argv, argv+argc, "-init"))
    			 {
    				 dbutil.InitSystem();
    				 return 0;
    			 }
				 else
				 {
					//dbutil.loadDBfile("nms.db");//init from restart read the DB from the file system

				 }
    		 }
    	 }
    	 //update system
    	 dbutil.InitSystem();
    }





//		dbutil.parsingDBFile(ret);
//	hashtable.emplace("www.element14.com", "184.51.49.225");
//	cout << "IP Address: " << hashtable["www.element14.com"] << endl;
	return 0;
}
