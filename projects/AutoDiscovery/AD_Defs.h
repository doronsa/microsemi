/* 
 * File:   AD_Defs.h
 * Author: shikh
 *
 * Created on December 3, 2014, 1:29 PM
 */

#ifndef AD_DEFS_H
#define	AD_DEFS_H

#include <stdint.h>

#define MAC_ADDRESS_FILE "/sys/class/net/eth0/address"

#pragma pack(1) // align structure by 1 ( no spaces). Size should be 148 byte long
typedef struct
{
    uint32_t recordsize;   /* The stored size of the struct */
    uint32_t ip_Addr;      /* The device IP Address */
    uint32_t ip_Mask;      /* The IP Address Mask */
    uint32_t ip_GateWay;   /* The address of the P gateway */
    uint32_t ip_TftpServer;/* The address of the TFTP server to load data from for debugging */
    uint32_t baud_rate;    /* The initial system Baud rate */
    uint8_t wait_seconds; /* The number of seconds to wait before booting */
    uint8_t bBoot_To_Application; /*True if we boot to the application, not the monitor */
    uint8_t bException_Action;    /*What should we do when we have an exception? */
    uint8_t m_FileName[80];       /*The file name of the TFTP file to load */
    uint8_t mac_address[6];       /*The Ethernet MAC address */
    uint8_t ser_boot;
    uint32_t ip_DNS_server;
    uint8_t core_mac_address[6];  /*The Base unit MAC address */
    uint8_t typeof_if;
    uint8_t direct_Tx;
    uint32_t m_ExtraData[4];
    uint8_t m_bUnused[3];
    uint8_t m_q_boot;        /* True to boot without messages */
    uint16_t checksum;       /* A Checksum for this structure */
} ConfigRecord;
#pragma pack() // end

#endif	/* AD_DEFS_H */

