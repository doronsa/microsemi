/* 
 * File:   AutoDiscovery.cpp
 * Author: shikh
 * 
 * Created on December 3, 2014, 3:15 PM
 */

#include "AutoDiscovery.h"
#include "AD_Defs.h"

#include <string.h>
#include <stdio.h>

using namespace Shared;

const byte AutoDiscovery::AD_RCV_MSG_BUF[AD_RCV_MSG_LEN] = {0x42, 0x55, 0x52, 0x4E, 0x52};

AutoDiscovery::AutoDiscovery()
{
}

void AutoDiscovery::Start()
{
    uint8_t in_packet[AD_RCV_MSG_LEN];
    
    // Init the basic outgoing packet for AD requests
    ConfigRecord out_packet;
    memset(&out_packet, 0, sizeof(out_packet));
    
    // Get mac address info
    if (!(GetMacAddr(out_packet.core_mac_address)))
    {
        // TODO : WHAT?
        return;
    }
    memcpy(out_packet.mac_address, out_packet.core_mac_address, 6);
    
    // TODO : GET OUR IP/MAC INFO(?)
    
    if (!listen_sock_.Bind(AD_LISTEN_PORT))
        return;
    
    for (;;)
    {
        int received = 0;
        
        // Recvfrom should return -1 if incoming buffer is larger than our given buffer
        // TODO : CHECK THAT
        if ((received = listen_sock_.Recv(in_packet, AD_RCV_MSG_LEN)) != -1)
        {
            // Check it
            if (!IsValidADBuf(in_packet))
                continue;
            
            // Create our response and send it
        }
    }
}

AutoDiscovery::~AutoDiscovery()
{
}

bool AutoDiscovery::IsValidADBuf(byte buf[AD_RCV_MSG_LEN])
{
    return (memcmp(AD_RCV_MSG_BUF, buf, AD_RCV_MSG_LEN) == 0);
}

bool AutoDiscovery::GetMacAddr(uint8_t mac[6])
{
    FILE *mac_file;
    
    if ((mac_file = fopen(MAC_ADDRESS_FILE, "r")) == NULL)
        return false;
    
    fscanf(mac_file, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", 
            &mac[0], &mac[1], &mac[2], &mac[3], &mac[4], &mac[5]);
    
    fclose(mac_file);
}