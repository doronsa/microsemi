/* 
 * File:   AutoDiscovery.h
 * Author: shikh
 *
 * Created on December 3, 2014, 3:15 PM
 */

#ifndef AUTODISCOVERY_H
#define	AUTODISCOVERY_H

#include "AD_Defs.h"

#include "../PDShared/Socket_UDP.h"

class AutoDiscovery {
private:
    const static word AD_LISTEN_PORT = 20034;
    const static int AD_RCV_MSG_LEN = 5;
    const static int AD_REPLY_MSG_LEN = sizeof(ConfigRecord); 
    
    const static byte AD_RCV_MSG_BUF[AD_RCV_MSG_LEN];
    

public:
    AutoDiscovery();
    virtual ~AutoDiscovery();
    
    void Start();
private:
    
    // Compares the received buffer to the valid value for an AD message
    bool IsValidADBuf(byte buf[AD_RCV_MSG_LEN]);
    
    bool GetMacAddr(uint8_t mac[6]);
    
    // Our local listening socket
    Shared::Socket_UDP listen_sock_;
};

#endif	/* AUTODISCOVERY_H */

