/* 
 * File:   ConfigUtils.cpp
 * Author: shikh
 * 
 * Created on December 17, 2014, 3:13 PM
 */

#include "ConfigUtils.h"
using namespace std;

ConfigUtils::ConfigUtils()
{
}

ConfigUtils::~ConfigUtils()
{
}

bool ConfigUtils::ToBool(string value)
{
    return (value[0] == '1');
}
