/* 
 * File:   ConfigDB.h
 * Author: shikh
 *
 * Created on December 16, 2014, 11:00 AM
 */

#ifndef CONFIGDB_H
#define	CONFIGDB_H

#include "ConfigParser.h"
#include "ConfigDefs.h"

#include "../PDShared/PdsError.h"

#include <string>
#include <vector>
#include <map>

class ConfigDBError : public PdsError
{
public:
    ConfigDBError(PdsErrmodule module) :
    PdsError(module),
    line_(0)
    {};
    
    virtual void SetLine(int line) { line_ = line; };
    virtual int GetLine() const { return line_; };

    virtual std::string ToString() const;

protected:
    int line_;
};

class ConfigDB
{
public:
    
    ConfigDB() :
    loaded_(false),
    error_(ERRMOD_CONFIG)
    {}; // TODO : Change error module between this and parser?
    
    virtual ~ConfigDB() {};
    
    const PdsError& GetError() { return error_; };
    
    /**
     * Attempts to load the configuration
     * @return Error code
     */
    PdsErrcode Load(const std::string filename);
    
    /**
     * Returns a value from the DB
     * @param key The key to fetch
     * @param o_value The returned value
     * @return Error code
     */
    PdsErrcode GetValue(const std::string &key, std::string &o_value);
    
    /**
     * Returns a value from the DB that has nested subkeys
     * @param key The key to fetch
     * @param o_values A map containing the fetched subvalues by their subkeys
     * @return Error code
     */
    PdsErrcode GetNestedValue(const std::string &key, 
                               NestedMap &io_values);
    
    /**
     * Sets a value in the DB(does not update file)
     * @param key The key to update
     * @param value The new value
     * @return Error code
     */
    PdsErrcode SetValue(const std::string &key, const std::string &value);
    
    /**
     * Sets a value in the DB that has nested subkeys
     * @param key The key to update
     * @param values The new values
     * @return Error code
     */
    PdsErrcode SetNestedValue(const std::string &key, const NestedMap &values);
    
private:
    
    /**
     * Checks that a key exists and returns it
     * @param key The key to search
     * @param o_itr Iterator with it's location, if found
     * @return Error code
     */
    PdsErrcode FindInDB(const std::string& key, ConfigMap::iterator& o_itr);
    
    /**
     * Parses a stream and attempts to load it into the database
     * @param configstream A stream reference to read from
     * @return Error code
     */
    PdsErrcode ParseStream(std::istream &configstream);
    
    // The parser object for the file
    ConfigParser parser_;
    
    // Whether the configuration was successfully loaded
    bool loaded_;
    
    // Stores the values extracted from the configuration by their keys
    ConfigMap values_;
    
    ConfigDBError error_;
};

#endif	/* CONFIGDB_H */

