/* 
 * File:   ConfigParser.h
 * Author: microsemi
 *
 * Created on December 11, 2014, 6:36 PM
 */

#ifndef CONFIGPARSER_H
#define	CONFIGPARSER_H

#include "ConfigDefs.h"
#include "../PDShared/PdsError.h"
#include <string>
#include <map>

class ConfigParser
{
public:
    ConfigParser();
    virtual ~ConfigParser();

    const PdsError& GetError() { return error_; };
    
    /**
     * Parses a single line of the configuration file
     * @param line The line to be parsed
     * @param o_key The key
     * @param o_value The value
     * 
     * @return Error code
     */
    PdsErrcode ParseLine(const std::string &line, std::string &o_key, std::string &o_value);

    /**
     * Parses a nested parameter's data and returns a map linking the subkeys to the values
     * @param nestedvalue Data to parse
     * @param o_values Map, containing subkey -> value links
     * 
     * @return Error code
     */
    PdsErrcode ParseNested(const std::string &nested, NestedMap &o_values);

    /**
     * Sets a complex value's subkeys to new subvalues
     * @param io_nested The value to update
     * @param values The new values
     * @return Error code
     */
    PdsErrcode SetNested(std::string &io_nested, const NestedMap &values);
private:
    
    /**
     * Searches for a subkey and subvalue in a complex value and returns their location
     * @param nested The value to parse
     * @param key The subkey to search
     * @param o_valuestart Where the value starts
     * @param o_valuelen The value length
     * @return 
     */
    PdsErrcode FindSubValue(const std::string &nested, 
    const std::string &subkey, 
    size_t &o_valuestart, 
    size_t &o_valuelen);

    PdsError error_;
};

#endif	/* CONFIGPARSER_H */

