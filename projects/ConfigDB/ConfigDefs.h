/* 
 * File:   Config_Defs.h
 * Author: shikh
 *
 * Created on December 16, 2014, 11:02 AM
 */

#ifndef CONFIG_DEFS_H
#define	CONFIG_DEFS_H

#include <string>
#include <map>

// A map type for the configuration DB
typedef std::map<std::string, std::string> ConfigMap;
// A map type for complex values with nested subkeys
typedef std::map<std::string, std::string> NestedMap;
    
#define CONFIG_FILE_MAIN "/var/web_root/nms.db"
#define CONFIG_FILE_BACKUP "nms.db.backup"

#define  CFG_POE_P00                           "POE_P00"
#define  CFG_POE_P01                           "POE_P01"
#define  CFG_POE_P02                           "POE_P02"
#define  CFG_POE_P03                           "POE_P03"
#define  CFG_TIME_ZONE_HOUR                    "TIME_ZONE_HOUR"
#define  CFG_TIME_ZONE_MIN                     "TIME_ZONE_MIN"
#define  CFG_IPV4_DHCP_EN                      "IPV4_DHCP_EN"
#define  CFG_IPV4_ADDR                         "IPV4_ADDR"
#define  CFG_IPV4_MASK                         "IPV4_MASK"
#define  CFG_IPV4_DGW                          "IPV4_DGW"
#define  CFG_IPV6_DHCP_EN                      "IPV6_DHCP_EN"
#define  CFG_IPV6_ADDR                         "IPV6_ADDR"
#define  CFG_IPV6_PREFIX                       "IPV6_PREFIX"
#define  CFG_IPV6_DGW                          "IPV6_DGW"
#define  CFG_IP_DNS0                           "IP_DNS0"
#define  CFG_IP_DNS1                           "IP_DNS1"
#define  CFG_IP_HOST_NAME                      "IP_HOST_NAME"
#define  CFG_IP_NTP                            "IP_NTP"
#define  CFG_IP_SYSLOG0                        "IP_SYSLOG0"
#define  CFG_IP_SYSLOG1                        "IP_SYSLOG1"
#define  CFG_USER_NAME                         "USER_NAME"
#define  CFG_USER_PASS                         "USER_PASS"
#define  CFG_TELNET_EN                         "TELNET_EN"
#define  CFG_SSH_EN                            "SSH_EN"
#define  CFG_SNMPV2_EN                         "SNMPV2_EN"
#define  CFG_SNMPV3_EN                         "SNMPV3_EN"
#define  CFG_HTTPS_EN                          "HTTPS_EN"
#define  CFG_TRAP_IP0                          "TRAP_IP0"
#define  CFG_TRAP_IP1                          "TRAP_IP1"
#define  CFG_MIB2_SYS_CNTCT                    "MIB2_SYS_CNTCT"
#define  CFG_MIB2_SYS_NAME                     "MIB2_SYS_NAME"
#define  CFG_MIB2_SYS_LOC                      "MIB2_SYS_LOC"
#define  CFG_V2_TRAP_PASS                      "V2_TRAP_PASS"
#define  CFG_V2_GET_PASS                       "V2_GET_PASS"
#define  CFG_V2_SET_PASS                       "V2_SET_PASS"
#define  CFG_TRAP_POE_EN                       "TRAP_POE_EN"
#define  CFG_V3_USER                           "V3_USER"
#define  CFG_V3_AUTHEN_PASS                    "V3_AUTHEN_PASS"
#define  CFG_V3_ENCR_PASS                      "V3_ENCR_PASS"
#define  CFG_V3_ENCR_LVL                       "V3_ENCR_LVL"
#define  CFG_V3_TRAP_USER                      "V3_TRAP_USER"
#define  CFG_V3_TRAP_AUTEN_PASS                "V3_TRAP_AUTEN_PASS"
#define  CFG_V3_TRAP_ENC_PASS                  "V3_TRAP_ENC_PASS"
#define  CFG_V3_TRAP_ENCR_LVL                  "V3_TRAP_ENCR_LVL"
#define  CFG_V3_BOOT_CNTR                      "V3_BOOT_CNTR"
#define  CFG_PWR_USAGE_WRN                     "PWR_USAGE_WRN"
#define  CFG_POE_USER_BYTE                     "POE_USER_BYTE"
#define  CFG_POLL_DGW_RST_IF_FAIL              "POLL_DGW_RST_IF_FAIL"
#define  CFG_WEEK_SCHED_PORT_ACT_EN            "WEEK_SCHED_PORT_ACT_EN"
#define  CFG_WEEK_SCHED_PORT_ACT0              "WEEK_SCHED_PORT_ACT0"
#define  CFG_WEEK_SCHED_PORT_ACT1              "WEEK_SCHED_PORT_ACT1"
#define  CFG_WEEK_SCHED_PORT_RESET_EN          "WEEK_SCHED_PORT_RESET_EN"
#define  CFG_WEEK_SCHED_PORT_RESET             "WEEK_SCHED_PORT_RESET"
#define  CFG_PROD_MAX_POE_PORTS                "PROD_MAX_POE_PORTS"
#define  CFG_PROD_MAX_SWITCH_PORTS             "PROD_MAX_SWITCH_PORTS"
#define  CFG_PROD_UNIT_INT_PWR_SUPPLY_MAX_PWR  "PROD_UNIT_INT_PWR_SUPPLY_MAX_PWR"
#define  CFG_PROD_SWITCH_MATRIX                "PROD_SWITCH_MATRIX"
#define  CFG_PROD_PORT_TYPE                    "PROD_PORT_TYPE"
#define  CFG_PROD_POE_PORT_MAX_PWR_MWATT       "PROD_POE_PORT_MAX_PWR_MWATT"
#define  CFG_PROD_POE_TYPE_MAX_PWR_MWATT       "PROD_POE_TYPE_MAX_PWR_MWATT"
#define  CFG_PROD_NAME                         "PROD_NAME"
#define  CFG_PROD_SNMP_OID                     "PROD_SNMP_OID"


// Default values go here

#endif	/* CONFIG_DEFS_H */

