/* 
 * File:   ConfigParser.cpp
 * Author: shikh
 * 
 * Created on December 11, 2014, 6:36 PM
 */

#include "ConfigParser.h"

#define MIN_CONFIG_LINE_LEN     4 // The minimum length for a line to be parsed - []{}

#define KEY_START               '['
#define KEY_END                 ']'

#define VALUE_START             '{'
#define VALUE_END               '}'

#define SUBVALUE_START          '['
#define SUBVALUE_END            ']'

#define CONFIG_EOF              "END" // The key representing the end of the config

using namespace std;

ConfigParser::ConfigParser() :
error_(ERRMOD_CONFIG)
{
}

ConfigParser::~ConfigParser()
{
}

PdsErrcode ConfigParser::ParseLine(const string &line, string &o_key, string &o_value)
{
    if (line.length() < MIN_CONFIG_LINE_LEN)
        return error_.SetCode(ERRSEV_ERROR, ERR_BADPARAM);
    
    if (line[0] != KEY_START)
        return error_.SetCode(ERRSEV_ERROR, ERR_BADPARAM);
    
    // Search for the closing bracket
    size_t keyend;
    if ((keyend = line.find(KEY_END)) == string::npos)
        return error_.SetCode(ERRSEV_ERROR, ERR_BADPARAM);
    
    // Get the key string between the brackets  - [KEY]
    string retkey = line.substr(1, keyend - 1);
    
    // Check for logical end of file
    if (retkey == CONFIG_EOF)
        return error_.SetCode(ERRSEV_INFO, ERR_EOF);
    
    // Search for the opening value bracket
    size_t valuestart = line.find(VALUE_START, keyend);
    if ((valuestart == string::npos) || (valuestart <= keyend))
        return error_.SetCode(ERRSEV_ERROR, ERR_BADPARAM);
    
    // And the closing bracket
    size_t valueend = line.find(VALUE_END, valuestart);
    if (valueend == string::npos)
        return error_.SetCode(ERRSEV_ERROR, ERR_BADPARAM);
    
    // TODO : CHECK THIS CALCULATION
    string retvalue = line.substr(valuestart + 1, valueend - valuestart - 1);
    
    o_key = retkey;
    o_value = retvalue;
    
    return ERR_OK;
}

PdsErrcode ConfigParser::ParseNested(const string& nested, NestedMap &io_values)
{
    PdsErrcode err;
    
    // We retrieve based on keys inside o_values
    // Check if we got anything to retrieve
    if (io_values.empty())
        return error_.SetCode(ERRSEV_ERROR, ERR_BADPARAM); // TODO : Maybe ERR_OK?
    
    // Find each key in the string
    NestedMap::iterator itr;
    for (itr = io_values.begin(); itr != io_values.end(); itr++)
    {
        // Search the string
        string currkey = itr->first;
        size_t valuestart, valuelen;
        if ((err = FindSubValue(nested, currkey, valuestart, valuelen)) != ERR_OK)
            return err;
        
        io_values[currkey] = nested.substr(valuestart, valuelen);
    }
    
    return ERR_OK;
}

PdsErrcode ConfigParser::SetNested(string& io_nested, const NestedMap& values)
{
    PdsErrcode err;
    
    // We update based on keys inside o_values
    // Check if we got anything to update
    if (values.empty())
        return error_.SetCode(ERRSEV_ERROR, ERR_BADPARAM); // TODO : Maybe ERR_OK?
    
    // Find each key in the string
    NestedMap::const_iterator itr;
    for (itr = values.begin(); itr != values.end(); itr++)
    {
        // Search the string
        string currkey = itr->first;
        size_t valuestart, valuelen;
        if ((err = FindSubValue(io_nested, currkey, valuestart, valuelen)) != ERR_OK)
            return err;
        
        // And replace it
        io_nested.replace(valuestart, valuelen, itr->second);
    }
    
    return ERR_OK;
}

PdsErrcode ConfigParser::FindSubValue(const string& nested, const string& subkey, size_t& o_valuestart, size_t& o_valuelen)
{
    size_t keystart, keyend, valuestart, valueend;
    
    // We add a delimiter to avoid partial matches
    string tempkey = subkey + SUBVALUE_START;
    
    // Find the key
    if ((keystart = nested.find(tempkey)) == string::npos)
        return error_.SetCode(ERRSEV_ERROR, ERR_PARAMNOTFOUND);
    
    // Find the value starting delimiter
    if ((valuestart = nested.find(SUBVALUE_START, keystart)) == string::npos)
        return error_.SetCode(ERRSEV_ERROR, ERR_BADPARAM);
    
    valuestart = keystart + tempkey.length();
    
    // Find the value ending delimiter
    if ((valueend = nested.find(SUBVALUE_END, valuestart)) == string::npos)
        return error_.SetCode(ERRSEV_ERROR, ERR_BADPARAM);
    
    // Calculate the return values
    o_valuestart = valuestart;
    o_valuelen = valueend - valuestart;
    
    return ERR_OK;
}




