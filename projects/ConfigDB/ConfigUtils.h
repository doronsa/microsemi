/* 
 * File:   ConfigUtils.h
 * Author: shikh
 *
 * Created on December 17, 2014, 3:13 PM
 */

#ifndef CONFIGUTILS_H
#define	CONFIGUTILS_H

#include <string>

class ConfigUtils
{
public:
    static bool ToBool(std::string value);
    
private:
    ConfigUtils();
    virtual ~ConfigUtils();

};

#endif	/* CONFIGUTILS_H */

