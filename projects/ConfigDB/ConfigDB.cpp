/* 
 * File:   ConfigDB.cpp
 * Author: shikh
 * 
 * Created on December 16, 2014, 11:00 AM
 */

#include "ConfigDB.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdint.h>

using namespace std;

std::string ConfigDBError::ToString() const
{
    stringstream ss;
    ss << 
    PdsError::ToString() <<
    " At line " <<
    line_;
    
    return ss.str();
}

PdsErrcode ConfigDB::GetValue(const std::string& key, std::string& o_value)
{
    // Search the DB for the key
    ConfigMap::iterator itr;
    PdsErrcode err;
    if ((err = FindInDB(key, itr)) != ERR_OK)
        return err;
    
    // Return the associated value
    o_value = itr->second;
    
    return ERR_OK;
}

PdsErrcode ConfigDB::GetNestedValue(const std::string& key, map<string, string>& io_values)
{
    // Search the DB for the key
    ConfigMap::iterator itr;
    PdsErrcode err;
    if ((err = FindInDB(key, itr)) != ERR_OK)
        return err;

    // Get the value
    string currvalue = itr->second;
    
    // Parse it based on the keys we received in the map
    if (parser_.ParseNested(currvalue, io_values) != ERR_OK)
        return error_.SetCode(ERRSEV_ERROR, ERR_PARSEFAIL);
    
    return ERR_OK;
}

PdsErrcode ConfigDB::SetValue(const string& key, const string& value)
{
    // TODO : Allow setting keys that don't yet exist?
    
    // Search the DB for the key
    ConfigMap::iterator itr;
    PdsErrcode err;
    if ((err = FindInDB(key, itr)) != ERR_OK)
        return err;
    
    // Set the new value
    itr->second = value;
    
    return ERR_OK;
}

PdsErrcode ConfigDB::SetNestedValue(const string& key, const NestedMap& values)
{
    // Search the DB for the key
    ConfigMap::iterator itr;
    PdsErrcode err;
    if ((err = FindInDB(key, itr)) != ERR_OK)
        return err;

    // Get the value
    string currvalue = itr->second;
    
    // Parse it based on the keys we received in the map
    if (parser_.SetNested(currvalue, values) != ERR_OK)
        return error_.SetCode(ERRSEV_ERROR, ERR_PARSEFAIL);
    
    // Update the DB
    itr->second = currvalue;
    
    return ERR_OK;
}


PdsErrcode ConfigDB::Load(const string filename)
{
    // TODO : Work directly with file or as a string? Currently stream implementation supports both
    
    // Open the config file
    // TODO : TEMP
    ifstream file(filename.c_str());
    if (!file.is_open())
        return error_.SetCode(ERRSEV_ERROR, ERR_FILEOPENFAIL);
    
    // Parse it and close
    PdsErrcode err;
    if ((err = ParseStream(file)) == ERR_OK)
        loaded_ = true;
    
    file.close();
    
    return err;
}

PdsErrcode ConfigDB::ParseStream(istream &configstream)
{
    uint32_t checksum = 0;
    
    string line;
    
    // The first line is our checksum
    if (!getline(configstream, line))
        return error_.SetCode(ERRSEV_ERROR, ERR_FILEREADFAIL);
    
    // Extract the checksum
    stringstream ss;
    ss << std::hex << line;
    ss >> checksum;
    
    // TODO : CHECKSUM
    
    // Parse the values line by line
    string key, value;
    int linenum = 1; // Considering the checksum
    while (getline(configstream, line))
    {
        linenum++;
        
        // Skip empty lines
        // TODO : Stupid linux/windows end of line crap
        if (line.empty() || line == "\r")
            continue;
        
        // Parse the line
        PdsErrcode err;
        err = parser_.ParseLine(line, key, value);
        
        // Check if logical eof was reached
        if (err == ERR_EOF)
        {
            break;
        }
        else if (err != ERR_OK)
        {
            error_.SetLine(linenum);
            return error_.SetCode(ERRSEV_ERROR, ERR_PARSEFAIL);
        }
        
        // The line was parsed, save the key and value in the DB
        values_[key] = value;
    }
    
    return ERR_OK;
}

PdsErrcode ConfigDB::FindInDB(const std::string& key, ConfigMap::iterator& o_itr)
{
    // Init check
    if (!loaded_)
        return error_.SetCode(ERRSEV_ERROR, ERR_NOTINIT);
    
    // Search for the key in the map
    o_itr = values_.find(key);
    
    // Error if not found
    if (o_itr == values_.end())
        return error_.SetCode(ERRSEV_ERROR, ERR_PARAMNOTFOUND);
    
    return ERR_OK;
}
