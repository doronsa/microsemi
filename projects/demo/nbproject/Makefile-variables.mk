#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug=../builds/x86/debug
CND_ARTIFACT_NAME_Debug=demo
CND_ARTIFACT_PATH_Debug=../builds/x86/debug/demo
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug=demo.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux-x86/package/demo.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux-x86
CND_ARTIFACT_NAME_Release=demo
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux-x86/demo
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release=demo.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux-x86/package/demo.tar
# CROSS_ARM_Debug configuration
CND_PLATFORM_CROSS_ARM_Debug=MARVELL_ARM-Linux-x86
CND_ARTIFACT_DIR_CROSS_ARM_Debug=../builds/arm/debug
CND_ARTIFACT_NAME_CROSS_ARM_Debug=demo
CND_ARTIFACT_PATH_CROSS_ARM_Debug=../builds/arm/debug/demo
CND_PACKAGE_DIR_CROSS_ARM_Debug=dist/CROSS_ARM_Debug/MARVELL_ARM-Linux-x86/package
CND_PACKAGE_NAME_CROSS_ARM_Debug=demo.tar
CND_PACKAGE_PATH_CROSS_ARM_Debug=dist/CROSS_ARM_Debug/MARVELL_ARM-Linux-x86/package/demo.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
