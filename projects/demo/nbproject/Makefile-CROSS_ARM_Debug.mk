#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=arm-marvell-linux-uclibcgnueabi-gcc
CCC=arm-marvell-linux-uclibcgnueabi-g++
CXX=arm-marvell-linux-uclibcgnueabi-g++
FC=gfortran
AS=arm-marvell-linux-uclibcgnueabi-as

# Macros
CND_PLATFORM=MARVELL_ARM-Linux-x86
CND_DLIB_EXT=so
CND_CONF=CROSS_ARM_Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L../builds/arm/debug/ -Wl,-rpath,../builds/arm/debug/

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ../builds/arm/debug/demo

../builds/arm/debug/demo: ${OBJECTFILES}
	${MKDIR} -p ../builds/arm/debug
	${LINK.cc} -o ../builds/arm/debug/demo ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../sdk/Tools/armv5-marvell-linux-uclibcgnueabi-soft_i686/arm-marvell-linux-uclibcgnueabi/libc/usr/include -I../../sdk/Tools/armv5-marvell-linux-uclibcgnueabi-soft_i686/arm-marvell-linux-uclibcgnueabi/include/c++/4.6.3 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ../builds/arm/debug/demo

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
