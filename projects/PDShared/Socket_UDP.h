/* 
 * File:   Socket_UDP.h
 * Author: shikh
 *
 * Created on December 3, 2014, 3:34 PM
 */

#ifndef SOCKET_UDP_H
#define	SOCKET_UDP_H

#include "Socket.h"

#include <string>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

namespace Shared
{
    
class Socket_UDP : public Socket 
{
public:
    Socket_UDP();
    virtual ~Socket_UDP();
    
    bool Bind(uint16_t port);
    
    virtual bool Send(const uint8_t *buf, int length, const std::string &dest_address, uint16_t dest_port);
    virtual int Recv(uint8_t *buf, int length, std::string &src_address, uint16_t &src_port);
    
    // When we don't care about the source
    virtual int Recv(uint8_t *buf, int length);
    
    /*virtual bool Send(const uint8_t* buf, int length, uint32_t dest_address, uint16_t dest_port);
    virtual int Recv(uint8_t* buf, int length, uint32_t &src_address, uint16_t &src_port);*/
private:

};

} // namespace Shared

#endif	/* SOCKET_UDP_H */

