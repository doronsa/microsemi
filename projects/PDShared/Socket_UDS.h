/* 
 * File:   Socket_UDS.h
 * Author: shikh
 *
 * Created on December 7, 2014, 12:42 PM
 */

#ifndef SOCKET_UDS_H
#define	SOCKET_UDS_H

#include "Socket.h"
#include "OSError.h"

#include <string>
namespace Shared
{
    
class Socket_UDS : public Socket {
public:
    Socket_UDS();
    virtual ~Socket_UDS() {};
    
    PdsErrcode Bind(const std::string &path);
    
    std::string GetPath() { return path_; };
    
    virtual PdsErrcode Send(const uint8_t *buf, int length, const std::string &dest_path);
    virtual PdsErrcode Recv(uint8_t *buf, int length, int &o_recvlen, std::string &src_path);
    
    // When we don't care about the source
    virtual PdsErrcode Recv(uint8_t *buf, int length, int &o_recvlen);
    
private:
    
    // FS path for the socket
    std::string path_;

};

} // namespace Shared
#endif	/* SOCKET_UDS_H */

