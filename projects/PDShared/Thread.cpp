/* 
 * File:   Thread.cpp
 * Author: shikh
 * 
 * Created on December 7, 2014, 5:44 PM
 */

#include "Thread.h"

namespace Shared
{

bool Thread::Start()
{
    // Do any preliminary initialization
    if (!Init())
        return false;
    
    // Call the pthread create function with the static function and this object as an argument
    pthread_create(&thread_, NULL, &Thread::Run, this);
    
    // TODO : Any chance this will not be the case?
    // Detach the thread for automatic garbage collection
    pthread_detach(thread_);
    
    return true;
}

void* Thread::Run(void *me)
{
    if (me == NULL)
        return NULL;
    
    ((Thread*)me)->Main();
}

} // namespace Shared