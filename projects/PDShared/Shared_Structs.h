/* 
 * File:   Shared_Structs.h
 * Author: shikh
 *
 * Created on December 8, 2014, 2:51 PM
 */

#ifndef SHARED_STRUCTS_H
#define	SHARED_STRUCTS_H

#include "Defs.h"

struct
{
    uint8_t   Port_Config;  // Defined Port Config. Bit0-1=AF/AT/PoH, Bit2=Force Pwr, Bit3=4pair-EN, bit4=Port Enabled, get from 'GET_NEW_PORT_STATUS'                                                                               
    uint8_t   Port_Actual;  // Actual Port Config.  Bit0-1=AF/AT/PoH, Bit2=Force Pwr, Bit3=4pair-EN, bit4=Port Enabled, get from 'GET_NEW_PORT_STATUS'                                                                                                       
    uint8_t   Status     ;  // Port Status               get from 'GET_NEW_PORT_STATUS'                                                                              
    uint8_t   Class      ;  // Port Class                get from 'GET_NEW_PORT_STATUS'                                                                                                                                                                        

    uint32_t  Cntr_UDL   ;  // UDL counter               get from 'GET_NEW_PORT_STATUS'                                                                                                                                            
    uint32_t  Cntr_OVL   ;  // OVL counter               get from 'GET_NEW_PORT_STATUS'                                                                                                                                            
    uint32_t  Cntr_SC    ;  // SC  counter               get from 'GET_NEW_PORT_STATUS'                                                                                                                                           
    uint32_t  Cntr_INVSIG;  // invalid signature counter get from 'GET_NEW_PORT_STATUS'                                                                                                                              
    uint32_t  Cntr_PWRDNY;  // power denied counter      get from 'GET_NEW_PORT_STATUS'                                                                                                                                   

    uint32_t  Power      ;  // In mWatt                  get from 'GET_4PAIR_PORT_MEASUREMENTS'
    uint16_t  Volt       ;  // in Dec Volt               get from 'GET_PORT_MEASUREMENTS'
    uint16_t  Current    ;  // in mAmp MAMP              get from 'GET_PORT_MEASUREMENTS'

}POE_Port;


#endif	/* SHARED_STRUCTS_H */

