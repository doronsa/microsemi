/* 
 * File:   ErrorHandler.cpp
 * Author: shikh
 * 
 * Created on December 18, 2014, 11:43 AM
 */

#include "ErrorHandler.h"

#include <iostream>

using namespace std;

namespace Shared
{

void ErrorHandler::HandleError(PdsError& error)
{
    // This method should be overriden for any module that wishes to handle errors differently
    // Simply print out the error
    cout << error.ToString() << endl;
}

} // namespace Shared