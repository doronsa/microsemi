/* 
 * File:   Socket_UDP.cpp
 * Author: shikh
 * 
 * Created on December 3, 2014, 3:34 PM
 */

#include "Socket_UDP.h"
#include <memory>
#include <string.h>
#include <arpa/inet.h>
#include <netdb.h>

using namespace std;

namespace Shared
{
    
Socket_UDP::Socket_UDP() :
Socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
{
}

Socket_UDP::~Socket_UDP()
{
}

bool Socket_UDP::Bind(uint16_t port)
{
    struct sockaddr_in si_local;
    memset((uint8_t*)&si_local, 0, sizeof(si_local));
    
    si_local.sin_family = AF_INET;
    si_local.sin_port = htons(port);
    si_local.sin_addr.s_addr = htonl(INADDR_ANY);
    
    if(bind(socket_ , (struct sockaddr*)&si_local, sizeof(si_local)) == -1)
        return false;
    
    return true;
}

bool Socket_UDP::Send(const uint8_t *buf, int length, const string &dest_address, uint16_t dest_port)
{
    struct sockaddr_in si_remote;
    
    si_remote.sin_family = AF_INET;
    si_remote.sin_port = htons(dest_port);
    
    struct hostent *host;
    if ((host = gethostbyname(dest_address.c_str())) == NULL)
        return false;
    
    si_remote.sin_addr.s_addr = *((unsigned long *) host->h_addr_list[0]);
    
    if (sendto(socket_, buf, length, 0, (struct sockaddr *) &si_remote, sizeof(si_remote)) == -1)
        return false;
    
    return true;
}

int Socket_UDP::Recv(uint8_t *buf, int length)
{
    struct sockaddr_in si_remote;
    unsigned int si_len = sizeof(si_remote);
    
    return (recvfrom(socket_, buf, length, 0, (struct sockaddr *) &si_remote, &si_len));
}

int Socket_UDP::Recv(uint8_t *buf, int length, string &src_address, uint16_t &src_port)
{
    int received = 0;
    struct sockaddr_in si_remote;
    unsigned int si_len = sizeof(si_remote);
    
    received = recvfrom(socket_, buf, length, 0, (struct sockaddr *) &si_remote, &si_len);
    
    // Return the values in readable format if the receive was successful
    if (received != -1)
    {
        // TODO : IPv6?
        src_address = inet_ntoa(si_remote.sin_addr);
        src_port = ntohs(si_remote.sin_port);
    }
    
    return received;
}

} // namespace Shared
