/* 
 * File:   ErrorHandler.h
 * Author: shikh
 *
 * Created on December 18, 2014, 11:43 AM
 */

#ifndef ERRORHANDLER_H
#define	ERRORHANDLER_H

#include "PdsError.h"

namespace Shared
{

class ErrorHandler
{
public:
    ErrorHandler() {};
    virtual ~ErrorHandler() {};

    /**
     * Handles an error object
     * @param error The error to handle
     */
    virtual void HandleError(PdsError &error);
private:

};

} // namespace Shared
#endif	/* ERRORHANDLER_H */

