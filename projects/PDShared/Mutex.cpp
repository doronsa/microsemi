/* 
 * File:   Mutex.cpp
 * Author: microsemi
 * 
 * Created on December 17, 2014, 6:38 PM
 */

#include "Mutex.h"
namespace Shared
{

Mutex::Mutex() :
error_(ERRMOD_MUTEX)
{
    // Create the mutex
    // TODO : ERROR HANDLING?
    pthread_mutex_init(&mutex_, NULL);
}

void Mutex::Get()
{
    pthread_mutex_lock(&mutex_);
}

void Mutex::Release()
{
    pthread_mutex_unlock(&mutex_);
}

} // namespace Shared