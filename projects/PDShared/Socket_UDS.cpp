/* 
 * File:   Socket_UDS.cpp
 * Author: shikh
 * 
 * Created on December 7, 2014, 12:42 PM
 */

#include "Socket_UDS.h"
#include "PdsError.h"

#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>

namespace Shared
{
    
Socket_UDS::Socket_UDS() :
Socket(AF_UNIX, SOCK_DGRAM)
{
}

PdsErrcode Socket_UDS::Bind(const std::string &path)
{
    // Save the path
    path_ = path;
    
    // Initialize address struct
    struct sockaddr_un su_local;
    memset((uint8_t*)&su_local, 0, sizeof(su_local));
    
    // Init the path
    su_local.sun_family = AF_UNIX;
    memcpy(su_local.sun_path, path_.c_str(), path_.length() + 1);
    
    // Bind the sucket
    if(bind(socket_ , (struct sockaddr*)&su_local, sizeof(su_local)) == -1)
    {
        error_.SetErrnum(errno);
        return error_.SetCode(ERRSEV_ERROR, ERR_OSERROR);
    }
    
    return ERR_OK;
}

PdsErrcode Socket_UDS::Send(const uint8_t *buf, int length, const std::string &dest_path)
{
    struct sockaddr_un su_remote;
    
    // Initialize the remote address
    su_remote.sun_family = AF_UNIX;
    memcpy(su_remote.sun_path, dest_path.c_str(), dest_path.length() + 1);
    
    // Send the data
    if (sendto(socket_, buf, length, 0, (struct sockaddr *) &su_remote, sizeof(su_remote)) == -1)
    {
        error_.SetErrnum(errno);
        return error_.SetCode(ERRSEV_ERROR, ERR_OSERROR);
    }
    
    return ERR_OK;
}

PdsErrcode Socket_UDS::Recv(uint8_t *buf, int length, int &o_recvlen)
{
    struct sockaddr_un su_remote;
    unsigned int su_len = sizeof(su_remote);
    
    if ((o_recvlen = recvfrom(socket_, buf, length, 0, (struct sockaddr *) &su_remote, &su_len)) == -1)
    {
        error_.SetErrnum(errno);
        return error_.SetCode(ERRSEV_ERROR, ERR_OSERROR);
    }
    
    return ERR_OK;
}

PdsErrcode Socket_UDS::Recv(uint8_t *buf, int length, int &o_recvlen, std::string &src_path)
{
    struct sockaddr_un su_remote;
    unsigned int su_len = sizeof(su_remote);
    
    if ((o_recvlen = recvfrom(socket_, buf, length, 0, (struct sockaddr *) &su_remote, &su_len)) == -1)
    {
        error_.SetErrnum(errno);
        return error_.SetCode(ERRSEV_ERROR, ERR_OSERROR);
    }
    
    src_path = su_remote.sun_path;
    
    return ERR_OK;
}

} // namespace Shared