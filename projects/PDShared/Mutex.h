/* 
 * File:   Mutex.h
 * Author: microsemi
 *
 * Created on December 17, 2014, 6:38 PM
 */

#ifndef MUTEX_H
#define	MUTEX_H

#include "LockType.h"
#include "OSError.h"

#include <pthread.h>

namespace Shared
{
    
class Mutex : public ILockType
{
public:
    Mutex();
    
    virtual ~Mutex() {};
    
    OSError& GetError() { return error_; };
    
    virtual void Get();

    virtual void Release();
    
private:
    // The actual mutex object
    pthread_mutex_t mutex_;
    
    OSError error_;
};

} // namespace Shared
#endif	/* MUTEX_H */

