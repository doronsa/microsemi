/* 
 * File:   Lockable.h
 * Author: microsemi
 *
 * Created on December 17, 2014, 7:23 PM
 */

#ifndef LOCKABLE_H
#define	LOCKABLE_H

namespace Shared
{
    
class ILockType
{
public:
    virtual void Get() = 0;
    virtual void Release() = 0;
};

} // namespace Shared

#endif	/* LOCKABLE_H */

