/* 
 * File:   Pds_Error.cpp
 * Author: shikh
 * 
 * Created on December 13, 2014, 8:46 PM
 */

#include "PdsError.h"
#include <stdint.h>
#include <sstream>
using namespace std;

// Converts the given severity to a string
string PdsError::SeverityToString(PdsErrseverity severity)
{
    switch (severity)
    {
        case (ERRSEV_INFO):
            return "Info";
        case (ERRSEV_WARN):
            return "Warning";
        case (ERRSEV_ERROR):
            return "Error";
        case (ERRSEV_CRITICAL):
            return "Critical error";
    }
}


PdsError::PdsError(PdsErrmodule module) :
module_(module),
severity_(ERRSEV_INFO),
code_(ERR_OK)
{
}

PdsError::~PdsError()
{
}

string PdsError::ToString() const
{
    stringstream retstr;
    retstr << 
    SeverityToString(severity_) << 
    ": Module " << 
    module_ << 
    " reports code " << 
    code_;
    
    return retstr.str();
}
