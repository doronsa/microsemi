/* 
 * File:   Socket.h
 * Author: shikh
 *
 * Created on December 3, 2014, 12:50 PM
 */

#ifndef SOCKET_H
#define	SOCKET_H

#include "Defs.h"
#include "OSError.h"

namespace Shared
{

class Socket 
{
public:
    virtual ~Socket();
    
    OSError& GetError() { return error_; };

protected:
    // Protected ctor - this is semi-abstract
    Socket(int domain, int type, int proto = 0);
    
    // The socket file handle
    int socket_;
    
    // Whether the socket was initialized successfully
    bool init_;
    
    OSError error_;
};

} // namespace Shared
#endif	/* SOCKET_H */

