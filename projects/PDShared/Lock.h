/* 
 * File:   Lock.h
 * Author: microsemi
 *
 * Created on December 17, 2014, 7:22 PM
 */

#ifndef LOCK_H
#define	LOCK_H

#include "LockType.h"

namespace Shared
{
    
class Lock
{
public:
    Lock(ILockType& lock);
    virtual ~Lock();
    
private:
    ILockType &lock_;
};

}
#endif	/* LOCK_H */

