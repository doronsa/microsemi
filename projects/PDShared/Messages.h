/* 
 * File:   Messages.h
 * Author: shikh
 *
 * Created on December 8, 2014, 2:57 PM
 */

#ifndef MESSAGES_H
#define	MESSAGES_H

#include "Defs.h"
#include "Shared_Structs.h"

#define MSG_HEADER_CONST 0xAA55BB66

// A basic message header used in all IPC
typedef struct
{
    uint32_t header_const;  // Should be MSG_HEADER_CONST
    uint32_t src_service;   // The service sending the message
    uint32_t dst_service;   // The service receiving the message
    uint32_t op;            // The operation to be performed
}Msg_Header;

typedef struct
{
    Msg_Header header;
    uint32_t port_num;
    POE_Port port_state;
}Msg_PoePortStatus;

#endif	/* MESSAGES_H */

