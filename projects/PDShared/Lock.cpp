/* 
 * File:   Lock.cpp
 * Author: microsemi
 * 
 * Created on December 17, 2014, 7:22 PM
 */

#include "Lock.h"

using namespace Shared;

Lock::Lock(ILockType& lock):
lock_(lock)
{
    lock_.Get();
}

Lock::~Lock()
{
    lock_.Release();
}

