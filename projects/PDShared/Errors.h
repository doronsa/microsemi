/* 
 * File:   Errors.h
 * Author: shikh
 *
 * Created on December 11, 2014, 6:21 PM
 */

#ifndef ERRORS_H
#define	ERRORS_H

// Error severity levels
typedef enum
{
    ERRSEV_INFO     = 0,
    ERRSEV_WARN     = 1,
    ERRSEV_ERROR    = 2,
    ERRSEV_CRITICAL = 3
}PdsErrseverity;

// Definitions for all the modules returning errors in the system
typedef enum
{
    ERRMOD_GENERAL  = 0,
    
    ERRMOD_SOCKET   = 1,
    ERRMOD_THREAD   = 2,
    ERRMOD_MUTEX    = 3,
    
    ERRMOD_APP      = 4,
    ERRMOD_CONFIG   = 5,
    ERRMOD_SERVICE  = 6,
    
    // Add other modules here
}PdsErrmodule;

// Definitions for all error codes in the system
typedef enum
{
    ERR_OK              = 0,    // No error occured
    ERR_GENERAL         = 1,    // Any error except those specified here
    ERR_NULLPTR         = 2,    // A null pointer was detected
    ERR_MEMALLOC        = 3,    // Failed allocating memory
    ERR_NOTINIT         = 4,    // Attempt to use an unintialized object
    ERR_BADPARAM        = 5,    // An invalid parameter passed to a function
    ERR_BADCONFIG       = 6,    // Configuration parameter is wrong
    ERR_OSERROR         = 7,    // The OS returned an error on a fuction call
    
    // File access errors
    ERR_FILEOPENFAIL    = 50,    // A file could not be opened
    ERR_FILEREADFAIL    = 51,    // A file could not be read
    ERR_FILEWRITEFAIL   = 52,    // A file could not be written
    ERR_EOF             = 53,    // End of file was reached
    ERR_PARSEFAIL       = 54,    // File parsing failed
    
    // Configuration specific errors
    ERR_CHECKSUMINVALID = 100,   // Configuration file checksum is invalid
    ERR_PARAMNOTFOUND   = 101,   // Attempt to access a parameter not in the DB
    ERR_NOTNESTEDPARAM  = 102,   // Tried to retrieve subvalues from non-nested parameter
    
    // Service errors
    ERR_SRVCLAUNCHFAIL = 150, // Service failed at startup
    ERR_SRVCNOTENABLED = 151, // The service wasn't launched
    
    // Communication errors
    ERR_COMMBINDFAIL   = 200, // We failed to bind our local socket
    ERR_COMMSENDFAIL       = 201, // Sending failed
    ERR_COMMRECVFAIL       = 202, // Receiving failed

}PdsErrcode;

#endif	/* ERRORS_H */

