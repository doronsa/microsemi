/* 
 * File:   Pds_Error.h
 * Author: shikh
 * 
 * Created on December 13, 2014, 8:46 PM
 */

#ifndef PDS_ERROR_H
#define	PDS_ERROR_H

#include "Errors.h"
#include <string>

class PdsError
{
public:
    
    static std::string SeverityToString(PdsErrseverity severity);
    
    PdsError(PdsErrmodule module);
    virtual ~PdsError();
    
    // Retrieves the error code
    PdsErrcode GetCode() { return code_; };
    
    // Sets the error code. 
    // Also returns it so things like "return err.SetCode()" are possible
    PdsErrcode SetCode(PdsErrseverity severity, PdsErrcode code)
    {
        severity_ = severity;
        return (code_ = code); 
    };
    
    // Converts the error to a string for display
    virtual std::string ToString() const;
    
protected:
    
    // The specific error type that occured
    PdsErrcode code_;
    
    // The severity of the error
    PdsErrseverity severity_;
    
private:
    
    // The module this error object belongs to
    const PdsErrmodule module_;
};

#endif	/* PDS_ERROR_H */

