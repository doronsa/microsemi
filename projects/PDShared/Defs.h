/* 
 * File:   Defs.h
 * Author: shikh
 *
 * Created on December 3, 2014, 3:47 PM
 */

#ifndef DEFS_H
#define	DEFS_H

#include <stdint.h>

// Socket names for various services and the like
#define APP_UDS_PATH "/tmp/app_socket"
#define DATACONT_UDS_PATH "/tmp/datacont_socket"
#define POE_UDS_PATH "/tmp/poe_socket"

#endif	/* DEFS_H */

