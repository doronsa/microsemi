/* 
 * File:   OSError.cpp
 * Author: microsemi
 * 
 * Created on December 17, 2014, 6:44 PM
 */

#include "OSError.h"
#include <string>
#include <sstream>

using namespace std;
using namespace Shared;

string OSError::ToString() const
{
    stringstream ss;
    ss << 
    PdsError::ToString() <<
    " OS error code: " <<
    errnum_;
    
    return ss.str();
}