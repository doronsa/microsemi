/* 
 * File:   Socket.cpp
 * Author: shikh
 * 
 * Created on December 3, 2014, 12:50 PM
 */

#include "Socket.h"
#include <sys/socket.h>

namespace Shared
{

Socket::Socket(int domain, int type, int proto) :
init_(false),
error_(ERRMOD_SOCKET)
{
    if (socket_ = socket(domain, type, proto) == -1)
        return;
    
    init_ = true;
}

Socket::~Socket()
{
}

} // namespace Shared

