/* 
 * File:   Thread.h
 * Author: shikh
 *
 * Created on December 7, 2014, 5:44 PM
 */

#ifndef THREAD_H
#define	THREAD_H

#include <pthread.h>

namespace Shared
{
    
class Thread {
public:
    
    Thread() {};
    virtual ~Thread() {};
    
    // Creates the actual threads and starts it
    bool Start();
    
protected:
    
    // Should be overriden by any thread that requires initialization before running
    virtual bool Init() { return true; };
    
    // The main thread method, to be implemented in inheriting classes
    virtual void Main() = 0;
private:
    
    // Thread handle
    pthread_t thread_;
    
    // Used as an argument to pthread_create which can't accept member methods
    static void* Run(void *me);
};

} // namespace Shared

#endif	/* THREAD_H */

