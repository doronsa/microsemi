/* 
 * File:   OSError.h
 * Author: microsemi
 *
 * Created on December 17, 2014, 6:44 PM
 */

#ifndef OSERROR_H
#define	OSERROR_H

#include "PdsError.h"

namespace Shared
{
    
class OSError : public PdsError
{
public:
    OSError(PdsErrmodule module) :
    PdsError(module),
    errnum_(0)
    {};
    
    virtual void SetErrnum(int errnum) { errnum_ = errnum; };
    virtual int GetErrnum() const { return errnum_; };

    virtual std::string ToString() const;

protected:
    int errnum_;
};

} // namespace Shared;
#endif	/* OSERROR_H */

