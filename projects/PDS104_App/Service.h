/* 
 * File:   Service.h
 * Author: shikh
 *
 * Created on December 3, 2014, 11:33 PM
 */

#ifndef SERVICE_H
#define	SERVICE_H

#include "App_Defs.h"
#include <string>

class Service
{
public:
    Service(Service_ID id, std::string name);
    virtual ~Service();

    virtual bool Launch() = 0;

    virtual bool Stop()
    {
        return false;
    };

    bool IsActive()
    {
        return is_active_;
    }

protected:
    // Whether the service was launched
    bool is_active_;

private:
    //Service ID and name
    Service_ID id_;
    std::string name_;
};

class DHCPv4Service : public Service
{
public:

    DHCPv4Service() :
    Service(Service_ID_DHCPv4, "DHCPv4")
    {
    }

    virtual bool Launch();
    virtual bool Stop();
};

class DHCPv6Service : public Service
{
public:

    DHCPv6Service() :
    Service(Service_ID_DHCPv6, "DHCPv6")
    {
    }

    virtual bool Launch();
    virtual bool Stop();
};

class TelnetService : public Service
{
public:

    TelnetService() :
    Service(Service_ID_Telnet, "Telnet")
    {
    }

    virtual bool Launch();
    virtual bool Stop();
};

class SSHService : public Service
{
public:

    SSHService() :
    Service(Service_ID_SSH, "SSH")
    {
    }

    virtual bool Launch();
    virtual bool Stop();
};

class WebService : public Service
{
public:

    WebService() :
    Service(Service_ID_Web, "Web")
    {
    }

    virtual bool Launch();
    virtual bool Stop();
};

class NTPService : public Service
{
public:

    NTPService() :
    Service(Service_ID_NTP, "NTP")
    {
    }

    virtual bool Launch();
    virtual bool Stop();
};

class SNMPService : public Service
{
public:

    SNMPService() :
    Service(Service_ID_SNMP, "SNMP")
    {
    }

    virtual bool Launch();
    virtual bool Stop();
};

class AutoDiscoveryService : public Service
{
public:

    AutoDiscoveryService() :
    Service(Service_ID_AutoDiscovery, "AutoDiscovery")
    {
    }

    virtual bool Launch();
    virtual bool Stop();
};

#endif	/* SERVICE_H */

