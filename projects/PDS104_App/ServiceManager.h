/* 
 * File:   ServiceManager.h
 * Author: shikh
 *
 * Created on December 3, 2014, 11:31 PM
 */

#ifndef SERVICEMANAGER_H
#define	SERVICEMANAGER_H

#include "App_Defs.h"
#include "Service.h"

#include "../PDShared/PdsError.h"
#include "../PDShared/Mutex.h"

class ServiceError : public PdsError
{
public:
    ServiceError() :
    PdsError(ERRMOD_SERVICE),
    id_(Service_ID_Max)
    {};
    
    virtual ~ServiceError() {};
    
    virtual void SetService(Service_ID id) { id_ = id; };
    virtual Service_ID GetService() { return id_; };

protected:
    Service_ID id_;
};
class ServiceManager {
public:
    ServiceManager();
    virtual ~ServiceManager() {};
    
    PdsError& GetError() { return error_; };
    
    PdsErrcode Init();
    bool IsInit() { return init_; }
    
    PdsErrcode LaunchService(Service_ID service);
    bool IsEnabled(Service_ID service);
    PdsErrcode StopService(Service_ID service);
private:
    
    /**
     * Initializes all service objects to the appropriate class
     */
    void InitServices();

    // An array of all the services in the system
    Service *services_[Service_ID_Max];
    
    // Mutex protecting the service array
    Shared::Mutex services_mutex_;
    
    // Whether this class was initialized
    bool init_;
    
    ServiceError error_;
};

#endif	/* SERVICEMANAGER_H */

