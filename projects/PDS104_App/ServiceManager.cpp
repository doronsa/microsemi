/* 
 * File:   ServiceManager.cpp
 * Author: shikh
 * 
 * Created on December 3, 2014, 11:31 PM
 */

#include "ServiceManager.h"
#include "App_Defs.h"
#include "../PDShared/Lock.h"

using namespace Shared;

ServiceManager::ServiceManager() :
init_(false)
{
}

PdsErrcode ServiceManager::Init()
{
    // Initialize the service array
    InitServices();

    init_ = true;
    return ERR_OK;
}

PdsErrcode ServiceManager::LaunchService(Service_ID service)
{
    error_.SetService(service);
    
    // Lock the service array
    Lock lock(services_mutex_);

    // Check if such a service exists
    if (service >= Service_ID_Max || services_[service] == NULL)
        return error_.SetCode(ERRSEV_ERROR, ERR_SRVCNOTENABLED);

    // Call it launch function
    if (!(services_[service]->Launch()))
        return error_.SetCode(ERRSEV_ERROR, ERR_SRVCLAUNCHFAIL);

    error_.SetService(Service_ID_Max);
    return ERR_OK;
}

bool ServiceManager::IsEnabled(Service_ID service)
{
    // Lock the service array
    Lock lock(services_mutex_);
    
    if (service >= Service_ID_Max || services_[service] == NULL)
        return false;

    return (services_[service]->IsActive());
}

PdsErrcode ServiceManager::StopService(Service_ID service)
{
    error_.SetService(service);

    // Lock the service array
    Lock lock(services_mutex_);
    
    // Check if such a service exists
    if ((service >= Service_ID_Max) ||
    (services_[service] == NULL) ||
    (!(services_[service]->IsActive())))
    {
        return error_.SetCode(ERRSEV_ERROR, ERR_SRVCNOTENABLED);
    }

    // Call its stop function
    if (!(services_[service]->Stop()))
        return error_.SetCode(ERRSEV_ERROR, ERR_SRVCLAUNCHFAIL);

    error_.SetService(Service_ID_Max);
    return ERR_OK;
}

void ServiceManager::InitServices()
{
    // Don't init twice
    if (init_)
        return;
    
    // Add new services here
    services_[Service_ID_DHCPv4] = new DHCPv4Service();
    services_[Service_ID_DHCPv6] = new DHCPv6Service();
    services_[Service_ID_Telnet] = new TelnetService();
    services_[Service_ID_SSH] = new SSHService();
    services_[Service_ID_Web] = new WebService();
    services_[Service_ID_NTP] = new NTPService();
    services_[Service_ID_SNMP] = new SNMPService();
    services_[Service_ID_AutoDiscovery] = new AutoDiscoveryService();
}
