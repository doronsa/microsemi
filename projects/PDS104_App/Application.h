/* 
 * File:   Application.h
 * Author: shikh
 *
 * Created on December 7, 2014, 7:09 PM
 */

#ifndef APPLICATION_H
#define	APPLICATION_H

#include "ServiceManager.h"
#include "SocketManager.h"
#include "../ConfigDB/ConfigDB.h"

#include "../PDShared/ErrorHandler.h"

class Application {
public:
    Application();
    virtual ~Application();
    
    // Main method
    void Start();
    
private:
    
    /**
     * Initializes network parameters and services
     * @return Success or failure
     */
    bool InitNetwork();
    
    SocketManager socket_mng_;
    ServiceManager service_mng_;
    ConfigDB config_db_;
    Shared::ErrorHandler error_handler_;
};

#endif	/* APPLICATION_H */

