/* 
 * File:   MessageHandler.cpp
 * Author: microsemi
 * 
 * Created on December 18, 2014, 11:42 AM
 */

#include "MessageHandler.h"

MessageHandler::MessageHandler(SocketManager& socket_mng) :
socket_mng_(socket_mng)
{
}

bool MessageHandler::Init()
{
    // Check if the socket manager was already initialized
    return (socket_mng_.IsInit());
}


void MessageHandler::Main()
{
    
}

