/* 
 * File:   SocketManager.cpp
 * Author: shikh
 * 
 * Created on December 4, 2014, 10:26 AM
 */

#include "SocketManager.h"

#include <sstream>

using namespace std;
using namespace Shared;

// TODO : SEE IF THIS IS OK
std::string SocketManagerError::ToString() const
{
    stringstream ss;
    ss << 
    PdsError::ToString() <<
    " Socket ID: " <<
    socket_id_;
    
    return ss.str();
}

SocketManager::SocketManager() :
init_(false)
{
}

PdsErrcode SocketManager::Init()
{
    InitSockets();
    
    // Initialize our local socket for listening
    if ((local_socket_.Bind(uds_paths_[Socket_ID_App])) != ERR_OK)
    {
        // TODO : HANDLE ERRNO
        error_.SetSocketID(Socket_ID_App);
        return error_.SetCode(ERRSEV_ERROR, ERR_COMMBINDFAIL);
    }
    
    init_ = true;
    return ERR_OK;
}

PdsErrcode SocketManager::SendMsg(Socket_ID socket, uint8_t *msg, int length)
{
    if (!init_)
        return error_.SetCode(ERRSEV_ERROR, ERR_NOTINIT);
    
    if (socket > Socket_ID_Max)
        return error_.SetCode(ERRSEV_ERROR, ERR_BADPARAM);

    // Get the socket path from our local array and attempt to send
    if ((local_socket_.Send(msg, length, uds_paths_[socket])) != ERR_OK)
    {
        // TODO : HANDLE ERRNO
        error_.SetSocketID(socket);
        return error_.SetCode(ERRSEV_ERROR, ERR_COMMSENDFAIL);
    }
        
    return ERR_OK;
}

PdsErrcode SocketManager::RecvMsg(uint8_t *msg, int length, int &o_recvlen)
{
    if (!init_)
        return error_.SetCode(ERRSEV_ERROR, ERR_NOTINIT);
    
    // TODO : Multiple local sockets support?
    if ((local_socket_.Recv(msg, length, o_recvlen)) != ERR_OK)
    {
        // TODO : HANDLE ERRNO
        error_.SetSocketID(Socket_ID_App);
        return error_.SetCode(ERRSEV_ERROR, ERR_COMMRECVFAIL);
    }
    
    return ERR_OK;
}
void SocketManager::InitSockets()
{
    // Don't init twice
    if (!init_)
        return;
    
    // Add new socket definitions here
    uds_paths_[Socket_ID_App] = APP_UDS_PATH;
    uds_paths_[Socket_ID_DataCont] = DATACONT_UDS_PATH;
}

