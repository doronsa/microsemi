/* 
 * File:   App_Defs.h
 * Author: shikh
 *
 * Created on December 3, 2014, 11:15 PM
 */

#ifndef APP_DEFS_H
#define	APP_DEFS_H

#include "../PDShared/Defs.h"

typedef enum
{
    Service_ID_DHCPv4,
    Service_ID_DHCPv6,
    Service_ID_Telnet,
    Service_ID_SSH,
    Service_ID_Web,
    Service_ID_NTP,
    Service_ID_SNMP,
    Service_ID_AutoDiscovery,

    Service_ID_Max
}Service_ID;

typedef enum
{
    Socket_ID_App,
            
    Socket_ID_Remote,

    Socket_ID_DataCont = Socket_ID_Remote,
    Socket_ID_SNMP,
            
    Socket_ID_Max
}Socket_ID;

// Shell commands needed by the application
#define CMD_HOSTNAME "hostname "

// TODO : TEMP
#define CMD_SETIPV4 "/var/set_ip_addr.sh "
#define CMD_SETIPV4MASK "/var/set_ip_mask.sh "
#define CMD_SETIPV4GW "/var/set_ipv4_dgw.sh "

#define CMD_SETIPV6 "/var/set_ipv6.sh "
#define CMD_SETIPV6GW "/var/set_ipv6_dgw.sh "

#define CMD_ADDDNS "/var/set_dns_ip.sh "

#endif	/* APP_DEFS_H */

