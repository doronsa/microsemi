/* 
 * File:   main.cpp
 * Author: shikh
 *
 * Created on December 3, 2014, 4:07 PM
 */

#include <cstdlib>

#include "Application.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    Application app;
    
    app.Start();
    
    return 0;
}

