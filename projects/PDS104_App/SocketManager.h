/* 
 * File:   SocketManager.h
 * Author: shikh
 *
 * Created on December 4, 2014, 10:26 AM
 */

#ifndef SOCKETMANAGER_H
#define	SOCKETMANAGER_H

#include "App_Defs.h"
#include "../PDShared/Socket_UDS.h"
#include "../PDShared/PdsError.h"

class SocketManagerError : public PdsError
{
public:
    SocketManagerError() :
    PdsError(ERRMOD_SOCKET)
    {};
    
    virtual void SetSocketID(Socket_ID socket_id) { socket_id_ = socket_id; };
    virtual int GetSocketID() const { return socket_id_; };

    virtual std::string ToString() const;

protected:
    Socket_ID socket_id_;
};

class SocketManager {
public:
    SocketManager();
    virtual ~SocketManager() {};
    
    PdsErrcode Init();
    bool IsInit() { return init_; }
    
    PdsErrcode SendMsg(Socket_ID socket, uint8_t *msg, int length);
    PdsErrcode RecvMsg(uint8_t *msg, int length, int &o_recvlen);
    
private:
    
    // Initializes the socket paths stored here 
    void InitSockets();
    
    // Init status of the manager
    bool init_;
    
    // Our local UDS
    Shared::Socket_UDS local_socket_;
    
    // An array of paths to every socket managed by this
    std::string uds_paths_[Socket_ID_Max];
    
    SocketManagerError error_;
};

#endif	/* SOCKETMANAGER_H */

