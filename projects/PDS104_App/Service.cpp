/* 
 * File:   Service.cpp
 * Author: shikh
 * 
 * Created on December 3, 2014, 11:33 PM
 */

#include "Service.h"
#include <iostream>

using namespace std;

Service::Service(Service_ID id, string name)
{
    id_ = id;
    name_ = name;
}

Service::~Service()
{
}

bool DHCPv4Service::Launch()
{
    // TODO : ADD LAUNCH COMMAND HERE
    cout << "Service DHCPv4 not implemented" << endl;

    return (is_active_ = false);
}

bool DHCPv4Service::Stop()
{
    // Check if the service was launched in the first place
    if (!is_active_)
        return false; // TODO : true?

    // TODO : ADD STOP COMMAND HERE
    return (is_active_ = false);
}

bool DHCPv6Service::Launch()
{
    // TODO : ADD LAUNCH COMMAND HERE
    cout << "Service DHCPv6 not implemented" << endl;

    return (is_active_ = false);
}

bool DHCPv6Service::Stop()
{
    // Check if the service was launched in the first place
    if (!is_active_)
        return false; // TODO : true?

    // TODO : ADD STOP COMMAND HERE
    return (is_active_ = false);
}

bool TelnetService::Launch()
{
    // TODO : ADD LAUNCH COMMAND HERE
    cout << "Service Telnet not implemented" << endl;

    return (is_active_ = false);
}

bool TelnetService::Stop()
{
    // Check if the service was launched in the first place
    if (!is_active_)
        return false; // TODO : true?

    // TODO : ADD STOP COMMAND HERE
    return (is_active_ = false);
}

bool SSHService::Launch()
{
    // TODO : ADD LAUNCH COMMAND HERE
    cout << "Service SSH not implemented" << endl;

    return (is_active_ = false);
}

bool SSHService::Stop()
{
    // Check if the service was launched in the first place
    if (!is_active_)
        return false; // TODO : true?

    // TODO : ADD STOP COMMAND HERE
    return (is_active_ = false);
}

bool WebService::Launch()
{
    // TODO : ADD LAUNCH COMMAND HERE
    cout << "Service Web not implemented" << endl;

    return (is_active_ = false);
}

bool WebService::Stop()
{
    // Check if the service was launched in the first place
    if (!is_active_)
        return false; // TODO : true?

    // TODO : ADD STOP COMMAND HERE
    return (is_active_ = false);
}

bool NTPService::Launch()
{
    // TODO : ADD LAUNCH COMMAND HERE
    cout << "Service NTP not implemented" << endl;

    return (is_active_ = false);
}

bool NTPService::Stop()
{
    // Check if the service was launched in the first place
    if (!is_active_)
        return false; // TODO : true?

    // TODO : ADD STOP COMMAND HERE
    return (is_active_ = false);
}

bool SNMPService::Launch()
{
    // TODO : ADD LAUNCH COMMAND HERE
    cout << "Service SNMP not implemented" << endl;

    return (is_active_ = false);
}

bool SNMPService::Stop()
{
    // Check if the service was launched in the first place
    if (!is_active_)
        return false; // TODO : true?

    // TODO : ADD STOP COMMAND HERE
    return (is_active_ = false);
}

bool AutoDiscoveryService::Launch()
{
    // TODO : ADD LAUNCH COMMAND HERE
    cout << "Service AutoDiscovery not implemented" << endl;

    return (is_active_ = false);
}

bool AutoDiscoveryService::Stop()
{
    // Check if the service was launched in the first place
    if (!is_active_)
        return false; // TODO : true?

    // TODO : ADD STOP COMMAND HERE
    return (is_active_ = false);
}

