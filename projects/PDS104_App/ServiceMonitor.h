/* 
 * File:   ServiceMonitor.h
 * Author: microsemi
 *
 * Created on December 17, 2014, 6:20 PM
 */

#ifndef SERVICEMONITOR_H
#define	SERVICEMONITOR_H

#include "../PDShared/Thread.h"

class ServiceMonitor  : public Shared::Thread
{
public:
    ServiceMonitor();
    virtual ~ServiceMonitor();

    /**
     * Main thread function
     */
    //virtual void Main();

private:

};

#endif	/* SERVICEMONITOR_H */

