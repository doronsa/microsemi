/* 
 * File:   MessageHandler.h
 * Author: shikh
 *
 * Created on December 18, 2014, 11:42 AM
 */

#ifndef MESSAGEHANDLER_H
#define	MESSAGEHANDLER_H

#include "SocketManager.h"

#include "../PDShared/Thread.h"

class MessageHandler : public Shared::Thread
{
public:
    MessageHandler(SocketManager& socket_mng);
    virtual ~MessageHandler() {};
    
    // Initializes the handler
    virtual bool Init();

    // Main thread method
    virtual void Main();

private:
    // Our application socket manager
    SocketManager& socket_mng_;
};

#endif	/* MESSAGEHANDLER_H */

