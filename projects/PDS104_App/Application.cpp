/* 
 * File:   Application.cpp
 * Author: shikh
 * 
 * Created on December 7, 2014, 7:09 PM
 */

#include "Application.h"
#include "../ConfigDB/ConfigUtils.h"

#include <stdlib.h>
#include <iostream>

using namespace std;
using namespace Shared;

Application::Application()
{
}

Application::~Application()
{
}

void Application::Start()
{
    PdsErrcode err;
    
    // TODO : DEBUG
    //if (!socket_mng_.Init())
    //    return;
    
    if ((err = service_mng_.Init()))
    {
        cout << service_mng_.GetError().ToString() << endl; // TODO : ERROR HANDLING
        return;
    }
    
    // TODO : LOCAL ERROR HANDLING
    
    // Load our configuration
    // TODO : Load from multiple sources - backup, defaults
    if ((err = config_db_.Load(CONFIG_FILE_MAIN)) != ERR_OK)
        // ERROR HANDLING
        cout << config_db_.GetError().ToString() << endl;
    
    InitNetwork();
}

bool Application::InitNetwork()
{
    // TODO : ERROR HANDLING
    PdsErrcode err;
    string command, value;
    
    // Set hostname
    config_db_.GetValue(CFG_IP_HOST_NAME, value);
    command = CMD_HOSTNAME + value;
    system(const_cast<char  *>(command.c_str()));

    /* IPv4 configuration */
    
    // DHCP status
    bool dhcpv4_enabled = false;
    config_db_.GetValue(CFG_IPV4_DHCP_EN, value);
    dhcpv4_enabled = ConfigUtils::ToBool(value);
    
    if (dhcpv4_enabled)
    {
        // Launch the DHCP service
        service_mng_.LaunchService(Service_ID_DHCPv4);
    }
    else
    {
        // IPv4 Address
        config_db_.GetValue(CFG_IPV4_ADDR, value);
        command = CMD_SETIPV4 + value;
        system(const_cast<char  *>(command.c_str()));
        
        // Network mask
        config_db_.GetValue(CFG_IPV4_MASK, value);
        command = CMD_SETIPV4MASK + value;
        system(const_cast<char  *>(command.c_str()));
        
        // Default gateway
        config_db_.GetValue(CFG_IPV4_DGW, value);
        command = CMD_SETIPV4GW + value;
        system(const_cast<char  *>(command.c_str()));
    }
    
    /* IPv6 configuration */
    
    // DHCP status
    bool dhcpv6_enabled = false;
    config_db_.GetValue(CFG_IPV6_DHCP_EN, value);
    dhcpv6_enabled = ConfigUtils::ToBool(value);
    
    if (dhcpv6_enabled)
    {
        // Launch the DHCP service
        service_mng_.LaunchService(Service_ID_DHCPv6);
    }
    else
    {
        // IPv6 Address
        config_db_.GetValue(CFG_IPV6_ADDR, value);
        command = CMD_SETIPV6 + value;
        config_db_.GetValue(CFG_IPV6_PREFIX, value);
        command += "/" + value;
        system(const_cast<char  *>(command.c_str()));
        
        // Set default gateway
        config_db_.GetValue(CFG_IPV6_DGW, value);
        command = CMD_SETIPV6GW + value;
        system(const_cast<char  *>(command.c_str()));
    }
    
    // If we have any static IP(v4 or v6), add DNS servers from config
    if (dhcpv4_enabled || dhcpv6_enabled)
    {
        config_db_.GetValue(CFG_IP_DNS0, value);
        command = CMD_ADDDNS + value;
        system(const_cast<char  *>(command.c_str()));
        
        config_db_.GetValue(CFG_IP_DNS1, value);
        command = CMD_ADDDNS + value;
        system(const_cast<char  *>(command.c_str()));
    }
    
    // Telnet
    config_db_.GetValue(CFG_TELNET_EN, value);
    if (ConfigUtils::ToBool(value))
        service_mng_.LaunchService(Service_ID_Telnet);
    
    // SSH
    config_db_.GetValue(CFG_SSH_EN, value);
    if (ConfigUtils::ToBool(value))
        service_mng_.LaunchService(Service_ID_SSH);
    
    // Web server
    service_mng_.LaunchService(Service_ID_Web); // TODO : WEB SERVER SHOULD GET HTTPS CONFIG WHEN LAUNCHED
    
    // NTP
    service_mng_.LaunchService(Service_ID_NTP); // TODO : NTP SHOULD GET NTP SERVER CONFIG WHEN LAUNCHED
    
    // SNMP
    // If either SNMPv2 or v3 is enabled, launch the service
    config_db_.GetValue(CFG_SNMPV2_EN, value);
    bool snmpv2_enabled = ConfigUtils::ToBool(value);
    config_db_.GetValue(CFG_SNMPV3_EN, value);
    bool snmpv3_enabled = ConfigUtils::ToBool(value);
    
    // TODO : SPLIT THIS?
    if (snmpv2_enabled || snmpv3_enabled)
        service_mng_.LaunchService(Service_ID_SNMP);
    
    return true;
}
