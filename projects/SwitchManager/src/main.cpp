/*
 * main.cpp
 *
 *  Created on: Dec 9, 2014
 *      Author: microsemi
 */

#include <iostream>
#include <stdlib.h>
#include "SwitchManager.h"


using namespace std;

int main(int argc, char **argv)
{
	GT_STATUS status;
	GT_BOOL linkState;
	GT_LPORT port;
	string str = "down";

	SwitchManager manager;


	if(argc != 2)
	{
		cout << "Usage: ./SwitchManager <PORT_NUM>" << endl;
		return 0;
	}

	port = (GT_LPORT)atoi(argv[1]);

	status = manager.init();

	cout << "Switch Manager init: 0x" << hex << status << endl;

	status = manager.getLinkState(port, linkState);

	if(status != GT_OK)
	{
		cout << "Get Link status failed: 0x" << hex << status << endl;
		return 0;
	}

	if(linkState)
	{
		str = "up";
	}
	cout << "link status of port " << port << "is " << str << endl;

	return 0;
}
