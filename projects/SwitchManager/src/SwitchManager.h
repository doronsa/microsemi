/*
 * SwitchManager.h
 *
 *  Created on: Dec 9, 2014
 *      Author: microsemi
 */

#ifndef SWITCHMANAGER_H_
#define SWITCHMANAGER_H_

#include "msApi.h"


#define SWITCH_CPU_PORT  0x5

/*
 *
 *
 * 	gprtGetDuplex()
	gprtSetDuplex()
	gprtGetSpeed()

	(6.5.1)
	gprtSetPortSpeed()
	gprtPortAutoNegEnable()
 *
 */

class SwitchManager
{
public:
	SwitchManager();
	virtual ~SwitchManager();

	GT_STATUS init(void);

	GT_STATUS getLinkState(GT_LPORT port, GT_BOOL &link);

private:
	GT_SYS_CONFIG m_cfg;
	GT_QD_DEV m_dev;
};



#endif /* SWITCHMANAGER_H_ */
