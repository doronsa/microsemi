//============================================================================
// Name        : SwitchManager.cpp
// Author      : Genadi
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "miiDriver.h"
#include "SwitchManager.h"

SwitchManager::SwitchManager()
{

}
SwitchManager::~SwitchManager()
{
	//qdUnloadDriver(&m_dev);
}

GT_STATUS SwitchManager::init(void)
{
	GT_STATUS gtStatus;
	gtStatus = miiDriverInit(&m_cfg, SWITCH_CPU_PORT);

	gtStatus = qdLoadDriver(&m_cfg, &m_dev);

	if((gtStatus != GT_OK) &&(gtStatus != GT_ALREADY_EXIST))
	{
		return gtStatus;
	}

	//MSG_PRINT(("Device ID     : 0x%x\n",dev->deviceId));
	//MSG_PRINT(("Base Reg Addr : 0x%x\n",dev->baseRegAddr));
	//MSG_PRINT(("No of Ports   : %d\n",dev->numOfPorts));
	//MSG_PRINT(("CPU Ports     : %d\n",dev->cpuPortNum));

	/*
	 *  start the QuarterDeck
	*/
	if((gtStatus=sysEnable(&m_dev)) != GT_OK)
	{
		return gtStatus;
	}

	    //MSG_PRINT(("QuarterDeck has been started.\n"));

	return GT_OK;
}

GT_STATUS SwitchManager::getLinkState(GT_LPORT port, GT_BOOL &link)
{
	GT_STATUS status;
	status = gprtGetLinkState(&m_dev, port, &link);

	return status;
}
