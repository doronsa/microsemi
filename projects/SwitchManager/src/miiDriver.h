/*
 * miiDriver.h
 *
 *  Created on: Dec 9, 2014
 *      Author: microsemi
 */

#ifndef MIIDRIVER_H_
#define MIIDRIVER_H_

#include "msApi.h"

GT_STATUS miiDriverInit(GT_SYS_CONFIG* cfg, GT_U8 cpuPort);

#endif /* MIIDRIVER_H_ */
