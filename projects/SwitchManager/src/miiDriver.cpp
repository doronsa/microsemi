/*
 * miiDriver.cpp
 *
 *  Created on: Dec 9, 2014
 *      Author: microsemi
 */

#include "miiDriver.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define REG_R "/sys/devices/platform/neta/switch/reg_r"
#define REG_W "/sys/devices/platform/neta/switch/reg_w"

GT_BOOL gtBspReadMii1 (GT_QD_DEV* dev, unsigned int portNumber , unsigned int MIIReg, unsigned int* value);
GT_BOOL gtBspWriteMii1 (GT_QD_DEV* dev, unsigned int portNumber , unsigned int MIIReg, unsigned int value);

void ReadPhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long *value);
void WritePhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long value);

GT_STATUS miiDriverInit(GT_SYS_CONFIG* cfgPtr, GT_U8 cpuPort)
{
    memset((char*)cfgPtr,0,sizeof(GT_SYS_CONFIG));

	cfgPtr->BSPFunctions.readMii   = gtBspReadMii1;//set for new  function
	cfgPtr->BSPFunctions.writeMii  = gtBspWriteMii1;//set for new  function
	cfgPtr->BSPFunctions.semCreate = NULL;
	cfgPtr->BSPFunctions.semDelete = NULL;
	cfgPtr->BSPFunctions.semTake   = NULL;
	cfgPtr->BSPFunctions.semGive   = NULL;


    cfgPtr->initPorts = GT_TRUE;    /* Set switch ports to Forwarding mode. If GT_FALSE, use Default Setting. */
    cfgPtr->cpuPortNum = cpuPort;
#ifdef MANUAL_MODE    /* not defined. this is only for sample */
    				/* user may want to use this mode when there are two QD switchs on the same MII bus. */
    cfg.mode.scanMode = SMI_MANUAL_MODE;    	/* Use QD located at manually defined base addr */
    cfg.mode.baseAddr = 0x10;    				/* valid value in this case is either 0 or 0x10 */
#else
#ifdef MULTI_ADDR_MODE
    cfg.mode.scanMode = SMI_MULTI_ADDR_MODE;    /* find a QD in indirect access mode */
    cfg.mode.baseAddr = 1;        				/* this is the phyAddr used by QD family device.
                                					Valid value are 1 ~ 31.*/
#else
    cfgPtr->mode.scanMode = SMI_AUTO_SCAN_MODE;    /* Scan 0 or 0x10 base address to find the QD */
    cfgPtr->mode.baseAddr = 0x10;
#endif
#endif

    return GT_OK;
}

GT_BOOL gtBspReadMii1 (GT_QD_DEV* dev, unsigned int portNumber , unsigned int MIIReg,
                        unsigned int* value)
{
    unsigned short L_registerIndex;
    unsigned long L_value;
    unsigned char dev_id;

    //MSG_PRINT(("gtBspReadMii1: start\n"));

    dev_id = portNumber;//(unsigned char)DEVICE0_ID;
    L_registerIndex = (unsigned short)MIIReg;
    //void ReadPhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long *value)
    ReadPhyReg(dev_id, L_registerIndex, &L_value);
    *value = L_value;

   /* MSG_PRINT(("gtBspReadMii1: %x %x 0x%x.\n",portNumber,L_registerIndex,L_value));*/

    return GT_TRUE;
}

GT_BOOL gtBspWriteMii1 (GT_QD_DEV* dev, unsigned int portNumber , unsigned int MIIReg,
                       unsigned int value)
{
    unsigned short L_registerIndex;
    unsigned long L_value;
    unsigned char dev_id;

   // MSG_PRINT(("gtBspWriteMii1: start\n"));

    dev_id = portNumber;//(unsigned char)DEVICE0_ID;
    L_registerIndex = (unsigned short)MIIReg;
    L_value = value;

    //void WritePhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long value)
    WritePhyReg(dev_id, L_registerIndex, L_value);

  /*  MSG_PRINT(("gtBspWriteMii1: %x %x %x\n",dev_id,L_registerIndex,L_value));*/

    return GT_TRUE;
}


unsigned short GetValue(void)
{
    int fd;
    char buf[256],data[1000];
//    char ch;
    unsigned short value = 0;

    snprintf(buf, sizeof(buf),"/sys/devices/platform/neta/switch/stats");
    //len = snprintf(buf, sizeof(buf),"/sys/devices/platform/neta/switch/status");

    fd = open(buf, O_RDONLY);
    if (fd < 0) {
        perror("switch-value");
        return fd;
    }

    read(fd, data, sizeof(unsigned short ));
    //memset(data,0,100);
    //read(fd, data, 1000);
    memcpy (&value , data ,sizeof(unsigned short ));
    //printf("\n************** GetValue: %X ->  %x %x\n", value,  data[0],data[1]);
    close(fd);
    return value;
}

void ReadPhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long *value)
{
	// if (md_init == 0)
	// 	init_mdio();
	// external PHY
//	struct ifreq ifr;
//	struct mii_ioctl_data *mii;
    int fd, len;
    char buf[10];//,val[10];
    memset(value,0,sizeof(unsigned long));
    unsigned short  val;

    fd = open(REG_R , O_WRONLY);
    if (fd < 0) {
        perror("reg r");
        return;
    }

    len = snprintf(buf, sizeof(buf), "%d %d 10", dev_id, registerIndex);
    write(fd, buf, len);
    close(fd);
    usleep(100000);
    val = GetValue();
    // memcpy(&val, buf, sizeof(unsigned short ));
     memcpy(value, &val, sizeof(unsigned short ));

	////printf ("\n\r<<< **** ReadPhyReg %d reg:%02x value:%04x *****\n", dev_id , registerIndex, val);

}



void WritePhyReg(unsigned char dev_id, unsigned short registerIndex, unsigned long value)
{
	// if (md_init == 0)
	// 	init_mdio();

//    struct ifreq ifr;
//    struct mii_ioctl_data *mii;
    int fd, len;
    char buf[10]; //,val[10];

    fd = open(REG_W , O_WRONLY);
    if (fd < 0) {
        perror("reg r");
        return;
    }

    len = snprintf(buf, sizeof(buf), "%u %u 10 %lu", dev_id, registerIndex ,value);
    write(fd, buf, len);
    close(fd);

  	//printf (">>> WritePhyReg %d %02x: %04x\n", registerIndex, value);

}

