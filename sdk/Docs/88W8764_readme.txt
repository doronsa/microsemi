Wifi drvs and configuration scripts
------------------------------------
Under /root directory, there are 2 subdirs: wifi_normal and wifi_mfg
	- wifi_normal contains normal version drv and startup scripts.  
	  Commands in startup scripts are from drv Configuration Guide pdf file.
	- wifi_mfg contains mfg version drv, mfg FW, mfg daemon, and startup script.  
	  This dir is for mfg test only, and should be operated with the help of EEBU AE/FAEs.  
	  Mfg daemon runs on top of mfg drv, acting as a bridge between drv and Windows labtools.  
	  Labtools run on Win PC connected to ethernet port, which is bridged to wifi interface.
