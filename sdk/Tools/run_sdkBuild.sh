#!/bin/bash

export ARCH=arm
export CROSS_COMPILE=$(pwd)/armv5-marvell-linux-uclibcgnueabi-soft_i686/bin/arm-marvell-linux-uclibcgnueabi-
./sdkBuild.sh AVANTA_MC_SFU_VOIP
cp -f ../Output/Products/AVANTA_MC_SFU_VOIP/RecoveryImage/sdk_unified.img /tftpboot/
cp -f ../Source/Kernel/linux_feroceon-KW2/arch/arm/boot/uImage /tftpboot/
cp -f ../Output/Products/AVANTA_MC_SFU_VOIP/FileSystem/rootfs.tar /tftpboot/
