/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _MTHREAD_H_
#define _MTHREAD_H_

#ifdef __cplusplus
extern "C" {
#endif

#define MTHREAD_OK                   (0)
#define MTHREAD_ERROR                (-1) // must be less than 0

/*******************************************************************************
* MTHREAD_MQ_FN_T
*
* DESCRIPTION:      Proto type of message queue message handler
*
* INPUTS:
* msg           - buffer pointer of message
* size          - size of received message, in bytes
* pri           - the message priority
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
typedef int (*MTHREAD_MQ_FN_T)(char* msg, unsigned int size, unsigned int pri);

/*******************************************************************************
* MTHREAD_SK_FN_T
*
* DESCRIPTION:      Proto type of socket message handler
*
* INPUTS:
* msg           - buffer pointer of message
* size          - size of received message, in bytes
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
typedef int (*MTHREAD_SK_FN_T)(char* msg, unsigned int size);

/*******************************************************************************
* MTHREAD_TIMER_FN_T
*
* DESCRIPTION:      Proto type of timer handler
*
* INPUTS:
* timer_id           - timer ID
* arg                - user defined pointer, come from arg of mthread_register_timer
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
typedef int (*MTHREAD_TIMER_FN_T)(int timer_id, void* arg);

/*******************************************************************************
* mthread_register_mq
*
* DESCRIPTION:      Register a message queue message handler
*
* INPUTS:
* fd           - the message queue file descriptor
* fn           - the message queue message handler
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
int mthread_register_mq(int fd, MTHREAD_MQ_FN_T fn);

/*******************************************************************************
* mthread_register_sk
*
* DESCRIPTION:      Register a socket message handler
*
* INPUTS:
* fd           - the socket file descriptor
* fn           - the socket message handler
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
int mthread_register_sk(int fd, MTHREAD_SK_FN_T fn);

/*******************************************************************************
* mthread_register_timer
*
* DESCRIPTION:      Register a timer
*
* INPUTS:
* interval     - the expire time or interval
* repetitive   - 1: timer is repetitive, 0: timer is non-repetitive
* arg          - the user defined data pointer, it will be passed to fn when timer expired
* fn           - timer handler
* OUTPUTS:
*  None.
*
* RETURNS:
* timer id: if it >= 0; error: < 0
* 
*
* COMMENTS:
* If timer is non-repetitive, it will de-registered automatically when time out
*******************************************************************************/
int mthread_register_timer(int interval, int repetitive, void* arg, MTHREAD_TIMER_FN_T fn);

/*******************************************************************************
* mthread_reset_timer
*
* DESCRIPTION:      Reset a timer
*
* INPUTS:
* timer_id     - the timer ID
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
* 
*
* COMMENTS: 
*******************************************************************************/
int mthread_reset_timer(int timer_id);

/*******************************************************************************
* mthread_deregister_timer
*
* DESCRIPTION:      De-register a timer
*
* INPUTS:
* timer_id     - the timer ID
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
* 
*
* COMMENTS: 
* If timer is non-repetitive, it will de-registered automatically when time out
*******************************************************************************/
int mthread_deregister_timer(int timer_id);

/*******************************************************************************
* mthread_start
*
* DESCRIPTION:      Start the main thread loop
*
* INPUTS:
*  None.
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
* 
*
* COMMENTS: 
*******************************************************************************/
int mthread_start(void);

#ifdef __cplusplus
}
#endif
#endif
