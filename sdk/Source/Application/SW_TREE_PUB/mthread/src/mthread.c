/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <mqueue.h>
#include <sys/socket.h>

#include "mthread.h"

#define MTHREAD_MAX_NUM        (8)
#define MTHREAD_MAX_MQ_NUM     (8)
#define MTHREAD_MAX_SK_NUM     (4)
#define MTHREAD_MAX_TIMER_NUM  (256)
#define MTHREAD_MAX_MSG_SIZE   (4096)
#define MTHREAD_OS_ERROR       (-1)
#define MTHREAD_INVALID_INDEX  (-1)
#define MTHREAD_TRUE           (1)
#define MTHREAD_FALSE          (0)
#define MTHREAD_DEF_SLEEP_TIME (3000)
#define MTHREAD_MIN_TIMER      (100)

typedef struct
{
    int             valid;
    int             fd;
	MTHREAD_MQ_FN_T fn;
} MTHREAD_MQ_T;

typedef struct
{
    int             valid;
    int             fd;
	MTHREAD_SK_FN_T fn;
} MTHREAD_SK_T;

typedef struct
{
    int                valid;
    int                interval; /* in ms */
    void*              arg;
    int                repeatitive;
    int                next_expire_tm; /* in ms */
	MTHREAD_TIMER_FN_T fn;
} MTHREAD_TIMER_T;

typedef struct
{
    int             valid;
    pthread_t       thread_id;
    int             mq_num;
    int             sk_num;
    int             timer_num;
    int             delay; /* the timeout value for select, in ms */

    MTHREAD_MQ_T    mq[MTHREAD_MAX_MQ_NUM];
    MTHREAD_SK_T    sk[MTHREAD_MAX_SK_NUM];
    MTHREAD_TIMER_T timer[MTHREAD_MAX_TIMER_NUM];
} MTHREAD_INFO_T;


static MTHREAD_INFO_T gMthreadInfo[MTHREAD_MAX_NUM];
static int gMthreadSemId = MTHREAD_OS_ERROR;
static int gMthreadInited = MTHREAD_FALSE;

int mthread_create_sem(void)
{
	int semId;

	semId = semget(IPC_PRIVATE, 1, IPC_CREAT | S_IRUSR | S_IWUSR );
	if (semId == MTHREAD_OS_ERROR)
	{
		printf("mthread_create_sem: failed to create semphore(%s)!\n", strerror(errno));
		return MTHREAD_ERROR;
	}
	if (semctl(semId, 0, SETVAL, 1) == MTHREAD_OS_ERROR)        
	{        
		printf("mthread_create_sem: failed to init semphore(%s)!\n", strerror(errno));
		return MTHREAD_ERROR;
	}
	return semId;
}

int mthread_take_sem(int semId)
{
	struct sembuf sem_op; 

	sem_op.sem_num = 0;     
    sem_op.sem_op  = -1;     
    sem_op.sem_flg = 0; 
    if (MTHREAD_OS_ERROR == semop(semId, &sem_op, 1))         
    {          
    	printf("mthread_take_sem: failed to take semphore(%s)!\n", strerror(errno));
    	return MTHREAD_ERROR;      
    }
    return MTHREAD_OK;
}

int mthread_give_sem(int semId)
{
	struct sembuf sem_op; 

    sem_op.sem_num = 0;     
    sem_op.sem_op  = 1;     
    sem_op.sem_flg = 0; 
    if (MTHREAD_OS_ERROR == semop(semId, &sem_op, 1))         
    {          
    	printf("mthread_give_sem: failed to give semphore(%s)!\n", strerror(errno));
    	return MTHREAD_ERROR;      
    }

    return MTHREAD_OK;
}

int mthread_pre_init(void)
{
	gMthreadSemId = mthread_create_sem();
	if (MTHREAD_ERROR == gMthreadSemId)
	{
		return MTHREAD_ERROR;
	}

	memset(gMthreadInfo, 0, sizeof(MTHREAD_INFO_T)* MTHREAD_MAX_NUM);
	return MTHREAD_OK;
}

int mthread_find_invalid_thread_index(void)
{
    int i     = 0;

	if (MTHREAD_FALSE == gMthreadInited)
	{
		mthread_pre_init();
		gMthreadInited = MTHREAD_TRUE;
	}


	/* Take semphore */
    mthread_take_sem(gMthreadSemId);

    for(i = 0; i < MTHREAD_MAX_NUM; i++)
    {
        if(MTHREAD_FALSE == gMthreadInfo[i].valid)
        {
            break;
        }
    }

	/* Give semphore */
	mthread_give_sem(gMthreadSemId);

    if (i == MTHREAD_MAX_NUM)
    {
    	return MTHREAD_INVALID_INDEX;
    }
    else
    {
    	return i;
    }
}

int mthread_find_invalid_mq_index(MTHREAD_INFO_T* pThread)
{
    int i = 0;

    if (!pThread)
    {
    	return MTHREAD_INVALID_INDEX;
    }

    for(i = 0; i < MTHREAD_MAX_MQ_NUM; i++)
    {
        if(MTHREAD_FALSE == pThread->mq[i].valid)
        {
            return i;
        }
    }
    return MTHREAD_INVALID_INDEX;
}

int mthread_find_invalid_sk_index(MTHREAD_INFO_T* pThread)
{
    int i = 0;

    if (!pThread)
    {
    	return MTHREAD_INVALID_INDEX;
    }

    for(i = 0; i < MTHREAD_MAX_SK_NUM; i++)
    {
        if(MTHREAD_FALSE == pThread->sk[i].valid)
        {
            return i;
        }
    }
    return MTHREAD_INVALID_INDEX;
}

int mthread_find_invalid_timer_index(MTHREAD_INFO_T* pThread)
{
    int i = 0;

    if (!pThread)
    {
    	return MTHREAD_INVALID_INDEX;
    }

    for(i = 0; i < MTHREAD_MAX_TIMER_NUM; i++)
    {
        if(MTHREAD_FALSE == pThread->timer[i].valid)
        {
            return i;
        }
    }
    return MTHREAD_INVALID_INDEX;
}

int mthread_find_thread(void)
{
    int i = 0;
    pthread_t thread_id = pthread_self();

    for(i = 0; i < MTHREAD_MAX_NUM; i++)
    {
        if((MTHREAD_TRUE == gMthreadInfo[i].valid) && (thread_id == gMthreadInfo[i].thread_id))
        {
            return i;
        }
    }

    return MTHREAD_INVALID_INDEX;
}

int mthread_find_mq(int fd, MTHREAD_INFO_T* pThread)
{
    int i = 0;

    if (!pThread)
    {
    	return MTHREAD_INVALID_INDEX;
    }

    for(i = 0; i < MTHREAD_MAX_MQ_NUM; i++)
    {
        if((MTHREAD_TRUE == pThread->mq[i].valid) && (fd == pThread->mq[i].fd))
        {
            return i;
        }
    }
    return MTHREAD_INVALID_INDEX;
}

int mthread_find_sk(int fd, MTHREAD_INFO_T* pThread)
{
    int i = 0;

    if (!pThread)
    {
    	return MTHREAD_INVALID_INDEX;
    }

    for(i = 0; i < MTHREAD_MAX_SK_NUM; i++)
    {
        if((MTHREAD_TRUE == pThread->sk[i].valid) && (fd == pThread->sk[i].fd))
        {
            return i;
        }
    }
    return MTHREAD_INVALID_INDEX;
}

int mthread_get_or_init_thread(void)
{
	int index = mthread_find_thread();

	if (MTHREAD_INVALID_INDEX == index)
	{
		index = mthread_find_invalid_thread_index();
		if (MTHREAD_INVALID_INDEX == index)
		{
			printf("mthread: too many threads!\n");
			return MTHREAD_INVALID_INDEX;
		}

		/* Init */
		gMthreadInfo[index].valid     = MTHREAD_TRUE;
		gMthreadInfo[index].thread_id = pthread_self();
		gMthreadInfo[index].delay     = MTHREAD_DEF_SLEEP_TIME;
	}
	return index;
}

int mthread_register_mq(int fd, MTHREAD_MQ_FN_T fn)
{
	int thread_index;
	int mq_index;

	/* Init thread if not inited */
	thread_index = mthread_get_or_init_thread();
	if (MTHREAD_INVALID_INDEX == thread_index)
	{
		return MTHREAD_ERROR;
	}

	/* Check if it is already registered */
	mq_index = mthread_find_mq(fd, &gMthreadInfo[thread_index]);
	if (MTHREAD_INVALID_INDEX != mq_index)
	{
		printf("mthread_register_mq: fd already registered!\n");
		return MTHREAD_ERROR;
	}

	/* Register it */
	mq_index = mthread_find_invalid_mq_index(&gMthreadInfo[thread_index]);
	if (MTHREAD_INVALID_INDEX == mq_index)
	{
		printf("mthread_register_mq: too many message queues registered!\n");
		return MTHREAD_ERROR;
	}

	gMthreadInfo[thread_index].mq[mq_index].valid = MTHREAD_TRUE;
	gMthreadInfo[thread_index].mq[mq_index].fd    = fd;
	gMthreadInfo[thread_index].mq[mq_index].fn    = fn;
	gMthreadInfo[thread_index].mq_num++;

	return MTHREAD_OK;
}

int mthread_register_sk(int fd, MTHREAD_SK_FN_T fn)
{
	int thread_index;
	int sk_index;

	/* Init thread if not inited */
	thread_index = mthread_get_or_init_thread();
	if (MTHREAD_INVALID_INDEX == thread_index)
	{
		return MTHREAD_ERROR;
	}

	/* Check if it is already registered */
	sk_index = mthread_find_sk(fd, &gMthreadInfo[thread_index]);
	if (MTHREAD_INVALID_INDEX != sk_index)
	{
		printf("mthread_register_sk: fd already registered!\n");
		return MTHREAD_ERROR;
	}

	/* Register it */
	sk_index = mthread_find_invalid_sk_index(&gMthreadInfo[thread_index]);
	if (MTHREAD_INVALID_INDEX == sk_index)
	{
		printf("mthread_register_sk: too many sockets registered!\n");
		return MTHREAD_ERROR;
	}

	gMthreadInfo[thread_index].sk[sk_index].valid = MTHREAD_TRUE;
	gMthreadInfo[thread_index].sk[sk_index].fd    = fd;
	gMthreadInfo[thread_index].sk[sk_index].fn    = fn;
	gMthreadInfo[thread_index].sk_num++;

	return MTHREAD_OK;
}

int mthread_recalculate_delay(MTHREAD_INFO_T* pThread)
{
	int i;
	int temp = MTHREAD_DEF_SLEEP_TIME * 10;
	
	/* if no timer, delay 3 seconds */
	/* Search the min interval value */
	for(i = 0; i < MTHREAD_MAX_TIMER_NUM; i++)
    {
        if(MTHREAD_TRUE == pThread->timer[i].valid)
        {
        	if (pThread->timer[i].interval < temp)
        	{
        		temp = pThread->timer[i].interval;
        	}
        }
    }
    pThread->delay = temp/10;
    printf("new delay is %d\n", pThread->delay);

    return MTHREAD_OK;
}

int mthread_register_timer(int interval, int repetitive, void* arg, MTHREAD_TIMER_FN_T fn)
{
	int            thread_index;
	int            timer_index;
	struct timeval tv;

	if (interval < MTHREAD_MIN_TIMER)
	{
		printf("mthread_register_timer: timers less than %d are not supported\n", MTHREAD_MIN_TIMER);
		return MTHREAD_ERROR;
	}

	/* Init thread if not inited */
	thread_index = mthread_get_or_init_thread();
	if (MTHREAD_INVALID_INDEX == thread_index)
	{
		return MTHREAD_ERROR;
	}


	/* Register it */
	timer_index = mthread_find_invalid_timer_index(&gMthreadInfo[thread_index]);
	if (MTHREAD_INVALID_INDEX == timer_index)
	{
		printf("mthread_register_timer: too many timers registered!\n");
		return MTHREAD_ERROR;
	}

	gMthreadInfo[thread_index].timer[timer_index].valid       = MTHREAD_TRUE;
	gMthreadInfo[thread_index].timer[timer_index].interval    = interval;
	gMthreadInfo[thread_index].timer[timer_index].repeatitive = repetitive;
	gMthreadInfo[thread_index].timer[timer_index].arg         = arg;
	gMthreadInfo[thread_index].timer[timer_index].fn          = fn;
	gMthreadInfo[thread_index].timer_num++;
	gettimeofday(&tv, NULL);
	
	/* the next expire time is current time plus interval */
	gMthreadInfo[thread_index].timer[timer_index].next_expire_tm = tv.tv_sec * 1000 + tv.tv_usec / 1000 + interval;

	mthread_recalculate_delay(&gMthreadInfo[thread_index]);
	return timer_index;
}

int mthread_reset_timer(int timer_id)
{
	int            thread_index;
	struct timeval tv;

	if ((timer_id < 0) || (timer_id >= MTHREAD_MAX_TIMER_NUM))
	{
		printf("mthread_reset_timer: invalid timer id %d!\n", timer_id);
		return MTHREAD_ERROR;
	}

	thread_index = mthread_find_thread();
	if (MTHREAD_INVALID_INDEX == thread_index)
	{
		printf("mthread_reset_timer: thread not inited\n");
		return MTHREAD_ERROR;
	}

	if (MTHREAD_FALSE == gMthreadInfo[thread_index].timer[timer_id].valid)
	{
		printf("mthread_reset_timer: invalid timer id %d!\n", timer_id);
		return MTHREAD_ERROR;
	}

	gettimeofday(&tv, NULL);
	/* the next expire time is current time plus interval */
	gMthreadInfo[thread_index].timer[timer_id].next_expire_tm = tv.tv_sec * 1000 + tv.tv_usec / 1000 
	                                                            + gMthreadInfo[thread_index].timer[timer_id].interval;
	return MTHREAD_OK;
}

int mthread_deregister_timer(int timer_id)
{
	int thread_index;


	if ((timer_id < 0) || (timer_id >= MTHREAD_MAX_TIMER_NUM))
	{
		printf("mthread_reset_timer: invalid timer id %d!\n", timer_id);
		return MTHREAD_ERROR;
	}

	thread_index = mthread_find_thread();
	if (MTHREAD_INVALID_INDEX == thread_index)
	{
		printf("mthread_reset_timer: thread not inited\n");
		return MTHREAD_ERROR;
	}


	if (MTHREAD_FALSE == gMthreadInfo[thread_index].timer[timer_id].valid)
	{
		printf("mthread_reset_timer: invalid timer id %d!\n", timer_id);
		return MTHREAD_ERROR;
	}

	gMthreadInfo[thread_index].timer[timer_id].valid = MTHREAD_FALSE;
	gMthreadInfo[thread_index].timer_num--;

	mthread_recalculate_delay(&gMthreadInfo[thread_index]);
	return MTHREAD_OK;
}

void mthread_check_mq_msg(MTHREAD_INFO_T* pThread, fd_set* fset, char* buf)
{
	int i;
	int size, pri;
	
	for(i = 0; i < MTHREAD_MAX_MQ_NUM; i++)
    {
        if(MTHREAD_TRUE == pThread->mq[i].valid)
        {
        	/* Something received on this fd */
            if (FD_ISSET(pThread->mq[i].fd, fset))
            {
            	size = mq_receive(pThread->mq[i].fd, buf, MTHREAD_MAX_MSG_SIZE, &pri);
            	if (size > 0)
            	{
            		/* Invoke message handler */
            		(*pThread->mq[i].fn)(buf, size, pri);
            	}
            }
        }
    }
}

void mthread_check_sk_msg(MTHREAD_INFO_T* pThread, fd_set* fset, char* buf)
{
	int i;
	int size, pri;
	
	for(i = 0; i < MTHREAD_MAX_SK_NUM; i++)
    {
        if(MTHREAD_TRUE == pThread->sk[i].valid)
        {
        	/* Something received on this fd */
            if (FD_ISSET(pThread->sk[i].fd, fset))
            {
            	size = recv(pThread->sk[i].fd, buf, MTHREAD_MAX_MSG_SIZE, MSG_DONTWAIT);
            	if (size > 0)
            	{
            		/* Invoke message handler */
            		(*pThread->sk[i].fn)(buf, size);
            	}
            }
        }
    }
}

void mthread_check_expired_timers(MTHREAD_INFO_T* pThread)
{
	struct timeval tv;
	int ms, i;
	int needToRecalculate = MTHREAD_FALSE;
	
 	for(i = 0; i < MTHREAD_MAX_TIMER_NUM; i++)
    {
        if(MTHREAD_TRUE == pThread->timer[i].valid)
        {
        	/* In case the total valid num of timer is big, it is better */
        	/* to put the  gettimeofday inside the iteration */
			gettimeofday(&tv, NULL); 
			ms = tv.tv_sec * 1000 + tv.tv_usec / 1000;

        	/* expired */
            if (ms >= pThread->timer[i].next_expire_tm)
            {
            	/* Remove timer if it is non-repeatitive 
                   It must be before the timer handler, because
                   user may register new timer in timer handler */
            	if (!pThread->timer[i].repeatitive)
            	{
            		pThread->timer[i].valid = MTHREAD_FALSE;
			pThread->timer_num--;

					/* if the cancelled timer is the smallest one, then need to recalculate the delay */
					if (pThread->timer[i].interval <= 10 * pThread->delay)
					{
						needToRecalculate = MTHREAD_TRUE;
					}
            	}
            	else
            	{
            		// Update next expire time
            	    pThread->timer[i].next_expire_tm += pThread->timer[i].interval;
            	}
            	(*pThread->timer[i].fn)(i, pThread->timer[i].arg);
            }
        }
    }

	if (needToRecalculate)
	{
		mthread_recalculate_delay(pThread);
	}
}

int mthread_start(void)
{
	int            thread_index;
	fd_set         fset, temp_fset;
	struct timeval tv;
	int            max_fds = 0;
    char           msg_buf[MTHREAD_MAX_MSG_SIZE];
    int            i;

	/* Init thread if not inited */
	thread_index = mthread_get_or_init_thread();
	if (MTHREAD_INVALID_INDEX == thread_index)
	{
		return MTHREAD_ERROR;
	}

	/* Init fd set */
	FD_ZERO(&fset);
	if (gMthreadInfo[thread_index].mq_num > 0)
	{
		for(i = 0; i < MTHREAD_MAX_MQ_NUM; i++)
	    {
	        if(MTHREAD_TRUE == gMthreadInfo[thread_index].mq[i].valid)
	        {
	            FD_SET(gMthreadInfo[thread_index].mq[i].fd, &fset);
	            if (gMthreadInfo[thread_index].mq[i].fd > max_fds)
	            {
	            	max_fds = gMthreadInfo[thread_index].mq[i].fd;
	            }
	        }
	    }
	}
	if (gMthreadInfo[thread_index].sk_num > 0)
	{
		for(i = 0; i < MTHREAD_MAX_SK_NUM; i++)
	    {
	        if(MTHREAD_TRUE == gMthreadInfo[thread_index].sk[i].valid)
	        {
	            FD_SET(gMthreadInfo[thread_index].sk[i].fd, &fset);
	            if (gMthreadInfo[thread_index].sk[i].fd > max_fds)
	            {
	            	max_fds = gMthreadInfo[thread_index].sk[i].fd;
	            }
	        }
	    }
	}
	
	
	while (1)
	{
		/* no socket and no message queue */
		if ((0 == gMthreadInfo[thread_index].mq_num) && (0 == gMthreadInfo[thread_index].sk_num))
		{
			usleep(gMthreadInfo[thread_index].delay * 1000);
		}
		else
		{
			tv.tv_sec = gMthreadInfo[thread_index].delay / 1000;
			tv.tv_usec = (gMthreadInfo[thread_index].delay % 1000) * 1000;

			/* Re-configure the fd set */
			FD_ZERO(&temp_fset);
			temp_fset = fset;
			if (select(max_fds + 1, &temp_fset, NULL, NULL, &tv) > 0)
			{
				/* Check message queues */
				if (gMthreadInfo[thread_index].mq_num > 0)
				{
					mthread_check_mq_msg(&gMthreadInfo[thread_index], &temp_fset, msg_buf);
				}
				/* Check sockets */
				if (gMthreadInfo[thread_index].sk_num > 0)
				{
					mthread_check_sk_msg(&gMthreadInfo[thread_index], &temp_fset, msg_buf);
				}
			}
		}
		mthread_check_expired_timers(&gMthreadInfo[thread_index]);
	}
}

void mthread_show(void)
{
	int thread_index = mthread_find_thread();
	int i;

	if (MTHREAD_INVALID_INDEX == thread_index)
	{
		return;
	}
	
	printf("\n--------\n");
	printf("My Info\n");
	printf("--------\n");
	printf("PID=%d, threadId=%d, semID=%d, NumOfMq=%d, NumOfSk=%d, NumOfTimer=%d, Delay=%d\n", 
	        getpid(), pthread_self(), gMthreadSemId, gMthreadInfo[thread_index].mq_num, 
	        gMthreadInfo[thread_index].sk_num, gMthreadInfo[thread_index].timer_num, 
	        gMthreadInfo[thread_index].delay);  

	if (gMthreadInfo[thread_index].mq_num > 0)
	{
		printf("------------------\n");
		printf("| MqId | Fn       |\n");
		printf("------------------\n");

	    for(i = 0; i < MTHREAD_MAX_MQ_NUM; i++)
	    {
	        if(MTHREAD_TRUE == gMthreadInfo[thread_index].mq[i].valid)
	        {
	            printf("| %-4d | %-8p |\n", gMthreadInfo[thread_index].mq[i].fd, gMthreadInfo[thread_index].mq[i].fn);
				printf("------------------\n");
	        }
	    }
    }

    if (gMthreadInfo[thread_index].sk_num > 0)
	{
		printf("------------------\n");
		printf("| SkId | Fn       |\n");
		printf("------------------\n");

	    for(i = 0; i < MTHREAD_MAX_SK_NUM; i++)
	    {
	        if(MTHREAD_TRUE == gMthreadInfo[thread_index].sk[i].valid)
	        {
	            printf("| %-4d | %-8p |\n", gMthreadInfo[thread_index].sk[i].fd, gMthreadInfo[thread_index].sk[i].fn);
				printf("------------------\n");
	        }
	    }
    }

    if (gMthreadInfo[thread_index].timer_num> 0)
	{
		printf("----------------------------------------------------------------\n");
		printf("| Interval | Arg         | Repeat | NextExpireTm | Fn          |\n");
		printf("----------------------------------------------------------------\n");

	    for(i = 0; i < MTHREAD_MAX_TIMER_NUM; i++)
	    {
	        if(MTHREAD_TRUE == gMthreadInfo[thread_index].timer[i].valid)
	        {
	            printf("| %-8d | %-8p | %-6d | %-12d | %-8p |\n", gMthreadInfo[thread_index].timer[i].interval, 
		            gMthreadInfo[thread_index].timer[i].arg, gMthreadInfo[thread_index].timer[i].repeatitive, 
		            gMthreadInfo[thread_index].timer[i].next_expire_tm, gMthreadInfo[thread_index].timer[i].fn);
				printf("----------------------------------------------------------------\n");
	        }
	    }
    }
}

