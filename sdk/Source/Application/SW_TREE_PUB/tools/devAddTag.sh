#!/bin/bash
#
#  Adding GIT tag
#

print_help()
{
    echo ""
    echo " Adding GIT tag"
    echo ""
    echo " ./devAddTag.sh -h | <rc_number>"
    echo ""
    echo "  <rc_number>         - Release candidate number"
    echo ""
    echo " Example:"
    echo ""
    echo "   cd /nfs/pt/swdev/areas/projects/gr_gpon/$USER/<git_dir>/xpon_sdk/Application/SW_TREE"
    echo "   ./devAddTag.sh -h"
    echo "   ./devAddTag.sh RC1"
    echo ""
}

### NOTE: developer can use GERRIT_SSH variable to define another GERRIT location.
###       The following definition can be added to the current environment from the
###       command line:
###        export GERRIT_SSH=<private path>
if [ "$GERRIT_SSH" == "" ];then
    export GERRIT_SSH=ssh://vgitil03:29418
fi

if [ "$1" = "-h" ]
then
    print_help
    exit 1
fi

if [ $# -ne 1 ]
then
    echo "====> Invalid number of parameters "
    print_help
    exit 1
fi

SDK_SCRIPTS_DIR=`pwd`

echo "====> git tag -a -m 'SW tree application - $1' SW_TREE-$1"
git tag -a -m 'SW tree application - $1' SW_TREE-$1
if [ $? -ne 0 ]
then
    echo "====> Failed to set tag"
    exit 1
fi

echo "====> git push $GERRIT_SSH/xpon/sdk/sw_tree tag SW_TREE-$1"
git push $GERRIT_SSH/xpon/sdk/sw_tree tag SW_TREE-$1
if [ $? -ne 0 ]
then
    echo "====> Failed to push tag"
    exit 1
fi

echo "=================================================="
