#!/bin/bash
#
#  Updating sw_tree version
#

print_help()
{
    echo ""
    echo " Updating sw_tree version"
    echo ""
    echo " ./devUpdateVer.sh -h | <rc_number>"
    echo ""
    echo "  <rc_number>         - Release candidate number"
    echo ""
    echo " Example:"
    echo ""
    echo "   cd /nfs/pt/swdev/areas/projects/gr_gpon/$USER/<git_sw_tree_dir>/tools"
    echo "   ./devUpdateVer.sh -h"
    echo "   ./devUpdateVer.sh RC1"
    echo ""
}

### NOTE: developer can use GERRIT_SSH variable to define another GERRIT location.
###       The following definition can be added to the current environment from the
###       command line:
###        export GERRIT_SSH=<private path>
if [ "$GERRIT_SSH" == "" ];then
    export GERRIT_SSH=ssh://vgitil03:29418
fi

TOOLS_DIR=`pwd`
VERSION_FILE=$TOOLS_DIR/../main/incl/version.h
BRANCH_NAME=`git symbolic-ref HEAD | cut -d"/" -f3`

if [ "$1" = "-h" ]
then
    print_help
    exit 1
fi

if [ $# -ne 1 ]
then
    echo "====> Invalid number of parameters "
    print_help
    exit 1
fi

echo "=================================================="
echo "====> Updating $VERSION_FILE"
chmod +w $VERSION_FILE

echo "/* Automatically generated file */

#ifndef _US_VERSION_H_
#define _US_VERSION_H_

#define SDK_VERSION \"$1\"
#define SDK_VERSION_DATE \"`date +"%b %d %Y %T"`\"

#endif
" > $VERSION_FILE

echo "====> cd $TOOLS_DIR/.."
cd $TOOLS_DIR/..
echo "====> Pushing changes to Gerrit"
echo "====> git commit -a -s -m \"Update version number to $1\""
git commit -a -s -m "Update version number to $1"
if [ $? -ne 0 ]
then
    echo "====> Failed to commit"
    exit 1
fi

echo "====> git push $GERRIT_SSH/xpon/sdk/sw_tree HEAD:refs/for/$BRANCH_NAME"
git push $GERRIT_SSH/xpon/sdk/sw_tree HEAD:refs/for/$BRANCH_NAME
if [ $? -ne 0 ]
then
    echo "====> Failed to push commit"
    exit 1
fi

echo "=================================================="
