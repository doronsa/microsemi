/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : oam_hook_eth.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM hook layer Ethernet functions           **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    12/Jan/2011   - initial version created.                                     
 *                                                                      
 ******************************************************************************/
 
#ifndef __OAM_HOOK_ETH_H__
#define __OAM_HOOK_ETH_H__

extern STATUS oam_mc_ctrl_proc_hook(OAM_MC_CTRL_T *para_cfg);
extern STATUS oam_mc_ctrl_get_hook(OAM_MC_CTRL_T *para_cfg);
extern UINT32 oam_mc_port_max_group_get(UINT32 slot,UINT32 port);
extern STATUS oam_mc_port_max_group_set(UINT32 slot,UINT32 port,UINT32 group_max_num);

MRVL_ERROR_CODE_T oam_hook_eth_add_mc_control(UINT32 stream_num, UINT32 new_mbr,  OAM_ETH_MC_CONTROL_T *mc_group_ctrl);
MRVL_ERROR_CODE_T oam_hook_eth_mod_mc_control(UINT32 stream_num, UINT32 diff_mbr, OAM_ETH_MC_CONTROL_T *mc_group_ctrl);
MRVL_ERROR_CODE_T oam_hook_eth_del_mc_control(UINT32 stream_num, UINT32 rem_mbr,  OAM_ETH_MC_CONTROL_T *mc_group_ctrl);

#endif /*__OAM_HOOK_ETH_H__*/