/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : oam_adapter_vendor.h                                      **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM adapter layer vendor                    **/
/**                related functions                                         **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    12/Jan/2011   - initial version created.                                 
 *                                                                      
 ******************************************************************************/
 
#ifndef __OAM_ADAPTER_VENDOR_H__
#define __OAM_ADAPTER_VENDOR_H__

/*ONU basic*/
#define OAM_VENDOR_88F6560_MODEL        0x6560
#define OAM_VENDOR_88F6560_SWVERSION    "mvl_sfu_0_3_1"
#define OAM_VENDOR_88F6560_HWVERSION    "M88F6065"
#define OAM_VENDOR_88F6560_BATTERYBAK   0
#define OAM_VENDOR_88F6560_IPV6_SUPPORT TRUE 

#define OAM_VENDOR_LOID_USER_NAME       "admin"
#define OAM_VENDOR_LOID_PASSWD          "admin"

/*PON information*/
#define OAM_VENDOR_88F6560_PON_FW                 0x00020019  
#define OAM_VENDOR_88F6560_PON_VENDOR             "MV"  
#define OAM_VENDOR_88F6560_PON_CHIP_MODEL         0x6560
#define OAM_VENDOR_88F6560_PON_CHIP_REVERSION     0X1
#define OAM_VENDOR_88F6560_PON_CHIP_BUILTTIME     0X0a081a
#define OAM_VENDOR_88F6560_PON_MULT_LLID_SUPPORT  0X8
#define OAM_VENDOR_88F6560_PON_IF_NUMBER          1

/*Capability*/
#define OAM_VENDOR_UPSTREAM_QUEUES_NUMBER         4 
#define OAM_VENDOR_MAX_UPSTREAM_QUEUES_PER_PORT   8 
#define OAM_VENDOR_DOWNSTREAM_QUEUES_NUMBER       4
#define OAM_VENDOR_MAX_DOWNSTREAM_QUEUES_PER_PORT 8

/*Image ID*/
#define OAM_VENDOR_IMAGE_MIN_ID    1
#define OAM_VENDOR_IMAGE_MAX_ID    2

/*LOID*/
#define OAM_AUTH_LOID_USER_LEN     24
#define OAM_AUTH_LOID_PASSWORD_LEN 12
typedef struct  
{
    UINT8  loid[OAM_AUTH_LOID_USER_LEN];
    UINT8  password[OAM_AUTH_LOID_PASSWORD_LEN];
}OAM_AUTH_LOID_T;

extern STATUS oam_adpt_get_wan_ip_vlan(UINT32 *ip_address,UINT32 *ip_mask,UINT32 *gate_way, UINT16 *vid ,UINT8 *pri );
extern STATUS oam_adpt_set_wan_ip_vlan(UINT32 ip_address,UINT32 ip_mask,UINT32 gate_way, UINT16 vid,UINT8 pri);

MRVL_ERROR_CODE_T oam_adpt_vendor_get_onu_loid(OAM_AUTH_LOID_T *auth);
MRVL_ERROR_CODE_T oam_adpt_vendor_reset_onu(VOID);
MRVL_ERROR_CODE_T oam_adpt_upgrade_firmware(UINT8 image_id, UINT8 file[64]);
MRVL_ERROR_CODE_T oam_adpt_get_upgrade_status(OAM_SW_UPDATE_STATE_E *state);
MRVL_ERROR_CODE_T oam_adpt_vendor_onu_mac_get(OAM_ETH_MAC_ADDR_T mac_address);
MRVL_ERROR_CODE_T oam_adpt_upgrade_activate_image (UINT8 image_number);
MRVL_ERROR_CODE_T mrvl_pon_get_active_image (UINT8 *image_number);
MRVL_ERROR_CODE_T oam_adpt_upgrade_commit_image (UINT8 image_number);
MRVL_ERROR_CODE_T oam_adpt_upgrade_get_image_status (UINT8 image_number, BOOL *isActive, BOOL *isCommited);

#endif /*__OAM_ADAPTER_VENDOR_H__*/
