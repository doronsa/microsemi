/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : oam_adapter_voip_xml.h                                    **/
/**                                                                          **/
/**  DESCRIPTION : Definition of adapter layer VoIP XML related functions    **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    12/Jan/2011   - initial version created.                                
 *                                                                      
 ******************************************************************************/
 
#ifndef __OAM_ADAPTER_VOIP_XML_H__
#define __OAM_ADAPTER_VOIP_XML_H__

struct voip_xml_node
{
    char *tagname;
    char tagvalue[64];    
    struct voip_xml_node *child;    
    INT32 depth;
};

typedef struct voip_xml_node *OAM_VOIP_XML_NODE_PT;

#ifdef DEBUG
#define VOIP_DBG(format, ...)  printf("%s(%d)" format "\n",__FUNCTION__,__LINE__, ##__VA_ARGS__)
#else
#define VOIP_DBG(format, ...)
#endif

#define BLANK(n) {INT32 i=n*4; for(i--;i>=0;i--) sprintf(xmlBuffer, "%s%c", xmlBuffer, 32);}
#define BUFSZ 64

#endif /*__OAM_ADAPTER_VOIP_XML_H__*/