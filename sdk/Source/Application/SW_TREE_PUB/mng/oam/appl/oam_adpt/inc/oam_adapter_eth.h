/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : oam_adapter_eth.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : Definition of adapter layer Ethernet related functions    **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    12/Jan/2011   - initial version created.                              
 *                                                                      
 ******************************************************************************/
 
#ifndef __OAM_ADAPTER_ETH_H__
#define __OAM_ADAPTER_ETH_H__

/*UNI GE port index*/
#define MRVL_ETH_FE_BM 0x07
#define MRVL_ETH_GE_BM 0x08

#define MRVL_ETH_DUMMY_ACTION       0
#define MRVL_ETH_DUMMY_PARSE_FLAG   0
#define MRVL_ETH_DUMMY_IPV4_FLAG    0

#define MRVL_ETH_TRANSPARENT_PRI    11
#define MRVL_ETH_VLAN_PRI           10
#define MRVL_ETH_DROP_NULL_PRI      9

#define MRVL_ETH_CNM_PRI            2
#define MRVL_ETH_IGMP_PRI           1
#define MRVL_ETH_MC_SPAN_VLAN       0

#define MRVL_ETH_MC_TABLE_LEN       64

/*
    MARVELl VLAN attribute definition
*/
#define MRVL_ETH_DEFAULT_TPID   0x8100
#define MRVL_ETH_DEFAULT_CFI    0
#define MRVL_ETH_DEFAULT_PRI    0

#define MRVL_OAM_LOOPBACK_PRI   0

#define MRVL_ETH_IP_TYPE        0x0800

/* MARVELl ethernet function macro defines 
   Enumerations and Structures
*/
typedef enum
{
    MRVL_ETH_PORT_INACTIVE,
    MRVL_ETH_PORT_ACTIVE
}MRVL_ETH_PORT_LINK_STATE_E;

typedef enum
{
    MRVL_ETH_NEGA_STATE_INACTIVE = 0x01,
    MRVL_ETH_NEGA_STATE_ACTIVE   = 0x02
}MRVL_ETH_PORT_NEGA_STATE_E;

typedef enum
{
    MRVL_ETH_NEG_UNKNOWN         = 0x2,
    MRVL_ETH_NEG_10BASET         = 14,
    MRVL_ETH_NEG_10BASET_FULL    = 142,
    MRVL_ETH_NEG_100BASET4       = 23,
    MRVL_ETH_NEG_100BASETX       = 25,
    MRVL_ETH_NEG_100BASETX_FULL  = 252,
    MRVL_ETH_NEG_100BASET2       = 32,
    MRVL_ETH_NEG_100BASET2_FULL  = 322,
    MRVL_ETH_NEG_1000BASET       = 36,
    MRVL_ETH_NEG_1000BASET_FULL  = 362,
    MRVL_ETH_NEG_1000BASEX       = 40,
    MRVL_ETH_NEG_1000BASEX_FULL  = 402
}MRVL_ETH_PORT_NEG_ABILITY_E;

#define _LITTLE_ENDIAN
#undef  _BIG_ENDIAN
#ifdef _LITTLE_ENDIAN
typedef struct
{
    UINT16 vlan;
    UINT16 ether_type;    
}POS_PACKED OAM_VLAN_TAG_T;
#elif _BIG_ENDIAN
typedef struct
{
    UINT16 vlan;
    UINT16 ether_type;    
}POS_PACKED OAM_VLAN_TAG_T;
#endif

typedef struct
{
    OAM_VLAN_TAG_T old_vlan;
    OAM_VLAN_TAG_T new_vlan;
}POS_PACKED OAM_ETH_VLAN_TRANSLATION_ENTRY_T;

#define OAM_ETH_MAX_VLAN_TRANSLATION_PER_PORT 16
typedef struct
{
    OAM_ETH_VLAN_TRANSLATION_ENTRY_T
    entries[OAM_ETH_MAX_VLAN_TRANSLATION_PER_PORT];
    UINT8 number_of_entries;
}POS_PACKED OAM_ETH_VLAN_TRANSLATION_T;

#define OAM_ETH_MAX_VLAN_AGGRE_PER_PORT 8
typedef struct
{
    UINT8 num_of_entries;
    OAM_VLAN_TAG_T new_tag;    
    OAM_VLAN_TAG_T old_tag[OAM_ETH_MAX_VLAN_AGGRE_PER_PORT];
}POS_PACKED OAM_ETH_VLAN_AGGREGATION_T;

#define OAM_ETH_MAX_TRUNK_VLAN_PER_PORT 32
typedef struct
{
    UINT8 num_of_entries;
    OAM_VLAN_TAG_T tag[OAM_ETH_MAX_TRUNK_VLAN_PER_PORT];
}POS_PACKED OAM_ETH_VLAN_TRUNK_T;

#define OAM_ETH_MAX_RULES_PER_FLOW 4
typedef enum
{
    OAM_ETH_FIELD_DMAC        = 0x0,
    OAM_ETH_FIELD_SMAC        = 0x1,
    OAM_ETH_FIELD_PBIT        = 0x2,
    OAM_ETH_FIELD_VID         = 0x3,
    OAM_ETH_FIELD_ETHTYPE     = 0x4,
    OAM_ETH_FIELD_IPv4_DIP     = 0x5,
    OAM_ETH_FIELD_IPv4_SIP     = 0x6,
    OAM_ETH_FIELD_IP_PROTOCOL  = 0x7,
    OAM_ETH_FIELD_IPv4_DSCP    = 0x8,
    OAM_ETH_FIELD_IPv6_DSCP    = 0x9,
    OAM_ETH_FIELD_SPORT        = 0xa,
    OAM_ETH_FIELD_DPORT        = 0xb,
    OAM_ETH_FIELD_IP_VERSION   = 0xc,
    OAM_ETH_FIELD_IPv6_LABEL   = 0xd,
    OAM_ETH_FIELD_IPv6_DIP     = 0xe,
    OAM_ETH_FIELD_IPv6_SIP     = 0xf,
    OAM_ETH_FIELD_IPv6_DPREFIX = 0x10,
    OAM_ETH_FIELD_IPv6_SPREFIX = 0x11,
    OAM_ETH_FIELD_IPv6_NH      = 0x12
}OAM_ETH_FIELD_E;

typedef enum
{
    OAM_ETH_OPERATOR_NMACTCH = 0,
    OAM_ETH_OPERATOR_EQUAL   = 1,
    OAM_ETH_OPERATOR_NEQUAL  = 2,
    OAM_ETH_OPERATOR_LESS    = 3,
    OAM_ETH_OPERATOR_GREATER = 4,
    OAM_ETH_OPERATOR_EXIST   = 5,
    OAM_ETH_OPERATOR_NEXIST  = 6,
    OAM_ETH_OPERATOR_MATCH   = 7
}OAM_ETH_OPERATOR_E;

typedef struct
{
    UINT32      match_value;
    INT8        mac_address[OAM_MAC_ADDR_LEN];
    UINT8       ipv6_value[OAM_IPv6_ADDR_LEN ];
}POS_PACKED OAM_ETH_MATCH_T;

typedef struct
{
    OAM_ETH_FIELD_E field;
    OAM_ETH_MATCH_T value;
    OAM_ETH_OPERATOR_E operator;
}POS_PACKED OAM_ETH_RULE_ENTRY_T;

typedef struct
{
    OAM_ETH_RULE_ENTRY_T entries[OAM_ETH_MAX_RULES_PER_FLOW];
    UINT8 num_of_entries;
}POS_PACKED OAM_ETH_RULE_T;

#define OAM_ETH_MAX_STATIC_MAC_PER_PORT 32
typedef struct
{
    UINT8 mac[OAM_ETH_MAX_STATIC_MAC_PER_PORT][6];
    UINT32 num_of_mac;
}POS_PACKED OAM_ETH_MAC_LIST_T;

/* 
   Multicast Enumerations and Structures 
*/

#define OAM_ETH_MAX_MC_VLAN_PER_PORT 32
typedef struct
{
    UINT16 vlan_id[OAM_ETH_MAX_MC_VLAN_PER_PORT];
    UINT32 num_of_vlan_id;
}OAM_ETH_MC_VLAN_T;

typedef enum
{
    OAM_ETH_MC_VLAN_ASIS = 0x00,
    OAM_ETH_MC_VLAN_STRIP,
    OAM_ETH_MC_VLAN_TRANSLATION
}OAM_ETH_MC_VLAN_MODE_E;

typedef struct
{
    BOOL   im_actv;
    UINT16 old_vlan;
    UINT16 new_vlan;
}OAM_ETH_MC_VLAN_TRANSLATION_ENTRY_T;

#define OAM_ETH_MAX_MC_VLAN_TRANSLATION_PER_PORT 32
typedef struct
{
    OAM_ETH_MC_VLAN_TRANSLATION_ENTRY_T
    entries[OAM_ETH_MAX_MC_VLAN_TRANSLATION_PER_PORT];
    UINT32 number_of_entries;
}OAM_ETH_MC_VLAN_TRANSLATION_T;

typedef union
{
    UINT32 ip_v4;
    UINT8  ip_v6[16];
}OAM_IP_T;

typedef struct
{
    UINT8    dmac[6];
    UINT16   vid;
    OAM_IP_T dip;
}OAM_ETH_MC_CONTROL_ENTRY_T;

typedef enum
{
    OAM_ETH_MULTICAST_CONTROL_TYPE_DMAC_ONLY = 0x00,
    OAM_ETH_MULTICAST_CONTROL_TYPE_DMAC_VID ,
    OAM_ETH_MULTICAST_CONTROL_TYPE_DMAC_SMAC,
    OAM_ETH_MULTICAST_CONTROL_TYPE_DIPV4_VID,
    OAM_ETH_MULTICAST_CONTROL_TYPE_DIPV6_VID,
}OAM_ETH_MC_CONTROL_TYPE_E;

typedef struct
{
    UINT32                     port_bm;
    OAM_ETH_MC_CONTROL_TYPE_E  control_type;
    OAM_ETH_MC_CONTROL_ENTRY_T entry;
}OAM_ETH_MC_CONTROL_T;

typedef struct
{
    BOOL igmp_snooping_non_fast_leave;
    BOOL igmp_snooping_fast_leave;
    BOOL igmp_ctc_non_fast_leave;
    BOOL igmp_ctc_fast_leave;
    BOOL mld_non_fast_leave;
    BOOL mld_fast_leave;
}OAM_ETH_MC_FAST_LEAVE_ABILITY_T;

typedef enum
{
    OAM_ETH_IGMP_CTRL_LEAVE_REPORT_MSG, /*igmp leave report  */
    OAM_ETH_IGMP_CTRL_JOIN_REPORT_MSG,  /*igmp jion report   */
    OAM_ETH_IGMP_CTRL_GENERAL_QUERY_MSG,/*igmp general query */
    OAM_ETH_IGMP_CTRL_SPECIAL_QUERY_MSG /*igmp special query */
}OAM_ETH_IGMP_REPORT_MSG_E;

typedef enum
{
    OAM_ETH_IGMP_CTRL_UPSTREAM_DATEPATH,
    OAM_ETH_IGMP_CTRL_DOWNSTREAM_DATEPATH
}OAM_ETH_IGMP_CTRL_DIRECTION_E;

typedef enum
{ 
    MRVL_ETH_FIRST,
    MRVL_ETH_MCVLAN,    
    MRVL_ETH_IGMP,
    MRVL_ETH_CNM,
    MRVL_ETH_VLAN,    
    MRVL_ETH_TRANSPARENT
}MRVL_ETH_OBJ_E;

typedef enum
{
    MRVL_ETH_RULE_L2    = TPM_API_L2_PRIM,
    MRVL_ETH_RULE_L3,
    MRVL_ETH_RULE_IP4,
    MRVL_ETH_RULE_IP6,
    MRVL_ETH_RULE_MC_IP4,
    MRVL_ETH_RULE_MC_IP6,
    MRVL_ETH_RULE_END
}MRVL_ETH_RULE_TYPE_E;

typedef enum
{
    MRVL_ETH_RULE_DIR_DS = 0,
    MRVL_ETH_RULE_DIR_US,
    MRVL_ETH_RULE_DIR_END,
}MRVL_ETH_RULE_DIR_E;

typedef struct
{
    UINT32 L2_tag_default_rule_idx;
    UINT32 L2_untag_default_rule_idx;
    UINT32 IPv4_default_rule_idx;
}MRVL_ETH_IPV4_ADDITIONAL_RULE_INDEX_T;

typedef struct
{
    UINT32              port;
    MRVL_ETH_OBJ_E      obj;
    tpm_api_type_t      type;
    MRVL_ETH_RULE_DIR_E direction;
    UINT32              priority;
    UINT32              rule_idx;
    tpm_rule_entry_t    entry;
    MRVL_ETH_IPV4_ADDITIONAL_RULE_INDEX_T  ipv4_add_rule_index;
}MRVL_ETH_RULE_T;

typedef struct 
{
    LIST_HEAD_T      list;
    MRVL_ETH_RULE_T  rule_list;
}MRVL_ETH_RULE_LIST_T;

typedef enum
{
    MRVL_ETH_CLEAR,
    MRVL_ETH_DEL    
}MRVL_ETH_L2_DEL_TYPE_E;

#define MRVL_ETH_MAX_MC_VLAN_NUM_PER_PORT        (8)
#define MRVL_ETH_MAX_VLAN_SWITCH_NUM_PER_PORT    (8)

typedef struct
{
    OAM_VLAN_TAG_T mvlan;
    UINT32         count;
}OAM_ETH_MC_VLAN_SET_T[MRVL_ETH_MAX_MC_VLAN_NUM_PER_PORT];

typedef struct
{
    BOOL           valid;
    OAM_VLAN_TAG_T mvlan;
    OAM_VLAN_TAG_T uvlan;
}OAM_ETH_MC_VLAN_SWITCH_T[MRVL_ETH_MAX_VLAN_SWITCH_NUM_PER_PORT];


MRVL_ERROR_CODE_T oam_adpt_eth_get_port_link_state(UINT32 slot, UINT32 port,
        MRVL_ETH_PORT_LINK_STATE_E *link_state);

MRVL_ERROR_CODE_T oam_adpt_eth_get_port_neg_state(UINT32 slot, UINT32 port,
        MRVL_ETH_PORT_NEGA_STATE_E *neg_state);

MRVL_ERROR_CODE_T oam_adpt_eth_set_port_neg_state(UINT32 slot, UINT32 port,
        MRVL_ETH_PORT_NEGA_STATE_E *neg_state);

MRVL_ERROR_CODE_T oam_adpt_eth_enable_ingress_flow_control(UINT32 slot, UINT32 port,
        BOOL enable);
        
MRVL_ERROR_CODE_T oam_adpt_eth_get_ingress_flow_control(UINT32 slot, UINT32 port,
        BOOL *enable);
        
MRVL_ERROR_CODE_T oam_adpt_eth_enable_ingress_policing (UINT32 slot, UINT32 port, 
        BOOL enable, UINT32 cir, UINT32 cbs, UINT32 ebs);

MRVL_ERROR_CODE_T oam_adpt_eth_enable_egress_rate_limiting(UINT32 slot, UINT32 port,
        BOOL enable, UINT32 cir, UINT32 pir);

MRVL_ERROR_CODE_T oam_adpt_eth_set_port_vlan_mode(UINT32 slot, UINT32 port, 
        OAM_PORT_VLAN_MODE_E *vlan_mode, OAM_ETH_PORT_VLAN_CONF_T *param);

MRVL_ERROR_CODE_T oam_adpt_eth_set_vlan_transparent(UINT32 slot, UINT32 port);

MRVL_ERROR_CODE_T oam_adpt_eth_set_port_default_vlan(UINT32 slot, UINT32 port, 
        OAM_VLAN_TAG_T *vlan_tag);

MRVL_ERROR_CODE_T oam_adpt_eth_set_vlan_translation(UINT32 slot, UINT32 port, 
        OAM_ETH_VLAN_TRANSLATION_T *vlan_translation);

MRVL_ERROR_CODE_T oam_adpt_eth_set_vlan_aggregation(UINT32 slot, UINT32 port, 
        OAM_ETH_VLAN_AGGREGATION_T *vlan_aggregation);

MRVL_ERROR_CODE_T oam_adpt_eth_set_vlan_trunk(UINT32 slot, UINT32 port, 
        OAM_ETH_VLAN_TRUNK_T *vlan_trunk);

MRVL_ERROR_CODE_T oam_adpt_eth_create_classification_flow(UINT32 slot, UINT32 port, 
        UINT32 flow_id, UINT32 queue_id, UINT32 pbit, OAM_ETH_RULE_T *flow_rule);

MRVL_ERROR_CODE_T oam_adpt_eth_add_rules_to_flow(UINT32 flow_id, OAM_ETH_RULE_T *rules);

MRVL_ERROR_CODE_T oam_adpt_eth_set_queue_for_flow(UINT32 flow_id, UINT32 queue_id);

MRVL_ERROR_CODE_T oam_adpt_eth_set_pbit_for_flow(UINT32 flow_id, UINT32 pbit);

MRVL_ERROR_CODE_T oam_adpt_eth_del_flow(UINT32 flow_id);

MRVL_ERROR_CODE_T oam_adpt_eth_set_port_mac_filter(UINT32 slot, UINT32 port, 
        UINT32 mac_limit_num, BOOL default_pass);

MRVL_ERROR_CODE_T oam_adpt_eth_add_port_blocking_mac(UINT32 slot, UINT32 port, 
        OAM_ETH_MAC_LIST_T *mac_list);

MRVL_ERROR_CODE_T oam_adpt_eth_add_port_bypass_mac(UINT32 slot, UINT32 port, 
        OAM_ETH_MAC_LIST_T *mac_list);

MRVL_ERROR_CODE_T oam_adpt_eth_remove_port_blocking_mac(UINT32 slot, UINT32 port, 
        OAM_ETH_MAC_LIST_T *mac_list);

MRVL_ERROR_CODE_T oam_adpt_eth_remove_port_bypass_mac(UINT32 slot, UINT32 port, 
        OAM_ETH_MAC_LIST_T *mac_list);

/* Multicast API*/
MRVL_ERROR_CODE_T mrvl_eth_set_mc_protocol(OAM_MC_PROTOCOL_E mc_protocol);

MRVL_ERROR_CODE_T oam_adpt_eth_add_mc_vlan(UINT32 slot, UINT32 port,
        OAM_ETH_MC_VLAN_T *mc_vlans);

MRVL_ERROR_CODE_T oam_adpt_eth_del_mc_vlan(UINT32 slot, UINT32 port,
        OAM_ETH_MC_VLAN_T *mc_vlans);

MRVL_ERROR_CODE_T oam_adpt_eth_clear_mc_vlan (UINT32 slot ,UINT32 port);

MRVL_ERROR_CODE_T oam_adpt_eth_set_mc_vlan_mode (UINT32 slot, UINT32 port,
        OAM_ETH_MC_VLAN_MODE_E vlan_mode);

MRVL_ERROR_CODE_T oam_adpt_eth_set_mc_vlan_strip(UINT32 slot, UINT32 port, UINT16 vlan, BOOL strip);

MRVL_ERROR_CODE_T oam_adpt_eth_set_mc_vlan_translation(UINT32 slot, UINT32 port, 
        OAM_ETH_MC_VLAN_TRANSLATION_T *vlan_trans, BOOL translate);

/*MRVL_ERROR_CODE_T oam_adpt_eth_set_mc_group_max_num (UINT32 slot, UINT32 port,
        UINT8 max_group_num);
*/        

MRVL_ERROR_CODE_T oam_adpt_eth_add_mc_control(UINT32 stream_num, UINT32 new_mbr, 
        OAM_ETH_MC_CONTROL_T *mc_group_ctrl);
                                                  
MRVL_ERROR_CODE_T oam_adpt_eth_mod_mc_control(UINT32 stream_num, UINT32 diff_mbr, 
        OAM_ETH_MC_CONTROL_T *mc_group_ctrl);
                                                   
MRVL_ERROR_CODE_T oam_adpt_eth_del_mc_control(UINT32 stream_num, UINT32 rem_mbr, 
        OAM_ETH_MC_CONTROL_T *mc_group_ctrl);
                                                 
MRVL_ERROR_CODE_T oam_adpt_eth_clear_mc_control(UINT32 slot, UINT32 port);

MRVL_ERROR_CODE_T oam_adpt_eth_get_mc_fast_leave_ability 
        (OAM_ETH_MC_FAST_LEAVE_ABILITY_T *fast_leave_ability);

MRVL_ERROR_CODE_T oam_adpt_eth_set_mc_fast_leave_mode(BOOL enable);

/* IGMP protocol construct multicast control */
MRVL_ERROR_CODE_T oam_adpt_eth_igmp_control_message_received_handle(UINT8* pMsg, UINT16 *msg_len,
        OAM_ETH_IGMP_CTRL_DIRECTION_E *direction, UINT16 *port_num);

MRVL_ERROR_CODE_T oam_adpt_eth_igmp_control_message_send(UINT8 *pMsg, UINT16 msg_len, 
        OAM_ETH_IGMP_CTRL_DIRECTION_E direction, UINT16 port_num);

MRVL_ERROR_CODE_T oam_adpt_eth_igmp_eth_port_add_multicast_mac_entry(OAM_ETH_MAC_ADDR_T *pMacAddr, UINT16 port_num);

MRVL_ERROR_CODE_T oam_adpt_eth_igmp_eth_port_multicast_mac_delete(OAM_ETH_MAC_ADDR_T *pMacAddr, UINT16 port_num);

MRVL_ERROR_CODE_T oam_adpt_eth_igmp_eth_port_multicast_mac_clear(UINT16 port_num);

MRVL_ERROR_CODE_T oam_adpt_eth_igmp_eth_mac_agint_time(UINT32 aging_time);

MRVL_ERROR_CODE_T oam_adpt_eth_igmp_eth_mac_address_seach(OAM_ETH_MAC_ADDR_T* pMacAddr, UINT32 *port_id);

MRVL_ERROR_CODE_T oam_adpt_eth_init(VOID);

#endif /*__OAM_ADAPTER_ETH_H__*/
