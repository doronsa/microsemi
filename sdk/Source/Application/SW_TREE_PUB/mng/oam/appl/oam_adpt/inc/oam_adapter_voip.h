/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : oam_adapter_voip.h                                        **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM adapter layer VoIP functions            **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    12/Jan/2011   - initial version created.                                  
 *                                                                      
 ******************************************************************************/

#ifndef __OAM_ADAPTER_VOIP_H__
#define __OAM_ADAPTER_VOIP_H__

/*------------------------------------------------------------
 *   VoIP Related EPON OAM stack API
 ------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_get_port_num ( UINT32 * port_num);
    \brief return total port number of VoIP capability .

    \param port_num output parameter, return the port number
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_get_port_num(UINT32 *port_num);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_enable_port (const UINT32 slot,const UINT32 port, BOOL enable);
    \brief Enable/Disable voip pots admin.

    \param slot input parameter, specify the slot number
    \param port input parameter, specify the port number
    \param enable input parameter,pots admin control.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_enable_port(UINT32 slot, UINT32 port, BOOL enable);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_get_iad_info (OAM_VOIP_IAD_INFO_T* voip_iad_info);
    \brief return iad basic information,such as mac address,voip protocol ,software .etc

    \param voip_iad_info output parameter, return the iad module basic information .
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_get_iad_info(OAM_VOIP_IAD_INFO_T *voip_iad_info);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_set_global_parameter (OAM_VOIP_GLOBAL_PARAM_T* voip_iad_info);
    \brief set the mg local config ,net parameter,vlan configure.etc.

    \param voip_global_parameter input parameter,specify iad local config,mg local profile related usually .
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_set_global_parameter(OAM_VOIP_GLOBAL_PARAM_T *voip_global_parameter);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_set_h248_parameter (OAM_VOIP_H248_PARAM_T* voip_iad_info);
    \brief set mg h248 protocol related configure.

    \param voip_h248_mgc_t input parameter,specify 248 parameter config,mg local profile usually .
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_set_h248_parameter(OAM_VOIP_H248_PARAM_T *voip_h248_mgc_t);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_get_h248_active_mgc (OAM_VOIP_H248_ACTIVE_MGC_E * active_mgc);
    \brief return current actived mgc entity.

    \param active_mgc output parameter,get the active mgc entity that iad registered with currently.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_get_h248_active_mgc(OAM_VOIP_H248_ACTIVE_MGC_E *active_mgc);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_set_h248_user_tid ( UINT32 slot,  UINT32 port,  OAM_VOIP_H248_USER_TID_CONFIG_T * voip_h248_user_tid);
    \brief set h248 port TID configure.

    \param slot input parameter, specify the slot number
    \param port input parameter, specify the port number
    \param voip_h248_user_tid input parameter,pots physical terminal id.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */

MRVL_ERROR_CODE_T oam_adpt_voip_set_h248_user_tid(UINT32 slot, UINT32 port, OAM_VOIP_H248_USER_TID_CONFIG_T *voip_h248_user_tid);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_set_h248_rtp_tid ( OAM_VOIP_H248_RTP_TID_CONFIG_T * voip_h248_rtp_tid);
    \brief set h248 rtp configure.

    \param voip_h248_rtp_tid input parameter,get mg rtp related config,more info #OAM_VOIP_H248_RTP_TID_CONF_T.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_set_h248_rtp_tid(OAM_VOIP_H248_RTP_TID_CONFIG_T *voip_h248_rtp_tid);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_get_h248_rtp_tid ( OAM_VOIP_H248_RTP_TID_INFO_T * voip_h248_rtp_tid);
    \brief return mg h248 rtp information.

    \param voip_h248_rtp_tid output parameter,get mg rtp related config,more info #OAM_VOIP_H248_RTP_TID_CONF_T.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_get_h248_rtp_tid(OAM_VOIP_H248_RTP_TID_INFO_T *voip_h248_rtp_tid);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_set_sip_parameter ( OAM_VOIP_SIP_PARAM_T * sip_param);
    \brief set sip protocol parameter.

    \param sip_param output parameter,get current mgc sip protocol related config,such as server,proxy-server .etc..
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_set_sip_parameter(OAM_VOIP_SIP_PARAM_T *sip_param);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_set_sip_user_parameter ( UINT32 slot,  UINT32 port,  OAM_VOIP_SIP_USER_PARAM_T * sip_user_param);
    \brief set sip user related parameter.

    \param slot input parameter, specify the slot number
    \param port input parameter, specify the port number
    \param sip_user_param input parameter,sip pots related configure.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_set_sip_user_parameter(UINT32 slot, UINT32 port, OAM_VOIP_SIP_USER_PARAM_T *sip_user_param);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_set_fax_config ( OAM_VOIP_FAX_CONFIG_T * fax_config);
    \brief set iad fax configure.

    \param fax_config input parameter,control the mg fax configure,fax mode and control type.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_set_fax_config(OAM_VOIP_FAX_CONFIG_T *fax_config);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_get_h248_iad_operation_status (OAM_VOIP_H248_OPER_STATUS_E * voip_iad_oper_status);
    \brief return current iad operation status ,only h248 protocol support.

    \param voip_iad_oper_status output parameter,return current registered status,H.248 protocol only.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_get_h248_iad_operation_status(OAM_VOIP_H248_OPER_STATUS_E *voip_iad_oper_status);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_get_pots_status ( UINT32 slot,  UINT32 port, OAM_VOIP_PORT_STATUS_T * pots_status);
    \brief return port operation status.
   
    \param slot input parameter, specify the slot number
    \param port input parameter, specify the port number
    \param pots_status output parameter,return pots related operation status.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_get_pots_status(UINT32 slot, UINT32 port, OAM_VOIP_POTS_STATUS_T *pots_status);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_set_iad_operation (OAM_VOIP_IAD_OPER_E* voip_iad_operation);
    \brief this api is used to operate the iad. 
   
    \param voip_iad_operation input parameter,control the iad entity operation.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_set_iad_operation(OAM_VOIP_IAD_OPER_E *voip_iad_operation);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_set_sip_digit_map (OAM_SIP_DIGIT_MAP_T * sip_digit_map);
    \brief this api is used to set sip digit map to iad ,only sip protocol support.
   
    \param sip_digit_map input parameter,sip digit map ,no more than 1024 Byte length.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_set_sip_digit_map(OAM_SIP_DIGIT_MAP_T *sip_digit_map);


/*-------------------------------------------------------------------------------------------*/
/*! \fn MRVL_ERROR_CODE_T oam_adpt_voip_get_sip_active_mgc (UINT32 * active_mgc_ip);
    \brief return current active mgc entity ,only sip protoclo support.

    \param active_mgc_ip output parameter,get current mgc address mg registered.
    \return 0 if OK, negative if error, see oam_stack_comm.h for details
 */
MRVL_ERROR_CODE_T oam_adpt_voip_get_sip_active_mgc(UINT32 *active_mgc_ip);

#endif /*__OAM_ADAPTER_VOIP_H__*/
