/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : oam_adapter_pon.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : Definition of adapter layer PON related functions         **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    12/Jan/2011   - initial version created.                                    
 *                                                                      
 ******************************************************************************/
 
#ifndef __OAM_ADAPTER_PON_H__
#define __OAM_ADAPTER_PON_H__

typedef struct
{
    UINT16 transceiver_temperature;
    UINT16 supply_voltage;
    UINT16 tx_bias_current;
    UINT16 tx_power;
    UINT16 rx_power;
}OAM_PON_TRANSCEIVER_DIAGNOSIS_T;

typedef struct
{
    UINT8     high_priority_boundary;
    UINT8     queue_id;             /* Queue to which service traffic is classified */
    UINT16    fixed_packet_size;    /* Given in byte units */
    UINT16    fixed_bandwidth;      /* Fixed bandwidth in 256 Kbps units */
    UINT16    guaranteed_bandwidth; /* Assured bandwidth in 256 Kbps units */
    UINT16    best_effort_bandwidth;/* Best effort bandwidth in 256 Kbps units */
    UINT8     wrr_weight;           /* Possible values: 0 (SP), 1 - 100 (WRR) */
}OAM_PON_SLA_SCHEME_T;

MRVL_ERROR_CODE_T oam_adpt_pon_get_transceiver_diagnosis(OAM_PON_TRANSCEIVER_DIAGNOSIS_T* transceiver_diagnosis);
MRVL_ERROR_CODE_T oam_adpt_pon_set_sla(E_PonSlaBestEffortSchedulingScheme bf_sched,  UINT8 boundary,UINT32 cycle_length, UINT32 service_num, OAM_PON_SLA_SCHEME_T *sla_para);
MRVL_ERROR_CODE_T oam_adpt_pon_set_holdover_time(UINT32 holdover_time, UINT32 holdover_state);
MRVL_ERROR_CODE_T oam_adpt_pon_set_llid_queue(UINT8 llid, const OAM_PON_LLID_QUEUE_CONFIG_T *llid_queue_configure);
MRVL_ERROR_CODE_T oam_adpt_pon_set_actived_llid(UINT32 llid_actived );

#endif /*__OAM_ADAPTER_PON_H__*/
