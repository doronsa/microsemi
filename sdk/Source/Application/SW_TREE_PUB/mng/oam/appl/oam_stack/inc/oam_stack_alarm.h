/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_stack_alarm.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM alarm                                   **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.         
 *                                                                              
 ******************************************************************************/
 
#ifndef __OAM_STACK_ALARM_H__
#define __OAM_STACK_ALARM_H__

#define OAM_ALARM_SUPPORT_LIST_NUM  512

/* Set alarm admin
**
** This function set onu alarm control.
**
** Input Parameters:
**        slot_id                : OLT id, range: PON_MIN_slot_id - PON_MAX_slot_id
**        port_id                : ONU id, range: PON_MIN_port_id_PER_OLT - PON_MAX_port_id_PER_OLT
**                         : as to the object that donot need tlv , ignored
**      alarm_admin    : See CTC_spec_alarm_admin_state_t for details.
**
**
** Return codes:
**        All the defined oam_stack_* return codes
**
*/
STATUS oam_stack_alarm_set_admin(UINT32 slot, UINT32 port, OAM_ALARM_ID_E alarm_id, OAM_OAM_ALARM_CTRL_STATE_E admin_state);

/* Get alarm admin
**
** This function get onu alarm control.
**
** Input Parameters:
**        slot_id                : OLT id, range: PON_MIN_slot_id - PON_MAX_slot_id
**        port_id                : ONU id, range: PON_MIN_port_id_PER_OLT - PON_MAX_port_id_PER_OLT
**      alarm_admin    : See CTC_spec_alarm_admin_state_t for details.
**
**
** Return codes:
**        All the defined oam_stack_* return codes
**
*/
STATUS oam_stack_alarm_get_admin(UINT32 slot, UINT32 port, OAM_ALARM_ID_E alarm_id, OAM_ALARM_ADMIN_STATE_T *admin_state);

/* Get alarm thrshold
**
** This function get onu alarm thrshold.
**
** Input Parameters:
**        slot_id                : OLT id, range: PON_MIN_slot_id - PON_MAX_slot_id
**        port_id                : ONU id, range: PON_MIN_port_id_PER_OLT - PON_MAX_port_id_PER_OLT
**      alarm_threshold    : See OAM_ALARM_THRESHOLD_T for details.
**
**
** Return codes:
**        All the defined oam_stack_* return codes
**
*/
STATUS oam_stack_alarm_get_threshold(UINT32 slot, UINT32 port, OAM_ALARM_ID_E alarm_id, OAM_ALARM_THRESHOLD_T *alarm_threshold);

/* Set alarm thrshold
**
** This function get onu alarm thrshold.
**
** Input Parameters:
**        slot_id                : OLT id, range: PON_MIN_slot_id - PON_MAX_slot_id
**        port_id                : ONU id, range: PON_MIN_port_id_PER_OLT - PON_MAX_port_id_PER_OLT
**      alarm_threshold    : See OAM_ALARM_THRESHOLD_T for details.
**
**
** Return codes:
**        All the defined oam_stack_* return codes
**
*/
STATUS oam_stack_alarm_set_threshold(UINT32 slot, UINT32 port, OAM_ALARM_ID_E alarm_id, OAM_ALARM_THRESHOLD_T *alarm_threshold);

/* CTC spec alarm send api
**
** This function get onu alarm thrshold.
**
** Input Parameters:
**        send_alarm_buff                :alarm struct refer to CTC.V21 spec.
**        length                : 
**
**
** Return codes:
**        All the defined oam_stack_* return codes
**
*/
STATUS oam_stack_alarm_general_send_func(UINT8 *send_alarm_buff, UINT16 length);

/*------------------------------------------------------------------------
**
**
*/
#define OAM_ALARM_THREAD_QUERY_INTERVAL_IN_MS 2000
#define OAM_ALARM_ETH_PORT_AUTONEG_FAIL       (0x0001) 
#define OAM_ALARM_ETH_PORT_LOS                (0x0002) 
#define OAM_ALARM_ETH_PORT_FAIL               (0x0004)
#define OAM_ALARM_ETH_PORT_LOOPBACK           (0x0008)
#define OAM_ALARM_ETH_PORT_CONGESTION         (0x0010)

#define OAM_ALARM_ETH_PORT_AUTONEG_FAIL_CLEAR (0x0001<<16) 
#define OAM_ALARM_ETH_PORT_LOS_CLEAR          (0x0002<<16) 
#define OAM_ALARM_ETH_PORT_FAIL_CLEAR         (0x0004<<16)
#define OAM_ALARM_ETH_PORT_LOOPBACK_CLEAR     (0x0008<<16)
#define OAM_ALARM_ETH_PORT_CONGESTION_CLEAR   (0x0010<<16)

#define OAM_ALARM_ETH_DEF_STATE  (OAM_ALARM_ETH_PORT_AUTONEG_FAIL | \
                                  OAM_ALARM_ETH_PORT_LOS              | \
                                  OAM_ALARM_ETH_PORT_FAIL         | \
                                  OAM_ALARM_ETH_PORT_LOOPBACK         | \
                                  OAM_ALARM_ETH_PORT_CONGESTION)
                                  
typedef STATUS (*OAM_ALARM_STATE_REQUEST_FUNC_T)(UINT32 slot, UINT32 port, OAM_ALARM_ADMIN_STATE_T *alarm_admin); 

typedef STATUS (*OAM_ALARM_ADMIN_CONFIG_FUNC_T)(UINT32 slot, UINT32 port, OAM_ALARM_ADMIN_STATE_T *alarm_admin); 

typedef STATUS (*OAM_ALARM_THRESHOLD_REQUEST_FUNC_T)(UINT32 slot, UINT32 port, OAM_ALARM_THRESHOLD_T *alarm_admin); 

typedef STATUS (*OAM_ALARM_THRESHOLD_CONFIG_FUNC_T)(UINT32 slot, UINT32 port, OAM_ALARM_THRESHOLD_T *alarm_threshold); 

typedef enum
{
    OAM_ALARM_TYPE_CLEAR       = 0x00,    
    OAM_ALARM_TYPE_REPORT      = 0x01   
}OAM_ALARM_TYPE_E;

typedef enum
{
    OAM_ALARM_STATE_DISABLE    = 0x01,    
    OAM_ALARM_STATE_ENABLE     = 0x02    
}OAM_ALARM_STATE_E;

typedef struct
{
    OAM_ALARM_ID_E                     alarm_id;
    OAM_ALARM_ADMIN_CONFIG_FUNC_T      alarm_admin_set;        
    OAM_ALARM_THRESHOLD_CONFIG_FUNC_T  alarm_threshold_set;        
    OAM_MNG_OBJECT_LEAF_TYPE_E         if_instance_type;
}OAM_ALARM_HANDLING_FUNC_T;

typedef struct
{
    OAM_ALARM_ID_E                     alarm_id;
    UINT32                             slot_if;
    UINT32                             port_if;
    BOOL                               alarm_admin;
    UINT32                             raise_alarm_threshold;
    UINT32                             clear_alarm_threshold;
}OAM_ALARM_CFG_RECORD_T;

typedef struct 
{
    OAM_ALARM_NODE_T       list;
    OAM_ALARM_CFG_RECORD_T cfg_list;
}OAM_ALARM_CFG_RECORD_LIST_T;

typedef struct
{
    OAM_ALARM_ADMIN_CONFIG_FUNC_T      alarm_admin_set;
    OAM_ALARM_THRESHOLD_CONFIG_FUNC_T  alarm_threshold_set;        
}OAM_ALARM_HANDLER_HOOK_T;

#define OAM_ALARM_DESC_PER_LIST_MAX_LEN 4
#define OAM_ALARM_DESC_LIST_TOTAL       8
#define OAM_ALARM_DESC_SINGLE_ALR_LEN   18

typedef struct
{
    UINT16 obj_type_leaf;
    UINT32 instance_id;
}POS_PACKED OAM_ALARM_OBJECT_INSTANCE_T;

typedef struct 
{
    OAM_ALARM_OBJECT_INSTANCE_T obj_instance;
    OAM_ALARM_ID_E              alarm_id;
    OAM_ALARM_REPORT_STATE_E    alarm_state;
    UINT16                      time_stamp;
    UINT32                      value;
}POS_PACKED  OAM_ALARM_REPORT_INFO_T;

INT32 oam_stack_compose_alarm_report_pdu
(   
    OAM_ALARM_REPORT_INFO_T    *alarm_info,
    UINT8                      *pdu_data,
    UINT16                     *length 
);

INT32 oam_stack_compose_alarm_report_oam_frame
(
    UINT8          *alarm_pdu_data,
    UINT16         alarm_pdu_length,
    UINT8          sequence_number,   
    UINT8          *send_pdu_data,
    UINT16         *send_length 
);

UINT32 oam_stack_sub_compose_object_info_v21
(
    UINT32 port_type, 
    UINT32 frame, 
    UINT32 slot, 
    UINT32 port
); 

STATUS oam_stack_check_valid_object_port_and_parse_out
(
    OAM_MNG_OBJECT_PORT_TYPE_E expect_object_type, 
    const OAM_MNG_OBJECT_T     *a_port_type, 
    UINT32                     *frame_id, 
    UINT32                     *slot_id, 
    UINT32                     *port_id
);

#endif /*__OAM_STACK_ALARM_H__*/
