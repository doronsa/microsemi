/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : oam_stack_os_vxworks.h                                    **/
/**                                                                          **/
/**  DESCRIPTION : Wrap Vxworks system function                              **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.                
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_OS_VXWORKS_H__
#define __OAM_STACK_OS_VXWORKS_H__

#ifdef VXWORKS

#include <vxWorks.h>
#include<sysLib.h>
#include <iolib.h>
#include <semlib.h>
#include <smmemlib.h>
#include <vwModNum.h>
#include <tasklib.h>
#include <msgqlib.h>
#include <timers.h>
#include <ticklib.h>
#include <exclib.h>
#include <intlib.h>
#include <esf.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <logLib.h>
#include <sockLib.h>
#include <inetLib.h>
#include <selectLib.h>
#include "sysLib.h"
#include <symLib.h>
#include <sysSymTbl.h>
#include <assert.h>

/*
** PosSL defines
*/
#define OSSRV_INVALID_VALUE            (INT32)(-1)

#define OSSRV_COM_PID_PRI              100
/*
** wait option
*/
#define OSSRV_TIME_FOREVER             WAIT_FOREVER
#define OSSRV_TIME_NOWAIT              NO_WAIT 

/*
** wait queue method
*/

/*
** first in first out wait queue
*/
#define OSSRV_WAIT_FIFO                SEM_Q_FIFO

/*
** priority wait queue
*/
#define OSSRV_WAIT_PRIORITY            SEM_Q_PRIORITY
/*
** task ID in vxworks platform
*/
typedef INT32    POS_TASKID;


typedef MSG_Q_ID    MSG_QUEUE_ID_T;


typedef SEM_ID  SEM_ID_T ;
typedef key_t SEM_KEY_T;

extern INT32 semCCreate();
extern STATUS semGive(SEM_ID_T semId );              /* semaphore ID to give */
extern STATUS semDelete(SEM_ID_T semId);             /* semaphore ID to give */
extern STATUS semTake(SEM_ID_T semId, INT32 timeout);/* semaphore ID to take */
extern INT32 sysClkRateGet( ) ;

extern INT32 msgQCreate ();
extern INT32 msgQDelete ();
extern INT32 msgQNumMsgs ();
extern INT32 msgQReceive();
extern INT32 msgQSend();
extern INT32 taskDelay(INT32 ticks);

extern INT32 errno;

#define OAM_OSSRV_sem_creat(func,m,i) (*(m) = oam_ossrv_sem_create(func, m,i))
#define OAM_OSSRV_sem_destory(func,m) (oam_ossrv_sem_destroy(func,m))
#define oam_ossrv_sem_wait(func,m,t) (oam_ossrv_sem_wait(func ,m, t))
#define oam_ossrv_sem_give(func,m) (oam_ossrv_sem_give(func,m))

#define OAM_OSSRV_msgQueue_creat(func,m,i) (*(m) = oam_ossrv_sem_create(func, m,i))
#define OAM_OSSRV_msgQueue_delete(func,m) (oam_ossrv_msg_delete(func,m))
#define OAM_OSSRV_msgQueue_send(func,m,buff,size,pri) (oam_ossrv_msg_send(func ,m, buff,size,pri))
#define OAM_OSSRV_msgQueue_received(func,m,buff,size,t) (oam_ossrv_msg_receive(func,m,buff,size,t))
    
#define OAM_OSSRV_wait_in_tick oam_vxworks_task_delay

#define OAM_OSSRV_task_pid_Get oam_vxworks_task_id_get

#define OAM_OSSRV_pthread_create(pid,pName,start_routine,arg)   oam_vxworks_ptherad_create(pid,pName,start_routine,arg)

#define __exit  exit
#endif /*VXWORKS*/
#endif /*__OAM_STACK_OS_VXWORKS_H__*/
