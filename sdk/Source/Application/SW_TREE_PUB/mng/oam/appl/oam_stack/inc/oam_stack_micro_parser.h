/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : PON Stack                                                 **/
/**                                                                          **/
/**  FILE        : CTC_micro_parser.h                                        **/
/**                                                                          **/
/**  DESCRIPTION : BYTE converter functions                                  **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.               
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_MICRO_PARSER_H__
#define __OAM_STACK_MICRO_PARSER_H__

#ifndef PON_SYSTEM_ENDIANITY_CHANGE
#define PON_SYSTEM_ENDIANITY_CHANGE 
#endif

/* Macro for translation from bool variable to 2 UINT8s buffer
**
** Input parameters:
**    __x__ - bool parameter
**
** Output parameters:
**    buffer  - Pointer to UINT8 array of two chars or more
** 
*/
#define BOOL_2_WORD_UBUFFER( __x__, buffer )  {if (__x__ == TRUE) buffer[1] = 0x1; else buffer[1] = 0x0; buffer[0] = 0x0;}

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define BOOL_2_ENDIANITY_WORD_UBUFFER( __x__, buffer )  {if (__x__ == TRUE) buffer[0] = 0x1; else buffer[0] = 0x0; buffer[1] = 0x0;}
#else
#define BOOL_2_ENDIANITY_WORD_UBUFFER BOOL_2_WORD_UBUFFER
#endif


/* Macro for translation from UINT8s buffer to bool variable
**
** Input parameters:
**    buffer  - Pointer to UINT8 array of two chars 
**
** Output parameters:
**    __x__ - bool parameter
** 
*/
#define WORD_UBUFFER_2_BOOL(buffer, __x__) {if ((buffer[0] == 0x0) && (buffer[1] == 0x0)) __x__ = FALSE; else __x__ = TRUE;}

#ifdef PON_SYSTEM_ENDIANITY_CHANGE /* doesn't affect the implementation */
#define ENDIANITY_WORD_UBUFFER_2_BOOL  WORD_UBUFFER_2_BOOL
#else
#define ENDIANITY_WORD_UBUFFER_2_BOOL  WORD_UBUFFER_2_BOOL
#endif


/* Macro for translation from UINT16 number to UINT8 buffer
**
** Input parameters:
**    x - UINT16 number
**
** Output parameters:
**    buffer - Pointer to UINT8 array
** 
*/
#define USHORT_2_UBUFFER( x, buffer )  {buffer[1] = (UINT8) (x & (0x00ff)); buffer[0] = (UINT8)((x & (0xff00))>>BITS_IN_BYTE);}

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define USHORT_2_ENDIANITY_UBUFFER( x, buffer )  {buffer[0] = (UINT8) (x & (0x00ff)); buffer[1] = (UINT8)((x & (0xff00))>>BITS_IN_BYTE);}
#else
#define USHORT_2_ENDIANITY_UBUFFER USHORT_2_UBUFFER
#endif


/* Macro for translation from UINT8 buffer to UINT16 number
**
** Input parameters:
**    buffer - Pointer to UINT8 array
**
** Output parameters:
**    x - UINT16 number
** 
*/
#define UBUFFER_2_USHORT( buffer, x ) x = (UINT16)(buffer[1] + (buffer[0] << BITS_IN_BYTE));

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define UBUFFER_2_ENDIANITY_USHORT( buffer, x ) x = (UINT16)(buffer[0] + ((buffer[1] << BITS_IN_BYTE)));
#else
#define UBUFFER_2_ENDIANITY_USHORT UBUFFER_2_USHORT
#endif


/* Macro for translation from UINT8 (represent by word ) buffer to UINT8
**
** Input parameters:
**    buffer  - Pointer to UINT8 array
**
** Output parameters:
**    x - UINT8 number
** 
*/
#define WORD_UBUFFER_2_UCHAR( buffer, x )  x = buffer[1];

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define WORD_UBUFFER_2_ENDIANITY_UCHAR( buffer, x ) x = buffer[0];
#else
#define WORD_UBUFFER_2_ENDIANITY_UCHAR  WORD_UBUFFER_2_UCHAR
#endif


/* Macro for translation from UINT8 to buffer
**
** Input parameters:
**    x - UINT8 number
**
** Output parameters:
**    buffer  - Pointer to UINT8 array
**
*/
#define UCHAR_2_UBUFFER( x, buffer ) buffer[1] = x;

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define UCHAR_2_ENDIANITY_UBUFFER( x, buffer ) buffer[0] = x;
#else
#define UCHAR_2_ENDIANITY_UBUFFER  UCHAR_2_UBUFFER
#endif


/* Macro for translation from UINT32 number to UINT8 buffer
**
** Input parameters:
**    x - UINT32 number
**
** Output parameters:
**    buffer  - Pointer to UINT8 array
** 
*/
#define ULONG_2_UBUFFER( x, buffer ) {buffer[3] = (UINT8) (x & (0x000000ff)); buffer[2] = (UINT8)((x & (0x0000ff00))>>BITS_IN_BYTE); buffer[1] = (UINT8)((x & (0x00ff0000))>>(BITS_IN_BYTE*2)); buffer[0] = (UINT8)((x & (0xff000000))>>(BITS_IN_BYTE*3));}

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define ULONG_2_ENDIANITY_UBUFFER( x, buffer )  {buffer[0] = (UINT8) ((UINT32)x & (0x000000ff)); buffer[1] = (UINT8)(((UINT32)x & (0x0000ff00))>>BITS_IN_BYTE); buffer[2] = (UINT8)(((UINT32)x & (0x00ff0000))>>(BITS_IN_BYTE*2)); buffer[3] = (UINT8)(((UINT32)x & (0xff000000))>>(BITS_IN_BYTE*3));}
#else
#define ULONG_2_ENDIANITY_UBUFFER ULONG_2_UBUFFER
#endif


/* Macro for translation from UINT8 buffer to UINT32 number
**
** Input parameters:
**    x - UINT32 number
**
** Output parameters:
**    buffer - Pointer to UINT8 array
** 
*/
#define UBUFFER_2_ULONG( buffer, x )  x = buffer[3] + (buffer[2] << BITS_IN_BYTE) + (buffer[1] << (BITS_IN_BYTE*2)) + (buffer[0] << (BITS_IN_BYTE*3));

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
    #define UBUFFER_2_ENDIANITY_ULONG( buffer, x ) x = buffer[0] + (buffer[1] << BITS_IN_BYTE) + (buffer[2] << (BITS_IN_BYTE*2)) + (buffer[3] << (BITS_IN_BYTE*3));
#else
    #define UBUFFER_2_ENDIANITY_ULONG UBUFFER_2_ULONG
#endif


/* Macro for translation from UINT8 buffer to int24 (maybe casted into long)
**
** Input parameters:
**    x - unsigned int24 number
**
** Output parameters:
**    buffer - Pointer to UINT8 array
**    
*/
#define UBUFFER_2_UINT24( buffer, x )  x = buffer[2] + (buffer[1] << BITS_IN_BYTE) + (buffer[0] << (BITS_IN_BYTE*2));

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
    #define UBUFFER_2_ENDIANITY_UINT24( buffer, x ) x = buffer[0] + (buffer[1] << BITS_IN_BYTE) + (buffer[2] << (BITS_IN_BYTE*2));
#else
    #define UBUFFER_2_ENDIANITY_UINT24 UBUFFER_2_UINT24
#endif


/* Macro for translation from UINT8 buffer to double number
**
** Input parameters:
**    x - double number
**
** Output parameters:
**    buffer - Pointer to UINT8 array
**    
*/
#define UBUFFER_2_DOUBLE( buffer, x ){INT32 __counter___; x = 0; for (__counter___=0;__counter___ < BYTES_IN_DOUBLE;__counter___ ++)      x = ((x * (2<<(BITS_IN_BYTE-1))) + buffer[__counter___]);}

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define UBUFFER_2_ENDIANITY_DOUBLE( buffer, x ) {INT32 __counter___; x = 0; for (__counter___=(BYTES_IN_DOUBLE-1);__counter___ >= 0;__counter___ --) x = ((x * (2<<(BITS_IN_BYTE-1))) + buffer[__counter___]);}
#else
#define UBUFFER_2_ENDIANITY_DOUBLE UBUFFER_2_DOUBLE
#endif


/* Macro for translation from 64-bit unsigned integer (unsigned__int64) to UINT8 buffer 
**
** Input parameters:
**    x - 64-bit unsigned integer
**
** Output parameters:
**    buffer - Pointer to UINT8 array
**    
*/
#define INT64_2_UBUFFER( x, buffer ) { ULONG_2_UBUFFER((x.msb), buffer) ULONG_2_UBUFFER((x.lsb), (buffer+BYTES_IN_LONG)) }

#define INT48_2_UBUFFER( x, buffer ) { USHORT_2_UBUFFER((x.msb), buffer) ULONG_2_UBUFFER((x.lsb), (buffer+BYTES_IN_WORD))}

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define INT64_2_ENDIANITY_UBUFFER( x, buffer )    { ULONG_2_ENDIANITY_UBUFFER((UINT32)(x.lsb), buffer) ULONG_2_ENDIANITY_UBUFFER((UINT32)(x.msb), (buffer+BYTES_IN_LONG)) }
#else
#define INT64_2_ENDIANITY_UBUFFER  INT64_2_UBUFFER
#endif


/* Macro for translation from UINT8 buffer to 64-bit unsigned integer (unsigned__int64) 
**
** Input parameters:
**    buffer - Pointer to UINT8 array
**
** Output parameters:
**    x - 64-bit unsigned integer
**
*/
#define UBUFFER_2_INT64( buffer, x ) { UBUFFER_2_ULONG   (buffer, x.msb) UBUFFER_2_ULONG  ((buffer+BYTES_IN_LONG), x.lsb) }

#define UBUFFER_2_INT48( buffer, x ) { UBUFFER_2_USHORT  (buffer, x.msb) UBUFFER_2_ULONG  ((buffer+BYTES_IN_WORD), x.lsb) }

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define UBUFFER_2_ENDIANITY_INT64( buffer, x) { UBUFFER_2_ENDIANITY_ULONG(buffer, x.lsb) UBUFFER_2_ENDIANITY_ULONG((buffer+BYTES_IN_LONG), x.msb) }
#else
#define UBUFFER_2_ENDIANITY_INT64  UBUFFER_2_INT64
#endif


/* Macro for translation from UINT8 buffer to another UINT8 buffer
**
** Input parameters:
**    src_buffer - Pointer to UINT8 array
**    size - Size of chars (bytes) to copy
**
** Output parameters:
**    dest_buffer - Pointer to UINT8 array
**    
*/
#define UBUFFER_2_UBUFFER( dest_buffer, src_buffer, size )                        {INT32 __counter___; for (__counter___=0; __counter___<size; dest_buffer[__counter___] = src_buffer[__counter___], __counter___ ++);}

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define UBUFFER_2_ENDIANITY_UBUFFER( dest_buffer, src_buffer, size )          {INT32  __counter___; for (__counter___=0; __counter___<size; dest_buffer[size-__counter___-1] = src_buffer[__counter___], __counter___ ++);}
#define UBUFFER_2_ENDIANITY_UBUFFER_UNSIGNED( dest_buffer, src_buffer, size ) {UINT32 __counter___; for (__counter___=0; __counter___<size; dest_buffer[size-__counter___-1] = src_buffer[__counter___], __counter___ ++);}
#else
#define UBUFFER_2_ENDIANITY_UBUFFER  UBUFFER_2_UBUFFER
#define UBUFFER_2_ENDIANITY_UBUFFER_UNSIGNED( dest_buffer, src_buffer, size ) {UINT32 __counter___; for (__counter___=0; __counter___<size; dest_buffer[__counter___] = src_buffer[__counter___], __counter___ ++);}   
#endif


#define UBUFFER_2_BIGGER_UBUFFER( dest_buffer, dest_buffer_size, src_buffer, src_buffer_size, padd ) \
{   INT32 __counter___,__diff__; \
    __diff__ = dest_buffer_size - src_buffer_size; \
    if (__diff__>=0) { for (__counter___=0; __counter___<src_buffer_size; dest_buffer[__counter___+__diff__] = src_buffer[__counter___],__counter___ ++); \
     memset( dest_buffer, padd, __diff__ );}}

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define UBUFFER_2_BIGGER_ENDIANITY_UBUFFER( dest_buffer, dest_buffer_size, src_buffer, src_buffer_size, padd ) \
{   INT32 __counter___,__diff__; \
    __diff__ = dest_buffer_size - src_buffer_size; \
     if (__diff__>=0) { for (__counter___=0; __counter___<src_buffer_size; dest_buffer[dest_buffer_size-__counter___-1-__diff__] = src_buffer[__counter___],__counter___ ++); \
         memset( dest_buffer+src_buffer_size, padd, __diff__ );}}
#else
#define UBUFFER_2_BIGGER_ENDIANITY_UBUFFER  UBUFFER_2_BIGGER_UBUFFER
#endif


/* Macro for translation from UINT8 array to UINT8 buffer
**
** Input parameters:
**    src_buffer - UINT8 array
**    size -   Size of chars (bytes) to copy
**
** Output parameters:
**    dest_buffer - Pointer to UINT8 array
**    
*/
/*#define UARRAY_2_ENDIANITY_UBUFFER( dest_buffer, src_buffer, size ) {  long INT32 __counter___; for (__counter___=0; __counter___<(size/4);  ULONG_2_ENDIANITY_UBUFFER((UINT32)src_buffer[__counter___*4], dest_buffer + (__counter___*4)) ,__counter___ ++);} */
/*#define UARRAY_2_ENDIANITY_UBUFFER( dest_buffer, src_buffer, size )\
{\  
INT32 temp=0;\
}
for(__count___=0; (__count___) < 4;  __count___++)\
{\
dest_buffer[__count___] = src_buffer[__count___];\
ULONG_2_ENDIANITY_UBUFFER((*(src_buffer+(__counter___*4))), dest_buffer + (__counter___*4)); 
}\  
} */


/* Copy macro optimized for MAC address */
#define  PON_MAC_ADDRESS_ENDIANITY_COPY( dst_buffer, src_buffer )  UBUFFER_2_ENDIANITY_UBUFFER( dst_buffer, src_buffer, OAM_MAC_ADDR_LEN )


/* Reverse bytes in UINT16 parameter (hardware register type)
**
** Parameters:
** value  - number to be bereversed
**  
** Return values:
** EXIT_ERROR : no error
** EXIT_OK    : error
*/

#define LIMITED_ULONG_2_UBUFFER( x, buffer ) {buffer[2] = (UINT8) (x & (0x000000ff)); buffer[1] = (UINT8)((x & (0x0000ff00))>>BITS_IN_BYTE); buffer[0] = (UINT8)((x & (0x00ff0000))>>(BITS_IN_BYTE*2));}

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define LIMITED_ULONG_2_ENDIANITY_UBUFFER( x, buffer )  {buffer[0] = (UINT8) (x & (0x000000ff)); buffer[1] = (UINT8)((x & (0x0000ff00))>>BITS_IN_BYTE); buffer[2] = (UINT8)((x & (0x00ff0000))>>(BITS_IN_BYTE*2));}
#else
#define LIMITED_ULONG_2_ENDIANITY_UBUFFER LIMITED_ULONG_2_UBUFFER
#endif


/* Macro for translation from UINT8 buffer to limited(only 3 bytes) UINT32 number
**
** Input parameters:
**         - limited UINT32 number
**
** Output parameters:
** buffer  - Pointer to UINT8 array
**
*/

#define UBUFFER_2_LIMITED_LONG( buffer, x ) x = buffer[2] + (buffer[1] << BITS_IN_BYTE) + (buffer[0] << (BITS_IN_BYTE*2));

#ifdef PON_SYSTEM_ENDIANITY_CHANGE
#define UBUFFER_2_ENDIANITY_LIMITED_LONG( buffer, x ) x = buffer[0] + (buffer[1] << BITS_IN_BYTE) + (buffer[2] << (BITS_IN_BYTE*2));
#else
#define UBUFFER_2_ENDIANITY_LIMITED_LONG UBUFFER_2_LIMITED_LONG
#endif


#endif /* __OAM_STACK_MICRO_PARSER_H__ */
