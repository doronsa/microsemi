/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : oam_stack_ossl.c                                          **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OS adapter layer                            **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *    12/Jan/2011   - initial version created.        
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_OSSL_H__
#define __OAM_STACK_OSSL_H__

#include <string.h>
#include <errno.h>

/*============================Include Files ==================================*/
#ifdef LINUX
#include "oam_stack_os_linux.h"
#elif VXWORKS 
#include "oam_stack_os_vxworks.h"
#endif

/*=========================== task priority ==================================*/


/*========================== assert defines ==================================*/
STATUS oam_stack_ossl_assert_x(STATUS lExpression, UINT8 *pcFileName, STATUS ulLine);

#define OAMASSERT(lExpression) oam_stack_ossl_assert_x(lExpression, (UINT8 *)__FILE__, __LINE__)

#define OAMASSERT_RT(p, r) \
{ \
    if (OAMASSERT(p) != TRUE) \
    { \
        oam_stack_log_printf(OAMLOG_MODULE, OAM_LOG_DEBUG, "Exit %s(), ret_code[0x%x]\r\n", __FUNCTION__, r); \
        return (r); \
    } \
}

UINT32 os_strlen(const INT8 *string);

INT8*  os_strstr(INT8 *haystack, INT8 *needle);

VOID   os_exit(INT32 status);

INT32  os_chmod(INT8 *pathname, INT32 mode);

VOID   os_memmove(VOID *dest, const VOID *src, UINT32 len);

VOID   os_memset(VOID *dest, const UINT8, UINT32 lenth_in_byte);

/*============================== memory operation================================*/
extern UINT32 g_inUse;

VOID   oam_stack_nfree(VOID *p, UINT32 size);

VOID   *oam_stack_malloc(UINT32 size);

VOID   *oam_stack_calloc(UINT32 num_elems, UINT32 elem_size);

INT8   *os_inet_ntoa(UINT32 ip_address);

/*================================ lst definition ===============================*/
#define OAM_ALARM_LIST_TMAX               100000

/* type definitions */
typedef struct OAM_NODE        /* Node of a linked list.                    */
{
    struct OAM_NODE *next;     /* Pointers to the next node in the list     */
    struct OAM_NODE *previous; /* Pointers to the previous node in the list */
}OAM_ALARM_NODE_T;

typedef struct       /* Header of a linked list. */
{
    OAM_ALARM_NODE_T node;    /* Header list node            */
    INT32            count;   /* Number of nodes in list     */
    INT32            max;     /* Max number of nodes in list */
}OAM_ALARM_LIST_T;

/* Function declarations */
extern OAM_ALARM_NODE_T* os_listFirst        (OAM_ALARM_LIST_T *pList);
extern OAM_ALARM_NODE_T* os_listNext         (OAM_ALARM_NODE_T *pNode);
extern OAM_ALARM_NODE_T* os_listPrevious     (OAM_ALARM_NODE_T *pNode);
extern INT32             os_listCount        (OAM_ALARM_LIST_T *pList);
extern INT32             os_listAdd          (OAM_ALARM_LIST_T *pList, OAM_ALARM_NODE_T *pNode);
extern VOID              os_listAddFirst     (OAM_ALARM_LIST_T *pList, OAM_ALARM_NODE_T *pNode);
extern VOID              os_listDelete       (OAM_ALARM_LIST_T *pList, OAM_ALARM_NODE_T *pNode);
extern OAM_ALARM_NODE_T* os_listDeleteFirst  (OAM_ALARM_LIST_T *pList);
extern VOID              os_listInit         (OAM_ALARM_LIST_T *pList, INT32 max);

#endif /*__OAM_STACK_OSSL_H__*/
