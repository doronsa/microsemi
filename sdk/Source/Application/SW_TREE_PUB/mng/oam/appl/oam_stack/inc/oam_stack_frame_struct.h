/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : PON Stack                                                 **/
/**                                                                          **/
/**  FILE        : oam_stack_frame_struct.h                                  **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM frame structure                         **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *  
 *    12/Jan/2011   - initial version created.         
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_FRAME_STRUCT_H__
#define __OAM_STACK_FRAME_STRUCT_H__

/* <OAM_EXTENDED_VER_ITEM_T> is a version item in the version list. */
#define LOCAL_OAM_ITEM_CNT 10

typedef struct
{
    UINT8 m_oui[OAM_OUI_LEN];
    UINT8 m_version;
}POS_PACKED  OAM_EXTENDED_VER_ITEM_T;

typedef struct
{
    OAM_EXTENDED_VER_ITEM_T oui_list[LOCAL_OAM_ITEM_CNT] ;
    UINT8 m_list_number ;
}POS_PACKED  OAM_EXTENDED_VER_ITEM_LIST_T;

/* <OAM_EXTENDED_INF_TLV_T> is the extended Information TLV for the Extended OAM Discovery frame, offset 50 of the Ethernet frame. */
typedef struct
{
    /* Standard OAM issue */
    UINT8 m_info_type;
    UINT8 m_length;
    UINT8 m_oui[OAM_OUI_LEN];
    
    /* CTC specific */
    UINT8 m_ext_support;
    UINT8 m_version;
    UINT8 m_ver_list[ETHERNET_MTU - OAM_ETHERNET_HEADER_SIZE - 2*OAM_ETHERNET_STD_TLV_SIZE - OAM_ETHERNET_EX_TLV_STATIC_SIZE];    /* Can be casted into <OAM_EXTENDED_VER_ITEM_T*> type */
}POS_PACKED OAM_EXTENDED_INF_TLV_T;

/* <OAM_VENDOR_EXTENSION_T> is the OAM vendor extension frame structure */
typedef struct
{
    UINT8 m_oui[OAM_OUI_LEN];
    UINT8 m_opcode;
    UINT8 m_specific_buf[ETHERNET_MTU - OAM_ETHERNET_HEADER_SIZE - OAM_ETHERNET_VEND_EXT_HEADER_SIZE]; /* This buffer contents relies on the vendor-extension definition for the frame coded by the <m_opcode> field */
}POS_PACKED OAM_VENDOR_EXTENSION_T;

/* ************************************ */
/* DBA OAM Vendor Extension definitions */
/* ************************************ */

/* Since the get_DBA_request frame is basically empty, define the offset of the two fields that do exists there, relative to the Ethernet OAM frame Data field, which is at offset 22 of the Ethernet frame */
#define GET_DBA_REQUEST_CODE_OFFSET        0
#define GET_DBA_REQUEST_Q_SET_COUNT_OFFSET 1

/* <OAM_EXTENSION_DBA_SET_REQUEST_T> is the data field of the OAM vendor extension denoting a CTC DBA set-request, starting from offset 22 of the Ethernet frame. */
typedef  struct
{
    UINT8   m_dba_code;
    UINT8   m_queue_set_count;
    UINT8   m_report_bitmap;
    UINT16  m_queue_thresh[(ETHERNET_MTU - OAM_ETHERNET_HEADER_SIZE - OAM_ETHERNET_VEND_EXT_HEADER_SIZE - 3)/2];    /* This is place for a lot of queue thresholds, but doubt if all will be used.. */
}POS_PACKED OAM_EXTENSION_DBA_SET_REQUEST_T;

/* <OAM_EXTENSION_DBA_GET_RESPONSE_T> is the data field of the OAM vendor extension denoting a CTC DBA get-response, starting from offset 22 of the Ethernet frame. */
typedef  struct
{
    UINT8   m_dba_code;
    UINT8   m_queue_set_count;
    UINT8   m_report_bitmap;
    UINT16  m_queue_thresh[(ETHERNET_MTU - OAM_ETHERNET_HEADER_SIZE - OAM_ETHERNET_VEND_EXT_HEADER_SIZE - 3)/2];    /* This is place for a lot of queue thresholds, but doubt if all will be used.. */
}POS_PACKED OAM_EXTENSION_DBA_GET_RESPONSE_T;

/* <OAM_EXTENSION_DBA_SET_RESPONSE_T> is the data field of the OAM vendor extension denoting a CTC DBA set_response, starting from offset 22 of the Ethernet frame. */
typedef  struct
{
    UINT8   m_dba_code;
    UINT8   m_ack;
    UINT8   m_queue_set_count;
    UINT8   m_report_bitmap;
    UINT16  m_queue_thresh[(ETHERNET_MTU - OAM_ETHERNET_HEADER_SIZE - OAM_ETHERNET_VEND_EXT_HEADER_SIZE - 4)/2];    /* This is place for a lot of queue thresholds, but doubt if all will be used.. */
}POS_PACKED  OAM_EXTENSION_DBA_SET_RESPONSE_T;

/* <OAM_EXTENSION_ENC_KEY_REQUEST_T> is the data field of the OAM vendor extension denoting a CTC ENC key-request, starting from offset 22 of the Ethernet frame. */
typedef  struct
{
    UINT8 m_churning_code;
    UINT8 m_key_index;
}POS_PACKED OAM_EXTENSION_ENC_KEY_REQUEST_T;

/* <OAM_EXTENSION_ENC_KEY_RESPONSE_T> is the data field of the OAM vendor extension denoting a CTC ENC new-key-response, starting from offset 22 of the Ethernet frame. */
typedef  struct
{
    UINT8 m_churning_code;
    UINT8 m_key_index;
    UINT8 m_key[3];
}   POS_PACKED OAM_EXTENSION_ENC_KEY_RESPONSE_T;
#endif /* __OAM_STACK_FRAME_STRUCT_H__ */
