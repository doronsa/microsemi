/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : oam_stack_voip_expo.h                                     **/
/**                                                                          **/
/**  DESCRIPTION : Definition of VoIP module                                 **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *    12/Jan/2011   - initial version created.               
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_VOIP_EXPO_H__
#define __OAM_STACK_VOIP_EXPO_H__

#include "oam_stack_type_expo.h"
#include "oam_stack_comm.h"

/**************************************************************************************
Leaf: C7-0x0004 - Get VoIP port count

Paramaters:
    port_count (OUT):    The number of VoIP ports available.

NOTE: The absence of this hook is permitted, and then the ONU is regarded to have no
      VoIP ports.
**************************************************************************************/
typedef STATUS (*ext_onu_voip_get_port_count_t)(UINT32 *port_count);


/**************************************************************************************
Leaf: C7-0x0013 - Get VoIP ports status

Paramaters:
       slot (IN):               The VoIP slog to get information for.
    port (IN):            The VoIP port to get information for.
    port_stat (OUT):    The information for the port.
**************************************************************************************/
typedef STATUS (*ext_onu_voip_get_port_status_t)(UINT32 slot, UINT32 port, OAM_PORT_ON_OFF_STATE_E *port_stat);


/**************************************************************************************
Leaf: C7-0x0013 - Set VoIP ports status

Paramaters:
       slot (IN):               The VoIP slog to get information for.
    port (IN):            The VoIP port to get information for.
    port_state_in (IN):    A VoIP port status to set.
**************************************************************************************/
typedef STATUS (*ext_onu_voip_set_port_admin_t)(UINT32 slot, UINT32 port, const OAM_PORT_ON_OFF_STATE_E port_state_in);


/**************************************************************************************
Leaf: c7-0x0061 - Get voip_iad_info

Paramaters:
    voip_iad_info (OUT): Get voip_iad_info
**************************************************************************************/
typedef STATUS (*ext_onu_voip_iad_info_get_t)(OAM_VOIP_IAD_INFO_T *voip_iad_info);


/**************************************************************************************
Leaf: c7-0x0062 - Get voip_global_parameter_config

Paramaters:
    voip_global_parameter_config (OUT): Get voip_global_parameter_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_global_parameter_config_get_t)(OAM_VOIP_GLOBAL_PARAM_T *voip_global_parameter_config);


/**************************************************************************************
Leaf: c7-0x0062 - Set voip_global_parameter_config

Paramaters:
    voip_global_parameter_config (IN): Set voip_global_parameter_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_global_parameter_config_set_t)(const OAM_VOIP_GLOBAL_PARAM_T *voip_global_parameter_config);


/**************************************************************************************
Leaf: c7-0x0063 - Get voip_h248_parameter_config

Paramaters:
    voip_h248_parameter_config (OUT): Get voip_h248_parameter_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_h248_parameter_config_get_t)(OAM_VOIP_H248_PARAM_T *voip_h248_parameter_config);


/**************************************************************************************
Leaf: c7-0x0063 - Set voip_h248_parameter_config

Paramaters:
    voip_h248_parameter_config (IN): Set voip_h248_parameter_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_h248_parameter_config_set_t)(const OAM_VOIP_H248_PARAM_T *voip_h248_parameter_config);


/**************************************************************************************
Leaf: c7-0x0064 - Get voip_h248_user_tid_info

Paramaters:
       slot (IN):               The VoIP slog to get information for.
    port (IN):            The VoIP port to get information for.
      voip_h248_user_tid_info (OUT): Get voip_h248_user_tid_info
**************************************************************************************/
typedef STATUS (*ext_onu_voip_h248_user_tid_get_t)
(
    UINT32                          slot,
    UINT32                          port,
    OAM_VOIP_H248_USER_TID_CONFIG_T *voip_h248_user_tid_info
);


/**************************************************************************************
Leaf: c7-0x0064 - Set voip_h248_user_tid_info

Paramaters:
       slot (IN):               The VoIP slog to get information for.
    port (IN):            The VoIP port to get information for.
       voip_h248_user_tid_info (IN): Set voip_h248_user_tid_info
**************************************************************************************/
typedef STATUS (*ext_onu_voip_h248_user_tid_set_t)
(
UINT32                                  slot,
UINT32                                  port,
const OAM_VOIP_H248_USER_TID_CONFIG_T   *voip_h248_user_tid_info);


/**************************************************************************************
Leaf: c7-0x0065 - Set voip_h248_rtp_tid_config

Paramaters:
    voip_h248_rtp_tid_config (IN): Set voip_h248_rtp_tid_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_h248_rtp_tid_config_set_t)(const OAM_VOIP_H248_RTP_TID_CONFIG_T *voip_h248_rtp_tid_config);


/**************************************************************************************
Leaf: c7-0x0065 - Get voip_h248_rtp_tid_config

Paramaters:
    voip_h248_rtp_tid_config (IN): Set voip_h248_rtp_tid_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_h248_rtp_tid_config_get_t)(OAM_VOIP_H248_RTP_TID_CONFIG_T *voip_h248_rtp_tid_config);


/**************************************************************************************
Leaf: c7-0x0066 - Get voip_h248_rtp_tid_info

Paramaters:
      voip_h248_rtp_tid_info (OUT): Get voip_h248_rtp_tid_info
**************************************************************************************/
typedef STATUS (*ext_onu_voip_h248_rtp_tid_info_get_t)( OAM_VOIP_H248_RTP_TID_INFO_T *voip_h248_rtp_tid_info);


/**************************************************************************************
Leaf: c7-0x0067 - Get voip_sip_parameter_config

Paramaters:
      voip_sip_parameter_config (OUT): Get voip_sip_parameter_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_sip_parameter_config_get_t)( OAM_VOIP_SIP_PARAM_T *voip_sip_parameter_config);


/**************************************************************************************
Leaf: c7-0x0067 - Set voip_sip_parameter_config

Paramaters:
    obj_type(IN):         The object type of this command.
    ctc_object(IN):       The VoIP object to get information for voip_sip_parameter_config.
    voip_sip_parameter_config (IN): Set voip_sip_parameter_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_sip_parameter_config_set_t)(const OAM_VOIP_SIP_PARAM_T *voip_sip_parameter_config);


/**************************************************************************************
Leaf: c7-0x0068 - Get voip_sip_user_parameter_config

Paramaters:
    obj_type(IN):         The object type of this command.
    ctc_object(IN/OUT):       The VoIP object to get information for voip_sip_user_parameter_config.
    voip_sip_user_parameter_config (OUT): Get voip_sip_user_parameter_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_sip_user_parameter_config_get_t)
(
    UINT32                    slot,
    UINT32                    port,
    OAM_VOIP_SIP_USER_PARAM_T *voip_sip_user_parameter_config
);


/**************************************************************************************
Leaf: c7-0x0068 - Set voip_sip_user_parameter_config

Paramaters:
    obj_type(IN):         The object type of this command.
    ctc_object(IN):       The VoIP object to get information for voip_sip_user_parameter_config.
    voip_sip_user_parameter_config (IN): Set voip_sip_user_parameter_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_sip_user_parameter_config_set_t)
( 
    UINT32                           slot,
    UINT32                           port,
    const OAM_VOIP_SIP_USER_PARAM_T  *voip_sip_user_parameter_config
);


/**************************************************************************************
Leaf: c7-0x0069 - Get voip_fax_config

Paramaters:
    obj_type(IN):         The object type of this command.
    ctc_object(IN/OUT):       The VoIP object to get information for voip_fax_config.
    voip_fax_config (OUT): Get voip_fax_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_fax_config_get_t)(OAM_VOIP_FAX_CONFIG_T *voip_fax_config);


/**************************************************************************************
Leaf: c7-0x0069 - Set voip_fax_config

Paramaters:
    obj_type(IN):         The object type of this command.
    ctc_object(IN):       The VoIP object to get information for voip_fax_config.
    voip_fax_config (IN): Set voip_fax_config
**************************************************************************************/
typedef STATUS (*ext_onu_voip_fax_config_set_t)(const OAM_VOIP_FAX_CONFIG_T *voip_fax_config);


/**************************************************************************************
Leaf: c7-0x006A - Get voip_iad_operation_status

Paramaters:
    obj_type(IN):         The object type of this command.
    ctc_object(IN/OUT):       The VoIP object to get information for voip_iad_operation_status.
    voip_iad_operation_status (OUT): Get voip_iad_operation_status
**************************************************************************************/
typedef STATUS (*ext_onu_voip_iad_operation_status_get_t)(OAM_VOIP_H248_OPER_STATUS_E *voip_iad_operation_status);


/**************************************************************************************
Leaf: c7-0x006B - Get voip_pots_status

Paramaters:
    obj_type(IN):         The object type of this command.
    ctc_object(IN/OUT):       The VoIP object to get information for voip_pots_status.
    voip_pots_status (OUT): Get voip_pots_status
**************************************************************************************/
typedef STATUS (*ext_onu_voip_pots_status_get_t)
(
    UINT32                  slot,
    UINT32                  port,
    OAM_VOIP_POTS_STATUS_T  *voip_pots_status
);


/**************************************************************************************
Leaf: c9-0x006C - Set voip_iad_operation

Paramaters:
    obj_type(IN):         The object type of this command.
    ctc_object(IN):       The VoIP object to get information for voip_iad_operation.
    voip_iad_operation (IN): Set voip_iad_operation
**************************************************************************************/
typedef STATUS (*ext_onu_voip_iad_operation_set_t)(const OAM_VOIP_IAD_OPER_E voip_iad_operation);


/**************************************************************************************
Leaf: c9-0x006D - Set sip_digit_map

Paramaters:
    sip_digit_map (IN): Set sip_digit_map
**************************************************************************************/
typedef STATUS (*ext_onu_voip_digit_map_set_t)(const OAM_SIP_DIGIT_MAP_T* sip_digit_map);


/* <OAM_STACK_VOIP_FUNC_HOOK_T> structure defines the hooks that are available for the application for supporting
   VoIP functionality defined by CTC Extended Variable Descriptors. */
typedef struct
{
    ext_onu_voip_get_port_count_t                 voip_get_port_count_hook;             /*Leaf C7-0x0004*/
    ext_onu_voip_get_port_status_t                voip_get_ports_status_hook;           /*Leaf C7-0x0013*/
    ext_onu_voip_set_port_admin_t                 voip_set_ports_admin_hook;            /*Leaf C7-0x0013*/
    ext_onu_voip_iad_info_get_t                   voip_iad_info_get_hook;               /*Leaf C7-0x0013*/
    ext_onu_voip_global_parameter_config_get_t    voip_global_parameter_config_get_hook;/*Leaf C7-0x0013*/
    ext_onu_voip_global_parameter_config_set_t    voip_global_parameter_config_set_hook;/*Leaf c7-0x0062*/
    ext_onu_voip_h248_parameter_config_get_t      voip_h248_parameter_config_get_hook;  /*Leaf c7-0x0063*/
    ext_onu_voip_h248_parameter_config_set_t      voip_h248_parameter_config_set_hook;  /*Leaf c7-0x0063*/
    ext_onu_voip_h248_user_tid_get_t              voip_h248_user_tid_get_hook;          /*Leaf c7-0x0064*/
    ext_onu_voip_h248_user_tid_set_t              voip_h248_user_tid_set_hook;          /*Leaf c7-0x0064*/
    ext_onu_voip_h248_rtp_tid_config_set_t        voip_h248_rtp_tid_config_set_hook;    /*Leaf c7-0x0065*/
    ext_onu_voip_h248_rtp_tid_config_get_t        voip_h248_rtp_tid_config_get_hook;    /*Leaf c7-0x0066*/
    ext_onu_voip_h248_rtp_tid_info_get_t          voip_h248_rtp_tid_info_get_hook;
    ext_onu_voip_sip_parameter_config_get_t       voip_sip_parameter_config_get_hook;   /*Leaf c7-0x0067*/
    ext_onu_voip_sip_parameter_config_set_t       voip_sip_parameter_config_set_hook;   /*Leaf c7-0x0067*/
    ext_onu_voip_sip_user_parameter_config_get_t  voip_sip_user_parameter_config_get_hook;
    ext_onu_voip_sip_user_parameter_config_set_t  voip_sip_user_parameter_config_set_hook;

    ext_onu_voip_fax_config_get_t                 voip_fax_config_get_hook;             /*Leaf c7-0x0069*/
    ext_onu_voip_fax_config_set_t                 voip_fax_config_set_hook;             /*Leaf c7-0x0069*/
    ext_onu_voip_iad_operation_status_get_t       voip_iad_operation_status_get_hook;   /*Leaf c7-0x006A*/
    ext_onu_voip_pots_status_get_t                voip_pots_status_get_hook;            /*Leaf c7-0x006B*/
    ext_onu_voip_iad_operation_set_t              voip_iad_operation_set_hook;          /*Leaf c9-0x006C*/
    ext_onu_voip_digit_map_set_t                  oam_stack_voip_sip_digit_map_set_hook;/*Leaf c9-0x006D*/
}OAM_STACK_VOIP_FUNC_HOOK_T;

#endif /*__OAM_STACK_VOIP_EXPO_H__*/
