/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : oam_stack_opcode.c                                        **/
/**                                                                          **/
/**  DESCRIPTION : Definition of eoam operation code                         **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.               
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_OPCODE_H__
#define __OAM_STACK_OPCODE_H__
#include "oam_stack_ossl.h"
#include "oam_stack_type_expo.h"

#define oam_stack_g_module_name "CTC_STACK"

/***********************************************************************************************
* oam stack parse / compose oam handler()
*
* DESCRIPTION:         This hook is used for parse and compose OAM packet, it's content is OAM specific,
*                      Please see oam_stack_parse_comp.c for reference.
*
* INPUTS:
*
* OUTPUTS:
*
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*************************************************************************************************/
typedef STATUS (*OAM_STACK_GENERAL_HANDLER_FUNC_T)();

/***********************************************************************************************
* oam_stack_ctc_reqest_get_handler()
*
* DESCRIPTION:         Parse CTC (China Telecom Corpration) requests from PON side, get the configuration,
*                      compose an answer, return it to PON side
*
* INPUTS:
* a_obj_info          - Organization specific request received from PON side. It's content is started
*                       from TLV type (Ex.0x37)to the end.
*
* OUTPUTS:
* a_out_buf           - Answers that needs to be responded to PON side 
* a_container_size    - this size is variable according to each specific OAM
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*************************************************************************************************/
typedef STATUS (*OAM_STACK_REQ_GET_HANDLER_FUNC_T)(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size); 


/***********************************************************************************************
* oam_stack_ctc_reqest_set_handler()
*
* DESCRIPTION:         Parse CTC specific requests from PON side, apply the configuration,
*                      compose an answer, return it to PON side
*
* INPUTS:
* a_obj_info          - Organization specific request received from PON side. It's content is started
*                       from TLV type (Ex.0x37)to the end.
* a_in_buf            - configuration info, this content is from OAM parse function
*                       (it's location is after Branch/leaf/Variable width)
*
* OUTPUTS:
* None 
*
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*************************************************************************************************/
typedef STATUS (*OAM_STACK_REQ_SET_HANDLER_FUNC_T)(OAM_OBJECT_TYPE_T *a_obj_info , VOID *a_in_buf);

/*OUI hooks format that saved in system*/
typedef struct
{
    UINT32                        custom_oui_integer;
    OAM_CUSTOM_OUI_RECEIVE_HOOK_T oam_receive_handle; 
    BOOL                          valid ;
}OAM_CUSTOM_OUI_HOOK_T;

/* <OAM_OPCODE_HANDLER_FUNC_T> registers the functions handling parsing, composing, getting-values & setting-values (which also represents actions). */
/* Function hooks may be null */
typedef struct
{
    UINT8                            m_branch;
    UINT16                           m_leaf;          /* The leaf representing the functions                        */
    OAM_STACK_REQ_GET_HANDLER_FUNC_T m_get_func;      /* The function to handle 'Get Attribute'                     */
    OAM_STACK_REQ_SET_HANDLER_FUNC_T m_set_func;      /* The function to handle 'Set Attribute' or 'Execute Action' */
    OAM_STACK_GENERAL_HANDLER_FUNC_T m_compose_func;  /* The function to handle 'Compose payload'                   */
    OAM_STACK_GENERAL_HANDLER_FUNC_T m_parse_func;    /* The function to handle 'Parse data'                        */
    OAM_STACK_GENERAL_HANDLER_FUNC_T m_preparse_func; /* The function to handle 'Parse data'                        */
}POS_PACKED  OAM_OPCODE_HANDLER_FUNC_T;

INT16  oam_stack_unknown_command_process_function
( 
    const UINT8             opcode,             /* OAM_VENDOR_EXT_CTC_VAR_REQUEST, OAM_VENDOR_EXT_SET_REQUEST or */
    const UINT8             branch,
    const UINT16            leaf,
    const OAM_MNG_OBJECT_T  *object_info,       /* Port */
    const UINT16            input_length,       
    const UINT8             *input_raw_buffer, 
    UINT16                  *output_length,     
    UINT8                   *output_raw_buffer   
);

INT16 oam_stack_compose_comnon_response
(
    const UINT8  branch,
    const UINT16 leaf,
    const UINT8  ret_val,
    UINT8        *send_buffer,
    UINT16       *send_buf_length
);

STATUS oam_stack_compose_object_context(UINT8 *a_buffer, UINT16 *a_buffer_size, OAM_MNG_OBJECT_T *object_info);
OAM_MNG_OBJECT_TLV_TYPE_E oam_stack_check_branch_tlv_valid(UINT32 opcode);
STATUS oam_stack_prepare_object_info(UINT8 *a_buffer, const UINT16 a_buffer_size, OAM_MNG_OBJECT_T *a_obj_type);

STATUS oam_stack_check_object_branch_valid(UINT8 l_branch);

UINT16 oam_stack_mpcp_max_negotiated_oampdu(VOID);

/*---------------------------------------------------------------------------------------------*/
/*COMMON EXT HOOK  API CALLS VIEW*/    
/*---------------------------------------------------------------------------------------------*/
STATUS oam_stack_req_get_onu_serialnum_state(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_chipsetID_info(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_Firmware_info(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_capabilities(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_eth_link_state(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_eth_port_policing(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_eth_port_pause(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_eth_port_policing(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_voip_port_admin(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_fast_leave_ability(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_multicast_group_count(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_e1_port_admin(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_eth_port_ds_rate_limit(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_eth_vlan_mode(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_class_n_mark(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_eth_multicast_switch(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_eth_multicast_control(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_eth_multicast_vlan(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_auto_neg_admin_state(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_eth_multicast_tag_strip(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_auto_neg_local_technology_ability(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_auto_neg_advertised_technology_ability(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_phy_admin_state(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_fast_leave_admin_state(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_fec_mode(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_fec_ability(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
/*---------------------------------------------------------------------------------------------*/
/*FOR CTC V2.1 EXT HOOK API CALLS VIEW*/    
/*---------------------------------------------------------------------------------------------*/
STATUS oam_stack_req_get_onu_pon_if_active(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_capabilities_plus_3(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_pwr_saving_capabilities(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_pwr_saving_config(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_protect_para(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_capabilities_plus(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_port_mac_aging_time(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_optical_transceiver_diagnosis(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_sla_service(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_llid_queue_config(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_hold_over(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_MuxMng_GlobalPara(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_MuxMng_SnmpPara(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_pon_multi_llid_admin(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_alarm_admin(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_alarm_threshold(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_pm_status(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_pm_current(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_pm_history(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_get_onu_portLoop_detect(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);

/*-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------*/
STATUS oam_stack_req_set_phy_admin_state(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_voip_port_admin(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_out_buf);
STATUS oam_stack_req_set_fec_mode(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_eth_port_policing(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_out_buf);
STATUS oam_stack_req_set_fast_leave_admin_control(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_multicast_group_count(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_e1_port_admin(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_out_buf);
STATUS oam_stack_req_set_onu_alarm_admin(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_eth_port_ds_rate_limit(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_out_buf);
STATUS oam_stack_req_set_class_n_mark(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_out_buf);
STATUS oam_stack_req_set_eth_multicast_tag_strip(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_eth_multicast_switch(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_eth_vlan_mode(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_out_buf);
STATUS oam_stack_req_set_eth_multicast_control(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_eth_multicast_vlan(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_out_buf);
STATUS oam_stack_req_set_onu_llid_queue_config(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_reset_card(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_pon_multi_llid_amdin_ctrl(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);

/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/
STATUS oam_stack_req_set_onu_sla_service(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_out_buf);
STATUS oam_stack_req_set_pon_if_active_config(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_eth_port_policing(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_out_buf);
STATUS oam_stack_req_action_reset(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_action_auto_neg_restart_auto_config(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_action_auto_neg_admin_control(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_eth_port_pause(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_out_buf);
STATUS oam_stack_req_set_onu_holdover_config(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_llid_queue_config(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_MuxMng_GlobalPara(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_MuxMng_SnmpPara(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_alarm_admin(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_alarm_threshold(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_pm_status(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_pm_current(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_portLoop_detect(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_tx_power_ctl(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_pwr_saving_config(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_protect_para(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_port_mac_aging_time(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_port_disable_looped(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_set_onu_sleep_control(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);

/*VoIP*/
STATUS oam_stack_req_voip_iad_info_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_iad_info_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_global_parameter_configure_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_global_parameter_configure_set(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_voip_h248_parameter_configure_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_h248_parameter_configure_set(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_voip_h248_tid_configure_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_h248_tid_configure_set(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_voip_rtp_tid_configure_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_rtp_tid_configure_set(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_voip_rtp_tid_info_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_sip_parameter_config_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_sip_parameter_config_set(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_voip_sip_user_parameter_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_sip_user_parameter_config_set(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_voip_fax_config_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_fax_config_set(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_voip_h248_operation_state_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_pots_state_get(VOID *a_out_buf, OAM_OBJECT_TYPE_T *a_obj_info, UINT16 *a_container_size);
STATUS oam_stack_req_voip_iad_operate(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);
STATUS oam_stack_req_oam_stack_voip_sip_digit_map_configre(OAM_OBJECT_TYPE_T *a_obj_info, VOID *a_in_buf);

/*---------------------------------------------------------------------------------------------*/
/*COMMON COMPOSE&PARSE API CALLS VIEW*/    
/*---------------------------------------------------------------------------------------------*/
INT16 oam_stack_compose_onu_identifier
(
    const OAM_ONU_SERIAL_NUM_T  *serial_number,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_onu_identifier
(
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_ONU_SERIAL_NUM_T        *onu_identifier,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_chipset_id
(
    const OAM_CHIPSET_ID_T      *chipset_id,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_chipset_id
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_CHIPSET_ID_T            *chipset_id,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_parse_firmware_version
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_FIRMWARE_VERSION_T      *firmware,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_firmware_version
(  
    const OAM_FIRMWARE_VERSION_T *version,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_onu_capabilities
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_ONU_CAPABILITIES_T      *onu_capabilities,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_onu_capabilities
( 
    const OAM_ONU_CAPABILITIES_T *onu_capabilities,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);


INT16 oam_stack_parse_onu_capabilities_plus
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_ONU_CAPABILITIES_PLUS_T  *onu_capabilities_2,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_onu_capabilities_plus
( 
    const OAM_ONU_CAPABILITIES_PLUS_T *onu_capabilities,
    const UINT8                       response_ret_value,
    UINT8                             *send_buffer,
    UINT16                            *send_buf_length
);

INT16 oam_stack_parse_onu_capabilities_plus_3
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_ONU_CAPABILITIES_3_T     *cap,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_onu_capabilities_3
( 
    const OAM_ONU_CAPABILITIES_3_T  *cap,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_compose_onu_pwr_saving_capabilities
( 
    const OAM_ONU_PWR_SAVING_CAP_T  *cap,
    const UINT8                     response_ret_value,
    UINT8                           *send_buffer,
    UINT16                          *send_buf_length
);

INT16 oam_stack_parse_onu_pwr_saving_config
( 
    UINT8                        *recv_buffer,
    const UINT16                  recv_buffer_size,
    OAM_ONU_PWR_SAVING_CFG_T     *cfg,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_onu_pwr_saving_config
( 
    const OAM_ONU_PWR_SAVING_CFG_T  *cfg,
    const UINT8                      response_ret_value,
    UINT8                           *send_buffer,
    UINT16                          *send_buf_length
);

INT16 oam_stack_parse_onu_protect_para
( 
    UINT8                        *recv_buffer,
    const UINT16                  recv_buffer_size,
    OAM_ONU_PROTECT_PARA_T       *cfg,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_onu_protect_para
( 
    const OAM_ONU_PROTECT_PARA_T    *cfg,
    const UINT8                      response_ret_value,
    UINT8                           *send_buffer,
    UINT16                          *send_buf_length
);

INT16 oam_stack_parse_port_mac_aging_time
( 
    UINT8                        *recv_buffer,
    const UINT16                  recv_buffer_size,
    UINT32                       *aging_time,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_port_mac_aging_time
( 
    const UINT32                    *aging_time,
    const UINT8                      response_ret_value,
    UINT8                           *send_buffer,
    UINT16                          *send_buf_length
);

INT16 oam_stack_parse_sleep_control
( 
    UINT8                        *recv_buffer,
    const UINT16                  recv_buffer_size,
    OAM_ONU_SLEEP_CONTROL_T      *sleep_control,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_parse_port_disable_looped
( 
    UINT8                             *recv_buffer,
    const UINT16                       recv_buffer_size,
    OAM_PORT_DISABLE_LOOPED_STATE_E   *disable_looped_state,
    UINT8                             *ctc_ret_val,
    UINT16                            *received_buf_length
);

INT16 oam_stack_parse_optical_transceiver_diagnosis
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PON_OPTICAL_TRANSCEIVER_DIAGNOSIS_T  *optical_transceiver_diagnosis,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_optical_transceiver_diagnosis
( 
    const OAM_PON_OPTICAL_TRANSCEIVER_DIAGNOSIS_T  *result,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_muxmng_global_para
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_MNG_GLOBAL_PARAM_T       *para,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_muxmng_global_para
( 
    const OAM_MNG_GLOBAL_PARAM_T *para,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_muxmng_snmp_para
(
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_MNG_SNMP_PARAM_CONF_T    *para,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_muxmng_snmp_para
(
    const OAM_MNG_SNMP_PARAM_CONF_T *para,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_compose_sla_service
( 
    const OAM_PON_SERVICE_SLA_T  *service_sla,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_service_sla
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PON_SERVICE_SLA_T        *service_sla,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_parse_llid_queue
( 
    UINT8                        *recv_buffer,
    UINT16                       recv_buffer_size,
    OAM_PON_LLID_QUEUE_CONFIG_T  *llid_queue,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_llid_queue
( 
    const OAM_PON_LLID_QUEUE_CONFIG_T  *llid_queue,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_compose_holdover
( 
    const OAM_PON_HOLDOVER_STATE_T  *hold_over,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16  oam_stack_parse_holdover
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PON_HOLDOVER_STATE_T     *hold_over,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_parse_multi_llid_admin
( 
    UINT8                        *recv_buffer,
    UINT16                       recv_buffer_size,
    UINT32                       *multi_llid_number,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_multi_llid_admin
(
    const UINT32                 *multi_llid_number,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_muxmng_global_para
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_MNG_GLOBAL_PARAM_T       *mng_para,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_muxmng_global_para
( 
    const OAM_MNG_GLOBAL_PARAM_T *mng_para,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT32 oam_stack_parse_reset_card
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    UINT8                        *reset,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16  oam_stack_parse_pon_if_active
(
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PON_ACTIVE_IF_STATE_T    *pon_if_active,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_pon_if_active
(  
    OAM_PON_ACTIVE_IF_STATE_T    *pon_if_active,
    UINT8                        ponse_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_ethernet_port_link_state
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PORT_LINK_STATE_E        *link_state,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_ethrnet_port_link_state
( 
    OAM_PORT_LINK_STATE_E        *link_state,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *end_buf_length
);

INT16 oam_stack_parse_ethernet_port_pause
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PORT_PAUSE_STATE_E       *flow_control_enable,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_ethrnet_port_pause
( 
    OAM_PORT_PAUSE_STATE_E       *flow_control_enable,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_port_loopback_detect
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PORT_LOOPBACK_DETECTION_ADMIN_E  *loop_detect_enable,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_parse_tx_power_ctl
(
    UINT8                                 *recv_buffer,
    const UINT16                          recv_buffer_size,
    OAM_POWER_TX_CTL_T                    *para,
    UINT16                                *received_buf_length
);
                  
INT16 oam_stack_compose_port_loopback_detect
( 
    const OAM_PORT_ON_OFF_STATE_E *port_state,
    const UINT8                   response_ret_value,
    UINT8                         *send_buffer,
    UINT16                        *send_buf_length    
);

INT16 oam_stack_parse_ethernet_port_policing
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PORT_US_POLICING_ENTRY_T *ports_info,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_ethrnet_port_policing
( 
    const OAM_PORT_US_POLICING_ENTRY_T  *ports_info,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_compose_voip_port
( 
    const OAM_PORT_ON_OFF_STATE_E  *port_state,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_voip_port
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PORT_ON_OFF_STATE_E      *port_state,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_e1_port
( 
    const OAM_PORT_ON_OFF_STATE_E  *port_state,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_e1_port
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PORT_ON_OFF_STATE_E      *port_state,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_parse_ethernet_port_ds_rate_limit
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PORT_DS_RATE_LIMITING_ENTRY_T  *ports_info,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_ethrnet_port_ds_rate_limit
( 
    OAM_PORT_DS_RATE_LIMITING_ENTRY_T  *ports_info,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_vlan
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_ETH_PORT_VLAN_CONF_T     *ports_info,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_vlan
( 
    const OAM_ETH_PORT_VLAN_CONF_T  *ports_info,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_classification_n_marking
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_CLASS_N_MARK_T           *ports_info,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_classification_n_marking
( 
    const OAM_CLASS_N_MARK_T     *ports_info,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_compose_multicast_control
( 
    const OAM_MC_CTRL_T          *ports_info,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_multicast_control
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_MC_CTRL_T                *ports_info,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_parse_multicast_vlan
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_MC_VLAN_T                *ports_info,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);
                  
INT16 oam_stack_compose_onu_multicast_vlan
( 
    const OAM_MC_VLAN_T          *ports_info,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);
                  
INT16 oam_stack_compose_olt_multicast_vlan
( 
    const OAM_MC_VLAN_T          *ports_info,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_auto_negotiation_admin_state
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PORT_NEGA_STATE_E        *state,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_auto_negotiation_admin_state
( 
    const OAM_PORT_NEGA_STATE_E  *state,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_compose_auto_neg_local_technology_ability
(
    const OAM_PORT_AUTO_NEGA_TECH_ABILITY_T  *local_technology,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_auto_neg_local_technology_ability
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PORT_AUTO_NEGA_TECH_ABILITY_T  *local_technology,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_parse_auto_neg_advertised_technology_ability
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PORT_AUTO_NEGA_TECH_ABILITY_T  *local_technology,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_auto_neg_advertised_technology_ability
(  
    const OAM_PORT_AUTO_NEGA_TECH_ABILITY_T  *local_technology,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_fec_ability
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PON_STD_FEC_ABILITY_E    *ability,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_fec_ability
( 
    const OAM_PON_STD_FEC_ABILITY_E  *ability,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT16 oam_stack_parse_fec_mode
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_PON_STD_FEC_MODE_E      *mode,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_fec_mode
(
    const OAM_PON_STD_FEC_MODE_E *mode,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_phy_admin_control
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_PORT_ADMIN_E            *state,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_phy_admin_control
( 
    const OAM_PORT_ADMIN_E      *state,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_auto_negotiation_restart_auto_config
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    VOID                        *dummy,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_auto_negotiation_restart_auto_config
( 
    const VOID                  *state,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_compose_auto_negotiation_admin_control
( 
    const OAM_PORT_ACTION_E     *state,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_auto_negotiation_admin_control
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_PORT_ACTION_E           *state,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_multicast_tag_strip
( 
    const OAM_MC_PORT_TAG_OPER_T *tag_oper,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_multicast_tag_strip
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_MC_PORT_TAG_OPER_T*     multicast_vlan_switching,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_fast_leave_ability
( 
    const OAM_MC_FAST_LEAVE_ABILITY_T  *ability,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_compose_fast_leave_admin_state
(  
    const OAM_FAST_LEAVE_ADMIN_STATE_E *state,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_fast_leave_admin_control
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_FAST_LEAVE_ADMIN_STATE_E  *state,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_parse_multicast_switch
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_MC_PROTOCOL_TYPE_E      *multicast_protocol,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_multicast_switch
( 
    const OAM_MC_PROTOCOL_TYPE_E  *multicast_protocol,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT32 oam_stack_parse_multicast_group
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    UINT8                       *group_num,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT32 oam_stack_compose_multicast_group
( 
    const UINT8                 *group_num,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT32 oam_stack_parse_reset
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    UINT8                        *reset,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT16 oam_stack_compose_iad_info
( 
    const OAM_VOIP_IAD_INFO_T    *iad_info,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT32 oam_stack_parse_voip_global_param_configrue
( 
    UINT8                        *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_VOIP_GLOBAL_PARAM_T      *global_param,
    UINT8                        *ctc_ret_val,
    UINT16                       *received_buf_length
);

INT32 oam_stack_compose_voip_global_param_configrue
( 
    const OAM_VOIP_GLOBAL_PARAM_T global_param,
    const UINT8                  response_ret_value,
    UINT8                        *send_buffer,
    UINT16                       *send_buf_length
);

INT32 oam_stack_parse_h248_param_config
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_VOIP_H248_PARAM_T       *h248_param,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT32 oam_stack_compose_h248_param_config
( 
    OAM_VOIP_H248_PARAM_T       *h248_param,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT32 oam_stack_parse_h248_user_tid_config
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_VOIP_H248_USER_TID_CONFIG_T  *h248_user_tid,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT32 oam_stack_compose_h248_user_tid_config
( 
    OAM_VOIP_H248_USER_TID_CONFIG_T  *h248_user_tid,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT32 oam_stack_parse_h248_rtp_tid_info
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_VOIP_H248_RTP_TID_INFO_T  *h248_rtp_tid,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT32 oam_stack_compose_h248_rtp_tid_config
( 
    const OAM_VOIP_H248_RTP_TID_CONFIG_T h248_rtp_tid_config,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT32 oam_stack_parse_h248_rtp_tid_config
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_VOIP_H248_RTP_TID_CONFIG_T  *h248_rtp_tid,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT32 oam_stack_parse_h248_user_tid_config
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_VOIP_H248_USER_TID_CONFIG_T  *h248_user_tid,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT32 oam_stack_parse_h248_rtp_tid_info
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_VOIP_H248_RTP_TID_INFO_T *h248_rtp_tid,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT32 oam_stack_compose_h248_rtp_tid_info
( 
    const OAM_VOIP_H248_RTP_TID_INFO_T *h248_rtp_tid_info,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT32 oam_stack_parse_sip_param_config
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_VOIP_SIP_PARAM_T        *sip_param,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT32 oam_stack_compose_sip_param_config
( 
    const OAM_VOIP_SIP_PARAM_T  *sip_param,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);


INT32 oam_stack_compose_voip_fax_config
( 
    const OAM_VOIP_FAX_CONFIG_T *voip_fax,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);


INT32 oam_stack_parse_voip_fax_config
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_VOIP_FAX_CONFIG_T       *voip_fax,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT32 oam_stack_parse_voip_iad_oper_status
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_VOIP_H248_OPER_STATUS_E *iad_oper_status,
    UINT8                      *ctc_ret_val,
    UINT16                     *received_buf_length
);

INT32 oam_stack_compose_voip_iad_oper_status
(
    const OAM_VOIP_H248_OPER_STATUS_E  *iad_oper_status,
    const UINT8                response_ret_value,
    UINT8                      *send_buffer,
    UINT16                     *send_buf_length
);

INT32 oam_stack_parse_voip_pots_status
( 
    UINT8                      *recv_buffer,
    const UINT16               recv_buffer_size,
    OAM_VOIP_POTS_STATUS_T     *pots_status,
    UINT8                      *ctc_ret_val,
    UINT16                     *received_buf_length
);

INT32 oam_stack_compose_voip_pots_status
( 
    const OAM_VOIP_POTS_STATUS_T *pots_status,
    const UINT8                response_ret_value,
    UINT8                      *send_buffer,
    UINT16                     *send_buf_length
);

INT32 oam_stack_parse_voip_iad_operate
( 
    UINT8                      *recv_buffer,
    const UINT16               recv_buffer_size,
    OAM_VOIP_IAD_OPER_E        *iad_operate,
    UINT8                      *ctc_ret_val,
    UINT16                     *received_buf_length
);

INT32 oam_stack_parse_sip_digit_map
( 
    UINT8                      *recv_buffer,
    const UINT16               recv_buffer_size,
    OAM_SIP_DIGIT_MAP_UNIT_T   *sip_digit_map,
    UINT8                      *ctc_ret_val,
    UINT16                     *received_buf_length
);

INT16 oam_stack_compose_alarm_admin_state
(  
    const OAM_ALARM_ADMIN_STATE_T *alarm_info,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_alarm_admin_state
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_ALARM_ADMIN_STATE_T     *alarm_config,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_alarm_threshold
(  
    const OAM_ALARM_THRESHOLD_T *alarm_info,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_alarm_threshold
( 
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_ALARM_THRESHOLD_T       *alarm_config,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_pm_status
(
    const OAM_PM_STATE_T        *pm_state,
    const UINT8                  response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_pm_status
(
    UINT8                       *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PM_STATE_T              *pm_state,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_pm_data_current
(
    const OAM_PM_DATA_T         *pm_data,
    const UINT8                  response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_pm_data_current
(
    UINT8                       *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PM_DATA_T               *pm_data,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT16 oam_stack_compose_pm_data_history
(
    const OAM_PM_DATA_T         *pm_data,
    const UINT8                  response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

INT16 oam_stack_parse_pm_data_history
(
    UINT8                       *recv_buffer,
    const UINT16                 recv_buffer_size,
    OAM_PM_DATA_T               *pm_data,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT32 oam_stack_parse_sip_user_param_config
(
    UINT8                       *recv_buffer,
    const UINT16                recv_buffer_size,
    OAM_VOIP_SIP_USER_PARAM_T   *sip_user_param,
    UINT8                       *ctc_ret_val,
    UINT16                      *received_buf_length
);

INT32 oam_stack_compose_sip_user_param_config
(
    OAM_VOIP_SIP_USER_PARAM_T   *sip_user_param,
    const UINT8                 response_ret_value,
    UINT8                       *send_buffer,
    UINT16                      *send_buf_length
);

STATUS oam_stack_alarm_oam_get_admin_request_access(OAM_OBJECT_TYPE_T *a_obj_info,VOID *a_out_buf);

STATUS oam_stack_alarm_oam_set_admin_request_access(OAM_OBJECT_TYPE_T *a_obj_info,VOID *a_out_buf);

STATUS oam_stack_alarm_oam_get_threshold_request_access(OAM_OBJECT_TYPE_T *a_obj_info,VOID *a_out_buf);

STATUS oam_stack_alarm_oam_set_threshold_request_access(OAM_OBJECT_TYPE_T *a_obj_info,VOID *a_out_buf);


#endif  /* __OAM_STACK_OPCODE_H__ */
