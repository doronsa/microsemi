/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_stack_para.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM XML parameter initialization                                   **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    2/May/2012   - initial version created.         
 *                                                                              
 ******************************************************************************/
 
#ifndef __OAM_STACK_PARA_H__
#define __OAM_STACK_PARA_H__

#define US_RC_OK           0
#define US_RC_FAIL         1
#define US_RC_NOT_FOUND    2

#define OAM_US_XML_CFG_FILE            "/etc/xml_params/oam_xml_cfg_file.xml"
#define US_XML_CFG_FILE_TPM            "/etc/xml_params/tpm_xml_cfg_file.xml"

#define OAM_US_XML_EPON_CONNECT_E      "EPON_connect_tpm"
#define OAM_DEBUG_LEVEL_E              "oam_debug_level"
#define OAM_US_XML_OAM_RXQ_E           "OAM_RX_queue"
#define OAM_US_XML_OAM_TXQ_E           "OAM_TX_queue"
#define OAM_US_XML_EPON_LLID_E         "LLID"
#define OAM_US_XML_TPM_E               "TPM"
#define OAM_US_XML_VLAN_FILTER_TPID_E  "vlan_filter_tpid"
#define OAM_US_XML_FILTER_TPID_E       "filter_tpid"
#define OAM_US_XML_MOD_TPID_E          "mod_tpid"
#define OAM_US_XML_VLANMOD_TPID_E      "vlan_mod_tpid"
#define OAM_US_XML_TYPE_ATTR           "type"

#define OAM_ONU_VENDOR                 "oam_onu_vendor"
#define OAM_ONU_MODEL                  "oam_onu_model"
#define OAM_ONU_ID                     "oam_onu_id"
#define OAM_ONU_HW_VER                 "oam_hard_ver"
#define OAM_ONU_SW_VER                 "oam_soft_ver"
#define OAM_SOFTVER_FROM_XML_PARA      "oam_softver_from_xml"

#define OAM_ONU_FIRMVER                "oam_onu_firmver"

#define OAM_CHIPSET_ID                 "oam_chipset_id"
#define OAM_CHIPSET_MODEL              "oam_chipset_model"
#define OAM_CHIPSET_REV                "oam_chipset_revision"
#define OAM_CHIPSET_ICDATE             "oam_chipset_icdate"

#define OAM_CAP_SERVICE_SUPPORT        "oam_service_support"
#define OAM_CAP_GE_SUPPORT             "oam_ge_support"
#define OAM_CAP_FE_SUPPORT             "oam_fe_support"
#define OAM_CAP_VOIP_SUPPORT           "oam_voip_support"
#define OAM_CAP_TDM_SUPPORT            "oam_tdm_support"
#define OAM_CAP_GE_NUMBER              "oam_ge_number"
#define OAM_CAP_GE_BITMAP_MSB          "oam_ge_bitmap_msb"
#define OAM_CAP_GE_BITMAP_LSB          "oam_ge_bitmap_lsb"
#define OAM_CAP_FE_NUMBER              "oam_fe_number"
#define OAM_CAP_FE_BITMAP_MSB          "oam_fe_bitmap_msb"
#define OAM_CAP_FE_BITMAP_LSB          "oam_fe_bitmap_lsb"
#define OAM_CAP_POTS_NUMBER            "oam_pots_number"
#define OAM_CAP_USQ_NUMBER             "oam_usq_number"
#define OAM_CAP_USQ_NUM_PORT           "oam_max_usq_number_port"
#define OAM_CAP_DSQ_NUMBER             "oam_dsq_number"
#define OAM_CAP_DSQ_NUM_PORT           "oam_max_dsq_number_port"
#define OAM_CAP_BATT_BACKUP            "oam_battery_backup"

#define OAM_CAP_ONU_TYPE               "oam_onu_type"
#define OAM_CAP_ONU_LLID_NUM           "oam_onu_llid_num"
#define OAM_CAP_PROT_TYPE              "oam_protection_type"
#define OAM_CAP_IPV6_SUPPORT           "oam_ipv6_support"
#define OAM_CAP_POWER_CTRL             "oam_power_ctrl"

#define OAM_SLEEP_MODE_CAP             "sleep_mode_cap"
#define OAM_EARLY_WAKEUP_CAP           "early_wakeup_cap"
#define OAM_EARLY_WAKEUP_STATE         "early_wakeup_state"
#define OAM_SLEEP_DURATION_MAX         "sleep_duration_max"
#define OAM_LOS_OPTICAL_TIME           "los_optical_time"
#define OAM_LOS_MAC_TIME               "los_mac_time"
#define OAM_SLEEP_DURATION             "sleep_duration"
#define OAM_WAIT_DURATION              "wait_duration"
#define OAM_SLEEP_FLAG                 "sleep_flag"
#define OAM_SLEEP_MODE                 "sleep_mode"

#define OAM_SLA_OP                     "oam_service_sla_op"
#define OAM_SLA_SCH                    "oam_service_sla_sch"
#define OAM_HOLDOVER_STATE             "oam_hldover_state"
#define OAM_HOLDOVER_TIME              "oam_hldover_time"

#define OAM_VOIP_POTS1                 "oam_voip_pots1"
#define OAM_VOIP_POTS2                 "oam_voip_pots2"

#define OAM_SWITCH_STATE               "switch_enable"

#define EPON_IGMP_SUPPORT              "epon_igmp_support"
#define EPON_SILENCE_MODE              "epon_silence_mode"

#define OAM_CAPTURE_SUPPORT            "oam_capture_support"
#define OAM_CAPTURE_SKIP_ALIVE         "oam_capture_skip_alive"
#define OAM_CAPTURE_LINE               "oam_capture_line"

#define OAM_LOOPDETECT_UNI0            "oam_loopdetect_UNI0"
#define OAM_LOOPDETECT_UNI1            "oam_loopdetect_UNI1"
#define OAM_LOOPDETECT_UNI2            "oam_loopdetect_UNI2"
#define OAM_LOOPDETECT_UNI3            "oam_loopdetect_UNI3"

#define OAM_MULTICAST_SWITCH           "oam_multicast_switch"

#define OAM_MULTI_MAX_GROUP_0          "multi_group_num_0"
#define OAM_MULTI_MAX_GROUP_1          "multi_group_num_1"
#define OAM_MULTI_MAX_GROUP_2          "multi_group_num_2"
#define OAM_MULTI_MAX_GROUP_3          "multi_group_num_3"

#define OAM_VLAN_TPID                  "oam_vlan_tpid"
#define OAM_VLAN_CFI                   "oam_vlan_cfi"
#define OAM_VLAN_PRI                   "oam_vlan_pri"

#define OAM_HWF_QUEUE_LLID0            "hwf_q_llid0"
#define OAM_HWF_QUEUE_LLID1            "hwf_q_llid1"
#define OAM_HWF_QUEUE_LLID2            "hwf_q_llid2"
#define OAM_HWF_QUEUE_LLID3            "hwf_q_llid3"
#define OAM_HWF_QUEUE_LLID4            "hwf_q_llid4"
#define OAM_HWF_QUEUE_LLID5            "hwf_q_llid5"
#define OAM_HWF_QUEUE_LLID6            "hwf_q_llid6"
#define OAM_HWF_QUEUE_LLID7            "hwf_q_llid7"


#define OAM_US_XML_EPON_US_QUEUE_E      "EPON_us_queue"
#define OAM_US_XML_EPON_DS_QUEUE_E      "EPON_ds_queue"

#define OAM_CTC_DISCOVERY_ENABLE        "ctc_discovery_enable"
#define OAMPDU_SIZE_MAX                 "oampdu_size_max"
#define OAM_LOCAL_OUI                   "oam_local_oui"
#define OAM_VENDOR_SPECIFIC             "vendor_specific"
#define OAM_INFO_TLV_REVISION           "tlv_revision"
#define OAM_LOOPBACK_VID                "oam_loopback_vid"
#define OAM_LOOPBACK_PRI                "oam_loopback_pri"

#define OAM_SN_XML_E                   "ONU_SN"
#define OAM_FIRMWARE_XML_E             "ONU_FIRMWARE"
#define OAM_CHIP_XML_E                 "ONU_CHIPSET"
#define OAM_CAP_XML_E                  "ONU_CAP1"
#define OAM_CAP1_XML_E                 "ONU_CAP2"
#define OAM_MISC_XML_E                 "ONU_MISC"
#define OAM_ONU_PWR_XML_E              "ONU_PWR"
#define OAM_LOOPDETEC_XML_E            "ONU_LOOPDETECT"
#define OAM_MULTICAST_XML_E            "ONU_MULTICAST"
#define OAM_RXTX_QUEUE_XML_E           "ONU_OAM_QUEUE"
#define OAM_HWF_USQUEUE_XML_E          "ONU_HWF_USQUEUE"
#define OAM_HWF_DSQUEUE_XML_E          "ONU_HWF_DSQUEUE"
#define OAM_VIRT_QUEUE_XML_E           "ONU_VIRT_QUEUE"
#define OAM_CONFIG_XML_E               "OAM_CONFIG"

#define US_XML_ID_ATTR             "id"
#define US_XML_NUM_ATTR            "num"
#define US_XML_EPON_LLID_E         "LLID"
#define US_XML_UNI_E               "port"
#define US_XML_GROUP_NUM_ATTR      "groupnum"
#define US_XML_LOOPDETECT_ATTR     "loopdetect"

#define US_XML_PORT_INIT_E         "port_init"
#define US_XML_SIZE_ATTR           "size"
#define US_XML_TX_MOD_PARAMS_E     "tx_module_parameters"
#define US_XML_TX_MOD_E            "tx_mod"
#define US_XML_QUEUE_MAP_E         "queue_map"
#define US_XML_QUEUE_E             "queue"
#define US_XML_QUEUE_SHED_ATTR     "sched_method"
#define US_XML_QUEUE_OWNER_ATTR    "owner"
#define US_XML_QUEUE_OW_Q_NUM_ATTR "owner_q_num"
#define US_XML_QUEUE_WEIGHT_ATTR   "weight"

/*default value definition*/

#define OAM_SFU_VENDOR_ID           "MRVL"
#define OAM_SFU_MODULE_ID           "6510"
#define OAM_SFU_HARDWARE_VERSION    "M88F6510"
#define OAM_SFU_SOFTWARE_VERSION    "2.5.25RC60"
#define OAM_SOFTVER_NOT_FROM_XML    0
#define OAM_SOFTVER_FROM_XML        1
#define OAM_SFU_FIRMWARE_VERSION    {0x12, 0x34, 0x56, 0x78}
#define OAM_SFU_CHIP_ID             {0x00, 0x88}
#define OAM_SFU_CHIP_VERSION        {0xa, 0x8, 0x1a}
#define OAM_SFU_CHIP_MODEL          0x6510
#define OAM_SFU_CHIP_REV            0x2
#define OAM_SFU_SN                  "MRVL_SN"
#define OAM_ONU_FIRMWARE_VERSION    0x000209
#define OAM_SERVICE_GE_FE_VOIP      0x7
#define OAM_SFU_GE_PORT_NUM_MIN     0
#define OAM_SFU_GE_PORT_NUM_MAX     4
#define OAM_SFU_GE_PORT_NUM_DEFAULT 1
#define OAM_SFU_FE_PORT_NUM_MIN     0
#define OAM_SFU_FE_PORT_NUM_MAX     4
#define OAM_SFU_FE_PORT_NUM_DEFAULT 3

#define OAM_GE_PORT_BITMAP_LSB_DEF  0x8
#define OAM_GE_PORT_BITMAP_LSB_MAX  0xf
#define OAM_GE_PORT_BITMAP_MSB_DEF  0x0
#define OAM_GE_PORT_BITMAP_MSB_MAX  0xf

#define OAM_FE_PORT_BITMAP_LSB_DEF  0x7
#define OAM_FE_PORT_BITMAP_LSB_MAX  0xf
#define OAM_FE_PORT_BITMAP_MSB_DEF  0x0
#define OAM_FE_PORT_BITMAP_MSB_MAX  0xf

#define OAM_SFU_POTS_PORT_NUM_MAX   2
#define OAM_SFU_POTS_PORT_NUM_MIN   0
#define OAM_SFU_E1_PORT_NUM         0
#define OAM_SFU_WLAN_PORT_NUM       0
#define OAM_SFU_USB_PORT_NUM        0
#define OAM_SFU_UPSTREAM_QUEUES_NUMBER_DEF      4 
#define OAM_SFU_UPSTREAM_QUEUES_PER_PORT        8 
#define OAM_SFU_DOWNSTREAM_QUEUES_NUMBER_DEF    4
#define OAM_SFU_DOWNSTREAM_QUEUES_PER_PORT_DEF  8
#define OAM_SFU_BATT_BACKUP                     1
#define OAM_SFU_BATT_NO_BACKUP                  0

#define OAM_ONU_IPV6_SUPPORT                    0x1
#define OAM_ONU_IPV6_NOT_SUPPORT                0x0
#define OAM_SFU_LLID_MIN            1
#define OAM_SFU_LLID_MAX            8
#define OAM_SFU_LLID_SINGLE         1
#define OAM_SFU_QUEUE_NUM_PER_LLID  4
#define OAM_HWF_QUEUE_DEFAULT       1
#define OAM_VIRT_QUEUE_DEFAULT      0
#define OAM_RX_QUEUE_DEFAULT        2
#define OAM_TX_QUEUE_DEFAULT        6

#define OAM_ONU_QUEUE_MIN        0
#define OAM_ONU_QUEUE_MAX        7
#define OAM_ONU_QUEUE_MIN_NUM    1
#define OAM_ONU_QUEUE_MAX_NUM    8

#define OAM_MULTICAST_TAB_LEN       256
#define OAM_MAX_IMAGE_SIZE          30*1024*1024

#define OAM_VENDOR_88F6560_PON_CHIP_BUILTTIME 0x0c0601

#define OAM_HOLDOVER_STATE_DEACTIVE           0x00000001
#define OAM_HOLDOVER_STATE_ACTIVE             0x00000002
#define OAM_HOLDOVER_STATE_TIME               200
#define OAM_HOLDOVER_TIME_MIN                 50
#define OAM_HOLDOVER_TIME_MAX                 1000

#define OAM_DEF_GROUP_NUM                     64
#define OAM_MAX_GROUP_NUM                     255

#define OAM_VLAN_TPID_DEFAULT                 0x8100
#define OAM_VLAN_CFI_STANDARD                 0x0
#define OAM_VLAN_CFI_NONSTANDARD              0x1
#define OAM_VLAN_PRI_0                        0x0
#define OAM_VLAN_PRI_7                        0x7

#define OAM_VLAN_ID_MAX                       4095

#define OAM_SERVICE_ALL_SUPPORT               0xf
#define OAM_SERVICE_NONE                      0x0

#define OAM_MAX_POT_NUM     2
#define OAM_MAX_LLID_NUM    8

#define OAM_SWITCH_STATE_ENABLE    1
#define OAM_SWITCH_STATE_DISABLE   0

#define EPON_IGMP_SUPPORT_ENABLE   1
#define EPON_IGMP_NOT_SUPPORT      0
#define EPON_SILENCE_ENABLE        1
#define EPON_SILENCE_DISABLE       0

#define OAM_CAPTURE_SUPPORT_ENABLE     1
#define OAM_CAPTURE_SKIP_ALIVE_ENABLE  1
#define OAM_CAPTURE_LINE_SIZE_DEFAULT  400
#define OAM_CAPTURE_LINE_SIZE_MAX      2000
#define OAM_ONU_SLEEP_DURATION_DEFAULT 1000
#define OAM_ONU_LOS_OPTICAL_TIME_DEF   2
#define OAM_ONU_LOS_OPTICAL_TIME_MAX   0xFFFF
#define OAM_ONU_LOS_MAC_TIME_MAX       0XFFFF
#define OAM_ONU_LOS_MAC_TIME_DEF       50
#define OAM_ONU_SLEEP_DURATION_MAX     0x03B9ACA0
#define OAM_ONU_SLEEP_DURATION_DEF     0x1000
#define OAM_ONU_WAIT_DURATION_MAX      0x03B9ACA0
#define OAM_ONU_WAIT_DURATION_DEF      0x1000

#define LLID0     0
#define LLID1     1
#define LLID2     2
#define LLID3     3
#define LLID4     4
#define LLID5     5
#define LLID6     6
#define LLID7     7
#define UNI0      0
#define UNI1      1
#define UNI2      2
#define UNI3      3

#define MAX_USQUEUESPERLLID          (8)
#define XMLPMAC_0                    (2)
#define XMLPMAC_7                    (9)
#define XMLGMAC_0                    (0)
#define XMLGMAC_1                    (1)
#define OAM_GMAC                     "gmac"
#define OAM_VQNUM                    "v_q_num"
#define OAM_HWFQMAP                  "hwf_q_map"
#define OAM_SWFQMAP                  "swf_q_map"

#define OAM_US_QUEUE_DEF             (4)
#define OAM_DS_QUEUE_DEF             (4)

#define OAM_VENDOR_SPECIFIC_LEN       4
#define OAM_OUI_DEF                   0x111111
#define OAM_VENDOR_SPECIFIC_DEF       0x88ff6500
#define OAMPDU_MAX_SIZE_DEF           1518
#define OAM_CTC_DISCOVERY_DEF         1
#define OAM_TLV_REVISION_DEF          0x00

#define OAM_LOOPBACK_VID_DEF          3999
#define OAM_LOOPBACK_PRI_DEF          0

typedef enum
{
    OAM_VQ_HFWTYPE,
    OAM_VQ_SFWTYPE
} OAM_VQTYPE;

typedef enum
{
    OAM_RULE_DIR_DS = 0,
    OAM_RULE_DIR_US,
    OAM_RULE_DIR_END,
}OAM_RULE_DIR_E;

typedef struct
{
    OAM_RULE_DIR_E       dir;
    OAM_VQTYPE           fwType;
    UINT8                llid;
    UINT8                queue;
} OAM_QUEUE_T;

typedef struct
{
  BOOL    valid;
  UINT32  queueNum;
} OAM_QUEUE_MAP_T;

typedef struct
{
  BOOL             valid;
  UINT32           virtualQueueNum;
  OAM_QUEUE_MAP_T  hwfQMap[MAX_USQUEUESPERLLID];
  OAM_QUEUE_MAP_T  swfQMap[MAX_USQUEUESPERLLID];
} OAM_VIRT_QUEUE_CFG_T;

typedef enum
{
    OAM_XML_MAX_UNI_PORT          = 0,
    OAM_XML_DEBUG_LEVEL           = 1,
    OAM_XML_SWITCH_ENABLE         = 2,
    OAM_XML_EPON_CONNECT_TPM      = 3,
    OAM_XML_OAM_RX_QUEUE          = 4,
    OAM_XML_OAM_TX_QUEUE0         = 5,
    OAM_XML_HWF_TX_USQUEUE0       = 6,
    OAM_XML_HWF_TX_DSQUEUE0       = 7,
    OAM_XML_IPv6_SUPPORT          = 8,
    OAM_XML_ONU_TYPE              = 9,
    OAM_XML_GE_BITMAP             = 10,
    OAM_XML_IGMP_SUPPORT          = 11,
    OAM_XML_SILENCE_MODE          = 12,
    OAM_XML_CAPTURE_STATE         = 13,
    OAM_XML_CAPTURE_SKIP          = 14,
    OAM_XML_CAPTURE_SIZE          = 15,
    OAM_XML_SLEEP_MODE_CAP        = 16,
    OAM_XML_EARLY_WAKEUP_CAP      = 17,
    OAM_XML_CTC_DISCOVERY_ENABLE  = 18,
    OAM_XML_OAMPDU_MAX_SIZE       = 19,
    OAM_XML_OUI                   = 20,
    OAM_XML_VENDOR_SPECIFIC       = 21,
    OAM_XML_TLV_REVISION          = 22,
    OAM_XML_LPBK_VID              = 23,
    OAM_XML_LPBK_PRI              = 24,
    OAM_XML_LPBK_TPID             = 25,
    OAM_XML_VQUEUE_NUM_LLID0      = 26

}OAM_XML_PARA_TYPE_E;

typedef struct
{
    UINT16 v1_tpid;
    UINT16 v2_tpid;
} OAM_TPID_T;

typedef struct
{
    BOOL    ctc_discovery_enable;
    UINT32  oampdu_max_size;
    UINT32  oam_oui;
    UINT32  oam_vendor_specific;
    UINT16  tlv_revision;
} OAM_CONFIG_T;

typedef struct
{
    UINT8              oam_vlan_cfi;
    UINT8              oam_vlan_pri;
    UINT8              oam_debug_level;
    UINT8              oam_multi_switch;
    UINT8              oam_loopback_pri;
    UINT32             oam_loopback_vid;
    UINT32             oam_loopback_tpid;
    UINT32             multi_max_group_num[OAM_MAX_UNI_NUM];
    UINT32             epon_connect_tpm;
    UINT32             switch_enable;
    UINT32             epon_oam_rx_queue;
    UINT32             oam_multicast_tab_len;  
    UINT64             oam_max_image_size;
    UINT32             softver_from_xml;
    UINT32             max_uni_port;
    UINT32             epon_silence_mode;
    UINT32             epon_igmp_support;
    UINT32             oam_capture_support;
    UINT32             oam_capture_skip_alive;
    UINT32             oam_capture_line;
    UINT32             oam_tx_queue[OAM_MAX_LLID_NUM];
    UINT32             hwf_tx_usqueue[OAM_MAX_LLID_NUM];
    UINT32             hwf_tx_dsqueue[OAM_MAX_LLID_NUM];
    OAM_TPID_T         oam_vlan_tpid;
    OAM_ONU_SERIAL_NUM_T                  onu_sn;
    OAM_FIRMWARE_VERSION_T                onu_firmware;
    OAM_CHIPSET_ID_T                      onu_chipset;
    OAM_ONU_CAPABILITIES_T                onu_cap1;
    OAM_ONU_CAPABILITIES_PLUS_T           onu_cap2;
    OAM_ONU_CAPABILITIES_3_T              onu_cap3;    
    OAM_PON_SERVICE_SLA_T                 sla_config;
    OAM_PON_HOLDOVER_STATE_T              holdover_config;
    OAM_ONU_PROTECT_PARA_T                onu_protect_para;
    OAM_ONU_PWR_SAVING_CFG_T              pwr_saving_cfg;
    OAM_ONU_PWR_SAVING_CAP_T              pwr_saving_cap;
    OAM_ONU_SLEEP_CONTROL_T               sleep_ctrl;
    OAM_PORT_ON_OFF_STATE_E               voip_port_admin[OAM_MAX_POT_NUM]; 
    OAM_PORT_LOOPBACK_DETECTION_ADMIN_E   port_loopdetect[OAM_MAX_UNI_NUM];
    OAM_VIRT_QUEUE_CFG_T                  us_virt_queue_cfg[OAM_MAX_LLID_NUM];
    OAM_VIRT_QUEUE_CFG_T                  ds_virt_queue_cfg;
    OAM_CONFIG_T                          oam_cfg;

}POS_PACKED  OAM_XML_PARAM_DB_T;


INT32 oam_stack_adjust_trg_queue(OAM_QUEUE_T *p_VirtualQueue, UINT8 *p_Queue);
UINT32 oam_stack_get_xml_para( OAM_XML_PARA_TYPE_E oam_para_type);

#endif /*__OAM_STACK_PARA_H__*/

