/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_stack_comm.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : Comon definition of EOAM stack                            **/
/**                                                                          **/
/****************************************************************************** 
**                                                                             
*   MODIFICATION HISTORY:                                                      
*
*    12/Jan/2011   - initial version created.         
*                                                                              
******************************************************************************/

#ifndef __OAM_STACK_COMM_H__
#define __OAM_STACK_COMM_H__

#include "oam_ret_val.h"

/*Common definition*/
#define POS_PACKED      __attribute__ ((__packed__))

#define OAM_LOG_FUNCTION_NAME  __FUNCTION__

/*Vendor definition*/
#define OAM_FW_VER         "CV.0.0.1"

/*eOAM version*/
#define OAM_SW_VERSION     "2.1.25"

/*EPON MAC definition*/

/* 
** 802.1P standard
*/
#define MIN_PRIORITY_QUEUE    0 /* 802.1P standard */
#define MAX_PRIORITY_QUEUE    7 /* 802.1P standard */

/* CTC EPON OAM stack code version */
#define CTC_3_0_ONU_VERSION               0x30
#define CTC_2_1_ONU_VERSION               0x21
#define CTC_2_0_ONU_VERSION               0x20
#define CTC_0_1_ONU_VERSION               0x01
#define CTC_1_2_ONU_VERSION               0x13
#define CTC_ONU_VERSION_NON_ANGO          0xff
#define CTC_ONU_VERSION_DEFAULT           CTC_0_1_ONU_VERSION

/*----------------------------------------------------------*/
/*            CTC STACK data type declaration               */
/*----------------------------------------------------------*/
typedef enum
{
    CTC_OAM_CODE_OAM_INFORMATION        = 0x00, /* Coordination of local and remote OAM information. Sent both by PAS-SOFT and ONU*/
    CTC_OAM_CODE_EVENT_NOTIFICATION     = 0x01, /* Alerts remote device of OAM event(s). Sent by ONU, parsed by PAS-SOFT          */
    CTC_OAM_CODE_VAR_REQUEST            = 0x02, /* Requests one or more specific MIB variables                                    */
    CTC_OAM_CODE_VAR_RESPONSE           = 0x03, /* Returns one or more specific MIB variables                                     */
    CTC_OAM_CODE_LOOPBACK               = 0x04, /* control remote DTE's OAM remote loopback state                                 */
    CTC_OAM_CODE_VENDOR_EXTENSION       = 0xFE  /* Available for System Vendor specific extension 0xB0- 0xFF                      */
}OAM_CODE_FIELD_E;

typedef enum
{
    OAM_STACK_STATE_LOS   = 0,     /* No optical                                                                                                              */
    OAM_STACK_STATE_DEREG,         /* The ONU is currently deregistered, no autonmaton progress can be made                                                   */
    OAM_STACK_STATE_MPCP,          /* ONU is registered, and Standard OAM Discovery is done successfully, waiting for (CTC's) Extended OAM Discovery to start */
    OAM_STACK_STATE_IN_PROGRESS,   /* The Extended OAM Discovery process has started                                                                          */
    OAM_STACK_STATE_COMPLETE       /* The Extended OAM Discovery process finished successfully, ONU is ok to answer to vendor-extensions                      */
}OAM_STACK_STATE_E;

typedef enum
{
    CTC_STD_OAM_DISCOVERY = 1,
    CTC_EXTEND_OAM_DISCOVERY,
    CTC_EXTEND_OAM_TRAFFIC
}CTC_OAM_MSG_TYPE_E;

#define CTC_OUI_DEF 0x0111111
#define YOTC_OUI_DEF 0x00CD8
typedef enum
{
    OAM_PON_DISCONNECT = 0,
    OAM_PON_CONNECT    = 1,
    OAM_PON_CONNECT_FEC_ENC = 2,
}OAM_PON_CONNECT_TYPE_E;

typedef enum
{
    OAM_SILENCE_DISABLE = 0,
    OAM_SILENCE_ENABLE  = 1,
}OAM_SILENCE_MODE_TYPE_E;

#define OAM_VENDOR_STANDARD_BRANCH_END          0x00 /* Branch value identifying end-of-frame-data           */
#define OAM_VENDOR_STANDARD_BRANCH_ATTR         0x07 /* Branch value of the Standard (IEEE 802.3) Attributes */
#define OAM_VENDOR_STANDARD_BRANCH_ACTION       0x09 /* Brnach value of the Standard (IEEE 802.3) Actions    */
#define OAM_VENDOR_EXT_EX_VAR_BRANCH_ATTR       0xc7 /* Branch value of the CTC Extended Variable Attributes */
#define OAM_VENDOR_EXT_EX_VAR_BRANCH_ACTION     0xc9 /* Branch value of the CTC Extended Variable Actions    */

#define OAM_VENDOR_STANDARD_OBJECT_LEAF_NONE    0xffff /* A value that is defined in relation with object context variable container, and indicates - no-object-type */

/* CTC Extended Variables Branch 0xC7 Available Leafs: */
#define CTC_EX_VAR_ONU_IDENTIFIER               0x0001  /* ONU Identifier              */
#define CTC_EX_VAR_FIRMWARE_VERSION             0x0002  /* firmware version            */
#define CTC_EX_VAR_CHIPSET_ID                   0x0003  /* chipset id                  */
#define CTC_EX_VAR_ONU_CAPABILITIES             0x0004  /* ONU capabilities            */
#define CTC_EX_VAR_ONU_0PTICAL_TRANSCEIVER_DIAGNOSIS    0x0005  /* ONU transceiver diagnosis   */
#define CTC_EX_VAR_ONU_SLA_SERVICE                      0x0006  /* ONU Service SLA             */  
#define CTC_EX_VAR_ONU_CAPABILITIES_PLUS                0x0007  /* ONU capabilities plus       */ 
#define CTC_EX_VAR_ONU_HOLDOVER                         0x0008  /* ONU holdover config         */
#define CTC_EX_VAR_ONU_MUXMNG_GLOBALPARA                0x0009  /* ONU management global para  */   
#define CTC_EX_VAR_ONU_MUXMNG_SNMPPARA                  0x000A  /* ONU SNMP parameters         */   
#define CTC_EX_VAR_ONU_PON_IF_ACTIVE                    0x000B  /* ONU PON interface config    */ 
#define CTC_EX_VAR_ONU_CAPABILITIES_PLUS_3              0x000C  /* ONU capabiliteis ipv6       */  
#define CTC_EX_VAR_ONU_PWR_SAVE_CAP                     0x000D  /* ONU power saving capabilites*/
#define CTC_EX_VAR_ONU_PWR_SAVE_CFG                     0x000E  /* ONU power saving config     */
#define CTC_EX_VAR_ONU_PROTECT_PARA                     0x000F  /* ONU protection parameters   */
#define CTC_EX_VAR_ETH_PORT_LINK_STATE                  0x0011  /* Ethernet port link state    */
#define CTC_EX_VAR_ETH_PORT_PAUSE               0x0012  /* Ethernet port pause         */
#define CTC_EX_VAR_ETH_PORT_POLICING            0x0013  /* Ethernet port policing      */
#define CTC_EX_VAR_VOIP_PORT                    0x0014  /* VOIP port                   */
#define CTC_EX_VAR_E1_PORT                      0x0015  /* E1 port                     */
#define CTC_EX_VAR_ETH_PORT_DS_RATE_LIMIT       0x0016  /* Ethernet port DS rate limit */
#define CTC_EX_VAR_ONU_PORTLOOPDETECT                   0x0017  /* ONU loop detection          */
#define CTC_EX_VAR_PORT_DISABLE_LOOPED                  0x0018  /* Port Disable Looped         */
#define CTC_EX_VAR_PORT_MAC_AGING_TIME                  0x0019  /* Port MAC Aging Time         */
#define CTC_EX_VAR_VLAN                                 0x0021  /* VLAN modes                  */
#define CTC_EX_VAR_QINQ_VLAN                    0x1021  /* QinQ VLAN modes             */
#define CTC_EX_VAR_CLASSIFICATION_N_MARKING     0x0031  /* Classification & Marking    */
#define CTC_EX_VAR_MULTICAST_VLAN_OPER          0x0041  /* Add/Delete Multicast VLAN   */
#define CTC_EX_VAR_MULTICAST_TAG_STRIP          0x0042  /* Multicast tag strip         */
#define CTC_EX_VAR_MULTICAST_SWITCH             0x0043  /* Multicast switch            */
#define CTC_EX_VAR_MULTICAST_CONTROL            0x0044  /* Multicast VLAN control      */
#define CTC_EX_VAR_MULTICAST_GROUP_NUM          0x0045  /* Multicast group number      */
#define CTC_EX_VAR_FAST_LEAVE_ABILITY           0x0046  /* Fast leave ability of ONU   */
#define CTC_EX_VAR_FAST_LEAVE_ADMIN_STATE       0x0047  /* Fast leave state of ONU     */
#define CTC_EX_VAR_ONU_MULLLID_QUEUECONFIG              0x0051  /* ONU LLID and Queue Config   */
#define CTC_EX_VAR_ONU_ALARM_ADMIN_STATE                0x0081  /* ONU alarm control           */
#define CTC_EX_VAR_ONU_ALARM_THRESHOLD                  0x0082  /* ONU alarm threashold        */
#define CTC_EX_VAR_ONU_TX_POWER_CTL                     0x00A1  /* ONU TX POWER CONTROL        */
#define CTC_EX_VAR_ONU_PM_STATUS                        0x00B1  /* Performance Monitoring Status*/
#define CTC_EX_VAR_ONU_PM_DATA_CURRENT                  0x00B2  /* Performance Monitoring Data Current*/
#define CTC_EX_VAR_ONU_PM_DATA_HISTORY                  0x00B3  /* Performance Monitoring Data History */

#define CTC_EX_VAR_LAST 0x0045  /* This does not represent a REAL Extended Variable, but rather the size of an array to hold them all */

/*for VoIP*/
#define CTC_EX_VAR_VOIP_IAD_INFO             0x0061 /* Get IAD card info                                   */
#define CTC_EX_VAR_VOIP_GLOBAL_PARAM_CONF    0x0062 /* Set/Get VOIP global parameter configuration         */
#define CTC_EX_VAR_H248_PARAM_CONFIG         0x0063 /* Set/Get H.248/MGCP parameter configuration          */
#define CTC_EX_VAR_H248_USER_TID_CONFIG      0x0064 /* Set/Get H.248/MGCP user TID parameter configuration */
#define CTC_EX_VAR_H248_RTP_TID_CONFIG       0x0065 /* Set H.248/MGCP RTP TID parameter configuration      */  
#define CTC_EX_VAR_H248_RTP_TID_INFO         0x0066 /* Get H.248/MGCP RTP TID parameter information        */  
#define CTC_EX_VAR_SIP_PARAM_CONFIG          0x0067 /* Set/Get SIP protocol configuration                  */
#define CTC_EX_VAR_SIP_USER_PARAM_CONFIG     0x0068 /* Set/Get SIP user parameter configuration            */
#define CTC_EX_VAR_VOIP_FAX_CONFIG           0x0069 /* Set/Get VOIP FAX parameter configuration            */
#define CTC_EX_VAR_VOIP_IAD_OP_STATUS        0x006A /* Get IAD operation status                            */
#define CTC_EX_VAR_VOIP_POTS_STATUS          0x006B /* Get user POTS interface status                      */
#define CTC_EX_ACTION_VOIP_IAD_OPERATION     0x006C /* ONU voice module reset                              */
#define CTC_EX_ACTION_SIP_DIGIT_MAP_CONFIG   0x006D /* SIP Digitial Map configuration                      */

/* CTC Extended Actions Branch 0xC9 Available Leafs: */
#define CTC_EX_ACTION_RESET_ONU                0x0001 /* ONU reset                       */
#define CTC_EX_ACTION_SLEEP_CONTROL            0x0002 /* ONU sleep control               */
#define CTC_EX_ACTION_FAST_LEAVE_ADMIN_CONTROL 0x0048 /* Set the fast leave state of ONU */
#define CTC_EX_ACTION_MULTILLID_ADMIN_CONTROL  0x202
#define CTC_EX_ACTION_RESET_CARD               0x0401

#define CTC_EX_ACTION_LAST                     0x0002 /* This does not represent a REAL Extended Action, but rather the size of an array to hold them all */

/* CTC Standard Attributes Branch 0x07 Available Leafs: */
#define CTC_STD_ATTR_GET_PHY_ADMIN_STATE                        37  /* aPhyAdminState Phy admin state                                            */
#define CTC_STD_ATTR_GET_AUTO_NEG_ADMIN_STATE                   79  /* aAutoNegAdminState Auto neg admin state                                   */
#define CTC_STD_ATTR_GET_AUTO_NEG_LOCAL_TECHNOLOGY_ABILITY      82  /* aAutoNegLocalTechnologyAbility Auto neg local technology ability          */
#define CTC_STD_ATTR_GET_AUTO_NEG_ADVERTISED_TECHNOLOGY_ABILITY 83  /* aAutoNegAdvertisedTechnologyAbility Auto neg advertised technology ability*/
#define CTC_STD_ATTR_FEC_ABILITY                                313 /* aFECAbility                                                               */
#define CTC_STD_ATTR_FEC_MODE                                   314 /* aFECmode                                                                  */

/* CTC Standard Actions Branch 0x09 Available Leafs: */
#define CTC_STD_ACTION_PHY_ADMIN_CONTROL             5  /* acPHYAdminControl Phy admin control*/
#define CTC_STD_ACTION_AUTO_NEG_RESTART_AUTO_CONFIG  11 /* acAutoNegRestartAutoConfig         */
#define CTC_STD_ACTION_AUTO_NEG_ADMIN_CONTROL        12 /* acAutoNegAdminControl              */

#endif /*__OAM_STACK_COMM_H__*/
