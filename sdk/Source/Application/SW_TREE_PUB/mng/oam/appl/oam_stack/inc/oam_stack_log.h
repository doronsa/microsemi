/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : oam_stack_log.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : Definition of log module                                  **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.               
 *                                                                              
 ******************************************************************************/
#ifndef __OAM_STACK_LOG_H__
#define __OAM_STACK_LOG_H__

#define OAMLOG_MODULE  "OAM STACK"

#ifdef __FUNCTION__
#undef OAMLOG_MODULE
#define  OAMLOG_MODULE __FUNCTION__
#endif

typedef enum
{
    OAM_LOG_NONE = 0x00,
    OAM_LOG_ERROR,
    OAM_LOG_ALARM, /*such alarm not importance     */
    OAM_LOG_INFO,  /*informat ,such as pkt received*/
    OAM_LOG_DEBUG,
    OAM_LOG_DEBUG_EXTPKT,
    OAM_LOG_DEBUG_PKT
}OAM_LOG_SEVERITY_E;

typedef enum
{
    OAM_LOG_TO_FILE = 0x00,
    OAM_LOG_TO_CLI,
    OAM_LOG_TO_STDOUT
}OAM_LOG_TERMINAL_E;

#define OAM_LOG_ERROR_0(x) oam_stack_log_printf(OAMLOG_MODULE,OAM_LOG_ERROR,x)
#define OAM_LOG_ERROR_1(x,a) oam_stack_log_printf(OAMLOG_MODULE,OAM_LOG_ERROR,x,a)
#define OAM_LOG_ERROR_2(x,a,b) oam_stack_log_printf(OAMLOG_MODULE,OAM_LOG_ERROR,x,a,b)
#define OAM_LOG_ERROR_3(x,a,b,c) oam_stack_log_printf(OAMLOG_MODULE,OAM_LOG_ERROR,x,a,b,c)
#define OAM_LOG_ERROR_4(x,a,b,c,d) oam_stack_log_printf(OAMLOG_MODULE,OAM_LOG_ERROR,x,a,b,c,d)
#define OAM_LOG_ERROR_5(x,a,b,c,d,e) oam_stack_log_printf(OAMLOG_MODULE,OAM_LOG_ERROR,x,a,b,c,d,e)
#define OAM_LOG_ERROR_6(x,a,b,c,d,e,f) oam_stack_log_printf(OAMLOG_MODULE,OAM_LOG_ERROR,x,a,b,c,d,e,f)
#define OAM_LOG_ERROR_7(x,a,b,c,d,e,f,g) oam_stack_log_printf(OAMLOG_MODULE,OAM_LOG_ERROR,x,a,b,c,d,e,f,g)

#define CHECK_API_CALLS_RETURN_AND_LOG_ERROR(_ret_,_LogPrt) \
{\
    INT32 rc = 0 ;\
    if((rc = (_ret_)) != 0)\
    {\
        OAM_LOG_ERROR_2("%s ,ret %d\r\n", (_LogPrt), rc);\
        return (rc) ;\
    }\
}

VOID oam_stack_log_printf(const char *func_name, OAM_LOG_SEVERITY_E level, const char *format, ...);

VOID oam_stack_dbg_print_frame(OAM_LOG_SEVERITY_E level, UINT8 *a_frame, UINT16 a_frame_size);

VOID oam_stack_log_severity_level_set(OAM_LOG_SEVERITY_E level);

OAM_LOG_SEVERITY_E oam_stack_log_severity_level_get(VOID);

VOID oam_stack_log_terminal_mode_set(OAM_LOG_TERMINAL_E mode);

OAM_LOG_TERMINAL_E oam_stack_log_terminal_mode_get();

extern char*  oam_get_sw_version(VOID);

#endif /*__OAM_STACK_LOG_H__*/
