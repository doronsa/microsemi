/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : oam_stack_pon.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : Definition of PON related OAM handler                     **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.                
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_PON_H__
#define __OAM_STACK_PON_H__

#include "oam_stack_ossl.h"
#include "oam_stack_comm.h"
#include "oam_stack_type_expo.h"
#include "oam_inc.h"

#define PON_BASE_LLID 0
#define PON_SUPPORT_MAX_LLID_CNT 16

/*CTC OAM API*/
typedef INT32 (*ext_pon_onu_get_dba_report_thresholds)(S_EponIoctlDba *queues_sets_thresholds, UINT16 llid);

typedef INT32 (*ext_pon_onu_set_dba_report_thresholds)(S_EponIoctlDba *queues_sets_thresholds, UINT16 llid);

typedef INT32 (*ext_pon_onu_get_fec_ability)(OAM_PON_STD_FEC_ABILITY_E  *fec_ability);

typedef INT32 (*ext_pon_onu_set_fec_mode)(const OAM_PON_STD_FEC_MODE_E  *mode);

typedef INT32 (*ext_pon_onu_get_fec_mode)(OAM_PON_STD_FEC_MODE_E  *mode);

typedef INT32 (*ext_pon_onu_get_optical_transceiver_diagnosis)(OAM_PON_OPTICAL_TRANSCEIVER_DIAGNOSIS_T  *result);

typedef INT32 (*ext_pon_onu_get_service_sla)(OAM_PON_SERVICE_SLA_T  *mode);

typedef INT32 (*ext_pon_onu_set_service_sla)(const OAM_PON_SERVICE_SLA_T  *mode);

typedef INT32 (*ext_pon_onu_send_frame_to_olt)(OAM_CODE_FIELD_E opcode,UINT8 *a_in_message, const UINT16 a_in_length, UINT16 llid);

typedef INT32 (*ext_pon_onu_oam_frame_received)(OAM_CODE_FIELD_E opcode, UINT8 *a_in_message, const UINT16 a_in_length);
/*STD OAM API*/
typedef INT32 (*ext_pon_encryption_get_active_key)(OAM_PON_ENC_KEY_INDEX_E *key);

typedef INT32 (*ext_pon_encryption_set_active_key)(OAM_PON_ENC_KEY_INDEX_E key);

typedef INT32 (*ext_pon_encryption_update_key)(OAM_PON_ENC_KEY_INDEX_E key_index, OAM_PON_ENC_KEY_T key, UINT16 llid);

typedef INT32 (*ext_pon_encryption_set_mode)(OAM_PON_ENC_MODE_E mode);

typedef INT32 (*ext_pon_encryption_get_mode)(OAM_PON_ENC_MODE_E *mode);

typedef INT32 (*ext_pon_connection_hook_t)(OAM_PON_CONNECT_TYPE_E);

typedef INT32 (*ext_pon_connection_state_hook_t)(OAM_PON_CONNECT_TYPE_E *state);

typedef INT32 (*ext_pon_authentication_hook_t)(BOOL);

typedef INT32 (*ext_pon_onu_get_hold_over_hook_t)(OAM_PON_HOLDOVER_STATE_T *);

typedef INT32 (*ext_pon_onu_set_hold_over_hook_t)(const  OAM_PON_HOLDOVER_STATE_T *);

typedef INT32 (*ext_pon_onu_get_if_active_hook_t)(OAM_PON_ACTIVE_IF_STATE_T *);

typedef INT32 (*ext_pon_onu_set_if_active_hook_t)(const  OAM_PON_ACTIVE_IF_STATE_T *);

typedef INT32 (*ext_pon_onu_set_multi_llid_ctrl_hook_t)(const  UINT32);

typedef INT32 (*ext_pon_onu_get_multi_llid_ctrl_hook_t)(UINT32 *);

typedef INT32 (*ext_pon_onu_get_base_llid_tid_hook_t)(UINT32 *);

typedef INT32 (*ext_pon_onu_set_tx_power_cfg_hook_t)(const OAM_POWER_TX_CTL_T *);

typedef INT32 (*ext_pon_onu_get_tx_power_cfg_hook_t)(OAM_POWER_TX_CTL_T *);

typedef INT32 (*ext_pon_onu_set_pwr_saving_cap_hook_t)(const OAM_ONU_PWR_SAVING_CAP_T *);

typedef INT32 (*ext_pon_onu_get_pwr_saving_cap_hook_t)(OAM_ONU_PWR_SAVING_CAP_T *);

typedef INT32 (*ext_pon_onu_set_pwr_saving_cfg_hook_t)(const OAM_ONU_PWR_SAVING_CFG_T *);

typedef INT32 (*ext_pon_onu_get_pwr_saving_cfg_hook_t)(OAM_ONU_PWR_SAVING_CFG_T *);

typedef INT32 (*ext_pon_onu_set_protect_para_hook_t)(const OAM_ONU_PROTECT_PARA_T *);

typedef INT32 (*ext_pon_onu_get_protect_para_hook_t)(OAM_ONU_PROTECT_PARA_T *);

typedef INT32 (*ext_pon_onu_set_sleep_control_hook_t)(const OAM_ONU_SLEEP_CONTROL_T *);

typedef INT32 (*ext_pon_onu_get_sleep_control_hook_t)(OAM_ONU_SLEEP_CONTROL_T *);

typedef INT32 (*ext_pon_onu_get_if_number_hook_t)(UINT32 *);

typedef INT32 (*ext_pon_onu_get_llid_number_hook_t)(UINT32 *);

typedef INT32 (*ext_pon_onu_mpcp_nack_hook_t)(VOID);

typedef INT32 (*ext_pon_onu_get_llid_queue_configure_hook_t)(UINT32 llid,OAM_PON_LLID_QUEUE_CONFIG_T  *llid_queue_configure);

typedef INT32 (*ext_pon_onu_set_llid_queue_configure_hook_t)(UINT32 llid,const OAM_PON_LLID_QUEUE_CONFIG_T  *llid_queue_configure);

typedef INT32 (*ext_pon_onu_maintain_terminate_hook_t)(VOID);

typedef struct
{
    ext_pon_onu_get_dba_report_thresholds         pon_get_dba_hook;
    ext_pon_onu_set_dba_report_thresholds         pon_set_dba_hook;

    ext_pon_onu_get_fec_ability                   pon_get_fec_ability;

    ext_pon_onu_set_fec_mode                      pon_set_fec_hook;
    
    ext_pon_onu_get_fec_mode                      pon_get_fec_hook;

    ext_pon_onu_get_optical_transceiver_diagnosis pon_get_optial_diag_hook;
    
    ext_pon_onu_set_service_sla                   pon_set_sla_hook;
    
    ext_pon_onu_get_service_sla                   pon_get_sla_hook;
  
    ext_pon_onu_send_frame_to_olt                 pon_oam_send_hook;

    ext_pon_connection_hook_t                     pon_connect_hook;

    ext_pon_connection_state_hook_t               pon_connect_state_hook;

    ext_pon_onu_mpcp_nack_hook_t                  pon_mpcp_nack_hook;
    /*enc*/
    ext_pon_encryption_get_active_key             pon_enc_get_active_key_hook;

    ext_pon_encryption_set_active_key             pon_enc_set_active_key_hook;

    ext_pon_encryption_update_key                 pon_enc_update_key_hook;

    ext_pon_encryption_set_mode                   pon_enc_mode_set_hook;

    ext_pon_encryption_get_mode                   pon_enc_mode_get_hook;

    ext_pon_onu_get_hold_over_hook_t              pon_hold_over_get_hook;

    ext_pon_onu_set_hold_over_hook_t              pon_hold_over_set_hook;

    ext_pon_onu_get_if_active_hook_t              pon_if_active_get_hook;    

    ext_pon_onu_set_if_active_hook_t              pon_if_active_set_hook;    

    ext_pon_onu_get_llid_queue_configure_hook_t   pon_llid_queue_config_get_hook;

    ext_pon_onu_set_llid_queue_configure_hook_t   pon_llid_queue_config_set_hook;

    ext_pon_onu_set_multi_llid_ctrl_hook_t        pon_multi_llid_admin_ctl_hook;

    ext_pon_onu_get_multi_llid_ctrl_hook_t        pon_multi_llid_ctl_get_hook;

    ext_pon_onu_maintain_terminate_hook_t         pon_onu_extended_terminate_hook;    

    ext_pon_onu_get_base_llid_tid_hook_t          pon_onu_base_llid_get;

    ext_pon_onu_set_tx_power_cfg_hook_t           pon_onu_power_tx_ctl_set_hook;

    ext_pon_onu_get_tx_power_cfg_hook_t           pon_onu_power_tx_ctl_get_hook;

    ext_pon_onu_set_pwr_saving_cap_hook_t         pon_onu_pwr_saving_cap_set_hook;

    ext_pon_onu_get_pwr_saving_cap_hook_t         pon_onu_pwr_saving_cap_get_hook;

    ext_pon_onu_get_pwr_saving_cfg_hook_t         pon_onu_pwr_saving_cfg_get_hook;

    ext_pon_onu_set_pwr_saving_cfg_hook_t         pon_onu_pwr_saving_cfg_set_hook;

    ext_pon_onu_get_protect_para_hook_t           pon_onu_protect_para_get_hook;

    ext_pon_onu_set_protect_para_hook_t           pon_onu_protect_para_set_hook;

    ext_pon_onu_get_sleep_control_hook_t          pon_onu_sleep_control_get_hook;

    ext_pon_onu_set_sleep_control_hook_t          pon_onu_sleep_control_set_hook;

}OAM_STACK_PON_INIT_T;

#endif /*__OAM_STACK_PON_H__*/
