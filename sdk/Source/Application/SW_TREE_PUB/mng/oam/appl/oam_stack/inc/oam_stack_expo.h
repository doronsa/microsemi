/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_stack_expo.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : Definition of exported paramters and                      **/
/**                Declaration of functions                                  **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.         
 *                                                                              
 ******************************************************************************/

#ifndef _OAM_STACK_EXPO_H__
#define _OAM_STACK_EXPO_H__

/*============================= Include Files ==============================*/    

/*============================== Constants =================================*/

/*================================ Macros ==================================*/

/*============================== Data Types ================================*/

/*============================== Data Types ================================*/

/*========================= Function Prototypes ============================*/

/**
*
* This command initializes the CTC stack.  
*
*@param [IN]
*
*info:    See VENDOR_SYS_INFO_T for details
*
*@return 
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_init(VENDOR_SYS_INFO_T *info);


/**
* This command returns whether the CTC stack is initialized
*
*
*@return
*    PON_FALSE or TRUE
*
*/
BOOL oam_stack_is_init(VOID);


/** 
* This command send the specify oam message to olt ,according to the OAM opcode
*
*@param [IN]
* 
*@param [OUT]  
*      init                    : PON_FALSE or TRUE
*
*return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_send_frame_to_olt(OAM_CODE_FIELD_E opcode, UINT8 *a_in_message, const UINT16 a_in_length, UINT16 llid);


/** 
* This command terminates the CTC stack. 
*
*@param [IN]
*  
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_terminate(VOID);


/** 
* This command get custom defined extend oam hook for non-ctc oui oam. 
*
*@param [IN]
*    custom_oui                         :custom oui
*           OAM_CUSTOM_RECEIVE_HOOK_T     :   see OAM_CUSTOM_RECEIVE_HOOK_T for details
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_api_custom_extend_oam_hook_get(UINT32 custom_oui, OAM_CUSTOM_OUI_RECEIVE_HOOK_T *custom_oam_handler);


/**
* An event indicating the completion of an extended OAM discovery of an ONU.
* The discovery process may succeed or fail.
*
*@param [IN]
*    ui_version_records_list: See OAM_OUI_VERSION_RECORD_T for details
*
*@return
*    Return codes are not specified for an event
*
*/
OAM_STACK_STATE_E oam_stack_discovery_state_get(VOID);


/** 
*@param [IN]
*
*@param [OUT]
*    queues_sets_thresholds : Array of qeueue sets, each set contains several queues, for each queue state and threshold are configured
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_set_dba_report_thresholds(S_EponIoctlDba *queues_sets_thresholds, UINT16 llid);


/** 
*@param [IN]   
*    number_of_queue_sets   : Number of queue sets in REPORT frames
*@param [IN]    
*    queues_sets_thresholds : Array of qeueue sets, each set contains several queues, for each queue state and threshold are retrieved
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_get_dba_report_thresholds(S_EponIoctlDba *queues_sets_thresholds, UINT16 llid);


/** 
* This function retrieves ONU serial number which is composed of vendor id, ONU model and ONU ID (MAC address)
*
*
*@param [OUT]
*    onu_serial_number : See OAM_ONU_SERIAL_NUM_T for details.
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_serial_number(OAM_ONU_SERIAL_NUM_T *onu_serial_number);


/** 
* This function retrieves ONU firmware version.
*
*
*@param [OUT]
*    version : Firmware version.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_firmware_version(OAM_FIRMWARE_VERSION_T *version);


/** 
* This function retrieves ONU chipset information.
*
*
*@param [OUT]
*    chipset_id : See OAM_CHIPSET_ID_T for details.
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_chipset_id(OAM_CHIPSET_ID_T  *chipset_id);


/**
* This function retrieves ONU main HW capabilities
*
*
*@param [OUT]
*    onu_capabilities : See OAM_ONU_CAPABILITIES_T for details.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_capabilities(OAM_ONU_CAPABILITIES_T  *onu_capabilities);


/** 
* This function retrieves ONU main HW capabilities plus 2
*
*@param [OUT]
*    onu_capabilities : See OAM_ONU_CAPABILITIES_PLUS_T for details.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_capabilities_plus(OAM_ONU_CAPABILITIES_PLUS_T  *onu_capabilities);


/** 
* This function retrieves ONU main HW capabilities plus 3
*
*@param [OUT]
*    onu_capabilities : See OAM_ONU_CAPABILITIES_3_T for details.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_capabilities_plus_3(OAM_ONU_CAPABILITIES_3_T  *onu_capabilities);


/** 
* This function retrieves ONU main mac address
*
*
*@param [OUT]
*    mac_address : See OAM_ETH_MAC_ADDR_T for details.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_mac_address(OAM_ETH_MAC_ADDR_T mac_address);


/** 
* This function retrieves ONU main HW capabilities
*
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_ethernet_port_bitmap(UINT32 slot_id, UINT64 *eth_bitmap, UINT64 *ge_bitmap);


/** 
* This function retrieves ONU main HW capabilities
*
*@param [IN]
*    slot_id            : slot - for MUD, 0 for SFU/HUG
*@param [OUT]
*    min_port           : minimal port within specified slot.
*
*@param [OUT]
*    max_port           : minimal port within specified slot.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_ethernet_port_num(UINT32 slot_id, UINT32 *min_port, UINT32 *max_port);


/** 
* This function sets flow control activation status for an Ethernet port of a specified ONU
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet port number .     
*@param  [IN]
*    flow_control_enable    :Enable or disable flow control for the port.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_ethernet_port_pause 
(
    const UINT32                 slot_id,
    const UINT32                 port_id,
    const OAM_PORT_PAUSE_STATE_E *flow_control_enable
);


/** 
* This function retrieves flow control activation status for an Ethernet port of a specified ONU
*
*@param  [IN]
*    slot_id                    : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                    : Ethernet port number .     
*
*@param  [OUT]
*    flow_control_enable        : Is flow Control enabled for the port
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_ethernet_port_pause 
( 
    const UINT32           slot_id,
    const UINT32           port_id,
    OAM_PORT_PAUSE_STATE_E *flow_control_enable
);


/** 
* This function sets upstream service policing configuration for specified  UNI Ethernet ports of an ONU
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet port number .     
*@param  [IN]
*    port_policing          : See oam_stack_ethernet_port_policing_entry_t for details.  
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_ethernet_port_up_policing 
( 
    const UINT32                        slot_id,
    const UINT32                        port_id,
    const OAM_PORT_US_POLICING_ENTRY_T  *port_policing
);


/** 
* This function sets port loop detection for specified  UNI Ethernet ports of an ONU
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet port number .     
*@param  [IN]
*    loop_detect_enable     : See OAM_PORT_LOOPBACK_DETECTION_ADMIN_E for details.  
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_ethernet_port_loop_detect 
(
    const UINT32                        slot_id,
    const UINT32                        port_id,
    OAM_PORT_LOOPBACK_DETECTION_ADMIN_E *loop_detect_enable
);

                        
/** 
* This function retrieves upstream service policing configuration for an Ethernet ports of a specified ONU
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet port number .     
*
*@param  [OUT]
*    port_policing          : See oam_stack_ethernet_port_policing_entry_t for details.  
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_ethernet_port_up_policing 
( 
    const UINT32                  slot_id,
    const UINT32                  port_id,
    OAM_PORT_US_POLICING_ENTRY_T  *port_policing
);

                             
/** 
* This function retrieves ONU ethernet port downstream rate limiting configuration
*
*@param  [IN]
*    slot_id                    : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                    : Ethernet port number .     
*
*@param  [OUT]
*    port_ds_rate_limiting      : See OAM_PORT_DS_RATE_LIMITING_ENTRY_T for details  
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_ethernet_port_ds_rate_limiting 
(
    const UINT32                       slot_id,
    const UINT32                       port_id,
    OAM_PORT_DS_RATE_LIMITING_ENTRY_T  *port_ds_rate_limiting 
);


/** 
* This function set ONU ethernet port downstream rate limiting configuration
*
*@param  [IN]
*    slot_id                 : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                 : Ethernet port number .     
*@param  [IN]
*    port_ds_rate_limiting   : See OAM_PORT_DS_RATE_LIMITING_ENTRY_T for details  
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_ethernet_port_ds_rate_limiting 
( 
    const UINT32                             slot_id,
    const UINT32                             port_id,
    const OAM_PORT_DS_RATE_LIMITING_ENTRY_T  *port_ds_rate_limiting
);


/** 
* This function retrieves phy admin state for a specific port.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet port number .     
*
*@param  [OUT]
*    state                  : phy admin control state. ENABLE\DISABLE.
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_phy_admin_state 
( 
    const UINT32           slot_id,
    const UINT32           port_id,
    OAM_PORT_ADMIN_E      *state 
);


/** 
* This function set phy admin control.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet port number .     
*@param  [IN]
*    state                  : phy admin control state. ENABLE\DISABLE.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_phy_admin_control 
( 
    const UINT32                 slot_id,
    const UINT32                 port_id,
    const OAM_PORT_ADMIN_E       *state
);


/** 
* This function retrieves the link operation state of an Ethernet port
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet port number .     
*
*@param  [OUT]
*    link_state             : Ethernet link operation state. See OAM_PORT_LINK_STATE_E for details
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_ethernet_link_state 
( 
    const UINT32            slot_id,
    const UINT32            port_id,
    OAM_PORT_LINK_STATE_E   *link_state
);


/** 
* This function set POTS port state.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Voip pots number .     
*@param  [IN]
*    port_state             : Active or deactive the VoIP port.
*       
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_voip_port 
( 
    const UINT32                    slot_id,
    const UINT32                    port_id,
    const OAM_PORT_ON_OFF_STATE_E   *port_state 
);


/**
* This function retrieves port's states for the specified ONU VoIP port.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Voip pots number .     
*
*@param  [OUT]

*    port_state             : Is port activated or deactivated.
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_voip_port 
(
    const UINT32                    slot_id,
    const UINT32                    port_id,
    const OAM_PORT_ON_OFF_STATE_E   *port_state 
);


/**
* This function set E1 port state.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : E1 pots number 
*@param  [IN]
*    port_state             : Active or deactive the E1 port.
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_e1_port 
( 
    const UINT32                    slot_id,
    const UINT32                    port_id,
    const OAM_PORT_ON_OFF_STATE_E   *port_state 
);


/** 
* This function retrieves port's states for the specified ONU E1 port.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : E1 pots number 
*
*@param [OUT]
*    port_state             : Is port activated or deactivated.
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_e1_port 
( 
    const UINT32             slot_id,
    const UINT32             port_id,
    OAM_PORT_ON_OFF_STATE_E  *port_state
);


/** 
* This function performs VLAN configuration to a specific Ethernet port
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*@param  [IN]
*    port_configuration     : See OAM_ETH_PORT_VLAN_CONF_T for details.
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_vlan_port_configuration 
( 
    const UINT32                    slot_id,
    const UINT32                    port_id,
    const OAM_ETH_PORT_VLAN_CONF_T  *port_configuration
);


/** 
* This function retrieves VLAN operation Ethernet port configuration for the Ethernet port of specified ONU
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id                : Ethernet pots number 
*
*@param [OUT]
*    port_configuration        : See OAM_ETH_PORT_VLAN_CONF_T for details.
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_vlan_port_configuration 
(
    const UINT32                slot_id,
    const UINT32                port_id,
    OAM_ETH_PORT_VLAN_CONF_T    *port_configuration
);


/** 
* This function set classification and marking rules.
*
*@param  [IN]
*    slot_id                     : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                     : Ethernet pots number 
*@param  [IN]
*    classification_and_marking  : See oam_stack_classification_rules_t for details.
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_classification_and_marking 
( 
    const UINT32                slot_id,
    const UINT32                port_id,
    const OAM_CLASS_N_MARK_T    *classification_and_marking
);


/** 
* This function retrieves classification and marking rules for a specific port.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*
*@param [OUT]
*    classification_and_marking:  See oam_stack_classification_rules_t for details.
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_classification_and_marking 
(
const UINT32         slot_id,
const UINT32         port_id,
OAM_CLASS_N_MARK_T   *classification_and_marking
);


/**
* This function sets ONU multicast VLAN configuration.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*@param  [IN]
*    multicast_vlan            : See OAM_MC_VLAN_T for details.
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_multicast_vlan 
( 
    const UINT32            slot_id,
    const UINT32            port_id,
    const OAM_MC_VLAN_T     *multicast_vlan
);


/**
* This function clears ONU multicast VLAN configuration
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_clear_multicast_vlan 
( 
    const UINT32 slot_id,
    const UINT32 port_id
);


/** 
* This function retrieves ONU multicast VLAN configuration for a specific Ethernet port
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*
*@param [OUT]
*    multicast_vlan         : See OAM_MC_VLAN_T for details.
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_multicast_vlan 
( 
    const UINT32   slot_id,
    const UINT32   port_id,
    OAM_MC_VLAN_T  *multicast_vlan
);


/** 
* This function set multicast control.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*@param  [IN]
*    multicast_control      : See OAM_MC_CTRL_T for details.
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_multicast_control 
( 
    const OAM_MC_CTRL_T *multicast_control 
);


/**
* This function clears ONU multicast service control configuration
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_clear_multicast_control 
( 
    const UINT32 slot_id, 
    const UINT32 port_id 
);


/** 
* This function retrieves multicast control.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*
*@param [OUT]
*    multicast_control      : See OAM_MC_CTRL_T for details.
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_multicast_control 
(  
    OAM_MC_CTRL_T  *multicast_control 
);


/** 
* This function set multicast group number for a specific port.
*
*@param  [IN]
*    slot_id                 : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                 : Ethernet pots number 
*@param  [IN]
*    group_num               : Allowed multicast group number that can be handled at the same time.
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_multicast_group_num 
( 
    const UINT32 slot_id, 
    const UINT32 port_id,
    UINT8        *group_num
);


/** 
* This function retrieves multicast group number for a specific port.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*
*@param [OUT]
*    group_num              : Allowed multicast group number that can be handled at the same time.
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_multicast_group_num 
( 
    const UINT32 slot_id,
    const UINT32 port_id,
    UINT8        *group_num
);


/** 
* This function retrieves phy admin state for a specific port.
*
*@param  [IN]
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id           : Ethernet pots number 
*
*@param [OUT]
*    state             : ENABLE\DISABLE.
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_auto_negotiation_admin_state 
( 
    const UINT32            slot_id,
    const UINT32            port_id,
    OAM_PORT_NEGA_STATE_E   *state 
);
                        
                        
/** 
* This function retrieves loop detection admin state for a specific port.
*
*@param  [IN]
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id           : Ethernet pots number 
*
*@param [OUT]
*    state             : ENABLE\DISABLE.
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_loop_detect_admin_state 
( 
    const UINT32                        slot_id,
    const UINT32                        port_id,
    OAM_PORT_LOOPBACK_DETECTION_ADMIN_E *state 
);

                        
/** 
* This function set auto negotiation admin control.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*@param  [IN]
*    state                  : auto negotiation admin control state. ENABLE\DISABLE.
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_auto_negotiation_admin_control 
( 
    const UINT32                 slot_id,
    const UINT32                 port_id,
    const OAM_PORT_NEGA_STATE_E  *state
);


/** 
* This function retrieves local technology abilities for a specific port.
*
*@param  [IN]
*   slot_id            : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*   port_id            : Ethernet pots number 
*
*@param [OUT]
*   abilities          : See OAM_PORT_AUTO_NEGA_TECH_ABILITY_T for details.
*@return
*   All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_auto_negotiation_local_technology_ability 
( 
    const UINT32                        slot_id,
    const UINT32                        port_id,
    OAM_PORT_AUTO_NEGA_TECH_ABILITY_T   *abilities 
);


/** 
* This function retrieves advertised technology abilities for a specific port.
*
*@param  [IN]
*   slot_id             : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*   port_id             : Ethernet pots number 
*
*@param [OUT]
*   abilities           :See OAM_PORT_AUTO_NEGA_TECH_ABILITY_T for details.
*@return
*   All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_auto_negotiation_advertised_technology_ability 
( 
    const UINT32                        slot_id,
    const UINT32                        port_id,
    OAM_PORT_AUTO_NEGA_TECH_ABILITY_T   *abilities 
);


/** 
* This function set auto negotiation restart auto config.
*
*@param  [IN]
*    slot_id            : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id            : Ethernet pots number 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_auto_negotiation_restart_auto_config 
( 
    const UINT32 slot_id,
    const UINT32 port_id
);


/** 
* This function retrieves FEC ability.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*
*@param [OUT]
*    fec_ability            : FEC ability. See OAM_PON_STD_FEC_ABILITY_E for details
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_get_fec_ability 
( 
    OAM_PON_STD_FEC_ABILITY_E *fec_ability
);


/** 
* This function retrieves FEC mode.
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*
*@param [OUT]
*    fec_mode               : FEC mode. See OAM_PON_STD_FEC_MODE_E for details
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_get_fec_mode 
( 
    OAM_PON_STD_FEC_MODE_E *fec_mode 
);


/** 
* This function set FEC mode.
*
*@param  [IN]
*    slot_id            : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id            : Ethernet pots number 
*@param [IN]
*    fec_mode           : FEC mode. See OAM_PON_STD_FEC_MODE_E for details.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_set_fec_mode 
( 
    const OAM_PON_STD_FEC_MODE_E  *fec_mode
);



/**
* This function request MPCP nack action from upper implementation .
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_mpcp_nack_request(VOID) ;



/**
* This function reset a specific ONU.
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_reset_onu (VOID);


/** 
* This function reset a specific ONU.
*
*@param [IN]
*    card_id            : Only for MXU.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_reset_card(UINT32 card_id);


/** 
* This function configures if ONU must strip the VLAN TAG of multicast traffic report or not
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*@param [IN]
*    tag_strip              : Disable / Enable tag strip 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_multicast_tag_strip 
(
    const UINT32                    slot_id,
    const UINT32                    port_id,
    const OAM_MC_PORT_TAG_OPER_T    *tag_strip
);


/** 
* This function retrieves if ONU must strip the VLAN TAG of multicast traffic report or not
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*
*@param [OUT]
*    tag_strip              : Disable / Enable tag strip 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_multicast_tag_strip 
( 
    const UINT32            slot_id,
    const UINT32            port_id,
    OAM_MC_PORT_TAG_OPER_T  *tag_strip
);


/**
* This function sets ONU multicast protocol
*
*@param  [IN]
*    slot_id                : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id                : Ethernet pots number 
*@param [IN]
*    multicast_protocol     : multicast management protocol 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_multicast_switch 
( 
    const OAM_MC_PROTOCOL_TYPE_E *multicast_protocol 
);


/** 
* This function retrieves  ONU multicast protocol
*
*@param  [IN]
*    slot_id            : slot - for MUD, 0 for SFU/HUG
*@param  [IN]
*    port_id            : Ethernet pots number 
*
*@param [OUT]
*    multicast_protocol : multicast management protocol 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_multicast_switch 
(   
    OAM_MC_PROTOCOL_TYPE_E *multicast_protocol 
);


/** 
* This function set ONU multicast fast leave admin control
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number 
*@param [IN]
*    fast_leave_admin_state    : fast leave admin state
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_fast_leave_admin_control 
(   
    OAM_FAST_LEAVE_ADMIN_STATE_E *fast_leave_admin_state 
);

  
/** 
* This function retrieves ONU multicast fast leave admin state
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number 
*
*@param [OUT]
*    fast_leave_admin_state: fast leave admin state 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_fast_leave_admin_state 
( 
    OAM_FAST_LEAVE_ADMIN_STATE_E *fast_leave_admin_state 
);


/** 
* This function retrieves ONU multicast fast leave ability
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number 
*
*@param [OUT]
*    fast_leave_ability: fast leave ability
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_fast_leave_ability 
( 
    OAM_MC_FAST_LEAVE_ABILITY_T *fast_leave_ability 
);


/** 
* Get authentication port_id+Password data table
*
*@param [OUT]
*    oid_data_table : See OAM_AUTH_LOID_DATA_T for details.
*
*@return
*    All the defined oam_stack_* return codes
*/
STATUS oam_stack_get_auth_loid
(
    OAM_AUTH_LOID_DATA_T *loid_data_table
);


/**
* Add authentication port_id+Password data
*
*@param [IN]
*    oid_data        : See OAM_AUTH_LOID_DATA_T for details.
*
*@param [OUT]
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_add_auth_loid_data
(
    OAM_AUTH_LOID_DATA_T *loid_data
);


/** 
* Set authentication mode
*
*@param [IN]
*    ctc_auth_mode       : See OAM_AUTH_MODE_E for details.
*
*@param [OUT]
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_auth_mode
(           
    const OAM_AUTH_MODE_E *ctc_auth_mode
);


/** 
* Get authentication mode
*
*@param [OUT]
*    ctc_auth_mode       : See OAM_AUTH_MODE_E for details.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_auth_mode
(           
    OAM_AUTH_MODE_E *ctc_auth_mode
);


/**
* This function retrieves ONU optical transceiver diagnosis
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number 
*
*@param [OUT]
*    optical_transceiver_diagnosis: See OAM_PON_OPTICAL_TRANSCEIVER_DIAGNOSIS_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_optical_transceiver_diagnosis 
( 
    OAM_PON_OPTICAL_TRANSCEIVER_DIAGNOSIS_T  *optical_transceiver_diagnosis 
);


/** 
* This function sets ONU service SLA
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number 
*@param [IN]
*    service_sla       : See oam_stack_service_sla_t for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_service_sla 
( 
    const OAM_PON_SERVICE_SLA_T *service_sla 
);


/** 
* This function retrieves ONU service SLA
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number 
*
*@param [OUT]
*    activate            : Is service SLA activated or deactivated.
*    service_sla         : See oam_stack_service_sla_t for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_service_sla 
( 
OAM_PON_SERVICE_SLA_T* service_sla 
);

/**
* This function sets ONU alarm admin state
*
*@param  [IN] 
*    slot_id            : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id            : Ethernet pots number 
*@param [IN]
*    alarm_id           : See OAM_ALARM_ID_E for details 
*@param [IN]
*    alarms_state        : See CTC_spec_alarm_admin_state_t for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_alarm_set_admin_state 
( 
    UINT32                          slot_id,
    UINT32                          port_id,
    OAM_ALARM_ID_E                  alarm_id,
    const OAM_ALARM_ADMIN_STATE_T   *alarms_state
);


/** 
* This function retrieves ONU alarm admin state
*
*@param  [IN] 
*    slot_i            : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number 
*@param [IN]
*    alarm_id          : See OAM_ALARM_ID_E for details 
*
*@param [OUT]
*    alarms_state      : Array of CTC_spec_alarm_admin_state_t ,See CTC_spec_alarm_admin_state_t for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_alarm_get_admin_state 
( 
    const UINT32                slot_id,
    const UINT32                port_id,
    const OAM_ALARM_ID_E        alarm_id,
    OAM_ALARM_ADMIN_STATE_T     *alarms_state 
);


/** 
* This function sets ONU alarm threshold
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number 
*@param [IN]
*    alarm_id          : See OAM_ALARM_ID_E for details 
*@param [IN]
*    alarm_threshol    : Alarm threshold
*
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_alarm_set_threshold 
( 
    const UINT32            slot_id, 
    const UINT32            port_id,
    const OAM_ALARM_ID_E    alarm_id,
    OAM_ALARM_THRESHOLD_T   *alarms_threshold
);


/** 
* This function retrieves ONU alarm admin state
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number 
*@param [IN]
*    alarm_id          : See OAM_ALARM_ID_E for details 
*
*@param [OUT]
*    alarms_threshold  : Array of oam_stack_alarm_threshold_t,See oam_stack_alarm_threshold_t for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_alarm_get_threshold 
( 
    const UINT32            slot_id,
    const UINT32            port_id,
    const OAM_ALARM_ID_E    alarm_id,
    OAM_ALARM_THRESHOLD_T   *alarms_threshold
);


/**
* This function set llid queue config
*
*@param [IN]
*    llid          : LLID id
*@param [IN]
*    llid_queue_config: See OAM_PON_LLID_QUEUE_CONFIG_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_pon_llid_queue_config 
( 
    const UINT32                        llid,
    const OAM_PON_LLID_QUEUE_CONFIG_T   *llid_queue_config 
);


/**
* This function retrieves llid queue config
*
*@param [IN]
*    llid                   : LLID id
*
*@param [OUT]
*    llid_queue_config      : See OAM_PON_LLID_QUEUE_CONFIG_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_get_llid_queue_config 
( 
    const UINT32                 llid,
    OAM_PON_LLID_QUEUE_CONFIG_T  *lid_queue_config 
);


/** 
* This function set multi llid admin control
*
*@param [IN]
*    num_of_llid_activated  : number of llid activated
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_set_multi_llid_admin_control 
( 
    const UINT32 num_of_llid_activated
);


/** 
* An event of ONU authentication response port_id+Password.
* OLT needs to confirm ONU loid_data.
*
*@param [IN]
*
*@param [OUT]
*    num_of_llid_activated : TRUE/PON_FALSE - authentication succeed/failed   
*
*@return
*    Return codes are not specified for an event
*
*/
STATUS oam_stack_pon_get_multi_llid_admin_control
( 
    UINT32 *num_of_llid_activated
);


/** 
* This function set mdu/mtu management global parameter 
*
*@param [IN]
*    OAM_MNG_GLOBAL_PARAM_T: See OAM_MNG_GLOBAL_PARAM_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_mux_mng_global_configure 
( 
    const OAM_MNG_GLOBAL_PARAM_T *parameter
);


/** 
* This function retrieves mdu/mtu management global parameter 
*
*@param [OUT]
*    OAM_MNG_GLOBAL_PARAM_T: See OAM_MNG_GLOBAL_PARAM_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_mux_mnm_global_configure 
( 
    OAM_MNG_GLOBAL_PARAM_T *parameter
);


/** 
* This function set mdu/mtu management snmp parameter 
*
*@param [IN]
*    OAM_MNG_SNMP_PARAM_CONF_T : See OAM_MNG_SNMP_PARAM_CONF_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_mxu_mng_snmp_parameter_config 
( 
    const OAM_MNG_SNMP_PARAM_CONF_T *parameter
);


/** 
* This function set mdu/mtu management snmp parameter 
*
*@param [IN]
*    OAM_MNG_SNMP_PARAM_CONF_T  : See OAM_MNG_SNMP_PARAM_CONF_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_mxu_mng_snmp_parameter_config 
( 
    OAM_MNG_SNMP_PARAM_CONF_T *parameter
);


/** 
* This function sets loop detect activation status for an Ethernet port or a DSL port of a specified ONU
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number
*@param [IN]
*      loop_detect: Enable or disable loop detect for the port.
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_set_management_object_port_loop_detect 
( 
    const UINT32   slot_id, 
    const UINT32   port_id,
    const UINT32   *loop_detect
);


/** 
* This function retrieves loop detect activation status for an Ethernet port or a DSL port of a specified ONU
*
*@param  [IN] 
*    slot_id                  : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id             : Ethernet pots number
*
*@param [OUT]
*    loop_detect         : Is loop detect enabled for the port
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_get_port_management_object_loop_detect 
( 
    const UINT32 slot_id,
    const UINT32 port_id,
    UINT32       *loop_detect
);


/** 
* This function set ONU holdover configure 
*
*@param [IN]
*    OAM_PON_HOLDOVER_STATE_T    : See OAM_PON_HOLDOVER_STATE_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_set_holdover_configure(const OAM_PON_HOLDOVER_STATE_T *parameter);


/** 
* This function get ONU holdover state 
*
*@param [IN]
*    OAM_PON_HOLDOVER_STATE_T   : See OAM_PON_HOLDOVER_STATE_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_get_holdover_state(OAM_PON_HOLDOVER_STATE_T *parameter);


/**
* This function set ONU Active PON_IF state 
*
*@param [IN]
*    OAM_PON_ACTIVE_IF_STATE_T    : See OAM_PON_ACTIVE_IF_STATE_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_set_if_active_config(const OAM_PON_ACTIVE_IF_STATE_T *parameter);


/** 
* This function get ONU Active PON_IF state 
*
*@param [IN]
*    OAM_PON_ACTIVE_IF_STATE_T   : See OAM_PON_ACTIVE_IF_STATE_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_pon_get_active_pon_if_state(OAM_PON_ACTIVE_IF_STATE_T *parameter);


/** 
* This function retrieves voip iad info
*
*@param [OUT]
*    iad_info : See OAM_VOIP_IAD_INFO_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_iad_info_get(OAM_VOIP_IAD_INFO_T *iad_info);


/** 
* This function get voip pots number
*
*@param [OUT]
*    port_cnt     : pots number 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_port_count_get(UINT32 *port_cnt);


/** 
* This function get voip pots number
*
*@param [IN]
*    slot_id                : OLT id, range: PON_MIN_slot_id - PON_MAX_slot_id
*    port_id                : ONU id, range: PON_MIN_port_id_PER_OLT - PON_MAX_port_id_PER_OLT
*
*@param [OUT]
*    pots_status         : See OAM_PORT_ON_OFF_STATE_E for details 
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_port_status_get(UINT32 slot, UINT32 port, OAM_PORT_ON_OFF_STATE_E *pots_status);


/** 
* This function get voip pots number
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number
*
*@param [OUT]
*    pots_status       : See OAM_PORT_ON_OFF_STATE_E for details 
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_port_admin_set(UINT32 slot, UINT32 port, const OAM_PORT_ON_OFF_STATE_E pots_status);


/** 
* This function retrieves voip global param conf
*
*@param [IN]
*
*@param [OUT]
*        global_param         : See OAM_VOIP_GLOBAL_PARAM_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_global_parameter_config_get(OAM_VOIP_GLOBAL_PARAM_T *global_param);


/** 
* This function sets voip global param conf
*
*@param [IN]
*    global_param  : See OAM_VOIP_GLOBAL_PARAM_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_global_parameter_config_set(const OAM_VOIP_GLOBAL_PARAM_T *global_param);


/** 
* This function sets h248 param config
*
*@param [IN]
*    h248_param : See OAM_VOIP_H248_PARAM_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_h248_parameter_config_set(const OAM_VOIP_H248_PARAM_T *h248_param);


/** 
* This function retrieves h248 param config
*
*@param [IN]
*
*@param [OUT]
*    h248_param : See OAM_VOIP_H248_PARAM_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_h248_parameter_config_get(OAM_VOIP_H248_PARAM_T *h248_param);


/** 
* This function retrieves h248 user tid config
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number
*
*@param [OUT]
*    number_of_entries       : Number of valid entries in h248_user_tid_array
*    h248_user_tid           : See OAM_VOIP_H248_USER_TID_CONFIG_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_h248_user_tid_configure_get
(
    const UINT32                        slot_id,
    const UINT32                        port_id,
    OAM_VOIP_H248_USER_TID_CONFIG_T     *h248_user_tid_
);


/**
* This function set h248 user tid config
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number
*@param [IN]
*    h248_user_tid     : See OAM_VOIP_H248_USER_TID_CONFIG_T for details 
*
*@param [OUT]
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_h248_user_tid_configure_set
( 
    const UINT32                            slot_id, 
    const UINT32                            port_id,
    const OAM_VOIP_H248_USER_TID_CONFIG_T   *h248_user_tid
);


/** 
* This function retrieves H248 RTP TID info
*
*@param [IN]
*
*@param [OUT]
*    h248_rtp_tid_info   : See OAM_VOIP_H248_RTP_TID_INFO_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_h248_rtp_tid_configure_get(OAM_VOIP_H248_RTP_TID_CONFIG_T *h248_rtp_tid_info);


/** 
* This function sets h248 user tid config
*
*@param [IN]
*    h248_rtp_tid_info        : See OAM_VOIP_H248_USER_TID_CONFIG_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_h248_rtp_tid_configure_set(const OAM_VOIP_H248_RTP_TID_CONFIG_T *h248_rtp_tid_info);


/** 
* This function sets h248 user tid config
*
*@param [IN]
*    parar_config  : See OAM_VOIP_H248_RTP_TID_INFO_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_h248_rtp_tid_info_get(OAM_VOIP_H248_RTP_TID_INFO_T *parar_config);


/** 
* This function sets sip param config
*
*@param [IN]
*    sip_param      : See OAM_VOIP_SIP_PARAM_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_sip_param_config_set(const OAM_VOIP_SIP_PARAM_T *sip_param);


/** 
* This function retrieves sip param config
*
*@param [IN]
*
*@param [OUT]
*    sip_param      : See OAM_VOIP_SIP_PARAM_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_sip_param_config_get(OAM_VOIP_SIP_PARAM_T *sip_param);


/**
* This function sets sip user param config
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number
*@param [IN]
*    sip_user_param    : See OAM_VOIP_SIP_USER_PARAM_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_sip_user_param_config_set
( 
    UINT32                           slot_id, 
    UINT32                           port_id,
    const OAM_VOIP_SIP_USER_PARAM_T  *sip_user_param
);


/** 
* This function retrieves sip user param config
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number
*
*@param [OUT]
*    number_of_entries       : Number of valid entries in sip_user_param_array
*    sip_user_param_array    : See OAM_VOIP_SIP_USER_PARAM_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_sip_user_param_config_get
(
    UINT32                      slot_id, 
    UINT32                      port_id,
    OAM_VOIP_SIP_USER_PARAM_T   *sip_user_param_array
);


/** 
* This function sets voip fax config
*
*@param [IN]
*    voip_fax : See OAM_VOIP_FAX_CONFIG_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_fax_config_set(const OAM_VOIP_FAX_CONFIG_T *voip_fax);


/** 
* This function retrieves voip fax config
*
*
*@param [OUT]
*    voip_fax      : See OAM_VOIP_FAX_CONFIG_T for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_fax_config_get(OAM_VOIP_FAX_CONFIG_T *voip_fax);


/** 
* This function retrieves voip iad oper status
*
*
*@param [OUT]
*    iad_oper_status         : See OAM_VOIP_H248_OPER_STATUS_E for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_h248_operation_status_get(OAM_VOIP_H248_OPER_STATUS_E *iad_oper_status);


/** 
* This function retrieves voip port userif status
*
*@param  [IN] 
*    slot_id           : slot - for MUD, 0 for SFU/HUG
*@param  [IN] 
*    port_id           : Ethernet pots number
*
*@param [OUT]
*    number_of_entries : Number of valid entries in pots_status_array
*    pots_status_array : See oam_stack_voip_pots_status_array_t for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_pots_status_get
( 
    UINT32                  slot_id,
    UINT32                  port_id,
    OAM_VOIP_POTS_STATUS_T  *pots_status_array
);


/** 
* This function resets voip iad
*
*@param [IN]
*    operation_type  : See OAM_VOIP_IAD_OPER_E for details 
*
*@return
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_iad_operation(const OAM_VOIP_IAD_OPER_E operation_type);


/**
* This function sets SIP digit map
*
*@param [IN]
*    sip_digit_map    : See OAM_SIP_DIGIT_MAP_T for details 
*
* Return codes:
*    All the defined oam_stack_* return codes
*
*/
STATUS oam_stack_voip_sip_digit_map_set(const OAM_SIP_DIGIT_MAP_T *sip_digit_map);


/* Set alarm admin
**
** This function set onu alarm control.
**
** Input Parameters:
**    slot_id               : OLT id, range: PON_MIN_slot_id - PON_MAX_slot_id
**    port_id               : ONU id, range: PON_MIN_port_id_PER_OLT - PON_MAX_port_id_PER_OLT
**    alarm_id              : as to the object that donot need tlv , ignored
**    alarm_admin           : See CTC_spec_alarm_admin_state_t for details.
**
**
** Return codes:
**    All the defined oam_stack_* return codes
**
*/
STATUS oam_stack_alarm_set_admin(UINT32 slot, UINT32 port, OAM_ALARM_ID_E alarm_id, OAM_OAM_ALARM_CTRL_STATE_E admin_state);


/* Get alarm admin
**
** This function get onu alarm control.
**
** Input Parameters:
**    slot_id            : OLT id, range: PON_MIN_slot_id - PON_MAX_slot_id
**    port_id            : ONU id, range: PON_MIN_port_id_PER_OLT - PON_MAX_port_id_PER_OLT
**    alarm_admin        : See CTC_spec_alarm_admin_state_t for details.
**
**
** Return codes:
**    All the defined oam_stack_* return codes
**
*/
STATUS oam_stack_alarm_get_admin (UINT32 slot, UINT32 port, OAM_ALARM_ID_E alarm_id, OAM_ALARM_ADMIN_STATE_T *admin_state);


/* Get alarm thrshold
**
** This function get onu alarm thrshold.
**
** Input Parameters:
**    slot_id               : OLT id, range: PON_MIN_slot_id - PON_MAX_slot_id
**    port_id               : ONU id, range: PON_MIN_port_id_PER_OLT - PON_MAX_port_id_PER_OLT
**    alarm_threshold       : See OAM_ALARM_THRESHOLD_T for details.
**
**
** Return codes:
**    All the defined oam_stack_* return codes
**
*/
STATUS oam_stack_alarm_get_threshold (UINT32 slot, UINT32 port, OAM_ALARM_ID_E alarm_id, OAM_ALARM_THRESHOLD_T *alarm_threshold);


/* Set alarm thrshold
**
** This function get onu alarm thrshold.
**
** Input Parameters:
**    slot_id            : OLT id, range: PON_MIN_slot_id - PON_MAX_slot_id
**    port_id            : ONU id, range: PON_MIN_port_id_PER_OLT - PON_MAX_port_id_PER_OLT
**    alarm_threshold    : See OAM_ALARM_THRESHOLD_T for details.
**
**
** Return codes:
**    All the defined oam_stack_* return codes
**
*/
STATUS oam_stack_alarm_set_threshold(UINT32 slot, UINT32 port, OAM_ALARM_ID_E alarm_id, OAM_ALARM_THRESHOLD_T  *alarm_threshold);


/* CTC spec alarm send api
**
** This function get onu alarm thrshold.
**
** Input Parameters:
**    send_alarm_buff       :alarm struct refer to CTC.V21 spec.
**    length                : 
**
**
** Return codes:
**    All the defined oam_stack_* return codes
**
*/
STATUS oam_stack_alarm_general_send_func(UINT8 *send_alarm_buff, UINT16 length);


STATUS oam_stack_sw_update(UINT8 image_id, UINT8 *fileName);


STATUS oam_stack_sw_update_state_get(OAM_SW_UPDATE_STATE_E *state);


STATUS oam_stack_image_active(UINT8 image_id);


STATUS oam_stack_get_active_image(UINT8 *image_id);


STATUS oam_stack_image_commit(UINT8 image_id);


STATUS oam_stack_pon_get_base_llid_num(UINT32 *base_llid_num);


STATUS oam_stack_get_image_number(UINT8 *image_number);


STATUS oam_stack_pon_get_if_num(UINT32 *pon_if_num);


STATUS oam_stack_pon_get_llid_num(UINT32 *llid_num);


STATUS oam_stack_custom_oui_handler_get(UINT32 custom_oui, OAM_CUSTOM_OUI_RECEIVE_HOOK_T *oam_receive_handle);


STATUS oam_stack_custom_oui_handler_register(UINT32 custom_oui, OAM_CUSTOM_OUI_RECEIVE_HOOK_T oam_receive_handle);

#endif /** _OAM_STACK_EXPO_H__ */

