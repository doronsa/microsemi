/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : PON Stack                                                 **/
/**                                                                          **/
/**  FILE        : oam_stack_frame_defs.h                                    **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM frame                                   **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.          
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_FRAME_DEFS_H__
#define __OAM_STACK_FRAME_DEFS_H__

#define ETHERNET_MTU                          1518 /* The length (in bytes) of an Ethernet MTU                                 */
#define OAMPDU_MAX_DATA_SIZE                  1496
#define OAM_DEFAULT_MAX_NEGOTIATED_OAMPDU     ETHERNET_MTU
#define OAM_ETHERNET_HEADER_SIZE              18   /* The size of the Ethernet and OAM header in bytes                         */
#define OAM_ETHERNET_STD_TLV_SIZE             16   /* The size of a TLV (local & remote) of the OAM-INF                        */
#define OAM_ETHERNET_EX_TLV_STATIC_SIZE       7    /* The size of the static fields in the extended TLV of the OAM-INF (without the version list, which is dynamic in size) */
#define OAM_ETHERNET_VEND_EXT_HEADER_SIZE     4    /* The size of the OAM vendor extension header                              */
#define OAM_ETHERNET_VEND_EXT_DBA_HEADER_SIZE 3    /* The size of the header of the header of the CTC DBA OAM vendor extension */

#define OAM_OPCODE_OFFSET                     17
#define OAM_EXT_OPCODE_OFFSET                 21

#define ETHERNET_VTAGGED_VID_MASK          0X0FFF
#define ETHERNET_VTAGGED_TPID_MASK         0XFFFF0000

#define ETHERNET_IP_EXTENSION_VERSION      0X46
#define ETHERNET_IP_PDU_EXTENSION_LENGTH   24 /*Extension ip header length, include the 'option' field*/
#define ETHERNET_IP_PDU_COMM_LENGTH        20

#define OAM_ETHERNET_TYPE_OFFSET           12
#define OAM_ETHERNET_SUB_TYPE_OFFSET       14
#define OAM_ETHERNET_CODE_OFFSET           17

#define OAM_NOTIFY_INFO_LEN_MIN            42
#define OAM_NOTIFY_INFO_LEN_MAX            1496

#define OAM_ETHERNET_HEADER_MAC_LEN        6

#define ETHERNET_TYPE_SLOW_PROTOCOL        0X8809
#define ETHERNET_TYPE_SLOW_PROTOCOL_OAM    0X03

#define ETHERNET_OAM_FLAG_CRITICAL_EVENT   0X04
#define ETHERNET_OAM_FLAG_DYING_GASP       0X02
#define ETHERNET_OAM_FLAG_LINK_FAULT       0X01

#define ETHERNET_OAM_INFORMATION_TLV_END_MASK    0X0
#define ETHERNET_OAM_INFORMATION_TLV_LOCAL_TYPE  0X1
#define ETHERNET_OAM_INFORMATION_TLV_REMOTE_TYPE 0X2

#define ETHERNET_OAM_INFORMATION_TLV_LENGTH      0x10
#define ETHERNET_OAM_INFORMATION_OAM_VERSION     0x01

#define OAM_TLV_STATE_LOOPBACK_ENABLE            0x05
#define OAM_TLV_STATE_LOOPBACK_DISABLE           0x0

/*OAM configuration field offset*/
#define OAM_VAR_RETRIEVAL_SUPPORT_OFFSET 4
#define OAM_LINK_EVENTS_SUPPORT_OFFSET   3
#define OAM_LOOPBACK_SUPPORT_OFFSET      2
#define OAM_UNIDIRECTION_SUPPORT_OFFSET  1
#define OAM_ACTIVE_MODE_OFFSET           0

#define ETHERNET_OAM_INFORMATION_TLV_POSITION_BASE    0
#define ETHERNET_OAM_INFORMATION_TLV_TYPE_OFFSET      ETHERNET_OAM_INFORMATION_TLV_POSITION_BASE
#define ETHERNET_OAM_INFORMATION_TLV_LENGTH_OFFSET    (ETHERNET_OAM_INFORMATION_TLV_TYPE_OFFSET+1)
#define ETHERNET_OAM_INFORMATION_TLV_VERSION_OFFSET   (ETHERNET_OAM_INFORMATION_TLV_LENGTH_OFFSET+1)
#define ETHERNET_OAM_INFORMATION_TLV_REVERSION_OFFSET (ETHERNET_OAM_INFORMATION_TLV_VERSION_OFFSET+1)
#define ETHERNET_OAM_INFORMATION_TLV_STATE_OFFSET     (ETHERNET_OAM_INFORMATION_TLV_REVERSION_OFFSET+2)
#define ETHERNET_OAM_INFORMATION_TLV_CONFIGURE_OFFSET (ETHERNET_OAM_INFORMATION_TLV_STATE_OFFSET+1)
#define ETHERNET_OAM_INFORMATION_TLV_PDUCFG_OFFSET    (ETHERNET_OAM_INFORMATION_TLV_CONFIGURE_OFFSET+1)
#define ETHERNET_OAM_INFORMATION_TLV_OUI_OFFSET       (ETHERNET_OAM_INFORMATION_TLV_PDUCFG_OFFSET+2)
#define ETHERNET_OAM_INFORMATION_TLV_VERDOR_OFFSET    (ETHERNET_OAM_INFORMATION_TLV_OUI_OFFSET+3)

#define ETHERNET_OAM_FLAG_DISCOVERY_LOCAL_CANNOT_COMPLETED  0X0
#define ETHERNET_OAM_FLAG_DISCOVERY_LOCAL_NOT_COMPLETED     0X1
#define ETHERNET_OAM_FLAG_DISCOVERY_LOCAL_COMPLETED         0X2

#define ETHERNET_OAM_FLAG_LOCAL_STABLE_OFFSET       3
#define ETHERNET_OAM_FLAG_REMOTE_STABLE_OFFSET      5

#define ETHERNET_OAM_FLAG_LOCAL_STABLE_MASK         0x18
#define ETHERNET_OAM_FLAG_REMOTE_STABLE_MASK        0x60

#define ETHERNET_OAM_FLAG_DISCOVERY_NOT_COMPLETED   0X08
#define ETHERNET_OAM_FLAG_DISCOVERY_WAIT_FOR_REMOTE 0X28
#define ETHERNET_OAM_FLAG_DISCOVERY_LOCAL_SATISFIED 0X30

#define ETHERNET_OAM_FLAG_DISCOVERY_COMPLETED       0x50

#define ETHERNET_OAM_STATE_FILED_FWD_NON_OAMPDU     0
#define ETHERNET_OAM_STATE_FILED_DSFWD_NON_OAMPDU   6

#define ETHERNET_OAM_LOCAL_INFO_TLV_LEN   16
#define ETHERNET_OAM_REMOTE_INFO_TLV_LEN  16
/* First layer of OAM codes: */

/* Second layer of opcodes, OAM Vendor Extension types for CTC defined: */
#define OAM_VENDOR_EXT_EX_VAR_REQUEST     0x01 /* Extended Variable Request */
#define OAM_VENDOR_EXT_EX_VAR_RESPONSE    0x02 /* Extended Variable Response */
#define OAM_VENDOR_EXT_SET_REQUEST        0x03 /* Set Request */
#define OAM_VENDOR_EXT_SET_RESPONSE       0x04 /* Set Response */

#define OAM_VENDOR_EXT_ONU_AUTHENTICATION 0x05 /* Onu auth */
#define CTC_AUTH_ONU_ID_DEFAULT           "MRVL"
#define CTC_AUTH_ONU_PASSWD_DEFAULT       "MRVL"

#define CTC_AUTH_ONU_OAM_AUTH_REQUEST   0X01
#define CTC_AUTH_ONU_OAM_AUTH_RESPONSE  0X02
#define CTC_AUTH_ONU_OAM_AUTH_SUCESS    0X03
#define CTC_AUTH_ONU_OAM_AUTH_FAILURE   0X04

#define CTC_AUTH_REQUEST_DATA_SIZE      0x01 
#define CTC_AUTH_RESPONSE_DATA_SIZE     0x25 
#define CTC_AUTH_NAK_RESPONSE_DATA_SIZE 0x02
#define CTC_AUTH_SUCCESS_DATA_SIZE      0x00 
#define CTC_AUTH_FAILURE_DATA_SIZE      0x01 
#define CTC_AUTH_LOID_DATA_NOT_USED     "NOT_USED" 

#define OAM_VENDOR_EXT_FILE_TRANSFER    0x06 /* File Transfer (TFTP) session frame */

#define OAM_VENDOR_EXT_ENC              0x09 /* ENC (Churning) configuration */
#define OAM_VENDOR_EXT_DBA              0x0a /* DBA reports configuration */

#define OAM_VENDOR_EXT_EVENT            0xFF /* Event related extended OAM message */
#define CTC_EVENT_STATUS_REQUEST        0x01 /* Event Statues_Request*/
#define CTC_EVENT_STATUS_SET            0x02 /* Event Statues_Set*/
#define CTC_EVENT_STATUS_RESPONSE       0x03 /* Event Statues_Response*/
#define CTC_EVENT_THRESHOLD_REQUEST     0x04 /* Event Threshold Request*/
#define CTC_EVENT_THRESHOLD_SET         0x05 /* Event Threshold Set*/
#define CTC_EVENT_THRESHOLD_RESPONSE    0x06 /* Event Threshold Response*/

/*add temp*/
#define OAM_VENDOR_GET_VER              0x21 /* DBA reports configuration */
#define OAM_DISCOVER_FOR_C1K            0xFF /* discover frame for c1000-OLT */
/*end*/
#define EX_OAM_INF_INFO_TYPE            0xfe /* Extended OAM Discovery, InfoType field value */

#define CTC_ONU_TLV_V_2_1_ALL_PORT      0xffff
#define CTC_ONU_TLV_V_2_0_ALL_PORT      0x0ff
#define CTC_ONU_TLV_V_COM_ALL_PORT      0x0ffff

#define CTC_ONU_TLV_V_2_1_TLV_INDEX     0X37
#define CTC_ONU_TLV_V_2_0_TLV_INDEX     0X36

#define CTC_2_1_ONU_TLV_PORT_MASK       0x0000FFFF
#define CTC_2_1_ONU_TLV_PORT_TYPE_MASK  0xFF000000
#define CTC_2_1_ONU_TLV_ONU_TRUNK_MASK  0x00C00000
#define CTC_2_1_ONU_TLV_SLOT_MASK       0x003F0000

#define MAX_PARAMETER_LENGTH            1000

/*define for V2.0 port & object range*/
#define CTC_MIN_PON_PORT_NUMBER         0x0
#define CTC_MAX_PON_PORT_NUMBER         0x0

#define CTC_MIN_ETH_PORT_NUMBER         0x1
#define CTC_MIX_POTS_PORT_NUMBER        0x1

#define CTC_MAX_PON_PORT_NUMBER_DEFAULT CTC_MIN_PON_PORT_NUMBER

#define CTC_MIN_PON_LLID_ID             0

#define CTC_MIN_ETHERNET_PORT_NUMBER_V20 1
#define CTC_MAX_ETHERNET_PORT_NUMBER_V20 0x4F

#define CTC_MIN_VOIP_PORT_NUMBER_V20   0x50
#define CTC_MAX_VOIP_PORT_NUMBER_V20   0x8F

#define CTC_MIN_E1_PORT_NUMBER_V20     0x90
#define CTC_MAX_E1_PORT_NUMBER_V20     0x9F

/*Define for V2.1 port & object range*/
#define CTC_MIN_PON_LLID_ID_V21        1

/*According to CTC2.1(20091012, PON IF should start from 0)*/
//#define CTC_MIN_PON_IF_ID_V21 1
#define CTC_MIN_PON_IF_ID_V21          0
#define CTC_MIN_VOIP_IF_ID_V21         1

#define CTC_MIN_VOIP_PORT_NUMBER_V21   1
#define CTC_MIN_E1_PORT_NUMBER_V21     1

#define CTC_COM_FRAME_IRRELEVANT_ID    0
#define CTC_COM_SLOT_IRRELEVANT_ID     0
#define CTC_COM_PORT_IRRELEVANT_ID     0

/* DBA OAM vendor extension sub-types (third layer of opcodes)  */
#define DBA_VENDOR_EXT_GET_REQUEST     0x00 /* get_DBA_request  */
#define DBA_VENDOR_EXT_GET_RESPONSE    0x01 /* get_DBA_response */
#define DBA_VENDOR_EXT_SET_REQUEST     0x02 /* set_DBA_request  */
#define DBA_VENDOR_EXT_SET_RESPONSE    0x03 /* set_DBA_response */

#define DBA_VENDOR_EXT_REQUEST_ACK     0x01 /* set_DBA_request  */
#define DBA_VENDOR_EXT_REQUEST_NACK    0X0  /* set_DBA_response */

#define DBA_OAM_INFO_BRANCH_LEAF_STEP_LEN 3 /*the len of Branch&leaf field*/

/* ENC OAM vendor extension sub-types (third layer of opcodes) */
#define ENC_VENDOR_EXT_KEY_REQUEST     0x00 /* new_key_request  */
#define ENC_VENDOR_EXT_KEY_RESPONSE    0x01 /* new_churning_key */

/* Offsets from the begining of the Variable Descriptor: */
#define OAM_VENDOR_EXT_EX_VAR_OFFSET_BRANCH 0 /* The offset of the Branch code */
#define OAM_VENDOR_EXT_EX_VAR_OFFSET_LEAF   1 /* The offset of the leaf code */
#define OAM_VENDOR_EXT_EX_VAR_OFFSET_WIDTH  3 /* The offset of the width field */
#define OAM_VENDOR_EXT_EX_VAR_OFFSET_DATA   4 /* The offset of the data */

/*Oam Externed Oam discovery head def*/
#define OAM_TLV_HEAD_WIDH_REQUIRED_V20 1
#define OAM_TLV_HEAD_WIDH_REQUIRED_V21 4

#define CTC_OAM_DISCOVER_HEAD_INFO_TEYP            0XFE
#define CTC_OAM_DISCOVER_HEAD_INFO_TEYP_OFFSIZE    0
#define CTC_OAM_DISCOVER_HEAD_LENGTH_OFFSIZE       1
#define CTC_OAM_DISCOVER_HEAD_OUI_OFFISIZE         2
#define CTC_OAM_DISCOVER_HEAD_DATA_PAYLOAD_OFFSIZE 5
#define CTC_OAM_DISCOVER_HEAD_EXT_SUPPORT_OFFSIZE  CTC_OAM_DISCOVER_HEAD_DATA_PAYLOAD_OFFSIZE
#define CTC_OAM_DISCOVER_HEAD_EXT_VERSION_OFFSIZE  6
#define CTC_OAM_DISCOVER_HEAD_NEGO_VERSION_ENTRIES_OFFSIZE 7

#define CTC_OAM_DISCOVER_HEAD_OUI_LENGTH     3
#define CTC_OAM_DISCOVER_HEAD_VERSION_LENGTH 1

#define CTC_OAM_DISCOVER_VER_ACKNOWLEDGEMENT_LEN 7
#define CTC_OAM_DISCOVER_VER_LIST_PER_LEN  (sizeof(OAM_EXTENDED_VER_ITEM_T))

#define CTC_DISCOVER_EXP_VERSION_DEFAULT 0 

#define CTC_DISCOVER_EXP_NOT_SUPPORT 0X0
#define CTC_DISCOVER_EXP_SUPPORT     0X1

#define CTC_ONU_VER_MAX_SUPPORT_CNT 5

#define CTC_ONU_HOLDOVER_MIN_TIME 50
#define CTC_ONU_HOLDOVER_DEFAULT_TIME 200
#define CTC_ONU_HOLDOVER_MAX_TIME 1000

#endif /* __OAM_STACK_FRAME_DEFS_H__ */
