/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_stack_eth.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : Definition of Ethernet module                             **/
/**                                                                          **/
/****************************************************************************** 
**                                                                             
*   MODIFICATION HISTORY:                                                      
*
*    12/Jan/2011   - initial version created.         
*                                                                              
******************************************************************************/

#ifndef __OAM_STACK_ETH_H__
#define __OAM_STACK_ETH_H__

/**************************************************************************************
Leaf: C7-0x004 - Get Ethernet sw ports bitmap

Paramaters:

NOTE: 
**************************************************************************************/
typedef STATUS (*oam_eth_get_port_bitmap_t)(const UINT32 slot, UINT64 *eth_bitmap, UINT64 *ge_bitmap);

/**************************************************************************************
Leaf: C7-0x004 - Get Ethernet ports port number

Paramaters:

NOTE: 
**************************************************************************************/
typedef STATUS (*oam_eth_get_port_number_t)(const UINT32 slot, UINT32 *min_port, UINT32 *max_port);


/**************************************************************************************
Leaf: C7-0x0012 - Get Ethernet ports flow control status

Paramaters:
    port (IN):   The number of the port to retrieve the information for.
    flow_stats (OUT):       The current flow-control status of the given <port>.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for getting flow control status.
**************************************************************************************/
typedef STATUS (*oam_eth_get_flow_control_status_t)(const UINT32 slot, UINT32 port_i, OAM_PORT_PAUSE_STATE_E *flow_stats);


/**************************************************************************************
Leaf: C7-0x0012 - Set Ethernet ports flow control status

Paramaters:
    port (IN):              The port to set with the new flow-control configuration.
    flow_stat_in (IN):      Is the configuration requested to set for <port>.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for setting flow control status.
**************************************************************************************/
typedef STATUS (*oam_eth_set_flow_control_status_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_PAUSE_STATE_E *flow_stat_in);


/**************************************************************************************
Leaf: C7-0x0013 - Get Ethernet ports upstream policer

Paramaters:
    port (IN):              The port number to retreive the us policer parameters for.
    policer_params (OUT):   A pre-allocated structure of a policer-parameters entry, in which
                            to put the information about the Ethernet <port>.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for getting the policer parameters.
**************************************************************************************/
typedef STATUS (*oam_eth_get_policer_params_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_US_POLICING_ENTRY_T *policer_params);


/**************************************************************************************
Leaf: C7-0x0013 - Set Ethernet ports upstream policer

Paramaters:
    port (IN):                  The port to set the us policer params for.
    policer_params_in (IN):     The policer params for the Ethernet <port> to set.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for setting the policer parameters.
**************************************************************************************/
typedef STATUS (*oam_eth_set_policer_params_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_US_POLICING_ENTRY_T *policer_params_in);


/**************************************************************************************
Leaf: C7-0x0016 - Get Ethernet ports rate limit

Paramaters:
    port (IN):              The port number to retreive the rate limit parameters for.
    rate_limit_params (OUT):   A pre-allocated structure of a rate_limit-parameters entry, in which
                            to put the information about the Ethernet <port>.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for getting the rate_limit parameters.
**************************************************************************************/
typedef STATUS (*oam_eth_get_rate_limit_params_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_DS_RATE_LIMITING_ENTRY_T *rate_limit_params);


/**************************************************************************************
Leaf: C7-0x0013 - Set Ethernet ports rate limit

Paramaters:
    port (IN):                  The port to set the rate_limit params for.
    rate_limit_params_in (IN):     The rate_limit params for the Ethernet <port> to set.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for setting the rate_limit parameters.
**************************************************************************************/
typedef STATUS (*oam_eth_set_rate_limit_params_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_DS_RATE_LIMITING_ENTRY_T *rate_limit_params_in);


/**************************************************************************************
Leaf: C7-0x0016 - Get Ethernet ports loop detection state

Paramaters:
    port (IN):              The port number to retreive the rate limit parameters for.
    loop_detect_enable (OUT):   A pre-allocated structure of a loop detection, in which
                            to put the information about the Ethernet <port>.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for getting the loop detection parameters.
**************************************************************************************/
typedef STATUS (*oam_eth_get_loop_detect_status_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_LOOPBACK_DETECTION_ADMIN_E *loop_detect_enable);

/**************************************************************************************
Leaf: C7-0x0018 - Set Disable Looped State

Paramaters:
    port (IN):              The port number to retreive the rate limit parameters for.
    disable_loop_state (OUT):   A pre-allocated structure of a loop detection, in which
                            to put the information about the Ethernet <port>.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for getting the loop detection parameters.
**************************************************************************************/
typedef STATUS (*oam_eth_set_disable_looped_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_DISABLE_LOOPED_STATE_E *disable_loop_state);

/**************************************************************************************
Leaf: C7-0x0018 - Get Disable Looped state

Paramaters:
    port (IN):              The port number to retreive the rate limit parameters for.
    disable_loop_state (OUT):   A pre-allocated structure of a loop detection, in which
                            to put the information about the Ethernet <port>.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for getting the loop detection parameters.
**************************************************************************************/
typedef STATUS (*oam_eth_get_disable_looped_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_DISABLE_LOOPED_STATE_E *disable_loop_state);

/**************************************************************************************
Leaf: C7-0x0019 - Set MAC Aging Time

Paramaters:
    port (IN):              The port number to retreive the rate limit parameters for.
    aging_time (OUT):       UINT32 variable, in which to put the information about the Ethernet <port>.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for getting the loop detection parameters.
**************************************************************************************/
typedef STATUS (*oam_eth_set_mac_aging_time_t)(const UINT32 slot, UINT32 port_id, UINT32 *aging_time);

/**************************************************************************************
Leaf: C7-0x0019 - Get MAC Aging Time

Paramaters:
    port (IN):              The port number to retreive the rate limit parameters for.
    aging_time (OUT):       UINT32 variable, in which to put the information about the Ethernet <port>.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for getting the loop detection parameters.
**************************************************************************************/
typedef STATUS (*oam_eth_get_mac_aging_time_t)(const UINT32 slot, UINT32 port_id, UINT32 *aging_time);

/**************************************************************************************
Leaf: C7-0x0013 - Set Ethernet ports loop detection

Paramaters:
    port (IN):                  The port to set the rate_limit params for.
    loop_detect_enable (IN):     The loop detection params for the Ethernet <port> to set.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI port
      is available for setting the loop detection parameters.
**************************************************************************************/
typedef STATUS (*oam_eth_set_loop_detect_status_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_LOOPBACK_DETECTION_ADMIN_E *loop_detect_enable);


/**************************************************************************************
Leaf: C7-0x0021 - Get Ethernet port configured VLAN mode

Paramaters:
    port (IN):              The Ethernet port for which to return the VLAN mode information.
    vlan_port_info (OUT):   The VLAN mode & information configured to the given Ethernet port.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for getting the vlan information.
**************************************************************************************/
typedef STATUS (*oam_eth_get_vlan_modes_t)(const UINT32 slot, UINT32 port_id, OAM_ETH_PORT_VLAN_CONF_T *vlan_port_info);


/**************************************************************************************
Leaf: C7-0x0021 - Set Ethernet port VLAN mode

Paramaters:
    port (IN):              The port to set VLAN configuration for.
    vlan_mode_in (IN):      The information to set to a given port.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for setting the vlan information.
**************************************************************************************/
typedef STATUS (*oam_eth_set_vlan_mode_t)(const UINT32 slot, UINT32 port_id, OAM_ETH_PORT_VLAN_CONF_T *vlan_mode_in);

/**************************************************************************************
Leaf: C7-0x1021 - Get Ethernet port configured QinQ VLAN mode

Paramaters:
    port (IN):              The Ethernet port for which to return the QinQ VLAN mode information.
    qinq_port_info (OUT):   The QinQ VLAN mode & information configured to the given Ethernet port.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for getting the qinq vlan information.
**************************************************************************************/
typedef STATUS (*oam_eth_get_qinq_modes_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_QINQ_CONFIG_T *qinq_port_info);


/**************************************************************************************
Leaf: C7-0x1021 - Set Ethernet port QinQ VLAN mode

Paramaters:
    port (IN):              The port to set QinQ VLAN configuration for.
    qinq_port_info (IN):      The information to set to a given port.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for setting the QinQ vlan information.
**************************************************************************************/
typedef STATUS (*oam_eth_set_qinq_mode_t)(const UINT32 slot, UINT32 port_id, const OAM_PORT_QINQ_CONFIG_T *qinq_port_info);


/**************************************************************************************
Leaf: C7-0x0031 - Get Classification & Marking information,
    currently supports only the physical-port-based classification & marking.

Paramaters:
    port (IN):                 The Ethernet port number for which to retrieve the
                               C&M information.
    class_n_mark_conf (OUT):   The C&M information retrieved. This is an allocated array, as
                               well as the port number requested.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for setting the vlan information.
**************************************************************************************/
typedef STATUS (*oam_eth_get_class_n_mark_t)(const UINT32 slot, UINT32 port_id, OAM_CLASS_N_MARK_T *class_n_mark_conf);

                                                    
/**************************************************************************************
Leaf: C7-0x0031 - Set Classification & Marking information,
    currently supports only the physical-port-based classification & marking,
    for Set-Queue operation.

Paramaters:
    port (IN):                The Ethernet port number for which to set the
                              C&M information.
    class_n_mark_conf (IN):   The C&M information to set. This is an allocated array, as
                               well as the port number requested.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for setting the vlan information.
**************************************************************************************/
typedef STATUS (*oam_eth_set_class_n_mark_t)(UINT32 slot, UINT32 port_id, OAM_CLASS_N_MARK_T *class_n_mark_conf);


/**************************************************************************************
Leaf: C7-0x0041 - Get Multicast VLAN filters configured for a certain Ethernet port

Paramaters:
    port (IN):                      The Ethernet port number for which to retrieve the
                                    Multicast VLAN fiter information.
    multicast_vlan_filters (OUT):   The multicast VLAN filters information. This is an allocated array, as
                                    well as the port number requested.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for getting the multicast VLAN filter information.
**************************************************************************************/
typedef STATUS (*oam_eth_get_multicast_vlan_t)(UINT32 slot,UINT32 port_id, OAM_MC_VLAN_T *multicast_vlan_filters);


/**************************************************************************************
Leaf: C7-0x0041 - Set Multicast VLAN filters configured for a certain Ethernet port

Paramaters:
    multicast_vlan_filters (IN):    The multicast VLAN filters information. This is an allocated array, as
                                    well as the port number to set.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for setting the multicast VLAN filter information.
**************************************************************************************/
typedef STATUS (*oam_eth_set_multicast_vlan_t)(UINT32 slot, UINT32 port_id, OAM_MC_VLAN_T *multicast_vlan_filters);


/**************************************************************************************
Leaf: C7-0x0042 - Get Multicast Control.

Paramaters:
    port (IN):                  The Ethernet port number for which to retrieve the
                                Multicast control information.
    multicast_control (OUT):    The multicast control information related to the given port.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for getting the required information.
**************************************************************************************/
typedef STATUS (*oam_eth_get_multicast_control_t)(OAM_MC_CTRL_T *multicast_control);


/**************************************************************************************
Leaf: C7-0x0042 - Set Multicast Control configured for a certain Ethernet port

Paramaters:
    multicast_control (IN): The multicast control information. This is an allocated array, as
                            well as the port number to set.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for getting the required information.
**************************************************************************************/
typedef STATUS (*oam_eth_set_multicast_control_t)(OAM_MC_CTRL_T *multicast_control);


/**************************************************************************************
Leaf: C7-0x0045 - Get Multicast Group count.

Paramaters:
    port (IN):                  The port to retrieve information for.
    multicast_group_info (OUT): The count of multicast groups supported by this port.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for getting the required information.
**************************************************************************************/
typedef STATUS (*oam_eth_get_multicast_group_count_t)(const UINT32 slot, UINT32 port_id, UINT8 *multicast_group_info);


/**************************************************************************************
Leaf: C7-0x0045 - Set Multicast Group count.

Paramaters:
    port (IN):                      The port to set.
    multicast_group_info_in (IN):   The Etherent port count to set.

NOTE: The absence of this hook is permitted, and then it is assumed only the UNI & PON ports
      are available for getting the required information.
**************************************************************************************/
typedef STATUS (*oam_eth_set_multicast_group_count_t)( UINT32 slot, UINT32 port_id, UINT8 *multicast_group_info_in);


/**************************************************************************************
Leaf: C7-0x0046 - Get fast leave ability of ONU.

Paramaters:
    oam_stack_fast_leave_capability_t (OUT):   Reserve fast leave ability gotten.

**************************************************************************************/
typedef STATUS (*oam_eth_get_fast_leave_ability_t)(OAM_MC_FAST_LEAVE_ABILITY_T *ability);


/**************************************************************************************
Leaf: C7-0x0047 - Get fast leave admin state.

Paramaters:
    oam_stack_fast_leave_state_t (OUT):   Reserve fast leave state gotten.

**************************************************************************************/
typedef STATUS (*oam_eth_get_fast_leave_state_t)(OAM_FAST_LEAVE_ADMIN_STATE_E *enabled);


/**************************************************************************************
Hook Prototype for a general case Ethernet Port Action

Paramaters:
    Port    - The Port on which to perform the action

NOTE: If this hook is not provided, activating relevant actions will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_set_auto_neg_admin_t)(UINT32 slot, UINT32 port_id, OAM_PORT_NEGA_STATE_E *admin);


/**************************************************************************************
Hook Prototype for a general case Ethernet Port Action

Parameters:
    Port    - The Port on which to perform the action
    Action  - The BOOL action to perform on the port

    answer (OUT):           The answer to the SetRequest.

NOTE: If this hook is not provided, activating relevant actions will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_get_auto_neg_admin_t)(UINT32 slot, UINT32 port_id, OAM_PORT_NEGA_STATE_E *action);


/**************************************************************************************
Leaf: 0x0052 - Restart the aAutoNeg on a port

Parameters:
    port (IN):  The port number to retrieve information for
    info (OUT): The information retrieved

NOTE: If this hook is not provided, querying relevant attributes will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_restart_auto_action_t)(UINT32 slot, UINT32 port_id);


/**************************************************************************************
C9-0x0001 - Reset ONU

Use this hook to implement any soft reset required by the switch.
The PASONU Reset will be activated upon return from this hook function.
To revoke the reset and return a failure response return any value other than S_OK.

Paramaters: None

NOTE: The absence of this hook is permitted.
**************************************************************************************/
typedef STATUS (*oam_eth_ext_action_reset_t)(VOID);


/**************************************************************************************
C9-0x0048 - Set ONU fast leave state

Use this hook to set fast leave state required by the switch.
The PASONU fast leave admin control will be activated upon return from this hook function.

Paramaters: 
    enabled (IN) - The state for the fast leave function.

NOTE: The absence of this hook is permitted.
**************************************************************************************/
typedef STATUS (*oam_eth_set_fast_leave_action_t)(OAM_FAST_LEAVE_ADMIN_STATE_E *admin);


/**************************************************************************************
Leaf: C7-0x0043 - Get Multicast switch mode

Parameters:
    multicast_switch_mode (OUT):  multicast switch mode (operates on all switch ports) 

NOTE: If this hook is not provided, querying relevant attributes will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_get_multicast_switch_t)(OAM_MC_PROTOCOL_TYPE_E *multicast_switch_mode);


/**************************************************************************************
Leaf: C7-0x0043 - Set Multicast switch mode

Parameters:
    multicast_switch_mode (IN):  multicast switch mode (operates on all switch ports) 

NOTE: If this hook is not provided, querying relevant attributes will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_set_multicast_switch_t)(OAM_MC_PROTOCOL_TYPE_E *multicast_switch_mode);


/**************************************************************************************
Leaf: C7-0x0042 - Get Multicast Tag Stripe mode

Parameters:
    multicast_tag_strip_mode (OUT):  multicast tag stripe mode

NOTE: If this hook is not provided, querying relevant attributes will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_get_multicast_tag_strip_t)(UINT32 slot, UINT32 port_id, OAM_MC_PORT_TAG_OPER_T *multicast_tag_strip_mode);


/**************************************************************************************
Leaf: C7-0x0042 - Set Multicast Tag Stripe mode

Parameters:
    multicast_tag_strip_mode (IN):  multicast tag stripe mode

NOTE: If this hook is not provided, querying relevant attributes will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_set_multicast_tag_strip_t)(UINT32 slot, UINT32 port_id, OAM_MC_PORT_TAG_OPER_T *multicast_tag_strip_mode);


/**************************************************************************************
Leaf: C7-0x0011 - Get the ethernet port link state 

Parameters:
    port (IN):          The port number to retrieve information for
    link_state (OUT):   Pointer to return the link state  

NOTE: If this hook is not provided, querying relevant attributes will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_get_link_state_t)(UINT32 slot, UINT32 port_id, OAM_PORT_LINK_STATE_E *link_state);


/**************************************************************************************
Leaf: C7-0x0011 - Get the ethernet port admin state 

Parameters:
    port (IN):          The port number to retrieve information for
    link_state (OUT):   Pointer to return the link state  

NOTE: If this hook is not provided, querying relevant attributes will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_get_phy_admin_t)(UINT32 slot, UINT32 port_id, OAM_PORT_ADMIN_E *link_state);


/**************************************************************************************
Leaf: C7-0x0011 - Get the ethernet port admin state 

Parameters:
    port (IN):          The port number to retrieve information for
    link_state (OUT):   Pointer to return the link state  

NOTE: If this hook is not provided, querying relevant attributes will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_set_phy_admin_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_ADMIN_E *link_state);


/**************************************************************************************
Leaf: 0x0052 - Get the aAutoNegLocalTechnologyAbility information

Parameters:
    port (IN):  The port number to retrieve information for
    info (OUT): The information retrieved

NOTE: If this hook is not provided, querying relevant attributes will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_get_auto_local_technology_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_AUTO_NEGA_TECH_ABILITY_T *info);


/**************************************************************************************
Leaf: 0x0053 - Get the aAutoNegAdvertisedTechnologyAbility information

Parameters:
    port (IN):  The port number to retrieve information for
    info (OUT): The information retrieved

NOTE: If this hook is not provided, querying relevant attributes will cause CTC Stack to return a failure response
**************************************************************************************/
typedef STATUS (*oam_eth_get_auto_advertize_tech_t)(const UINT32 slot, UINT32 port_id, OAM_PORT_AUTO_NEGA_TECH_ABILITY_T *info);


/* <OAM_STACK_ETH_HOOK_FUNC_T> structure defines the hooks that are available for the application for supporting
   Ethernet functionality defined by CTC extended variable descriptors. */
typedef struct
{
    /* CTC extended set request & get request */
    oam_eth_get_port_bitmap_t           eth_get_port_bitmap_hook;
    oam_eth_get_port_number_t           eth_get_port_number_hook;
    oam_eth_get_flow_control_status_t   eth_get_flow_control_stats_hook;         /* Leaf C7-0x0012 */
    oam_eth_set_flow_control_status_t   eth_set_flow_control_stats_hook;         /* Leaf C7-0x0012 */
    oam_eth_get_policer_params_t        eth_get_us_policer_params_hook;          /* Leaf C7-0x0013 */
    oam_eth_set_policer_params_t        eth_set_us_policer_params_hook;          /* Leaf C7-0x0013 */
    oam_eth_get_rate_limit_params_t     eth_get_ds_rate_limit_params_hook;       /* Leaf C7-0x0016 */
    oam_eth_set_rate_limit_params_t     eth_set_ds_rate_limit_params_hook;       /* Leaf C7-0x0016 */
    oam_eth_set_loop_detect_status_t    eth_set_loop_detect_stats_hook;          /* Leaf C7-0x0017 */
    oam_eth_get_loop_detect_status_t    eth_get_loop_detect_stats_hook;          /* Leaf C7-0x0017 */
    oam_eth_set_disable_looped_t        eth_set_port_disable_looped_hook;        /* Leaf C7-0x0018 */
    oam_eth_get_disable_looped_t        eth_get_port_disable_looped_hook;        /* Leaf C7-0x0018 */
    oam_eth_set_mac_aging_time_t        eth_set_port_mac_aging_time_hook;        /* Leaf C7-0x0019 */
    oam_eth_get_mac_aging_time_t        eth_get_port_mac_aging_time_hook;        /* Leaf C7-0x0019 */
    oam_eth_get_vlan_modes_t            eth_get_vlan_modes_hook;                 /* Leaf C7-0x0021 */
    oam_eth_set_vlan_mode_t             eth_set_vlan_mode_hook;                  /* Leaf C7-0x0021 */
    oam_eth_get_qinq_modes_t            eth_get_qinq_modes_hook;                 /* Leaf C7-0x1021 */
    oam_eth_set_qinq_mode_t             eth_set_qinq_mode_hook;                  /* Leaf C7-0x1021 */
    oam_eth_get_class_n_mark_t          eth_get_class_n_mark_hook;               /* Leaf C7-0x0031 */
    oam_eth_set_class_n_mark_t          eth_set_class_n_mark_hook;               /* Leaf C7-0x0031 */
    oam_eth_get_multicast_vlan_t        eth_get_multicast_vlan_filter_hook;      /* Leaf C7-0x0041 */
    oam_eth_set_multicast_vlan_t        eth_set_multicast_vlan_filter_hook;      /* Leaf C7-0x0041 */
    oam_eth_get_multicast_tag_strip_t   eth_get_multicast_tag_strip_hook;        /* Leaf C7-0x0042 */
    oam_eth_set_multicast_tag_strip_t   eth_set_multicast_tag_strip_hook;        /* Leaf C7-0x0042 */
    oam_eth_get_multicast_switch_t      eth_get_multicast_switch_hook;           /* Leaf C7-0x0043 */
    oam_eth_set_multicast_switch_t      eth_set_multicast_switch_hook;           /* Leaf C7-0x0043 */
    oam_eth_get_multicast_control_t     eth_get_multicast_control_hook;          /* Leaf C7-0x0044 */
    oam_eth_set_multicast_control_t     eth_set_multicast_control_hook;          /* Leaf C7-0x0044 */
    oam_eth_get_multicast_group_count_t eth_get_multicast_group_count_hook;      /* Leaf C7-0x0045 */
    oam_eth_set_multicast_group_count_t eth_set_multicast_group_count_hook;      /* Leaf C7-0x0045 */
    oam_eth_get_fast_leave_ability_t    eth_get_fast_leave_ability_hook;         /* Leaf C7-0x0046 */
    oam_eth_get_fast_leave_state_t      eth_get_fast_leave_admin_state_hook;     /* Leaf C7-0x0047 */
    oam_eth_set_fast_leave_action_t     eth_set_fast_leave_admin_state_hook;     /* Leaf C7-0x0047 */
    oam_eth_get_link_state_t            eth_get_link_state;                      /* Leaf C7-0x0011 */
    oam_eth_get_phy_admin_t             eth_std_attr_get_phy_admin_state;        /* Leaf 07-0x0025 */
    oam_eth_set_phy_admin_t             eth_action_phy_admin_control;            /* Leaf 09-0x0005 */
    oam_eth_set_auto_neg_admin_t        eth_action_auto_neg_admin_control;       /* Leaf 09-0x000C */
    oam_eth_get_auto_neg_admin_t        eth_std_attr_get_auto_neg_admin_state;   /* Leaf 07-0x004f */
    oam_eth_restart_auto_action_t       eth_action_auto_neg_restart_auto_config; /* Leaf 09-0x000B */
    oam_eth_get_auto_local_technology_t eth_std_attr_get_auto_local_tech;        /* Leaf 07-0x0052 */
    oam_eth_get_auto_advertize_tech_t   eth_std_attr_get_auto_advertize_tech;    /* Leaf 07-0x0053 */
} OAM_STACK_ETH_HOOK_FUNC_T;

#endif  /* __OAM_STACK_ETH_H__ */
