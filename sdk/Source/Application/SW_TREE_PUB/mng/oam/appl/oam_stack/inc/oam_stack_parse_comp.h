/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : oam_stack_parse_comp.h                                    **/
/**                                                                          **/
/**  DESCRIPTION : Parse and compose OAM function module                     **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.                
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_PARSE_COMP_H__
#define __OAM_STACK_PARSE_COMP_H__

/*============================= Include Files ===============================*/    
#include "oam_stack_comm.h"
#include "oam_stack_micro_parser.h"
#include "oam_stack_log.h"

#define BITS_IN_BYTE    8
#define BYTES_IN_LONG   4
#define BYTES_IN_DOUBLE 8
#define BYTES_IN_SYMBOL 4
#define BYTES_IN_WORD   2
#define BYTES_IN_CHAR   1


#define COMPARSER_STATUS_OK              OAM_EXIT_OK
#define COMPARSER_STATUS_ERROR           OAM_ERROR_EXIT
#define COMPARSER_STATUS_NOT_SUPPORTED   OAM_ERROR_NOT_SUPPORT
#define COMPARSER_STATUS_BAD_PARAM       OAM_ERROR_PARAMETER
#define COMPARSER_STATUS_TIMEOUT         OAM_ERROR_TIME_OUT
#define COMPARSER_STATUS_MEMORY_ERROR    OAM_ERROR_MEMORY
#define COMPARSER_STATUS_OUT_OF_RANGE    OAM_ERROR_OUT_OF_RANGE
#define COMPARSER_STATUS_WRONG_OPCODE    OAM_ERROR_PARAMETER
#define COMPARSER_STATUS_CTC_FAIL        OAM_ERROR_PARAMETER
#define COMPARSER_STATUS_END_OF_FRAME    OAM_ERROR_PARAMETER

#define COMPARSER_STATUS_SUCCESS         0x80
#define COMPARSER_STATUS_BADPARAM        0x86
#define COMPARSER_STATUS_NO_RESOURCE     0x87

#define OAM_MIN_CNM_CONTAINER_WIDTH      0x1

#define VLAN_AGGEGRATION_LENGTH 6   /*len of "DefaultVLAN  + Number of VLAN Aggregation tables" field*/

#define PON_MAC_ADDRESS_COPY(dst_address, src_address)  os_memmove(dst_address, src_address, OAM_MAC_ADDR_LEN);

#define OAM_IPv4_ADDR_COPY(dst_address, src_address)  os_memmove(dst_address, src_address, OAM_IPv4_ADDR_LEN);

#define OAM_IPv6_ADDR_COPY(dst_address, src_address)  os_memmove(dst_address, src_address, OAM_IPv6_ADDR_LEN);

/*=============================== Constants =================================*/
#define CTC_ENCRYPTION_KEY_SIZE           3    /* CTC encryption key size measured in Bytes */

#define MAX_OUI_RECORDS                   62   /* Max OUI+Version pairs in Extended OAM information frame*/


/*================================ Macros ==================================*/
#define CHECK_OPCODE_AND_RETURN(received_opcode,expected_opcode) \
{   if( (received_opcode) != (expected_opcode) )\
    {\
      OAM_LOG_ERROR_2 ( "Unexpected opcode: received-0x%x,expected-0x%x \n", (received_opcode),(expected_opcode));\
      return COMPARSER_STATUS_WRONG_OPCODE;\
    }\
}

#define CHECK_OPCODE_AND_RETURN_FUNC(func,received_opcode,expected_opcode) \
{   if( (received_opcode) != (expected_opcode) )\
    {\
      OAM_LOG_ERROR_2 ( "Unexpected opcode: received-0x%x,expected-0x%x \n",(received_opcode),(expected_opcode));\
      return COMPARSER_STATUS_WRONG_OPCODE;\
    }\
}

#define CHECK_BUFFER_SIZE_AND_RETURN( offset, total_length, buffer, size_to_write )\
{\
if((offset + size_to_write - buffer) > total_length)\
    {\
    OAM_LOG_ERROR_1("failed to compose command. Allocated buffer is too small %d\n", total_length);\
    return COMPARSER_STATUS_MEMORY_ERROR;\
    }\
    }

#define CHECK_BUFFER_SIZE_AND_RETURN_FUNC( _func, offset, total_length, buffer, size_to_write )\
{\
    if((offset + size_to_write - buffer) > total_length)\
    {\
    OAM_LOG_ERROR_1(  "failed to read or write. Allocated buffer is too small %d\n", total_length); \
        return COMPARSER_STATUS_MEMORY_ERROR;\
    }\
}

#define FILL_USHORT_ENDIANITY_IN_UBUFFER( x, offset, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN(offset, length, buffer, BYTES_IN_WORD)\
    USHORT_2_UBUFFER( (UINT32) (x), offset )\
    offset+= BYTES_IN_WORD;\
}

#define FILL_USHORT_ENDIANITY_IN_UBUFFER_FUNC( _func, x, offset, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, BYTES_IN_WORD)\
    USHORT_2_UBUFFER( (UINT32) (x), offset )\
    offset+= BYTES_IN_WORD;\
}

#define FILL_LIMITED_LONG_IN_3_BYTES_ENDIANITY_IN_UBUFFER_FUNC( _func, x, offset, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, 3)\
    LIMITED_ULONG_2_UBUFFER( x, offset )\
    offset+= 3;\
}

#define FILL_ULONG_ENDIANITY_IN_UBUFFER_FUNC( _func, x, offset, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, BYTES_IN_LONG)\
    ULONG_2_UBUFFER( x, offset )\
    offset+= BYTES_IN_LONG;\
}

#define FILL_ULONG_ENDIANITY_IN_UBUFFER(  x, offset, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN( offset, length, buffer, BYTES_IN_LONG)\
    ULONG_2_UBUFFER( x, offset )\
    offset+= BYTES_IN_LONG;\
}

#define FILL_ULONG_LONG_ENDIANITY_IN_UBUFFER_FUNC( _func, x, offset, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, BYTES_IN_DOUBLE)\
    INT64_2_UBUFFER( x, offset )\
    offset+= BYTES_IN_DOUBLE;\
}

#define EXTRACT_ULONG_LONG_ENDIANITY_FROM_UBUFFER_FUNC( _func, offset, x, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, 2*BYTES_IN_LONG)\
    UBUFFER_2_INT64( offset, x )\
    offset+= 2*BYTES_IN_LONG;\
}

#define FILL_BRANCH_LEAF_FUNC( _func, branch, leaf, offset, length, buffer )\
{ \
    FILL_UCHAR_ENDIANITY_IN_UBUFFER_FUNC(_func, branch, offset, length, buffer)\
    FILL_USHORT_ENDIANITY_IN_UBUFFER_FUNC(_func, leaf, offset, length, buffer)\
}

#define FILL_BRANCH_LEAF(  branch, leaf, offset, length, buffer )\
{ \
    FILL_UCHAR_ENDIANITY_IN_UBUFFER( branch, offset, length, buffer)\
    FILL_USHORT_ENDIANITY_IN_UBUFFER( leaf, offset, length, buffer)\
}

#define FILL_UCHAR_ENDIANITY_IN_UBUFFER( x, offset, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN(offset, length, buffer, BYTES_IN_CHAR)\
    UCHAR_2_ENDIANITY_UBUFFER( (UINT8) (x), offset )\
    offset+= BYTES_IN_CHAR;\
}

#define FILL_UCHAR_ENDIANITY_IN_UBUFFER_FUNC( _func, x, offset, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, BYTES_IN_CHAR)\
    UCHAR_2_ENDIANITY_UBUFFER( (UINT8) (x), offset )\
    offset+= BYTES_IN_CHAR;\
}

#define FILL_UCHAR_FORM_ENUM_ENDIANITY_IN_UBUFFER_FUNC( _func, x, offset, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, BYTES_IN_CHAR)\
    UCHAR_2_ENDIANITY_UBUFFER( (UINT8) (x), offset )\
    offset+= BYTES_IN_LONG;\
}

#define FILL_UBUFFER_ENDIANITY_IN_UBUFFER_FUNC( _func, xbuffer, size_to_write, offset, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, size_to_write)\
    UBUFFER_2_UBUFFER( offset, xbuffer, size_to_write )\
    offset+= size_to_write;\
}

#define FILL_MAC_ADDRESS_IN_BUFFER_FUNC( _func, x, offset, length, buffer )\
{ \
    memset(offset,0,OAM_MAC_ADDR_LEN);\
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, OAM_MAC_ADDR_LEN)\
    PON_MAC_ADDRESS_COPY( offset, x )\
    offset+= OAM_MAC_ADDR_LEN;\
}

#define FILL_OUI_IN_UBUFFER( oui, offset, length, buffer )\
{ \
    FILL_UCHAR_ENDIANITY_IN_UBUFFER((UINT8)((oui >>(BYTES_IN_WORD*BITS_IN_BYTE))& 0xff), offset, length, buffer)\
    FILL_UCHAR_ENDIANITY_IN_UBUFFER((UINT8)((oui >>BITS_IN_BYTE)& 0xff), offset, length, buffer)\
    FILL_UCHAR_ENDIANITY_IN_UBUFFER((UINT8)(oui & 0xff), offset, length, buffer)\
}

#define EXTRACT_USHORT_ENDIANITY_FROM_UBUFFER( offset, x, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN(offset, length, buffer, BYTES_IN_WORD)\
    UBUFFER_2_USHORT( offset, x )\
    offset+= BYTES_IN_WORD;\
}

#define EXTRACT_USHORT_ENDIANITY_FROM_UBUFFER_FUNC( _func, offset, x, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, BYTES_IN_WORD)\
    UBUFFER_2_USHORT( offset, x )\
    offset+= BYTES_IN_WORD;\
}

#define EXTRACT_UCHAR_ENDIANITY_FROM_UBUFFER( offset, x, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN(offset, length, buffer, BYTES_IN_CHAR)\
    WORD_UBUFFER_2_ENDIANITY_UCHAR( offset, x )\
        offset+= BYTES_IN_CHAR;\
}

#define EXTRACT_UCHAR_ENDIANITY_FROM_UBUFFER_FUNC( _func, offset, x, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, BYTES_IN_CHAR)\
    WORD_UBUFFER_2_ENDIANITY_UCHAR( offset, x )\
    offset+= BYTES_IN_CHAR;\
}

#define EXTRACT_ULONG_ENDIANITY_FROM_UBUFFER_FUNC( _func, offset, x, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, BYTES_IN_LONG)\
    UBUFFER_2_ULONG( offset, x )\
    offset+= BYTES_IN_LONG;\
}

#define EXTRACT_ULONG_ENDIANITY_FROM_UBUFFER( offset, x, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN(offset, length, buffer, BYTES_IN_LONG)\
    UBUFFER_2_ULONG( offset, x )\
    offset+= BYTES_IN_LONG;\
}

#define EXTRACT_LIMITED_LONG_ENDIANITY_FROM_UBUFFER_FUNC( _func, offset, x, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, 3)\
    UBUFFER_2_LIMITED_LONG( offset, x )\
    offset+= 3;\
}

#define EXTRACT_MAC_ADDRESS_FROM_BUFFER( __func, offset, x, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(__func, offset, length, buffer, OAM_MAC_ADDR_LEN)\
    PON_MAC_ADDRESS_COPY( x, offset )\
    offset+= OAM_MAC_ADDR_LEN;\
}

#define EXTRACT_IPv4_ADDR_FROM_BUFFER( __func, offset, x, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(__func, offset, length, buffer, OAM_IPv4_ADDR_LEN)\
    OAM_IPv4_ADDR_COPY( x, offset )\
    offset+= OAM_IPv4_ADDR_LEN;\
}

#define EXTRACT_IPv6_ADDR_FROM_BUFFER( __func, offset, x, length, buffer )\
{ \
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(__func, offset, length, buffer, OAM_IPv6_ADDR_LEN)\
    OAM_IPv6_ADDR_COPY( x, offset )\
    offset+= OAM_IPv6_ADDR_LEN;\
}

#define EXTRACT_OPCODE_RETURN_IF_ERROR_FUNC(__func, receive_pdu_data,_received_opcode_,_expected_code, length, buffer) \
{\
    EXTRACT_UCHAR_ENDIANITY_FROM_UBUFFER_FUNC(__func, receive_pdu_data, _received_opcode_, length, buffer)\
    CHECK_OPCODE_AND_RETURN(_received_opcode_,_expected_code)\
}

#define CHECK_BRANCH_AND_RETURN_FUNC(__func, __expected_branch__, __received_branch__) \
{   if( __received_branch__ != __expected_branch__ )\
    {\
        OAM_LOG_ERROR_2( "Wrong branch: received-0x%x,expected-0x%x \n", __received_branch__,__expected_branch__); \
      return OAM_ERROR_PARAMETER;\
    }\
}

#define CHECK_LEAF_AND_RETURN_FUNC(__func, __expected_leaf__, __received_leaf__) \
{   if( __received_leaf__ != __expected_leaf__ )\
    {\
        OAM_LOG_ERROR_2( "Wrong leaf: received-0x%x,expected-0x%x \n", __received_leaf__,__expected_leaf__); \
             return OAM_ERROR_PARAMETER;\
    }\
}

#define CHECK_LEAF_AND_RETURN( __expected_leaf__, __received_leaf__) \
{   if( __received_leaf__ != __expected_leaf__ )\
    {\
        OAM_LOG_ERROR_2( "Wrong leaf: received-0x%x,expected-0x%x \n", __received_leaf__,__expected_leaf__); \
             return OAM_ERROR_PARAMETER;\
    }\
}

#define CHECK_PORT_TYPE_AND_RETURN( _received_type_, _expected_type_ ) \
{   if( _expected_type_ != _received_type_ )\
    {\
        OAM_LOG_ERROR_2( "Wrong port type: received-0x%x,expected-0x%x \n", _received_type_, _expected_type_ ); \
      return OAM_ERROR_PARAMETER;\
    }\
}

#define CHECK_COM_COMPARE_AND_RETURN( _expected_type_, _received_type_,_prtLog) \
{   if( _expected_type_ != _received_type_ )\
    {\
        OAM_LOG_ERROR_2( "Unexpected received value: received-0x%x,expected-0x%x \n", _expected_type_,_received_type_); \
      return OAM_ERROR_PARAMETER;\
    }\
}

#define CHECK_COMPARE_RANGE_AND_RETURN( _expected_min_,_receved_max_, _received_type_) \
{   if(( _expected_min_ > _received_type_ )||(_receved_max_ < _received_type_))\
    {\
        OAM_LOG_ERROR_3( "Unexpected received value,expected [0x%x ~ 0x%x] ,received -0x%x \n", _expected_min_ ,_receved_max_,_received_type_); \
              return OAM_ERROR_OUT_OF_RANGE;\
    }\
}

#define EXTRACT_OUI_ENDIANITY_FROM_UBUFFER( offset, oui, length, buffer ) \
{ \
        CHECK_BUFFER_SIZE_AND_RETURN(offset, length, buffer, 3)\
        UBUFFER_2_LIMITED_LONG( offset, oui )\
        offset+= 3;\
}

#define EXTRACT_BRANCH_LEAF(__func, receive_pdu_data,expected_branch,expected_leaf, length, buffer) \
{\
    UINT8 received_branch; \
    UINT16 received_leaf; \
    EXTRACT_UCHAR_ENDIANITY_FROM_UBUFFER_FUNC(__func, receive_pdu_data, received_branch, length, buffer)\
    EXTRACT_USHORT_ENDIANITY_FROM_UBUFFER_FUNC(__func, receive_pdu_data, received_leaf, length, buffer)\
    CHECK_BRANCH_AND_RETURN_FUNC(__func, expected_branch, received_branch)\
    CHECK_LEAF_AND_RETURN_FUNC(__func, expected_leaf, received_leaf)\
}

#define EXTRACT_BRANCH_LEAF_OR_CHECK_IF_ZERO(__func, receive_pdu_data,expected_branch,expected_leaf, length, buffer) \
{\
    UINT8 received_branch; \
    UINT16 received_leaf; \
    if((receive_pdu_data + BYTES_IN_CHAR - buffer) > length)\
    {\
        return COMPARSER_STATUS_END_OF_FRAME;\
    }\
    WORD_UBUFFER_2_ENDIANITY_UCHAR( receive_pdu_data, received_branch )\
    receive_pdu_data+= BYTES_IN_CHAR;\
    if(received_branch == 0)\
        return COMPARSER_STATUS_END_OF_FRAME;\
    if((receive_pdu_data + BYTES_IN_WORD - buffer) > length)\
    {\
        return COMPARSER_STATUS_END_OF_FRAME;\
    }\
    UBUFFER_2_USHORT( receive_pdu_data, received_leaf )\
    receive_pdu_data+= BYTES_IN_WORD;\
}

#define EXTRACT_CODE_RETURN_IF_ERROR(receive_pdu_data,received_code,expected_code,result, length, buffer) \
{\
    EXTRACT_UCHAR_ENDIANITY_FROM_UBUFFER(receive_pdu_data, received_code, length, buffer)\
    if( received_code != expected_code )\
    {\
      OAM_LOG_ERROR_2("Unexpected code: received-0x%lx,expected-0x%lx \n", received_code,expected_code);\
      return result;\
    }\
}

#define EXTRACT_UBUFFER_ENDIANITY_FROM_UBUFFER_FUNC( _func, offset, x, length, buffer, size_to_read )\
{\
    CHECK_BUFFER_SIZE_AND_RETURN_FUNC(_func, offset, length, buffer, size_to_read)\
    UBUFFER_2_UBUFFER( x, offset, size_to_read )\
    offset+= size_to_read;\
}

#define EXTRACT_TFTP_OPCODE_RETURN_IF_ERROR(receive_pdu_data,received_opcode,expected_opcode, length, buffer) \
{\
    EXTRACT_USHORT_ENDIANITY_FROM_UBUFFER(receive_pdu_data, received_opcode, length, buffer)\
    if( received_opcode != expected_opcode )\
    {\
      OAM_LOG_ERROR_2("Received Wrong TFTP opcode. expected opcode:%d received opcode:%d\n", expected_opcode,received_opcode);\
      return COMPARSER_STATUS_WRONG_OPCODE;\
    }\
}

#define EXTRACT_TFTP_OPCODE_RETURN_IF_ERROR_FUNC(func,receive_pdu_data,received_opcode,expected_opcode, length, buffer) \
{\
    EXTRACT_USHORT_ENDIANITY_FROM_UBUFFER(receive_pdu_data, received_opcode, length, buffer)\
    if( received_opcode != expected_opcode )\
    {\
      OAM_LOG_ERROR_2("Received Wrong TFTP opcode. expected opcode:%d received opcode:%d\n", expected_opcode,received_opcode);\
      return COMPARSER_STATUS_WRONG_OPCODE;\
    }\
}

/*============================== Data Types =================================*/

/*========================= Functions Prototype =============================*/
#endif /* __OAM_STACK_PARSE_COMP_H__ */
