/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : oam_stack_os_linux.c                                      **/
/**                                                                          **/
/**  DESCRIPTION : Wrap Linux system function                                **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *    12/Jan/2011   - initial version created.               
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_OS_LINUX_H__
#define __OAM_STACK_OS_LINUX_H__

#ifdef LINUX
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <semaphore.h>
#include <sys/msg.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>

#include <pthread.h>

//#include "oam_stack_ossl.h"


#define OSSRV_MSG_Q_BASE_ID 0x09010001

/*
** PosSL defines
*/
#define OSSRV_INVALID_VALUE            (INT32)(-1)

/*
** wait option
*/
#define OSSRV_TIME_FOREVER            0xffffffff
#define OSSRV_TIME_NOWAIT            IPC_NOWAIT 

/*
** wait queue method
*/
#define OSSRV_MSG_SEND_EXCLUSION  3

#define OSSRV_MSG_SEND_TIMIE_OUT (1000*1000)

#define OSSRV_MSG_FIFO            0
/*
** first in first out wait queue
*/
#define OSSRV_WAIT_FIFO       SEM_Q_FIFO

/*
** priority wait queue
*/
#define OSSRV_WAIT_PRIORITY   SEM_Q_PRIORITY
/*
** task ID in vxworks platform
*/
typedef INT32     POS_TASKID;

typedef INT32     MSG_QUEUE_ID_T;

typedef INT32     SEM_ID_T ;

typedef key_t     SEM_KEY_T;

typedef pthread_t PID_T;

typedef struct 
{
    long mtype;    /* message type, must be > 0 */
    char mtext[1]; /* message data */
}MSG_Q_BUFF_T;


//extern INT32 msgget(SEM_KEY_T key, INT32 msgflg);
//extern INT32 msgsnd(INT32 msqid, const VOID *msgp, size_t msgsz, INT32 msgflg);
//extern INT32 msgrcv(INT32 msqid, VOID *msgp, size_t msgsz, long msgtyp, INT32 msgflg);
//extern INT32 msgctl(INT32 msqid, INT32 cmd, struct msqid_ds *buf);
//extern INT32 semop(INT32 semid, struct sembuf *sops, unsigned nsops);
//extern INT32 semtimedop(INT32 semid, struct sembuf *sops, unsigned nsops, struct timespec *timeout);

//extern INT32 errno;

SEM_ID_T oam_ossrv_sem_create(const char *func_name, SEM_ID_T *sem_id, UINT16 a_init_value);

INT32 oam_ossrv_sem_wait(const char *func_name, SEM_ID_T sem_id, UINT32 a_timeout);

INT32 oam_ossrv_sem_give(const char *func_name, SEM_ID_T sem_id);

INT32 oam_ossrv_sem_destroy(const char *func_name, SEM_ID_T sem_id);

MSG_QUEUE_ID_T oam_ossrv_msg_queue_create(const char *func_name, UINT32 dwMaxMsgs, UINT32 dwMaxMsgLength, UINT32 dwOptions);
 
INT32 oam_ossrv_msg_receive (const char *func_name, MSG_QUEUE_ID_T msgQID, VOID *pvBuf, UINT32 dwLength, UINT32 dwTimeout);

INT32 oam_ossrv_msg_send(const char *func_name, MSG_QUEUE_ID_T msgQID, VOID *pvBuf, UINT32 dwLength, UINT32 dwTimeout, UINT32 dwPriority);

INT32 oam_ossrv_msg_delete (const char *func_name, MSG_QUEUE_ID_T msgQID);

INT32 oam_linux_pthread_create(PID_T *pid, UINT8 *pthread_name, UINT8 prio, VOID *start_routine, VOID *arg);

INT32 oam_linux_pthread_close(PID_T  pid);

VOID oam_linux_task_delay(UINT32 time_in_ms);

INT32 oam_linux_task_id_get(VOID);

INT32 oam_linux_chmod(char *pathname,INT32 mode);

#define  __exit    _exit
#define  __chmod   oam_linux_chmod

#define OAM_OSSRV_sem_creat(func,m,i) (*(m) = oam_ossrv_sem_create(func, m,i))
#define OAM_OSSRV_sem_destory(func,m) (oam_ossrv_sem_destroy(func,m))
#define oam_ossrv_sem_wait(func,m,t) (oam_ossrv_sem_wait(func ,m, t))
#define oam_ossrv_sem_give(func,m) (oam_ossrv_sem_give(func,m))

#define OAM_OSSRV_wait_in_tick(tick)  oam_linux_task_delay((((tick)/(sysconf(_SC_CLK_TCK)))*1000))

#define OAM_OSSRV_task_pid_Get oam_linux_task_id_get


#define OAM_OSSRV_pthread_create(pid,pthread_name,prio,start_routine,arg) oam_linux_pthread_create(pid,pthread_name,prio,start_routine,arg)

#define OAM_OSSRV_pthread_close(pid) oam_linux_pthread_close(pid)

#endif /*LINUX*/
#endif /*__OAM_STACK_OS_LINUX_H__*/
