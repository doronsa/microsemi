/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : oam_stack_rx_tx.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM RX/TX related functions                 **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.                
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_RX_TX_H__
#define __OAM_STACK_RX_TX_H__

#define MRVL_PON_FIFO_MAX_LEN    84 /*The maximum length of EPON FIFO in bytes*/

MRVL_ERROR_CODE_T oam_stack_pon_send_oam_frame(UINT8 oam_code, UINT8 *data, UINT16 length, UINT16 llid);
MRVL_ERROR_CODE_T oam_stack_pon_receive_oam_frame(UINT8 *oam_code, UINT8 *data, UINT16 *length);

INT32 oam_stack_pon_send_oam_pdu_frame(OAM_CODE_FIELD_E oam_code, UINT8 *msg, INT32 length, UINT16 llid);
INT32 oam_stack_rx_tx_terminate(VOID);
INT32 oam_stack_rx_tx_init(VOID);

#endif /*__OAM_STACK_RX_TX_H__*/
