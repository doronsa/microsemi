/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : oam_stack_vendor.h                                        **/
/**                                                                          **/
/**  DESCRIPTION : Definition of vendor specific function                    **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.               
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_VENDOR_H__
#define  __OAM_STACK_VENDOR_H__

#include "oam_stack_ossl.h"
#include "oam_stack_comm.h"
#include "oam_stack_type_expo.h"
/*--------------------------------------------------------------------------------
---------------------------------------------------------------------------------*/
typedef enum
{
    VENDOR_SW_MARVEL = 0X1,
    VENDOR_SW_BROADCOM,    
    VENDOR_SW_OTHER,    
}VENDOR_SW_TYPE_E;

typedef enum
{
    VENDOR_PON_PMC = 0X1,
    VENDOR_PON_TK,    
    VENDOR_PON_MARVEL,
    VENDOR_PON_OTHER,
}VENDOR_PON_TYPE_E;

typedef struct
{
    VENDOR_PON_TYPE_E           pon_type;
    VENDOR_SW_TYPE_E            sw_type;
    OAM_ONU_TYPE_E              onu_type; 
}POS_PACKED VENDOR_SYS_INFO_T;

typedef INT32 (*ext_vendor_sys_get_pon_type)(VENDOR_PON_TYPE_E *pon_type);

typedef INT32 (*ext_vendor_sys_get_sw_type)(VENDOR_SW_TYPE_E *sw_type);

typedef INT32 (*ext_vendor_sys_get_serial_number)(OAM_ONU_SERIAL_NUM_T *info);

typedef INT32 (*ext_vendor_sys_get_firmware_version)(OAM_FIRMWARE_VERSION_T *info);

typedef INT32 (*ext_vendor_sys_get_chipset_id)(OAM_CHIPSET_ID_T *info);

typedef INT32 (*ext_vendor_sys_get_capabilities)(OAM_ONU_CAPABILITIES_T *info);

typedef INT32 (*ext_vendor_sys_get_capabilities_plus)(OAM_ONU_CAPABILITIES_PLUS_T *info);

typedef INT32 (*ext_vendor_sys_get_capabilities_plus_3)(OAM_ONU_CAPABILITIES_3_T *info);

typedef INT32 (*ext_vendor_sys_onu_rest)(VOID);

typedef INT32 (*ext_vendor_sys_onu_rest_card)(VOID);

typedef INT32 (*ext_vendor_sys_onu_mac_address_get)(OAM_ETH_MAC_ADDR_T mac_address);

typedef INT32 (*ext_vendor_sys_get_loid)(OAM_AUTH_LOID_DATA_T *info);

typedef INT32 (*ext_vendor_sys_set_loid)(OAM_AUTH_LOID_DATA_T *info);

typedef INT32 (*ext_vendor_sys_log_printfOut)(const char *format, ...);

typedef INT32 (*ext_vendor_sys_get_mxu_mng_global_para)(OAM_MNG_GLOBAL_PARAM_T *para);

typedef INT32 (*ext_vendor_sys_set_mxu_mng_global_para)(const OAM_MNG_GLOBAL_PARAM_T *para);

typedef INT32 (*ext_vendor_sys_get_mxu_mng_snmp_para)(OAM_MNG_SNMP_PARAM_CONF_T *para);

typedef INT32 (*ext_vendor_sys_set_mxu_mng_snmp_para)(const OAM_MNG_SNMP_PARAM_CONF_T *para);

typedef INT32 (*ext_vendor_sys_sw_upgrade)(UINT8 image_id, UINT8 *fileName);

typedef INT32 (*ext_vendor_sys_get_sw_upgrade_status)(OAM_SW_UPDATE_STATE_E *state);

typedef INT32 (*ext_vendor_sys_sw_activate)(UINT8 image_id);

typedef INT32 (*ext_vendor_sys_get_active_sw)(UINT8 *image_id);

typedef INT32 (*ext_vendor_sys_sw_commit)(UINT8 image_id);

typedef INT32 (*ext_vendor_sys_get_image_num)(UINT8 *image_num);


typedef struct
{
    ext_vendor_sys_get_pon_type             sys_get_pon_type;
    ext_vendor_sys_get_sw_type              sys_get_sw_type;
    ext_vendor_sys_get_serial_number        sys_get_serial_number;
    ext_vendor_sys_get_firmware_version     sys_get_firmware_version;
    ext_vendor_sys_get_chipset_id           sys_get_chipset;
    ext_vendor_sys_get_capabilities         sys_get_capbility;
    ext_vendor_sys_get_capabilities_plus    sys_get_capbility_plus;
    ext_vendor_sys_get_capabilities_plus_3  sys_get_capbility_plus_3;
    ext_vendor_sys_onu_rest                 ext_onu_rest;
    ext_vendor_sys_onu_rest_card            ext_onu_rest_card;
    ext_vendor_sys_get_loid                 ext_get_onu_loid;
    ext_vendor_sys_set_loid                 ext_set_onu_loid;
    ext_vendor_sys_log_printfOut            sys_log_outPrtf ;
    ext_vendor_sys_get_mxu_mng_global_para  sys_get_mux_mng_global_para;
    ext_vendor_sys_set_mxu_mng_global_para  sys_set_mux_mng_global_para;
    ext_vendor_sys_get_mxu_mng_snmp_para    sys_get_mux_mng_snmp_para;
    ext_vendor_sys_set_mxu_mng_snmp_para    sys_set_mux_mng_snmp_para;
    ext_vendor_sys_onu_mac_address_get      sys_get_onu_mac_address;
    /*update*/
    ext_vendor_sys_sw_upgrade               sys_sw_update;
    ext_vendor_sys_get_sw_upgrade_status    sys_sw_update_get;
    ext_vendor_sys_get_active_sw            sys_sw_active_get;
    ext_vendor_sys_sw_activate              sys_sw_active;
    ext_vendor_sys_sw_commit                sys_sw_commit;
    ext_vendor_sys_get_image_num            sys_sw_image_num_get;
}OAM_STACK_SYS_FUNC_HOOK_T;

#endif /*__OAM_STACK_VENDOR_H__*/
