/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOPON OAM Stack                                           **/
/**                                                                          **/
/**  FILE        : oam_stack_update.h                                        **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM upgrading module                        **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *    12/Jan/2011   - initial version created.               
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_UPDATE_H__
#define __OAM_STACK_UPDATE_H__

#define OAM_SW_VERSION_MAX_LEN              50
#define OAM_UPDATE_MODE_STRING_LENGTH       10
#define OAM_UPDATE_ERROR_MSG_STRING_LENGTH  30
#define OAM_UPDATE_FILE_FORMAT_MODE         "Octet"
#define OAM_UPDATE_HEAD_LEN                 5

#define OAM_UPDATE_FILE_PATH                "/tmp/"
#define OAM_UPDATE_FILE_PATH_NAME_LEN       100

#define OAM_UDPDATE_COMMMON_HEAD_LEN        5  /*1 dataType + 2 len + 2 tid*/

#define OAM_UPDATE_FILE_BLOCK_START_SEQUENCE_NEXT   1
#define OAM_UPDATE_FILE_BLOCK_START_SEQUENCE_START  1
#define OAM_UPDATE_FILE_BLOCK_START_SEQUENCE_ACK    0
#define OAM_UPDATE_FILE_BLOCK_PIECE_LENGTH_MAX      1400

#define OAM_UPDATE_FILE_TOTAL_LENGTH_MAX            (100*1024)  

typedef enum
{
    OAM_UPDATE_TFTP_TRANSFER_DATA     = 0X1,
    OAM_UPDATE_FILE_VERIFICATION,            
    OAM_UPDATE_ACTIVE_IMAGE,
    OAM_UPDATE_COMMIT_IMAGEE
}OAM_UPDATE_ACTION_TYPE_E;

typedef enum
{
    OAM_UPDATE_STATE_IDLE             = 0X0,
    OAM_UPDATE_FILE_WRITE_REQUEST     = 0X02,
    OAM_UPDATE_TRANSFER_DATE_REQUEST  = 0X03,            
    OAM_UPDATE_TRANSFER_ACK_SUCCESS   = 0X04,
    OAM_UPDATE_TRANSFER_ACK_FAILED    = 0X05,
    OAM_UPDATE_END_DOWNLOAD_REQUEST   = 0X06,
    OAM_UPDATE_END_DOWNLOAD_RESPONSE  = 0X07,    
    OAM_UPDATE_ACTIVE_IMAGE_REQUEST   = 0X08,
    OAM_UPDATE_ACTIVE_IMAGE_RESPONSE  = 0X09,
    OAM_UPDATE_COMMIT_IMAGE_REQUEST   = 0X0A,
    OAM_UPDATE_COMMIT_IMAGE_RESPONSE  = 0X0B
}OAM_UPDATE_OPCODE_TYPE_E;

typedef struct
{
    OAM_UPDATE_OPCODE_TYPE_E          opcode;
    INT8                              fileName[OAM_SW_VERSION_MAX_LEN];
    UINT8                             method[OAM_UPDATE_MODE_STRING_LENGTH];
}OAM_UPDATE_FILE_WRITE_REQUEST_T;

typedef struct
{
    OAM_UPDATE_OPCODE_TYPE_E          opcode;   
    UINT16                            block_sequence;
    UINT8                             *block_addr;
    UINT16                            block_len;
}OAM_UPDATE_FILE_TRANSFER_REQUEST_T;

typedef struct
{
    OAM_UPDATE_OPCODE_TYPE_E          opcode;   
    UINT16                            block_sequence;
    UINT16                            error_code;
    UINT8                             error_msg[OAM_UPDATE_ERROR_MSG_STRING_LENGTH];
}OAM_UPDATE_TRANSFER_ACK_T;

typedef struct
{
    UINT32                            file_size;
}OAM_UPDATE_END_DOWNLOAD_REQUEST_T;

typedef enum
{
    OAM_TFTP_RPS_CODE_VERIFICATION_SUCCESS = 0,
    OAM_TFTP_RPS_CODE_STILL_WRITING_SOFTWARE,
    OAM_TFTP_RPS_CODE_VERIFICATION_ERROR,
    OAM_TFTP_RPS_CODE_PARAMETER_ERROR,
    OAM_TFTP_RPS_CODE_NOT_SUPPORTED
}OAM_UPDATE_RESPONSE_TYPE_E;

typedef struct
{
    OAM_UPDATE_RESPONSE_TYPE_E            rspcode;
}OAM_UPDATE_END_DOWNLOAD_RESPONSE_T;

typedef enum
{
    OAM_UPDATE_ACTIVE_IMAGE_ACK_SUCCESS     = 0,
    OAM_UPDATE_ACTIVE_IMAGE_ACK_PARAMETER_ERROR,
    OAM_UPDATE_ACTIVE_IMAGE_ACK_NOT_SUPPORTED,
    OAM_UPDATE_ACTIVE_IMAGE_ACK_LOAD_FAILED = 5
}OAM_UPDATE_ACTIVE_IMAGE_ACK_TYPE_E;

typedef struct
{
    UINT8                                 flag;
}OAM_UPDATE_ACTIVE_IMAGE_REQUEST_T;

typedef struct
{
    OAM_UPDATE_ACTIVE_IMAGE_ACK_TYPE_E    ack;
}OAM_UPDATE_ACTIVE_IMAGE_RESPONSE_T;

typedef enum
{
    TFTP_COMMIT_IMAGE_ACK_SUCCESS         = 0,
    TFTP_COMMIT_IMAGE_ACK_PARAMETER_ERROR,
    TFTP_COMMIT_IMAGE_ACK_NOT_SUPPORTED,
    TFTP_COMMIT_IMAGE_ACK_LOAD_FAILED     = 5,
}OAM_UPDATE_COMMIT_IMAGE_ACK_TYPE_E;

typedef struct
{
    UINT8                                 flag;
}OAM_UPDATE_COMMIT_IMAGE_REQUEST_T;

typedef struct
{
    OAM_UPDATE_COMMIT_IMAGE_ACK_TYPE_E    ack;
}OAM_UPDATE_COMMIT_IMAGE_RESPONSE_T;

typedef struct
{
    OAM_UPDATE_OPCODE_TYPE_E              update_state;
    INT8                                  ver_file_name[OAM_SW_VERSION_MAX_LEN];
    UINT32                                last_update_block; 
    UINT32                                next_expect_block; 
    UINT8                                 *alloc_buff;
    UINT32                                alloc_buff_length;
    UINT32                                curr_ver_file_lenth;
    UINT8                                 last_update_image;
}OAM_UPDATE_RECORD_T;

INT32 oam_stack_parse_oam_update_common_header
(      
    UINT8                           *receive_buffer,       
    const UINT16                    length,  
    OAM_UPDATE_ACTION_TYPE_E        *received_data_type,
    UINT16                          *received_payload_length ,
    UINT16                          *received_buf_length 
);

INT32 oam_stack_compose_oam_update_common_header
(
    const OAM_UPDATE_ACTION_TYPE_E  data_type,
    const UINT16                    payload_length,
    const UINT16                    expect_tid,
    UINT8                           *send_buffer,
    UINT16                          *send_buf_length
);

INT32 oam_stack_compose_file_write_request
(      
    const OAM_UPDATE_FILE_WRITE_REQUEST_T  *file_write_request,
    UINT8                           *pdu_data,
    UINT16                          *length 
);

INT32 oam_stack_parse_file_write_request
(      
    UINT8                           *receive_buffer,
    const UINT16                    length,
    OAM_UPDATE_FILE_WRITE_REQUEST_T *file_write_response,
    UINT16                          *received_buf_length
);

INT32 oam_stack_compose_file_transfer_data
(      
    const OAM_UPDATE_FILE_TRANSFER_REQUEST_T  *transfer_date_recode,
    UINT8                           *pdu_data,
    UINT16                          *length
);

INT32 oam_stack_parse_file_transfer_data
(     
    UINT8                          *receive_buffer,
    const UINT16                   length,
    OAM_UPDATE_FILE_TRANSFER_REQUEST_T  *file_date_record,
    UINT16                         *received_buf_length 
);

INT32 oam_stack_parse_end_download_response
(      
    UINT8                          *receive_buffer,
    const UINT16                   length,
    OAM_UPDATE_END_DOWNLOAD_RESPONSE_T  *download_response,
    UINT16                         *received_buf_length 
);

INT32 oam_stack_compose_end_download_response
(      
    const OAM_UPDATE_END_DOWNLOAD_RESPONSE_T  *transfer_date_recode,
    UINT8                          *pdu_data,
    UINT16                         *length 
);

INT32 oam_stack_compose_end_download_request
(      
    const OAM_UPDATE_END_DOWNLOAD_REQUEST_T  *transfer_date_recode,
    UINT8                          *pdu_data,
    UINT16                         *length 
);

INT32 oam_stack_parse_end_download_request
(      
    UINT8                          *receive_buffer,
    const UINT16                   length,
    OAM_UPDATE_END_DOWNLOAD_REQUEST_T  *download_response,
    UINT16                         *received_buf_length 
);

INT32 oam_stack_compose_activate_image_request
(      
    const OAM_UPDATE_ACTIVE_IMAGE_REQUEST_T    *active_image,
    UINT8                          *pdu_data,
    UINT16                         *length 
);

INT32 oam_stack_parse_activate_image_request
(      
    UINT8                          *receive_buffer,
    const UINT16                   length,
    OAM_UPDATE_ACTIVE_IMAGE_REQUEST_T  *active_image,
    UINT16                         *received_buf_length 
);

INT32 oam_stack_compose_activate_image_response
(     
    OAM_UPDATE_ACTIVE_IMAGE_RESPONSE_T  *active_image,
    UINT8                          *pdu_data,
    UINT16                         *length 
);

INT32 oam_stack_parse_activate_image_response
(      
    UINT8                          *receive_buffer,
    const UINT16                   length,
    OAM_UPDATE_ACTIVE_IMAGE_RESPONSE_T  *image_active,
    UINT16                         *received_buf_length 
);

INT32 oam_stack_compose_commit_image_request
(      
    const OAM_UPDATE_COMMIT_IMAGE_REQUEST_T  *commit_image,
    UINT8                          *pdu_data,
    UINT16                         *length 
);

INT32 oam_stack_parse_commit_image_request
(      
    UINT8                          *receive_buffer,
    const UINT16                   length,
    OAM_UPDATE_COMMIT_IMAGE_RESPONSE_T  *commit_image,
    UINT16                         *received_buf_length 
);

INT32 oam_stack_compose_commit_image_response
(      
    const OAM_UPDATE_COMMIT_IMAGE_RESPONSE_T  *commit_image,
    UINT8                          *pdu_data,
    UINT16                         *length 
);

INT32 oam_stack_parse_commit_image_response
(      
    UINT8                          *receive_buffer,
    const UINT16                   length,
    OAM_UPDATE_COMMIT_IMAGE_RESPONSE_T  *commit_image,
    UINT16                         *received_buf_length
);

INT32 oam_stack_parse_file_transfer_response
(      
    UINT8                          *receive_buffer,
    const UINT16                   length,
    OAM_UPDATE_TRANSFER_ACK_T      *response_ack,
    UINT16                         *received_buf_length 
);

INT32  oam_stack_compose_file_transfer_response
(      
    const OAM_UPDATE_TRANSFER_ACK_T *transfer_response,
    UINT8                          *pdu_data,
    UINT16                         *length 
);

#endif /*__OAM_STACK_UPDATE_H__*/
