/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : PON Stack                                                 **/
/**                                                                          **/
/**  FILE        : oam_stack_list.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM list function                           **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *  
 *    12/Jan/2011   - initial version created.         
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_STACK_LIST_H__
#define __OAM_STACK_LIST_H__

#include <stddef.h>

typedef struct list_head {
        struct list_head *prev, *next;
}LIST_HEAD_T;

#define INIT_LIST_HEAD(name_ptr)   do {(name_ptr)->next = (name_ptr);(name_ptr)->prev = (name_ptr);}while (0)

#define OFFSET(type, member)            (char *)&(((type *)0x0)->member)
#define CONTAINER_OF(ptr, type, member) ({(type *)((char * )ptr - OFFSET(type, member)); });

/**
 * LIST_FOR_EACH        -       iterate over a list
 * @pos:        the &struct list_head to use as a loop cursor.
 * @head:       the head for your list.
 */
#define LIST_FOR_EACH(pos, head) \
        for (pos = (head)->next; pos != (head); \
                pos = pos->next)
                
/**
 * LIST_FOR_EACH_PREV   -       iterate over a list backwards
 * @pos:        the &struct list_head to use as a loop cursor.
 * @head:       the head for your list.
 */
#define LIST_FOR_EACH_PREV(pos, head) \
        for (pos = (head)->prev; pos != (head); \
                pos = pos->prev)

/**
 * LIST_FOR_EACH_SAFE - iterate over a list safe against removal of list entry
 * @pos:        the &struct list_head to use as a loop cursor.
 * @n:          another &struct list_head to use as temporary storage
 * @head:       the head for your list.
 */
#define LIST_FOR_EACH_SAFE(pos, n, head) \
        for (pos = (head)->next, n = pos->next; pos != (head); \
                pos = n, n = pos->next)

/**
 * LIST_FOR_EACH_PREV_SAFE - iterate over a list backwards safe against removal of list entry
 * @pos:        the &struct list_head to use as a loop cursor.
 * @n:          another &struct list_head to use as temporary storage
 * @head:       the head for your list.
 */
#define LIST_FOR_EACH_PREV_SAFE(pos, n, head) \
        for (pos = (head)->prev, n = pos->prev; \
             prefetch(pos->prev), pos != (head); \
             pos = n, n = pos->prev)
             
                
/**
 * LIST_ENTRY - get the struct for this entry
 * @ptr:        the &struct list_head pointer.
 * @type:       the type of the struct this is embedded in.
 * @member:     the name of the list_struct within the struct.
 */
#define LIST_ENTRY(ptr, type, member) \
        CONTAINER_OF(ptr, type, member)

/*
 * Insert a new entry between two known consecutive entries.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */

static inline VOID __list_add(struct list_head *new,
                              struct list_head *prev,
                              struct list_head *next)
{
        next->prev = new;
        new->next = next;
        new->prev = prev;
        prev->next = new;
}

/**
 * LIST_ADD - add a new entry
 * @new: new entry to be added
 * @head: list head to add it after
 *
 * Insert a new entry after the specified head.
 * This is good for implementing stacks.
 */
static inline VOID LIST_ADD(struct list_head *new, struct list_head *head)
{
        __list_add(new, head, head->next);
}

/**
 * LIST_ADD_TAIL - add a new entry
 * @new: new entry to be added
 * @head: list head to add it before
 *
 * Insert a new entry before the specified head.
 * This is useful for implementing queues.
 */
static inline VOID LIST_ADD_TAIL(struct list_head *new, struct list_head *head)
{
        __list_add(new, head->prev, head);
}

/*
 * Delete a list entry by making the prev/next entries
 * point to each other.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static inline VOID __list_del(struct list_head * prev, struct list_head * next)
{
        next->prev = prev;
        prev->next = next;
}

/**
 * LIST_DEL - deletes entry from list.
 * @entry: the element to delete from the list.
 * Note: LIST_EMPTY() on entry does not return true after this, the entry is
 * in an undefined state.
 */
static inline VOID LIST_DEL(struct list_head *entry)
{
    __list_del(entry->prev, entry->next);
    entry->next = NULL;
    entry->prev = NULL;
    /*   
    entry->next = LIST_POISON1;
    entry->prev = LIST_POISON2;

    Linux kernel��These are non-NULL pointers that will result in page faults 
    under normal circumstances, used to verify that nobody uses non-initialized 
    list entries.
    #define LIST_POISON1 ((VOID *) 0x00100100)
    #define LIST_POISON2 ((VOID *) 0x00200200)
    */
}

/**
 * LIST_IS_LAST - tests whether @list is the last entry in list @head
 * @list: the entry to test
 * @head: the head of the list
 */
static inline INT32 LIST_IS_LAST(const struct list_head *list,
                                const struct list_head *head)
{
        return list->next == head;
}

/**
 * LIST_EMPTY - tests whether a list is empty
 * @head: the list to test.
 */
static inline INT32 LIST_EMPTY(const struct list_head *head)
{
        return head->next == head;
}

#endif /*__OAM_STACK_LIST_H__*/
