/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAMStack                                             **/
/**                                                                          **/
/**  FILE        : oam_inc.h                                                 **/
/**                                                                          **/
/**  DESCRIPTION : Definition of common public module                        **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    12/Jan/2011   - initial version created.                                   
 *                                                                      
 ******************************************************************************/
#ifndef __OAM_INC_H__
#define __OAM_INC_H__

#include "ponOnuMngIf.h"

#endif /*__OAM_INC_H__*/