/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_stack_type_expo.h                                     **/
/**                                                                          **/
/**  DESCRIPTION : Definition of common enums and structures for EOAM        **/
/**                stack module                                              **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *                                                                               
 *         Victor  - initial version created.   10/Jan/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OAM_STACK_TYPE_EXPO_H__
#define __OAM_STACK_TYPE_EXPO_H__

#define OAM_MAC_ADDR_LEN 6
typedef UINT8 OAM_ETH_MAC_ADDR_T[OAM_MAC_ADDR_LEN];

/*OUI definition*/
#define OAM_OUI_LEN  3
typedef UINT8 OAM_OUI_T[OAM_OUI_LEN];

#define OAM_IPv4_ADDR_LEN 4
#define OAM_IPv6_ADDR_LEN 16
#define OAM_IPv4_ADDR_STR_LEN  16
#define OAM_IPv6_ADDR_STR_LEN  64

#define OAM_MAX_PON_IF     2
#define OAM_MAX_UNI_NUM    4
#define OAM_MIN_UNI_INDEX  1
#define OAM_MAX_PACKET_LEN 1500

#define OAM_U8_TO_BOOL(U8)   ((U8)? true:false)

typedef enum
{
    OAM_OBJECT_LEAF_TYPE_ONU_LEAF         = 0,
    OAM_OBJECT_LEAF_TYPE_PORT_LEAF        = 1,
    OAM_OBJECT_LEAF_TYPE_VLAN_LEAF        = 3,
    OAM_OBJECT_LEAF_TYPE_QOS_LEAF         = 4,
    OAM_OBJECT_LEAF_TYPE_MULTICAST_LEAF   = 5,
    OAM_OBJECT_LEAF_TYPE_NONE             = 0xff
}OAM_OBJECT_LEAF_TYPE_E;

typedef enum
{
    OAM_MNG_OBJECT_TLV_TYPE_V_2_0         = 0x36, /*TLV type supported by CTC OAM version 2.0 and lower ones*/
    OAM_MNG_OBJECT_TLV_TYPE_V_2_1         = 0x37  /*TLV type supported by CTC OAM version 2.1 and higher ones*/
}OAM_MNG_OBJECT_TLV_TYPE_E;

typedef enum
{
    OAM_MNG_OBJECT_PORT_TYPE_NONE            = 0x00,
    OAM_MNG_OBJECT_PORT_TYPE_ETHERNET_PORT   = 0x01,
    OAM_MNG_OBJECT_PORT_TYPE_VOIP_PORT       = 0x02,
    OAM_MNG_OBJECT_PORT_TYPE_ADSL2_PLUS_PORT = 0x03,
    OAM_MNG_OBJECT_PORT_TYPE_VDSL2_PORT      = 0x04,
    OAM_MNG_OBJECT_PORT_TYPE_E1_PORT         = 0x05
}OAM_MNG_OBJECT_PORT_TYPE_E;

typedef enum
{
    OAM_MNG_OBJECT_LEAF_TYPE_NONE        = 0xFFFF, /*For ONU with no leaf*/
    OAM_MNG_OBJECT_LEAF_TYPE_PORT        = 0x0001,
    OAM_MNG_OBJECT_LEAF_TYPE_CARD        = 0x0002,
    OAM_MNG_OBJECT_LEAF_TYPE_LLID        = 0x0003,
    OAM_MNG_OBJECT_LEAF_TYPE_PON_IF      = 0x0004,
    OAM_MNG_OBJECT_LEAF_TYPE_FRAME       = 0x0005,
    OAM_MNG_OBJECT_LEAF_TYPE_LAST
}OAM_MNG_OBJECT_LEAF_TYPE_E;

typedef struct
{
    UINT32                                frame_number;
    UINT32                                slot_number; 
    UINT32                                port_number;
    OAM_MNG_OBJECT_PORT_TYPE_E            port_type;
}POS_PACKED OAM_MNG_OBJECT_INDEX_T;

typedef struct
{
    OAM_MNG_OBJECT_TLV_TYPE_E         tlv_type;
    OAM_MNG_OBJECT_LEAF_TYPE_E        leaf;
    OAM_MNG_OBJECT_INDEX_T            index;
}POS_PACKED OAM_MNG_OBJECT_T;

typedef struct
{
   UINT8        info_type;
   UINT8        info_length; 
   UINT8        oam_version;
   UINT16       revision;
   UINT8        state;
   UINT8        oam_config;
   UINT16       oampdu_config;
   UINT8        oui[OAM_OUI_LEN];
   UINT8        vendor[4];
}POS_PACKED OAM_LOCAL_TLV_T;

/* Types for information attached to object variable-containers */
#define OAM_MAX_INFO_ENTRY_NUM 100

typedef struct
{
    OAM_MNG_OBJECT_T  m_info[OAM_MAX_INFO_ENTRY_NUM];
    UINT16 m_num_of_entries;
}POS_PACKED OAM_OBJECT_TYPE_T;

/* CTC Extended Variable Descriptor instances structures */
/* C7-CTC_EX_VAR_ONU_IDENTIFIER */

#define OAM_HW_VERSION_LEN 8
#define OAM_SW_VERSION_LEN 16

/* Contain the OUI and version pairs */
typedef struct 
{
    OAM_OUI_T   oui;
    UINT8       version;
}POS_PACKED OAM_OUI_VERSION_RECORD_T;

#define OAM_VENDOR_SN_LEN     4
#define OAM_VENDOR_MODEL_LEN  4
typedef struct
{
    UINT8              vendor_id[OAM_VENDOR_SN_LEN];
    UINT8              onu_model[OAM_VENDOR_MODEL_LEN];    
    OAM_ETH_MAC_ADDR_T onu_id;
    UINT8              hardware_version[OAM_HW_VERSION_LEN];
    UINT8              software_version[OAM_SW_VERSION_LEN];
}POS_PACKED  OAM_ONU_SERIAL_NUM_T;

/*C7-CTC_EX_VAR_FW*/
#define OAM_FW_VERSION_LEN  4
typedef struct
{
    UINT8  width ;
    UINT8  version[OAM_FW_VERSION_LEN];
}POS_PACKED OAM_FIRMWARE_VERSION_T;

/* C7-CTC_EX_VAR_CHIPSET_ID */
#define OAM_CHIP_ID_LEN     2
#define OAM_CHIP_RVTIME_LEN 3

typedef struct
{
    UINT8        vendor_id[OAM_CHIP_ID_LEN]; /* PMC-SIERRA value: ASCII 'E6' */
    UINT16       chip_model;
    UINT32       revision;
    UINT8        date[OAM_CHIP_RVTIME_LEN];
}POS_PACKED  OAM_CHIPSET_ID_T;

/* C7-CTC_EX_VAR_ONU_CAPABILITIES */
typedef enum
{
    OAM_SERVICE_TYPE_GBIT_ETHERNET    = 0x1,
    OAM_SERVICE_TYPE_FE_ETHERNET      = 0x2,
    OAM_SERVICE_TYPE_VOIP             = 0x4,
    OAM_SERVICE_TYPE_TDM_CES          = 0x8
}OAM_SERVICE_TYPE_E;

typedef struct unsigned_int64
{
    UINT32  msb;
    UINT32  lsb;
} POS_PACKED micro_unsigned_int64;

/* Representation of 64-bit signed integer in ANSI-C  */
/* Integer value is ((msb<<BYTES_IN_SYMBOL)+lsb)      */
typedef struct signed_int64
{
    INT32   msb;
    UINT32  lsb;
} POS_PACKED micro_signed_int64;

typedef micro_unsigned_int64  OAM_UINT64;
typedef micro_signed_int64    OAM_INT64;

typedef struct
{
    UINT8         service_supported;
    UINT32        ge_eth_ports_number;
    OAM_UINT64    ge_ports_bitmap;
    UINT32        fe_eth_ports_number;
    OAM_UINT64    fe_ports_bitmap;
    UINT8         pots_ports_number;
    UINT8         e1_ports_number;
    UINT8         upstream_queues_number;
    UINT8         max_us_queues_per_port;
    UINT8         downstream_queues_number;
    UINT8         max_ds_queues_per_port;
    UINT8         battery_backup ;
}POS_PACKED  OAM_ONU_CAPABILITIES_T;

typedef enum
{
    OAM_ONU_TYPE_SFU              = 0,
    OAM_ONU_TYPE_HGU              = 1,
    OAM_ONU_TYPE_SBU              = 2,
    OAM_ONU_TYPE_F_MDU            = 3, 
    OAM_ONU_TYPE_X_MDU            = 4, 
    OAM_ONU_TYPE_BOX_DSL_MDU      = 5, 
    OAM_ONU_TYPE_RACK_DSL_MDU     = 6, 
    OAM_ONU_TYPE_HYBIDR_MDU       = 7, 
    OAM_ONU_TYPE_OTHER                 
}OAM_ONU_TYPE_E;

typedef enum
{
    OAM_ONU_CARD_TYPE_GE = 0,
    OAM_ONU_CARD_TYPE_FE,
    OAM_ONU_CARD_TYPE_POTS,
    OAM_ONU_CARD_TYPE_TDM,
    OAM_ONU_CARD_TYPE_ADSL2,
    OAM_ONU_CARD_TYPE_VDSL2,
    OAM_ONU_CARD_TYPE_WIFI,
    OAM_ONU_CARD_TYPE_USB
}OAM_ONU_CARD_TYPE_E;

typedef enum
{
    OAM_OPTICAL_PROTECTION_TYPE_NOT_SUPPORT = 0,
    OAM_OPTICAL_PROTECTION_TYPE_C,
    OAM_OPTICAL_PROTECTION_TYPE_D
}OAM_OPTICAL_PROTECTION_TYPE_E;

typedef struct 
{
    OAM_ONU_CARD_TYPE_E   interface_type;
    UINT16                pots_number;
}POS_PACKED OAM_INTERFACE_CAPABILITIES_T;

/*C7--0007*/
#define OAM_MDU_MAX_SLOT_NUM        20

#define OAM_PON_LLID_SUPPORTED_NUM  1

typedef struct
{
    OAM_ONU_TYPE_E                onu_type;
    UINT8                         multi_llid_support_capabilities;
    OAM_OPTICAL_PROTECTION_TYPE_E onu_optical_protection_type;
    UINT8                         pon_ports_number;
    UINT8                         slot_number;
    UINT8                         interface_type_number;
    OAM_INTERFACE_CAPABILITIES_T  interface_capabilities[OAM_MDU_MAX_SLOT_NUM];
    UINT8                         battery_backup ;
}POS_PACKED  OAM_ONU_CAPABILITIES_PLUS_T;

typedef enum
{
    OAM_POWER_CTRL_TYPE_NOT_SUPPORT = 0,
    OAM_POWER_CTRL_TYPE_TX_SUPPORT,        
    OAM_POWER_CTRL_TYPE_TX_RX_SUPPORT        
}OAM_POWER_CTRL_TYPE_E;

typedef struct
{
    BOOL                   ipv6_supported;
    OAM_POWER_CTRL_TYPE_E  power_ctl_supported;
    UINT8                  service_sla;   /*V3.0 required*/
}POS_PACKED  OAM_ONU_CAPABILITIES_3_T;

typedef enum
{
    OAM_POWER_SAVING_NOT_SUPPORT = 0,
    OAM_POWER_SAVING_TX_SUPPORT,
    OAM_POWER_SAVING_TX_RX_SUPPORT
}OAM_SLEEP_MODE_CAP_E;

typedef enum
{
    OAM_EARLY_WEAKUP_SUPPORT = 0,
    OAM_EARLY_WEAKUP_NOT_SUPPORT
}OAM_EARLY_WAKEUP_CAP_E;

/*C7-0x000D*/
typedef struct
{
    OAM_SLEEP_MODE_CAP_E    sleep_mode;
    OAM_EARLY_WAKEUP_CAP_E  early_wakeup;
}POS_PACKED  OAM_ONU_PWR_SAVING_CAP_T;

typedef enum
{
    OAM_EARLY_WEAKUP_ENABLE = 0,
    OAM_EARLY_WEAKUP_DISABLE
}OAM_EARLY_WAKEUP_STATE_E;

typedef struct
{
    UINT32    lsb;
    UINT16    msb;
}POS_PACKED  OAM_DURATION_MAX_T;

/*C7-0x000E*/
typedef struct
{
    OAM_EARLY_WAKEUP_STATE_E early_wakeup;
    OAM_DURATION_MAX_T       sleep_duration_max;
}POS_PACKED  OAM_ONU_PWR_SAVING_CFG_T;

/*C7-0x000F*/
typedef struct
{
    UINT32                  los_optical;
    UINT32                  los_mac;
}POS_PACKED  OAM_ONU_PROTECT_PARA_T;

/* C7-0x0011 */
typedef enum
{
    OAM_PORT_LINK_STATE_DOWN    = 0x00,
    OAM_PORT_LINK_STATE_UP      = 0x01
}OAM_PORT_LINK_STATE_E;

typedef enum
{
    OAM_PORT_ADMIN_DISABLE      = 0x01,
    OAM_PORT_ADMIN_ENABLE       = 0x02
}OAM_PORT_ADMIN_E;

typedef enum
{
    OAM_PORT_ACTION_DEACTIVATE  = 0x01,
    OAM_PORT_ACTION_ACTIVATE    = 0x02
}OAM_PORT_ACTION_E;

/* C7-CTC_EX_VAR_ETH_PORT_POLICING */
typedef enum
{
    OAM_PORT_ON_OFF_STATE_DEACTIVATE  = 0x00,
    OAM_PORT_ON_OFF_STATE_ACTIVATE    = 0x01
}OAM_PORT_ON_OFF_STATE_E;

typedef enum
{
    OAM_PORT_DISABLE_LOOPED_STATE_DEACTIVATE  = 0x00000000,
    OAM_PORT_DISABLE_LOOPED_STATE_ACTIVATE    = 0x00000001
}OAM_PORT_DISABLE_LOOPED_STATE_E;

typedef enum
{
    OAM_EXIT_SLEEP_MODE   = 0x00,
    OAM_ENTER_SLEEP_MODE ,
    OAM_CHANGE_SLEEP_MODE
}OAM_SLEEP_FLAG_E;

typedef enum
{
    OAM_SLEEP_NONE   = 0x00,
    OAM_SLEEP_TX_MODE ,
    OAM_SLEEP_TRX_MODE
}OAM_SLEEP_MODE_E;

/*C9-0x0002*/
typedef struct
{
    UINT32                  sleep_duration;
    UINT32                  wait_duration;
    OAM_SLEEP_FLAG_E        sleep_flag;
    OAM_SLEEP_MODE_E        sleep_mode;
}POS_PACKED  OAM_ONU_SLEEP_CONTROL_T;

/*C7 CTC_EX_VAR_ETH_PORT_PASUE*/
typedef enum
{
    OAM_PORT_PAUSE_STATE_DEACTIVATE   = 0x00,
    OAM_PORT_PAUSE_STATE_ACTIVATE     = 0x01
}OAM_PORT_PAUSE_STATE_E;

/*C7 CTC_eX_VAR_ETH_AUG_STAT*/
typedef enum
{
    OAM_PORT_NEGA_STATE_DEACTIVATE    = 0x01,
    OAM_PORT_NEGA_STATE_ACTIVATE      = 0x02
}OAM_PORT_NEGA_STATE_E;

typedef struct
{
    OAM_PORT_ON_OFF_STATE_E  operation;
    UINT32                   cir;
    UINT32                   bucket_depth;
    UINT32                   extra_burst_size;
}POS_PACKED OAM_PORT_US_POLICING_ENTRY_T;

typedef struct
{
    OAM_PORT_ON_OFF_STATE_E  operation;
    UINT32                   cir;    /* Committed Information Rate of the port (in Kbps) */
    UINT32                   pir;    /* Peak Information Rate of the port (in Kbps) */
}POS_PACKED OAM_PORT_DS_RATE_LIMITING_ENTRY_T;

/* C7-0x0021 */
#define OAM_PORT_MAX_AGGREGATED_ENTRY_NUM        4
#define OAM_PORT_MAX_AGGREGATED_VLANFILTER_NUM   8
#define OAM_MAX_VLAN_FILTER_ENTRY_NUM            32

typedef enum
{
    OAM_PORT_VLAN_MODE_TRANSPARENT        = 0x00,
    OAM_PORT_VLAN_MODE_TAG                = 0x01,
    OAM_PORT_VLAN_MODE_TRANSLATION        = 0x02,
    OAM_PORT_VLAN_MODE_AGGREGATION        = 0x03,
    OAM_PORT_VLAN_MODE_TRUNK              = 0x04
}OAM_PORT_VLAN_MODE_E;

typedef struct
{
    UINT8                 number_of_aggregated_vlan;
    UINT32                target_vlan;    
    UINT32                aggregated_vlan_list[OAM_PORT_MAX_AGGREGATED_VLANFILTER_NUM];
}POS_PACKED OAM_PORT_VLAN_AGGREGATED_ENTRY_T;

typedef struct
{
    OAM_PORT_VLAN_MODE_E               mode;
    UINT32                             default_vlan;
    UINT32                             vlan_list[OAM_MAX_VLAN_FILTER_ENTRY_NUM];
    OAM_PORT_VLAN_AGGREGATED_ENTRY_T   aggregated_entry[OAM_PORT_MAX_AGGREGATED_ENTRY_NUM] ;  /***Used only when <mode> == <OAM_PORT_VLAN_MODE_AGGREGATION>*/
    UINT8                              number_of_entries;    /**This parameter describes how many entries in <vlan_list> are used, when <mode> == <OAM_PORT_VLAN_MODE_TRANSLATION>  
                                                                 and <OAM_PORT_VLAN_MODE_TRUNK> <and < OAM_PORT_VLAN_MODE_AGGREGATION>*/
}POS_PACKED OAM_ETH_PORT_VLAN_CONF_T; 

/* C7-0x0031 */
#define OAM_MAX_CNM_ENTRY_NUM    4 

#define OAM_MAX_CNM_RULE_NUM     8

#define OAM_MAX_CNM_PRECEDENCE   255

typedef enum
{
    OAM_CLASS_ACTION_DELETE_RULE    = 0x00,
    OAM_CLASS_ACTION_ADD_RULE       = 0x01,
    OAM_CLASS_ACTION_DELETE_LIST    = 0x02,
    OAM_CLASS_ACTION_LIST           = 0x03
}OAM_CLASS_ACTION_E;

typedef enum
{
    OAM_CLASS_FIELD_SEL_DA_MAC                = 0x00,
    OAM_CLASS_FIELD_SEL_SA_MAC                = 0x01,
    OAM_CLASS_FIELD_SEL_ETH_PRI               = 0x02,
    OAM_CLASS_FIELD_SEL_VLAN_ID               = 0x03,
    OAM_CLASS_FIELD_SEL_ETH_TYPE              = 0x04,
    OAM_CLASS_FIELD_SEL_IPv4_DIP              = 0x05,
    OAM_CLASS_FIELD_SEL_IPv4_SIP              = 0x06,
    OAM_CLASS_FIELD_SEL_IP_PROTOCOL_TYPE      = 0x07,
    OAM_CLASS_FIELD_SEL_IPV4_DSCP             = 0x08,
    OAM_CLASS_FIELD_SEL_IPV6_DSCP             = 0x09,
    OAM_CLASS_FIELD_SEL_L4_SRC_PORT           = 0x0A,
    OAM_CLASS_FIELD_SEL_L4_DEST_PORT          = 0x0B,
    OAM_CLASS_FIELD_SEL_IP_VERSION            = 0x0C,
    OAM_CLASS_FIELD_SEL_IPv6_LABEL            = 0x0D,
    OAM_CLASS_FIELD_SEL_IPv6_DIP              = 0x0E,
    OAM_CLASS_FIELD_SEL_IPv6_SIP              = 0x0F,
    OAM_CLASS_FIELD_SEL_IPv6_DPREFIX          = 0x10,
    OAM_CLASS_FIELD_SEL_IPv6_SPREFIX          = 0x11,
    OAM_CLASS_FIELD_SEL_IPv6_NH               = 0x12
}OAM_CLASS_RULE_FIELD_SEL_E;

typedef enum
{
    OAM_CLASS_VALIDATION_OPER_NEVER_MATCH           = 0x00,
    OAM_CLASS_VALIDATION_OPER_EQUAL                 = 0x01,
    OAM_CLASS_VALIDATION_OPER_NOT_EQUAL             = 0x02,
    OAM_CLASS_VALIDATION_OPER_LESS_THAN_OR_EQUAL    = 0x03,
    OAM_CLASS_VALIDATION_OPER_GREATER_THAN_OR_EQUAL = 0x04,
    OAM_CLASS_VALIDATION_OPER_FIELD_EXIST           = 0x05,
    OAM_CLASS_VALIDATION_OPER_FIELD_NOT_EXIST       = 0x06,
    OAM_CLASS_VALIDATION_OPER_ALWAYS_MATCH          = 0x07
}OAM_CLASS_VALIDATION_OPER_E;

typedef struct
{
    UINT32              match_value;
    OAM_ETH_MAC_ADDR_T  mac_address;
    UINT8               ipv6_value[OAM_IPv6_ADDR_LEN ];
}POS_PACKED OAM_CLASS_MATCH_VALUE_T;

typedef struct
{
    OAM_CLASS_RULE_FIELD_SEL_E    field_select;
    OAM_CLASS_MATCH_VALUE_T       value;
    OAM_CLASS_VALIDATION_OPER_E   validation_operator;
}POS_PACKED  OAM_CLASS_RULE_T;

typedef enum
{
    OAM_TL_TAG_NOT_CARE    = 0x00,
    OAM_TL_ETH_NO_TAG      = 0x01,
    OAM_TL_ETH_SINGLE_TAG  = 0x02,
    OAM_TL_ETH_DOUBLE_TAG  = 0x03
} OAM_TL_VLAN_NUM_E;

#define    OAM_PARSE_NOT_KNOWN    (0x0000)
#define    OAM_PARSE_FIRST_24B    (0x0001)
#define    OAM_PARSE_SECOND_24B   (0x0002)
#define    OAM_PARSE_NOT_IPv6     (0x0004)

typedef UINT32 OAM_IPv6_FIELD_T;

typedef struct
{
    BOOL               valid;
    UINT8              precedence;
    UINT8              queue_mapped;
    UINT8              priority_mark;
    OAM_TL_VLAN_NUM_E  num_of_vlan; /*for handling ethertype TPM rules.*/
    OAM_IPv6_FIELD_T   ipv6_parse_field;
    UINT8              num_of_entries;
    OAM_CLASS_RULE_T   entries[OAM_MAX_CNM_ENTRY_NUM];
}POS_PACKED OAM_CLASS_N_MARK_ENTRY_T;

typedef  OAM_CLASS_N_MARK_ENTRY_T OAM_CLASS_N_MARK_RULE_T[OAM_MAX_CNM_RULE_NUM];

typedef struct
{
    OAM_CLASS_N_MARK_RULE_T   rules;
    OAM_CLASS_ACTION_E        action;
}POS_PACKED  OAM_CLASS_N_MARK_T;    /** NOTE: This may exceed the standard size of 128bytes, 
                                        but the frame composer blocks this with error. */
/* C7-0x0041 */
#define OAM_MAX_MULTICAST_VLAN_ENTRY_NUM 32

typedef enum
{
    OAM_MC_VLAN_OPER_DELETE    = 0x00,
    OAM_MC_VLAN_OPER_ADD       = 0x01,
    OAM_MC_VLAN_OPER_CLEAR     = 0x02,
    OAM_MC_VLAN_OPER_LIST      = 0x03
} OAM_MC_VLAN_OPER_E;

typedef enum
{
    OAM_MC_PROTOCOL_IGMP_SNOOPING = 0x00,
    OAM_MC_PROTOCOL_CTC           = 0x01
}OAM_MC_PROTOCOL_E;

typedef struct
{
    OAM_MC_VLAN_OPER_E    vlan_operation;
    UINT16                vlan_id[OAM_MAX_MULTICAST_VLAN_ENTRY_NUM];
    UINT8                 num_of_vlan_id;    /** Number of entries used in <vlan_id> */
}POS_PACKED  OAM_MC_VLAN_T;

/* C7-0x0042 */
#define OAM_MAX_MC_VLAN_TRANSLATION_ENTRY_NUM 32

typedef enum
{
    OAM_MC_VLAN_TAG_NOT_STRIP  = 0x00,
    OAM_MC_VLAN_TAG_STRIP      = 0x01,
    OAM_MC_VLAN_TAG_TRANSLATE  = 0x02
}OAM_MC_VLAN_TAG_STRIP_E;

typedef struct
{
    UINT16       multicast_vlan;
    UINT16       iptv_user_vlan;
}POS_PACKED  OAM_MC_VLAN_TRANSLATION_ENTRY_T;

typedef struct
{
    OAM_MC_VLAN_TRANSLATION_ENTRY_T entries[OAM_MAX_MC_VLAN_TRANSLATION_ENTRY_NUM];
    UINT8                           number_of_entries;
}POS_PACKED  OAM_MC_VLAN_TRANSLATION_T;

typedef struct
{
    OAM_MC_VLAN_TAG_STRIP_E       tag_oper;
    OAM_MC_VLAN_TRANSLATION_T     multicast_vlan_switching;
}POS_PACKED  OAM_MC_PORT_TAG_OPER_T;

#define OAM_MAX_MC_MAC_ENTRY_NUM         64     /** 8 port * 8 entris per port*/
#define OAM_MAX_MC_MAC_ENTRY_NUM_PER_TLV 12
#define OAM_MC_CTRL_ENTRY_LEN            10
#define OAM_MC_CTRL_TLV_HEAD_LEN         3
#define OAM_IPV6_ADDR_LEN                16

typedef enum
{
    OAM_MC_CTRL_TYPE_DA_ONLY = 0x00,
    OAM_MC_CTRL_TYPE_DA_VID,
    OAM_MC_CTRL_TYPE_DA_SRCIP,       
    OAM_MC_CTRL_TYPE_IPV4_VID,
    OAM_MC_CTRL_TYPE_IPV6_VID
}OAM_MC_CTRL_TYPE_E;

typedef enum
{
    OAM_MC_CTRL_ACTION_DELETE    = 0x00,
    OAM_MC_CTRL_ACTION_ADD       = 0x01,
    OAM_MC_CTRL_ACTION_CLEAR     = 0x02,
    OAM_MC_CTRL_ACTION_LIST      = 0x03
}OAM_MC_CTRL_ACTION_E;

typedef struct
{
    OAM_ETH_MAC_ADDR_T    da;
    UINT16                vid;      /** Valid if vid == 0 ,only GDA based*/
    UINT32                src_ipv4; /** Source IP                        */
    UINT8                 src_ipv6[OAM_IPV6_ADDR_LEN]; /** Source IP     */
    UINT16                user_id;
}POS_PACKED  OAM_MC_CTRL_ENTRY_T;

typedef struct
{
    OAM_MC_CTRL_ACTION_E    action;
    OAM_MC_CTRL_TYPE_E      control_type;
    OAM_MC_CTRL_ENTRY_T     entries[OAM_MAX_MC_MAC_ENTRY_NUM];
    UINT16                  num_of_entries; /** The count of rules within <entries> */
}POS_PACKED  OAM_MC_CTRL_T;

typedef enum
{
    OAM_FAST_LEAVE_ADMIN_STATE_DISABLED   = 0x00000001,
    OAM_FAST_LEAVE_ADMIN_STATE_ENABLED    = 0x00000002
}OAM_FAST_LEAVE_ADMIN_STATE_E;

/* aFastLeaveAbility */
#define OAM_MAX_FAST_LEAVE_ABILITY_MODE_NUM    6
typedef enum
{
    OAM_MC_IGMP_SNOOPING_NONE_FAST_LEAVE_SUPPORT = 0x00000000,
    OAM_MC_IGMP_SNOOPING_FAST_LEAVE_SUPPORT      = 0x00000001,
    OAM_MC_MLD_SNOOPING_NONE_FAST_LEAVE_SUPPORT  = 0x00000002,
    OAM_MC_MLD_SNOOPING_FAST_LEAVE_SUPPORT       = 0x00000003,    
    OAM_MC_CTC_CONTROL_FAST_NONE_LEAVE_SUPPORT   = 0x00000010,
    OAM_MC_CTC_CONTROL_FAST_LEAVE_SUPPORT        = 0x00000011
}OAM_MC_FAST_LEAVE_MODE_E;

typedef struct
{
    UINT16                     num_of_modes; /* The count of modes within <modes> */
    OAM_MC_FAST_LEAVE_MODE_E   modes[OAM_MAX_FAST_LEAVE_ABILITY_MODE_NUM];
}POS_PACKED OAM_MC_FAST_LEAVE_ABILITY_T;

/* 9-313 aFECAbility */
typedef enum
{
    OAM_PON_STD_FEC_ABILITY_UNKNOWN     = 0x1,
    OAM_PON_STD_FEC_ABILITY_SUPPORTED   = 0x2,
    OAM_PON_STD_FEC_ABILITY_UNSUPPORTED = 0x3
}OAM_PON_STD_FEC_ABILITY_E;

/* 9-314 aFECmode */
typedef enum
{
    OAM_PON_STD_FEC_MODE_UNKNOWN        = 0x1,
    OAM_PON_STD_FEC_MODE_ENABLED        = 0x2,
    OAM_PON_STD_FEC_MODE_DISABLED       = 0x3
}OAM_PON_STD_FEC_MODE_E;

typedef enum
{
    OAM_MC_PROTOCOL_TYPE_IGMP_MLD_SNOOPING  = 0x00, /*IGMP snooping & MLD */
    OAM_MC_PROTOCOL_TYPE_CTC                = 0x01,
    OAM_MC_PROTOCOL_TYPE_PROXY              = 0x02
}OAM_MC_PROTOCOL_TYPE_E;

typedef enum
{
    OAM_PORT_TECH_ABILITY_UNDEFINED     = 1,
    OAM_PORT_TECH_ABILITY_UNKNOWN       = 2,
    OAM_PORT_TECH_ABILITY_10_BASE_T     = 14,
    OAM_PORT_TECH_ABILITY_10_BASE_TFD   = 142,
    OAM_PORT_TECH_ABILITY_100_BASE_T4   = 23,
    OAM_PORT_TECH_ABILITY_100_BASE_TX   = 25,
    OAM_PORT_TECH_ABILITY_100_BASE_TXFD = 252,
    OAM_PORT_TECH_ABILITY_FDX_PAUSE     = 312,
    OAM_PORT_TECH_ABILITY_FDX_A_PAUSE   = 313,
    OAM_PORT_TECH_ABILITY_FDX_S_PAUSE   = 314,
    OAM_PORT_TECH_ABILITY_FDX_B_PAUSE   = 315,
    OAM_PORT_TECH_ABILITY_100_BASE_T2   = 32,
    OAM_PORT_TECH_ABILITY_100_BASE_T2FD = 322,
    OAM_PORT_TECH_ABILITY_1000_BASE_X   = 36,
    OAM_PORT_TECH_ABILITY_1000_BASE_XFD = 362,
    OAM_PORT_TECH_ABILITY_1000_BASE_T   = 40,
    OAM_PORT_TECH_ABILITY_1000_BASE_TFD = 402,
    OAM_PORT_TECH_ABILITY_REM_FAULT1    = 37,
    OAM_PORT_TECH_ABILITY_REM_FAULT2    = 372,
    OAM_PORT_TECH_ABILITY_ISO_ETHERNET  = 8029
}OAM_PORT_TECH_ABILITY_E;

#define OAM_MAX_PORT_TECH_ABILITY_NUM 20
typedef struct
{
    UINT8                    number_of_abilities;
    OAM_PORT_TECH_ABILITY_E  technology[OAM_MAX_PORT_TECH_ABILITY_NUM];
}POS_PACKED  OAM_PORT_AUTO_NEGA_TECH_ABILITY_T;

/* C7-0x1021 */
#define OAM_MAX_PORT_VLAN_QINQ_ENTRY_NUM 17

typedef enum
{
    OAM_PORT_QINQ_MODE_NONE             = 0x00,
    OAM_PORT_QINQ_MODE_PER_PORT         = 0x01,
    OAM_PORT_QINQ_MODE_PER_VLAN         = 0x02
}OAM_PORT_QINQ_MODE_E;

typedef struct
{
    OAM_PORT_QINQ_MODE_E  mode;          
    UINT32                default_vlan; /** For Per Port QinQ , Q_TV*/
    UINT32                vlan_list[OAM_MAX_PORT_VLAN_QINQ_ENTRY_NUM]; /**For Per VLAN QinQ, 1st Entry is Q_OV, 2nd is Q_SV */  
    UINT8                 number_of_entries;/** This parameter describes how many entries in <vlan_list> are used, when <mode> == < OAM_PORT_QINQ_MODE_PER_VLAN > */     
}POS_PACKED  OAM_PORT_QINQ_CONFIG_T; 

typedef struct
{
    UINT16 transceiver_temperature; /**< Working temperature of ONU optical module */
    UINT16 supply_voltage;          /**< Supply Voltage(Vcc) of ONU optical module */
    UINT16 tx_bias_current;         /**< Bias Current of ONU optical TX            */
    UINT16 tx_power;                /**< Power of ONU optical TX (Output)          */
    UINT16 rx_power;                /**< Power of ONU optical RX (Input)           */
}POS_PACKED  OAM_PON_OPTICAL_TRANSCEIVER_DIAGNOSIS_T; 

/*SLA*/
#define OAM_MAX_SERVICE_SLA_ENTRY_NUM        8

typedef enum
{
    OAM_SLA_BEST_EFFORT_SCHEDULING_SCHEME_SP          = 0,
    OAM_SLA_BEST_EFFORT_SCHEDULING_SCHEME_WRR         = 1,
    OAM_SLA_BEST_EFFORT_SCHEDULING_SCHEME_SP_AND_WRR  = 2
}OAM_SLA_BEST_EFFORT_SCHEDULING_SCHEME_E;

typedef enum
{
    OAM_SLA_SERVICE_OPER_DEACTIVE = 0,
    OAM_SLA_SERVICE_OPER_ACTIVE   = 1
}OAM_SLA_SERVICE_OPER_E;

typedef struct
{
    UINT8     queue_number;          /** Queue to which service traffic is classified */
    UINT16    fixed_packet_size;     /** Given in byte units                          */
    UINT16    fixed_bandwidth;       /** Fixed bandwidth in 256 Kbps units            */
    UINT16    guaranteed_bandwidth;  /** Assured bandwidth in 256 Kbps units          */
    UINT16    best_effort_bandwidth; /** Best effort bandwidth in 256 Kbps units      */
    UINT8     wrr_weight;            /** Possible values: 0 (SP), 1 - 100 (WRR)       */
}POS_PACKED  OAM_SLA_SERVICE_ENTRY_T;

typedef struct
{
    OAM_SLA_BEST_EFFORT_SCHEDULING_SCHEME_E best_effort_scheduling_scheme;
    UINT8                                   high_priority_boundary;
    UINT32                                  cycle_length;                  /** Given in TQ units */
    UINT8                                   number_of_service_sla_entries; /** 1..8 */
    OAM_SLA_SERVICE_ENTRY_T                 service_sla_entries[ OAM_MAX_SERVICE_SLA_ENTRY_NUM];
}POS_PACKED  OAM_SLA_SERVICE_SCHEME_T;

typedef struct
{
    OAM_SLA_SERVICE_OPER_E   active_if;
    OAM_SLA_SERVICE_SCHEME_T sla;
}POS_PACKED OAM_PON_SERVICE_SLA_T;

/* Event notification */
#define OAM_SPECIFIC_EVENT_TYPE     0xfe /* Event type of Organization Specific Event Notification */
#define OAM_END_OF_FRAME            0x00
#define OAM_MAX_ALARM_STR_SIZE      40

typedef enum
{
    OAM_ALARM_EQUIPMENT              = 0x0001,
    OAM_ALARM_POWER                  = 0x0002,
    OAM_ALARM_BATTERY_MISSING        = 0x0003, 
    OAM_ALARM_BATTERY_FAILURE        = 0x0004,
    OAM_ALARM_BATTERY_VOLT_LOW       = 0x0005,
    OAM_ALARM_PHYSICAL_INTRUSION     = 0x0006, 
    OAM_ALARM_SELF_TEST_FAILURE      = 0x0007,
    OAM_ALARM_POWER_VOLT_LOW         = 0x0008,
    OAM_ALARM_TEMP_HIGH              = 0x0009, 
    OAM_ALARM_TEMP_LOW               = 0x000A,
    OAM_ALARM_CONNECT_FAILURE        = 0x000B,
    OAM_ALARM_PON_IF_SWITCH          = 0x000C,
    OAM_ALARM_SLEEP_STATUS_UPDATE    = 0x000D,

    /*PON interface*/
    OAM_ALARM_PON_RX_POWER_HIGH      = 0x0101,
    OAM_ALARM_PON_RX_POWER_LOW       = 0x0102, 
    OAM_ALARM_PON_TX_POWER_HIGH      = 0x0103,
    OAM_ALARM_PON_TX_POWER_LOW       = 0x0104,
    OAM_ALARM_PON_TX_BIAS_HIGH       = 0x0105, 
    OAM_ALARM_PON_TX_BIAS_LOW        = 0x0106,
    OAM_ALARM_PON_VCC_HIGH           = 0x0107,
    OAM_ALARM_PON_VCC_LOW            = 0x0108, 
    OAM_ALARM_PON_TEMP_HIGH          = 0x0109,
    OAM_ALARM_PON_TEMP_LOW           = 0x010a,
    OAM_ALARM_PON_RX_POWER_HIGH_WARN = 0x010b, 
    OAM_ALARM_PON_RX_POWER_LOW_WARN  = 0x010c,
    OAM_ALARM_PON_TX_POWER_HIGH_WARN = 0x010d,
    OAM_ALARM_PON_TX_POWER_LOW_WARN  = 0x010e, 
    OAM_ALARM_PON_TX_BIAS_HIGH_WARN  = 0x010f,
    OAM_ALARM_PON_TX_BIAS_LOW_WARN   = 0x0110,
    OAM_ALARM_PON_VCC_HIGH_WARN      = 0x0111, 
    OAM_ALARM_PON_VCC_LOW_WARN       = 0x0112,
    OAM_ALARM_PON_TEMP_HIGH_WARN     = 0x0113,
    OAM_ALARM_PON_TEMP_LOW_WARN      = 0x0114,
    OAM_ALARM_PON_DS_DROP_EVENT      = 0x0115,
    OAM_ALARM_PON_US_DROP_EVENT      = 0x0116,
    OAM_ALARM_PON_DS_CRC_ERR         = 0x0117,
    OAM_ALARM_PON_DS_UNDERSIZE       = 0x0118,
    OAM_ALARM_PON_US_UNDERSIZE       = 0x0119,
    OAM_ALARM_PON_DS_OVERSIZE        = 0x011A,
    OAM_ALARM_PON_US_OVERSIZE        = 0x011B,
    OAM_ALARM_PON_DS_FRAGMENT        = 0x011C,
    OAM_ALARM_PON_DS_JABBER          = 0x011D,
    OAM_ALARM_PON_DS_COLLISION       = 0x011E,
    OAM_ALARM_PON_DS_DISCARD         = 0x011F,
    OAM_ALARM_PON_US_DISCARD         = 0x0120,
    OAM_ALARM_PON_DS_ERR_FRAME       = 0x0121,

    OAM_ALARM_PON_DS_DROP_EVENT_W    = 0x0122,
    OAM_ALARM_PON_US_DROP_EVENT_W    = 0x0123,
    OAM_ALARM_PON_DS_CRC_ERR_W       = 0x0124,
    OAM_ALARM_PON_DS_UNDERSIZE_W     = 0x0125,
    OAM_ALARM_PON_US_UNDERSIZE_W     = 0x0126,
    OAM_ALARM_PON_DS_OVERSIZE_W      = 0x0127,
    OAM_ALARM_PON_US_OVERSIZE_W      = 0x0128,
    OAM_ALARM_PON_DS_FRAGMENT_W      = 0x0129,
    OAM_ALARM_PON_DS_JABBER_W        = 0x012A,
    OAM_ALARM_PON_DS_COLLISION_W     = 0x012B,
    OAM_ALARM_PON_DS_DISCARD_W       = 0x012C,
    OAM_ALARM_PON_US_DISCARD_W       = 0x012D,
    OAM_ALARM_PON_DS_ERR_FRAME_W     = 0x012E,



    /*Frame*/
    OAM_ALARM_FRAME_FAILURE          = 0x0701,
    /*Card*/
    OAM_ALARM_CARD                   = 0x0201,
    OAM_ALARM_CARD_SELF_TEST_FAILURE = 0x0202,
    /*ETH port*/
    OAM_ALARM_PORT_AUTONEG_FAILURE   = 0x0301,
    OAM_ALARM_PORT_LOS               = 0x0302,
    OAM_ALARM_PORT_CONNECTION_FAILURE= 0x0303,
    OAM_ALARM_PORT_LOOPBACK          = 0x0304,
    OAM_ALARM_PORT_CONGESTION        = 0x0305,

    OAM_ALARM_PORT_DS_DROP_EVENT     = 0x0306,
    OAM_ALARM_PORT_US_DROP_EVENT     = 0x0307,
    OAM_ALARM_PORT_DS_CRC_ERR        = 0x0308,
    OAM_ALARM_PORT_DS_UNDERSIZE      = 0x0309,
    OAM_ALARM_PORT_US_UNDERSIZE      = 0x030A,
    OAM_ALARM_PORT_DS_OVERSIZE       = 0x030B,
    OAM_ALARM_PORT_US_OVERSIZE       = 0x030C,
    OAM_ALARM_PORT_DS_FRAGMENT       = 0x030D,
    OAM_ALARM_PORT_DS_JABBER         = 0x030E,
    OAM_ALARM_PORT_DS_COLLISION      = 0x030F,
    OAM_ALARM_PORT_DS_DISCARD        = 0x0310,
    OAM_ALARM_PORT_US_DISCARD        = 0x0311,
    OAM_ALARM_PORT_DS_ERR_FRAME      = 0x0312,
    OAM_ALARM_PORT_STATUS_CHANGE     = 0x0313,

    OAM_ALARM_PORT_DS_DROP_EVENT_W   = 0x0314,
    OAM_ALARM_PORT_US_DROP_EVENT_W   = 0x0315,
    OAM_ALARM_PORT_DS_CRC_ERR_W      = 0x0316,
    OAM_ALARM_PORT_DS_UNDERSIZE_W    = 0x0317,
    OAM_ALARM_PORT_US_UNDERSIZE_W    = 0x0318,
    OAM_ALARM_PORT_DS_OVERSIZE_W     = 0x0319,
    OAM_ALARM_PORT_US_OVERSIZE_W     = 0x031A,
    OAM_ALARM_PORT_DS_FRAGMENT_W     = 0x031B,
    OAM_ALARM_PORT_DS_JABBER_W       = 0x031C,
    OAM_ALARM_PORT_DS_COLLISION_W    = 0x031D,
    OAM_ALARM_PORT_DS_DISCARD_W      = 0x031E,
    OAM_ALARM_PORT_US_DISCARD_W      = 0x031F,
    OAM_ALARM_PORT_DS_ERR_FRAME_W    = 0x0320,
    OAM_ALARM_PORT_STATUS_CHANGE_W   = 0x0321,

    /*POTS*/
    OAM_ALARM_POTS_PORT_FAILURE      = 0x0401,
    /*ADSL2*/
    OAM_ALARM_ADSL2_PORT_FAILURE     = 0x0601,
    /*E1*/
    OAM_ALARM_E1_PORT_FAILURE        = 0x0501,
    OAM_ALARM_E1_TIMING_UNLOCK       = 0x0502,
    OAM_ALARM_E1_LOS                 = 0x0503
}OAM_ALARM_ID_E;

typedef enum
{
    OAM_ALARM_CLEAR_REPORT           = 0x00,
    OAM_ALARM_REPORT                 = 0x01
}OAM_ALARM_REPORT_STATE_E;

typedef enum
{
    OAM_ALARM_CTRL_DISABLE           = 0x01,
    OAM_ALARM_CTRL_ENABLE            = 0x02
}OAM_OAM_ALARM_CTRL_STATE_E;

typedef enum
{
    OAM_ALARM_INFO_TYPE_FAILURE_CODE,           
    OAM_ALARM_INFO_TYPE_TEST_VALUE, /*This type can get/set alarm threshold*/     
    OAM_ALARM_INFO_TYPE_NONE           
}OAM_ALARM_INFO_TYPE_E;
                                                 
typedef struct                                   
{                                                
    UINT8        event_type;
    UINT8        event_length;
    OAM_OUI_T    oui;
}POS_PACKED  OAM_SPEC_EVENT_HEADER_T;

typedef struct 
{            
    OAM_ALARM_ID_E                 alarm_id;
    OAM_OAM_ALARM_CTRL_STATE_E     enable;
}POS_PACKED OAM_ALARM_ADMIN_STATE_T;

typedef struct
{
    OAM_ALARM_ID_E        alarm_id;
    UINT32                raise_alarm_threshold;
    UINT32                clear_alarm_threshold;
}OAM_ALARM_THRESHOLD_T;
/* End of event notification */

typedef enum
{
    OAM_MONITORING_STATE_DISABLE = 0x01,
    OAM_MONITORING_STATE_ENABLE  = 0x02
}OAM_MONITORING_STATE_E;

#define OAM_MONITORING_INTERVAL_DEF 0x384

typedef enum
{
    OAM_PON_PORT_A   = 0x00,
    OAM_PON_PORT_B   = 0x01,
    OAM_UNI_PORT_0   = 0x02,
    OAM_UNI_PORT_1   = 0x03,
    OAM_UNI_PORT_2   = 0x04,
    OAM_UNI_PORT_3   = 0x05,
    OAM_UNI_PORT_4   = 0x06,
    OAM_UNI_PORT_5   = 0x07,
    OAM_UNI_PORT_6   = 0x08,
    OAM_UNI_PORT_7   = 0x09,
    OAM_UNI_PORT_ALL = 0xFFFF,
    OAM_UNI_PORT_ILLEGAL
}POS_PACKED OAM_PM_OBJECT_E;

typedef struct
{
    OAM_MONITORING_STATE_E monitoring_status;
    UINT32                 monitoring_period;
}POS_PACKED OAM_PM_STATE_T;

typedef struct
{
    UINT32      dropevent_ds;
    UINT32      dropevent_us;
    UINT32      octet_ds;
    UINT32      octet_us;
    UINT32      frame_ds;
    UINT32      frame_us;
    UINT32      broadcast_ds;
    UINT32      broadcast_us;
    UINT32      multicast_ds;
    UINT32      multicast_us;
    UINT32      crcerror_ds;
    UINT32      undersize_ds;
    UINT32      undersize_us;
    UINT32      oversize_ds;
    UINT32      oversize_us;
    UINT32      fragment_ds;
    UINT32      jabber_ds;
    UINT32      collision_ds;
    UINT32      octet_64_ds;
    UINT32      octet_65_127_ds;
    UINT32      octet_128_255_ds;
    UINT32      octet_256_511_ds;
    UINT32      octet_512_1023_ds;
    UINT32      octet_1024_1518_ds;
    UINT32      octet_64_us;
    UINT32      octet_65_127_us;
    UINT32      octet_128_255_us;
    UINT32      octet_256_511_us;
    UINT32      octet_512_1023_us;
    UINT32      octet_1024_1518_us;
    UINT32      discard_ds;
    UINT32      discard_us;
    UINT32      error_ds;
    UINT32      status_change_times;
}POS_PACKED OAM_PM_DATA_T;

typedef enum
{
    OAM_EVENT_STATE_DISABLE   = 0x00000000,
    OAM_EVENT_STATE_ENABLE    = 0x00000001,
    OAM_EVENT_STATE_NOT_FOUND = 0xffffffff
}OAM_EVENT_STATE_T;

typedef struct
{
    UINT16             type;
    UINT16             alarm_id;
    UINT32             instance_id;
    OAM_EVENT_STATE_T  event_state;
}POS_PACKED OAM_EVENT_STATUS_T;

#define OAM_EVENT_THRESHOLD_NOT_FOUND 0xffffffff
typedef struct
{
    UINT16                 type;
    UINT32                 instance_id;
    OAM_ALARM_THRESHOLD_T  threshold;
}POS_PACKED OAM_EVENT_THRESHOLD_T;

#define OAM_MAX_LLID_QUEUE_CONFIG_ENTRY_NUM        8

typedef struct
{
    UINT16    queue_id;                    
    UINT16    wrr_weight;   /** Possible values: 0 (SP), 1 - 100 (WRR) */
}POS_PACKED  OAM_PON_LLID_QUEUE_CONFIG_ENTRY_T;

typedef struct
{
    UINT8                               number_of_llid_queue_config_entries; /* 1..8 */
    OAM_PON_LLID_QUEUE_CONFIG_ENTRY_T   llid_queue_config_entries[OAM_MAX_LLID_QUEUE_CONFIG_ENTRY_NUM];
}POS_PACKED  OAM_PON_LLID_QUEUE_CONFIG_T;

/* LOID register */
#define OAM_LOID_ONU_ID_LEN            24
#define OAM_LOID_PASSWORD_LEN          12
typedef enum
{
    OAM_AUTH_TYPE_LOID                = 0x01, /** ONU_ID+Password mode */
    OAM_AUTH_TYPE_NOT_SUPPORTED       = 0x02  /** ONU not supported or can not accept the required authentication type */
}OAM_AUTH_TYPE_E;

typedef enum
{
    OAM_AUTH_FAILURE_ONU_ID_NOT_EXIST = 0x01, /** ONU_ID is not exist                       */
    OAM_AUTH_FAILURE_WRONG_PASSWORD   = 0x02  /** ONU_ID is exist but the Password is wrong */
}OAM_AUTH_FAILURE_TYPE_E;

typedef enum
{
    OAM_AUTH_MODE_MAC,   /** MAC mode             */
    OAM_AUTH_MODE_LOID,  /** ONU_ID+Password mode */
    OAM_AUTH_MODE_HYBRID /** hybrid mode          */
}OAM_AUTH_MODE_E;

typedef struct  
{
    UINT8      onu_loid[OAM_LOID_ONU_ID_LEN];
    UINT8      password[OAM_LOID_PASSWORD_LEN];
}POS_PACKED OAM_AUTH_LOID_DATA_T;

typedef struct
{
    OAM_AUTH_TYPE_E    auth_type;
}POS_PACKED  OAM_AUTH_REQUEST_T; 

typedef struct
{
    OAM_AUTH_FAILURE_TYPE_E  failure_code;
}POS_PACKED  OAM_AUTH_FAILURE_T; 

typedef struct
{
    OAM_AUTH_TYPE_E           auth_type;
    OAM_AUTH_LOID_DATA_T      loid_data;
    OAM_AUTH_TYPE_E           desired_auth_type;
}POS_PACKED  OAM_AUTH_RESPONSE_T; 

/*Encryption set*/
#define OAM_PON_ENC_KEY_CNT 3

typedef UINT32 OAM_PON_ENC_KEY_T;

typedef enum
{
    OAM_PON_ENC_KEY_INDEX_0 = 0x0,
    OAM_PON_ENC_KEY_INDEX_1      
}OAM_PON_ENC_KEY_INDEX_E;

typedef enum
{
    OAM_PON_ENC_MODE_TRIPLE_CHURNING = 0,
    OAM_PON_ENC_MODE_OTHER,
}OAM_PON_ENC_MODE_E;

typedef struct
{
    union
    {
        UINT8 v4[OAM_IPv4_ADDR_LEN];
        UINT8 v6[OAM_IPv6_ADDR_LEN];
    }u;
    #define v6 u.v6
    #define v4 u.v4
}OAM_IP_ADDR;

typedef UINT8   OAM_NET_MASK[OAM_IPv4_ADDR_LEN];

typedef struct
{
    BOOL         isipv6;       /** The address type    */
    OAM_IP_ADDR  mng_ip;       /** The ip for manage   */
    OAM_NET_MASK mng_mask;     /** The mask/prefix     */
    OAM_IP_ADDR  mng_gw;       /** The gateway         */
    UINT16       data_cvlan;   /** The data cvlan      */
    UINT16       data_svlan;   /** The data svlan      */
    UINT8        data_priority;/** The data priority   */
}POS_PACKED  OAM_MNG_GLOBAL_PARAM_T;

#define OAM_SECURITY_NAME_LEN       32
#define OAM_COMMUNITY_FOR_READ_LEN  32
#define OAM_COMMUNITY_FOR_WRITE_LEN 32

typedef struct
{
    BOOL           isipv6;                                           /** The address type    */
    UINT8          snmp_ver;                                         /** The ip for manage       */
    OAM_IP_ADDR    trap_host_ip;                                     /** The mask for manage     */
    UINT16         trap_port;                                        /** Trap port               */
    UINT16         snmp_port;                                        /** snmp server port        */
    UINT8          security_name[OAM_SECURITY_NAME_LEN];             /** The data svlan          */
    UINT8          community_for_read[OAM_COMMUNITY_FOR_READ_LEN];   /** name of read community  */
    UINT8          community_for_write[OAM_COMMUNITY_FOR_WRITE_LEN]; /** name of write community */
}POS_PACKED  OAM_MNG_SNMP_PARAM_CONF_T;

typedef struct
{
    UINT32         action;                                           /** Tx power supply action  */
    UINT8          onu_id[OAM_MAC_ADDR_LEN];                         /** The ONU ID              */
    UINT32         optical_tx_id;                                    /** Optical transmitter ID  */
}POS_PACKED  OAM_POWER_TX_CTL_T;

typedef enum
{
    OAM_PORT_LOOPBACK_DETECTION_DEACTIVE = 1,
    OAM_PORT_LOOPBACK_DETECTION_ACTIVE   = 2
}OAM_PORT_LOOPBACK_DETECTION_ADMIN_E;

/* C7-0x0008 OAM_EX_VAR_HOLDOVER_CONFIG */
typedef enum
{
    OAM_PON_HOLDOVER_STATE_DEACTIVE      = 0x00000001,
    OAM_PON_HOLDOVER_STATE_ACTIVE        = 0x00000002
}OAM_PON_HOLDOVER_STATE_E;

typedef enum
{
    OAM_LOOPBACK_STATE_ENABLE        = 0x01,
    OAM_LOOPBACK_STATE_DISABLE       = 0x02
}OAM_REMOTE_LOOPBACK_STATE_E;

typedef struct
{
    OAM_PON_HOLDOVER_STATE_E       holdover_state; /** Holdover state*/
    UINT32                         holdover_time;  /** Holdover time */
}POS_PACKED  OAM_PON_HOLDOVER_STATE_T;

/* C7-0x000B OAM_EX_VAR_PON_IF_ADMIN_STATE */
typedef struct
{
    UINT8                   active_pon_if; /**< Active PON_IF */
}POS_PACKED  OAM_PON_ACTIVE_IF_STATE_T;

/*------------------------------------------------------------*/
/*For VOIP OAM                                                */
/*------------------------------------------------------------*/
#define MRVL_88F6500_MAX_VOIP_POTS_NUM     2
#define MRVL_88F6500_VOIP_IAD_SOFTVERSION  "V1.0.0"

#define OAM_VOIP_POTS_USER_COUNT    MRVL_88F6500_MAX_VOIP_POTS_NUM

/*! \enum OAM_VOIP_PROTOCOL_E
 *  \brief assign VoIP protocol when initialize
 */
typedef enum
{
    OAM_VOIP_PROTOCOL_H248              = 0x00,
    OAM_VOIP_PROTOCOL_SIP               = 0x01
} OAM_VOIP_PROTOCOL_E;


#define OAM_VOIP_SW_VERSION_LEN         32
#define OAM_VOIP_SW_TIME_LEN            32

/*! \struct OAM_VOIP_IAD_INFO_T
 *  \brief information of IAD device
 */
typedef struct
{
    OAM_ETH_MAC_ADDR_T            mac_address;                         /** The IAD MAC address         */
    OAM_VOIP_PROTOCOL_E           voip_protocol;                       /** The supported VOIP protocol */
    UINT8                         sw_version[OAM_VOIP_SW_VERSION_LEN]; /** The IAD software version    */
    UINT8                         sw_time[OAM_VOIP_SW_TIME_LEN];       /** The LAD software time       */
    UINT32                        user_count;                          /** The VOIP user count         */
}POS_PACKED OAM_VOIP_IAD_INFO_T;

/*! \enum OAM_VOIP_IP_MODE_E
 *  \brief net mode of IAD device
 */
typedef enum
{
    OAM_VOIP_IP_MODE_STATIC_IP          = 0,
    OAM_VOIP_IP_MODE_DHCP               = 1,
    OAM_VOIP_IP_MODE_PPPOE              = 2
}OAM_VOIP_IP_MODE_E;

/*! \enum OAM_PPPOE_MODE_E
 *  \brief net mode of IAD device
 */
typedef enum
{
    OAM_PPPOE_MODE_AUTO                 = 0,           
    OAM_PPPOE_MODE_CHAP                 = 1, /** Change Handshake Authentication Protocol */
    OAM_PPPOE_MODE_PAP                  = 2  /** Password Authentication Protocol */
}OAM_PPPOE_MODE_E;

/*! \enum OAM_VOIP_VLAN_TAG_E
 *  \brief vlan mode of IAD device
 */
typedef enum
{
    CTC_VOIP_TAGGED_FLAG_TRANSPARENT    = 0,           
    CTC_VOIP_TAGGED_FLAG_TAG            = 1,       
    CTC_VOIP_TAGGED_FLAG_VLAN_STACK     = 2  
}OAM_VOIP_VLAN_TAG_E;

#define OAM_VOIP_PPPOE_USER_LEN         32
#define OAM_VOIP_PPPOE_PASSWORD_LEN     32

/*! \struct OAM_VOIP_GLOBAL_PARAM_T
 *  \brief voip mg profile configure.
 */
typedef struct
{
    OAM_VOIP_IP_MODE_E          voice_ip_mode; /** The IP address configured mode*/
    UINT32                      iad_ip_addr;   /** The IAD IP address            */
    UINT32                      iad_net_mask;  /** The IAD NET mask              */
    UINT32                      iad_def_gw;    /** The IAD default gateway       */
    OAM_PPPOE_MODE_E            pppoe_mode;    /** The PPPOE mode                */
    UINT8                       pppoe_user[OAM_VOIP_PPPOE_USER_LEN];       /** The PPPOE user name */
    UINT8                       pppoe_passwd[OAM_VOIP_PPPOE_PASSWORD_LEN];/** The PPPOE password  */
    OAM_VOIP_VLAN_TAG_E         vlan_tag_mode; /** Voice data transmit with flag */
    UINT16                      cvlan_id;      /** The CVLAN of voice data       */
    UINT16                      svlan_id;      /** The SVLAN of voice data       */
    UINT8                       priority;      /** The voice priority            */
}POS_PACKED OAM_VOIP_GLOBAL_PARAM_T;

/*! \enum OAM_VOIP_H248_REG_MODE_E
 *  \brief voip mg register mode.the mgc could identify the mg with different mode
 */
typedef enum
{
    OAM_VOIP_H248_HEART_BEAT_MODE_OFF       = 0,
    OAM_VOIP_H248_HEART_BEAT_MODE_CTC_STO   = 1,
    OAM_VOIP_H248_HEART_BEAT_MODE_OTHER     = 0xFF
}OAM_VOIP_H248_HEART_BEAT_MODE_E;

typedef enum
{
    OAM_VOIP_H248_REG_MODE_IP               = 0,
    OAM_VOIP_H248_REG_MODE_DOMAIN           = 1,
    OAM_VOIP_H248_REG_MODE_DEVICE_NAME      = 2
}OAM_VOIP_H248_REG_MODE_E;

typedef enum
{
    OAM_VOIP_H248_ACTIVE_MGC_BACKUP         = 0,
    OAM_VOIP_H248_ACTIVE_MGC_PRIMARY        = 1
}OAM_VOIP_H248_ACTIVE_MGC_E;

#define OAM_VOIP_H248_MID_LEN               64

/*! \struct OAM_VOIP_H248_PARAM_T
 *  \brief voip h248 common configure,protocol parameter 
 */
typedef struct
{
    UINT16                            mg_port;                   /** MG port number                                           */
    UINT32                            mgcip;                     /** Primary softswitch platform IP address                   */
    UINT32                            mgccom_port_num;           /** Primary softswitch platform port number                  */
    UINT32                            back_mgcip;                /** Backup softswitch platform IP address                    */
    UINT16                            back_mgccom_port_num;      /** Backup softswitch platform port number                   */
    OAM_VOIP_H248_ACTIVE_MGC_E        active_mgc;                /** Active MGC that ONU registered, 0x00-backup, 0x01:primary*/
    OAM_VOIP_H248_REG_MODE_E          reg_mode;                  /** Register mode:default MGCP domain name using mode        */
    UINT8                             mid[OAM_VOIP_H248_MID_LEN];/** MG domain name                                           */
    OAM_VOIP_H248_HEART_BEAT_MODE_E   heart_beat_mode;           /** Heart beat mode, default is 0x01                         */
    UINT16                            heart_beat_cycle;          /** Heartbeat Cycle,default is 60s                           */
    UINT8                             heart_beat_count;          /** Heartbeat detect quantity,default is 3 times             */
}POS_PACKED OAM_VOIP_H248_PARAM_T;

/*! \struct OAM_VOIP_H248_USER_TID_CONF_T
 *  \brief voip h248 phy terminate name,no more than 32 byte. 
 */
#define OAM_VOIP_H248_USER_TID_NAME_LEN      32
typedef struct
{
    UINT8                             user_tid_name[OAM_VOIP_H248_USER_TID_NAME_LEN];      /* User TID name */
}POS_PACKED OAM_VOIP_H248_USER_TID_CONFIG_T;

/*! \enum OAM_VOIP_H248_USER_TID_CONF_T
 *  \brief This enum define the rtp name align mode. 
 *  For example,'RTP/0001' as aligned mode,digit width 4,'RTP/1'as the non-aligned mode.   
 */
typedef struct
{
    OAM_MNG_OBJECT_INDEX_T            management_object_index;
    OAM_VOIP_H248_USER_TID_CONFIG_T   h248_user_tid_config;
}POS_PACKED OAM_VOIP_H248_USER_TID_CONFIG_OBJ_T;

#define OAM_VOIP_H248_RTP_TID_PREFIX_LEN      16
#define OAM_VOIP_H248_RTP_TID_DIGIT_LEN       8

typedef enum
{
    OAM_VOIP_H248_RTP_TID_MODE_ALIGNED        = 0x00,
    OAM_VOIP_H248_RTP_TID_MODE_UNALIGNED      = 0x01
}OAM_VOIP_H248_RTP_TID_ALIGN_TYPE_E;

typedef struct
{
    UINT16                             num_of_rtp_tid;                                     /**< number of RTP TID*/
    UINT8                              rtp_tid_prefix[OAM_VOIP_H248_RTP_TID_PREFIX_LEN];            /**< RTP prefix(only for H.248) */
    UINT8                              rtp_tid_digit_begin[OAM_VOIP_H248_RTP_TID_DIGIT_LEN];         /**< RTP TID digit begin value */
    OAM_VOIP_H248_RTP_TID_ALIGN_TYPE_E rtp_tid_mode;  /** RTP TID digit align mode */
    UINT16                             rtp_tid_digit_length;                               /**< RTP TID digit length, valid when mode=0*/
}POS_PACKED OAM_VOIP_H248_RTP_TID_CONFIG_T;

/*! \struct OAM_VOIP_H248_RTP_TID_INFO_T
 *  \brief voip h248 rtp configure info,no more than 32 byte. 
 */
#define OAM_VOIP_H248_RTP_TID_NAME_LEN        32
typedef struct
{
    UINT16         num_of_rtp_tid;                              /** number of RTP TID*/
    UINT8          rtp_tid_name[OAM_VOIP_H248_RTP_TID_NAME_LEN];/** RTP TID name(only for H.248) */
 }POS_PACKED OAM_VOIP_H248_RTP_TID_INFO_T;

typedef enum
{
    OAM_VOIP_SIP_HEART_BEAT_ON                   = 0,
    OAM_VOIP_SIP_HEART_BEAT_OFF                  = 1
}OAM_VOIP_SIP_HEART_BEAT_E;

/*! \struct OAM_VOIP_SIP_PARAM_T
 *  \brief voip sip common configure info,protocol parameter. 
 */
typedef struct
{
    UINT16                       mg_port;                /** MG port number                              */
    UINT32                       server_ip;              /** Active SIP agent server IP address          */
    UINT32                       serv_com_port;          /** Active SIP agent server port number         */
    UINT32                       back_server_ip;         /** Standby SIP agent server IP address         */
    UINT32                       back_serv_com_port;     /** Standby SIP agent server port number        */
    UINT32                       active_proxy_server;    /** SIP Proxy that ONU registered               */
    UINT32                       reg_server_ip;          /** Active SIP register server IP address       */
    UINT32                       reg_serv_com_port;      /** Active SIP register server port number      */
    UINT32                       back_reg_server_ip;     /** Standby SIP register server IP address      */
    UINT32                       back_reg_serv_com_port; /** Standby SIP register server port number     */
    UINT32                       outbound_server_ip;     /** OutBound server IP address                  */
    UINT32                       outbound_serv_com_port; /** OutBound server port number                 */
    UINT32                       reg_interval;           /** Register refresh period, unit is second     */
    OAM_VOIP_SIP_HEART_BEAT_E    heart_beat_switch;      /** SIP Heartbeat enable switch,0-open,1-close  */
    UINT16                       heart_beat_cycle;       /** Heartbeat Cycle,default is 60s              */
    UINT16                       heart_beat_count;       /** Heartbeat detect quantity,default is 3 times*/
}POS_PACKED OAM_VOIP_SIP_PARAM_T;

#define OAM_SIP_PORT_NUM                       16
#define OAM_SIP_USER_NAME_LEN                  32
#define OAM_SIP_PASSWORD_LEN                   16


/*! \struct OAM_VOIP_SIP_USER_PARAM_T
 *  \brief voip sip user configure info. 
 */
typedef struct
{
    UINT8         sip_port_num[OAM_SIP_PORT_NUM];  /** SIP port phone number */
    UINT8         user_name[OAM_SIP_USER_NAME_LEN];/** SIP port user name    */
    UINT8         passwd[OAM_SIP_PASSWORD_LEN];    /** SIP port keyword      */
}POS_PACKED OAM_VOIP_SIP_USER_PARAM_T;

typedef struct
{
  OAM_MNG_OBJECT_INDEX_T         management_object_index;
  OAM_VOIP_SIP_USER_PARAM_T      sip_user_param_config;
}POS_PACKED OAM_VOIP_SIP_USER_CONFIG_OBJ_T;

/*! \enum OAM_VOIP_SIP_USER_PARAM_T
 *  \brief voip sip user configure info. 
 */
typedef enum
{
    OAM_VOIP_FAX_MODE_TRANSPARENT            = 0,           
    OAM_VOIP_FAX_MODE_T38                    = 1
}OAM_VOIP_FAX_MODE_E;

typedef enum
{
    OAM_VOIP_FAX_CONTROL_NEGOTIATED          = 0,
    OAM_VOIP_FAX_CONTROL_AUTO_VBD            = 1
}OAM_VOIP_FAX_CONTROL_E;

typedef struct
{
    OAM_VOIP_FAX_MODE_E             t38_enable; /** voice T38 enable */
    OAM_VOIP_FAX_CONTROL_E          fax_control;/** voice Fax control */
}POS_PACKED OAM_VOIP_FAX_CONFIG_T;

/*! \enum OAM_VOIP_H248_OPER_STATUS_E
 *  \brief voip h248 protocol register operation. 
 */
typedef enum
{
    OAM_VOIP_H248_OPER_STATUS_REGISTERING    = 0,
    OAM_VOIP_H248_OPER_STATUS_REGISTERED     = 1,
    OAM_VOIP_H248_OPER_STATUS_FAULT          = 2,
    OAM_VOIP_H248_OPER_STATUS_DEREGISTER     = 3,
    OAM_VOIP_H248_OPER_STATUS_REBOOT         = 4,
    CTC_VOIP_IAD_OPER_STATUS_OTHER           = 255
}OAM_VOIP_H248_OPER_STATUS_E;

/*! \enum OAM_VOIP_IAD_PORT_STATUS_E
 *  \brief voip h248 protocol register status. 
 */
typedef enum
{
    OAM_VOIP_IAD_PORT_STATUS_REGISTERING     = 0,
    OAM_VOIP_IAD_PORT_STATUS_IDLE            = 1,
    OAM_VOIP_IAD_PORT_STATUS_OFFHOOK         = 2,
    OAM_VOIP_IAD_PORT_STATUS_DAILING         = 3,
    OAM_VOIP_IAD_PORT_STATUS_RINGING         = 4,
    OAM_VOIP_IAD_PORT_STATUS_RINGBACK        = 5,
    OAM_VOIP_IAD_PORT_STATUS_CONNECTING      = 6,
    OAM_VOIP_IAD_PORT_STATUS_CONNECTED       = 7,
    OAM_VOIP_IAD_PORT_STATUS_DISCONNECTING   = 8,
    OAM_VOIP_IAD_PORT_STATUS_REGISTER_FAILED = 9,
    OAM_VOIP_IAD_PORT_STATUS_INACTIVE        = 10
}OAM_VOIP_IAD_PORT_STATUS_E;

typedef enum
{
    OAM_VOIP_IAD_SERVICE_STATUS_END_LOCAL    = 0,
    OAM_VOIP_IAD_SERVICE_STATUS_END_REMOTE   = 1,
    OAM_VOIP_IAD_SERVICE_STATUS_END_AUTO     = 2,
    OAM_VOIP_IAD_SERVICE_STATUS_NORMAL       = 3
}OAM_VOIP_IAD_SERVICE_STATUS_E;

typedef enum
{
    OAM_VOIP_IAD_PORT_CODEC_G711A            = 0,
    OAM_VOIP_IAD_PORT_CODEC_G729             = 1,
    OAM_VOIP_IAD_PORT_CODEC_G711U            = 2,
    OAM_VOIP_IAD_PORT_CODEC_G723             = 3,
    OAM_VOIP_IAD_PORT_CODEC_G726             = 4,
    OAM_VOIP_IAD_PORT_CODEC_T38              = 5
}OAM_VOIP_IAD_PORT_CODEC_E;

typedef struct
{
    OAM_VOIP_IAD_PORT_STATUS_E     port_status;    /** User port status       */
    OAM_VOIP_IAD_SERVICE_STATUS_E  service_status; /** User port service type */
    OAM_VOIP_IAD_PORT_CODEC_E      codec_mode;     /** User port codec mode   */
}POS_PACKED OAM_VOIP_POTS_STATUS_T;

typedef struct
{
  OAM_MNG_OBJECT_INDEX_T           management_object_index;
  OAM_VOIP_POTS_STATUS_T           pots_status;
}POS_PACKED OAM_VOIP_POTS_STATUS_OBJ_T;

typedef enum
{
    OAM_VOIP_IAD_OPER_TYPE_REGISTER          = 0,
    OAM_VOIP_IAD_OPER_TYPE_DEREGISTER        = 1,
    OAM_VOIP_IAD_OPER_TYPE_RESET             = 2
}OAM_VOIP_IAD_OPER_E;

#define OAM_VOIP_SIP_DIGIT_MAP_LEN           1024
#define OAM_VOIP_SIP_DIGIT_UNIT_LEN          125

typedef enum
{
    OAM_SIP_DIGIT_MAP_UPDATE_IDLE = 0,
    OAM_SIP_DIGIT_MAP_UPDATE_INPROGRESS ,  
    OAM_SIP_DIGIT_MAP_UPDATE_SUCCESSFUL ,  
    OAM_SIP_DIGIT_MAP_UPDATE_FAILED ,  
}OAM_OAM_SIP_DIGIT_MAP_UPDATE_STATE_E;

typedef struct
{
    UINT8        total_of_unit;
    UINT8        sip_current_unit_no;       
    UINT8        sip_unit_len;
    UINT8        sip_unit[OAM_VOIP_SIP_DIGIT_UNIT_LEN];
}POS_PACKED OAM_SIP_DIGIT_MAP_UNIT_T;

typedef struct
{
    UINT8        digital_map[OAM_VOIP_SIP_DIGIT_MAP_LEN];
}POS_PACKED OAM_SIP_DIGIT_MAP_T;

/*DBA threshold set*/
#define OAM_MIN_QUEUE_NUMBER 2
#define OAM_MAX_QUEUE_NUMBER 4
#define OAM_MAX_THRESHOLD_SET_RER_QUEUE 8

typedef enum 
{
    OAM_SW_UPDATE_OK , /*successful after last updating ,or idle for next request.*/
    OAM_SW_UPDATE_BUSY, 
    OAM_SW_UPDATE_WRITING,
    OAM_SW_UPDATE_CHECKERROR,
    OAM_SW_UPDATE_FILE_OUT_RANG,
    OAM_SW_UPDATE_FILE_NOT_EXIST,
    OAM_SW_UPDATE_NOT_SUPPORT,
    OAM_SW_UPDATE_NOT_STARTED
}OAM_SW_UPDATE_STATE_E;

#define OAM_DEFAULT_PON_SLOT_NUM 0

extern UINT32 g_owner_id;

/*PON interface number*/
#define MARVELL_PON_IF_NUM 1

/*Marvell Slot numner*/
#define MARVELL_ONU_SLOT_NUM 0

/*Upgrade*/
#define OAM_MIN_IMAGE_ID 0
#define OAM_MAX_IMAGE_ID 1

/*OAM UNI VLAN member type*/
typedef enum
{
    OAM_VLAN_MEMBER_TAG   = 0,
    OAM_VLAN_MEMBER_UNTAG = 1
}OAM_VLAN_MEMBER_E;

#define OAM_GE_BIT_RATE_IN_KBPS 1000000
#define OAM_FE_BIT_RATE_IN_KBPS 100000

#define MRVL_CPU_QUEUE 0

typedef struct
{
  UINT8   branch;
  UINT16  leaf;
}POS_PACKED OAM_VAR_REQ_T;

typedef struct
{
  UINT8   branch;
  UINT16  leaf;
  UINT8   width;
  UINT32  value;
}POS_PACKED OAM_VAR_RESP_T;

typedef struct
{
  UINT8   type;
  UINT8   length;
  UINT16  stamp;
  UINT64  window;
  UINT64  threshold;
  UINT64  errors;
  UINT64  errors_running_total;
  UINT32  event_running_total;
}POS_PACKED OAM_ERROR_LINK_EVENT_T;

typedef struct
{
  UINT16                 seq;
  OAM_ERROR_LINK_EVENT_T event;  
}POS_PACKED OAM_ERROR_LINK_EVENT_BUFF_T;

#endif /* __OAM_STACK_TYPE_EXPO_H__ */
