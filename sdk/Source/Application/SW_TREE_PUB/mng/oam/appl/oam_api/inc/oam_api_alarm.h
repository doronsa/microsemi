/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_api_alarm.c                                           **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM API functions to                        **/
/**                             register alarm function hooks to eOAM stack  **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *         Victor  - initial version created.   12/Jan/2011          
 *                                                                              
 ******************************************************************************/


#ifndef __OAM_API_ALARM_H__
#define __OAM_API_ALARM_H__

STATUS oam_stack_alarm_process_handle_get(OAM_ALARM_ID_E alarm_id, OAM_ALARM_HANDLER_HOOK_T *handle);
STATUS oam_stack_alarm_process_handle_delete(OAM_ALARM_ID_E alarm_id);
STATUS oam_stack_alarm_process_handle_assign(OAM_ALARM_ID_E alarm_id, OAM_ALARM_HANDLER_HOOK_T *func_hook);
STATUS oam_stack_alarm_generate
(
    OAM_ALARM_ID_E           l_alarm, 
    INT32                    slot, 
    INT32                    port, 
    OAM_ALARM_REPORT_STATE_E state, 
    UINT32                   alarm_val
);

#endif /*__OAM_API_ALARM_H__*/
