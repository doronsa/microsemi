/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_api_vendor.c                                          **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM API functions to                        **/
/**                register vendor specific function hooks to eOAM stack     **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *         Victor  - initial version created.   12/Jan/2011          
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_API_VENDOR_H__
#define __OAM_API_VENDOR_H__

MRVL_ERROR_CODE_T register_ext_vendor_sys_get_serial_number_hook
(
    ext_vendor_sys_get_serial_number hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_get_capabilities_hook
(
    ext_vendor_sys_get_capabilities hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_get_capabilities_plus_hook
(
    ext_vendor_sys_get_capabilities_plus hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_get_capabilities_plus_3_hook
(
    ext_vendor_sys_get_capabilities_plus_3 hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_onu_rest_hook
(
    ext_vendor_sys_onu_rest hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_get_loid_hook
(
    ext_vendor_sys_get_loid  hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_set_loid_hook
(
    ext_vendor_sys_set_loid hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_onu_mac_address_get_hook
(
    ext_vendor_sys_onu_mac_address_get hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_sw_upgrade_hook
(
    ext_vendor_sys_sw_upgrade hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_get_sw_upgrade_status_hook
(
    ext_vendor_sys_get_sw_upgrade_status hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_get_active_sw_hook
(
    ext_vendor_sys_get_active_sw hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_sw_activate_hook
(
    ext_vendor_sys_sw_activate hook
);

MRVL_ERROR_CODE_T register_ext_vendor_sys_sw_commit_hook
(
    ext_vendor_sys_sw_commit hook
);
#endif /*__OAM_API_VENDOR_H__*/