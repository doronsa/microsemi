/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE   : EPON OAM Stack                                               **/
/**                                                                          **/
/**  FILE        : oam_api_extend.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM API functions to                        **/
/**                register function hooks to handle custom extended OAM     **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *         Victor  - initial version created.   12/Jan/2011          
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_API_EXTEND_H__
#define __OAM_API_EXTEND_H__

/***********************************************************************************************
* oam_custom_variable_hook()
*
* DESCRIPTION:      Parse variable requests from PON side, return variable containers to PON side
*
* INPUTS:
* receive_oam_buffer  - Variable descriptors received from PON side. it includes one or multiple
*                       descrptors.(branch, leaf). Offset: 18, after variable request code (0x02)
* received_frame_size - frame length (42~1496 bytes)
*
* OUTPUTS:
* send_oam_buffer     - Variable containers responded to PON side. it includes one or multiple
*                       variable containers. (brach, leaf, width, value)Offset: 18, after variable 
*                       response code(0x03)
* send_frame_size     - responded containers length (42~1496 bytes)
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*************************************************************************************************/
typedef STATUS (*OAM_CUSTOM_VARIABLE_HOOK_T)
(UINT8 *receive_oam_buffer,UINT16 received_frame_size, UINT8 *send_oam_buffer,UINT16 *send_frame_size );


/***********************************************************************************************
* oam_custom_oui_receive_hook()
*
* DESCRIPTION:         Parse organization specific requests from PON side, apply the configuration,
*                      compose an answer, return it to PON side
*
* INPUTS:
* receive_oam_buffer  - Organization specific request received from PON side. It starts from OUI,
*                       offset is 18, it's content is organization specific
* received_frame_size - frame length (42~1496 bytes)
*
* OUTPUTS:
* send_oam_buffer     - Organization specific responded to PON side.it's content is organization
*                       specific,it starts from OUI, offset is 18, it's content is organization 
*                       specific.
* send_frame_size     - responded containers length (42~1496 bytes)
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*************************************************************************************************/
typedef STATUS (*OAM_CUSTOM_OUI_RECEIVE_HOOK_T)
(UINT8 *receive_oam_buffer, UINT16 received_frame_size,UINT8 *send_oam_buffer, UINT16 *send_frame_size );

#define OAM_SUPPORT_CUSTOM_OUI_NUM       5  /* maximum custom OUI number system supported */
#define OAM_SUPPORT_CUSTOM_ATTRIBUTE_NUM 10 /* maximum custom attributes number system supported */

/***********************************************************************************************
* OAM_STACK_ATTRIBUTE_GET_HANDLER_FUNC_T
*
* DESCRIPTION:         Parse CTC (China Telecom Corpration) GET attribute from PON side, get the configuration,
*                      compose an answer, return it to PON side
*
* INPUTS:
* in_mesage           - CTC message received from PON side. It's content is started from variable descriptor
*                       TLV type (Ex.0xC7) and Variable container leaf (Ex: 0x0001).
* in_length           - input message length (byte)     
* object_tlv          - objective tlv, includes tlv_type(0x36/0x37),tlv_leaf, tlv_index 
*
* OUTPUTS:
* out_message         - Answers that needs to be responded to PON side, it starts from tlv type (0x37/0x36)
* out_length          - output message length (byte)
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*************************************************************************************************/
typedef STATUS (*OAM_STACK_ATTRIBUTE_GET_HANDLER_FUNC_T)
(UINT8 *in_message, UINT16 in_length, OAM_OBJECT_TYPE_T *object_tlv,UINT8 *out_message,UINT16 *out_length); 

/***********************************************************************************************
* OAM_STACK_ATTRIBUTE_SET_HANDLER_FUNC_T
*
* DESCRIPTION:         Parse CTC (China Telecom Corpration) SET attribute from PON side, configure the configuration,
*                      compose an answer, return it to PON side
*
* INPUTS:
* in_mesage           - CTC message received from PON side. It's content is started from variable descriptor
*                       TLV type (Ex.0xC7) and Variable container leaf (Ex: 0x0001).
* in_length           - input message length (byte)     
* object_tlv          - objective tlv, includes tlv_type(0x36/0x37),tlv_leaf, tlv_index 
*
* OUTPUTS:
* out_message         - Answers that needs to be responded to PON side, it starts from tlv type (0x37/0x36)
* out_length          - output message length (byte)
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*************************************************************************************************/
typedef STATUS (*OAM_STACK_ATTRIBUTE_SET_HANDLER_FUNC_T)
(UINT8 *in_message, UINT16 in_length, OAM_OBJECT_TYPE_T *object_tlv,UINT8 *out_message,UINT16 *out_length);

/* OAM attribute Handler, it includes how to GET/SET function */
typedef struct
{
    UINT8                                  branch;
    UINT16                                 leaf;
    OAM_STACK_ATTRIBUTE_GET_HANDLER_FUNC_T get_func;      /* The function to handle 'Get Attribute'                     */
    OAM_STACK_ATTRIBUTE_SET_HANDLER_FUNC_T set_func;      /* The function to handle 'Set Attribute' or 'Execute Action' */
}POS_PACKED  OAM_ATTRIBUTE_HANDLER_FUNC_T;

/*******************************************************************************
* oam_custom_variable_hook_register()
*
* DESCRIPTION:     register the variable hook function to OAM stack.
*
* INPUTS:
* custom_variable_handler: the hook function for variable handler
*
* OUTPUTS:
* none.
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*******************************************************************************/
STATUS oam_custom_variable_hook_register(OAM_CUSTOM_VARIABLE_HOOK_T custom_variable_handler);


/*******************************************************************************
* oam_custom_variable_hook_unregister()
*
* DESCRIPTION:     unregister the variable hook function to OAM stack.
*
* INPUTS:
* none
*
* OUTPUTS:
* none.
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*******************************************************************************/
STATUS oam_custom_variable_hook_unregister( );


/*******************************************************************************
* oam_custom_variable_hook_get()
*
* DESCRIPTION:      Returns the function pinter of variable hook.
*
* INPUTS:
* none 
*
* OUTPUTS:
* custom_variable_handler     - the function pinter of variable hook.
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*******************************************************************************/
STATUS oam_custom_variable_hook_get(OAM_CUSTOM_VARIABLE_HOOK_T *custom_variable_handler);


/*******************************************************************************
* oam_custom_oui_hook_register()
*
* DESCRIPTION:     register the oui hook function to OAM stack.
*
* INPUTS:
* custom_variable_handler: the hook function for variable handler
*
* OUTPUTS:
* none.
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*******************************************************************************/
STATUS oam_custom_oui_hook_register(UINT32 custom_oui, OAM_CUSTOM_OUI_RECEIVE_HOOK_T custom_oam_handler);

/*******************************************************************************
* oam_custom_oui_hook_unregister()
*
* DESCRIPTION:     register the oui hook function to OAM stack.
*
* INPUTS:
* custom_variable_handler: the hook function for variable handler
*
* OUTPUTS:
* none.
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*******************************************************************************/
STATUS oam_custom_oui_hook_unregister(UINT32 custom_oui);

/*******************************************************************************
* oam_custom_oui_hook_get()
*
* DESCRIPTION:     register the oui hook function to OAM stack.
*
* INPUTS:
* custom_variable_handler: the hook function for variable handler
*
* OUTPUTS:
* none.
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*******************************************************************************/
STATUS oam_custom_oui_hook_get(UINT32 custom_oui, OAM_CUSTOM_OUI_RECEIVE_HOOK_T *custom_oam_handler);

/*******************************************************************************
* oam_custom_ctc_attribute_hook_register()
*
* DESCRIPTION:     register the oui hook function to OAM stack.
*
* INPUTS:
* custom_variable_handler: the hook function for variable handler
*
* OUTPUTS:
* none.
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*******************************************************************************/
STATUS oam_custom_ctc_attribute_hook_register(UINT8 branch, UINT16 leaf, OAM_ATTRIBUTE_HANDLER_FUNC_T custom_oam_handler);

/*******************************************************************************
* oam_custom_ctc_attribute_hook_unregister()
*
* DESCRIPTION:     register the oui hook function to OAM stack.
*
* INPUTS:
* custom_variable_handler: the hook function for variable handler
*
* OUTPUTS:
* none.
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*******************************************************************************/
STATUS oam_custom_ctc_attribute_hook_unregister(UINT8 branch, UINT16 leaf);

/*******************************************************************************
* oam_custom_ctc_attribute_hook_get()
*
* DESCRIPTION:     register the oui hook function to OAM stack.
*
* INPUTS:
* custom_variable_handler: the hook function for variable handler
*
* OUTPUTS:
* none.
*
* RETURNS:
* On success - OAM_EXIT_OK
* On error   - OAM_ERROR_EXIT.
*
*******************************************************************************/
STATUS oam_custom_ctc_attribute_hook_get(UINT8 branch, UINT16 leaf, OAM_ATTRIBUTE_HANDLER_FUNC_T custom_oam_handler);

/*******************************************************************************
 *
 *  Function:    oam_send_event_frame
 *
 *  Description: The function send OAM event frame
 *
 * INPUTS:
 *   event_buff: event buff, should be organized on Event TLV 802.3ah 57.4, start from sequence number
 *   event_len : event_length, including sequence number
 *
 * Output:
 *   None
 *
 * Return
 * On success - OAM_EXIT_OK
 * On error   - OAM_ERROR_EXIT.
 *
 ******************************************************************************/
STATUS oam_send_event_frame(UINT8 *event_buff, INT32 event_len);

#endif /*__OAM_API_EXTEND_H__*/
