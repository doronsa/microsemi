/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_api_mc.c                                              **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM API functions to                        **/
/**                register multicast function hooks to eOAM stack           **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *         Victor  - initial version created.   12/Jan/2011          
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_API_MC_H__
#define __OAM_API_MC_H__

MRVL_ERROR_CODE_T register_ext_eth_set_mc_protocol_hook
(
    oam_eth_set_multicast_switch_t hook
);

MRVL_ERROR_CODE_T register_ext_eth_set_mc_group_max_num_hook
(
    oam_eth_set_multicast_group_count_t hook
);

MRVL_ERROR_CODE_T register_ext_eth_set_mc_tag_oper_hook
(
    oam_eth_set_multicast_tag_strip_t hook
);

MRVL_ERROR_CODE_T register_ext_eth_set_mc_vlan_hook
(
    oam_eth_set_multicast_vlan_t hook
);

MRVL_ERROR_CODE_T register_ext_eth_get_mc_fast_leave_ability_hook
(
    oam_eth_get_fast_leave_state_t  hook
);

MRVL_ERROR_CODE_T register_ext_eth_set_mc_fast_leave_mode_hook
(
    oam_eth_set_fast_leave_action_t hook
);

#endif /*__OAM_API_MC_H__*/
