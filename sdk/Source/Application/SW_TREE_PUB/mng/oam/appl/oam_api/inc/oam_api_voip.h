/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_api_voip.c                                            **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM API functions to                        **/
/**                             register VoIP function hooks to eOAM stack   **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *         Victor  - initial version created.   12/Jan/2011          
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_API_VOIP_H__
#define __OAM_API_VOIP_H__

MRVL_ERROR_CODE_T register_ext_voip_get_port_num_hook
(
    ext_onu_voip_get_port_count_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_enable_port_hook
(
    ext_onu_voip_set_port_admin_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_get_iad_info_hook
(
    ext_onu_voip_iad_info_get_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_set_global_parameter_hook
(
    ext_onu_voip_global_parameter_config_set_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_set_h248_parameter_hook
(
    ext_onu_voip_h248_parameter_config_set_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_get_h248_active_mgc_hook
(
    ext_onu_voip_h248_parameter_config_get_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_get_h248_rtp_tid_hook
(
    ext_onu_voip_h248_rtp_tid_config_get_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_set_h248_rtp_tid_hook
(
    ext_onu_voip_h248_rtp_tid_config_set_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_set_sip_parameter_hook
(
    ext_onu_voip_sip_parameter_config_set_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_set_sip_user_parameter_hook
(
    ext_onu_voip_sip_user_parameter_config_set_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_set_fax_config_hook
(
    ext_onu_voip_fax_config_set_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_get_h248_iad_operation_status_hook
(
    ext_onu_voip_iad_operation_status_get_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_get_pots_status_hook
(
    ext_onu_voip_get_port_status_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_set_iad_operation_hook
(
    ext_onu_voip_iad_operation_set_t hook
);

MRVL_ERROR_CODE_T register_ext_voip_set_sip_digit_map_hook
(
    ext_onu_voip_digit_map_set_t hook
);

#endif /*__OAM_API_VOIP_H__*/
