/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : oam_tl_alarm.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : Definition of EOAM translation layer alarm functions      **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    Victor, 18/Feb/2011   - initial version created.                              
 *                                                                      
 ******************************************************************************/
 
#ifndef __OAM_TL_VENDOR_H__
#define __OAM_TL_VENDOR_H__
#define CTC_NOT_EFFECTIVE_UNSIGNED_THRESHOLD 0xFFFFFFFF
#define CTC_NOT_EFFECTIVE_SIGNED_THRESHOLD 0xEFFFFFFF


/*----------------------------------------------------------------------------*/
/* Data Structure definition                                                  */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/* Function Declaration                                                       */
/*----------------------------------------------------------------------------*/
typedef struct
{
    OAM_ALARM_ID_E      oam_alarm_id;
    APM_ALARM_TYPE_T    apm_alarm_type;    
}OAM_ALARM_APM_ALARM_MAP_T;

typedef struct
{
    OAM_ALARM_ID_E      oam_alarm_id;
    APM_AVC_TYPE_T      apm_avc_type;    
}OAM_ALARM_APM_AVC_MAP_T;


STATUS oam_tl_alarm_process_handle_get(OAM_ALARM_ID_E alarm_id, OAM_ALARM_HANDLER_HOOK_T *handle);
STATUS oam_tl_alarm_process_handle_delete(OAM_ALARM_ID_E alarm_id);
STATUS oam_tl_alarm_process_handle_assign(OAM_ALARM_ID_E alarm_id, OAM_ALARM_HANDLER_HOOK_T *func_hook);
extern STATUS oam_tl_alarm_generate(UINT32 alarm_type, UINT32 param1, UINT32 param2, OAM_ALARM_REPORT_STATE_E state,UINT32 alarm_val);
extern STATUS oam_tl_avc_generate(UINT32 avc_type, UINT32 param1, UINT32 param2, UINT32 avc_val);


#endif /*__OAM_TL_VENDOR_H__*/
