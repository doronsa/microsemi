/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : oam_tl_pon.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : Definition of EOAM translation layer PON functions        **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    Victor, 18/Feb/2011   - initial version created.                              
 *                                                                      
 ******************************************************************************/
 
#ifndef __OAM_TL_PON_H__
#define __OAM_TL_PON_H__

/*----------------------------------------------------------------------------*/
/* Data Structure definition                                                  */
/*----------------------------------------------------------------------------*/

#define MV_DBA_DEF_NUM_OF_QUEUE            (8)
#define MV_DBA_DEF_NUM_OF_QUEUESET         (1)
#define MV_DBA_DEF_QUEUESET_DEF_THRESHOLD  (0xFFFF)
#define MV_DBA_ACTIVE_LLID                 (1)
#define MV_DBA_NON_ACTIVE_LLID             (0)
#define MV_DBA_ACTIVE_QUEUE                (1)
#define MV_DBA_NON_ACTIVE_QUEUE            (0)


/*----------------------------------------------------------------------------*/
/* Function Declaration                                                       */
/*----------------------------------------------------------------------------*/




#endif /*__OAM_TL_PON_H__*/
