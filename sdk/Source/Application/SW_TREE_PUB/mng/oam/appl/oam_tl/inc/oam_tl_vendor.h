/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : oam_tl_vendor.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : Definition of EOAM translation layer vendor functions     **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    Victor, 18/Feb/2011   - initial version created.                              
 *                                                                      
 ******************************************************************************/
 
#ifndef __OAM_TL_VENDOR_H__
#define __OAM_TL_VENDOR_H__

#define OAM_IPv4_BUF_LEN 8
#define OAM_IPv6_BUF_LEN 64

/*----------------------------------------------------------------------------*/
/* Data Structure definition                                                  */
/*----------------------------------------------------------------------------*/
typedef struct in6_ifreq
{
    struct in6_addr ifr6_addr;
    UINT32 ifr6_prefixlen;
    UINT32 ifr6_ifindex;
}OAM_IPv6_REQ;

typedef struct
{
    char    *varName;
    INT32    varEnum;
} OAM_TL_LOID_T;

static OAM_TL_LOID_T loidarea[] =
{
    {"LOID",           0},
    {"PWD",            1},

};
/*----------------------------------------------------------------------------*/
/* Function Declaration                                                       */
/*----------------------------------------------------------------------------*/




#endif /*__OAM_TL_ALARM_H__*/
