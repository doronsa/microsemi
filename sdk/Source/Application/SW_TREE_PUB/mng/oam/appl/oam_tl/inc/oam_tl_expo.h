/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_ret_val.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : Comon definition of EOAM stack return value type          **/
/**                                                                          **/
/****************************************************************************** 
**                                                                             
*   MODIFICATION HISTORY:                                                      
*
*    12/Jan/2011   - initial version created.         
*                                                                              
******************************************************************************/

#ifndef __OAM_TL_EXPO_H__
#define __OAM_TL_EXPO_H__

#include "globals.h"

/*----------------------------------------------------------*/
/*         Return codes                                     */
/*----------------------------------------------------------*/
#define OAM_EXIT_OK                       0
#define OAM_ERROR_BASE                    (-17000)
#define OAM_ERROR_EXIT                    (OAM_ERROR_BASE-1)
#define OAM_ERROR_TIME_OUT                (OAM_ERROR_BASE-2) 
#define OAM_ERROR_NOT_IMPLEMENTED         (OAM_ERROR_BASE-3)
#define OAM_ERROR_PARAMETER               (OAM_ERROR_BASE-4)
#define OAM_ERROR_HARDWARE                (OAM_ERROR_BASE-5)
#define OAM_ERROR_MEMORY                  (OAM_ERROR_BASE-6)
#define OAM_ERROR_NOT_SUPPORT             (OAM_ERROR_BASE-7)
#define OAM_ERROR_QUERY_FAILED            (OAM_ERROR_BASE-8)
#define OAM_ERROR_NU_NOT_AVAILABLE        (OAM_ERROR_BASE-9)
#define OAM_ERROR_OLT_NOT_EXIST           (OAM_ERROR_BASE-10)
#define OAM_ERROR_EXIT_ONU_DBA_THRESHOLDS (OAM_ERROR_BASE-11)
#define OAM_ERROR_TFTP_SEND_FAIL          (OAM_ERROR_BASE-12)
#define OAM_ERROR_NOT_INITIALIZED         (OAM_ERROR_BASE-13)
#define OAM_ERROR_ALREADY_EXIST           (OAM_ERROR_BASE-14)
#define OAM_ERROR_DB_FULL                 (OAM_ERROR_BASE-15)
#define OAM_ERROR_OUT_OF_RANGE            (OAM_ERROR_BASE-19)

#define MIDWARE_RET_OK            0

//typedef void               VOID;
//typedef long long          INT64;
//typedef unsigned long long UINT64;

#ifndef NULL
#define NULL ((VOID*)0)
#endif

#ifndef ERROR
#define ERROR     (-1)
#endif

//#undef FALSE
//#undef TRUE
//#undef BOOL
//
//typedef enum {FALSE = 0, TRUE = 1}  BOOL;

//#define MIDWARE_ALL_ENTRY 0xff


/*Length definition*/
#define MIDWARE_DBA_MAX_QUEUESET_NUM  4
#define MIDWARE_VENDOR_ID_LEN         4
#define MIDWARE_VENDOR_ID_LEN_PL1     8
#define MIDWARE_ONU_MODEL_LEN         4
#define MIDWARE_ONU_MODEL_LEN_PL1     8
#define MIDWARE_HW_VERSION_LEN        8
#define MIDWARE_HW_VERSION_LEN_PL1    12
#define MIDWARE_SW_VERSION_LEN        16
#define MIDWARE_SW_VERSION_LEN_PL1    20
#define MIDWARE_FW_VERSION_LEN        4
#define MIDWARE_CHIP_ID_LEN           2
#define MIDWARE_CHIP_VER_LEN          3
#define MIDWARE_CHIP_VER_LEN_PL1      4
#define MIDWARE_MAC_ADD_LEN           6
#define MIDWARE_MAC_ADD_LEN_PL1       8
#define MIDWARE_SN_LEN                16
#define MIDWARE_SN_LEN_PL1            20
#define MIDWARE_IPV4_IP_LEN           4
#define MIDWARE_IPV6_IP_LEN           16
#define MIDWARE_DBA_MAX_QUEUE_PER_SET 8
#define MIDWARE_ENC_ENTRY_LEN         4
#define MIDWARE_IMAGE_NAME_LEN        64
//#define MIDWARE_IMAGE_VER_LEN         32
//#define MIDWARE_IMAGE_VER_LEN_PL1     36
#define MIDWARE_LOID_USER_LEN         24
#define MIDWARE_LOID_USER_LEN_PL1     28
#define MIDWARE_LOID_PWD_LEN          12
#define MIDWARE_LOID_PWD_LEN_PL1      16
#define MIDWARE_IAD_SW_VER_LEN        32
#define MIDWARE_IAD_SW_VER_LEN_PL1    36
#define MIDWARE_IAD_SW_TIME_LEN       32
#define MIDWARE_IAD_SW_TIME_LEN_PL1   36
#define MIDWARE_PPPOE_USER_LEN        32
#define MIDWARE_PPPOE_USER_LEN_PL1    36
#define MIDWARE_PPPOE_PWD_LEN         32
#define MIDWARE_PPPOE_PWD_LEN_PL1     36
#define MIDWARE_H248_MG_ID_LEN        64
#define MIDWARE_H248_MG_ID_LEN_PL1    68
#define MIDWARE_H248_USER_NAME_LEN    32
#define MIDWARE_H248_USER_NAME_LEN_PL1 36
#define MIDWARE_RTP_PREFIX_LEN        16
#define MIDWARE_RTP_PREFIX_LEN_PL1    20
#define MIDWARE_RTP_DIGIT_LEN         8
#define MIDWARE_RTP_DIGIT_LEN_PL1     12
#define MIDWARE_RTP_NAME_LEN          32
#define MIDWARE_RTP_NAME_LEN_PL1      36
#define MIDWARE_SIP_USER_ACCOUNT_LEN  16
#define MIDWARE_SIP_USER_ACCOUNT_LEN_PL1 20
#define MIDWARE_SIP_USER_NAME_LEN     32
#define MIDWARE_SIP_USER_NAME_LEN_PL1 36
#define MIDWARE_SIP_USER_PWD_LEN      16
#define MIDWARE_SIP_USER_PWD_LEN_PL1  20
#define MIDWARE_DIGIMAP_LEN           1024
#define MIDWARE_DIGIMAP_LEN_PL1       1028

#endif /*__OAM_TL_EXPO_H__*/
