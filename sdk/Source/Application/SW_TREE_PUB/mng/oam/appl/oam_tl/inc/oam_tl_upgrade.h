/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : oam_tl_upgrade.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : Definition of EOAM translation layer software upgrade     **/
/**                functions                                                 **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    Victor, 18/Feb/2011   - initial version created.                              
 *                                                                      
 ******************************************************************************/
 
#ifndef __OAM_TL_UPGRADE_H__
#define __OAM_TL_UPGRADE_H__

/*----------------------------------------------------------------------------*/
/* Data Structure definition                                                  */
/*----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------*/
/* Function Declaration                                                       */
/*----------------------------------------------------------------------------*/
#define OAM_IMAGE_NAME_LEN        64
#define OAM_IMAGE_VER_LEN         16
#define OAM_IMAGE_VER_LEN_PL1     20

/*Structure of software image table*/
typedef struct
{
    UINT8      image_id;
    UINT8      active_image;
    UINT8      commit_image;
    UINT8      is_valid;
    UINT8      verion[OAM_IMAGE_VER_LEN_PL1];
    UINT32     burn_to_flash;
    UINT32     abort;
    OAM_SW_UPDATE_STATE_E status;
}OAM_IMAGE_CONF_T;


#define BANK_2_IMG(x)    (((x) == 'A') ? 0 : 1)
#define IMG_2_BANK(x)    (!(x) ? 'A' : 'B')

#endif /*__OAM_TL_UPGRADE_H__*/
