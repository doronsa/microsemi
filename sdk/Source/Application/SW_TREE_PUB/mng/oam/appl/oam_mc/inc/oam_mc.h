/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_mc.h                                                  **/
/**                                                                          **/
/**  DESCRIPTION : Definition of Multicast module                            **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *    12/Jan/2011   - initial version created.               
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_MC_H__
#define __OAM_MC_H__

#define OAM_MC_MAX_GROUP_MEMBER_NUM   255

#define OAM_MC_MAX_PORT_CNT           32

#define OAM_MC_GROUP_CTRL_SLOT        0

#define OAM_MC_GROUP_CTRL_PORT        0

typedef enum
{
    MRVL_MC_DIRECTION_US = 0x00,
    MRVL_MC_DIRECTION_DS = 0x01
}MRVL_MC_DIRECTION_E;


/*Member mangament table*/
typedef enum
{
    OAM_MC_IGMP_PDU_TYPE_IPV4,
    OAM_MC_IGMP_PDU_TYPE_IPV6
}OAM_MC_IGMP_PDU_TYPE_E;

typedef enum
{
    OAM_MC_DATA_PATH_DIR_DOWNSTREAM,
    OAM_MC_DATA_PATH_DIR_UPSTREAM       
}OAM_MC_DATA_PATH_DIR_E;

/*IGMP group table*/
typedef struct
{
    UINT32  group_ip_address;
    UINT32  group_member_bitmap;
    UINT16  mc_vlan; /*The VID received from join message tagged vid*/
    BOOL    valid_if;
}OAM_MC_GROUP_MEMBER_T;

typedef enum
{
    OAM_MC_GROUP_TABLE_MEMBER_JOIN =1,
    OAM_MC_GROUP_TABLE_MEMBER_LEAVE,
    OAM_MC_GROUP_TABLE_MEMBER_CLEAR,   /*CLear up port membership,reset to null*/
    OAM_MC_GROUP_TABLE_MEMBER_DESTORY  /*Destory gda group                     */
}OAM_MC_GROUP_TABLE_OPER_E;

/*IGMP  PM counters*/
typedef struct
{
    UINT32 igmp_pkt_received;
    UINT32 igmp_pkt_send;
    UINT32 igmp_pkt_discard;

    UINT32 igmp_msg_report;
    UINT32 igmp_msg_leave;
    UINT32 igmp_msg_query;
}OAM_MC_PERFORMANCE_COUNT_T;

typedef enum
{
    OAM_IGMP_STAT_RECEIVED_TOTAL = 1,
    OAM_IGMP_STAT_SEND_TOTAL,        
    OAM_IGMP_STAT_REPORT_MSG,
    OAM_IGMP_STAT_LEAVE_MSG,
    OAM_IGMP_STAT_QUERY_MSG,
    OAM_IGMP_STAT_DISCARD_TOTAL       
}OAM_MC_PERFORMANCE_TYPE_E;

typedef enum
{
    OAM_MC_GROUP_SEND_GENERAL_QUERY = 1,
    OAM_MC_GROUP_SEND_SPECIAL_QUERY,        
    OAM_MC_GROUP_RECV_QUERY_REPLY
}OAM_MC_GROUP_QUERY_OPER_E;

typedef OAM_MC_GROUP_MEMBER_T OAM_MC_GROUP_MEMBER_ARRAY_T[OAM_MC_MAX_GROUP_MEMBER_NUM];

typedef struct
{
    OAM_MC_PROTOCOL_TYPE_E     mc_mode ;
    UINT8                      max_query_interval_time;
    UINT8                      max_retransmit_count;
    BOOL                       proxy_reporting_enable;   
    OAM_MC_CTRL_TYPE_E         ctrl_type;   
}OAM_MC_PROTOCOL_CONF_T;

/*
* IGMP message for IGMP_MSG_T.type
*/
#define ETH_VLAN_TPID_8100_TYPE        0X8100
#define ETH_VLAN_TPID_9100_TYPE        0X9100
#define ETH_VLAN_TPID_88A8_TYPE        0X88A8
#define ETH_IP_TYPE                    0x0800
#define ETH_IP_SUB_PROTOCOL_IGMP       0X02

typedef enum
{
    IGMP_PROTOCOL_MEMBERSHIP_QUERY    = 0X11,
    IGMP_PROTOCOL_V2_REPORT           = 0X16,
    IGMP_PROTOCOL_V2_LEAVE            = 0X17
}IGMP_PROTOCOL_TYPE_E;
    
typedef struct 
{
    OAM_ETH_MAC_ADDR_T  dst_mac;
    OAM_ETH_MAC_ADDR_T  src_mac;
    UINT16              tag_control;
    UINT16              vlan_tag;
    UINT16              eth_type;
}POS_PACKED ETH_HEADER_WITH_TAG_T;

typedef struct
{
    OAM_ETH_MAC_ADDR_T     dst_mac;
    OAM_ETH_MAC_ADDR_T     src_mac;
    UINT16                 eth_type;
}POS_PACKED ETH_HEADER_T;

typedef struct
{
    UINT8      version;
    UINT8      diff_serv;
    UINT16     total_len;
    UINT16     id;
    UINT16     flags;
    UINT8      time_to_live;
    UINT8      protocol;
    UINT16     chk_sum;
    UINT32     src_ip;
    UINT32     dst_ip;
    UINT32     option;
}POS_PACKED IP_HEADER_T;

typedef struct
{
    UINT8      type;          /* version & type of IGMP message  */
    UINT8      max_resp_time;
    UINT16     cksum;         /* IP-style checksum               */
    UINT32     groupId;       /* group address being reported    */
}POS_PACKED IGMP_HEADER_T;

typedef struct
{
    ETH_HEADER_WITH_TAG_T    mac_seg;
    IP_HEADER_T              ip_seg;
    IGMP_HEADER_T            igmp_seg;
    UINT8                    tailer[14];
}POS_PACKED IGMP_PACKET_T;

typedef enum
{
    IGMP_SNED_GROUP_SPECIAL_QUERY_MANUALLY
}IGMP_OPERATION_CMD_E;

typedef INT32 (*OAM_MC_GROUP_MAINTENANCE_HANDLER_T)(UINT32 gda_ip, UINT32 user_port);    

#define OAM_IGMP_ALARM_DESC_LEN                     50

typedef struct
{
    IGMP_OPERATION_CMD_E                opcode;
    OAM_MC_GROUP_MAINTENANCE_HANDLER_T  alarm_handler;
    UINT8                               alarm_desc[OAM_IGMP_ALARM_DESC_LEN]; /*Alarm description, which describe alarm information or status */
}OAM_CUSTOM_ALARM_HANDLER_T;


/*-------------------------------------------------------------*/
/*Low layer APIs,implementated by Marvell                      */
/*-------------------------------------------------------------*/
INT32 oam_mc_igmp_protocol_send_func(UINT8 *pMsg, UINT16 msg_len, OAM_MC_DATA_PATH_DIR_E direction, UINT8 port_num);
INT32 oam_mc_igmp_protocol_received_func(UINT8 *pMsg,UINT16 *msg_len, OAM_MC_DATA_PATH_DIR_E *direction, UINT8 *port_num);

typedef INT32 (*MC_ETH_RX_IGMP_PACKET)(UINT8 *pMsg, UINT16 *msg_len, OAM_MC_DATA_PATH_DIR_E *direction, UINT8 *port_num);
typedef INT32 (*MC_ETH_TX_IGMP_PACKET)(UINT8 *pMsg, UINT16 msg_len, OAM_MC_DATA_PATH_DIR_E direction, UINT8 port_num);

/* Check wether specific bit of x is ture */
#define M_IGMP_GROUP_MEMBER_BIT_CHECK(x, bit)    (((x) >> (bit-1)) & 0x01)    

/* Set specific bit of x to 1 or 0*/
#define M_IGMP_SET_GROUP_MEMBER_JOIN(x, bit)     ((x) =((x)|((0x01)<<(bit-1))))    
#define M_IGMP_SET_GROUP_MEMBER_LEAVE(x, bit)    ((x)=((x)&(~((0x01)<<(bit-1)))))
#define M_IGMP_SET_GROUP_MEMBER_CLEAR(x)         memset(&(x),0,sizeof(x))

#endif /*__OAM_MC_H__*/
