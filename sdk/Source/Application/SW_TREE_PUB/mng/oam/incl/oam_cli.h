/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : oam_cli.h                                                 **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM CLI                                     **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    12/Jan/2011   - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __OAM_CLI_H__
#define __OAM_CLI_H__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>
#include "clish/shell.h"

#include "oam_expo.h"
#include "oam_stack_ossl.h"

//#define BOOL_TRUE  1
//#define BOOL_FALSE 0
#define  bool_t BOOL

typedef struct
{
    FILE    *file_fd;
    PID_T   pthread_pid;
    bool_t  valid;
}OAM_CLI_TRACE_RECORD_T;

//#define OAM_PRINT(format, ...)  tinyrl_printf(clish_shell__get_tinyrl(instance),format , ##__VA_ARGS__)
#define OAM_PRINT printf

#define OAM_CLI_CHECK_API_CALLS_RETURN_AND_LOG_ERROR(_ret_, _LogPrt) \
{\
    INT32 rc = _ret_; \
    if (rc != OAM_EXIT_OK) { \
        mipc_printf(name, "%s ,err %s\n",(_LogPrt),ctc_cli_error_to_string(rc)); \
        return rc;\
    }\
}

#define OAM_CLI_CHECK_API_CALLS_RETURN_AND_LOG_ERROR_SVC(ret,_LogPrt) \
{\
    if (ret != __TRUE) {\
        OAM_PRINT("%s ,err %s\n",(_LogPrt),ctc_cli_error_to_string(ret));\
        rc=ret; \
        return(&rc); \
    }\
}

#define OAM_CLI_CMD_VTY_CSTRING_PRT(sting_prt ,len)\
{\
    UINT32 i,pLen ;\
    pLen = (os_strlen((INT8*)sting_prt)>len ? len : os_strlen((INT8*)sting_prt));\
    for(i = 0 ;i <pLen ;i ++ )OAM_PRINT("%c",sting_prt[i]);\
        OAM_PRINT("\r\n");\
}

/*copy mac_addr to string format*/
#define OAM_CLI_MAC_ADDRESS_SPRINTF_INTO_MAC_STRING(_mac_addr_no_joined ,__mac_string__)\
{\
    INT32 mac_len_count = 0 ;\
    for(mac_len_count = 0; mac_len_count < OAM_MAC_ADDR_LEN; mac_len_count++)sprintf(&(__mac_string__[3*mac_len_count]),"%02x:",_mac_addr_no_joined[mac_len_count]);\
    __mac_string__[(OAM_MAC_ADDR_LEN*3)-1]=(char)0x0;\
}

/********************************************************************************/
/*                              EPON CTC OAM CLI functions                      */
/********************************************************************************/
bool_t Oam_cli_cmd_onu_reset(const clish_shell_t                          *instance,
                             const lub_argv_t                             *argv);
                           
bool_t Oam_cli_cmd_show_onu_registered_status(const clish_shell_t         *instance,
                                              const lub_argv_t            *argv);
bool_t Oam_cli_cmd_show_onu_initialized_status(const clish_shell_t        *instance,
                                               const lub_argv_t           *argv);
                                             
bool_t Oam_cli_cmd_show_log_level(const clish_shell_t                     *instance,
                                  const lub_argv_t                        *argv);
bool_t Oam_cli_cmd_show_log_output(const clish_shell_t                    *instance,
                                   const lub_argv_t                       *argv);
bool_t Oam_cli_cmd_set_log_level(const clish_shell_t                      *instance,
                                 const lub_argv_t                         *argv);
bool_t Oam_cli_cmd_set_log_output(const clish_shell_t                     *instance,
                                  const lub_argv_t                        *argv);
                                
bool_t Oam_cli_cmd_show_onu_sn(const clish_shell_t                        *instance,
                               const lub_argv_t                           *argv);
bool_t Oam_cli_cmd_show_onu_fw(const clish_shell_t                        *instance,
                               const lub_argv_t                           *argv);
bool_t Oam_cli_cmd_show_onu_chipset(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);
bool_t Oam_cli_cmd_show_onu_loid(const clish_shell_t                      *instance,
                                 const lub_argv_t                         *argv);
bool_t Oam_cli_cmd_set_onu_loid(const clish_shell_t                       *instance,
                                const lub_argv_t                          *argv);
bool_t Oam_cli_cmd_show_onu_capability (const clish_shell_t               *instance,
                                        const lub_argv_t                  *argv);
bool_t Oam_cli_cmd_show_onu_capability_plus(const clish_shell_t           *instance,
                                            const lub_argv_t              *argv);
bool_t Oam_cli_cmd_show_onu_capability_plus_3(const clish_shell_t         *instance,
                                              const lub_argv_t            *argv);
                                            
bool_t Oam_cli_cmd_show_onu_optical_diag(const clish_shell_t              *instance,
                                         const lub_argv_t                 *argv);
                                       
bool_t Oam_cli_cmd_show_onu_mac_address(const clish_shell_t               *instance,
                                        const lub_argv_t                  *argv);
                                      
bool_t Oam_cli_cmd_set_onu_mux_mng_global(const clish_shell_t                        *instance,
                               const lub_argv_t                           *argv);
                               
bool_t Oam_cli_cmd_show_onu_mux_mng_global(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);

bool_t Oam_cli_cmd_set_onu_mux_mng_snmp(const clish_shell_t                        *instance,
                               const lub_argv_t                           *argv);

bool_t Oam_cli_cmd_show_onu_mux_mng_snmp(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);

bool_t Oam_cli_cmd_show_onu_oam_xml_para(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);

bool_t Oam_cli_cmd_show_oam_variable(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);

bool_t Oam_cli_cmd_show_oam_attributehook(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);

bool_t Oam_cli_cmd_show_oam_ouihook(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);

bool_t Oam_cli_cmd_show_oam_loopback(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);

bool_t Oam_cli_cmd_set_oam_loopback(const clish_shell_t                       *instance,
                                const lub_argv_t                          *argv);

bool_t Oam_cli_cmd_set_oam_ouihook(const clish_shell_t                       *instance,
                                const lub_argv_t                          *argv);

bool_t Oam_cli_cmd_set_oam_attributehook(const clish_shell_t                       *instance,
                                const lub_argv_t                          *argv);

bool_t Oam_cli_cmd_set_oam_variablehook(const clish_shell_t                       *instance,
                                const lub_argv_t                          *argv);

bool_t Oam_cli_cmd_send_oam_event(const clish_shell_t                       *instance,
                                const lub_argv_t                          *argv);

bool_t Oam_cli_cmd_set_pwr_saving_cfg(const clish_shell_t                 *instance,
                                      const lub_argv_t                    *argv);      

bool_t Oam_cli_cmd_show_pon_pwr_saving_cfg(const clish_shell_t                      *instance,
                                           const lub_argv_t                         *argv);

bool_t Oam_cli_cmd_show_pon_pwr_saving_cap(const clish_shell_t                 *instance,
                                           const lub_argv_t                    *argv);

bool_t Oam_cli_cmd_set_pon_protect_para(const clish_shell_t                 *instance,
                                        const lub_argv_t                    *argv);      

bool_t Oam_cli_cmd_show_pon_protect_para(const clish_shell_t                      *instance,
                                         const lub_argv_t                         *argv);

bool_t Oam_cli_cmd_set_pon_sleep_ctrl(const clish_shell_t                 *instance,
                                      const lub_argv_t                    *argv);      
bool_t Oam_cli_cmd_show_pon_sleep_ctrl(const clish_shell_t                      *instance,
                                       const lub_argv_t                         *argv);
bool_t Oam_cli_cmd_show_pon_service_sla(const clish_shell_t               *instance,
                                         const lub_argv_t                 *argv);  
bool_t Oam_cli_cmd_pon_service_sla_deactive(const clish_shell_t           *instance,
                                            const lub_argv_t              *argv);      
bool_t Oam_cli_cmd_pon_service_sla_active(const clish_shell_t             *instance,
                                          const lub_argv_t                *argv); 
                                             
bool_t Oam_cli_cmd_show_pon_hold_over(const clish_shell_t                 *instance,
                                      const lub_argv_t                    *argv);      
bool_t Oam_cli_cmd_pon_hold_over(const clish_shell_t                      *instance,
                                 const lub_argv_t                         *argv);      
bool_t Oam_cli_cmd_pon_hold_over_deactive(const clish_shell_t             *instance,
                                          const lub_argv_t                *argv); 
                                             
bool_t Oam_cli_cmd_pon_active_if_adminstate(const clish_shell_t           *instance,
                                            const lub_argv_t              *argv);     
bool_t Oam_cli_cmd_show_pon_active_if_adminstate(const clish_shell_t      *instance,
                                                 const lub_argv_t         *argv);  
                                             
bool_t Oam_cli_cmd_show_pon_fec_ability(const clish_shell_t               *instance,
                                        const lub_argv_t                  *argv);  
bool_t Oam_cli_cmd_pon_fec_mode(const clish_shell_t                       *instance,
                                const lub_argv_t                          *argv);                                
bool_t Oam_cli_cmd_get_pon_fec_mode(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);
bool_t Oam_cli_cmd_set_pon_power_tx_ctrl(const clish_shell_t                   *instance,
                               const lub_argv_t                           *argv);

bool_t Oam_cli_cmd_show_pon_power_tx_ctrl(const clish_shell_t             *instance,
                                    const lub_argv_t                      *argv);
bool_t Oam_cli_cmd_set_uni_admin(const clish_shell_t                      *instance,
                                 const lub_argv_t                         *argv);
bool_t Oam_cli_cmd_get_uni_admin(const clish_shell_t                      *instance,
                                 const lub_argv_t                         *argv);
bool_t Oam_cli_cmd_show_uni_link_status(const clish_shell_t               *instance,
                                        const lub_argv_t                  *argv);
bool_t Oam_cli_cmd_set_uni_autoneg(const clish_shell_t                    *instance,
                                   const lub_argv_t                       *argv);
bool_t Oam_cli_cmd_get_uni_autoneg(const clish_shell_t                    *instance,
                                   const lub_argv_t                       *argv);
bool_t Oam_cli_cmd_set_uni_restart_autoneg(const clish_shell_t            *instance,
                                           const lub_argv_t               *argv);
bool_t Oam_cli_cmd_set_uni_flow_control(const clish_shell_t               *instance,
                                        const lub_argv_t                  *argv);
bool_t Oam_cli_cmd_get_uni_flow_control(const clish_shell_t               *instance,
                                        const lub_argv_t                  *argv);
bool_t Oam_cli_cmd_set_uni_policing(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);                                            
bool_t Oam_cli_cmd_get_uni_policing(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);
bool_t Oam_cli_cmd_set_uni_rate_limit(const clish_shell_t                 *instance,
                                      const lub_argv_t                    *argv);                                                 
bool_t Oam_cli_cmd_get_uni_rate_limit(const clish_shell_t                 *instance,
                                      const lub_argv_t                    *argv);
bool_t Oam_cli_cmd_set_uni_loopdetect(const clish_shell_t                 *instance,
                                      const lub_argv_t                    *argv);
bool_t Oam_cli_cmd_get_uni_loopdetect(const clish_shell_t                 *instance,
                                      const lub_argv_t                    *argv);
bool_t Oam_cli_cmd_set_uni_disable_looped(const clish_shell_t                 *instance,
                                          const lub_argv_t                    *argv);
bool_t Oam_cli_cmd_get_uni_disable_looped(const clish_shell_t                 *instance,
                                          const lub_argv_t                    *argv);
bool_t Oam_cli_cmd_get_uni_mac_aging_time(const clish_shell_t                 *instance,
                                          const lub_argv_t                    *argv);
bool_t Oam_cli_cmd_set_uni_mac_aging_time(const clish_shell_t                 *instance,
                                          const lub_argv_t                    *argv);
bool_t Oam_cli_cmd_get_uni_local_ability(const clish_shell_t              *instance,
                                         const lub_argv_t                 *argv);
bool_t Oam_cli_cmd_get_uni_adv_ability(const clish_shell_t                *instance,
                                       const lub_argv_t                   *argv);                                             

bool_t Oam_cli_cmd_pon_multi_llid_ctrl(const clish_shell_t                *instance,
                                       const lub_argv_t                   *argv);  
bool_t Oam_cli_cmd_show_pon_multi_llid_ctrl(const clish_shell_t           *instance,
                                            const lub_argv_t              *argv);  
bool_t Oam_cli_cmd_show_pon_llid_queue_configure(const clish_shell_t      *instance,
                                                 const lub_argv_t         *argv);  
bool_t Oam_cli_cmd_pon_llid_queue_configure(const clish_shell_t           *instance,
                                            const lub_argv_t              *argv);  

bool_t Oam_cli_cmd_show_voip_pots_number(const clish_shell_t              *instance,
                                         const lub_argv_t                 *argv);  
bool_t Oam_cli_cmd_show_voip_all_pots_admin(const clish_shell_t           *instance,
                                            const lub_argv_t              *argv);  
bool_t Oam_cli_cmd_voip_pots_admin_set(const clish_shell_t                *instance,
                                       const lub_argv_t                   *argv);  
bool_t Oam_cli_cmd_voip_iad_info_get(const clish_shell_t                  *instance,
                                     const lub_argv_t                     *argv);  
bool_t Oam_cli_cmd_show_voip_global_parameter_config(const clish_shell_t  *instance,
                                                     const lub_argv_t     *argv);  
bool_t Oam_cli_cmd_voip_global_parameter_config(const clish_shell_t       *instance,
                                                const lub_argv_t          *argv);  
                                              
bool_t Oam_cli_cmd_show_voip_h248_parameter_config(const clish_shell_t    *instance,
                                                   const lub_argv_t       *argv);  
bool_t Oam_cli_cmd_voip_h248_parameter_config(const clish_shell_t         *instance,
                                              const lub_argv_t            *argv);  
bool_t Oam_cli_cmd_show_voip_h248_user_tid_info_get(const clish_shell_t   *instance,
                                                    const lub_argv_t      *argv);  

bool_t Oam_cli_cmd_voip_h248_user_tid_config_set(const clish_shell_t      *instance,
                                                 const lub_argv_t         *argv);  
bool_t Oam_cli_cmd_show_voip_h248_rtp_config_get(const clish_shell_t      *instance,
                                                 const lub_argv_t         *argv);  

bool_t Oam_cli_cmd_voip_h248_rtp_config_set(const clish_shell_t           *instance,
                                            const lub_argv_t              *argv);  
bool_t Oam_cli_cmd_show_voip_h248_rtp_info_get(const clish_shell_t        *instance,
                                               const lub_argv_t           *argv);  

bool_t Oam_cli_cmd_voip_sip_parameter_config_get(const clish_shell_t      *instance,
                                                 const lub_argv_t         *argv);  
bool_t Oam_cli_cmd_voip_sip_parameter_config_set(const clish_shell_t      *instance,
                                                 const lub_argv_t         *argv);  

bool_t Oam_cli_cmd_voip_sip_user_config_get(const clish_shell_t           *instance,
                                            const lub_argv_t              *argv);  
bool_t Oam_cli_cmd_voip_sip_user_config_set(const clish_shell_t           *instance,
                                            const lub_argv_t              *argv);  
        
bool_t Oam_cli_cmd_voip_fax_config_get(const clish_shell_t                *instance,
                                       const lub_argv_t                   *argv);  
bool_t Oam_cli_cmd_voip_fax_config_set(const clish_shell_t                *instance,
                                       const lub_argv_t                   *argv);  

bool_t Oam_cli_cmd_show_voip_h248_operationStatus(const clish_shell_t     *instance,
                                                  const lub_argv_t        *argv);  
bool_t Oam_cli_cmd_show_voip_pots_status(const clish_shell_t              *instance,
                                         const lub_argv_t                 *argv);  
bool_t Oam_cli_cmd_voip_iad_operate(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);  
bool_t Oam_cli_cmd_voip_sip_digit_map_set(const clish_shell_t   *instance,
                                                    const lub_argv_t      *argv);  

bool_t Oam_cli_cmd_show_eth_port_vlan(const clish_shell_t                 *instance,
                                      const lub_argv_t                    *argv);  
bool_t Oam_cli_cmd_set_eth_port_vlan_transparent(const clish_shell_t      *instance,
                                                 const lub_argv_t         *argv);  
bool_t Oam_cli_cmd_set_eth_port_vlan_tag(const clish_shell_t              *instance,
                                         const lub_argv_t                 *argv);  
bool_t Oam_cli_cmd_set_eth_port_vlan_trunk(const clish_shell_t            *instance,
                                           const lub_argv_t               *argv);  
bool_t Oam_cli_cmd_set_eth_port_vlan_translation(const clish_shell_t      *instance,
                                                 const lub_argv_t         *argv);  

bool_t Oam_cli_cmd_set_eth_port_class_n_marking_add(const clish_shell_t   *instance,
                                                    const lub_argv_t      *argv);  
bool_t Oam_cli_cmd_set_eth_port_class_n_marking_del(const clish_shell_t   *instance,
                                                    const lub_argv_t      *argv);  
bool_t Oam_cli_cmd_set_eth_port_class_n_marking_clear(const clish_shell_t *instance,
                                                      const lub_argv_t    *argv);  
bool_t Oam_cli_cmd_show_eth_port_class_n_marking_configure(const clish_shell_t *instance,
                                                           const lub_argv_t    *argv);  

bool_t Oam_cli_cmd_mc_show_igmp_statistic(const clish_shell_t             *instance,
                                          const lub_argv_t                *argv);
bool_t Oam_cli_cmd_mc_igmp_protocol_switch(const clish_shell_t            *instance,
                                           const lub_argv_t               *argv);
bool_t Oam_cli_cmd_show_mc_igmp_protocol_switch(const clish_shell_t       *instance,
                                                const lub_argv_t          *argv);
bool_t Oam_cli_cmd_show_mc_igmp_group(const clish_shell_t                 *instance,
                                      const lub_argv_t                    *argv);
bool_t Oam_cli_cmd_show_mc_fast_leave_ability(const clish_shell_t         *instance,
                                              const lub_argv_t            *argv);
bool_t Oam_cli_cmd_show_mc_fast_leave_admin(const clish_shell_t           *instance,
                                            const lub_argv_t              *argv);
bool_t Oam_cli_cmd_set_mc_fast_leave_admin(const clish_shell_t            *instance,
                                          const lub_argv_t                *argv);
bool_t Oam_cli_cmd_set_mc_max_group_num(const clish_shell_t               *instance,
                                        const lub_argv_t                  *argv);
bool_t Oam_cli_cmd_show_mc_max_group_num(const clish_shell_t              *instance,
                                         const lub_argv_t                 *argv);

bool_t Oam_cli_cmd_set_mc_tag_oper(const clish_shell_t               *instance,
                                        const lub_argv_t                  *argv);
bool_t Oam_cli_cmd_show_mc_tag_oper(const clish_shell_t              *instance,
                                         const lub_argv_t                 *argv);                                         

bool_t Oam_cli_cmd_set_mc_LMQ_query_intv(const clish_shell_t              *instance,
                                         const lub_argv_t                 *argv);                                       
bool_t Oam_cli_cmd_set_mc_LMQ_max_query_count(const clish_shell_t         *instance,
                                              const lub_argv_t            *argv);

bool_t Oam_cli_cmd_mc_pkt_send_test(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);
bool_t Oam_cli_cmd_mc_vlan_operate(const clish_shell_t                    *instance,
                                   const lub_argv_t                       *argv);
bool_t Oam_cli_cmd_show_mc_vlan_snooping(const clish_shell_t              *instance,
                                         const lub_argv_t                 *argv);
bool_t Oam_cli_cmd_mc_ctrl_entry_add(const clish_shell_t                  *instance,
                                     const lub_argv_t                     *argv);
bool_t Oam_cli_cmd_mc_ctrl_entry_del(const clish_shell_t                  *instance,
                                     const lub_argv_t                     *argv);
bool_t Oam_cli_cmd_mc_ctrl_entry_show(const clish_shell_t                 *instance,
                                      const lub_argv_t                    *argv);
bool_t Oam_cli_cmd_mc_ctrl_entry_clear(const clish_shell_t                *instance,
                                       const lub_argv_t                   *argv);

bool_t Oam_cli_cmd_alarm_perf_show(const clish_shell_t                    *instance,
                                   const lub_argv_t                       *argv);
bool_t Oam_cli_cmd_alarm_config_show(const clish_shell_t                  *instance,
                                     const lub_argv_t                     *argv);
bool_t Oam_cli_cmd_alarm_admin_cfg(const clish_shell_t                    *instance,
                                   const lub_argv_t                       *argv);
bool_t Oam_cli_cmd_alarm_threshold_cfg(const clish_shell_t                *instance,
                                       const lub_argv_t                   *argv);
bool_t Oam_cli_cmd_alarm_generation_report(const clish_shell_t            *instance,
                                           const lub_argv_t               *argv);
bool_t Oam_cli_cmd_set_pm_admin(const clish_shell_t                    *instance,
                                   const lub_argv_t                       *argv);
bool_t Oam_cli_cmd_set_pm_current(const clish_shell_t                  *instance,
                                     const lub_argv_t                     *argv);
bool_t Oam_cli_cmd_show_pm_admin(const clish_shell_t                    *instance,
                                   const lub_argv_t                       *argv);
bool_t Oam_cli_cmd_show_pm_current(const clish_shell_t                *instance,
                                       const lub_argv_t                   *argv);
bool_t Oam_cli_cmd_show_pm_history(const clish_shell_t            *instance,
                                           const lub_argv_t               *argv);
bool_t Oam_cli_cmd_update_test(const clish_shell_t                        *instance,
                               const lub_argv_t                           *argv);
bool_t Oam_cli_cmd_show_pon_key(const clish_shell_t                       *instance,
                                const lub_argv_t                          *argv);  
bool_t Oam_cli_cmd_show_pon_dba(const clish_shell_t                       *instance,
                                const lub_argv_t                          *argv);                                
bool_t Oam_cli_cmd_sim_oam_pkt_init(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);
bool_t Oam_cli_cmd_sim_oam_pkt_buf(const clish_shell_t                    *instance,
                                   const lub_argv_t                       *argv);
bool_t Oam_cli_cmd_sim_oam_pkt_send(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);
bool_t Oam_cli_cmd_sim_oam_pkt_buf_send(const clish_shell_t                   *instance,
                                    const lub_argv_t                      *argv);

#endif /*__OAM_CLI_H__*/
