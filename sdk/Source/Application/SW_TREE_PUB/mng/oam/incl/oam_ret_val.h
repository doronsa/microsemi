/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_ret_val.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : Comon definition of EOAM stack return value type          **/
/**                                                                          **/
/****************************************************************************** 
**                                                                             
*   MODIFICATION HISTORY:                                                      
*
*    12/Jan/2011   - initial version created.         
*                                                                              
******************************************************************************/

#ifndef __OAM_RET_VAL_H__
#define __OAM_RET_VAL_H__

/*----------------------------------------------------------*/
/*         Return codes                                     */
/*----------------------------------------------------------*/
#define OAM_EXIT_OK                       0
#define OAM_ERROR_BASE                    (-17000)
#define OAM_ERROR_EXIT                    (OAM_ERROR_BASE-1)
#define OAM_ERROR_TIME_OUT                (OAM_ERROR_BASE-2) 
#define OAM_ERROR_NOT_IMPLEMENTED         (OAM_ERROR_BASE-3)
#define OAM_ERROR_PARAMETER               (OAM_ERROR_BASE-4)
#define OAM_ERROR_HARDWARE                (OAM_ERROR_BASE-5)
#define OAM_ERROR_MEMORY                  (OAM_ERROR_BASE-6)
#define OAM_ERROR_NOT_SUPPORT             (OAM_ERROR_BASE-7)
#define OAM_ERROR_QUERY_FAILED            (OAM_ERROR_BASE-8)
#define OAM_ERROR_NU_NOT_AVAILABLE        (OAM_ERROR_BASE-9)
#define OAM_ERROR_OLT_NOT_EXIST           (OAM_ERROR_BASE-10)
#define OAM_ERROR_EXIT_ONU_DBA_THRESHOLDS (OAM_ERROR_BASE-11)
#define OAM_ERROR_TFTP_SEND_FAIL          (OAM_ERROR_BASE-12)
#define OAM_ERROR_NOT_INITIALIZED         (OAM_ERROR_BASE-13)
#define OAM_ERROR_ALREADY_EXIST           (OAM_ERROR_BASE-14)
#define OAM_ERROR_DB_FULL                 (OAM_ERROR_BASE-15)
#define OAM_ERROR_OUT_OF_RANGE            (OAM_ERROR_BASE-19)

#endif /*__OAM_RET_VAL_H__*/
