/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : mrvl_type.h                                               **/
/**                                                                          **/
/**  DESCRIPTION : MARVELL private definition                                **/
/**                                                                          **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *                                                                               
 *         Victor  - initial version created.   10/Jan/2011           
 *                                                                              
 ******************************************************************************/

#ifndef __MRVL_TYPE_H__
#define __MRVL_TYPE_H__

typedef unsigned int  MRVL_ERROR_CODE_T;

#define MRVL_EXIT_OK                            (0)
#define MRVL_OK                                 (MRVL_EXIT_OK)
#define MRVL_ERROR_BASE                         (-17000)
#define MRVL_ERROR_EXIT                         (MRVL_ERROR_BASE-1)
#define MRVL_ERROR_TIME_OUT                     (MRVL_ERROR_BASE-2) 
#define MRVL_ERROR_NOT_IMPLEMENTED              (MRVL_ERROR_BASE-3)
#define MRVL_ERROR_PARAMETER                    (MRVL_ERROR_BASE-4)
#define MRVL_ERROR_HARDWARE                     (MRVL_ERROR_BASE-5)
#define MRVL_ERROR_MEMORY                       (MRVL_ERROR_BASE-6)
#define MRVL_ERROR_NOT_SUPPORT                  (MRVL_ERROR_BASE-7)
#define MRVL_ERROR_QUERY_FAILED                 (MRVL_ERROR_BASE-8)
#define MRVL_ERROR_NU_NOT_AVAILABLE             (MRVL_ERROR_BASE-9)
#define MRVL_ERROR_OLT_NOT_EXIST                (MRVL_ERROR_BASE-10)
#define MRVL_ERROR_EXIT_ONU_DBA_THRESHOLDS      (MRVL_ERROR_BASE-11)
#define MRVL_ERROR_TFTP_SEND_FAIL               (MRVL_ERROR_BASE-12)
#define MRVL_ERROR_NOT_INITIALIZED              (MRVL_ERROR_BASE-13)
#define MRVL_ERROR_ALREADY_EXIST                (MRVL_ERROR_BASE-14)
#define MRVL_ERROR_DB_FULL                      (MRVL_ERROR_BASE-15)
#define MRVL_ERROR_OUT_OF_RANGE                 (MRVL_ERROR_BASE-19)

typedef struct
{
    unsigned char  epon_us_queue;
    unsigned char  epon_ds_queue;       
} MRVL_TRAFFIC_QUEUE_T;

#endif /*__MRVL_TYPE_H__*/
