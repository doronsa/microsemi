/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM Stack                                            **/
/**                                                                          **/
/**  FILE        : oam_expo.h                                                **/
/**                                                                          **/
/**  DESCRIPTION : Common definition to be outputted                         **/
/**                                                                          **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *                                                                               
 *         Victor  - initial version created.   10/Jan/2011           
 *                                                                              
 ******************************************************************************/

#ifndef __OAM_EXPO_H__
#define __OAM_EXPO_H__

#include "mrvl_type.h"

/*================================ Typedefs ==================================*/
/*Whether link EOAM stack to middlware*/
#define OAM_STACK_LINK_MIDWARE
#define OAM_STACK_LINK_TPM

#define  POS_PACKED      __attribute__ ((__packed__))

#define _LITTLE_ENDIAN

#ifdef _LITTLE_ENDIAN
#undef _BIG_ENDIAN
#endif

#ifdef _BIG_ENDIAN
#undef _LITTLE_ENDIAN
#endif

typedef char               CHAR;
typedef unsigned char      UCHAR;
typedef signed char        INT8;
typedef unsigned char      UINT8;
typedef signed short       INT16;
typedef unsigned short     UINT16;
typedef signed int         INT32;
typedef unsigned int       UINT32;
typedef long long          INT64;
typedef unsigned long long UINT64;

typedef signed int         STATUS;
typedef void               VOID;

#ifndef NULL
#define NULL ((VOID*)0)
#endif

#ifndef ERROR
#define ERROR     (-1)
#endif

#undef FALSE
#undef TRUE
#undef BOOL

typedef enum {FALSE = 0, TRUE = 1}  BOOL;

#endif /*__OAM_EXPO_H__*/
