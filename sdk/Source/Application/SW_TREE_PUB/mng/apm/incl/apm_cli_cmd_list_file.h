/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apm_cli_cmd_list_file.h                                   **/
/**                                                                          **/
/**  DESCRIPTION : This file has CLISH action/routine matchup                **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *                                                                            *
 ******************************************************************************/

/********************************************************************************/
/*                            APM show CLI functions                            */
/********************************************************************************/
  {"apm_cli_show_alarm_statistics",         apm_cli_show_alarm_statistics},
  {"apm_cli_show_pm_statistics",            apm_cli_show_pm_statistics},
  {"apm_cli_show_avc_statistics",           apm_cli_show_avc_statistics},  
  {"apm_cli_show_alarm_entity",             apm_cli_show_alarm_entity},
  {"apm_cli_show_pm_entity",                apm_cli_show_pm_entity},
  {"apm_cli_show_avc_entity",               apm_cli_show_avc_entity},
  {"apm_cli_show_alarm_list",               apm_cli_show_alarm_list},
  {"apm_cli_show_pm_list",                  apm_cli_show_pm_list},
  {"apm_cli_show_avc_list",                 apm_cli_show_avc_list},
  {"apm_cli_show_pm_related_alarm_types",   apm_cli_show_pm_related_alarm_types},  
  {"apm_cli_set_log_level",                 apm_cli_set_log_level},
  {"apm_cli_set_pm_interval",               apm_cli_set_pm_interval},  
  {"apm_cli_create_alarm_entity",           apm_cli_create_alarm_entity},
  {"apm_cli_create_pm_entity",              apm_cli_create_pm_entity},
  {"apm_cli_create_avc_entity",             apm_cli_create_avc_entity},
  {"apm_cli_delete_alarm_entity",           apm_cli_delete_alarm_entity},
  {"apm_cli_delete_pm_entity",              apm_cli_delete_pm_entity},
  {"apm_cli_delete_avc_entity",             apm_cli_delete_avc_entity},    
  {"apm_cli_set_alarm_admin",               apm_cli_set_alarm_admin},
  {"apm_cli_set_alarm_theshold",            apm_cli_set_alarm_theshold},
  {"apm_cli_set_alarm_state_info",          apm_cli_set_alarm_state_info},
  {"apm_cli_set_pm_accumulation_mode",      apm_cli_set_pm_accumulation_mode},
  {"apm_cli_set_pm_admin_bits",             apm_cli_set_pm_admin_bits},
  {"apm_cli_set_pm_pm_data",                apm_cli_set_pm_pm_data},
  {"apm_cli_set_pm_global_clear",           apm_cli_set_pm_global_clear},  
  {"apm_cli_reset_db",                      apm_cli_reset_db},
  {"apm_cli_synchronize_time",              apm_cli_synchronize_time},
  {"apm_cli_set_avc_admin",                 apm_cli_set_avc_admin},
  {"apm_cli_set_avc_value_int",             apm_cli_set_avc_value_int},
  {"apm_cli_set_avc_value_str",             apm_cli_set_avc_value_str},

/********************************************************************************/
/*                            APM debug CLI functions                           */
/********************************************************************************/
