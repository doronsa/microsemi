/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apm_cli.h                                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file is main include file. Has qprintf definitions   **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *                                                                            *
 ******************************************************************************/


#ifndef __APM_CLI_H__
#define __APM_CLI_H__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"


/********************************************************************************/
/*                              APM CLI functions                               */
/********************************************************************************/
bool_t apm_cli_show_alarm_statistics(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_show_pm_statistics(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_show_avc_statistics(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_show_alarm_entity(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_show_pm_entity(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_show_avc_entity(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_show_alarm_list(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_show_pm_list(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_show_avc_list(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_show_pm_related_alarm_types(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_log_level(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_pm_interval(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_create_alarm_entity(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_create_pm_entity(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_create_avc_entity(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_delete_alarm_entity(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_delete_pm_entity(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_delete_avc_entity(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_alarm_admin(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_alarm_theshold(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_alarm_state_info(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_pm_accumulation_mode(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_pm_admin_bits(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_pm_pm_data(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_pm_global_clear(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_reset_db(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_synchronize_time(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_avc_admin(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_avc_value_int(const clish_shell_t *instance, const lub_argv_t *argv);
bool_t apm_cli_set_avc_value_str(const clish_shell_t *instance, const lub_argv_t *argv);

#endif
