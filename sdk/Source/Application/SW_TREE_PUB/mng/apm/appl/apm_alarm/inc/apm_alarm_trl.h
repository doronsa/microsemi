/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apm_alarm_trl.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : APM TRL data structure definitions                        **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 *                                                                            *                              
 *  MODIFICATION HISTORY:                                                     *
 *                                                                            *
 *   alexv  - initial version created                                         *  
 * ========================================================================== *      
 *                                                                          
 * 
 *    Rev 1.0   March 28 2012 17:38:42   ken
 * Initial revision.
 * 
 *
 * 
 *                                                                     
 ******************************************************************************/



#ifndef __APM_ALARM_TRL_H__
#define __APM_ALARM_TRL_H__

typedef bool (*APM_ALARM_POLL_FUNCPTR)(uint32_t, uint32_t, uint8_t *, uint32_t *);

extern bool apm_alarm_trl_get_poll_handler(APM_ALARM_TYPE_T type, APM_ALARM_POLL_FUNCPTR *funcPtr);

#endif 
