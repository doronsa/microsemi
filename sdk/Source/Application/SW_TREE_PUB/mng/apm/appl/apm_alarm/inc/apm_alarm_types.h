/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apm_alarm_types.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : APM data types                                            **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *                                                                            *
 ******************************************************************************/

#ifndef __APM_ALARM_TYPES_H__
#define __APM_ALARM_TYPES_H__

typedef struct 
{
    uint32_t  type;           /*index, alarm type*/
    uint32_t  param1;         /*index, for example, slot/port*/
    uint32_t  param2;         /*the mapping info of omci or eoam, for omci is class+instanceid, for eoam is eoam alarm id */
    uint8_t   alarm_class;    /*has 2 values: APM_ALARM(0), APM_TCA(1)*/
    uint8_t   admin;          /*has 2 values: APM_ADMIN_DISABLE(0) and APM_ADMIN_ENABLE(1)*/
    uint8_t   state;          /*has 2 values: APM_STATE_OFF(0) and APM_STATE_ON(1)*/
    uint32_t  threshold;      /*alarm threshold*/
    uint32_t  clear_threshold;/*alarm clear threshold*/
    uint32_t  info;           /*alarm value*/
}APM_ALARM_T;



typedef struct 
{
    LIST_HEAD_T list;
    APM_ALARM_T alarm_entity;
}APM_ALARM_LIST_T;


typedef struct APM_ALARM_BLOCK_MEMORY
{
    APM_ALARM_LIST_T                alarm_list_entity;
    struct APM_ALARM_BLOCK_MEMORY   *next;   
}APM_ALARM_BLOCK_MEMORY;

#endif

