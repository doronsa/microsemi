/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apm_alarm_api.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : This file contains APM API definitions, prototypes        **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *    28March12     Ken  - initial version created.                           *
 *                                                                            *   
 *                                                                            *
 ******************************************************************************/


#ifndef __APM_ALARM_API_H__
#define __APM_ALARM_API_H__

extern bool apm_alarm_create_entity(uint32_t type, uint32_t param1, uint32_t param2, uint8_t alarm_class, uint8_t admin, uint32_t threshold, uint32_t clear_threshold);
extern bool apm_alarm_delete_entity(uint32_t type, uint32_t param1);
extern bool apm_alarm_set_admin(uint32_t type, uint32_t param1, uint8_t admin);
extern bool apm_alarm_set_threshold(uint32_t type, uint32_t param1, uint32_t threshold, uint32_t clear_threshold);
extern bool apm_alarm_db_obj_reset(void);

extern bool apm_alarm_get_threshold(uint32_t type, uint32_t param1, uint32_t *threshold, uint32_t *clear_threshold);


#endif


