/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apm.h                                                     **/
/**                                                                          **/
/**  DESCRIPTION : This file is main include file. Has qprintf definitions   **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *    12March10     Ken  - initial version created.                           *
 *                                                                            *   
 *                                                                            *
 ******************************************************************************/

#ifndef __INCapmh
#define __INCapmh
#include "mng_trace.h"


#define  POS_PACKED      __attribute__ ((__packed__))

#define QPRAPM_ERROR       MNG_TRACE_ERROR
#define QPRAPM_WARNING     MNG_TRACE_INFO
#define QPRAPM_INFO        MNG_TRACE_INFO
#define QPRAPM_TEST        MNG_TRACE_DEBUG


#define QPRAPMMOD_SUB1         (MNG_TRACE_MODULE_APM)

extern uint32_t apm_glob_trace;

#ifdef qprintf
#undef qprintf
#define qprintf(attribute, submodule, msg, arg...) do{if (attribute<=apm_glob_trace) {printf(msg, ##arg);}}while (0)
#else
#define qprintf(attribute, submodule, msg, arg...) do{if (attribute<=apm_glob_trace) {printf(msg, ##arg);}}while (0)
#endif

#endif 

extern unsigned int elapsedMilliTime(struct timespec *pLate, struct timespec *pEarly);
extern void *apm_alloc_memory(size_t size);
extern void apm_free_memory(void *apm_ptr);







