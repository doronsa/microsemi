/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apmTypes.h                                                **/
/**                                                                          **/
/**  DESCRIPTION : APM data types                                            **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *                                                                            *
 ******************************************************************************/

#ifndef __APM_AVC_TYPES_H__
#define __APM_AVC_TYPES_H__

#define APM_AVC_VALUE_LENGTH 28

typedef union
{
    //uint8_t   char_value;
    //uint16_t  short_value;
    uint32_t  int_value;
    uint8_t   string_value[APM_AVC_VALUE_LENGTH];
}APM_AVC_VALUE;

typedef struct 
{
    uint32_t  type;       /* avc type */
    uint32_t  param1;     /*the first index, for example, slot/port*/
    uint32_t  param2;     /*the second index, 0 when no use*/
    uint8_t   admin;      /*avc admin*/
    APM_AVC_VALUE value;
}APM_AVC_T;

typedef struct 
{
    LIST_HEAD_T     list;
    APM_AVC_T       avc_entity;
}APM_AVC_LIST_T;

typedef struct APM_AVC_BLOCK_MEMORY
{
    APM_AVC_LIST_T                  avc_list_entity;
    struct APM_AVC_BLOCK_MEMORY     *next;   
}APM_AVC_BLOCK_MEMORY;

#endif

