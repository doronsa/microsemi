/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apm_avc_api.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file contains APM API definitions, prototypes        **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *    28March10     Ken  - initial version created.                           *
 *                                                                            *   
 *                                                                            *
 ******************************************************************************/


#ifndef __INCApmApih
#define __INCApmApih

extern bool apm_avc_create_entity(uint32_t type, uint32_t param1, uint32_t param2, uint8_t admin);
extern bool apm_avc_delete_entity(uint32_t type, uint32_t param1);
extern bool apm_avc_set_admin(uint32_t type, uint32_t param1, uint8_t admin);
extern bool apm_avc_set_param2(uint32_t type, uint32_t param1, uint32_t param2);
extern bool apm_avc_db_obj_reset(void);
#endif


