/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apm_avc_trl.h                                                  **/
/**                                                                          **/
/**  DESCRIPTION : APM TRL data structure definitions                        **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 *                                                                            *                              
 *  MODIFICATION HISTORY:                                                     *
 *                                                                            *
 *   ken  - initial version created  28March12                                       *  
 * ========================================================================== *      
 *                                                                          
 *
 *
 * 
 *                                                                     
 ******************************************************************************/



#ifndef __APM_AVC_TRL_H__
#define __APM_AVC_TRL_H__

typedef bool (*APM_AVC_POLL_FUNCPTR)(APM_AVC_LIST_T *, APM_AVC_VALUE *);

extern bool apm_avc_trl_get_poll_handler(APM_AVC_TYPE_T type, APM_AVC_POLL_FUNCPTR *funcPtr);

#endif 
