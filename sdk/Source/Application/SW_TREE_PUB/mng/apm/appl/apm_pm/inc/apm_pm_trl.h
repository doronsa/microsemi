/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apm_pm_trl.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : APM TRL data structure definitions                        **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 *                                                                            *                              
 *  MODIFICATION HISTORY:                                                     *
 *                                                                            *
 *   ken  - initial version created                                           *  
 * ========================================================================== *      
 *                                                                          
 *   $Log: apmTrl.h,v $
 * 
 *
 * 
 *                                                                     
 ******************************************************************************/



#ifndef __APM_PM_TRL_H__
#define __APM_PM_TRL_H__


#define PMVAL_DELTA(oldpm,newpm)         ((INT32)((newpm) - (oldpm)) >= 0) ? ((newpm) - (oldpm)) : ((0xFFFFFFFF - (oldpm)) + 1 + (newpm))
#define APM_PM_CEILING_PLUS(pm,deltapm)  (((pm)+(deltapm))<(pm) ? 0xffffffff:((pm)+(deltapm)))


/*************************************************************** 
 *
 *  Ethernet PM
 *
 ***************************************************************/
typedef enum
{
    APMPM_ETHPM_FcsErrors                   = 0, 
    APMPM_ETHPM_ExcessiveCollisionCounter, 
    APMPM_ETHPM_LateCollisionCounter, 
    APMPM_ETHPM_Frametoolongs, 
    APMPM_ETHPM_BufferOverflowsReceive, 
    APMPM_ETHPM_BufferOverflowsTransmit,
    APMPM_ETHPM_SingleCollisionFrameCounter, 
    APMPM_ETHPM_MultCollisionFrameCounter, 
    APMPM_ETHPM_SqeCounter, 
    APMPM_ETHPM_DeferredTransmissionCounter, 
    APMPM_ETHPM_InternalMacTransmitError, 
    APMPM_ETHPM_CarrierSenseErrorCounter, 
    APMPM_ETHPM_AlignmentErrorCounter,
    APMPM_ETHPM_InternalMacReceiveErrorCounter,

} apmTrlPmEth_E;


/*************************************************************** 
 *
 *  Ethernet PM 3
 *
 ***************************************************************/
typedef enum
{
    APMPM_ETHPM3_dropEvents                 = 0,             
    APMPM_ETHPM3_octets,                 
    APMPM_ETHPM3_packets,                
    APMPM_ETHPM3_broadcastPackets,       
    APMPM_ETHPM3_multicastPackets,       
    APMPM_ETHPM3_undersizePackets,       
    APMPM_ETHPM3_fragments,              
    APMPM_ETHPM3_jabbers,                
    APMPM_ETHPM3_packets_64Octets,       
    APMPM_ETHPM3_packets_65_127Octets,   
    APMPM_ETHPM3_packets_128_255Octets,  
    APMPM_ETHPM3_packets_256_511Octets,  
    APMPM_ETHPM3_packets_512_1023Octets, 
    APMPM_ETHPM3_packets_1024_1518Octets,

} APMTRLMNG_ETHPM3;

/***************************************************************
 *
 *  GEM Port Network CTP PM: 
 *
 *  NOTE: These enumerations are NOT per counter, but offset in counter array
 *        The order of the counters is preserved
 *
 ***************************************************************/
typedef enum
{
    APMPM_ETHTCW_dropEvents_ds                 = 0,
    APMPM_ETHTCW_dropEvents_us,
    APMPM_ETHTCW_CRC_error_ds,
    APMPM_ETHTCW_underSize_ds,
    APMPM_ETHTCW_underSize_us,
    APMPM_ETHTCW_overSize_ds,
    APMPM_ETHTCW_overSize_us,
    APMPM_ETHTCW_fragmets_ds,
    APMPM_ETHTCW_jabbers_ds,
    APMPM_ETHTCW_collision_ds,
    APMPM_ETHTCW_discard_ds,
    APMPM_ETHTCW_discard_us,
    APMPM_ETHTCW_error_ds,
    APMPM_ETHTCW_status_change_times,

} APMTRLMNG_ETHTCW;

/***************************************************************
 *
 *  EPON Eth TCA PM 3
 *
 ***************************************************************/
typedef enum
{
    APMPM_ETHTCA_dropEvents_ds                 = 0,
    APMPM_ETHTCA_dropEvents_us,
    APMPM_ETHTCA_CRC_error_ds,
    APMPM_ETHTCA_underSize_ds,
    APMPM_ETHTCA_underSize_us,
    APMPM_ETHTCA_overSize_ds,
    APMPM_ETHTCA_overSize_us,
    APMPM_ETHTCA_fragmets_ds,
    APMPM_ETHTCA_jabbers_ds,
    APMPM_ETHTCA_collision_ds,
    APMPM_ETHTCA_discard_ds,
    APMPM_ETHTCA_discard_us,
    APMPM_ETHTCA_error_ds,
    APMPM_ETHTCA_status_change_times,

} APMTRLMNG_ETHTCA;

/***************************************************************
 *
 *  EPON Eth Packet Length Statistics
 *
 ***************************************************************/
typedef enum
{
    APMPM_ETH_64_ds                     = 0,
    APMPM_ETH_65_127_ds,
    APMPM_ETH_128_255_ds,
    APMPM_ETH_256_511_ds,
    APMPM_ETH_512_1023_ds,
    APMPM_ETH_1024_1518_ds,
    APMPM_ETH_64_us,
    APMPM_ETH_65_127_us,
    APMPM_ETH_128_255_us,
    APMPM_ETH_256_511_us,
    APMPM_ETH_512_1023_us,
    APMPM_ETH_1024_1518_us,

} APMTRLMNG_ETH_PACKET_LENGTH;

/***************************************************************
 *
 *  EPON Eth General Packet
 *
 ***************************************************************/
typedef enum
{
    APMPM_ETH_octet_ds                     = 0,
    APMPM_ETH_octet_us,
    APMPM_ETH_frame_ds,
    APMPM_ETH_frame_us,
    APMPM_ETH_broadcast_ds,
    APMPM_ETH_broadcast_us,
    APMPM_ETH_multicast_ds,
    APMPM_ETH_multicast_us,

} APMTRLMNG_ETH_GENERAL;


/*************************************************************** 
 *
 *  GEM Port Network CTP PM: 
 *
 *  NOTE: These enumerations are NOT per counter, but offset in counter array
 *        The order of the counters is preserved
 *
 ***************************************************************/
typedef enum
{
    APMPM_NETWORKCTPPM_transmittedGemFrames = 0,            
    APMPM_NETWORKCTPPM_receivedGemFrames,
    APMPM_NETWORKCTPPM_receivedPayloadBytes_lo,
    APMPM_NETWORKCTPPM_receivedPayloadBytes_hi,
    APMPM_NETWORKCTPPM_transmittedPayloadBytes_lo,
    APMPM_NETWORKCTPPM_transmittedPayloadBytes_hi,
    APMPM_NETWORKCTPPM_encryption_key_errors          /*not supported now*/
} APMTRLMNG_NETWORKCTPPM;


typedef enum
{
    APMPM_ETHFRAME_dropEvents_received      = 0,            
    APMPM_ETHFRAME_octets_received,
    APMPM_ETHFRAME_frames_received,
    APMPM_ETHFRAME_broadcast_frames_received,
    APMPM_ETHFRAME_multicast_frames_received,
    APMPM_ETHFRAME_CRCErroredFrames_received,
    APMPM_ETHFRAME_undersizeFrames_received,
    APMPM_ETHFRAME_oversizeFrames_received,
    APMPM_ETHFRAME_frames_64_octets,
    APMPM_ETHFRAME_frames_65_to_127_octets,
    APMPM_ETHFRAME_frames_128_to_255octets,
    APMPM_ETHFRAME_frames_256_to_511_octets,
    APMPM_ETHFRAME_frames_512_to_1023_octets,
    APMPM_ETHFRAME_frames_1024_to_1518_octets,
} APMTRLMNG_GEMFRAMEDOWNSTREAMPM;


typedef bool (*APM_PM_POLL_FUNCPTR)(APM_PM_LIST_T *);

typedef struct
{
    uint32_t              type;
    char                *objDescription;
    uint32_t              alarmType[APM_EXT_PM_BLOCK_SIZE] ;    
    APM_PM_POLL_FUNCPTR pmGetDatas;
} ApmPmRegister_T;

extern bool apm_pm_trl_get_poll_handler(APM_PM_TYPE_T type, APM_PM_POLL_FUNCPTR *funcPtr);
extern bool apm_pm_get_pm_type_for_alarm_type(uint32_t alarm_type, uint32_t *pm_type, uint32_t *alarm_index);

#endif 
