/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apm_pm_type.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : APM pm data types                                         **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *                                                                            *
 ******************************************************************************/

#ifndef __APM_PM_TYPES_H__
#define __APM_PM_TYPES_H__

#define APM_PM_MAX_MONITORED_PM  (8)

typedef struct 
{
    uint32_t  type;       /*index, pm type */
    uint32_t  param1;     /*index, for example, slot/port*/
    uint32_t  param2;     /*the mapping info of omci, is class+instanceid*/
    uint32_t  interval;   /*monitoring period*/
    uint32_t  start_time; /*time when monitoring started*/
    uint8_t   accumulation_mode;  /*has 2 values : APM_ACCUMULATION_INTERVAL(0) and APM_ACCUMULATION_CONTINUOUS(1)*/
    uint8_t   admin[APM_EXT_PM_BLOCK_SIZE];       /*pm data[]'s admin array*/
    uint32_t  pmData[APM_EXT_PM_BLOCK_SIZE];      /*pm data*/
    uint32_t  prevPmData[APM_EXT_PM_BLOCK_SIZE];  /*used for those HW counters which are not clear on read*/
    uint32_t  alarm_types[APM_EXT_PM_BLOCK_SIZE]; /*the corresponding tca's alarm type of pm data*/
    uint32_t  thresholds[APM_EXT_PM_BLOCK_SIZE];  /*the corresponding tca's threshold of pm data*/
    uint32_t  clear_thresholds[APM_EXT_PM_BLOCK_SIZE];    /*the corresponding tca's clear threshold of pm data*/
    uint32_t  tca_states[APM_EXT_PM_BLOCK_SIZE];  /*the corresponding tca's alarm state of pm data*/
    uint32_t  numPolls;                           /*the number of polling of the pm*/
}APM_PM_T;

typedef struct 
{
    LIST_HEAD_T list;
    APM_PM_T    pm_entity;
}APM_PM_LIST_T;

typedef struct APM_PM_BLOCK_MEMORY
{
    APM_PM_LIST_T               pm_list_entity;
    struct APM_PM_BLOCK_MEMORY  *next;   
}APM_PM_BLOCK_MEMORY;

typedef struct
{  
  uint16_t  gem_port;
  uint16_t  ref_num;
}APM_PM_MONITORED_GEM_PORT_T;

#endif
