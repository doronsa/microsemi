/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : APM                                                       **/
/**                                                                          **/
/**  FILE        : apm_pm_api.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file contains APM API definitions, prototypes        **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *    28March12     Ken  - initial version created.                            *
 *                                                                            *   
 *                                                                            *
 ******************************************************************************/


#ifndef __APM_PM_API__H
#define __APM_PM_API__H

#define APM_EXT_PM_BLOCK_SIZE   (16)

typedef struct {
    uint32_t pm_data[APM_EXT_PM_BLOCK_SIZE];
}APM_PM_DATA_T;

/*
typedef struct {
    uint8_t pm_admin[APM_EXT_PM_BLOCK_SIZE];
}PM_ADMIN_T;
*/

typedef struct {
    uint32_t pm_related_alarm_types[APM_EXT_PM_BLOCK_SIZE];
}APM_PM_RELATED_ALARM_TYPES_T;

extern bool apm_pm_get_data(uint32_t type, uint32_t param1, APM_PM_DATA_T *pmData);
extern bool apm_pm_create_entity(uint32_t type, uint32_t param1, uint32_t param2, uint16_t admin_bits, uint8_t accumulation_mode,uint32_t interval);
extern bool apm_pm_delete_entity(uint32_t type, uint32_t param1);
/*extern bool apm_pm_set_admin(uint32_t type, uint32_t param1, PM_ADMIN_T admin);*/
extern bool apm_pm_set_admin(uint32_t type, uint32_t param1, uint16_t admin_bits);
extern bool apm_pm_set_tca_threshold(uint32_t alarm_type, uint32_t param1, uint32_t threshold, uint32_t clear_threshold);
extern bool apm_pm_set_accumulation_mode(uint32_t pm_type, uint32_t param1, uint8_t accumulation_mode);
extern bool apm_pm_set_global_clear(uint32_t pm_type, uint32_t param1);
extern bool apm_pm_synchronize(void);
extern bool apm_pm_db_obj_reset(void);
extern bool apm_pm_set_interval(uint32_t type, uint32_t param1, uint32_t interval);
extern bool apm_pm_get_related_alarm_types(uint32_t pm_type, APM_PM_RELATED_ALARM_TYPES_T *pm_related_alarm_types);



#endif


