/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.                    
*                                                                                
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED  
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD  
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS  
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".      
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS      
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY    
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),      
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL     
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).                               
******************************************************************************** 
*      main.c                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:                                                      
*                                                                                
* DATE CREATED: July 12, 2010     
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.2 $                                                           
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "common.h"
#include "tpm_types.h"
#include "params.h"
#include "params_mng.h"
#include "globals.h"
#include "errorCode.h"
#include "OsGlueLayer.h"
#include "mipc.h"

extern const char * g_lbdtMipsName;
extern const char * i2c_mipc_name;

extern void lbdt_main(void);
extern void i2c_main(void);


/*******************************************************************************
* main
*
* DESCRIPTION:      loop detect main
*
* INPUTS:            none
*
* OUTPUTS:            none
*
* RETURNS:          none
*
*******************************************************************************/
int main(int argc, char* argv[])
{
    int32_t     rcode;
    pthread_t   lbdt_thread;
    pthread_t   i2c_thread;
    int         mipc_fd;
    
    mipc_fd = mipc_init("misc", 0, 0);
    if (MIPC_ERROR == mipc_fd)  
    {       
        return; 
    }  

    if(misc_init()  != US_RC_OK)   { printf("Misc init failed\n");  return (US_RC_FAIL); }


   if (osTaskCreate(&lbdt_thread, 
                     g_lbdtMipsName, 
                     (GLFUNCPTR) lbdt_main,
                     0,
                     0,
                     80,
                     0x2800) != IAMBA_OK)
    {
        printf("%s: osTaskCreate failed for lbdt_main task!\n\r", __FUNCTION__);
        return ERR_TASK_CREATE;
    }    


   while (0 != mipc_ping(g_lbdtMipsName, -1))
  {
       osTaskDelay(100);
  }

    if (osTaskCreate(&i2c_thread, 
                     "i2c_thread", 
                     (GLFUNCPTR) i2c_main,
                     0,
                     0,
                     80,
                     0x2800) != IAMBA_OK)
    {
        printf("%s: osTaskCreate failed for i2c_main task!\n\r", __FUNCTION__);
        return ERR_TASK_CREATE;
    }    

    
    while (0 != mipc_ping(i2c_mipc_name, -1))
    {
        osTaskDelay(100);
    }
    
    mthread_register_mq(mipc_fd, mipc_msg_default_handler);
    mthread_start();
    mipc_deinit();
    return(US_RC_OK);
}

