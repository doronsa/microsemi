/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : lbdtCliClient.c                                                 **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM CLI commands                            **/
/**                                                                          **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *      Initial version created.   12/Jan/2011          
 *                                                                              
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"
#include <math.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "globals.h"

BOOL  Lbdt_print_data_info (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    BOOL          rc;

    rc = lbdt_print_data_info(NULL);

    if(rc)
    {
        printf("\n execute successfully\n");
    }
    else
    {
        printf("\n execute unsuccessfully\n");
    }
    
    return rc;
}

BOOL  Lbdt_set_log_level (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    BOOL rc;
    UINT32 num = 0;
    UINT8 *stop;

    num = strtol(lub_argv__get_arg(argv, 0), &stop, 16);

    rc = lbdt_set_log_level(NULL,num);

     if(rc)
    {
        printf("\n execute successfully\n");
    }
    else
    {
        printf("\n execute unsuccessfully\n");
    }

    return rc;
}

BOOL  Lbdt_set_ethernet_type (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    BOOL rc;
    UINT32 ethType = 0;
    UINT8 *stop;
    
    ethType = strtol(lub_argv__get_arg(argv, 0), &stop, 16);

    rc = lbdt_set_ethernet_type(NULL, ethType);

     if(rc)
    {
        printf("\n execute successfully\n");
    }
    else
    {
        printf("\n execute unsuccessfully\n");
    }

    return rc;
}

BOOL  Lbdt_set_pkt_interval (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    UINT32 interval = 0;
    BOOL rc;
    UINT8 *stop;

    interval = strtol(lub_argv__get_arg(argv, 0), &stop, 10);

    rc = lbdt_set_pkt_interval(NULL , interval);

    if(rc)
    {
        printf("\n execute successfully\n");
    }
    else
    {
        printf("\n execute unsuccessfully\n");
    }

     
    return TRUE;
}

BOOL  Lbdt_set_recovery_uni_interval (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    UINT32 interval = 0;
    BOOL rc;
    UINT8 *stop;

    interval = strtol(lub_argv__get_arg(argv, 0), &stop, 10);

    rc =  lbdt_set_recovery_uni_interval(NULL, interval);
    if(rc)
    {
        printf("\n execute successfully\n");
    }
    else
    {
        printf("\n execute unsuccessfully\n");
    }
   
    return TRUE;
}

BOOL  Lbdt_set_lbdt_manage (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    UINT32 portId = 0;
    UINT32 loopswitch = 0;
    BOOL rc;
    UINT8 *stop;

    portId = strtol(lub_argv__get_arg(argv, 0), &stop, 10);
    loopswitch = strtol(lub_argv__get_arg(argv, 1), &stop, 10);

    rc = lbdt_set_lbdt_manage(NULL, portId, loopswitch);
    if(rc)
    {
        printf("\n execute successfully\n");
    }
    else
    {
        printf("\n execute unsuccessfully\n");
    }
    
    return TRUE;
}

BOOL  Lbdt_set_port_shut_down_option(const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    UINT32 portId = 0;
    UINT32 loopctrl = 0;
    BOOL rc;
    UINT8 *stop;

    portId = strtol(lub_argv__get_arg(argv, 0), &stop, 10);
    loopctrl = strtol(lub_argv__get_arg(argv, 1), &stop, 10);

    rc = lbdt_set_port_shut_down_option(NULL, portId, loopctrl);
    if(rc)
    {
        printf("\n execute successfully\n");
    }
    else
    {
        printf("\n execute unsuccessfully\n");
    }

     
    return TRUE;
}

BOOL  Lbdt_tag_del(const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    UINT32 portId = 0;
    BOOL rc;
    UINT8 *stop;

    portId = strtol(lub_argv__get_arg(argv, 0), &stop, 10);

    rc =  lbdt_tag_del(NULL, portId);
    if(rc)
    {
        printf("\n execute successfully\n");
    }
    else
    {
        printf("\n execute unsuccessfully\n");
    }

     
    return TRUE;
}

BOOL  Lbdt_tag_add(const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    UINT32 portId = 0;
    UINT32 svlan = 0;
    UINT32 cvlan = 0;
    UINT32 untagCtrl = 0;
    BOOL rc;
    UINT8 *stop;

    portId = strtol(lub_argv__get_arg(argv, 0), &stop, 10);
    svlan = strtol(lub_argv__get_arg(argv, 1), &stop, 10);
    cvlan = strtol(lub_argv__get_arg(argv, 2), &stop, 10);
    untagCtrl = strtol(lub_argv__get_arg(argv, 3), &stop, 10);

    rc = lbdt_tag_add(NULL, portId, svlan, cvlan, untagCtrl);
    if(rc)
    {
        printf("\n execute successfully\n");
    }
    else
    {
        printf("\n execute unsuccessfully\n");
    }
  
    return TRUE;
}

