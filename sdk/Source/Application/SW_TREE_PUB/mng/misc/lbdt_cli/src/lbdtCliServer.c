/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EOAM Stack                                                **/
/**                                                                          **/
/**  FILE        : lbdtCliServer.c                                                 **/
/**                                                                          **/
/**  DESCRIPTION : Definition of OAM CLI commands                            **/
/**                                                                          **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *      Initial version created.   12/Jan/2011          
 *                                                                              
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"
#include <math.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "globals.h"
#include "lbdt_main.h"

BOOL  lbdt_print_data_info (char* name)
{

    lbdt_printf_loop_back_data();
   
    return TRUE;
}

BOOL  lbdt_set_log_level (char* name, UINT32 level)
{

    lbdt_configure_log_level(level);
  
    return TRUE;
}

BOOL  lbdt_set_ethernet_type (char* name, UINT32 ethernet_type)
{
    UINT16 ethType =0;
    
    ethType = (UINT16)ethernet_type;
    lbdt_configure_ethernet_type(ethType);
    
    return TRUE;
}
  
BOOL  lbdt_set_pkt_interval (char* name, UINT32 interval)
{

    lbdt_packet_interval_set(interval);
     
    return TRUE;
}

BOOL  lbdt_set_recovery_uni_interval (char* name, UINT32 timeinterval)
{

    lbdt_recovery_interval_time_set(timeinterval);
     
    return TRUE;
}

BOOL  lbdt_set_lbdt_manage (char* name, UINT32 portId, UINT32 loopswitch)
{
    UINT16 lswitch = 0;
    
    lswitch = (UINT16)loopswitch;
    lbdt_port_manage_set(portId, lswitch);
     
    return TRUE;
}

BOOL  lbdt_set_port_shut_down_option (char* name, UINT32 portId, UINT32 loopctrl)
{
    UINT16 lctrl = 0;
    
    lctrl = (UINT16)loopctrl;
    lbdt_port_down_option_set(portId, lctrl);
     
    return TRUE;
}

BOOL  lbdt_tag_del (char* name, UINT32 portId)
{
    lbdt_port_tag_information_del(portId);
    
    return TRUE;
}

BOOL  lbdt_tag_add(char* name, UINT32 portId, UINT32 svlan, UINT32 cvlan, UINT32 untagctrl)
{
    LBDT_TAG_INFO_S taginfo;

    memset(&taginfo, 0, sizeof(taginfo));
    taginfo.cvlan = (UINT16)cvlan;
    taginfo.svlan = (UINT16)svlan;
    taginfo.untagInUse = untagctrl ;
    
    lbdt_port_tag_information_set(portId, &taginfo);
     
    return TRUE;
}

