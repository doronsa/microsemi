/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : UNI loop detect                                           **/
/**                                                                          **/
/**  FILE        : lbdt_main.c                                         **/
/**                                                                          **/
/**  DESCRIPTION : Definition of log module                                  **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Thomas.wang  - initial version created.   1/Dev/2012           
 *                                                                              
 ******************************************************************************/
 
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>

#include "oam_expo.h"
#include "OsGlueLayer.h"
#include "common.h"
#include "tpm_types.h"
#include "ezxml.h"
#include "lbdt_main.h"
#include "lbdt_log.h"
#include "lbdt_fmm.h"
#include "FmmUtils.h"
#include "mv_cust_api.h"
#include "midware_expo.h"
#include "mthread.h"

extern INT32 errno;

const CHAR *g_lbdtMipsName = "lbdt";

LBDT_INFO_S  g_InitialLoopDetectInfo;
LBDT_PARAMETER_S g_LoopDetectcCfg;
FMM_INFO_S g_LoopDetectFsm[LBDT_MAX_PORT_NUM]; 
LBDT_PACKET_VLAN_S g_lbdtSendMaxPacket[LBDT_MAX_PORT_NUM][LBDT_PACKET_MAX_NUM]; 
UINT8 g_MacNetCard[LBDT_ETH_ADDR_LEN];
UINT32 g_LoopDetectAlarm     = 0;
UINT32 g_ManualDisableIndex =0;
UINT32 g_UniPortNumber = 0;
BOOL g_ldbtVlanUpdate[LBDT_MAX_PORT_NUM];
static INT32                   g_SockFd = 0; 
static struct ifreq           g_ifReq;
static struct sockaddr_ll  g_SockAddress;
static UINT32 g_lbdtEthernetTypeRuleIndex[LBDT_ATTRIBUTE_TAG];
static UINT32 g_LoopDetectOwnerId = 0;
static UINT8 g_LoopDetectTxBuf[LBDT_PKT_LEN];
UINT32  g_lbdtSendPackeTimer = LBDT_FMM_DEFAULT_VALUE;
UINT32  g_lbdtStatPkt = 0;


void lbdt_mipc_server_handler(char* inMsg, unsigned int size);

/******************************************************************************
 *
 * Function   : lbdt_get_xml_value
 *
 * Description: This function reads in child and returns value of attrName as bool
 *
 * Parameters :
 *
 * Returns    : void
 *
 ******************************************************************************/

static UINT32 lbdt_get_xml_value(ezxml_t parentElement, UINT8 * childElmName, UINT8 * attrName, BOOL * boolvalue, UINT8 *buf,UINT16 *intvalue)
{
    ezxml_t     xmlElement;
    const CHAR  *attr_str;
    CHAR *stop;

    if(parentElement == NULL)
    {
        return LBDT_ERROR;
    }

    if ((xmlElement = ezxml_get(parentElement, childElmName, -1)) == 0)
    {
        printf("ezxml_get() of %s failed", childElmName);
        return LBDT_ERROR;
    }
    else
    {
        if (strcmp(childElmName, LBDT_ETHERNET_TYPE_DEFAULT) == 0)
        {   
            *intvalue = strtol(xmlElement->txt, &stop, 16);
        }
        else 
        if (strcmp(childElmName, LBDT_NETCARD_NAME) == 0)
        {
            memcpy(buf, xmlElement->txt, strlen(xmlElement->txt));
        }
        else 
        if (strcmp(childElmName, LBDT_WAIT_LOOP_PACKET_TIME_DEFAULT) == 0)
        {
            *intvalue = strtol(xmlElement->txt, &stop, 16);
        } 
        else 
        if ((attr_str = ezxml_attr(xmlElement, attrName)) != 0)
        {
            if (strcmp(attr_str, "true") == 0)
            {
                *boolvalue = TRUE;
            }
            else 
            if (strcmp(attr_str, "false") == 0)
            {
                *boolvalue = FALSE;
            }
            else 
            if(strcmp(attr_str, "true") == 0)
            {
                return LBDT_ERROR;
            }
        }
        else
        {
            return LBDT_ERROR;
        }
    }

    return LBDT_OK;
}

/******************************************************************************
 *
 * Function   : lbdt_port_data_get
 *              
 * Description: This function set port data structure
 *              
 * Parameters :  outPut:output data structure value
 *
 * Returns    : void
 *              
 ******************************************************************************/
void lbdt_port_data_get(LBDT_PARAMETER_S *outPut)
{
    memcpy(outPut, &g_LoopDetectcCfg, sizeof(LBDT_PARAMETER_S));
}

/******************************************************************************
 *
 * Function   : lbdt_port_data_set
 *              
 * Description: This function set port data structure
 *              
 * Parameters :  inPut:output data structure value
 *
 * Returns    : void
 *              
 ******************************************************************************/
void lbdt_port_data_set(LBDT_PARAMETER_S *inPut)
{
    memcpy(&g_LoopDetectcCfg, inPut, sizeof(LBDT_PARAMETER_S));
}


/*******************************************************************************
 *
 *  Function:    lbdt_save_alarm
 * 
 *  Description: set port alarm bit. 
 *
 * Parameters :  port:port number
 *                 
 *  Returns:     None
 *
 ******************************************************************************/
void lbdt_save_alarm(UINT32 port)
{
    UINT32 temp = 0;
    
    if (0 != (g_LoopDetectAlarm&(1 << (16+port))))
    {
        temp = ((~g_LoopDetectAlarm)|(1 << (16+port)));
        g_LoopDetectAlarm = ~temp;
        g_LoopDetectAlarm |= (1 << port);
    }
    else
    {
        g_LoopDetectAlarm |= (1 << port);
    }
 }

/*******************************************************************************
 *
 *  Function:    lbdt_clear_alarm
 * 
 *  Description: set port alarm bit. 
 *                 
 *  Returns:     None
 *
 ******************************************************************************/
void lbdt_clear_alarm(UINT32 port)
{
    UINT32 temp = 0;
    
    if (0 != (g_LoopDetectAlarm&(1 << port)))
    {
        temp = ((~g_LoopDetectAlarm)|(1 << port));
        g_LoopDetectAlarm = ~temp;
        g_LoopDetectAlarm |= (1 << (16+port));
    }
}


/*******************************************************************************
 *
 *  Function:    lbdt_vlan_information_update_control
 * 
 *  Description: The function set vlan updating information
 *                 
 *  Returns:     INT32
 *
 ******************************************************************************/
INT32 lbdt_vlan_information_update_control(UINT32 portId, BOOL vlanUpdate)
{
    if(portId >= LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG, "port=%d, exceed maximum value.\r\n", __FUNCTION__, portId);
        return LBDT_ERROR;
    }

    g_ldbtVlanUpdate[portId] = vlanUpdate; 

    return LBDT_OK;
}

/*******************************************************************************
 *
 *  Function:    lbdt_vlan_information_update_get
 * 
 *  Description: The function get vlan updating switch value 
 *                 
 *  Returns:     BOOL
 *
 ******************************************************************************/
BOOL  lbdt_vlan_information_update_get(UINT32 portId)
{
    if(portId >= LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG, "port=%d, exceed maximum value.\r\n", __FUNCTION__, portId);
        return FALSE;
    }

    return  g_ldbtVlanUpdate[portId]; 
}


/*******************************************************************************
 *
 *  Function:    lbdt_set_phy_admin
 * 
 *  Description: The function enable/disable UNI port 
 *                 
 *  Returns:     STATUS
 *
 ******************************************************************************/
STATUS lbdt_set_phy_admin(LBDT_CTRL_PORT_S *portInfo)
{
    ONU_STATUS   ret_code;
    UINT32       entry_bitmap = 0;  
    MIDWARE_TABLE_UNI_CFG_T    mid_table; 

    entry_bitmap |= MIDWARE_ENTRY_PORT_ADMIN;

    memset(&mid_table, 0, sizeof(mid_table));
    mid_table.port_id = portInfo->port_id;

    if(1 == portInfo->enable)
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG, "IN %s(), port[%d] PHY admin[Enable]\r\n", __FUNCTION__, mid_table.port_id);
        mid_table.port_admin = MIDWARE_UNI_CFG_ENABLE;
    }
    else
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG, "IN %s(), port[%d] PHY admin[Disable]\r\n", __FUNCTION__, mid_table.port_id);        
        mid_table.port_admin = MIDWARE_UNI_CFG_DISABLE;
    }

    ret_code = midware_update_entry(MIDWARE_TABLE_UNI_CFG, entry_bitmap, &mid_table, sizeof(mid_table));

    lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG, "EXIT %s(), ret_code[%d]\r\n", __FUNCTION__, ret_code);

    return ret_code; 
}

/*******************************************************************************
 *
 *  Function:    lbdt_down_time_out_set_phy_admin
 * 
 *  Description: The function trigger recovery uni time out FSM 
 *                 
 *  Returns:     INT32
 *
 ******************************************************************************/
INT32 lbdt_down_time_out_set_phy_admin(INT32 timerId,LBDT_CTRL_DATA_S *portInfo)
{
    LBDT_FSM_PARAMETER_S mfmmParamer;
    
    memset(&mfmmParamer, 0, sizeof(LBDT_FSM_PARAMETER_S));
    mfmmParamer.portid = portInfo->portIndex;

    if(mfmmParamer.portid >= LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: ERROR:line = %d", __FUNCTION__, __LINE__);
        return LBDT_ERROR;
    }

    mfmm_run(g_LoopDetectFsm, mfmmParamer.portid, LBDT_RECOVERY_UNI_TIME_OUT_EVENT, &mfmmParamer);

    return LBDT_OK;
}



/*******************************************************************************
 *
 *  Function:    lbdt_recovery_time_out_trigger_mfmm
 * 
 *  Description: The function transmit loop detection frames 
 *        
 * Parameters :  timerId:timer ID
 *                     portData: port information
 *
 *  Returns:     INT32
 *
 ******************************************************************************/
INT32 lbdt_recovery_time_out_trigger_mfmm(INT32 timerId, LBDT_CTRL_DATA_S *portData)
{
    LBDT_FSM_PARAMETER_S mfmmParamer;

    mfmmParamer.portid = portData->portIndex;

    if(mfmmParamer.portid >= LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: ERROR:line = %d", __FUNCTION__, __LINE__);
        return LBDT_ERROR;
    }

    mfmm_run(g_LoopDetectFsm, portData->portIndex, LBDT_RECOVERY_UNI_TIME_OUT_EVENT, &mfmmParamer);

    return LBDT_OK;
}

/*******************************************************************************
 *
 *  Function:    lbdt_initialized_control_info
 * 
 *  Description: The function get controling infoamtion from XML file 
 *     
 *
 * Parameters :  
 *
 *  Returns:     void
 *
 ******************************************************************************/
void lbdt_initialized_control_info()
{
    ezxml_t LoopBackHead;
    ezxml_t LoopBackChild;
    BOOL  boolValue;
    INT8 nodeName[10];
    UINT16 ethernetType = 0;
    UINT16 waitTime = 0;
    UINT32 i =0 ;

    memset(&g_InitialLoopDetectInfo, 0, sizeof(LBDT_INFO_S));

    LoopBackHead = ezxml_parse_file(LBDT_XML_FILE_PATH);

    if (LBDT_OK ==  lbdt_get_xml_value(LoopBackHead, LBDT_SUPPORT_SWITCH, LBDT_ATTRIBUTE_NAME, &boolValue, 0, 0))
    {
        g_InitialLoopDetectInfo.isLoopBackSupport = boolValue;    
    }
    else
    {
        g_InitialLoopDetectInfo.isLoopBackSupport = FALSE;
    }

    for(i=0; i<LBDT_MAX_PORT_NUM; i++)
    {
        memset(nodeName, 0, sizeof(nodeName));
        sprintf(nodeName, "UNI_%d", (i+1));
        if (LBDT_OK ==  lbdt_get_xml_value(LoopBackHead, nodeName, LBDT_ATTRIBUTE_NAME, &boolValue, 0, 0))
        {
            g_InitialLoopDetectInfo.portEnable[i] = boolValue;
        }
        else
        {
            g_InitialLoopDetectInfo.portEnable[i] = FALSE;
        }
    }
    
     if (LBDT_OK ==  lbdt_get_xml_value(LoopBackHead, LBDT_ETHERNET_TYPE_DEFAULT, 0,0, 0, &ethernetType))
    {
        g_InitialLoopDetectInfo.ethernetType = ethernetType;
    }
    else
    {
        g_InitialLoopDetectInfo.ethernetType = LBDT_ETHERNET_TYPE;
    }    
    
    memset(nodeName, 0, sizeof(nodeName));

     if (LBDT_OK ==  lbdt_get_xml_value(LoopBackHead, LBDT_NETCARD_NAME, 0,0, nodeName, 0))
    {
        memcpy(g_InitialLoopDetectInfo.netCarName, nodeName, strlen(nodeName));
    }
    else
    {
        memcpy(g_InitialLoopDetectInfo.netCarName, "eth0", strlen("eth0"));
    }    

     if (LBDT_OK ==  lbdt_get_xml_value(LoopBackHead, LBDT_WAIT_LOOP_PACKET_TIME_DEFAULT, 0,0, 0, &waitTime))
    {
        g_InitialLoopDetectInfo.waitLoopPacketTime = waitTime;
    }
    else
    {
        g_InitialLoopDetectInfo.waitLoopPacketTime = LBDT_WAIT_LOOP_PACKET_TIME;
    }    
}

/*******************************************************************************
 *
 *  Function:    LBDT_wait_loop_packet_time_out
 * 
 *  Description: The function transmit loop detection frames 
 *     
 *
 * Parameters :  timerId:timer ID
 *                     portInfo: port information
 *
 *  Returns:     INT32
 *
 ******************************************************************************/
INT32 lbdt_wait_loop_packet_time_out(INT32 timerId, LBDT_CTRL_DATA_S *portInfo)
{
    LBDT_FSM_PARAMETER_S mfmmParamer;

    mfmmParamer.portid = portInfo->portIndex;
    
    if(mfmmParamer.portid >= LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: ERROR:line = %d", __FUNCTION__, __LINE__);
        return LBDT_ERROR;
    }
    
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"%s: line = %d", __FUNCTION__, __LINE__);
    mfmm_run(g_LoopDetectFsm, portInfo->portIndex, LBDT_RECEIVE_LOOP_PACKET_TIME_OUT_EVENT, &mfmmParamer);

     return LBDT_OK;
}

  /******************************************************************************
 *
 * Function   : lbdt_compose_head
 *
 * Description: This function build packet head.
 *
 * Parameters :msg:packet
 *                    length:packet length
 *                    vlan_info:data info
 *                  
 *
 * Returns    : void
 *
 ******************************************************************************/
void lbdt_compose_head(UINT8 *msg, UINT32   length, LBDT_PACKET_VLAN_S *vlan_info)
{    
    LBDT_HDR_TX_S hdr;
    UINT32 i = 0;
    UINT16 tempvid = 0;
    UINT16 aSvlan = 0;
    UINT16 aCvlan = 0;
    UINT16 tagLength = 2;
    UINT8 macAddress[LBDT_ETH_ADDR_LEN];
      
    /*set destination address as broadcast address*/
    for(i=0; i<LBDT_ETH_ADDR_LEN; i++)
    {
        msg[i] = 0xff;
    }

    memcpy(macAddress, g_MacNetCard, LBDT_ETH_ADDR_LEN);
    macAddress[LBDT_ETH_ADDR_LEN-1] = macAddress[LBDT_ETH_ADDR_LEN-1]+vlan_info->port;
    /*set source address to MAC address of eth0*/
    memcpy(msg+LBDT_ETH_ADDR_LEN, macAddress, LBDT_ETH_ADDR_LEN);

    /*add s-VLAN tag*/
    if(0 != vlan_info->svlan)
    {
        aSvlan = 1;
        tempvid = (vlan_info->svlan & 0xFFF);
        *(UINT16*)(msg+2*LBDT_ETH_ADDR_LEN) = htons(LBDT_S_VLAN_TPID);
        *(UINT16*)(msg+2*LBDT_ETH_ADDR_LEN+LBDT_VLAN_TPID_LEN) = htons(tempvid);
    }

    if(0 != vlan_info->cvlan)
    {
        if(1 == aSvlan)
        {
            /*add c-VLAN and s-VLAN tag*/
            aCvlan = 1;
            tempvid = (vlan_info->cvlan & 0xFFF);
            *(UINT16*)(msg+2*LBDT_ETH_ADDR_LEN+LBDT_VLAN_TAG_LEN) = htons(LBDT_C_VLAN_TPID);
            *(UINT16*)(msg+2*LBDT_ETH_ADDR_LEN+LBDT_VLAN_TAG_LEN+LBDT_VLAN_TPID_LEN) = htons(tempvid);
        }
        else
        {
            /*add c-VLAN tag*/
            aCvlan = 1;
            tempvid = (vlan_info->cvlan & 0xFFF);
            *(UINT16*)(msg+2*LBDT_ETH_ADDR_LEN) = htons(LBDT_C_VLAN_TPID);
            *(UINT16*)(msg+2*LBDT_ETH_ADDR_LEN+LBDT_VLAN_TPID_LEN) = htons(tempvid);
        }
    }
    
    /*set loop detection ethernet type*/
    if((1 == aSvlan) && (1 == aCvlan))
    {
        *(UINT16*)(msg+2*LBDT_ETH_ADDR_LEN+2*LBDT_VLAN_TAG_LEN) =htons(vlan_info->ethernetType);
    }
    else
    {
        if((1 == aSvlan) || (1 == aCvlan))
        {
            *(UINT16*)(msg+2*LBDT_ETH_ADDR_LEN+LBDT_VLAN_TAG_LEN) =htons(vlan_info->ethernetType);
        }
        else
        {

            *(UINT16*)(msg+2*LBDT_ETH_ADDR_LEN) =htons(vlan_info->ethernetType);
        }
    }
    
}


  /******************************************************************************
 *
 * Function   : lbdt_tx_frame_cfg_operation
 *
 * Description: This function do sending loop packet.
 *
 * Parameters :loopBackData:data ininformation
 *                  
 *
 * Returns    : void
 *
 ******************************************************************************/
void lbdt_tx_frame_cfg_operation(LBDT_PACKET_VLAN_S *loopBackData)
{
    UINT32 ret = 0;
    UINT32 portIndex = 0;

    /*Compose loop detection frame*/
    memset(g_LoopDetectTxBuf, 0, sizeof(g_LoopDetectTxBuf));
        
    lbdt_compose_head(g_LoopDetectTxBuf, LBDT_PKT_LEN, loopBackData);

    lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG_PKT, "Tx loop detection frame, len:%d\r\n",LBDT_PKT_LEN);    
    lbdt_dbg_print_frame(LBDT_LOG_DEBUG_PKT, g_LoopDetectTxBuf, LBDT_PKT_LEN);        

    switch((loopBackData->port+1))
    {
        case LBDT_UNI_1:
            portIndex = (portIndex | (1<<(loopBackData->port)));
            ret =mv_cust_app_tx(MV_CUST_APP_TYPE_LPBK, MV_CUST_FLOW_DIR_DS, portIndex, g_LoopDetectTxBuf, LBDT_PKT_LEN);
            if (CUST_OK != ret)
            {
                lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, 
                "%s:%d, failed to mv_cust_app_tx, ret (%d)\n\r", __FILE__, __LINE__, ret);
            }
            break;
       case LBDT_UNI_2:
            portIndex = (portIndex | (1<<(loopBackData->port)));
            ret =mv_cust_app_tx(MV_CUST_APP_TYPE_LPBK, MV_CUST_FLOW_DIR_DS, portIndex, g_LoopDetectTxBuf, LBDT_PKT_LEN);
            if (CUST_OK != ret)
            {
                lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, 
                "%s:%d, failed to mv_cust_app_tx, ret (%d)\n\r", __FILE__, __LINE__, ret);
            }
            break;
       case LBDT_UNI_3:
            portIndex = (portIndex | (1<<(loopBackData->port)));
            ret =mv_cust_app_tx(MV_CUST_APP_TYPE_LPBK, MV_CUST_FLOW_DIR_DS, portIndex, g_LoopDetectTxBuf, LBDT_PKT_LEN);
            if (CUST_OK != ret)
            {
                lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, 
                "%s:%d, failed to mv_cust_app_tx, ret (%d)\n\r", __FILE__, __LINE__, ret);
            }
            break;
       case LBDT_UNI_4:
            portIndex = (portIndex | (1<<(loopBackData->port)));
            ret =mv_cust_app_tx(MV_CUST_APP_TYPE_LPBK, MV_CUST_FLOW_DIR_DS, portIndex, g_LoopDetectTxBuf, LBDT_PKT_LEN);
            if (CUST_OK != ret)
            {
                lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, 
                "%s:%d, failed to mv_cust_app_tx, ret (%d)\n\r", __FILE__, __LINE__, ret);
            }
            break;
       case LBDT_UNI_5:
            portIndex = (portIndex | (1<<(loopBackData->port)));
            ret =mv_cust_app_tx(MV_CUST_APP_TYPE_LPBK, MV_CUST_FLOW_DIR_DS, portIndex, g_LoopDetectTxBuf, LBDT_PKT_LEN);
            if (CUST_OK != ret)
            {
                lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, 
                "%s:%d, failed to mv_cust_app_tx, ret (%d)\n\r", __FILE__, __LINE__, ret);
            }
            break;
       case LBDT_UNI_6:
            portIndex = (portIndex | (1<<(loopBackData->port)));
            ret =mv_cust_app_tx(MV_CUST_APP_TYPE_LPBK, MV_CUST_FLOW_DIR_DS, portIndex, g_LoopDetectTxBuf, LBDT_PKT_LEN);
            if (CUST_OK != ret)
            {
                lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, 
                "%s:%d, failed to mv_cust_app_tx, ret (%d)\n\r", __FILE__, __LINE__, ret);
            }
            break;
       case LBDT_UNI_7:
            portIndex = (portIndex | (1<<(loopBackData->port)));
            ret =mv_cust_app_tx(MV_CUST_APP_TYPE_LPBK, MV_CUST_FLOW_DIR_DS, portIndex, g_LoopDetectTxBuf, LBDT_PKT_LEN);
            if (CUST_OK != ret)
            {
                lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, 
                "%s:%d, failed to mv_cust_app_tx, ret (%d)\n\r", __FILE__, __LINE__, ret);
            }
            break;
       default:
            lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, 
                                              "%s:%d, not support port index port=%d\n\r", __FILE__, __LINE__, loopBackData->port);
          break;
    }

}

 /******************************************************************************
 *
 * Function   : LBDT_rx_loop_frame
 *
 * Description: This function do receiving loop packet.
 *
 * Parameters :pkt:packet;
 *                  recvLength:packet long size
 *
 * Returns    : void
 *
 ******************************************************************************/
void lbdt_rx_loop_frame(UINT8* pkt,  UINT32 recvLength)
{
    UINT32 ifIndex = 0; 
    mv_cust_flow_dir_e pktDirection = 0;
    LBDT_FSM_PARAMETER_S mfmmParamer;

    if(pkt[1] == 0xff)
    {
        return ;
    }

    lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG_PKT, "Rx loop detection raw frame, len:%d\r\n",recvLength);  
    lbdt_dbg_print_frame(LBDT_LOG_DEBUG_PKT, pkt, recvLength);        

    mv_cust_app_rx_parse(pkt, &recvLength, &pktDirection, &ifIndex);
    
    lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG_PKT, "Rx loop detection frame after mv_cust, len:%d\r\n",recvLength);  
    lbdt_dbg_print_frame(LBDT_LOG_DEBUG_PKT, pkt, recvLength);        

    if(pkt[11] >= g_MacNetCard[5])
    {
        mfmmParamer.portid = pkt[11]-g_MacNetCard[5];
    }
    else
    {
        mfmmParamer.portid = 256-g_MacNetCard[5]+pkt[11];
    } 

   lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d, port=%d\n", __FUNCTION__, __LINE__, mfmmParamer.portid);

    if(mfmmParamer.portid >= LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: ERROR:line = %d", __FUNCTION__, __LINE__);
        return ;
    }

   mfmm_run(g_LoopDetectFsm, mfmmParamer.portid, LBDT_RECEIVE_LOOP_PACKET_EVENT, &mfmmParamer);

}     

/******************************************************************************
 *
 * Function   : lbdt_socket_initial
 *
 * Description: This function initialize socket.
 *
 * Parameters :
 *
 * Returns    : void
 *
 ******************************************************************************/
INT32 lbdt_socket_initial()
{    
    /* Non-blocking socket */
    INT32 onFlag = 1;
    INT32 sockFd = -1;
    
    /* Open Socket for protocol CTC_LBDT_ETHERNET_TYPE=0xFFFA */
    sockFd = socket(AF_PACKET, SOCK_RAW, htons(g_InitialLoopDetectInfo.ethernetType));
    if (sockFd < 0) 
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"%s: socket(AF_PACKET, SOCK_RAW, htons(0x%x)) failed. errno = %d, %s", 
                                             __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
        return LBDT_ERROR;
    }

    /* Bind Non-blocking socket */
    if (ioctl(sockFd, FIONBIO, &onFlag) < 0)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"%s: ioctl(g_SockFd, FIONBIO, &onFlag=1) failed. errno = %d, %s", 
                                             __FUNCTION__, errno, strerror(errno));
        return LBDT_ERROR;
    }

    /* Set interface for transmitting */
    strcpy(g_ifReq.ifr_name, g_InitialLoopDetectInfo.netCarName);
    if (ioctl(sockFd, SIOCGIFINDEX, &g_ifReq) < 0) 
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR,"%s: ioctl(sockfd,SIOCGIFINDEX,&ifReq) failed. errno = %d, %s,device:%s", 
                                             __FUNCTION__, errno, strerror(errno), g_InitialLoopDetectInfo.netCarName);
        return LBDT_ERROR;
    }

    /* Set transmitting parameters */
    memset(&g_SockAddress, 0, sizeof(g_SockAddress));
    g_SockAddress.sll_ifindex  = g_ifReq.ifr_ifindex;
    g_SockAddress.sll_family   = AF_PACKET;
    g_SockAddress.sll_halen    = 6;
    g_SockAddress.sll_protocol = htons(LBDT_ETHERNET_TYPE);

    return sockFd ;

}

/******************************************************************************
 *
 * Function   : lbdt_set_default
 *
 * Description: This function set loop detect default value.
 *
 * Parameters :
 *
 * Returns    : void
 *
 ******************************************************************************/
void lbdt_set_default()
{
    UINT32 i =0;

    memset(&g_LoopDetectcCfg, 0, sizeof(g_LoopDetectcCfg));
    memset(&g_lbdtEthernetTypeRuleIndex, 0, sizeof(g_lbdtEthernetTypeRuleIndex));
    
    for(i=0; i<LBDT_MAX_PORT_NUM; i++)
    {
        g_LoopDetectcCfg.portparameter[i].management = 0; 
        g_LoopDetectcCfg.portparameter[i].portDownOption = 0x0; 
        g_LoopDetectcCfg.portparameter[i].untagInUse = TRUE;

        if(TRUE == g_InitialLoopDetectInfo.portEnable[i])
        {
            g_UniPortNumber++;
        }
    }
    
    g_LoopDetectcCfg.pktInterval = 2000; 
    g_LoopDetectcCfg.recoveryIntervalTime = 300;
    g_LoopDetectcCfg.ethernetType = g_InitialLoopDetectInfo.ethernetType;     
}

/******************************************************************************
 *
 * Function   : lbdt_ethernet_type_modify
 *
 * Description: This function modify loop detect ethernet type.
 *
 * Parameters :
 *
 * Returns    : void
 *
 ******************************************************************************/
INT32 lbdt_ethernet_type_modify(UINT16 ethType)
{
    MRVL_ERROR_CODE_T retCode = MRVL_EXIT_OK;

    retCode = tpm_loop_detect_del_channel(0);  
    if(retCode != MRVL_EXIT_OK)
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, "delete ethernet fail, tmprc:%d\r\n",retCode);    
        return LBDT_ERROR;
    }  

    retCode = tpm_loop_detect_add_channel(0, ethType);
    if(retCode != MRVL_EXIT_OK)
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, "add ethernet fail, tmprc:%d\r\n",retCode);    
        return LBDT_ERROR;
    }   

    g_LoopDetectcCfg.ethernetType = ethType;

    return LBDT_OK;

#if 0
    UINT32 ruleIndex1 = 0;
    UINT32 ruleIndex2 = 0;
    UINT32 ruleIndex3 = 0;
    UINT32 i = 0;
    tpm_rule_action_t lpRuleAction;
    tpm_parse_fields_t lpParseRule = 0;
    tpm_parse_flags_t lpParseFlags = 0;
    tpm_l2_acl_key_t lpl2Key;
    tpm_pkt_frwd_t lpPktFrwd;
    MRVL_ERROR_CODE_T retCode = MRVL_EXIT_OK;
    
    tpm_loop_detect_del_channel(0);
    
    tpm_loop_detect_add_channel(0, ethType);

    


    for(i=LBDT_UNTAG; i<=LBDT_DOUBLE_TAG; i++ )
    {
        retCode = tpm_del_l2_rule(g_LoopDetectOwnerId, g_lbdtEthernetTypeRuleIndex[i]);  
        if(retCode != MRVL_EXIT_OK)
        {
            lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, "delete ethernet fail, tmprc:%d\r\n",retCode);    
            return LBDT_ERROR;
        }   
    }
    
    memset(&lpl2Key,      0, sizeof(lpl2Key));
    memset(&lpPktFrwd,    0, sizeof(lpPktFrwd));   
    memset(&lpRuleAction, 0, sizeof(lpRuleAction));
    
    lpParseRule           = TPM_L2_PARSE_ETYPE;
    lpParseFlags          = 0;
    lpl2Key.ether_type   = g_InitialLoopDetectInfo.ethernetType;
    lpRuleAction.next_phase  = STAGE_DONE;
    lpRuleAction.pkt_act     = TPM_ACTION_CUST_CPU_PKT_PARSE;

    retCode = tpm_add_l2_rule(g_LoopDetectOwnerId, TPM_SRC_PORT_UNI_ANY, 0, &ruleIndex1, lpParseRule, lpParseFlags,
                                               &lpl2Key, &lpPktFrwd, 0, 0, &lpRuleAction);
    if(retCode != TPM_RC_OK)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR, "%s:Line[%d] Failed to call tpm_add_ethernet_type_rule, tpm_rc[%d]\n\r",
                               __FUNCTION__, __LINE__, retCode);  
        return ;
    }
    g_lbdtEthernetTypeRuleIndex[LBDT_UNTAG] = ruleIndex1;

    lpParseRule           = TPM_L2_PARSE_ONE_VLAN_TAG|TPM_L2_PARSE_ETYPE;
    lpParseFlags = TPM_PARSE_FLAG_TAG1_TRUE;
    retCode = tpm_add_l2_rule(g_LoopDetectOwnerId, TPM_SRC_PORT_UNI_ANY, 0, &ruleIndex2, lpParseRule, lpParseFlags,
                                               &lpl2Key, &lpPktFrwd, NULL, 0, &lpRuleAction);
    if(retCode != TPM_RC_OK)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR, "%s:Line[%d] Failed to call tpm_add_ethernet_type_rule, tpm_rc[%d]\n\r",
                               __FUNCTION__, __LINE__, retCode);  
        return ;
    }
    g_lbdtEthernetTypeRuleIndex[LBDT_SINGLE_TAG] = ruleIndex2;

    lpParseRule           = TPM_L2_PARSE_TWO_VLAN_TAG|TPM_L2_PARSE_ETYPE;
    lpParseFlags = TPM_PARSE_FLAG_TAG2_TRUE;
    retCode = tpm_add_l2_rule(g_LoopDetectOwnerId, TPM_SRC_PORT_UNI_ANY, 0, &ruleIndex3, lpParseRule, lpParseFlags,
                                               &lpl2Key, &lpPktFrwd, 0, 0, &lpRuleAction);
    if(retCode != TPM_RC_OK)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR, "%s:Line[%d] Failed to call tpm_add_ethernet_type_rule, tpm_rc[%d]\n\r",
                               __FUNCTION__, __LINE__, retCode);  
        return ;
    }
    
    g_lbdtEthernetTypeRuleIndex[LBDT_DOUBLE_TAG] = ruleIndex3;
      
    g_LoopDetectcCfg.ethernetType = ethType;

    mv_cust_app_etype_set(MV_CUST_APP_TYPE_LPBK, ethType);
#endif
}

/******************************************************************************
 *
 * Function   : lbdt_ethernet_type_init
 *
 * Description: This function initialize loop detect ethernet type.
 *
 * Parameters :
 *
 * Returns    : INT32
 *
 ******************************************************************************/

INT32 lbdt_ethernet_type_init()
{
    MRVL_ERROR_CODE_T retCode = MRVL_EXIT_OK;
    
    retCode = tpm_loop_detect_add_channel(0, g_InitialLoopDetectInfo.ethernetType);
    if(retCode != MRVL_EXIT_OK)
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_ERROR, "add ethernet fail, tmprc:%d\r\n",retCode);    
        return LBDT_ERROR;
    }   

    return LBDT_OK;
#if 0
    tpm_rule_action_t   lpRuleAction;
    tpm_parse_fields_t  lpParseRule = 0;
    tpm_parse_flags_t    lpParseFlags = 0;
    tpm_l2_acl_key_t    lpl2Key;
    tpm_pkt_frwd_t       lpPktFrwd;
    UINT32 ruleIndex1 = 0; 
    UINT32 ruleIndex2 = 0; 
    UINT32 ruleIndex3 = 0; 
    MRVL_ERROR_CODE_T retCode = MRVL_EXIT_OK;

    memset(&lpl2Key, 0, sizeof(lpl2Key));
    
    lpParseRule           = TPM_L2_PARSE_ETYPE;
    lpParseFlags          = 0;
    lpl2Key.ether_type   = g_InitialLoopDetectInfo.ethernetType;
    lpRuleAction.next_phase  = STAGE_DONE;
    lpRuleAction.pkt_act     = TPM_ACTION_CUST_CPU_PKT_PARSE;


    retCode = tpm_add_l2_rule(g_LoopDetectOwnerId, TPM_SRC_PORT_UNI_ANY, 0, &ruleIndex1, lpParseRule, lpParseFlags,
                                               &lpl2Key, &lpPktFrwd, 0, 0, &lpRuleAction);
    if(retCode != TPM_RC_OK)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR, "%s:Line[%d] Failed to call tpm_add_ethernet_type_rule, tpm_rc[%d]\n\r",
                               __FUNCTION__, __LINE__, retCode);  
        return ;
    }
    g_lbdtEthernetTypeRuleIndex[LBDT_UNTAG] = ruleIndex1;

    lpParseRule           = TPM_L2_PARSE_ONE_VLAN_TAG|TPM_L2_PARSE_ETYPE;
    lpParseFlags = TPM_PARSE_FLAG_TAG1_TRUE;
    retCode = tpm_add_l2_rule(g_LoopDetectOwnerId, TPM_SRC_PORT_UNI_ANY, 0, &ruleIndex2, lpParseRule, lpParseFlags,
                                               &lpl2Key, &lpPktFrwd, NULL, 0, &lpRuleAction);
    if(retCode != TPM_RC_OK)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR, "%s:Line[%d] Failed to call tpm_add_ethernet_type_rule, tpm_rc[%d]\n\r",
                               __FUNCTION__, __LINE__, retCode);  
        return ;
    }
    g_lbdtEthernetTypeRuleIndex[LBDT_SINGLE_TAG] = ruleIndex2;

    lpParseRule           = TPM_L2_PARSE_TWO_VLAN_TAG|TPM_L2_PARSE_ETYPE;
    lpParseFlags = TPM_PARSE_FLAG_TAG2_TRUE;
    retCode = tpm_add_l2_rule(g_LoopDetectOwnerId, TPM_SRC_PORT_UNI_ANY, 0, &ruleIndex3, lpParseRule, lpParseFlags,
                                               &lpl2Key, &lpPktFrwd, 0, 0, &lpRuleAction);
    if(retCode != TPM_RC_OK)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR, "%s:Line[%d] Failed to call tpm_add_ethernet_type_rule, tpm_rc[%d]\n\r",
                               __FUNCTION__, __LINE__, retCode);  
        return ;
    }
    
    g_lbdtEthernetTypeRuleIndex[LBDT_DOUBLE_TAG] = ruleIndex3;
#endif
    return ;
}

/******************************************************************************
 *
 * Function   : lbdt_loop_send_pkt_clear
 *
 * Description: This function clear packet 
 *
 * Parameters :
 *
 * Returns    : INT32
 *
 ******************************************************************************/
INT32 lbdt_loop_send_pkt_clear(UINT32 portIndex)
{

    if(portIndex >= LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: ERROR:line = %d", __FUNCTION__, __LINE__);
        return LBDT_ERROR;
    }
    
    memset(g_lbdtSendMaxPacket[portIndex], 0, sizeof(LBDT_PACKET_VLAN_S)*LBDT_PACKET_MAX_NUM);

     return LBDT_OK; 
}

/******************************************************************************
 *
 * Function   : lbdt_loop_send_pkt_save
 *
 * Description: This function save packet 
 *
 * Parameters :
 *
 * Returns    : INT32
 *
 ******************************************************************************/
INT32 lbdt_loop_send_pkt_save(UINT32 portIndex)
{
    LBDT_PARAMETER_S portData;
    LBDT_PACKET_VLAN_S pktModel;
    UINT32 pktIndex = 0;
    UINT32 tagIndex = 0;
    UINT32 Index = 0; 

    if(portIndex >= LBDT_MAX_PORT_NUM)
    {
        return LBDT_ERROR;
    }

    memset(&portData, 0, sizeof(LBDT_PARAMETER_S));
    memset(&pktModel, 0, sizeof(LBDT_PACKET_VLAN_S));

    lbdt_port_data_get(&portData);
    memset(g_lbdtSendMaxPacket[portIndex], 0, sizeof(LBDT_PACKET_VLAN_S)*LBDT_PACKET_MAX_NUM);
     if(TRUE == portData.portparameter[portIndex].untagInUse)
    {
        memset(&pktModel, 0, sizeof(LBDT_PACKET_VLAN_S));
        pktModel.port = portIndex;
        pktModel.ethernetType = portData.ethernetType;
        pktModel.opt = TRUE;
        for(tagIndex = 0; tagIndex < LBDT_PACKET_MAX_NUM; tagIndex++)
        {
            if(FALSE == g_lbdtSendMaxPacket[portIndex][tagIndex].opt)
            {
                memcpy(&(g_lbdtSendMaxPacket[portIndex][tagIndex]), &pktModel, sizeof(LBDT_PACKET_VLAN_S));
                break;
            }
        }
    }

    for(Index = 0; Index < LBDT_VLAN_LIST; Index++)
    {
        if(TRUE == portData.portparameter[portIndex].vlanListInuse[Index])
        {
            memset(&pktModel, 0, sizeof(LBDT_PACKET_VLAN_S));
            pktModel.port = portIndex;
            pktModel.ethernetType = portData.ethernetType;
            pktModel.cvlan = portData.portparameter[portIndex].vlan_list[Index].cvlan;
            pktModel.svlan = portData.portparameter[portIndex].vlan_list[Index].svlan;
            pktModel.opt = TRUE;
            for(tagIndex = 0; tagIndex < LBDT_PACKET_MAX_NUM; tagIndex++)
            {
                if(FALSE == g_lbdtSendMaxPacket[portIndex][tagIndex].opt)
                {
                    memcpy(&(g_lbdtSendMaxPacket[portIndex][tagIndex]), &pktModel, sizeof(LBDT_PACKET_VLAN_S));
                    break;
                }
            }
        }
    }

    return LBDT_OK; 
}

/******************************************************************************
 *
 * Function   : lbdt_loop_send_pkt_reset_fmm
 *
 * Description: This function reset packet operation.
 *
 * Parameters :pktCounter: all packet counter
 *
 * Returns    : INT32
 *
 ******************************************************************************/

INT32 lbdt_loop_send_pkt_reset_fmm( )
{
    UINT32 portIndex = 0;
    UINT32 tagIndex = 0;

        for(portIndex=0; portIndex< LBDT_MAX_PORT_NUM; portIndex++)
        {       
            for(tagIndex = 0; tagIndex < LBDT_PACKET_MAX_NUM; tagIndex++)
            {
                if(0 != g_lbdtSendMaxPacket[portIndex][tagIndex].ethernetType)
                {
                    g_lbdtSendMaxPacket[portIndex][tagIndex].opt = TRUE;
                }
            }
        }
        
        g_lbdtStatPkt = 0;

     return LBDT_OK;

}



/******************************************************************************
 *
 * Function   : lbdt_loop_send_pkt_reset
 *
 * Description: This function reset packet operation.
 *
 * Parameters :pktCounter: all packet counter
 *
 * Returns    : void
 *
 ******************************************************************************/

INT32 lbdt_loop_send_pkt_reset(UINT32 pktCounter)
{
    UINT32 portIndex = 0;
    UINT32 tagIndex = 0;

    if((pktCounter == g_lbdtStatPkt) && (pktCounter > 0))
    {
        for(portIndex=0; portIndex< LBDT_MAX_PORT_NUM; portIndex++)
        {       
            for(tagIndex = 0; tagIndex < LBDT_PACKET_MAX_NUM; tagIndex++)
            {
                if(0 != g_lbdtSendMaxPacket[portIndex][tagIndex].ethernetType)
                {
                    g_lbdtSendMaxPacket[portIndex][tagIndex].opt = TRUE;
                }
            }
        }
        
        g_lbdtStatPkt = 0;
    }  

     return LBDT_OK;

}

/******************************************************************************
 *
 * Function   : lbdt_loop_send_pkt
 *
 * Description: This function do sending packet operation.
 *
 * Parameters :timerId: timer NO. temp: parameter is not use for mthread.
 *
 * Returns    : void
 *
 ******************************************************************************/

INT32 lbdt_loop_send_pkt(INT32 timerId, UINT32 *temp)
{
    UINT32 portIndex = 0;
    UINT32 portIndex1 = 0;
    UINT32 tagIndex = 0;
    UINT32 tagIndex1 = 0;
    LBDT_PARAMETER_S portData;
    LBDT_PACKET_VLAN_S pktModel;
    UINT32 pktCounter = 0;
    UINT32 pktIndex1 = 0;
    BOOL portSwitch = FALSE;

    memset(&portData, 0, sizeof(LBDT_PARAMETER_S));
    memset(&pktModel, 0, sizeof(LBDT_PACKET_VLAN_S));
    
    lbdt_port_data_get(&portData);
    
    for(portIndex=0; portIndex< LBDT_MAX_PORT_NUM; portIndex++)
    {
        if(TRUE == lbdt_vlan_information_update_get(portIndex))
        {
            lbdt_loop_send_pkt_save(portIndex);
            lbdt_vlan_information_update_control(portIndex, FALSE);
        }
    }

    for(portIndex=0; portIndex< LBDT_MAX_PORT_NUM; portIndex++)
    {
        for(tagIndex = 0; tagIndex < LBDT_PACKET_MAX_NUM; tagIndex++)
        {
            if(0 != g_lbdtSendMaxPacket[portIndex][tagIndex].ethernetType)
            {
                pktCounter++;
            }
        }
    }
        
    for(portIndex1=0; portIndex1< LBDT_MAX_PORT_NUM; portIndex1++)
    {
        
        if ((g_InitialLoopDetectInfo.portEnable[portIndex1] == TRUE) && ( g_InitialLoopDetectInfo.isLoopBackSupport == TRUE))
        {
            for(pktIndex1 = 0; pktIndex1 < LBDT_PACKET_MAX_NUM; pktIndex1++)
            {
                if(TRUE == g_lbdtSendMaxPacket[portIndex1][pktIndex1].opt)
                {
                    lbdt_tx_frame_cfg_operation(&g_lbdtSendMaxPacket[portIndex1][pktIndex1]);
                    g_lbdtSendMaxPacket[portIndex1][pktIndex1].opt = FALSE;
                    portSwitch = TRUE;
                    g_lbdtStatPkt ++;
                    break;
                }
            }
        }
        
        if(TRUE == portSwitch)
        {
            break;
        }
    }

    lbdt_loop_send_pkt_reset(pktCounter);
    
    return LBDT_OK; 
 
}

/******************************************************************************
 *
 * Function   : lbdt_loop_send_packet_reset_timer
 *
 * Description: This function reset send packet timer.
 *
 * Parameters :
 *
 * Returns    : void
 *
 ******************************************************************************/
void lbdt_loop_send_packet_reset_timer( )
{
    LBDT_PARAMETER_S portInfo;

    memset(&portInfo,0, sizeof(LBDT_PARAMETER_S));

    lbdt_port_data_get(&portInfo);

    if(LBDT_FMM_DEFAULT_VALUE != g_lbdtSendPackeTimer)
    {
        if ( 0 != mthread_deregister_timer(g_lbdtSendPackeTimer))
        {
            lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset send packet timer failed. errno = %d, %s", 
                                     __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                

        }
    }

    g_lbdtSendPackeTimer = 
    mthread_register_timer(portInfo.pktInterval, TRUE, NULL, (MTHREAD_TIMER_FN_T)lbdt_loop_send_pkt);
    
}


/******************************************************************************
 *
 * Function   : lbdt_loop_pkt_init
 *
 * Description: This function initialize the loop detect packet
 *
 * Parameters :
 *
 * Returns    : void
 *
 ******************************************************************************/

void lbdt_loop_pkt_init( )
{

    LBDT_PARAMETER_S portData;

    memset(g_ldbtVlanUpdate,0, sizeof(g_ldbtVlanUpdate));
    memset(&portData,0, sizeof(LBDT_PARAMETER_S));
    memset(g_lbdtSendMaxPacket,0, sizeof(LBDT_PACKET_VLAN_S)*LBDT_MAX_PORT_NUM*LBDT_PACKET_MAX_NUM);

    lbdt_port_data_get(&portData);

    if(LBDT_FMM_DEFAULT_VALUE == g_lbdtSendPackeTimer)
    {
        g_lbdtSendPackeTimer = 
        mthread_register_timer(portData.pktInterval, TRUE, NULL, (MTHREAD_TIMER_FN_T)lbdt_loop_send_pkt);
    }
    else
    {
        if(0 != mthread_reset_timer(g_lbdtSendPackeTimer))
        {
            lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                     __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
        }
    }
  
    return ;
}


/******************************************************************************
 *
 * Function   : lbdt_initial
 *
 * Description: This function initialize the loop detect data
 *
 * Parameters :
 *
 * Returns    : void
 *
 ******************************************************************************/

void lbdt_initial( )
{
    /* Check whether is initialized*/
    lbdt_check_if_initialized();
    
    /*Init loop detect log*/
    lbdt_log_init();

    /*check loop back modify configuration*/
    lbdt_initialized_control_info();

    /*LBDT_set_default*/
    lbdt_set_default();

    /*Init loop back mfmm*/
    lbdt_mfmm_init();

    /*Get GMAC0 MAC address*/
    MvExtOamGetEth0Mac(g_MacNetCard);

    /*loop back set ethernet type*/
    lbdt_ethernet_type_init();
   
   /*loop detect initial*/
    lbdt_loop_pkt_init(); 
}

/******************************************************************************
 *
 * Function   : LBDT_main
 *
 * Description: This function initialize the loop detect data,regieter message queue and socket.
 *
 * Parameters :
 *
 * Returns    : void
 *
 ******************************************************************************/

void lbdt_main()
{
    INT32 asnyDataFd = 0;

    asnyDataFd = mipc_init(g_lbdtMipsName, 0, 0);
    if (LBDT_ERROR == asnyDataFd)  
    {       
        return; 
    }  

    lbdt_initial();

    mthread_register_mq(asnyDataFd, lbdt_mipc_server_handler);
    
    if(g_InitialLoopDetectInfo.isLoopBackSupport != FALSE)
    {
        if( (g_SockFd = lbdt_socket_initial()) != LBDT_ERROR)
        {
            mthread_register_sk(g_SockFd, lbdt_rx_loop_frame);
        }
    }
    
    mthread_start(); 

}

/******************************************************************************
 *
 * Function   : LBDT_get_xml_value
 *
 * Description: This function reads in child and returns value of attrName as bool
 *
 * Parameters :
 *
 * Returns    : void
 *
 ******************************************************************************/

void lbdt_mfmm_show(void)
{
    mfmm_show(g_LoopDetectFsm, g_UniPortNumber);
}

