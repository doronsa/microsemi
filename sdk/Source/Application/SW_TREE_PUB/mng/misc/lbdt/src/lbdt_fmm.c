/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : UNI loop detection                                        **/
/**                                                                          **/
/**  FILE        : lbdt_mfmm.c                                            **/
/**                                                                          **/
/**  DESCRIPTION : Definition of log module                                  **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Thomas.wang  - initial version created.   1/Dev./2012           
 *                                                                              
 ******************************************************************************/
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "oam_expo.h"
#include "OsGlueLayer.h"
#include "common.h"
#include "tpm_types.h"
#include "mthread.h"
#include "lbdt_fmm.h"
#include "lbdt_main.h"
#include "lbdt_log.h"
#include "FmmUtils.h"


extern UINT32 g_ManualDisableIndex;
extern LBDT_INFO_S  g_InitialLoopDetectInfo;
extern INT32 g_UniPortNumber;
extern FMM_INFO_S g_LoopDetectFsm[LBDT_MAX_PORT_NUM];
extern UINT32  g_lbdtSendPackeTimer;

UINT32  g_LoopDetectWaitLoopPackeTimer[LBDT_MAX_PORT_NUM];
UINT32  g_LoopDetectRecoverUniTimer[LBDT_MAX_PORT_NUM];

LBDT_CTRL_DATA_S g_WaitLoopPacketTimer[LBDT_MAX_PORT_NUM];  
LBDT_CTRL_DATA_S g_ReceiceLoopPkt[LBDT_MAX_PORT_NUM];
LBDT_CTRL_DATA_S g_WosSendPkTimeOut[LBDT_MAX_PORT_NUM];
LBDT_CTRL_DATA_S g_LoopShutDown[LBDT_MAX_PORT_NUM];
LBDT_CTRL_DATA_S g_LoopPktTimeOut[LBDT_MAX_PORT_NUM];
LBDT_CTRL_DATA_S g_LoopRecoverUniTimeOut[LBDT_MAX_PORT_NUM];
LBDT_CTRL_DATA_S g_LoopSendPktTimeOut[LBDT_MAX_PORT_NUM];
LBDT_CTRL_DATA_S g_DownWaitPktTimeOut[LBDT_MAX_PORT_NUM];
LBDT_CTRL_DATA_S g_DownLoopRecoverUniTimeOut[LBDT_MAX_PORT_NUM];
LBDT_CTRL_DATA_S g_WosLoopPkTimeOut[LBDT_MAX_PORT_NUM];

/******************************************************************************
 * Function   : lbdt_mfmm_disable_state_disable_event
 *              
 * Description: This function process the disable event in the disable state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_disable_state_disable_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);
    
    return LBDT_DISABLE_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_disable_state_enable_event
 *              
 * Description: This function process the enable event in the disable state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_disable_state_enable_event(LBDT_FSM_PARAMETER_S *mfmmParam )
{  
    if(mfmmParam->portid >=LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: port index exceed max value!. errno = %d, %s", 
                                           __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno)); 
        return LBDT_DISABLE_STATE;
    }

    lbdt_port_data_get(&(g_WaitLoopPacketTimer[mfmmParam->portid].ethPortData));
    
    g_WaitLoopPacketTimer[mfmmParam->portid].portIndex = mfmmParam->portid;

    if(LBDT_FMM_DEFAULT_VALUE == g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid])
    {
        g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid] = 
        mthread_register_timer(g_InitialLoopDetectInfo.waitLoopPacketTime*1000, TRUE, &(g_WaitLoopPacketTimer[mfmmParam->portid]), (MTHREAD_TIMER_FN_T)lbdt_wait_loop_packet_time_out);
    }
   else
    {
        if(0 != mthread_reset_timer(g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid]))
        {
            lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                                 __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
        }
    }
    
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);                

    return LBDT_UP_WITHOUT_LOOP_STATE;
}

/******************************************************************************
 * Function   : lbdt_mfmm_disable_state_shut_down_event
 *              
 * Description: This function process shutting down event in the disable state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_disable_state_shut_down_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);
    
    return LBDT_DISABLE_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_disable_state_no_shut_down_event
 *              
 * Description: This function process not shutting down event in the disable state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_disable_state_no_shut_down_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);
    
    return LBDT_DISABLE_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_disable_state_recovery_time_modify_event
 *              
 * Description: This function process modifying recovery time event in the disable state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_disable_state_recovery_time_modify_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);
    
    return LBDT_DISABLE_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_disable_state_receive_loop_packet_event
 *              
 * Description: This function process receiving loop packet event in the disable state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_disable_state_receive_loop_packet_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);
    
    return LBDT_DISABLE_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_disable_state_wait_loop_packet_time_out_event
 *              
 * Description: This function process waiting for loop packet event in the disable state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_disable_state_receive_loop_packet_time_out_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);
    
    return LBDT_DISABLE_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_disable_state_recovery_uni_time_out_event
 *              
 * Description: This function process recovery uni port time out event in the disable state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_disable_state_recovery_uni_time_out_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);
    
    return LBDT_DISABLE_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_without_loop_state_disable_event
 *              
 * Description: This function process the disable event in no loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_without_loop_state_disable_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_DISABLE_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_without_loop_state_enable_event
 *              
 * Description: This function enable loop event in no loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_without_loop_state_enable_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
     lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_UP_WITHOUT_LOOP_STATE;
}

/******************************************************************************
 * Function   : lbdt_mfmm_without_loop_state_shut_down_event
 *              
 * Description: This function process shutting down event in no loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_without_loop_state_shut_down_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_UP_WITHOUT_LOOP_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_without_loop_state_no_shut_down_event
 *              
 * Description: This function process no shutting down event in no loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_without_loop_state_no_shut_down_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_UP_WITHOUT_LOOP_STATE; 
}


/******************************************************************************
 * Function   : lbdt_mfmm_without_loop_state_recovery_time_modify_event
 *              
 * Description: This function process modifying recovery time in no loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_without_loop_state_recovery_time_modify_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_UP_WITHOUT_LOOP_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_without_loop_state_receive_loop_packet_event
 *              
 * Description: This function process receiving loop packet event in no loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_without_loop_state_receive_loop_packet_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    LBDT_PARAMETER_S ethPortData;
    LBDT_CTRL_PORT_S portCtrl;

    if(mfmmParam->portid >=LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: port index exceed max value!. errno = %d, %s", 
                                           __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno)); 
        return LBDT_UP_WITHOUT_LOOP_STATE; 
    }

    memset(&(g_ReceiceLoopPkt[mfmmParam->portid]), 0, sizeof(LBDT_CTRL_DATA_S));  
    memset(&ethPortData, 0, sizeof(LBDT_PARAMETER_S)); 
    memset(&portCtrl, 0, sizeof(LBDT_CTRL_PORT_S)); 
    
    lbdt_port_data_get(&ethPortData);
    g_ReceiceLoopPkt[mfmmParam->portid].portIndex = mfmmParam->portid;

    if (LBDT_SHUT_DOWN_PORT_OPER != ethPortData.portparameter[mfmmParam->portid].portDownOption)
    {
        /*Record loop detect alarm*/
        lbdt_save_alarm(mfmmParam->portid);
        lbdt_loop_send_pkt_clear(mfmmParam->portid);
        lbdt_loop_send_pkt_reset_fmm();

       if(0 != mthread_reset_timer(g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid]))
        {
            lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                                 __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
        }

        return LBDT_UP_WITH_LOOP_STATE; 
    }
    else
    {   
        /*Shut down port*/
        portCtrl.port_id = mfmmParam->portid;
        portCtrl.slot =LBDT_COM_SLOT_IRRELEVANT_ID;
        portCtrl.enable = FALSE;
        lbdt_set_phy_admin(&(portCtrl)); 

        /*Record loop detect alarm*/
       lbdt_save_alarm(mfmmParam->portid);
       lbdt_loop_send_pkt_clear(mfmmParam->portid);
       lbdt_loop_send_pkt_reset_fmm();
        if(LBDT_FMM_DEFAULT_VALUE == g_LoopDetectRecoverUniTimer[mfmmParam->portid])
        {
            g_LoopDetectRecoverUniTimer[mfmmParam->portid] = 
            mthread_register_timer(ethPortData.recoveryIntervalTime*1000, TRUE, &(g_ReceiceLoopPkt[mfmmParam->portid]), (MTHREAD_TIMER_FN_T)lbdt_down_time_out_set_phy_admin);
        }
        else
        {
            if(0 != mthread_reset_timer(g_LoopDetectRecoverUniTimer[mfmmParam->portid]))
            {
                lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                        __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
            }
        }

        if(0 != mthread_reset_timer(g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid]))
        {
            lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                    __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
        }

        return LBDT_DOWN_STATE;
  }
}

/******************************************************************************
 * Function   : lbdt_mfmm_without_loop_state_wait_loop_packet_time_out_event
 *              
 * Description: This function process the time out event of receiving loop packet in no loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_without_loop_state_receive_loop_packet_time_out_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    LBDT_CTRL_DATA_S mfmmPortData;

    if(mfmmParam->portid >=LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: port index exceed max value!. errno = %d, %s", 
                                           __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));  
        return LBDT_UP_WITHOUT_LOOP_STATE;
    }

    memset(&mfmmPortData, 0, sizeof(LBDT_CTRL_DATA_S));        
    lbdt_port_data_get(&(mfmmPortData.ethPortData));

    mfmmPortData.portIndex  = mfmmParam->portid;

    lbdt_clear_alarm(mfmmParam->portid);

    memcpy(&g_WosLoopPkTimeOut[mfmmParam->portid], &mfmmPortData, sizeof(LBDT_CTRL_DATA_S));

    if(LBDT_FMM_DEFAULT_VALUE != g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid])
    {
        if(0 != mthread_reset_timer(g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid]))
        {
            lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                 __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
        }
    }

    return LBDT_UP_WITHOUT_LOOP_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_without_loop_state_recovery_uni_time_out_event
 *              
 * Description: This function process time out event of recovery uni in no loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_without_loop_state_recovery_uni_time_out_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    if(mfmmParam->portid >=LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: port index exceed max value!. errno = %d, %s", 
                                           __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));   
        return LBDT_UP_WITHOUT_LOOP_STATE;
    }

    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    lbdt_clear_alarm(mfmmParam->portid);
    
    return LBDT_UP_WITHOUT_LOOP_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_with_loop_state_disable_event
 *              
 * Description: This function process the disable event in loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_with_loop_state_disable_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{

    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_DISABLE_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_with_loop_state_enable_event
 *              
 * Description: This function enable event in loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
UINT32 lbdt_mfmm_with_loop_state_enable_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_UP_WITH_LOOP_STATE;
}

/******************************************************************************
 * Function   : lbdt_mfmm_with_loop_state_shut_down_event
 *              
 * Description: This function process shutting down event in loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_with_loop_state_shut_down_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    LBDT_CTRL_PORT_S portCtrl; 

    if(mfmmParam->portid >=LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: port index exceed max value!. errno = %d, %s", 
                                           __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno)); 
        return LBDT_UP_WITH_LOOP_STATE; 
    }

    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    memset(&(g_LoopShutDown[mfmmParam->portid]), 0, sizeof(LBDT_CTRL_DATA_S));        
    lbdt_port_data_get(&(g_LoopShutDown[mfmmParam->portid].ethPortData));
    g_LoopShutDown[mfmmParam->portid].portIndex = mfmmParam->portid;
     /*Shut down port*/
    portCtrl.port_id = mfmmParam->portid;
    portCtrl.slot =LBDT_COM_SLOT_IRRELEVANT_ID;
    portCtrl.enable = FALSE;
    lbdt_set_phy_admin(&portCtrl); 

    if(0 != mthread_reset_timer(g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid]))
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                             __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
    }


    return LBDT_DOWN_STATE; 
}


/******************************************************************************
 * Function   : lbdt_mfmm_with_loop_state_no_shut_down_event
 *              
 * Description: This function process no shutting down event in loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_with_loop_state_no_shut_down_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_UP_WITH_LOOP_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_with_loop_state_recovery_time_modify_event
 *              
 * Description: This function process modifying recovery time event in loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_with_loop_state_recovery_time_modify_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    if(mfmmParam->portid >=LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: port index exceed max value!. errno = %d, %s", 
                                           __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno)); 
       return LBDT_UP_WITH_LOOP_STATE;
    }

    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    if(0 != mthread_reset_timer(g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid]))
    {
            lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                             __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                

    }

    return LBDT_UP_WITH_LOOP_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_with_loop_state_receive_loop_packet_event
 *              
 * Description: This function process receiving loop packet event in loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_with_loop_state_receive_loop_packet_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    if(mfmmParam->portid >=LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: port index exceed max value!. errno = %d, %s", 
                                           __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));
        return LBDT_UP_WITH_LOOP_STATE;
    }

    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);
    lbdt_loop_send_pkt_clear(mfmmParam->portid);
    lbdt_loop_send_pkt_reset_fmm();
    lbdt_save_alarm(mfmmParam->portid);
    
    if(0 != mthread_reset_timer(g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid]))
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                             __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
    }

    return LBDT_UP_WITH_LOOP_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_with_loop_state_wait_loop_packet_time_out_event
 *              
 * Description: This function process time out of receiving loop packet in loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_with_loop_state_receive_loop_packet_time_out_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    LBDT_CTRL_DATA_S mfmmPortData;
    
    if(mfmmParam->portid >=LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: port index exceed max value!. errno = %d, %s", 
                                           __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));
        return LBDT_UP_WITH_LOOP_STATE;
    }

    memset(&mfmmPortData, 0, sizeof(LBDT_CTRL_DATA_S));   
    
    lbdt_port_data_get(&mfmmPortData.ethPortData);
    mfmmPortData.portIndex = mfmmParam->portid;
    lbdt_clear_alarm(mfmmParam->portid);
    lbdt_loop_send_pkt_save(mfmmPortData.portIndex);
    lbdt_loop_send_pkt_reset_fmm();

    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    memcpy(&(g_LoopPktTimeOut[mfmmParam->portid]), &mfmmPortData, sizeof(LBDT_CTRL_DATA_S));

    if(0 != mthread_reset_timer(g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid]))
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                             __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
    }

    return LBDT_UP_WITHOUT_LOOP_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_with_loop_state_recovery_uni_time_out_event
 *              
 * Description: This function process time out event of recovery UNI in loop state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_with_loop_state_recovery_uni_time_out_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_UP_WITH_LOOP_STATE;

}


/******************************************************************************
 * Function   : lbdt_mfmm_down_state_disable_event
 *              
 * Description: This function process the disable event in down state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_down_state_disable_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_DISABLE_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_down_state_enable_event
 *              
 * Description: This function enable event in down state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_down_state_enable_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_DOWN_STATE;
}

/******************************************************************************
 * Function   : lbdt_mfmm_down_state_shut_down_event
 *              
 * Description: This function process shutting down event in down state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_down_state_shut_down_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_DOWN_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_down_state_no_shut_down_event
 *              
 * Description: This function process no shutting down event in down state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_down_state_no_shut_down_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_DOWN_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_down_state_recovery_time_modify_event
 *              
 * Description: This function process modifying recovery time event in down state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_down_state_recovery_time_modify_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    if(mfmmParam->portid >=LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: port index exceed max value!. errno = %d, %s", 
                                           __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno)); 
        return LBDT_DOWN_STATE;
    }

    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    if(0 != mthread_reset_timer(g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid]))
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                         __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                

    }
    
    return LBDT_DISABLE_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_down_state_receive_loop_packet_event
 *              
 * Description: This function process receiving loop packet event in down state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_down_state_receive_loop_packet_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_DEBUG,"\n%s: line = %d", __FUNCTION__, __LINE__);

    return LBDT_DOWN_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_down_state_wait_loop_packet_time_out_event
 *              
 * Description: This function process time out of receiving loop packet event in down state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_down_state_receive_loop_packet_time_out_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    LBDT_CTRL_DATA_S mfmmPortData;
    LBDT_CTRL_PORT_S portCtrl; 

    if(mfmmParam->portid >=LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: port index exceed max value!. errno = %d, %s", 
                                           __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno)); 
        return LBDT_DOWN_STATE;
    }

    memset(&mfmmPortData, 0, sizeof(LBDT_CTRL_DATA_S)); 
    memset(&portCtrl, 0, sizeof(LBDT_CTRL_PORT_S));    
    lbdt_port_data_get(&mfmmPortData.ethPortData);

    portCtrl.port_id = mfmmParam->portid;
    portCtrl.slot = LBDT_COM_SLOT_IRRELEVANT_ID;
    portCtrl.enable = TRUE;
    lbdt_set_phy_admin(&portCtrl); 
    
    lbdt_clear_alarm(mfmmParam->portid);

    mfmmPortData.portIndex = mfmmParam->portid;
    lbdt_loop_send_pkt_save(mfmmPortData.portIndex);
    lbdt_loop_send_pkt_reset_fmm();

    memcpy(&(g_DownWaitPktTimeOut[mfmmParam->portid]), &mfmmPortData, sizeof(LBDT_CTRL_DATA_S));
  
    if(0 != mthread_reset_timer(g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid]))
   {
       lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"%s: reset wait loop packet timer failed. errno = %d, %s", 
                                        __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
    }

  
    return LBDT_UP_WITHOUT_LOOP_STATE; 
}

/******************************************************************************
 * Function   : lbdt_mfmm_down_state_recovery_uni_time_out_event
 *              
 * Description: This function process time out event of recovery UNI in down state
 * 
 *
 * Parameters :  mfmmParam: FSM input parameter.
 *         
 *
 * Returns    : LBDT_DISABLE_STATE
 *
 ******************************************************************************/
INT32 lbdt_mfmm_down_state_recovery_uni_time_out_event(LBDT_FSM_PARAMETER_S *mfmmParam)
{
    LBDT_CTRL_DATA_S mfmmPortData;
    LBDT_CTRL_PORT_S portCtrl; 
    
    if(mfmmParam->portid >=LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: port index exceed max value!. errno = %d, %s", 
                                           __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));
        return LBDT_DOWN_STATE;
    }

    memset(&mfmmPortData, 0, sizeof(LBDT_CTRL_DATA_S));        
    lbdt_port_data_get(&(mfmmPortData.ethPortData));

    portCtrl.port_id = mfmmParam->portid;
    portCtrl.slot =LBDT_COM_SLOT_IRRELEVANT_ID;
    portCtrl.enable = TRUE;
    lbdt_set_phy_admin(&portCtrl);

    lbdt_clear_alarm(mfmmParam->portid);

    mfmmPortData.portIndex = mfmmParam->portid;
    lbdt_loop_send_pkt_save(mfmmPortData.portIndex);
    lbdt_loop_send_pkt_reset_fmm();
    memcpy(&(g_DownLoopRecoverUniTimeOut[mfmmParam->portid]), &mfmmPortData, sizeof(LBDT_CTRL_DATA_S));

    if(0 != mthread_reset_timer(g_LoopDetectWaitLoopPackeTimer[mfmmParam->portid]))
    {
        lbdt_log_printf(LBDT_LOG_MODULE,LBDT_LOG_ERROR,"\n%s: reset wait loop packet timer failed. errno = %d, %s", 
                                             __FUNCTION__, LBDT_ETHERNET_TYPE, errno, strerror(errno));                
    }
    return LBDT_UP_WITHOUT_LOOP_STATE;

}

/*******************************************************************************
 *
 *  Function:    lbdt_mfmm_init
 * 
 *  Description: The function init loop detect mfmm
 *                 
 *  Returns:     void
 *
 ******************************************************************************/
void lbdt_mfmm_init( )
{
    INT32 i = 0;

    for(i=0; i< LBDT_MAX_PORT_NUM; i++)
    {
        if (g_InitialLoopDetectInfo.portEnable[i] == FALSE || g_InitialLoopDetectInfo.isLoopBackSupport == FALSE)
        {
            mfmm_create(g_LoopDetectFsm, i, LBDT_MANUAL_DISABLE_STATE, 
                              LBDT_FMM_MAX_STATES, LBDT_FMM_MAX_EVENTS);

        }
        else
        {
            mfmm_create(g_LoopDetectFsm, i, LBDT_DISABLE_STATE, 
                             LBDT_FMM_MAX_STATES, LBDT_FMM_MAX_EVENTS);
        }

        /*disable state*/
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DISABLE_STATE, LBDT_ENABLE_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_disable_state_enable_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DISABLE_STATE, LBDT_DISABLE_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_disable_state_disable_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DISABLE_STATE, LBDT_SHUT_DOWN_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_disable_state_shut_down_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DISABLE_STATE, LBDT_NO_SHUT_DOWN_EVENT, (FMM_EVTHANDLER)&lbdt_mfmm_disable_state_no_shut_down_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DISABLE_STATE, LBDT_RECOVERY_TIME_MODIFY_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_disable_state_recovery_time_modify_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DISABLE_STATE, LBDT_RECEIVE_LOOP_PACKET_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_disable_state_receive_loop_packet_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DISABLE_STATE, LBDT_RECEIVE_LOOP_PACKET_TIME_OUT_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_disable_state_receive_loop_packet_time_out_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DISABLE_STATE, LBDT_RECOVERY_UNI_TIME_OUT_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_disable_state_recovery_uni_time_out_event);

        /*up without loop*/
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITHOUT_LOOP_STATE, LBDT_ENABLE_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_without_loop_state_enable_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITHOUT_LOOP_STATE, LBDT_DISABLE_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_without_loop_state_disable_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITHOUT_LOOP_STATE, LBDT_SHUT_DOWN_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_without_loop_state_shut_down_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITHOUT_LOOP_STATE, LBDT_NO_SHUT_DOWN_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_without_loop_state_no_shut_down_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITHOUT_LOOP_STATE, LBDT_RECOVERY_TIME_MODIFY_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_without_loop_state_recovery_time_modify_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITHOUT_LOOP_STATE, LBDT_RECEIVE_LOOP_PACKET_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_without_loop_state_receive_loop_packet_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITHOUT_LOOP_STATE, LBDT_RECEIVE_LOOP_PACKET_TIME_OUT_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_without_loop_state_receive_loop_packet_time_out_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITHOUT_LOOP_STATE, LBDT_RECOVERY_UNI_TIME_OUT_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_without_loop_state_recovery_uni_time_out_event);

        /*up with loop*/
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITH_LOOP_STATE, LBDT_ENABLE_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_with_loop_state_enable_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITH_LOOP_STATE, LBDT_DISABLE_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_with_loop_state_disable_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITH_LOOP_STATE, LBDT_SHUT_DOWN_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_with_loop_state_shut_down_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITH_LOOP_STATE, LBDT_NO_SHUT_DOWN_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_with_loop_state_no_shut_down_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITH_LOOP_STATE, LBDT_RECOVERY_TIME_MODIFY_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_with_loop_state_recovery_time_modify_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITH_LOOP_STATE, LBDT_RECEIVE_LOOP_PACKET_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_with_loop_state_receive_loop_packet_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITH_LOOP_STATE, LBDT_RECEIVE_LOOP_PACKET_TIME_OUT_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_with_loop_state_receive_loop_packet_time_out_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_UP_WITH_LOOP_STATE, LBDT_RECOVERY_UNI_TIME_OUT_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_with_loop_state_recovery_uni_time_out_event);

        /*down*/
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DOWN_STATE, LBDT_ENABLE_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_down_state_enable_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DOWN_STATE, LBDT_DISABLE_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_down_state_disable_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DOWN_STATE, LBDT_SHUT_DOWN_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_down_state_shut_down_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DOWN_STATE, LBDT_NO_SHUT_DOWN_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_down_state_no_shut_down_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DOWN_STATE, LBDT_RECOVERY_TIME_MODIFY_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_down_state_recovery_time_modify_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DOWN_STATE, LBDT_RECEIVE_LOOP_PACKET_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_down_state_receive_loop_packet_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DOWN_STATE, LBDT_RECEIVE_LOOP_PACKET_TIME_OUT_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_down_state_receive_loop_packet_time_out_event);
        mfmm_set_handler(&g_LoopDetectFsm, i,  LBDT_DOWN_STATE, LBDT_RECOVERY_UNI_TIME_OUT_EVENT, (FMM_EVTHANDLER)lbdt_mfmm_down_state_recovery_uni_time_out_event);

        /* Manual Disable State, do nothing */
    }
    
    for(i=0; i< LBDT_MAX_PORT_NUM; i++)
    {
        g_LoopDetectWaitLoopPackeTimer[i] = LBDT_FMM_DEFAULT_VALUE;
        g_LoopDetectRecoverUniTimer[i] = LBDT_FMM_DEFAULT_VALUE;
    }

    memset(&g_WaitLoopPacketTimer, 0, sizeof(LBDT_CTRL_DATA_S)*LBDT_MAX_PORT_NUM);
    memset(&g_WosSendPkTimeOut, 0, sizeof(LBDT_CTRL_DATA_S)*LBDT_MAX_PORT_NUM);
    memset(&g_LoopPktTimeOut, 0, sizeof(LBDT_CTRL_DATA_S)*LBDT_MAX_PORT_NUM);
    memset(&g_LoopSendPktTimeOut, 0, sizeof(LBDT_CTRL_DATA_S)*LBDT_MAX_PORT_NUM);
    memset(&g_ReceiceLoopPkt, 0, sizeof(LBDT_CTRL_DATA_S)*LBDT_MAX_PORT_NUM);
    memset(&g_DownWaitPktTimeOut, 0, sizeof(LBDT_CTRL_DATA_S)*LBDT_MAX_PORT_NUM);
    memset(&g_LoopRecoverUniTimeOut, 0, sizeof(LBDT_CTRL_DATA_S)*LBDT_MAX_PORT_NUM);
    memset(&g_WosLoopPkTimeOut, 0, sizeof(LBDT_CTRL_DATA_S)*LBDT_MAX_PORT_NUM);
    memset(&g_LoopShutDown, 0, sizeof(LBDT_CTRL_DATA_S)*LBDT_MAX_PORT_NUM);


}


