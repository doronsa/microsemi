/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : UNI loop detect                                           **/
/**                                                                          **/
/**  FILE        : lbdt_log.c                                         **/
/**                                                                          **/
/**  DESCRIPTION : Definition of log module                                  **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   10/Jan/2011           
 *                                                                              
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
//#include "globals.h"


#include <time.h>
#include "oam_expo.h"
#include "lbdt_log.h"
#include "lbdt_main.h"

/*----------------------------------------------------------------------------*/
/* External  declaration                                                      */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/* Global static definition                                                   */
/*----------------------------------------------------------------------------*/
static LBDT_LOG_SEVERITY_E g_lbdt_log_level    = LBDT_LOG_INFO;
static LBDT_LOG_TERMINAL_E g_lbdt_log_terminal = LBDT_LOG_TO_STDOUT; /*output log terminal mode*/
static BOOL  g_lbdt_log_trace_normal = 0;
static INT8  g_strBuff[2048]; 

static STATUS lbdt_log_trace_check_if_valid(VOID);

/*----------------------------------------------------------------------------*/
/* Function implementation                                                    */
/*----------------------------------------------------------------------------*/
static INT32 os_chmod(INT8 *pathname,INT32 mode)
{
    return chmod(pathname,(mode_t)mode);
}

BOOL lbdt_check_if_initialized()
{
    BOOL l_init_flag = FALSE;
    char buff[255] ={0};
     /*start cli pthread deamon*/
    FILE *fd =fopen("/tmp/lbdt_config.log", "r");
    if(fd == NULL)
    {
        fd =fopen("/tmp/lbdt_config.log", "w+");
        if(fd == NULL)
        {
            printf("/tmp/lbdt_config.log' ,please configure the envir-Variable first");
            return FALSE ;
        }
    }

    os_chmod("/tmp/lbdt_config.log",438);
        
    while(fgets(buff,255,fd)!= NULL)
    {
        if(strstr(buff,"<Loop detect statak initialized") != NULL)
        {
            if(strstr(buff,"true") != NULL)
            {
                l_init_flag = TRUE;
                break;
            }
            else
            {
                break;
            }
        }
        memset(buff, 0, sizeof(buff));
    }
    
    fclose(fd);
    
    return l_init_flag ;
}

/*Log level*/
VOID lbdt_log_severity_level_set(LBDT_LOG_SEVERITY_E level)
{
    if(level <= LBDT_LOG_DEBUG_PKT )
    {
        g_lbdt_log_level = level;
    }
}

LBDT_LOG_SEVERITY_E lbdt_log_severity_level_get(VOID)
{
    return g_lbdt_log_level;
}

/*Log terminal mode, to tty or file*/
VOID lbdt_log_terminal_mode_set(LBDT_LOG_TERMINAL_E mode)
{
    if(mode <= LBDT_LOG_TO_STDOUT )
    {
        g_lbdt_log_terminal = mode;
    }
}

LBDT_LOG_TERMINAL_E lbdt_log_terminal_mode_get()
{
    return g_lbdt_log_terminal;
}

/*Log terminal implementation*/
static VOID oam_log_terminal_tty_out(const char *format, va_list param)
{   
    //printf("OAM stack: ");
    vprintf(format, param); 
}

static VOID oam_log_terminal_traceout(const char *format, va_list param)
{
    FILE * fd = 0;
    
    if(lbdt_log_trace_check_if_valid() != LBDT_OK)
    {
        printf("ctc log terminal tracerout not valid\r\n");        
        
        return;
    }
    
    fd = fopen("/tmp/ctc_config.log", "a");
    if(fd != NULL)
    {
        vfprintf(fd, format, param);
    
        fclose(fd);        
    }
}

VOID lbdt_log_printf(const char *func_name, LBDT_LOG_SEVERITY_E level, const char * format, ...)
{ 
    va_list arg_ptr; 
    time_t tm;
    char s[32];

    if(lbdt_log_severity_level_get() < level)
    {
        return;
    }
    
    if(lbdt_log_terminal_mode_get() == LBDT_LOG_TO_FILE)
    {
        va_start(arg_ptr, format); 
        oam_log_terminal_traceout  (format, arg_ptr); 
        va_end(arg_ptr); 
    }
    else if(lbdt_log_terminal_mode_get() == LBDT_LOG_TO_STDOUT)
    {
        memset(s, 0, sizeof(s));
        
        time(&tm);
        strcpy(s, ctime(&tm));
        s[strlen(s)-1]='\0';
        
        printf("[ LOOP @ %s ]:  ", s);
        
        va_start(arg_ptr, format); 
        oam_log_terminal_tty_out  (format, arg_ptr); 
        va_end(arg_ptr); 
    }
    else if(lbdt_log_terminal_mode_get() == LBDT_LOG_TO_CLI)
    {
        va_start(arg_ptr, format); 
        va_end(arg_ptr); 
    }
}

VOID lbdt_dbg_print_frame(LBDT_LOG_SEVERITY_E level, UINT8 *a_frame, UINT16 a_frame_size)
{
    UINT16 l_index, l_temp_lenth = 0;
    UINT8  l_column;
    INT8   *strBuff = g_strBuff;
    
    if(g_lbdt_log_level < level)
    {
        return;
    } 
    
    for (l_index = 0; l_index < a_frame_size; ++l_index)
    {
        if (l_index == 0)   /* Print column banner */
        {
            sprintf(strBuff+l_temp_lenth,"\n");
            l_temp_lenth = strlen(strBuff);
             
            for (l_column = 0; l_column < 16; ++l_column)
            {
                sprintf(strBuff+l_temp_lenth,"%02x-", l_column);
                l_temp_lenth = strlen(strBuff);
            }         
        }
        
        if (l_index % 0x10 == 0) /* Print row offset prefix */
        {
            sprintf(strBuff+l_temp_lenth,"\n");
            l_temp_lenth = strlen(strBuff);
        }      
        
        sprintf(strBuff+l_temp_lenth,"%02x ", a_frame[l_index]);
        l_temp_lenth = strlen(strBuff);  
    }
    
    sprintf(strBuff+l_temp_lenth,"\n\n");
    l_temp_lenth = strlen(strBuff);  
    
    lbdt_log_printf(LBDT_LOG_MODULE,level,strBuff);
}

/*-------------------------------------------------------------------------------
* Log and traceout internal API 
*-------------------------------------------------------------------------------*/
static STATUS lbdt_log_trace_check_if_valid(VOID)
{
    if(g_lbdt_log_trace_normal != TRUE)
    {
        printf("Error,ctc log traceout is not initialzed!\r\n"); 
        
        return LBDT_ERROR;
    }
    
    return LBDT_OK;
}

/*-------------------------------------------------------------------------------
* Log and traceout init view
*-------------------------------------------------------------------------------*/
STATUS lbdt_log_init(VOID)
{
    static FILE       *fd = NULL; 
    
    fd = fopen("/tmp/lbdt_config.log", "r"); 
    if(fd==NULL)  
    { 
        printf("Error note :\n");
        printf("    log file '/tmp/lbdt_config.log'failed\n");
        printf("    the log dump function will be affected.\n");
        printf("    pls check the entir-variable or check log file\n");
    
        g_lbdt_log_trace_normal = 0;
    } 
    else
    {
        g_lbdt_log_trace_normal = TRUE;
    
        fclose(fd);
    }
    return 0;
}

STATUS lbdt_log_terminate(VOID)
{
    g_lbdt_log_trace_normal = 0;    
    return 0;
}
