/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : LOOP BACK                                               **/
/**                                                                          **/
/**  FILE        : lbdt_api.c                                   **/
/**                                                                          **/
/**  DESCRIPTION : Definition of UNI port loop detection functions           **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Thomas.wang  - initial version created.   1/Dev./2012          
 *                                                                              
 ******************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>

#include "oam_expo.h"
#include "lbdt_api.h"
#include "lbdt_log.h"
#include "lbdt_fmm.h"
#include "FmmUtils.h"

extern UINT32 g_LoopDetectAlarm;
extern UINT32  g_LoopDetectWaitLoopPackeTimer[LBDT_MAX_PORT_NUM];
extern UINT32  g_LoopDetectRecoverUniTimer[LBDT_MAX_PORT_NUM];
extern FMM_INFO_S g_LoopDetectFsm[LBDT_MAX_PORT_NUM]; 
extern LBDT_INFO_S  g_InitialLoopDetectInfo;
extern UINT32  g_lbdtSendPackeTimer;
extern LBDT_PACKET_VLAN_S g_lbdtSendMaxPacket[LBDT_MAX_PORT_NUM][LBDT_PACKET_MAX_NUM];

/*******************************************************************************
 *
 *  Function:    lbdt_get_alarm
 * 
 *  Description: Get loop detection alarm. 
 *                 
 *  Returns:     
 *  loop_alarm : Loop detect bitmap of all UNI port, bit0:UNI0,...bit3:UNI3.
 *               1:loopback detected, 0:no loopback
 *
 ******************************************************************************/
INT32  lbdt_get_alarm(UINT32 *loop_alarm)
{
    *loop_alarm = g_LoopDetectAlarm;
    
    return LBDT_OK;     
}

/*******************************************************************************
 *
 *  Function:    lbdt_port_tag_information_set
 * 
 *  Description: synchronize loop back data structure with OMCI and OAM 
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_port_tag_information_set(UINT32 portId, LBDT_TAG_INFO_S *portTagInfo)
{
    LBDT_PARAMETER_S lbdtData;
    UINT32 i = 0;

    memset(&lbdtData, 0, sizeof(LBDT_PARAMETER_S));
    lbdt_port_data_get(&lbdtData);

    if(portId >= LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG, "port=%d, exceed maximum value.\r\n", __FUNCTION__, portId);
        return LBDT_ERROR;
    }
    
    if((TRUE ==  g_InitialLoopDetectInfo.portEnable[portId]) && (TRUE == g_InitialLoopDetectInfo.isLoopBackSupport))
    {
        for(i=0; i< LBDT_VLAN_LIST; i++)
        {
            if(FALSE == lbdtData.portparameter[portId].vlanListInuse[i])
            {
                 if((0 != portTagInfo->cvlan) || (0  != portTagInfo->svlan))
                {
                    lbdtData.portparameter[portId].vlan_list[i].cvlan = portTagInfo->cvlan;
                    lbdtData.portparameter[portId].vlan_list[i].svlan = portTagInfo->svlan;
                    lbdtData.portparameter[portId].untagInUse = portTagInfo->untagInUse;
                    lbdtData.portparameter[portId].vlanListInuse[i] = TRUE;
                }
                else
                {
                    lbdtData.portparameter[portId].untagInUse = portTagInfo->untagInUse;                   
                }
                lbdt_vlan_information_update_control(portId, TRUE);
                break;
            }
        }
    }
    
    lbdt_port_data_set(&lbdtData);

    return LBDT_OK;
}

/*******************************************************************************
 *
 *  Function:    lbdt_port_tag_information_del
 * 
 *  Description: synchronize loop back data structure with OMCI and OAM 
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_port_tag_information_del(UINT32 portId)
{
    LBDT_PARAMETER_S lbdtData;
    UINT32 i = 0;

    memset(&lbdtData, 0, sizeof(LBDT_PARAMETER_S));
    lbdt_port_data_get(&lbdtData);
    if(portId >= LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG, "port=%d, exceed maximum value.\r\n", __FUNCTION__, portId);
        return LBDT_ERROR;
    }

    memset(lbdtData.portparameter[portId].vlan_list, 0, sizeof(LBDT_TABLE_VLAN_S)*LBDT_VLAN_LIST);
    memset(lbdtData.portparameter[portId].vlanListInuse, 0, sizeof(lbdtData.portparameter[portId].vlanListInuse));
    lbdtData.portparameter[portId].untagInUse = FALSE;

    lbdt_port_data_set(&lbdtData);

    return LBDT_OK;
}

/*******************************************************************************
 *
 *  Function:    lbdt_packet_interval_set
 * 
 *  Description: synchronize loop back data structure with OMCI and OAM 
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_packet_interval_set(UINT32 pktInter)
{
    LBDT_PARAMETER_S lbdtData;

    memset(&lbdtData, 0, sizeof(LBDT_PARAMETER_S));
    lbdt_port_data_get(&lbdtData);

    lbdtData.pktInterval = pktInter;

    lbdt_port_data_set(&lbdtData);

    lbdt_loop_send_packet_reset_timer();

    return LBDT_OK;
}


/*******************************************************************************
 *
 *  Function:    lbdt_recovery_interval_time_set
 * 
 *  Description: synchronize loop back data structure with OMCI and OAM 
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_recovery_interval_time_set(UINT32 recIntervalTime)
{
    LBDT_PARAMETER_S lbdtData;
    LBDT_FSM_PARAMETER_S fsmParams;
    UINT32 i = 0;

    memset(&fsmParams, 0, sizeof(LBDT_FSM_PARAMETER_S));
    memset(&lbdtData, 0, sizeof(LBDT_PARAMETER_S));
    
    lbdt_port_data_get(&lbdtData);

    for(i=0; i<LBDT_MAX_PORT_NUM; i++)
    {
        if((TRUE ==  g_InitialLoopDetectInfo.portEnable[i]) && (TRUE == g_InitialLoopDetectInfo.isLoopBackSupport))
        {
            if(lbdtData.recoveryIntervalTime != recIntervalTime)
            {
                 lbdtData.recoveryIntervalTime = recIntervalTime;
                 lbdt_port_data_set(&lbdtData);
                 fsmParams.portid = i;
                mfmm_run(g_LoopDetectFsm, i, LBDT_RECOVERY_TIME_MODIFY_EVENT, &fsmParams);
            }
        }
    }

    return LBDT_OK;
}

/*******************************************************************************
 *
 *  Function:    lbdt_port_manage_set
 * 
 *  Description: synchronize loop back data structure with OMCI and OAM
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_port_manage_set(UINT32 portId, UINT16 portManage)
{
    LBDT_PARAMETER_S lbdtData;
    LBDT_FSM_PARAMETER_S fsmParams;

    memset(&fsmParams, 0, sizeof(LBDT_FSM_PARAMETER_S));
    memset(&lbdtData, 0, sizeof(LBDT_PARAMETER_S));
    
    lbdt_port_data_get(&lbdtData);

    fsmParams.portid = portId;

    if(portId >= LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG, "port=%d, exceed maximum value.\r\n", __FUNCTION__, portId);
        return LBDT_ERROR;
    }
    
    if((TRUE ==  g_InitialLoopDetectInfo.portEnable[portId]) && (TRUE == g_InitialLoopDetectInfo.isLoopBackSupport))
    {
        lbdtData.portparameter[portId].management = portManage;

        lbdt_port_data_set(&lbdtData);

         if(LBDT_MANAGE_ACTIVATE == lbdtData.portparameter[portId].management)
        {
            mfmm_run(g_LoopDetectFsm, portId, LBDT_ENABLE_EVENT, &fsmParams);
        }
        else
        {
            mfmm_run(g_LoopDetectFsm, portId, LBDT_DISABLE_EVENT, &fsmParams);
            lbdt_loop_send_pkt_clear(portId);
        }
    }

    return LBDT_OK;
}

/*******************************************************************************
 *
 *  Function:    lbdt_port_down_option_set
 * 
 *  Description: synchronize loop back data structure OMCI and OAM
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_port_down_option_set(UINT32 portId, UINT16 portDownOpt)
{
    LBDT_PARAMETER_S lbdtData;
    LBDT_FSM_PARAMETER_S fsmParams;

    memset(&fsmParams, 0, sizeof(LBDT_FSM_PARAMETER_S));
    memset(&lbdtData, 0, sizeof(LBDT_PARAMETER_S));
    lbdt_port_data_get(&lbdtData);

    if(portId >= LBDT_MAX_PORT_NUM)
    {
        lbdt_log_printf(LBDT_LOG_MODULE, LBDT_LOG_DEBUG, "port=%d, exceed maximum value.\r\n", __FUNCTION__, portId);
        return LBDT_ERROR;
    }

    fsmParams.portid = portId;
    
    if((TRUE ==  g_InitialLoopDetectInfo.portEnable[portId]) && (TRUE == g_InitialLoopDetectInfo.isLoopBackSupport))
    {
        lbdtData.portparameter[portId].portDownOption = portDownOpt;

        lbdt_port_data_set(&lbdtData);

        if(LBDT_PORT_SHUT_DOWN_ACTIVATE == lbdtData.portparameter[portId].portDownOption)
        {
            mfmm_run(g_LoopDetectFsm, portId, LBDT_SHUT_DOWN_EVENT, &fsmParams);
        }

        if(0 == lbdtData.portparameter[portId].portDownOption)
        {
            mfmm_run(g_LoopDetectFsm, portId, LBDT_NO_SHUT_DOWN_EVENT, &fsmParams);
        }
    }

    return LBDT_OK;
}


/******************************************************************************
 *
 * Function   : lbdt_configure_log_level
 *              
 * Description: This function set  log print right level
 *              
 * Parameters : level: print control level 
 *
 * Returns    : LBDT_OK
 *              
 ******************************************************************************/
INT32 lbdt_configure_log_level(INT32 level)
{
    lbdt_log_severity_level_set(level);
    
    return LBDT_OK;
}

/******************************************************************************
 *
 * Function   : lbdt_configure_ethernet_type
 *              
 * Description: This function set  log print right level
 *              
 * Parameters : ethernetType: ethernet type.
 *
 * Returns    : LBDT_OK
 *              
 ******************************************************************************/
INT32 lbdt_configure_ethernet_type(UINT16 ethernetType)
{
    lbdt_ethernet_type_modify(ethernetType);
    
    return LBDT_OK;
}

/******************************************************************************
 *
 * Function   : lbdt_printf_loop_back_data
 *              
 * Description: This function print loop back data structure
 *              
 * Parameters :  
 *
 * Returns    : LBDT_OK
 *              
 ******************************************************************************/
INT32 lbdt_printf_loop_back_data()
{
    LBDT_PARAMETER_S LoopBackData;
    UINT32 i = 0, j = 0;
    UINT32 index = 0;
    UINT32 pktStat = 0;

    memset(&LoopBackData, 0, sizeof(LoopBackData));
    lbdt_port_data_get(&LoopBackData);
    
    printf("\nCTC loop back manange data:");
    printf("\nLoop detection packet interval=0x%x",LoopBackData.pktInterval);
    printf("\nLoop detection recovery interval time=0x%x",LoopBackData.recoveryIntervalTime);
    printf("\nLoop detection ethernet type=0x%x",LoopBackData.ethernetType);
    printf("\nUNI table data:(HEX format)");
    printf("\n---------------------------------------------------");
    for(i=0; i<LBDT_MAX_PORT_NUM; i++)
    {
        printf("\nport   management   portDownOption untagInUse\n");
        printf("%2x%11x%14x%13x",
                i,
                LoopBackData.portparameter[i].management,
                LoopBackData.portparameter[i].portDownOption,
                LoopBackData.portparameter[i].untagInUse);
        printf("\nvlan inforamtion:");
        for(index=0; index<LBDT_VLAN_LIST; index++)
        {
            if(TRUE == LoopBackData.portparameter[i].vlanListInuse[index])
            {
                printf("\nvlan[%d]=[svlan=0x%x, cvlan=0x%x], vlanListInuse[%d]=%d",index,LoopBackData.portparameter[i].vlan_list[index].svlan,LoopBackData.portparameter[i].vlan_list[index].cvlan,
                        index, LoopBackData.portparameter[i].vlanListInuse[index]);
            }
        }
        printf("\n---------------------------------------------------");
    }
   printf("\n");


    printf("\n Send packet timer information,time_id:%d",g_lbdtSendPackeTimer);
    
    printf("\n Wait loop packet timer information,time_id:");
    for(i=0; i< LBDT_MAX_PORT_NUM; i++)
    {
        printf("[0x%x],",g_LoopDetectWaitLoopPackeTimer[i]);
    }
    
    printf("\n Recover UNI timer information, time_id:");
    for(i=0; i< LBDT_MAX_PORT_NUM; i++)
    {
        printf("[0x%x],",g_LoopDetectRecoverUniTimer[i]);
    }

    printf("\n");

    for(i=0; i< LBDT_MAX_PORT_NUM; i++)
    {
        for(j=0; j< LBDT_PACKET_MAX_NUM; j++)
        {
            if(0 != g_lbdtSendMaxPacket[i][j].ethernetType)
            {
                pktStat++;
            }
        }
    }

    printf("\n all packet for loop detect sum number=%d:",pktStat);
    for(i=0; i< LBDT_MAX_PORT_NUM; i++)
    {
        for(j=0; j< LBDT_PACKET_MAX_NUM; j++)
        {
            if(0 != g_lbdtSendMaxPacket[i][j].ethernetType)
            {
                printf("\n-----------------------\n");
                printf("operation=%d\n",g_lbdtSendMaxPacket[i][j].opt);
                printf("uni port=%d\n",g_lbdtSendMaxPacket[i][j].port);
                printf("packet svlan=0x%x\n",g_lbdtSendMaxPacket[i][j].svlan );
                printf("packet cvlan=0x%x\n",g_lbdtSendMaxPacket[i][j].cvlan );
                printf("packet ethernetType=0x%x\n",g_lbdtSendMaxPacket[i][j].ethernetType );
                printf("\n-----------------------");
            }
        }
    }
    
    printf("\n");

    lbdt_mfmm_show();

    return LBDT_OK;
}

