/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : UNI loop detection                                        **/
/**                                                                          **/
/**  FILE        : lbdt_fmm.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : Definition of log module                                  **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Thomas.wang  - initial version created.   1/Dev./2012           
 *                                                                              
 ******************************************************************************/

#ifndef __LBDT_FMM_H__
#define __LBDT_FMM_H__

#define LBDT_NO_RECEIVED_PACKET_TIME           5000
#define LBDT_SHUT_DOWN_PORT_OPER               1
#define LBDT_FMM_MAX_STATES                    5
#define LBDT_FMM_MAX_EVENTS                    8
#define LBDT_FMM_DEFAULT_VALUE                 0xFFFF



typedef enum
{
    LBDT_DISABLE_STATE = 0,
    LBDT_UP_WITHOUT_LOOP_STATE = 1,
    LBDT_UP_WITH_LOOP_STATE = 2,
    LBDT_DOWN_STATE =3,
    LBDT_MANUAL_DISABLE_STATE = 4,
}LBDT_STATES_E;


typedef enum
{
    LBDT_ENABLE_EVENT     = 0,   /*enable loop detect by application*/
    LBDT_DISABLE_EVENT = 1,      /*disable loop detect by configuration*/ 
    LBDT_SHUT_DOWN_EVENT     = 2,   /*shut down port if application detect loop with the port*/
    LBDT_NO_SHUT_DOWN_EVENT     = 3,     /*not shut down port if application detect loop with the port*/
    LBDT_RECOVERY_TIME_MODIFY_EVENT = 4,  /*application modify recovery time with every port*/
    LBDT_RECEIVE_LOOP_PACKET_EVENT = 5,     /*application modify recovery time with every port*/
    LBDT_RECEIVE_LOOP_PACKET_TIME_OUT_EVENT = 6,   /*it time out if application receive packet in one port*/
    LBDT_RECOVERY_UNI_TIME_OUT_EVENT = 7,   /*recovery UNI time out for every port*/
}LBDT_EVENTS_E;

#endif /*__UNI_LBDT_H__*/

