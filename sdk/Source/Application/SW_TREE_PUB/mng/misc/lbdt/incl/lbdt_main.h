/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : UNI loop detection                                        **/
/**                                                                          **/
/**  FILE        : lbdt_main.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : Definition of log module                                  **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   10/Jan/2011           
 *                                                                              
 ******************************************************************************/

#ifndef __LBDT_MAIN_H__
#define __LBDT_MAIN_H__


#define LBDT_MAX_PORT_NUM     8

#define LBDT_ETHERNET_TYPE                                        0xfffa   /*GPON loop back ethernet type =0xFFFA */
#define LBDT_PKT_LEN                                              64
#define LBDT_S_VLAN_TPID                                          0x88a8
#define LBDT_C_VLAN_TPID                                          0x8100
#define LBDT_VLAN_TPID_LEN                                        2
#define LBDT_VLAN_TAG_LEN                                         4
#define LBDT_OK                                                   0
#define LBDT_ERROR                                               -1
#define LBDT_COM_SLOT_IRRELEVANT_ID                               0
#define LBDT_ETH_ADDR_LEN                                                6
#define LBDT_VLAN_LIST                                            32
#define LBDT_IF_NAME_LENGTH                                        20
#define LBDT_PACKET_MAX_NUM                                      40

#define LBDT_ATTRIBUTE_TAG                                      3
#define LBDT_WAIT_LOOP_PACKET_TIME                      5

typedef struct
{
    UINT32 portid;
}LBDT_FSM_PARAMETER_S;

typedef struct
{
    UINT8 vlan_tbl :7;
    UINT8 resv2    :1;  
    UINT8 resv1    :4;
    UINT8 db_num   :4;
} LBDT_HDR_TX_S;

union
{
    LBDT_HDR_TX_S bs;
    UINT32 wd;
} LBDT_MH_S;

typedef struct
{
    BOOL isLoopBackSupport;
    UINT8 netCarName[LBDT_IF_NAME_LENGTH];
    UINT16 ethernetType;    
    UINT32 waitLoopPacketTime;
    BOOL portEnable[LBDT_MAX_PORT_NUM];
} LBDT_INFO_S;

typedef enum
{
    LBDT_UNI_1 = 1,
    LBDT_UNI_2 = 2,
    LBDT_UNI_3 = 3,
    LBDT_UNI_4 = 4,
    LBDT_UNI_5 = 5,
    LBDT_UNI_6 = 6,
    LBDT_UNI_7 = 7,
    LBDT_UNI_8 = 8,    
}LBDT_MAX_UNI_INDEX_E;

typedef enum
{
    LBDT_UNTAG = 0,
    LBDT_SINGLE_TAG = 1,
    LBDT_DOUBLE_TAG = 2,
}LBDT_TAG_FLAG_E;


typedef struct
{
    BOOL    opt; 
    UINT8   port;
    UINT16 svlan;
    UINT16 cvlan;
    UINT16 ethernetType;
}LBDT_PACKET_VLAN_S;

typedef struct
{
    UINT16 svlan;
    UINT16 cvlan;
}LBDT_TABLE_VLAN_S;

typedef struct
{
    UINT16 management;
    UINT16 portDownOption;
    BOOL vlanListInuse[LBDT_VLAN_LIST];    
    LBDT_TABLE_VLAN_S vlan_list[LBDT_VLAN_LIST];
    BOOL untagInUse;
} LBDT_TABLE_PARAMETER_S;

typedef struct
{
    UINT32 pktInterval;
    UINT32 recoveryIntervalTime;
    UINT16 ethernetType;
    LBDT_TABLE_PARAMETER_S portparameter[LBDT_MAX_PORT_NUM];
}LBDT_PARAMETER_S;


typedef struct 
{
    UINT8 portIndex;
    LBDT_PARAMETER_S ethPortData;
} LBDT_CTRL_DATA_S;

typedef struct
{
    UINT32 slot; 
    UINT32 port_id; 
    BOOL enable;
}LBDT_CTRL_PORT_S;

typedef struct
{
    UINT16 svlan;
    UINT16 cvlan;
    BOOL untagInUse;
}LBDT_TAG_INFO_S;

typedef enum
{
    LBDT_MANAGE_DEACTIVATE  = 0,
    LBDT_MANAGE_ACTIVATE    = 0x1
}LBDT_MANAGE_E;

typedef enum
{
    LBDT_PORT_SHUT_DOWN_DEACTIVATE  = 0,
    LBDT_PORT_SHUT_DOWN_ACTIVATE = 0x1
}LBDT_PORT_SHUT_DOWN_E;


#define  LBDT_XML_FILE_PATH                                                       "/etc/xml_params/lbdt_xml_file.xml"
#define  LBDT_SUPPORT_SWITCH                                                     "loop_detect_support"
#define  LBDT_ETHERNET_TYPE_DEFAULT                                         "ethernet_type"
#define  LBDT_NETCARD_NAME                                                         "network_card_name"
#define  LBDT_WAIT_LOOP_PACKET_TIME_DEFAULT                         "wait_loop_packet_time"
#define  LBDT_ATTRIBUTE_NAME                                                      "enabled"

/*******************************************************************************
 *
 *  Function:    lbdt_wait_loop_packet_time_out
 * 
 *  Description: The function transmit loop detection frames 
 *     
 *
 * Parameters :  timerId:timer ID
 *                     portInfo: port information
 *
 *  Returns:     INT32
 *
 ******************************************************************************/
INT32 lbdt_wait_loop_packet_time_out(INT32 timerId, LBDT_CTRL_DATA_S *portInfo);

/*******************************************************************************
 *
 *  Function:    lbdt_recovery_time_out_trigger_mfmm
 * 
 *  Description: The function transmit loop detection frames 
 *        
 * Parameters :  timerId:timer ID
 *                     portData: port information
 *
 *  Returns:     INT32
 *
 ******************************************************************************/
INT32 lbdt_recovery_time_out_trigger_mfmm(INT32 timerId, LBDT_CTRL_DATA_S *portData);

/*******************************************************************************
 *
 *  Function:    lbdt_down_time_out_set_phy_admin
 * 
 *  Description: The function enable/disable UNI port 
 *                 
 *  Returns:     STATUS
 *
 ******************************************************************************/
INT32 lbdt_down_time_out_set_phy_admin(INT32 timerId,LBDT_CTRL_DATA_S *portInfo);

/*******************************************************************************
 *
 *  Function:    lbdt_vlan_information_update_control
 * 
 *  Description: The function set vlan updating information
 *                 
 *  Returns:     INT32
 *
 ******************************************************************************/
INT32 lbdt_vlan_information_update_control(UINT32 portId, BOOL vlanUpdate);

/******************************************************************************
 *
 * Function   : lbdt_loop_send_pkt_save
 *
 * Description: This function save packet 
 *
 *
 * Returns    : INT32
 *
 ******************************************************************************/
INT32 lbdt_loop_send_pkt_save(UINT32 portIndex);

/******************************************************************************
 *
 * Function   : lbdt_loop_send_pkt_clear
 *
 * Description: This function clear packet 
 *
 *
 * Returns    : INT32
 *
 ******************************************************************************/
INT32 lbdt_loop_send_pkt_clear(UINT32 portIndex);

/******************************************************************************
 *
 * Function   : lbdt_loop_send_packet_reset_timer
 *
 * Description: This function reset send packet timer.
 *
 *
 * Returns    : void
 *
 ******************************************************************************/
void lbdt_loop_send_packet_reset_timer( );

/******************************************************************************
 *
 * Function   : lbdt_loop_send_pkt_reset_fmm
 *
 * Description: This function reset packet operation.
 *
 *
 * Returns    : INT32
 *
 ******************************************************************************/

INT32 lbdt_loop_send_pkt_reset_fmm( );

#endif /*__UNI_LBDT_H__*/

