/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : UNI loop detection                                        **/
/**                                                                          **/
/**  FILE        : lbdt_log.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : Definition of UNI loop detect log module                  **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *                                                                               
 *         Victor  - initial version created.   14/March/2011          
 *                                                                              
 ******************************************************************************/

#ifndef __LBDT_LOG_H__
#define __LBDT_LOG_H__

#define LBDT_LOG_MODULE  "LBDT"

typedef enum
{
    LBDT_LOG_NONE = 0x00,
    LBDT_LOG_ERROR,        
    LBDT_LOG_ALARM, /*such alarm not importance*/
    LBDT_LOG_INFO , /*informat ,such as pkt received*/
    LBDT_LOG_DEBUG,
    LBDT_LOG_DEBUG_PKT,
}LBDT_LOG_SEVERITY_E;

typedef enum
{
    LBDT_LOG_TO_FILE = 0x00 ,
    LBDT_LOG_TO_STDOUT ,
    LBDT_LOG_TO_CLI
}LBDT_LOG_TERMINAL_E;


BOOL  lbdt_check_if_initialized();
void  lbdt_log_printf(const char * func_name, LBDT_LOG_SEVERITY_E level,const char * format, ...);
INT32 lbdt_log_init();

#endif /*__LOOP_DETECT_LOG_H__*/
