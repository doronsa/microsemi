/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : lbdt                                                      **/
/**                                                                          **/
/**  FILE        : lbdt_cli_cmd_list_file.h                                   **/
/**                                                                          **/
/**  DESCRIPTION : This file has the CLISH action/routine matchup            **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 * 
 ******************************************************************************/

/********************************************************************************/
/*                            lbdt  debug CLI functions                           */
/********************************************************************************/
/********************************************************************************/
/*                            lbdt functions                        */
/********************************************************************************/        
    {"lbdt_print_data_info",   Lbdt_print_data_info}, 
    {"lbdt_set_log_level",   Lbdt_set_log_level}, 
    {"lbdt_set_ethernet_type",  Lbdt_set_ethernet_type},   
    {"lbdt_set_pkt_interval",   Lbdt_set_pkt_interval}, 
    {"lbdt_set_recovery_uni_interval",   Lbdt_set_recovery_uni_interval}, 
    {"lbdt_set_lbdt_manage",  Lbdt_set_lbdt_manage},
    {"lbdt_set_port_shut_down_option",   Lbdt_set_port_shut_down_option}, 
    {"lbdt_tag_del",   Lbdt_tag_del}, 
    {"lbdt_tag_add",  Lbdt_tag_add}, 
    
