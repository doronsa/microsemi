/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : UNI loop detection                                        **/
/**                                                                          **/
/**  FILE        : lbdt_api.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : Definition of log module                                  **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Thomas.wang  - initial version created.   1/Dev./2011           
 *                                                                              
 ******************************************************************************/

#ifndef __LBDT_API_H__
#define __LBDT_API_H__
#include "lbdt_main.h"





/*******************************************************************************
 *
 *  Function:    lbdt_get_alarm
 * 
 *  Description: Get loop detection alarm. 
 *                 
 *  Returns:     
 *  loop_alarm : Loop detect bitmap of all UNI port, bit0:UNI0,...bit3:UNI3.
 *               1:loopback detected, 0:no loopback
 *
 ******************************************************************************/
INT32  lbdt_get_alarm(UINT32 *loop_alarm);

/*******************************************************************************
 *
 *  Function:    lbdt_port_tag_information_set
 * 
 *  Description: synchronize loop back data structure with OMCI and OAM 
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_port_tag_information_set(UINT32 portId, LBDT_TAG_INFO_S *portTagInfo);

/*******************************************************************************
 *
 *  Function:    lbdt_port_tag_information_del
 * 
 *  Description: synchronize loop back data structure with OMCI and OAM 
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_port_tag_information_del(UINT32 portId);

/*******************************************************************************
 *
 *  Function:    lbdt_packet_interval_set
 * 
 *  Description: synchronize loop back data structure with OMCI and OAM 
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_packet_interval_set(UINT32 pktInter);

/*******************************************************************************
 *
 *  Function:    lbdt_recovery_interval_time_set
 * 
 *  Description: synchronize loop back data structure with OMCI and OAM 
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_recovery_interval_time_set(UINT32 recIntervalTime);


/*******************************************************************************
 *
 *  Function:    lbdt_port_manage_set
 * 
 *  Description: synchronize loop back data structure with OMCI and OAM 
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_port_manage_set(UINT32 portId, UINT16 portManage);


/*******************************************************************************
 *
 *  Function:    lbdt_port_down_option_set
 * 
 *  Description: synchronize loop back data structure with OMCI and OAM 
 *                 
 *  Returns:     LBDT_OK
 *
 ******************************************************************************/
INT32 lbdt_port_down_option_set(UINT32 portId, UINT16 portDownOpt);


/******************************************************************************
 *
 * Function   : lbdt_configure_log_level
 *              
 * Description: This function set  log print right level
 *              
 * Parameters : level: print control level 
 *
 * Returns    : LOOP_DETECT_OK
 *              
 ******************************************************************************/
INT32 lbdt_configure_log_level(INT32 level);

/******************************************************************************
 *
 * Function   : lbdt_configure_ethernet_type
 *              
 * Description: This function set  log print right level
 *              
 * Parameters : ethernetType: ethernet type.
 *
 * Returns    : LOOP_DETECT_OK
 *              
 ******************************************************************************/
INT32 lbdt_configure_ethernet_type(UINT16 ethernetType);

/******************************************************************************
 *
 * Function   : lbdt_printf_loop_back_data
 *              
 * Description: This function print loop back data structure
 *              
 * Parameters :  
 *
 * Returns    : LOOP_DETECT_OK
 *              
 ******************************************************************************/
INT32 lbdt_printf_loop_back_data();

#endif /*__UNI_LOOP_DETECT_H__*/

