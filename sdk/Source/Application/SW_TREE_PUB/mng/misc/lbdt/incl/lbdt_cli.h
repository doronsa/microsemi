/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON OAM stack                                            **/
/**                                                                          **/
/**  FILE        : lbdt_cli.h                                                 **/
/**                                                                          **/
/**  DESCRIPTION : Definition of lbdt CLI                                     **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    12/Jan/2011   - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __LBDT_CLI_H__
#define __LBDT_CLI_H__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>
#include "clish/shell.h"


BOOL  Lbdt_print_data_info (const clish_shell_t    *instance, const lub_argv_t  *argv);

BOOL  Lbdt_set_log_level (const clish_shell_t    *instance, const lub_argv_t  *argv);

BOOL  Lbdt_set_ethernet_type (const clish_shell_t    *instance,  const lub_argv_t  *argv);

BOOL  Lbdt_set_pkt_interval (const clish_shell_t    *instance,const lub_argv_t       *argv);

BOOL  Lbdt_set_recovery_uni_interval (const clish_shell_t    *instance, const lub_argv_t       *argv);

BOOL  Lbdt_set_lbdt_manage (const clish_shell_t    *instance, const lub_argv_t       *argv);

BOOL  Lbdt_set_port_shut_down_option(const clish_shell_t    *instance, const lub_argv_t       *argv);

BOOL  Lbdt_tag_del(const clish_shell_t    *instance, const lub_argv_t       *argv);

BOOL  Lbdt_tag_add(const clish_shell_t    *instance, const lub_argv_t       *argv);

#endif /*__LOOP_DETECT_CLI_H__*/
