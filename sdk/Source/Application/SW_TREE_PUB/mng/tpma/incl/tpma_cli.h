/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************** 
*      tpma_cli.h                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:  AyaL                                                    
*                                                                                
* DATE CREATED: June 9, 2010     
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.1.1.1 $                                                           
*******************************************************************************/

#ifndef __TPMA_CLI_H__
#define __TPMA_CLI_H__

/* Include Files
------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"

/* Definitions
------------------------------------------------------------------------------*/
/* Enums                              
------------------------------------------------------------------------------*/ 

/* Structs
------------------------------------------------------------------------------*/

/* Global variables
------------------------------------------------------------------------------*/


/* Global functions
------------------------------------------------------------------------------*/

/* Prototypes
------------------------------------------------------------------------------*/  

/********************************************************************************/
/*                      TPMA CLI functions                                      */
/********************************************************************************/
bool_t tpma_cli_cmd_print_l2_omci_key           (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l2_omci_key_all       (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l2_destination        (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l2_dest_all           (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l2_modification       (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l2_mod_all            (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l2_l2key              (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l2_l2_key_all         (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l2_by_bridge          (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l2_chunk              (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l2_all                (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l3_by_bridge          (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l3_chunk              (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l3_all                (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l4_all                (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_add_omci_key                    (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_add_l2_dest                     (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_add_l2_mod                      (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_add_l2Key_mac                   (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_add_l2Key_vlan1                 (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_add_l2Key_vlan2                 (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_add_l2Key_etherType             (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_add_l2Key_PPPoE                 (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_add_l2Key_gem                   (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_add_l2_rule                     (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_sync_db                     (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_init                        (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_clear_bridge_rules          (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_remove_by_handle            (const clish_shell_t *instance, const lub_argv_t *argv);
bool_t tpma_cli_cmd_print_l2_rules_direction    (const clish_shell_t *instance, const lub_argv_t *argv);




#endif
