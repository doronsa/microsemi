/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

  {"tpma_cli_cmd_print_l2_omci_key"         ,tpma_cli_cmd_print_l2_omci_key},
  {"tpma_cli_cmd_print_l2_destination"      ,tpma_cli_cmd_print_l2_destination},
  {"tpma_cli_cmd_print_l2_modification"     ,tpma_cli_cmd_print_l2_modification},
  {"tpma_cli_cmd_print_l2_l2key"            ,tpma_cli_cmd_print_l2_l2key},
  {"tpma_cli_cmd_print_l2_mod_all"          ,tpma_cli_cmd_print_l2_mod_all},
  {"tpma_cli_cmd_print_l2_dest_all"         ,tpma_cli_cmd_print_l2_dest_all},
  {"tpma_cli_cmd_print_l2_omci_key_all"     ,tpma_cli_cmd_print_l2_omci_key_all},
  {"tpma_cli_cmd_print_l2_l2_key_all"       ,tpma_cli_cmd_print_l2_l2_key_all},
  {"tpma_cli_cmd_print_l2_by_bridge"        ,tpma_cli_cmd_print_l2_by_bridge},
  {"tpma_cli_cmd_print_l2_chunk"            ,tpma_cli_cmd_print_l2_chunk},
  {"tpma_cli_cmd_print_l2_all"              ,tpma_cli_cmd_print_l2_all},
  {"tpma_cli_cmd_print_l2_rules_direction"  ,tpma_cli_cmd_print_l2_rules_direction},
  {"tpma_cli_cmd_print_l3_by_bridge"        ,tpma_cli_cmd_print_l3_by_bridge},
  {"tpma_cli_cmd_print_l3_chunk"            ,tpma_cli_cmd_print_l3_chunk},
  {"tpma_cli_cmd_print_l3_all"              ,tpma_cli_cmd_print_l3_all},
  {"tpma_cli_cmd_print_l4_all"              ,tpma_cli_cmd_print_l4_all},
  {"tpma_cli_add_omci_key"                  ,tpma_cli_add_omci_key},
  {"tpma_cli_add_l2_dest"                   ,tpma_cli_add_l2_dest},
  {"tpma_cli_add_l2_mod"                    ,tpma_cli_add_l2_mod},
  {"tpma_cli_add_l2Key_mac"                 ,tpma_cli_add_l2Key_mac},
  {"tpma_cli_add_l2Key_vlan1"               ,tpma_cli_add_l2Key_vlan1},
  {"tpma_cli_add_l2Key_vlan2"               ,tpma_cli_add_l2Key_vlan2},
  {"tpma_cli_add_l2Key_etherType"           ,tpma_cli_add_l2Key_etherType},
  {"tpma_cli_add_l2Key_PPPoE"               ,tpma_cli_add_l2Key_PPPoE},
  {"tpma_cli_add_l2Key_gem"                 ,tpma_cli_add_l2Key_gem},
  {"tpma_cli_add_l2_rule"                   ,tpma_cli_add_l2_rule},
  {"tpma_cli_cmd_sync_db"                   ,tpma_cli_cmd_sync_db},
  {"tpma_cli_cmd_init"                      ,tpma_cli_cmd_init},
  {"tpma_cli_cmd_clear_bridge_rules"        ,tpma_cli_cmd_clear_bridge_rules},
  {"tpma_cli_cmd_remove_by_handle"          ,tpma_cli_cmd_remove_by_handle},
