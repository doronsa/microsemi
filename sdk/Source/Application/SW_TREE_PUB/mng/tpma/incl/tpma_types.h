/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _TPMA_TYPES_H_
#define _TPMA_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif


/* Include Files
------------------------------------------------------------------------------*/

#include "tpm_types.h"


/* Definitions
------------------------------------------------------------------------------*/

#define   PARSECOMMAND_L2_MAC_DEST         TPM_L2_PARSE_MAC_DA         /* Parse destination MAC address*/
#define   PARSECOMMAND_L2_MAC_SRC          TPM_L2_PARSE_MAC_SA         /* Parse source MAC address*/
#define   PARSECOMMAND_L2_1VLAN_TAG        TPM_L2_PARSE_ONE_VLAN_TAG   /* Parse outer VLAN tag*/
#define   PARSECOMMAND_L2_2VLAN_TAG        TPM_L2_PARSE_TWO_VLAN_TAG   /* Parse outer and inner VLAN tag*/
#define   PARSECOMMAND_L2_ETH_TYPE         TPM_L2_PARSE_ETYPE          /* Parse Ethernet type*/
#define   PARSECOMMAND_L2_PPPoE_SESSION    TPM_L2_PARSE_PPPOE_SES      /* Parse PPPoE session*/
#define   PARSECOMMAND_L2_PPP_PROTOCOL     TPM_L2_PARSE_PPP_PROT       /* Parse PPP protocol*/
#define   PARSECOMMAND_L2_GEM_PORT         TPM_L2_PARSE_GEMPORT        /* Parse GEM port */
#define   PARSECOMMAND_IPv4_SRC            TPM_IPv4_PARSE_SIP          /* Parse IPv4 source address */
#define   PARSECOMMAND_IPv4_DEST           TPM_IPv4_PARSE_DIP          /* Parse IPv4 destination address */
#define   PARSECOMMAND_IPv6_SRC            TPM_IPv6_PARSE_SIP          /* Parse IPv6 source address */
#define   PARSECOMMAND_IPv6_DEST           TPM_IPv6_PARSE_DIP          /* Parse IPv6 destination address */
#define   PARSECOMMAND_IPv6_DSCP           TPM_IPv6_PARSE_DSCP         /* Parse IPv6 DSCP*/
#define   PARSECOMMAND_IPv6_NEXT_HOP       TPM_IPv6_PARSE_NH           /* Parse IPv6 next hop*/
#define   PARSECOMMAND_IPv6_L4_SRC_PORT    TPM_IPv6_PARSE_L4_SRC       /* Parse IPv6 L4 source port*/
#define   PARSECOMMAND_IPv6_L4_DEST_PORT   TPM_IPv6_PARSE_L4_DEST      /* Parse IPv6 L4 destination port */
#define   PARSECOMMAND_TCP_FLAGS           TPM_TCP_PARSE_FLAGS         /* Parse TCP field*/
#define   PARSECOMMAND_L2_UNTAGGED         (0x0100)                    /* Parse by untagged frames - US ONLY */
#define   PARSECOMMAND_L2_TAGGED           (0x0200)                    /* Parse by tagged frames - US ONLY */

/*bit map to construct pattern for length valn distribution*/
#define  TPMA_INNER_VLAN_PBIT           (0x1)                           /*only inner pbit is explicitly set (*,*,;*,pb)*/
#define  TPMA_INNER_VLAN_VID            (0x2)                           /*only inner vid is explicitly set  (*,*,;vid,*)*/
#define  TPMA_OUTER_VLAN_PBIT           (0x4)                           /*only outer pbit is explicitly set (*,pb,;*,*)*/ 
#define  TPMA_OUTER_VLAN_VID            (0x8)                           /*only outer vid is explicitly set  (vid,*,;*,*)*/ 

/*bit map vlan patterns for length distribution */
#define TPMA_nothing_SET                         (0x0)                  /*nothing is explicitly set                                     (*,*,;*,*)*/
#define TPMA_ONLY_INNER_PBIT_SET                 (0x1)                  /*only inner pbit is explicitly set                             (*,*,;*,pb)*/
#define TPMA_ONLY_INNER_VID_SET                  (0x2)                  /*only inner vid is explicitly set                              (*,*,;vid,*)*/ 
#define TPMA_ONLY_INNER_VID_INNER_PBIT_SET       (0x3)                  /*only inner vid AND inner pbit are explicitly set              (*,*,;vid,pb)*/
#define TPMA_ONLY_OUTER_PBIT_SET                 (0x4)                  /*only outer pbit is explicitly set                             (*,pb,;*,*)*/
#define TPMA_ONLY_OUTER_INNER_PBIT_SET           (0x5)                  /*only outer AND inner pbit are explicitly set                  (*,pb,;*,pb)*/  
#define TPMA_ONLY_INNER_VID_OUTER_PBIT_SET       (0x6)                  /*only inner vid AND outer pbit are explicitly set              (*,pb,;vid,*)*/ 
#define TPMA_ONLY_OUTER_PBIT_INNER_VID_PBIT_SET  (0x7)                  /*only outer pbit AND inner vid AND pbit is explicitly set      (*,pb,;vid,pb)*/
#define TPMA_ONLY_OUTER_VID_SET                  (0x8)                  /*only outer vid is explicitly set                              (vid,*,;*,*)*/ 
#define TPMA_ONLY_OUTER_VID_INNER_PBIT_SET       (0x9)                  /*only outer vid AND inner pbit are explicitly set              (vid,*,;*,pb)*/ 
#define TPMA_ONLY_OUTER_VID_INNER_VID_SET        (0xA)                  /*only outer vid AND inner vid is explicitly set                (vid,*,;vid,*)*/
#define TPMA_ONLY_OUTER_VID_INNER_VID_PBIT_SET   (0xB)                  /*only outer vid AND inner vid AND pbit is explicitly set       (vid,*,;vid,pb)*/
#define TPMA_ONLY_OUTER_VID_OUTER_PBIT_SET       (0xC)                  /*only outer vid AND outer pbit is explicitly set               (vid,pb,;*,*)*/  
#define TPMA_ONLY_OUTER_VID_PBIT_INNER_PBIT_SET  (0xD)                  /*only outer vid AND outer pbit is explicitly set               (vid,pb,;*,pb)*/  
#define TPMA_ONLY_OUTER_VID_PBIT_INNER_VID_SET   (0xE)                  /*only outer vid AND outer pbit is explicitly set               (vid,pb,;vid,*)*/  
#define TPMA_all_SET                             (0xF)

/*protocol constatnts*/
#define TPMA_VLAN_ETHER_TYPE_DEFAULT             (0x8100)   
#define TPMA_IPv4_ETHER_TYPE                     (0x0800)
#define TPMA_IPv6_ETHER_TYPE                     (0x86DD)



#define TPMA_OMCC_PORT_INIT_VALUE (-1)


/* Enums                              
------------------------------------------------------------------------------*/ 


typedef enum 
{
    TPMAPROC_OK                             = TPM_RC_OK,                          /*Return ok (=0).*/
    TPMAPROC_INVALID_OWNER                  = ERR_OWNER_INVALID,                  /*Illegal owner id.*/    
    TPMAPROC_INVALID_SRCPORT                = ERR_SRC_PORT_INVALID,               /*Illegal source port.*/                                   
    TPMAPROC_INVALID_RULENUM                = ERR_RULE_NUM_INVALID,               /*Illegal rule number.*/                                    
    TPMAPROC_INVALID_PARSEMAP               = ERR_PARSE_MAP_INVALID,              /*Illegal parse mapping.*/  
    TPMAPROC_INVALID_VLANOP                 = ERR_VLAN_OP_INVALID,                /*Illegal VLAN operation.*/                                    
    TPMAPROC_INVALID_L2KEY                  = ERR_L2_KEY_INVALID,                 /*Illegal key information for creating the ACL rule.*/                                     
    TPMAPROC_INVALID_L2FRWD                 = ERR_FRWD_INVALID,                   /*Illegal forwarding decision for packet.*/    
    TPMAPROC_INVALID_L2MOD                  = ERR_MOD_INVALID,                    /*Illegal modification command on packet.*/                   
    TPMAPROC_INVALID_L2ACTION               = ERR_ACTION_INVALID,                 /*Illegal action on the packet.*/                 
    TPMAPROC_INVALID_NEXTPHASE              = ERR_NEXT_PHASE_INVALID,             /*Illegal next phase for primary ACL*/                             
    TPMAPROC_MISMATCH_KEYRULENUM            = ERR_RULE_KEY_MISMATCH,              /*Inconsistency between rule_num, src_port, l2_acl_key.*/                                     
    TPMAPROC_IPv4_NO_CONT_L4                = ERR_IPV4_NO_CONT_L4,                /*There is no continuous block with L4 src port and destination port in packet.*/
    TPMAPROC_IPv4_MISMATCH_KEYRULENUM       = ERR_RULE_KEY_MISMATCH,              /*Inconsistency between rule_num, src_port and ipv4_key*/
    TPMAPROC_IPv6_ADDFAIL                   = ERR_IPV6_ADD_FAIL,                  /*Inconsistency in the building of the IPv6 rule.*/
    TPMAPROC_IPv6_MISMATCH_KEYRULENUM       = ERR_RULE_KEY_MISMATCH,              /*Inconsistency between rule_num, src_port and ipv6_key*/
    TPMAPROC_INVALID_MCSREAMNUM             = ERR_MC_STREAM_INVALID,              /*Illegal stream number.*/
    TPMAPROC_INVALID_MCDESTPORTS            = ERR_MC_DST_PORT_INVALID,            /*Destination port bitmap does not match the UNI ports.*/
    TPMAPROC_IPv4_INVALID_MCDESTIP          = ERR_IPV4_MC_DST_IP_INVALID,         /*Destination IP address is not in the MC range.*/
    TPMAPROC_IPv6_INVALID_MCDESTIP          = ERR_IPV6_MC_DST_IP_INVALID,         /*Destination IPv6 address is not in the MC range.*/
    TPMAPROC_INVALID_OMCI_TCONT             = ERR_OMCI_TCONT_INVALID,             /*Illegal TCONT.*/
    TPMAPROC_INVALID_MNG_TX_Q               = ERR_MNGT_TX_Q_INVALID,              /*Illegal management TX queue.*/
    TPMAPROC_MNG_CREATE_DUPLICATE_CHANNEL   = ERR_MNGT_CREATE_DUPLICATE_CHANNEL,  /*Create channel that already exists,*/
    TPMAPROC_MNG_INVALID_DEL_CHANNEL        = ERR_MNGT_DEL_CHANNEL_INVALID,       /*Delete an unexisting management channel.*/
    TPMAPROC_OAM_INVALID_LLID               = ERR_OAM_LLID_INVALID,               /*Illegal LLID.*/
    TPMAPROC_CFG_INVALID_GETNEXT_INDEX      = ERR_CFG_GETNEXT_INDEX_INVALID,      /*Illegal get_next index for retrieving existing configuration.*/
    TPMAPROC_CFG_INVALID_GETNEXT_DIRECTION  = ERR_CFG_GETNEXT_DIRECTION_INVALID,  /*Illegal direction set (0-upstream, 1-downstream).*/
    TPMAPROC_CFG_INVALID_GET_CHANNEL        = ERR_CFG_GET_CHANNEL_INVALID,        /*Get an unexisting management channel.*/
    TPMAPROC_SW_INVALID_MAC                 = ERR_SW_MAC_INVALID,                 /*Illegal MAC address.*/
    TPMAPROC_SW_MAC_STATIC_NOT_FOUND        = ERR_SW_MAC_STATIC_NOT_FOUND,        /*Delete an unexisting static MAC.*/
    TPMAPROC_SW_INVALID_MAC_PER_PORT        = ERR_SW_MAC_PER_PORT_INVALID,        /*Illegal limit of MAC per port (1-265).*/
    TPMAPROC_SW_INVALID_NUM_OF_MAC_PER_PORT = ERR_SW_NUM_OF_MAC_PER_PORT_INVALID, /*Illegal limit of MAC per port (1-265).*/
    TPMAPROC_SW_INVALID_VID                 = ERR_SW_VID_INVALID,                 /*Illegal VLAN ID.*/
    TPMAPROC_SW_TM_INVALID_Q                = ERR_SW_TM_QUEUE_INVALID,            /*Illegal queue id.*/
    TPMAPROC_SW_TM_INVALID_WEIGHT           = ERR_SW_TM_WEIGHT_INVALID,           /*Illegal weight value.*/
    TPMAPROC_SW_TM_INVALID_WRR_MODE         = ERR_SW_TM_WRR_MODE_INVALID,         /*Illegal WRR mode.*/
    TPMAPROC_GENERROR                       = ERR_GENERAL,                        /*General purpose error.*/
    TPMAPROC_NOT_FOUND,                                                           /*rule was not found in the database */
    TPMAPROC_DATABASE_FULL,                                                       /*database is full - can NOT add another entry*/
    TPMAPROC_MATCH_NOT_SUPPORTED,                                                 /*parse map match and action are not supported*/  
    TPMAPROC_DELETE_FAILED,                                                       /*delete operation failed*/  
    TPMAPROC_ALREADY_EXIST                                                        /*entry already exists*/  
}tpma_proc_status_t;
                                                           

typedef enum 
{
    TPMA_COONECTION_CLOSED = 0,
    TPMA_COONECTION_OPEN
}tpma_connection_type_t;

/*Other then multicast, all traffic is either upstream or downstream
 I keep 2 databases for each section (l2, l3...) 
 One for upstream rules and the other for downstream rules.*/
typedef enum 
{
    TPMA_DIRECTION_DS = 0,
    TPMA_DIRECTION_US
}tpma_direction_t;

/*each rule is assigned a color black - drop or white - allow
 when setting 1 tag vlan rule, the color of the added rule effects 
 the color of the 'any' 1 tag rule - IT MUST BE OPPOSITE COLORS*/
typedef enum 
{
    TPMA_PCKT_ACTION_DROP,
    TPMA_PCKT_ACTION_ALLOW
}tpma_pckt_action_t;

/*when setting a new rule in a group (same source port) in l2 section, 
 3 new rules are added in the layer 3 section. 
 in order to continure distribution for other layers (IPv4 vs. IPv6)
 and from there to continue distribution to multicast*/
typedef enum 
{
    TPMA_L3_RULE_TYPE_IPv4,
    TPMA_L3_RULE_TYPE_IPv6,
    TPMA_L3_RULE_TYPE_DONE
}tpma_l3_rule_type_t;

/*l2 rules are sorted by length, 
rules with much spesific data are longer then rules with less explicit data*/
typedef enum{
    TPMA_LENGTH_NO_TAG = 0,
    TPMA_LENGTH_NO_TAG_GEM,
    TPMA_LENGTH_ANY_ONE_TAG,
    TPMA_LENGTH_ANY_ONE_TAG_GEM,
    TPMA_LENGTH_ONE_TAG_ANY_VID,
    TPMA_LENGTH_ONE_TAG_ANY_PBIT,
    TPMA_LENGTH_ONE_TAG,
    TPMA_LENGTH_ANY_TWO_TAG,
    TPMA_LENGTH_ANY_TWO_TAG_GEM,
    TPMA_LENGTH_TWO_TAG_SINGLE_PBIT_SET,
    TPMA_LENGTH_TWO_TAG_SINGLE_VID_SET,
    TPMA_LENGTH_TWO_TAG_TWO_PBIT_SET,
    TPMA_LENGTH_TWO_TAG_SINGLE_VID_SINGLE_PBIT_SET,
    TPMA_LENGTH_TWO_TAG_TWO_VID_SET,
    TPMA_LENGTH_TWO_TAG_TWO_PBIT_SINGLE_VID_SET,
    TPMA_LENGTH_TWO_TAG_TWO_VID_SINGLE_PBIT_SET,
    TPMA_LENGTH_TWO_TAG,
    TPMA_LENGTH_TWO_MAC
}tpma_length_t;
/* Structs
------------------------------------------------------------------------------*/

/* IP key structure */
typedef struct
{
   uint8_t      ip[4];
} tpma_ip_t;


/*hold bitmap of parsed field*/
typedef uint32_t tpma_parse_command_t;

/*Each L2 record is uniquely identified by:
             bridge ID, 
             source TP , 
             destination TP 
             and a TPMA handler set to a record on add event. 
  Bridge ID, ANI TP and UNI TP are constructed :
  #define MAKE_TPKEY(tpClass,tpInst) (((tpClass) & 0xFFFF) << 16) | (tpInst & oxFFFF))
 */
typedef struct
{
    uint32_t     bridge_id;
    uint32_t     source_tp;
    uint32_t     destination_tp;
} tpma_l2_key_t;

/*
each l2 rule has its:
     omci identifier
     tpm identifier
     tpm actual rule position in the PnC
     tpm parameters that are needed for deletion:
        source port in the tpm enumeration
        fields to parse 
        values top match
     rule action -  color black vs. white
     rule length that determines its position in the group
*/
typedef struct
{
    tpma_l2_key_t           key;
    uint32_t                idx;
    uint32_t                rule_number;
    tpm_src_port_type_t     src_port;
    tpma_parse_command_t    parse_rule_bm;
	tpm_l2_acl_key_t        l2_key;
    tpm_pkt_frwd_t          pkt_frwd;
    tpma_pckt_action_t      rule_action;
    tpma_length_t           rule_length;
} tpma_l2_rule_t;

typedef struct
{
    tpma_l2_key_t       key;
    uint32_t            idx;
    uint32_t            rule_number;
    tpm_src_port_type_t src_port;
    tpm_parse_fields_t  parse_rule_bm;
	tpm_l3_type_key_t   l3_key;
    tpma_l3_rule_type_t rule_type;
} tpma_l3_rule_t;

typedef struct
{
    uint32_t    vlan_id;
    tpma_ip_t   IPv4_src_add;
    tpma_ip_t   IPv4_dest_add;
} tpma_IPv4_mc_key_t;

typedef struct
{
    tpma_IPv4_mc_key_t  key;
    uint32_t            stream_number;
} tpma_IPv4_mc_rule_t;



/* Global variables
------------------------------------------------------------------------------*/

/* Global functions
------------------------------------------------------------------------------*/

/* Prototypes
------------------------------------------------------------------------------*/  

#ifdef __cplusplus
}
#endif

#endif

