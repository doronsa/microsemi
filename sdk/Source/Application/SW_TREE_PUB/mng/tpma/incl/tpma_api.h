/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _TPMA_APIS_H_
#define _TPMA_APIS_H_

#ifdef __cplusplus
extern "C" {
#endif


#include "tpma_types.h"

/******************************************************************************/
/********************************** Data Forwarding APIs **********************/
/******************************************************************************/

/*******************************************************************************
* tpma_add_l2_US()
*
* DESCRIPTION:      Verify there is room for another rule in the l2US section
*                   Search US database for rules with the same serviceKey i.e. bridge id and source port id 
*                   If found, insert first in this group, match rule number.
*                            call tpm_add_l2_prim_acl_rule
*               	Otherwise (first rule per bridge id and source port id):
*                       if rule contains a filter, verify room for 2 rules
*                       call tpm_add_l2_prim_acl_rule (new rule)
*                       create another rule with the opposite action without a filter.
*                       if rule doesn't contain a filter just add it
*
* INPUTS:
* *service_key       - Pointer holding identifier for
*                                 bridge, 
*                                 source UNI TP and 
*                                 destination ANI TP 
*                      in a format of ME class ID concutenated with instance ID
* src_port           - UNI TP from tpm_src_port_type_t enumewration
* parse_cmd_bm       - Bitmap containing the significant flags for parsing fields of the packet
* *parse_values      - Information to create a parsing key for the rule. 
*                      Some pointers may be NULL depending on the parseCmd_bm
* *dest              - ANI TP: target port, target queue and GEM port
* *mod               - Information for packet modification 
* action             - Bitmap containing the actions to preform on the packet
*
* OUTPUTS:
* *tpma_handle       - rule identifier .
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
* Rule with action different then drop are marked to move to next stage to L3
*
*******************************************************************************/
 
tpma_proc_status_t tpma_add_l2_US   (tpma_l2_key_t 	        *service_key, 
                                     tpm_src_port_type_t	src_port,
                                     uint32_t		        parse_cmd_bm,
                                     tpm_l2_acl_key_t       *parse_values,
                                     tpm_pkt_frwd_t	        *dest,
                                     tpm_pkt_mod_t	        *mod,
                                     tpm_pkt_action_t       action,
                                     uint32_t		        *tpma_handle);

/*******************************************************************************
* tpma_add_l2_DS()
*
* DESCRIPTION:      Verify there is room for another rule in the l2US section
*                   Search DS database for rules with the same serviceKey i.e. bridge id and source port id 
*                   If found, insert first in this group, match rule number.
*                            call tpm_add_l2_prim_acl_rule
*               	Otherwise (first rule per bridge id and source port id):
*                       if rule contains a filter, verify room for 2 rules
*                       call tpm_add_l2_prim_acl_rule (new rule)
*                       create another rule with the opposite action without a filter.
*                       if rule doesn't contain a filter just add it
*
* INPUTS:
* *service_key       - Pointer holding identifier for
*                                 bridge, 
*                                 source ANI TP and 
*                                 destination UNI TP 
*                      in a format of ME class ID concutenated with instance ID
* src_port           - ANI TP from tpm_src_port_type_type_t enumewration
* parse_cmd_bm       - Bitmap containing the significant flags for parsing fields of the packet
* *parse_values      - Information to create a parsing key for the rule. 
*                      Some pointers may be NULL depending on the parseCmd_bm
* *dest              - UNI TP: target port: ANY UNI port
* *mod               - Information for packet modification 
* action             - Bitmap containing the actions to preform on the packet
*
* OUTPUTS:
* *tpma_handle       - rule identifier .
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.*
* COMMENTS:
* Rule with action different then drop are marked to move to next stage to L3
*
*******************************************************************************/
 
tpma_proc_status_t tpma_add_l2_DS (tpma_l2_key_t          *service_key,  
                                   tpm_src_port_type_t    src_port,      
                                   uint32_t               parse_cmd_bm,  
                                   tpm_l2_acl_key_t       *parse_values, 
                                   tpm_pkt_frwd_t         *dest,         
                                   tpm_pkt_mod_t          *mod,          
                                   tpm_pkt_action_t       action,        
                                   uint32_t               *tpma_handle); 

/*******************************************************************************
* tpma_add_ipv4_MC()
*
* DESCRIPTION:      Search MC database for rules with the same key (VLAN ID, source IP and destination IP) 
*               	If found, get the stream number and try to update the bitmap of target ports. 
*                           call tpm_updt_ipv4_mc_stream
*               	Otherwise, check if there is more room in the database for another rule. 
*               	If so, find new rule number and try to set it (call tpm_add_ipv4_mc_stream). 
*
* INPUTS:
* vid	        - VLAN ID	0 - priority tag
*                           4096 - untagged
*                           0xFFFF - don't care 
* IPv4_src_add	- IPv4 source IP address in network order.	
*                           0.0.0.0 - if should be ignored
* IPv4_dest_add	- IPv4 destination IP address in network order.	
* dest_port_bm	 - bitmap which includes all destination UNI ports or destination ports with a loopback to CPU	
*
* OUTPUTS:
* *tpma_handle       - rule identifier .
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
* COMMENTS:
* Rule's order is not important 
*******************************************************************************/
tpma_proc_status_t tpma_add_ipv4_MC (uint32_t    vid, 
                                     tpma_ip_t   IPv4_src_add,
                                     tpma_ip_t   IPv4_dest_add,
                                     uint32_t    dest_port_bm,
                                     uint32_t    *tpma_handle);


/******************************************************************************/
/********************************** Delete APIs *******************************/
/******************************************************************************/

/*******************************************************************************
* tpma_reset_all()
*
* DESCRIPTION:  First, reset all ports configuration:
*                   Loop on all ports, for each call a TPM reset function depending on the port type (ANI, UNI)
*           	Second, clear PnC:
*               	Starting with index ==0 call to tpm_get_next_valid_rule on l2 (upstream and downstream). 
*                   Starting with index ==0 call to tpm_get_next_valid_rule on L4MC . 

*
* INPUTS:
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. On error, TPMAPROC_GENERROR is returned.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t   tpma_reset_all();

/*******************************************************************************
* tpma_remove_bridge()
*
* DESCRIPTION:  Search l2 database (both US and DS) for rules with matching bridge ID
*           	If found,
*           	Reset participating ports configuration (source and destination)
*           	Go through the DS, US database and find rule numbers to be removed, 
*               call  tpm_del_l2_prim_acl_rule (notice  that rules index updates as the rules are deleted)
*
* INPUTS:
* bridge_id	   - ME class concatenated with instance ID 
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. On error, TPMAPROC_GENERROR is returned.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_remove_bridge   (uint32_t bridge_id);

/*******************************************************************************
* tpma_remove_leg()
*
* DESCRIPTION:  Search l2 database (both US and DS) for rules with matching TP ID
*           	If found,
*           	Reset port configuration
*           	Go through the DS, US database and find rule numbers to be removed, 
*               call  tpm_del_l2_prim_acl_rule (notice  that rules index updates as the rules are deleted)
*
* INPUTS:
* bridge_id	   - ME class concatenated with instance ID 
* tp_id	       - ME class concatenated with instance ID 
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. On error, TPMAPROC_GENERROR is returned.
*
* COMMENTS:
* notice  that rules index updates as the rules are deleted
*******************************************************************************/
tpma_proc_status_t tpma_remove_leg   (uint32_t bridge_id ,uint32_t tp_id);

/*******************************************************************************
* tpma_remove_by_handler()
*
* DESCRIPTION:  Search US, DS and MC databases for rules with corresponding tpma_handler
*           	If found,
*           	Call  tpm_del_l2_prim_acl_rule or tpm_del_ipv4_mc_stream
*
* INPUTS:
* tpma_handler - rule idetifier 
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. On error, TPMAPROC_GENERROR is returned.
*
* COMMENTS:
* notice  that rules index updates as the rules are deleted
*******************************************************************************/
tpma_proc_status_t tpma_remove_by_handle   (uint32_t tpma_handle);

/******************************************************************************/
/********************************** ANI APIs **********************************/
/******************************************************************************/

/*******************************************************************************
* tpma_ani_queue_set_scheduling()
*
* DESCRIPTION:   Configures the scheduling mode per WAN queue. 
*                For strict priority configuration, the priority is hardcoded by the Queue number.
*                For WRR priority configuration, the queue weight is set
*                   Call to tpm_tm_set_wan_egr_queue_sched
* INPUTS:
*   tp       -	ANI entity: 	DestPrt_WAN, 
*                               DestPort_TCONT_0 - 7 (GPON) 
*                               DestPort_LLID_0 - 7 (EPON)
*   sched	 -  scheduler mode per port:	strict = 0x01
*                                           WRR    = 0x02
*   queue_id -	Queue 	0 - number of queues per TCON - 1
*   wrr_weight	The weight per queue when scheduling mode is WRR	1-256
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_ani_queue_set_scheduling (tpm_trg_port_type_t  tp, 
                                                  tpm_sw_sched_type_t  sched,
                                                  uint8_t              queue_id,
                                                  uint8_t              wrr_weight);


/*******************************************************************************
* tpma_ani_queue_reset_scheduling()
*
* DESCRIPTION:   Configures the scheduling mode per WAN queue back to startup values. 
*                   Call to tpm_tm_reset_wan_queue_sched
*
* INPUTS:
*   tp       -	ANI entity: 	DestPrt_WAN, 
*                               DestPort_TCONT_0 - 7 (GPON) 
*                               DestPort_LLID_0 - 7 (EPON)
*   queue_id -	Queue 	0 - number of queues per TCON - 1
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_ani_queue_reset_scheduling (tpm_trg_port_type_t   tp, 
                                                    uint8_t               queue_id);
                                            

/*******************************************************************************
* tpma_ani_set_egress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit of upstream traffic for 
*                upstream scheduling entity, - WAN port or TCONT or LLID. 
*                
*                When a queue_id is set to -1, call to tpm_tm_set_wan_sched_egr_rate_lim. 
*            	 Otherwise, call to tpm_tm_set_wan_queue_egr_rate_lim

* INPUTS:
*   tp       -	ANI entity: 	DestPrt_WAN, 
*                               DestPort_TCONT_0 - 7 (GPON) 
*                               DestPort_LLID_0 - 7 (EPON)
*   queue_id -	Queue 	        0 - number of queues per TCON - 1
*                               OR -1 for all queues related to this TCONT
*   rate_val	Rate limitation value	
*   bucket_size	Bucket size value	
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_ani_set_egress_rate_limit (tpm_trg_port_type_t  tp, 
                                                   int8_t               queue_id,
                                                   uint32_t             rate_val,
                                                   uint32_t             bucket_size);

/*******************************************************************************
* tpma_ani_reset_eggress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit of upstream back to startup values. 
*                
*                When a queue_id is set to -1, call to tpm_tm_reset_wan_sched_egr_rate_lim. 
*             	 Otherwise, call to tpm_tm_reset_wan_queue_egr_rate_lim


* INPUTS:
*   tp       -	ANI entity: 	DestPrt_WAN, 
*                               DestPort_TCONT_0 - 7 (GPON) 
*                               DestPort_LLID_0 - 7 (EPON)
*   queue_id -	Queue 	        0 - number of queues per TCON - 1
*                               OR -1 for all queues related to this TCONT
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_ani_reset_eggress_rate_limit (tpm_trg_port_type_t tp, 
                                                      int8_t          queue_id);

/*******************************************************************************
* tpma_ani_set_ingress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit on a specific queue for downstream traffic. 
*                
*                When a queue_id is set to -1, call to tpm_tm_set_wan_ingr_rate_lim. 
*            	 Otherwise, call to tpm_tm_set_wan_q_ingr_rate_lim

* INPUTS:
*   queue_id -	Queue 	        0 - number of queues per TCON - 1
*                               OR -1 all downstream traffic from the WAN
*   rate_val	Rate limitation value	
*   bucket_size	Bucket size value	
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_ani_set_ingress_rate_limit (int8_t       queue_id,
                                                    uint32_t     rate_val,
                                                    uint32_t     bucket_size);

/*******************************************************************************
* tpma_ani_reset_ingress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit on a specific queue back to startup values.  
*                
*                When a queue_id is set to -1, call to tpm_tm_reset_wan_ingr_rate_lim. 
*            	 Otherwise, call to tpm_tm_reset_wan_q_ingr_rate_lim


* INPUTS:
*   queue_id -	Queue 	        0 - number of queues per TCON - 1
*                               OR -1 all downstream traffic from the WAN
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_ani_reset_ingress_rate_limit (int8_t queue_id);

/******************************************************************************/
/********************************** UNI APIs **********************************/
/******************************************************************************/

/******************************************************************************/
/********************************** VLAN filtering  APIs **********************/
/******************************************************************************/

tpma_proc_status_t tpma_sw_set_port_tagged (tpm_src_port_type_t port,
                                            uint8_t             allow_tagged);

tpma_proc_status_t tpma_sw_set_port_untagged (tpm_src_port_type_t port,
                                              uint8_t             allow_untagged);

tpma_proc_status_t tpma_sw_port_add_vid (tpm_src_port_type_t port,
                                         uint16_t            vid);

tpma_proc_status_t tpma_sw_port_del_vid (tpm_src_port_type_t port,
                                         uint16_t            vid);

tpma_proc_status_t tpma_sw_port_set_vid_filter (tpm_src_port_type_t src_port,
                                                uint8_t             vid_filter);

/*******************************************************************************
* tpma_uni_queue_set_scheduling()
*
* DESCRIPTION:   Configures the scheduling mode per Ethernet port. 
*                With respect the value given in scheduling mode, 
*                   call to tpm_sw_set_uni_q_weight with weight values from weight_arr, 0 == unset. 
*                At any case generate a call to tpm_sw_set_uni_sched.

* INPUTS:
*   tp       -	uni port for configuring the scheduler type (limited to a specific UNI port)
*   sched	 -  0x0 -  WRR priority on ALL queues
*               0x1 - strict on Q3 and WRR on Q0-2
*               0x2 - strict on Q2-3 and WRR on Q0-1
*               0x3 - strict priority on ALL queues 
*   weight_arr	The weight per queue when scheduling mode is WRR	1-256, 0 == unset
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_uni_queue_set_scheduling (tpm_trg_port_type_t    tp, 
                                                  tpm_sw_sched_type_t    sched,
                                                  uint8_t  		         *weight_arr);



/*******************************************************************************
* tpma_uni_queue_reset_scheduling()
*
* DESCRIPTION:   Configures the scheduling mode per Ethernet port back to startup values. 
*                Call to tpm_tm_reset_uni_sched 
*                Followed by a call to tpm_sw_reset_uni_q_weight with all queues
*
* INPUTS:
*   tp       -	uni port for configuring the scheduler type (limited to a specific UNI port)
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_uni_queue_reset_scheduling (tpm_trg_port_type_t tp);

/*******************************************************************************
* tpma_uni_set_egress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit of an Ethernet UNI port. 
*                Call to tpm_sw_set_uni_egr_rate_limit 
*
* INPUTS:
*   uni_port   -	Related UNI port
*   rate_val   -	Rate limitation value
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_uni_set_egress_rate_limit (tpm_trg_port_type_t uni_port,
                                                   uint32_t            rate_val);

/*******************************************************************************
* tpma_uni_reset_egress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit of an Ethernet UNI port back to startup values.  
*                Call to tpm_sw_reset_uni_egr_rate_limit 
*
* INPUTS:
*   uni_port   -	Related UNI port
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_uni_reset_egress_rate_limit (tpm_trg_port_type_t uni_port);

/*******************************************************************************
* tpma_uni_set_ingress_rate_limit()
*
* DESCRIPTION:   Configures a policer function for a traffic class for an Ethernet UNI port. 
*                There are 4 globally defined traffic classes in the integrated switch. 
*                When tc < 0 
*               	call to tpm_sw_set_uni_ingr_police_rate
*            	 When tc > 0 
*               	call to tpm_sw_set_uni_tc_ingr_police_rate
*
* INPUTS:
*   uni_port   -	Related UNI port
*   cir	       -    Committed information rate
*   cbs	       -    Committed burst size
*   tc	       -    Traffic class
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_uni_set_ingress_rate_limit (tpm_trg_port_type_t uni_port,
                                                    uint32_t       cir,
                                                    uint32_t       cbs,
                                                    int32_t        tc);


/*******************************************************************************
* tpma_uni_reset_ingress_rate_limit()
*
* DESCRIPTION:   Configures a policer function for a traffic class for an Ethernet UNI port back to startup values.   
*
* INPUTS:
*   uni_port   -	Related UNI port
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_uni_reset_ingress_rate_limit (tpm_trg_port_type_t uni_port);

/*******************************************************************************
* tpma_uni_set_max_mac_depth()
*
* DESCRIPTION:   Limits the number of MAC addresses per port. 
*                call to tpm_sw_set_port_max_macs
*
* INPUTS:
*   uni_port     -	Related UNI port
*   mac_per_port -  Maximum number of MAC addresses per port 1-255
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_uni_set_max_mac_depth (tpm_trg_port_type_t  uni_port,
                                               uint8_t              mac_per_port);

/*******************************************************************************
* tpma_uni_reset_max_mac_depth()
*
* DESCRIPTION:   Limits the number of MAC addresses per port back to startup values. 
*                call to tpm_sw_reset_port_max_macs
*
* INPUTS:
*   uni_port   -	Related UNI port
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_uni_reset_max_mac_depth (tpm_trg_port_type_t uni_port);
                                        
/*******************************************************************************
* tpma_uni_set_egress_flooding()
*
* DESCRIPTION:   Control the flooding behavior (unknown Dest MAC address) per port. 
*                call to tpm_sw_set_port_flooding
*
* INPUTS:
*   uni_port	- Egress port to apply flooding	
*   allow_flood - Flooding permission	0 - Don't permit unknown MAC destination flooding 
*                                       1 - Permit unknown MAC destination flooding
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_uni_set_egress_flooding (tpm_trg_port_type_t uni_port,
                                           uint8_t     allow_flood);

/*******************************************************************************
* tpma_uni_reset_egress_flooding()
*
* DESCRIPTION:   Control the flooding behavior (unknown Dest MAC address) per port back to startup values.   
*                call to tpm_sw_reset_port_flooding
*
* INPUTS:
*   uni_port	- Egress port to apply flooding	
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_uni_reset_egress_flooding (tpm_trg_port_type_t uni_port);

/******************************************************************************/
/********************************** Managment APIs ****************************/
/******************************************************************************/
/*******************************************************************************
* tpma_set_OMCI_channel()
*
* DESCRIPTION:      Establishes a communication channel for the OMCI management protocol.
*                   The API sets the gemportid, the Rx input queue in the CPU, and the
*                   Tx T-CONT and queue parameters, which are configured in the driver.
*
* INPUTS:
* gem_port           - for OMCI Rx frames - the gem port wherefrom the OMCI frames are received.
* cpu_rx_queue       - for OMCI Rx frames - the CPU rx queue number.
* tcont_num          - for OMCI Tx frames - the TCONT number where to send the OMCI frames.
* cpu_tx_queue       - for OMCI Tx frames - the CPU tx queue number.
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_set_OMCI_channel (uint32_t              gem_port,
                                          uint32_t              cpu_rx_queue,
                                          tpm_trg_port_type_t   tcont_num,
                                          uint32_t              cpu_tx_queue);

#ifdef __cplusplus
}
#endif


#endif


