/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************** 
*      tpma_cli_db.h                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:  AyaL                                                    
*                                                                                
* DATE CREATED: June 9, 2010     
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.1.1.1 $                                                           
*******************************************************************************/


#ifndef __TPMA_CLI_DB_H__
#define __TPMA_CLI_DB_H__

/* Include Files
------------------------------------------------------------------------------*/

#include "tpm_types.h"
#include "tpma_cli_cmd.h"


/* Definitions
------------------------------------------------------------------------------*/

/* Enums                              
------------------------------------------------------------------------------*/ 

/* Structs
------------------------------------------------------------------------------*/

/* Global variables
------------------------------------------------------------------------------*/

/* Global functions
------------------------------------------------------------------------------*/

/* Prototypes
------------------------------------------------------------------------------*/  


bool_t match_name_to_index_bridge       (const char *name, uint32_t *ind);
bool_t match_name_to_index_dest         (const char *name, uint32_t *ind);
bool_t match_name_to_index_mod          (const char *name, uint32_t *ind);
bool_t match_name_to_index_l2Key        (const char *name, uint32_t *ind);

uint32_t get_number_of_bridge_entries   ();
uint32_t get_number_of_dest_entries     ();
uint32_t get_number_of_mod_entries      ();
uint32_t get_number_of_l2_key_entries   ();

bool_t	set_omci_key                    (tpma_cli_bridge_t* omci_key);
bool_t  set_det                         (tpma_cli_dest_t* dest);
bool_t  set_mod                         (tpma_cli_mod_t* mod);
bool_t	set_l2_key                      (tpma_cli_l2_key_members_t type, tpma_cli_l2_key_t* l2_key);
bool_t  set_l2_rull                     (bool_t isUS, char * omciKey_name, tpm_src_port_type_t port, uint32_t parseBM, char * l2Key_name, char * dest_name, char * mod_name,tpm_pkt_action_t action);

void	print_omci_key                  (uint32_t start, uint32_t end);
void	print_dest                      (uint32_t start, uint32_t end);
void	print_mod                       (uint32_t start, uint32_t end);
void	print_l2_key                    (uint32_t start, uint32_t end);


#endif /*__TPMA_CLI_DB_H__*/
