/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <clish/shell.h>
#include "tpma_cli_db.h"
#include "globals.h"

static tpma_cli_bridge_t tpma_cli_bridge_arr[TPMA_CLI_MAX_ENTRIES];
static tpma_cli_dest_t   tpma_cli_dest_arr  [TPMA_CLI_MAX_ENTRIES];
static tpma_cli_mod_t    tpma_cli_mod_arr   [TPMA_CLI_MAX_ENTRIES];
static tpma_cli_l2_key_t tpma_cli_l2_key_arr[TPMA_CLI_MAX_ENTRIES];

static uint32_t number_of_bridge_entries    = 0;
static uint32_t number_of_dest_entries      = 0;
static uint32_t number_of_mod_entries       = 0;
static uint32_t number_of_l2_key_entries    = 0;


bool_t match_name_to_index_bridge(const char *name, uint32_t *ind)
{

    bool_t found = FALSE; 
    uint32_t i; 


    for (i=0; i<number_of_bridge_entries; i++) {
        if (strcmp(name, tpma_cli_bridge_arr[i].name)==0) {
            *ind = i;
            found = TRUE;
            break;
        }
    }
    return found;
}

bool_t match_name_to_index_dest(const char *name, uint32_t *ind)
{

    bool_t found = FALSE; 
    uint32_t i; 


    for (i=0; i<number_of_dest_entries; i++) {
        if (strcmp(name, tpma_cli_dest_arr[i].name)==0) {
            *ind = i;
            found = TRUE;
            break;
        }
    }
    return found;
}

bool_t match_name_to_index_mod(const char *name, uint32_t *ind)
{

    bool_t found = FALSE; 
    uint32_t i; 


    for (i=0; i<number_of_mod_entries; i++) {
        if (strcmp(name, tpma_cli_mod_arr[i].name)==0) {
            *ind = i;
            found = TRUE;
            break;
        }
    }
    return found;
}

bool_t match_name_to_index_l2Key(const char *name, uint32_t *ind)
{

    bool_t found = FALSE; 
    uint32_t i; 


    for (i=0; i<number_of_l2_key_entries; i++) {
        if (strcmp(name, tpma_cli_l2_key_arr[i].name)==0) {
            *ind = i;
            found = TRUE;
            break;
        }
    }
    return found;
}
uint32_t get_number_of_bridge_entries(){
	return number_of_bridge_entries;
}
uint32_t get_number_of_dest_entries(){
	return number_of_dest_entries;
}
uint32_t get_number_of_mod_entries(){
	return number_of_mod_entries;
}
uint32_t get_number_of_l2_key_entries(){
	return number_of_l2_key_entries;
}

bool_t	set_omci_key(tpma_cli_bridge_t* omci_key){

	
	uint32_t i;

	if (number_of_bridge_entries >= TPMA_CLI_MAX_ENTRIES) {
        /*database is full*/
        return FALSE;
    }
    else 
    {
        if (match_name_to_index_bridge(omci_key->name,&i)==FALSE) {
            /*new record*/
            i = number_of_bridge_entries;
            memset(&(tpma_cli_bridge_arr[i]), 0, sizeof(tpma_cli_bridge_t));
            number_of_bridge_entries++;
        }
    }
	
    memmove(&(tpma_cli_bridge_arr[i].name),omci_key->name,strlen(omci_key->name));
    tpma_cli_bridge_arr[i].bridge_id = omci_key->bridge_id;
    tpma_cli_bridge_arr[i].src_tp_id = omci_key->src_tp_id;
    tpma_cli_bridge_arr[i].dst_tp_id = omci_key->dst_tp_id;

    return TRUE;

}


bool_t  set_det(tpma_cli_dest_t* dest){

    uint32_t i;

	if (number_of_dest_entries >= TPMA_CLI_MAX_ENTRIES) {
        /*database is full*/
        return FALSE;
    }
    else 
    {
        if (match_name_to_index_dest(dest->name,&i)==FALSE) {
            /*new record*/
            i = number_of_dest_entries;
            memset(&(tpma_cli_dest_arr[i]), 0, sizeof(tpma_cli_dest_t));
            number_of_dest_entries++;
        }
    }
	
    memmove(&(tpma_cli_dest_arr[i].name),dest->name,strlen(dest->name));
    tpma_cli_dest_arr[i].port  = dest->port;
    tpma_cli_dest_arr[i].queue = dest->queue;
    tpma_cli_dest_arr[i].gem   = dest->gem; 
    

    return TRUE;
}


bool_t  set_mod(tpma_cli_mod_t* mod){

    uint32_t i;

    if (number_of_mod_entries >= TPMA_CLI_MAX_ENTRIES) {
        /*database is full*/
        DBG_PRINT("set_mod FAILED database is full\n");
        return FALSE;
    }
    else 
    {
        if (match_name_to_index_mod(mod->name,&i)==FALSE) {
            /*new record*/
            
            i = number_of_mod_entries;
            memset(&(tpma_cli_mod_arr[i]), 0, sizeof(tpma_cli_mod_t));
            number_of_mod_entries++;
        }
    }

    memmove (&(tpma_cli_mod_arr[i]),mod,sizeof(tpma_cli_mod_t));
    
    
    return TRUE;
}

bool_t	set_l2_key(tpma_cli_l2_key_members_t type, tpma_cli_l2_key_t* l2_key){

     uint32_t i;


     if (number_of_l2_key_entries >= TPMA_CLI_MAX_ENTRIES) {
        /*database is full*/
        return FALSE;
    }
    else 
    {
        if (match_name_to_index_l2Key(l2_key->name,&i)==FALSE) {
            /*new record*/
            i = number_of_l2_key_entries;
            memset(&(tpma_cli_l2_key_arr[i]), 0, sizeof(tpma_cli_l2_key_t));
            memcpy(&(tpma_cli_l2_key_arr[i].name),l2_key->name, strlen(l2_key->name));
            number_of_l2_key_entries ++;
        }
    }
    switch (type) {
    case TPMA_MAC:
        memmove(&(tpma_cli_l2_key_arr[i].mac),&(l2_key->mac), sizeof(tpma_cli_mac_t));
        break;
    case TPMA_V1:
        memmove(&(tpma_cli_l2_key_arr[i].v1),&(l2_key->v1), sizeof(tpma_cli_vlan_t));
        break;
    case TPMA_V2:
        memmove(&(tpma_cli_l2_key_arr[i].v2),&(l2_key->v2), sizeof(tpma_cli_vlan_t));
        break;
    case TPMA_ETHERTYPE:
        tpma_cli_l2_key_arr[i].ether_type = l2_key->ether_type;
        break;
    case TPMA_PPPoE:
        memmove(&(tpma_cli_l2_key_arr[i].pppoe),&(l2_key->pppoe), sizeof(tpma_cli_pppoe_t));
        break;
    case TPMA_GEM:
         tpma_cli_l2_key_arr[i].gem = l2_key->gem;
         break;
    }

    return TRUE;
}

bool_t  set_l2_rule(bool_t isUS, 
                    char * omciKey_name, 
                    tpm_src_port_type_t port, 
                    uint32_t parseBM, 
                    char * l2Key_name, 
                    char * dest_name, 
                    char * mod_name,
                    tpm_pkt_action_t action){

   
    uint32_t            idx;
    bool_t              ret_val;
    tpma_l2_key_t  	    omci_key;
    tpm_l2_acl_key_t    l2Key_values;
    tpm_pkt_frwd_t	    dest_values;
    tpm_pkt_mod_t	    mod_values;


   
     /*get matching omci_key values by given name*/
    ret_val = match_name_to_index_l2Key(l2Key_name,&idx);
    if (ret_val == FALSE) {
        DBG_PRINT("\n l2 key name %s could not be found", l2Key_name);
        DBG_PRINT("\n l2 key will be set as zeroes");
        memset (&l2Key_values, 0 , sizeof(tpm_l2_acl_key_t));
       
    }
    else{
         /*found, populate*/
        memcpy(&(l2Key_values.mac)      ,&(tpma_cli_l2_key_arr[idx].mac),sizeof(tpma_cli_mac_t));
        memcpy(&(l2Key_values.vlan1)    ,&(tpma_cli_l2_key_arr[idx].v1) ,sizeof(tpma_cli_vlan_t)); 
        memcpy(&(l2Key_values.vlan2)    ,&(tpma_cli_l2_key_arr[idx].v2) ,sizeof(tpma_cli_vlan_t));
        memcpy(&(l2Key_values.pppoe_hdr),&(tpma_cli_l2_key_arr[idx].pppoe) ,sizeof(tpma_cli_pppoe_t));
        l2Key_values.ether_type = tpma_cli_l2_key_arr[idx].ether_type;
        l2Key_values.gem_port   = tpma_cli_l2_key_arr[idx].gem;
    }


  /*get matching omci_key values by given name*/
    ret_val = match_name_to_index_bridge(omciKey_name,&idx);
    if (ret_val == FALSE) {
        return (ret_val);
    }
    /*found, populate*/
    omci_key.bridge_id      = tpma_cli_bridge_arr[idx].bridge_id;
    omci_key.source_tp      = tpma_cli_bridge_arr[idx].src_tp_id;
    omci_key.destination_tp = tpma_cli_bridge_arr[idx].dst_tp_id;

    
     /*get matching dest values by given name*/
    ret_val = match_name_to_index_dest(dest_name,&idx);
    if (ret_val == FALSE) {
        DBG_PRINT("\n l2 destination name %s could not be found", dest_name);
        return (ret_val);
    }
      /*found, populate*/
    dest_values.gem_port  = tpma_cli_dest_arr[idx].gem;
    dest_values.trg_port  = tpma_cli_dest_arr[idx].port;
    dest_values.trg_queue = tpma_cli_dest_arr[idx].queue;



      /*get matching modification values by given name*/
    ret_val = match_name_to_index_mod(mod_name,&idx);
    if (ret_val == FALSE) {
        DBG_PRINT("\n l2 modification name %s could not be found", l2Key_name);
        DBG_PRINT("\n l2 modification will be set as zeroes");
        memset (&mod_values, 0 , sizeof(tpm_pkt_mod_t));
       
    }
    else{
         /*found, populate*/
        mod_values.vlan_mod.vlan_op = tpma_cli_mod_arr[idx].oper;
        memcpy(&(mod_values.vlan_mod.vlan1_out),&(tpma_cli_mod_arr[idx].v1_out) ,sizeof(tpma_cli_vlan_t)); 
        memcpy(&(mod_values.vlan_mod.vlan2_out),&(tpma_cli_mod_arr[idx].v2_out) ,sizeof(tpma_cli_vlan_t)); 
    }


   if (isUS) {
       DBG_PRINT("\n call tpma_add_l2_US \n");
        tpma_add_l2_US(&omci_key,
                       port,
                       parseBM,
                       &l2Key_values,
                       &dest_values,
                       &mod_values,
                       action,
                       &idx);
    }
    else{
        DBG_PRINT("\n call tpma_add_l2_DS \n");
        tpma_add_l2_DS(&omci_key,
                       port,
                       parseBM,
                       &l2Key_values,
                       &dest_values,
                       &mod_values,
                       action,
                       &idx);
    }
    return TRUE;
}

void	print_omci_key(uint32_t start, uint32_t end){

	uint32_t i;
	for (i=start; i<end; i++) {
        PRINT("name              = %s", tpma_cli_bridge_arr[i].name);
        PRINT("bridge id         = %d", tpma_cli_bridge_arr[i].bridge_id);
        PRINT("source tp id      = %d", tpma_cli_bridge_arr[i].src_tp_id);
        PRINT("destination tp id = %d", tpma_cli_bridge_arr[i].dst_tp_id);
        PRINT("***********************************************************");
    }
}

void	print_dest(uint32_t start, uint32_t end){
	
	uint32_t i;
	for (i=start; i<end; i++) {
        PRINT("name      = %s",tpma_cli_dest_arr[i].name);
        PRINT("port      = %d",tpma_cli_dest_arr[i].port);
        PRINT("queue     = %d",tpma_cli_dest_arr[i].queue);
        PRINT("gem       = %d",tpma_cli_dest_arr[i].gem);
        PRINT("***********************************************************");
    }
}

void	print_mod(uint32_t start, uint32_t end){

	uint32_t i; 
	for (i=start; i<end; i++) {
        PRINT("name             = %s",tpma_cli_mod_arr[i].name);
        PRINT("operation        = %d",tpma_cli_mod_arr[i].oper);
        PRINT("vlan1 tpid       = 0x%x",tpma_cli_mod_arr[i].v1_out.tpid);
        PRINT("vlan1 vid        = %d",tpma_cli_mod_arr[i].v1_out.vid);
        PRINT("vlan1 vid_mask   = 0x%x",tpma_cli_mod_arr[i].v1_out.vid_mask);
        PRINT("vlan1 cfi        = %d",tpma_cli_mod_arr[i].v1_out.cfi);
        PRINT("vlan1 cfi_mask   = 0x%x",tpma_cli_mod_arr[i].v1_out.cfi_mask);
        PRINT("vlan1 pbit       = %d",tpma_cli_mod_arr[i].v1_out.pbit);
        PRINT("vlan1 pbit_mask  = 0x%x",tpma_cli_mod_arr[i].v1_out.pbit_mask);
        PRINT("vlan2 tpid       = 0x%x",tpma_cli_mod_arr[i].v2_out.tpid);
        PRINT("vlan2 vid        = %d",tpma_cli_mod_arr[i].v2_out.vid);
        PRINT("vlan2 vid_mask   = 0x%x",tpma_cli_mod_arr[i].v2_out.vid_mask);
        PRINT("vlan2 cfi        = %d",tpma_cli_mod_arr[i].v2_out.cfi);
        PRINT("vlan2 cfi_mask   = 0x%x",tpma_cli_mod_arr[i].v2_out.cfi_mask);
        PRINT("vlan2 pbit       = %d",tpma_cli_mod_arr[i].v2_out.pbit);
        PRINT("vlan2 pbit_mask  = 0x%x",tpma_cli_mod_arr[i].v2_out.pbit_mask);
        PRINT("***********************************************************");
    }
}

void	print_l2_key(uint32_t start, uint32_t end){

    
	uint32_t i;
	for (i=start; i<end; i++) {
        PRINT("name             = %s",tpma_cli_l2_key_arr[i].name);
        PRINT("mac da           = %x:%x:%x:%x:%x:%x",tpma_cli_l2_key_arr[i].mac.mac_da[0],
                                                     tpma_cli_l2_key_arr[i].mac.mac_da[1],
                                                     tpma_cli_l2_key_arr[i].mac.mac_da[2],
                                                     tpma_cli_l2_key_arr[i].mac.mac_da[3],
                                                     tpma_cli_l2_key_arr[i].mac.mac_da[4],
                                                     tpma_cli_l2_key_arr[i].mac.mac_da[5]);
        PRINT("mac da mask      = %x:%x:%x:%x:%x:%x",tpma_cli_l2_key_arr[i].mac.mac_da_mask[0],
                                                     tpma_cli_l2_key_arr[i].mac.mac_da_mask[1],
                                                     tpma_cli_l2_key_arr[i].mac.mac_da_mask[2],
                                                     tpma_cli_l2_key_arr[i].mac.mac_da_mask[3],
                                                     tpma_cli_l2_key_arr[i].mac.mac_da_mask[4],
                                                     tpma_cli_l2_key_arr[i].mac.mac_da_mask[5]);
        PRINT("mac sa           = %x:%x:%x:%x:%x:%x",tpma_cli_l2_key_arr[i].mac.mac_sa[0],
                                                     tpma_cli_l2_key_arr[i].mac.mac_sa[1],
                                                     tpma_cli_l2_key_arr[i].mac.mac_sa[2],
                                                     tpma_cli_l2_key_arr[i].mac.mac_sa[3],
                                                     tpma_cli_l2_key_arr[i].mac.mac_sa[4],
                                                     tpma_cli_l2_key_arr[i].mac.mac_sa[5]);
        PRINT("mac sa mask      = %x:%x:%x:%x:%x:%x",tpma_cli_l2_key_arr[i].mac.mac_sa_mask[0],
                                                     tpma_cli_l2_key_arr[i].mac.mac_sa_mask[1],
                                                     tpma_cli_l2_key_arr[i].mac.mac_sa_mask[2],
                                                     tpma_cli_l2_key_arr[i].mac.mac_sa_mask[3],
                                                     tpma_cli_l2_key_arr[i].mac.mac_sa_mask[4],
                                                     tpma_cli_l2_key_arr[i].mac.mac_sa_mask[5]);
        PRINT("vlan 1 tpid      = 0x%x", tpma_cli_l2_key_arr[i].v1.tpid);
        PRINT("vlan 1 vid       = %d", tpma_cli_l2_key_arr[i].v1.vid);
        PRINT("vlan 1 vid mask  = 0x%x", tpma_cli_l2_key_arr[i].v1.vid_mask);
        PRINT("vlan 1 cfi       = %d", tpma_cli_l2_key_arr[i].v1.cfi);
        PRINT("vlan 1 cfi mask  = 0x%x", tpma_cli_l2_key_arr[i].v1.cfi_mask);
        PRINT("vlan 1 pbit      = %d", tpma_cli_l2_key_arr[i].v1.pbit);
        PRINT("vlan 1 pbit mask = 0x%x", tpma_cli_l2_key_arr[i].v1.pbit_mask);

        PRINT("vlan 2 tpid      = 0x%x", tpma_cli_l2_key_arr[i].v2.tpid);
        PRINT("vlan 2 vid       = %d", tpma_cli_l2_key_arr[i].v2.vid);
        PRINT("vlan 2 vid mask  = 0x%x", tpma_cli_l2_key_arr[i].v2.vid_mask);
        PRINT("vlan 2 cfi       = %d", tpma_cli_l2_key_arr[i].v2.cfi);
        PRINT("vlan 2 cfi mask  = 0x%x", tpma_cli_l2_key_arr[i].v2.cfi_mask);
        PRINT("vlan 2 pbit      = %d", tpma_cli_l2_key_arr[i].v2.pbit);
        PRINT("vlan 2 pbit mask = 0x%x", tpma_cli_l2_key_arr[i].v2.pbit_mask);

        PRINT("ether type       = 0x%x", tpma_cli_l2_key_arr[i].ether_type);
        PRINT("gem port         = %d", tpma_cli_l2_key_arr[i].gem);
        PRINT("PPPoE session    = %d", tpma_cli_l2_key_arr[i].pppoe.session);
        PRINT("PPPoE protocol   = %d", tpma_cli_l2_key_arr[i].pppoe.protocol);
        PRINT("***********************************************************");

                                                     
      
    }
}
