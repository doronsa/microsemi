/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************** 
*      tpma_cli_cmd.h                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:  AyaL                                                    
*                                                                                
* DATE CREATED: June 9, 2010     
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.1.1.1 $                                                           
*******************************************************************************/

#ifndef __TPMA_CLI_CMD_H__
#define __TPMA_CLI_CMD_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Include Files
------------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"
#include "tpm_types.h"
#include "tpma_types.h"
#include "globals.h"

/* Definitions
------------------------------------------------------------------------------*/

#define TPMA_CLI_MAX_NAME       16
#define TPMA_CLI_MAX_ENTRIES    512


/* Enums                              
------------------------------------------------------------------------------*/ 

typedef enum
{   
    TPMA_MAC,
    TPMA_V1,
    TPMA_V2,
    TPMA_ETHERTYPE,
    TPMA_PPPoE,
    TPMA_GEM
}tpma_cli_l2_key_members_t;

/* Structs
------------------------------------------------------------------------------*/


typedef struct
{
  uint16_t tpid;
  uint16_t vid;
  uint16_t vid_mask;
  uint8_t  cfi;
  uint8_t  cfi_mask;
  uint8_t  pbit;
  uint8_t  pbit_mask;
} tpma_cli_vlan_t;

typedef struct
{
  uint8_t mac_da[6];
  uint8_t mac_da_mask[6];
  uint8_t mac_sa[6];
  uint8_t mac_sa_mask[6];
} tpma_cli_mac_t;

typedef struct
{
  uint32_t session;
  uint32_t protocol;
} tpma_cli_pppoe_t;

typedef struct
{
  char     name[TPMA_CLI_MAX_NAME];
  uint32_t bridge_id;
  uint32_t src_tp_id;
  uint32_t dst_tp_id;
} tpma_cli_bridge_t;

typedef struct
{
  char     name[TPMA_CLI_MAX_NAME];
  uint32_t port;
  uint32_t queue;
  uint32_t gem;
} tpma_cli_dest_t;

typedef struct
{
  char              name[TPMA_CLI_MAX_NAME];
  uint32_t          oper;
  tpma_cli_vlan_t   v1_out;
  tpma_cli_vlan_t   v2_out;
} tpma_cli_mod_t;

typedef struct
{
  char              name[TPMA_CLI_MAX_NAME];
  tpma_cli_mac_t    mac;
  tpma_cli_vlan_t   v1;
  tpma_cli_vlan_t   v2;
  uint32_t          ether_type;
  tpma_cli_pppoe_t  pppoe;
  uint32_t          gem;
} tpma_cli_l2_key_t;


/* Global variables
------------------------------------------------------------------------------*/


/* Global functions
------------------------------------------------------------------------------*/

/* Prototypes
------------------------------------------------------------------------------*/  
#ifdef __cplusplus
}
#endif


#endif


