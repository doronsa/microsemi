/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "tpm_api.h"
#include "tpm_print.h"
#include "tpma_types.h"
#include "tpma_cli_cmd.h"


/********************************************************************************/
/********************************************************************************/
/************************* show commands ****************************************/
/********************************************************************************/
/********************************************************************************/
bool_t tpma_cli_cmd_print_l2_omci_key(const clish_shell_t    *instance,
                                      const lub_argv_t       *argv)
{
    uint32_t i;
    const char* omci_name = lub_argv__get_arg(argv,0);
 
    if (match_name_to_index_bridge(omci_name, &i) == BOOL_FALSE){

        DBG_PRINT("omci key %s was not found\n", omci_name);
        return BOOL_FALSE;
    }


    print_omci_key(i,i+1);
   
	return BOOL_TRUE;
}

bool_t tpma_cli_cmd_print_l2_omci_key_all(const clish_shell_t* instance,
                                                  const lub_argv_t *argv)
{
    print_omci_key(0,get_number_of_bridge_entries() );
	return BOOL_TRUE;
}

bool_t tpma_cli_cmd_print_l2_destination(const clish_shell_t* instance,
                                                const lub_argv_t *argv)
{
    uint32_t i;
    const char* dest_name = lub_argv__get_arg(argv,0);

    if (match_name_to_index_dest(dest_name,&i) == BOOL_FALSE){

        DBG_PRINT("destination %s was not found\n", dest_name);
        return BOOL_FALSE;
    }
   

    print_dest(i,i+1);
	return BOOL_TRUE;
}


bool_t tpma_cli_cmd_print_l2_dest_all(const clish_shell_t* instance,
                                                  const lub_argv_t *argv)
{
    print_dest(0,get_number_of_dest_entries() );
	return BOOL_TRUE;
}


bool_t tpma_cli_cmd_print_l2_modification(const clish_shell_t* instance,
                                                  const lub_argv_t *argv)
{
    uint32_t i;
    const char* mod_name = lub_argv__get_arg(argv,0);


    if (match_name_to_index_mod(mod_name,&i)==BOOL_FALSE){
        DBG_PRINT("modification %s was not found\n", mod_name);
        return BOOL_FALSE;
    }
    
  
    print_mod(i,i+1);
	return BOOL_TRUE;
}


bool_t tpma_cli_cmd_print_l2_mod_all(const clish_shell_t* instance,
                                            const lub_argv_t *argv)
{
    print_mod(0,get_number_of_mod_entries() );
	return BOOL_TRUE;
}


bool_t tpma_cli_cmd_print_l2_l2key(const clish_shell_t* instance,
                                          const lub_argv_t *argv)
{
    uint32_t i;
    const char* l2Key_name = lub_argv__get_arg(argv,0);



    if (match_name_to_index_l2Key(l2Key_name,&i)==BOOL_FALSE){
        PRINT("l2key %s was not found\n", l2Key_name);
        return BOOL_FALSE;
    }
    
    
   
    print_l2_key(i,i+1);
	return BOOL_TRUE;
}

bool_t tpma_cli_cmd_print_l2_l2_key_all(const clish_shell_t* instance,
                                        const lub_argv_t *argv)
{

    print_l2_key(0,get_number_of_l2_key_entries());
	return BOOL_TRUE;
}

bool_t tpma_cli_cmd_print_l2_by_bridge (const clish_shell_t *instance,
                                        const lub_argv_t    *argv)
{

     uint32_t bridge_id     = atoi(lub_argv__get_arg(argv,0));
     print_l2_rules_by_bridge(bridge_id);

     return BOOL_TRUE;
}

bool_t tpma_cli_cmd_print_l2_chunk (const clish_shell_t *instance,
                                    const lub_argv_t    *argv)
{

     uint32_t direction     = atoi(lub_argv__get_arg(argv,0));
     uint32_t start_at      = atoi(lub_argv__get_arg(argv,1));
     uint32_t chunck_size   = atoi(lub_argv__get_arg(argv,1));

     print_l2_db((tpma_direction_t)direction,start_at ,chunck_size);

     return BOOL_TRUE;
}

bool_t tpma_cli_cmd_print_l2_all (const clish_shell_t   *instance,
                                  const lub_argv_t      *argv)
{

     print_all_l2();

     return BOOL_TRUE;
}
bool_t tpma_cli_cmd_print_l2_rules_direction (const clish_shell_t   *instance,
                                              const lub_argv_t      *argv)
{
    uint32_t direction     = atoi(lub_argv__get_arg(argv,0));
    if (direction == TPMA_DIRECTION_DS) {
        print_all_l2_ds();
    }
    else{
        if(direction == TPMA_DIRECTION_US){
            print_all_l2_us();
        }
        else {
            return BOOL_FALSE;
        }
    }

     return BOOL_TRUE;
}

bool_t tpma_cli_cmd_print_l3_by_bridge (const clish_shell_t *instance,
                                        const lub_argv_t    *argv)
{

     uint32_t bridge_id     = atoi(lub_argv__get_arg(argv,0));
     print_l3_rules_by_bridge(bridge_id);

     return BOOL_TRUE;
}

bool_t tpma_cli_cmd_print_l3_chunk (const clish_shell_t *instance,
                                    const lub_argv_t    *argv)
{

     uint32_t direction     = atoi(lub_argv__get_arg(argv,0));
     uint32_t start_at      = atoi(lub_argv__get_arg(argv,1));
     uint32_t chunck_size   = atoi(lub_argv__get_arg(argv,2));

     print_l3_db((tpma_direction_t)direction,start_at ,chunck_size);

     return BOOL_TRUE;
}

bool_t tpma_cli_cmd_print_l3_all (const clish_shell_t   *instance,
                                  const lub_argv_t      *argv)
{

     print_all_l3();

     return BOOL_TRUE;
}

bool_t tpma_cli_cmd_print_l4_all (const clish_shell_t   *instance,
                                  const lub_argv_t      *argv)
{

     print_l4_db();

     return BOOL_TRUE;
}
	


/********************************************************************************/
/********************************************************************************/
/************************* add commands *****************************************/
/********************************************************************************/
/********************************************************************************/

bool_t tpma_cli_add_omci_key (const clish_shell_t   *instance,
                              const lub_argv_t      *argv)
{

    
	tpma_cli_bridge_t omci_key;
    bool_t            ret_val = BOOL_TRUE;   
    /*get params*/
    const char* omci_name  = lub_argv__get_arg(argv,0);
    omci_key.bridge_id     = atoi(lub_argv__get_arg(argv,1));
    omci_key.src_tp_id     = atoi(lub_argv__get_arg(argv,2));
    omci_key.dst_tp_id     = atoi(lub_argv__get_arg(argv,3));
 
    strcpy(omci_key.name,omci_name);

    ret_val = set_omci_key(&omci_key);

    return (ret_val);
    
}


bool_t tpma_cli_add_l2_dest (const clish_shell_t    *instance,  
                             const lub_argv_t       *argv)
{  
   
    tpma_cli_dest_t dest;
    bool_t          ret_val = BOOL_TRUE;   
    /*get params*/
    const char* dest_name   = lub_argv__get_arg(argv,0);
    dest.port               = atoi(lub_argv__get_arg(argv,1));
    dest.queue              = atoi(lub_argv__get_arg(argv,2));
    dest.gem                = atoi(lub_argv__get_arg(argv,3));


    memset (&(dest.name),0,TPMA_CLI_MAX_NAME);
    strcpy(dest.name, dest_name);

    ret_val = set_det(&dest);

    return (ret_val);

}

uint32_t setHexValue(const char *value){

    char     hex_input[7] = {0};
    uint32_t ret_val;

    strcpy(hex_input,value);
    sscanf(&(hex_input[2]), "%x", &ret_val);

    return (ret_val);

}

bool_t tpma_cli_add_l2_mod (const clish_shell_t *instance,  
                            const lub_argv_t    *argv)
{  
   
    tpma_cli_mod_t mod; 
    bool_t         ret_val = BOOL_TRUE;   
    
    /*get params*/
    const char* mod_name    = lub_argv__get_arg(argv,0);
    mod.oper                = atoi(lub_argv__get_arg(argv,1));

    mod.v1_out.tpid         = (uint16_t)setHexValue(lub_argv__get_arg(argv,2));
    mod.v1_out.vid          = atoi(lub_argv__get_arg(argv,3));
    mod.v1_out.vid_mask     = (uint16_t)setHexValue(lub_argv__get_arg(argv,4));
    mod.v1_out.cfi          = atoi(lub_argv__get_arg(argv,5));
    mod.v1_out.cfi_mask     = (uint8_t)setHexValue(lub_argv__get_arg(argv,6));
    mod.v1_out.pbit         = atoi(lub_argv__get_arg(argv,7));
    mod.v1_out.pbit_mask    = (uint8_t)setHexValue(lub_argv__get_arg(argv,8));

    mod.v2_out.tpid         = (uint16_t)setHexValue(lub_argv__get_arg(argv,9));
    mod.v2_out.vid          = atoi(lub_argv__get_arg(argv,10));
    mod.v2_out.vid_mask     = (uint16_t)setHexValue(lub_argv__get_arg(argv,11));
    mod.v2_out.cfi          = atoi(lub_argv__get_arg(argv,12));
    mod.v2_out.cfi_mask     = (uint8_t)setHexValue(lub_argv__get_arg(argv,13));
    mod.v2_out.pbit         = atoi(lub_argv__get_arg(argv,14));
    mod.v2_out.pbit_mask    = (uint8_t)setHexValue(lub_argv__get_arg(argv,15));

    memset(&(mod.name),0,TPMA_CLI_MAX_NAME);
    strcpy(mod.name, mod_name);


    ret_val = set_mod(&mod);
    return (ret_val);
    

}

bool_t tpma_cli_add_l2Key_mac (const clish_shell_t  *instance,  
                               const lub_argv_t     *argv)
{  

    tpma_cli_l2_key_t   l2_key;
    bool_t              ret_val = BOOL_TRUE;   

   
    /*get params*/
    const char* l2key_name  = lub_argv__get_arg(argv,0);
    const char* mac_da      = lub_argv__get_arg(argv,1);
    const char* mac_da_mask = lub_argv__get_arg(argv,2);
    const char* mac_sa      = lub_argv__get_arg(argv,3);
    const char* mac_sa_mask = lub_argv__get_arg(argv,4);



    sscanf(mac_da, "%x:%x:%x:%x:%x:%x", &(l2_key.mac.mac_da[0]),
                                        &(l2_key.mac.mac_da[1]),
                                        &(l2_key.mac.mac_da[2]),
                                        &(l2_key.mac.mac_da[3]),
                                        &(l2_key.mac.mac_da[4]),
                                        &(l2_key.mac.mac_da[5]));
    
    sscanf(mac_da_mask, "%x:%x:%x:%x:%x:%x", &(l2_key.mac.mac_da_mask[0]),
                                             &(l2_key.mac.mac_da_mask[1]),
                                             &(l2_key.mac.mac_da_mask[2]),
                                             &(l2_key.mac.mac_da_mask[3]),
                                             &(l2_key.mac.mac_da_mask[4]),
                                             &(l2_key.mac.mac_da_mask[5]));

    sscanf(mac_sa, "%x:%x:%x:%x:%x:%x", &(l2_key.mac.mac_sa[0]),
                                        &(l2_key.mac.mac_sa[1]),
                                        &(l2_key.mac.mac_sa[2]),
                                        &(l2_key.mac.mac_sa[3]),
                                        &(l2_key.mac.mac_sa[4]),
                                        &(l2_key.mac.mac_sa[5]));
    
    sscanf(mac_sa_mask, "%x:%x:%x:%x:%x:%x", &(l2_key.mac.mac_sa_mask[0]),
                                             &(l2_key.mac.mac_sa_mask[1]),
                                             &(l2_key.mac.mac_sa_mask[2]),
                                             &(l2_key.mac.mac_sa_mask[3]),
                                             &(l2_key.mac.mac_sa_mask[4]),
                                             &(l2_key.mac.mac_sa_mask[5]));

    DBG_PRINT("l2_key.mac.mac_da   = %x:%x:%x:%x:%x:%x\n",l2_key.mac.mac_da[0], 
                                                          l2_key.mac.mac_da[1],
                                                          l2_key.mac.mac_da[2],
                                                          l2_key.mac.mac_da[3],
                                                          l2_key.mac.mac_da[4],
                                                          l2_key.mac.mac_da[5]);

    DBG_PRINT("l2_key.mac.mac_da_mask   = %x:%x:%x:%x:%x:%x\n",l2_key.mac.mac_da_mask[0], 
                                                               l2_key.mac.mac_da_mask[1],
                                                               l2_key.mac.mac_da_mask[2],
                                                               l2_key.mac.mac_da_mask[3],
                                                               l2_key.mac.mac_da_mask[4],
                                                               l2_key.mac.mac_da_mask[5]);



    DBG_PRINT("l2_key.mac.mac_sa   = %x:%x:%x:%x:%x:%x\n",l2_key.mac.mac_sa[0], 
                                                          l2_key.mac.mac_sa[1],
                                                          l2_key.mac.mac_sa[2],
                                                          l2_key.mac.mac_sa[3],
                                                          l2_key.mac.mac_sa[4],
                                                          l2_key.mac.mac_sa[5]);

    DBG_PRINT("l2_key.mac.mac_sa_mask   = %x:%x:%x:%x:%x:%x\n",l2_key.mac.mac_sa_mask[0], 
                                                               l2_key.mac.mac_sa_mask[1],
                                                               l2_key.mac.mac_sa_mask[2],
                                                               l2_key.mac.mac_sa_mask[3],
                                                               l2_key.mac.mac_sa_mask[4],
                                                               l2_key.mac.mac_sa_mask[5]);


    memset(&(l2_key.name),0,TPMA_CLI_MAX_NAME);
    strcpy (l2_key.name, l2key_name);
    ret_val = set_l2_key(TPMA_MAC, &l2_key);
    return (ret_val);

}

bool_t tpma_cli_add_l2Key_vlan1 (const clish_shell_t    *instance,  
                                 const lub_argv_t       *argv)
{  
   
    tpma_cli_l2_key_t   l2_key;
    bool_t              ret_val = BOOL_TRUE;   

    /*get params*/
    const char* l2key_name  = lub_argv__get_arg(argv,0);
    l2_key.v1.tpid          = (uint16_t)setHexValue(lub_argv__get_arg(argv,1));
    l2_key.v1.vid           = atoi(lub_argv__get_arg(argv,2));
    l2_key.v1.vid_mask      = (uint16_t)setHexValue(lub_argv__get_arg(argv,3));
    l2_key.v1.cfi           = atoi(lub_argv__get_arg(argv,4));
    l2_key.v1.cfi_mask      = (uint8_t)setHexValue(lub_argv__get_arg(argv,5));
    l2_key.v1.pbit          = atoi(lub_argv__get_arg(argv,6));
    l2_key.v1.pbit_mask     = (uint8_t)setHexValue(lub_argv__get_arg(argv,7));

    memset(&(l2_key.name),0,TPMA_CLI_MAX_NAME);
    strcpy(l2_key.name, l2key_name);

 
    ret_val = set_l2_key(TPMA_V1, &l2_key); 
    return (ret_val);
}

bool_t tpma_cli_add_l2Key_vlan2 (const clish_shell_t    *instance,  
                                 const lub_argv_t       *argv)
{  
    tpma_cli_l2_key_t   l2_key;
    bool_t              ret_val = BOOL_TRUE;   

    /*get params*/
    const char* l2key_name  = lub_argv__get_arg(argv,0);
    l2_key.v2.tpid          = (uint16_t)setHexValue(lub_argv__get_arg(argv,1));
    l2_key.v2.vid           = atoi(lub_argv__get_arg(argv,2));
    l2_key.v2.vid_mask      = (uint16_t)setHexValue(lub_argv__get_arg(argv,3));
    l2_key.v2.cfi           = atoi(lub_argv__get_arg(argv,4));
    l2_key.v2.cfi_mask      = (uint8_t)setHexValue(lub_argv__get_arg(argv,5));
    l2_key.v2.pbit          = atoi(lub_argv__get_arg(argv,6));
    l2_key.v2.pbit_mask     = (uint8_t)setHexValue(lub_argv__get_arg(argv,7));


    memset(&(l2_key.name),0,TPMA_CLI_MAX_NAME);
    strcpy(l2_key.name, l2key_name);

    ret_val = set_l2_key(TPMA_V2, &l2_key);
    return (ret_val);

}

bool_t tpma_cli_add_l2Key_etherType (const clish_shell_t    *instance,  
                                     const lub_argv_t       *argv)
{  
   
    tpma_cli_l2_key_t   l2_key;
    bool_t              ret_val = BOOL_TRUE;   

    /*get params*/
    const char* l2key_name  = lub_argv__get_arg(argv,0);
    l2_key.ether_type         = setHexValue(lub_argv__get_arg(argv,1));

    memset(&(l2_key.name),0,TPMA_CLI_MAX_NAME);
    strcpy(l2_key.name, l2key_name);    

    ret_val = set_l2_key(TPMA_ETHERTYPE, &l2_key);
    return (ret_val);
    
}

bool_t tpma_cli_add_l2Key_PPPoE (const clish_shell_t    *instance,  
                                 const lub_argv_t       *argv)
{  
    
    tpma_cli_l2_key_t   l2_key;
    bool_t              ret_val = BOOL_TRUE;   

    /*get params*/
    const char* l2key_name  = lub_argv__get_arg(argv,0);
    l2_key.pppoe.session    = atoi(lub_argv__get_arg(argv,1));
    l2_key.pppoe.protocol   = setHexValue(lub_argv__get_arg(argv,2));
   

    memset(&(l2_key.name),0,TPMA_CLI_MAX_NAME);
    strcpy(l2_key.name, l2key_name);

    ret_val = set_l2_key(TPMA_PPPoE, &l2_key);
    return (ret_val);
}

bool_t tpma_cli_add_l2Key_gem (const clish_shell_t  *instance,  
                               const lub_argv_t     *argv)
{  
    tpma_cli_l2_key_t   l2_key;
    bool_t              ret_val = BOOL_TRUE;   

    /*get params*/
    const char* l2key_name  = lub_argv__get_arg(argv,0);
    l2_key.gem              = atoi(lub_argv__get_arg(argv,1));

    memset(&(l2_key.name),0,TPMA_CLI_MAX_NAME);
    strcpy(l2_key.name, l2key_name);

    ret_val = set_l2_key(TPMA_GEM, &l2_key);
    return (ret_val);
}

bool_t tpma_cli_add_l2_rule (const clish_shell_t    *instance,  
                             const lub_argv_t       *argv)
{  
    
    uint32_t direction;
    uint32_t src_port;
    uint32_t parse_cmd_bm;
    uint32_t action;
    bool_t   ret_val = BOOL_TRUE;   
   
    /*get params*/
    direction              = atoi(lub_argv__get_arg(argv,0));
    const char* omci_name  = lub_argv__get_arg(argv,1);
    src_port               = atoi(lub_argv__get_arg(argv,2)); 
    parse_cmd_bm           = setHexValue(lub_argv__get_arg(argv,3)); 
    const char* l2key_name = lub_argv__get_arg(argv,4);
    const char* dest_name  = lub_argv__get_arg(argv,5);
    const char* mod_name   = lub_argv__get_arg(argv,6);
    action                 = setHexValue(lub_argv__get_arg(argv,7)); 


    
    ret_val =  (set_l2_rule((bool_t) direction, 
                            omci_name, 
                            (tpm_src_port_type_t) src_port, 
                            parse_cmd_bm, 
                            l2key_name, 
                            dest_name, 
                            mod_name,
                            (tpm_pkt_action_t) action));

    return ret_val;
}
bool_t tpma_cli_cmd_sync_db (const clish_shell_t   *instance,  
                             const lub_argv_t      *argv){  
    syncTPMvsTPMAdb();
}
bool_t  tpma_cli_cmd_init (const clish_shell_t   *instance,
                           const lub_argv_t      *argv){  
    module_tpma_tpmSetup();
}


bool_t tpma_cli_cmd_remove_by_handle (const clish_shell_t *instance, 
                                      const lub_argv_t *argv){

    uint32_t            handle;
    tpma_proc_status_t  ret_val;

     handle   = atoi(lub_argv__get_arg(argv,0)); 
     ret_val  = tpma_remove_by_handle(handle);
     if (ret_val == TPMAPROC_OK) {
         return BOOL_TRUE;
     }
     return BOOL_FALSE;

}

bool_t tpma_cli_cmd_clear_bridge_rules (const clish_shell_t *instance, 
                                        const lub_argv_t *argv){

    uint32_t            bridge_id;
    tpma_proc_status_t  ret_val;

     bridge_id   = atoi(lub_argv__get_arg(argv,0)); 
     ret_val     = tpma_remove_bridge(bridge_id);
     if (ret_val == TPMAPROC_OK) {
         return BOOL_TRUE;
     }
     return BOOL_FALSE;

}






