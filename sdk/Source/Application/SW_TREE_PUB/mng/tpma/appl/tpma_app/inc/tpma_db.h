/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/


#ifndef _TPMA_DB_H_
#define _TPMA_DB_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Include Files
------------------------------------------------------------------------------*/

/* Definitions
------------------------------------------------------------------------------*/
#define tpma_max_etries_per_section 100
#define debug 1
/* Enums                              
------------------------------------------------------------------------------*/ 

/* Structs
------------------------------------------------------------------------------*/

/* Global variables
------------------------------------------------------------------------------*/

/* Global functions
------------------------------------------------------------------------------*/

/* Prototypes
------------------------------------------------------------------------------*/  

/*L2 print prototypes*/
void print_l2_rules_by_bridge   (uint32_t bridge_id);
uint8_t print_l2_db             (tpma_direction_t dir,uint32_t start_rule_num, uint32_t rules_count);

void print_all_l2_us();
void print_all_l2_ds();
void print_all_l2();

void    tpma_get_omcc_port          (uint32_t *omcc_port);
void    tpma_set_omcc_port          (uint32_t omcc_port);
void    get_l2_owner_id             (uint32_t *owner_id);
void    set_l2_owner_id             (uint32_t owner_id);
uint8_t get_l2_db                   (tpma_direction_t dir,tpma_l2_rule_t **db);
uint8_t get_current_l2_rule_count   (tpma_direction_t dir,uint32_t *current_rule_count);
uint8_t get_max_l2_rule_count       (tpma_direction_t dir,uint32_t *max_rule_count);
uint8_t get_l2_db_parameters        (tpma_direction_t dir,tpma_l2_rule_t **db, uint32_t *current_rule_count ,uint32_t *max_rule_count);
uint8_t set_current_l2_rule_count   (tpma_direction_t dir,uint32_t current_rule_count);
uint8_t set_max_l2_rule_count       (tpma_direction_t dir,uint32_t max_rule_count);
uint8_t insert_l2_rule              (tpma_direction_t dir, uint32_t rule_num, tpma_l2_rule_t* new_rule);
uint8_t remove_l2_rule              (tpma_direction_t dir, uint32_t rule_num);
uint8_t find_first_l2_rule_by_key   (tpma_l2_key_t *key, tpma_direction_t dir, uint32_t *rule_num);
uint8_t find_first_l2_rule_by_src_upstream   (tpm_src_port_type_t src, uint32_t *rule_num);
uint8_t find_l2_rule_by_handle      (uint32_t handle, tpma_direction_t dir, uint32_t *rule_num);
uint8_t find_first_rule_by_length   (tpma_direction_t dir,tpma_length_t length,tpm_src_port_type_t src, uint32_t start_rule_num, uint32_t* rule_num);
void print_l2_acl_key               (char* header, tpm_l2_acl_key_t* l2_key,tpm_parse_fields_t parse_rule_bm);


/*L3 print prototypes*/
void    get_l3_owner_id             (uint32_t *owner_id);
void    set_l3_owner_id             (uint32_t owner_id);
uint8_t get_l3_db                   (tpma_direction_t dir,tpma_l3_rule_t **db);
uint8_t get_current_l3_rule_count   (tpma_direction_t dir,uint32_t *current_rule_count);
uint8_t get_max_l3_rule_count       (tpma_direction_t dir,uint32_t *max_rule_count);
uint8_t get_l3_db_parameters        (tpma_direction_t dir,tpma_l3_rule_t **db, uint32_t *current_rule_count ,uint32_t *max_rule_count);
uint8_t set_current_l3_rule_count   (tpma_direction_t dir,uint32_t current_rule_count);
uint8_t set_max_l3_rule_count       (tpma_direction_t dir,uint32_t max_rule_count);
void    print_l3_rules_by_bridge    (uint32_t bridge_id);
void    print_all_l3_us             ();
void    print_all_l3_ds             ();
void    print_all_l3                ();
uint8_t print_l3_db                 (tpma_direction_t dir,uint32_t start_rule_num, uint32_t rules_count);


uint8_t insert_l3_rule              (tpma_direction_t dir, uint32_t rule_num, tpma_l3_rule_t* new_rule);
uint8_t remove_l3_rule              (tpma_direction_t dir, uint32_t rule_num);
uint8_t find_first_l3_rule_by_key   (tpma_l2_key_t *key, tpma_direction_t dir, uint32_t *rule_num);

/*IPv4 multicast print prototypes*/
void    get_l4_mc_db                (tpma_IPv4_mc_rule_t **db);
void    get_current_l4_mc_rule_count(uint32_t *current_rule_count);
void    get_max_l4_mc_rule_count    (uint32_t *max_rule_count);
void    get_l4_mc_db_parameters     (tpma_IPv4_mc_rule_t **db, uint32_t *current_rule_count ,uint32_t *max_rule_count);
void    get_l4_owner_id             (uint32_t *owner_id);
void    set_l4_owner_id             (uint32_t owner_id);
void    set_max_l4_mc_rule_count    (uint32_t max_rule_count);
void    set_current_l4_mc_rule_count(uint32_t current_rule_count);
void    print_l4_db                 ();
void    print_l4_rule               (char* header, tpma_IPv4_mc_rule_t* l4_rule);
uint8_t insert_IPv4_mc_rule         (uint32_t rule_num, tpma_IPv4_mc_rule_t* new_rule);
uint8_t remove_IPv4_mc_rule         (uint32_t rule_num);
uint8_t find_IPv4_mc_rule_by_key    (tpma_l2_key_t *key, uint32_t *rule_num);
uint8_t find_IPv4_mc_rule_by_handle (uint32_t handle, uint32_t *rule_num);


#ifdef __cplusplus
}
#endif


#endif


