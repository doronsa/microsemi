/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/


#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "tpm_api.h"
#include "tpma_types.h"
#include "tpma_db.h"
#include "globals.h"

uint32_t tpma_owner_id_l2 = 0;
uint32_t tpma_owner_id_l3 = 0;
uint32_t tpma_owner_id_l4 = 0;


tpma_l2_rule_t      *tpma_l2_us_arr = NULL;
tpma_l2_rule_t      *tpma_l2_ds_arr = NULL;
tpma_l3_rule_t      *tpma_l3_us_arr = NULL;
tpma_l3_rule_t      *tpma_l3_ds_arr = NULL;
tpma_IPv4_mc_rule_t *tpma_l4_mc_arr = NULL;



uint32_t tpma_number_of_l2_us_rules   = 0; 
uint32_t tpma_number_of_l2_ds_rules   = 0; 
uint32_t tpma_number_of_l3_us_rules   = 0; 
uint32_t tpma_number_of_l3_ds_rules   = 0; 
uint32_t tpma_number_of_ipv4_mc_rules = 0; 

uint32_t tpma_max_number_of_l2_us_rules   = 0;  
uint32_t tpma_max_number_of_l2_ds_rules   = 0;  
uint32_t tpma_max_number_of_l3_us_rules   = 0;  
uint32_t tpma_max_number_of_l3_ds_rules   = 0;  
uint32_t tpma_max_number_of_ipv4_mc_rules = 0;  

int32_t tpma_omcc_port = TPMA_OMCC_PORT_INIT_VALUE;



/******************************************************************************/
/********************Database initiate*****************************************/
/******************************************************************************/
/*E_ErrorCodes*/ uint8_t module_tpma_tpmSetup(void){

    uint32_t                owner_id = 0;
    uint32_t                max_depth;
      
    /*establish ownership*/

    #ifdef not_implemented  
    if (tpm_create_ownerid(&owner_id)!= TPMAPROC_OK){
        DBG_PRINT("Fail to tpm_create_ownerid l2 owner_id");
        return FALSE; /*ERR_GENERAL; */
    }
    #endif

    set_l2_owner_id(owner_id);

    #ifdef not_implemented  
    /*get ownership to L2 */
    get_l2_owner_id(&owner_id);
    if (tpm_request_apigroup_ownership(owner_id, APIG_L2_ACL)!= APIG_OWNERSHIP_SUCCESS){
        DBG_PRINT("Fail to tpm_request_apigroup_ownership l2 owner_id");
        return FALSE; /*ERR_GENERAL; */
    }
        


     #endif
    /**********************************************************************************************************/
    /************************   initiate L2 US database   *****************************************************/
    /**********************************************************************************************************/

    if (tpm_get_section_free_size (APIG_L2_ACL, 0,&max_depth)!= TPM_RC_OK){
        DBG_PRINT("Fail to tpm_get_section_free_size l2 us");
        return FALSE; /*ERR_GENERAL; */
    }
    #if debug 
        max_depth = tpma_max_etries_per_section;
    #endif

    if (set_max_l2_rule_count(TPMA_DIRECTION_US, max_depth) == FALSE)
    {
        DBG_PRINT("Fail to set_max_l2_rule_count US");
        return FALSE; /*ERR_GENERAL; */
    }
        
    /*allocate L2_US actual rule array*/
    tpma_l2_us_arr = (tpma_l2_rule_t*) malloc( sizeof(tpma_l2_rule_t)*max_depth );
    if (tpma_l2_us_arr == NULL) {
        DBG_PRINT("fail to alloc tpma_l2_us_arr");
        return FALSE; /*ERR_GENERAL; */
    }
    memset (tpma_l2_us_arr,0,sizeof(tpma_l2_rule_t)*max_depth);

    
    /**********************************************************************************************************/
    /************************   initiate L2 DS database   *****************************************************/
    /**********************************************************************************************************/
    if (tpm_get_section_free_size (APIG_L2_ACL, 1,&max_depth)!= TPM_RC_OK){
        DBG_PRINT("Fail to tpm_get_section_free_size l2 ds");
        return FALSE; /*ERR_GENERAL; */
    }
    #ifdef debug 
        max_depth = tpma_max_etries_per_section;
    #endif

    if (set_max_l2_rule_count(TPMA_DIRECTION_DS, max_depth) == FALSE)
    {
        DBG_PRINT("Fail to set_max_l2_rule_count DS");
        return FALSE; /*ERR_GENERAL; */
    }

      /*allocate L2_DS array*/
    tpma_l2_ds_arr = (tpma_l2_rule_t*) malloc( sizeof(tpma_l2_rule_t)*max_depth );
    if (tpma_l2_ds_arr == NULL) {
        DBG_PRINT("fail to alloc tpma_l2_ds_arr");
        return FALSE; /*ERR_GENERAL; */
    }
    memset (tpma_l2_ds_arr,0,sizeof(tpma_l2_rule_t)*max_depth);

    #ifdef first_phase
        return TRUE;
    #endif



   #ifdef not_implemented  
   if (tpm_create_ownerid(&owner_id)!= TPMAPROC_OK){
       DBG_PRINT("Fail to tpm_create_ownerid l3 owner_id");
       return FALSE; /*ERR_GENERAL; */
   }
   #endif
   set_l3_owner_id(owner_id);

   #ifdef not_implemented  
    /*get ownership to L3 */
   if (tpm_request_apigroup_ownership(owner_id, APIG_L3TYPE_ACL)!= APIG_OWNERSHIP_SUCCESS){
       DBG_PRINT("Fail to tpm_request_apigroup_ownership l3 owner_id");
       return FALSE; /*ERR_GENERAL; */

   }
   #endif
    /**********************************************************************************************************/
    /************************   initiate L3 US database   *****************************************************/
    /**********************************************************************************************************/

    /*initiate L3 US database size*/
    if (tpm_get_section_free_size (APIG_L3TYPE_ACL, 0,&max_depth)!= TPM_RC_OK){
        DBG_PRINT("Fail to tpm_get_section_free_size"); 
        return FALSE; /*ERR_GENERAL; */
    }
        
    #ifdef debug 
        max_depth = tpma_max_etries_per_section;
    #endif

    if (set_max_l3_rule_count(TPMA_DIRECTION_US, max_depth) == FALSE)
    {
        DBG_PRINT("Fail to set_max_l3_rule_count US");
        return FALSE; /*ERR_GENERAL; */
    }

    /*allocate L3_US array*/
    tpma_l3_us_arr = (tpma_l3_rule_t*) malloc( sizeof(tpma_l3_rule_t)*max_depth );
    if (tpma_l3_us_arr == NULL) {
        DBG_PRINT("fail to alloc tpma_l3_us_arr");
        return FALSE; /*ERR_GENERAL; */
    }
    memset (tpma_l3_us_arr,0,sizeof(tpma_l3_rule_t)*max_depth);

    /**********************************************************************************************************/
    /************************   initiate L3 DS database   *****************************************************/
    /**********************************************************************************************************/

    /*initiate L3 US database size*/
    if (tpm_get_section_free_size (APIG_L3TYPE_ACL, 1,&max_depth)!= TPMAPROC_OK){
        DBG_PRINT("Fail to tpm_get_section_free_size"); 
        return FALSE; /*ERR_GENERAL; */
    }
    #ifdef debug 
        max_depth = tpma_max_etries_per_section;
    #endif

    if (set_max_l3_rule_count(TPMA_DIRECTION_DS, max_depth) == FALSE)
    {
        DBG_PRINT("Fail to set_max_l3_rule_count DS");
        return FALSE; /*ERR_GENERAL; */
    }

    /*allocate L3_DS array*/
    tpma_l3_ds_arr = (tpma_l3_rule_t*) malloc( sizeof(tpma_l3_rule_t)*max_depth );
    if (tpma_l3_ds_arr == NULL) {
        DBG_PRINT("fail to alloc tpma_l3_ds_arr");
        return FALSE; /*ERR_GENERAL; */
    }
    memset (tpma_l3_ds_arr,0,sizeof(tpma_l3_rule_t)*max_depth);

return TRUE;

    if (tpm_create_ownerid(&owner_id)!= TPMAPROC_OK){
        DBG_PRINT("Fail to tpm_create_ownerid l4 owner_id");
        return FALSE; /*ERR_GENERAL; */
    }
    set_l4_owner_id(owner_id);

#if 0 
    if (tpm_request_apigroup_ownership(owner_id, APIG_IPv4_MC)!= APIG_OWNERSHIP_SUCCESS){
        DBG_PRINT("Fail to tpm_request_apigroup_ownership l4 owner_id");
        return FALSE; /*ERR_GENERAL; */
    }

    /**********************************************************************************************************/
    /************************   initiate L4 MC database   *****************************************************/
    /**********************************************************************************************************/

 
    /*initiate L4 MC database */
    if (tpm_get_section_free_size (APIG_IPv4_MC, 1,&max_depth)!= TPMAPROC_OK){
        DBG_PRINT("Fail to tpm_get_section_free_size MC ipv4");
        return FALSE; /*ERR_GENERAL; */
    }
#endif 
    #ifdef debug 
        max_depth = tpma_max_etries_per_section;
    #endif
    set_max_l4_mc_rule_count(max_depth);

   
    /*allocate L4MC array*/
    tpma_l4_mc_arr = (tpma_IPv4_mc_rule_t*) malloc( sizeof(tpma_IPv4_mc_rule_t)*max_depth );
    if (tpma_l4_mc_arr == NULL) {
        DBG_PRINT("fail to alloc tpma_l4_mc_arr");
        return FALSE; /*ERR_GENERAL; */
    }
    memset (tpma_l4_mc_arr,0,sizeof(tpma_IPv4_mc_rule_t)*max_depth);

    return TRUE;
}
/******************************************************************************/
/******************* globals access function **********************************/
/******************************************************************************/


/******************************************************************************/
/******************* Database L2 functions ************************************/
/******************************************************************************/


/******************************************************************************/
/**************     get_l2_db   ***********************************************/
/******************************************************************************/

uint8_t get_l2_db(tpma_direction_t dir,tpma_l2_rule_t **db){

    if (dir == TPMA_DIRECTION_US) {
        *db              = tpma_l2_us_arr;
    }else if (dir == TPMA_DIRECTION_DS) {
        *db              = tpma_l2_ds_arr;                
    }
    else
        return FALSE;/*REAL error*/

    return TRUE;

 }
/******************************************************************************/
/**************     get_current_l2_rule_count   ********************************/
/******************************************************************************/

uint8_t get_current_l2_rule_count(tpma_direction_t dir,uint32_t *current_rule_count){

    if (dir == TPMA_DIRECTION_US) {
        *current_rule_count     = tpma_number_of_l2_us_rules;
    }else if (dir == TPMA_DIRECTION_DS) {
        *current_rule_count     = tpma_number_of_l2_ds_rules;                
    }
    else
        return FALSE;/*REAL error*/

    return TRUE;

 }
/******************************************************************************/
/**************     get_max_l2_rule_count   ***********************************/
/******************************************************************************/

uint8_t get_max_l2_rule_count(tpma_direction_t dir,uint32_t *max_rule_count){

    if (dir == TPMA_DIRECTION_US) {
        *max_rule_count     = tpma_max_number_of_l2_us_rules;
    }else if (dir == TPMA_DIRECTION_DS) {
        *max_rule_count     = tpma_max_number_of_l2_ds_rules;                
    }
    else
        return FALSE;/*REAL error*/

    return TRUE;

 }

/******************************************************************************/
/**************     set_current_l2_rule_count   *******************************/
/******************************************************************************/

uint8_t set_current_l2_rule_count(tpma_direction_t dir,uint32_t current_rule_count){

    if (dir == TPMA_DIRECTION_US) {
        tpma_number_of_l2_us_rules = current_rule_count;
    }else if (dir == TPMA_DIRECTION_DS) {
        tpma_number_of_l2_ds_rules = current_rule_count;                
    }
    else
        return FALSE;/*REAL error*/

    return TRUE;

 }
/******************************************************************************/
/**************     set_max_l2_rule_count   ***********************************/
/******************************************************************************/

uint8_t set_max_l2_rule_count(tpma_direction_t dir,uint32_t max_rule_count){

    if (dir == TPMA_DIRECTION_US) {
        tpma_max_number_of_l2_us_rules = max_rule_count;
    }else if (dir == TPMA_DIRECTION_DS) {
        tpma_max_number_of_l2_ds_rules = max_rule_count;                
    }
    else
        return FALSE;/*REAL error*/

    return TRUE;

 }
/******************************************************************************/
/**************     get_l2_db_parameters   ************************************/
/******************************************************************************/

uint8_t get_l2_db_parameters (tpma_direction_t dir,tpma_l2_rule_t **db, uint32_t *current_rule_count ,uint32_t *max_rule_count){

    uint8_t ret_val = FALSE;
    
    ret_val = get_l2_db(dir,db);
    if (ret_val == FALSE) {
        DBG_PRINT("fail to  get_l2_db");
        return ret_val;
    }
    ret_val = get_current_l2_rule_count(dir,current_rule_count);
    if (ret_val == FALSE) {
        DBG_PRINT("fail to  get_current_l2_rule_count");
        return ret_val;
    }
    if(max_rule_count != NULL){
        ret_val = get_max_l2_rule_count(dir,max_rule_count);
        if (ret_val == FALSE) {
            DBG_PRINT("fail to  get_max_l2_rule_count");
            return ret_val;
        }
    }
    

    return TRUE;
    

}
/******************************************************************************/
/*******************     tpma_get_omcc_port   *********************************/
/******************************************************************************/

void tpma_get_omcc_port(uint32_t *omcc_port){

    *omcc_port  = tpma_omcc_port; 

    return;

 }
/******************************************************************************/
/*******************     tpma_set_omcc_port   *********************************/
/******************************************************************************/

void tpma_set_omcc_port(uint32_t omcc_port){

    tpma_omcc_port = omcc_port; 

    return;

 }

/******************************************************************************/
/*******************     get_l2_owner_id   ************************************/
/******************************************************************************/

void get_l2_owner_id(uint32_t *owner_id){

    *owner_id  = tpma_owner_id_l2; 

    return;

 }
/******************************************************************************/
/*******************     set_l2_owner_id   ************************************/
/******************************************************************************/

void set_l2_owner_id(uint32_t owner_id){

    tpma_owner_id_l2 = owner_id; 

    return;

 }
/******************************************************************************/
/******************* Database L3 functions ************************************/
/******************************************************************************/

/******************************************************************************/
/**************     get_l3_db   ***********************************************/
/******************************************************************************/

uint8_t get_l3_db(tpma_direction_t dir,tpma_l3_rule_t **db){

    if (dir == TPMA_DIRECTION_US) {
        *db              = tpma_l3_us_arr;
    }else if (dir == TPMA_DIRECTION_DS) {
        *db              = tpma_l3_ds_arr;                
    }
    else
        return FALSE;/*REAL error*/

    return TRUE;

 }
/******************************************************************************/
/**************     get_current_l3_rule_count   *******************************/
/******************************************************************************/

uint8_t get_current_l3_rule_count(tpma_direction_t dir,uint32_t *current_rule_count){

    if (dir == TPMA_DIRECTION_US) {
        *current_rule_count     = tpma_number_of_l3_us_rules;
    }else if (dir == TPMA_DIRECTION_DS) {
        *current_rule_count     = tpma_number_of_l3_ds_rules;                
    }
    else
        return FALSE;/*REAL error*/

    return TRUE;

 }
/******************************************************************************/
/**************     get_max_l3_rule_count   **********************************/
/******************************************************************************/

uint8_t get_max_l3_rule_count(tpma_direction_t dir,uint32_t *max_rule_count){

    if (dir == TPMA_DIRECTION_US) {
        *max_rule_count     = tpma_max_number_of_l3_us_rules;
    }else if (dir == TPMA_DIRECTION_DS) {
        *max_rule_count     = tpma_max_number_of_l3_ds_rules;                
    }
    else
        return FALSE;/*REAL error*/

    return TRUE;

 }
/******************************************************************************/
/**************     set_current_l3_rule_count   *******************************/
/******************************************************************************/

uint8_t set_current_l3_rule_count(tpma_direction_t dir,uint32_t current_rule_count){

    if (dir == TPMA_DIRECTION_US) {
        tpma_number_of_l3_us_rules = current_rule_count;
    }else if (dir == TPMA_DIRECTION_DS) {
        tpma_number_of_l3_ds_rules = current_rule_count;                
    }
    else
        return FALSE;/*REAL error*/

    return TRUE;

 }
/******************************************************************************/
/**************     set_max_l3_rule_count   **********************************/
/******************************************************************************/

uint8_t set_max_l3_rule_count(tpma_direction_t dir,uint32_t max_rule_count){

    if (dir == TPMA_DIRECTION_US) {
        tpma_max_number_of_l3_us_rules = max_rule_count;
    }else if (dir == TPMA_DIRECTION_DS) {
        tpma_max_number_of_l3_ds_rules = max_rule_count;                
    }
    else
        return FALSE;/*REAL error*/

    return TRUE;

 }
/******************************************************************************/
/**************     get_l3_db_parameters   ************************************/
/******************************************************************************/

uint8_t get_l3_db_parameters (tpma_direction_t dir,tpma_l3_rule_t **db, uint32_t *current_rule_count ,uint32_t *max_rule_count){

    uint8_t ret_val = FALSE;
    
    ret_val = get_l3_db(dir,db);
    if (ret_val == FALSE) {
        DBG_PRINT("fail to  get_l3_db");
        return ret_val;
    }
    ret_val = get_current_l3_rule_count(dir,current_rule_count);
    if (ret_val == FALSE) {
        DBG_PRINT("fail to  get_current_l3_rule_count");
        return ret_val;
    }
    if (max_rule_count != NULL) {
        ret_val = get_max_l3_rule_count(dir,max_rule_count);
        if (ret_val == FALSE) {
            DBG_PRINT("fail to  get_max_l3_rule_count");
            return ret_val;
        }
    }
    
    return TRUE;
    

}

/******************************************************************************/
/*******************     get_l3_owner_id   ************************************/
/******************************************************************************/

void get_l3_owner_id(uint32_t *owner_id){

    *owner_id  = tpma_owner_id_l3; 

    return;

 }
/******************************************************************************/
/*******************     set_l3_owner_id   ************************************/
/******************************************************************************/

void set_l3_owner_id(uint32_t owner_id){

    tpma_owner_id_l3 = owner_id; 

    return;

 }
/******************************************************************************/
/******************* Database L4 functions ************************************/
/******************************************************************************/

/******************************************************************************/
/*******************     get_l4_mc_db   ***************************************/
/******************************************************************************/

void get_l4_mc_db(tpma_IPv4_mc_rule_t **db){

    *db              = tpma_l4_mc_arr;
    return;

 }

/******************************************************************************/
/*******************     get_current_l4_mc_rule_count   ***********************/
/******************************************************************************/

void get_current_l4_mc_rule_count(uint32_t *current_rule_count){

    *current_rule_count     = tpma_number_of_ipv4_mc_rules;
    return;

 }
/******************************************************************************/
/*******************     get_max_l4_mc_rule_count   ***************************/
/******************************************************************************/

void get_max_l4_mc_rule_count(uint32_t *max_rule_count){

    *max_rule_count     = tpma_max_number_of_ipv4_mc_rules;
    return;

 }

/******************************************************************************/
/*******************     set_current_l4_mc_rule_count   ***********************/
/******************************************************************************/

void set_current_l4_mc_rule_count(uint32_t current_rule_count){

    tpma_number_of_ipv4_mc_rules = current_rule_count;
    return;

 }

/******************************************************************************/
/*******************     set_max_l4_mc_rule_count   **************************/
/******************************************************************************/

void set_max_l4_mc_rule_count(uint32_t max_rule_count){

    tpma_max_number_of_ipv4_mc_rules = max_rule_count;
    return;

 }
/******************************************************************************/
/*******************     get_l4_mc_db_parameters   ****************************/
/******************************************************************************/

void get_l4_mc_db_parameters (tpma_IPv4_mc_rule_t **db, uint32_t *current_rule_count ,uint32_t *max_rule_count){

        
    get_l4_mc_db(db);
    get_current_l4_mc_rule_count(current_rule_count);
    if (max_rule_count != NULL) {
        get_max_l4_mc_rule_count(max_rule_count);
    }
    
    return;
}

/******************************************************************************/
/***************************     get_l4_owner_id   ****************************/
/******************************************************************************/

void get_l4_owner_id(uint32_t *owner_id){

    *owner_id  = tpma_owner_id_l4; 

    return;

 }
/******************************************************************************/
/***************************     set_l4_owner_id   ****************************/
/******************************************************************************/

void set_l4_owner_id(uint32_t owner_id){

    tpma_owner_id_l4= owner_id; 

    return;

 }

/******************************************************************************/
/********************L2 access functions***************************************/
/******************************************************************************/
 

/******************** INSERT **************************************************/
/*************** insert_L2_rule ***********************************************
* Dependent on the given direction, get corresponding database pointer, current depth and max depth
* Verify that there IS room, and that requested rule's position is IN range
* Rules are set one after the other WITHOUT gaps, and are sorted by thier PHYSICAL source port
* When a rule is set in already populated position, I copy records one by one from the end to make room
* and update their rule number field.
* Zero the record that needs to be set
* populate the new record 
* update current database depth
******************************************************************************/
uint8_t insert_l2_rule(tpma_direction_t dir, uint32_t rule_num, tpma_l2_rule_t* new_rule){

    tpma_l2_rule_t *db;
    uint32_t        current_rules_count; 
    uint32_t        max_rules_count; 
    uint32_t        i; 


    /*Dependent on the given direction, get corresponding database pointer, current depth and max depth*/
    if (get_l2_db_parameters(dir, &db, &current_rules_count, &max_rules_count) == FALSE) {
        DBG_PRINT("fail to  get_l2_db_parameters");
        return FALSE;
    }

    /*Verify that there IS room*/
    if (current_rules_count >= max_rules_count) {
        DBG_PRINT("no room in direction = %d l2 db",dir);
        return FALSE;
    }
    /*check if rule_num in range 
    Rules are set one after the other WITHOUT gaps, and are sorted by thier PHYSICAL source port*/
    if ((rule_num >= max_rules_count)||(rule_num > current_rules_count)) {
        DBG_PRINT("rule number is out of range");
        return FALSE;
    }

    /*When a rule is set in already populated position*/
    if (rule_num < current_rules_count) {
       /*copy records one by one from the end to make room*/
        for (i = current_rules_count ; i > rule_num; i--) {
            memcpy(&db[i], &db[i-1], sizeof(tpma_l2_rule_t));
            /*and update their rule number field*/
            db[i].rule_number = i;
        }
    }
    /*Zero the record that needs to be set*/

     memset(&db[rule_num],0, sizeof(tpma_l2_rule_t));

     /*populate the new record */
    db[rule_num].idx                = new_rule->idx;
    db[rule_num].rule_number        = rule_num;
    db[rule_num].src_port           = new_rule->src_port;
    db[rule_num].parse_rule_bm      = new_rule->parse_rule_bm;
    db[rule_num].rule_action        = new_rule->rule_action;
    db[rule_num].rule_length        = new_rule->rule_length;
    memcpy(&(db[rule_num].key),&(new_rule->key), sizeof(tpma_l2_key_t));
    memcpy(&(db[rule_num].l2_key), &(new_rule->l2_key), sizeof(tpm_l2_acl_key_t));
    memcpy(&(db[rule_num].pkt_frwd), &(new_rule->pkt_frwd), sizeof(tpm_pkt_frwd_t));

    /*increment */
    current_rules_count ++;
        
    /*update current database depth*/
    if (set_current_l2_rule_count(dir,current_rules_count) == FALSE) {
        DBG_PRINT("Fail to update set_current_l2_rule_count %d, %d", dir, current_rules_count);
        return FALSE;
    }
    

    return TRUE;
}
/******************** REMOVE *************************************************/
/*************** remove_l2_rule ***********************************************
* Dependent on the given direction, get corresponding database pointer, current depth and max depth
* Verify that requested rule's position is IN range and that there ARE rules to be removed
* When a rule is removed NOT from the end of table, 
* copy records one by one from the removed position to the end overwriting eachother
* and update their rule number field.
* Zero the last record in the database 
* update current database depth
******************************************************************************/

uint8_t remove_l2_rule(tpma_direction_t dir, uint32_t rule_num){

    tpma_l2_rule_t *db;
    uint32_t        current_rules_count; 
    uint32_t        max_rules_count; 
    uint32_t        i; 

    /*set search database*/
    if (get_l2_db_parameters(dir, &db, &current_rules_count, &max_rules_count) == FALSE){
        DBG_PRINT("fail to  get_l2_db_parameters");
        return FALSE;
    }
    /*verify rule number is IN range*/
    if (rule_num >= current_rules_count) {
        return FALSE;
    }
    /*verify there AER rules to be removed */
    if (current_rules_count == 0) {
        return FALSE;
    }
  
    /*if not last copy following rules on it  */
    if (rule_num < (current_rules_count -1)) {
        for (i = rule_num; i<current_rules_count; i++) {
            /*copy records one by one from the removed position to the end overwriting eachother*/
            memcpy(&db[i],&db[i+1], sizeof(tpma_l2_rule_t));
            /*and update their rule number field*/
            db[i].rule_number = i;
        }
    }
    /*Zero the last record in the database */
    memset(&db[current_rules_count-1],0, sizeof(tpma_l2_rule_t));

    /*update current database depth*/
    current_rules_count --;

    if (set_current_l2_rule_count(dir,current_rules_count) == FALSE) {
        DBG_PRINT("FAIL to update set_current_l2_rule_count %d %d", dir, current_rules_count);
        return FALSE;
    }
    
    return TRUE;

}
/******************** FIND ***************************************************/
/*************** find_first_l2_rule_by_key*******************************************
* Dependent on the given direction, get corresponding database pointer and current depth
* key refers to the OMCI indication of bridge_id, source_tp and destination_tp
* these identifiers are composed of mecless & instance id therefor can't be zero
* When source_tp is set to Zero it is used as an indication to match bridge_id ONLY
* when a full match is preformed, source_tp and destination_tp can cange their roles
******************************************************************************/
uint8_t find_first_l2_rule_by_key(tpma_l2_key_t *key, tpma_direction_t dir, uint32_t *rule_num){


    tpma_l2_rule_t *db;
    uint32_t        current_rules_count; 
        uint32_t        i; 
    uint8_t         found = FALSE;

    /*Dependent on the given direction, get corresponding database pointer and current depth */
    if (get_l2_db_parameters(dir, &db, &current_rules_count, NULL) == FALSE){
        DBG_PRINT("fail to  get_l2_db_parameters");
        return FALSE;
    }
  /*key refers to the OMCI indication of bridge_id, source_tp and destination_tp
  these identifiers are composed of mecless & instance id therefor can't be zero*/  
    found = FALSE;
    *rule_num = 0;
    for (i=0; i<current_rules_count; i++) {

        if (key->bridge_id == db[i].key.bridge_id) {
            /*When source_tp is set to Zero it is used as an indication to match bridge_id ONLY*/
            if (key->source_tp == 0) {
                *rule_num = db[i].rule_number;
                found = TRUE;
                break;/*find first record with the same bridgeId*/
            }
            /*when a full match is preformed, source_tp and destination_tp can cange their roles*/
            if ((key->source_tp == db[i].key.source_tp)|| 
                (key->source_tp == db[i].key.destination_tp)||
                (key->destination_tp == db[i].key.destination_tp)||
                (key->destination_tp == db[i].key.source_tp)){
                *rule_num = db[i].rule_number;
                found = TRUE;
                break;/*find first record with a full match*/
            }
        }
    }

    return (found);
}
/*************** find_first_l2_rule_by_src_upstream*******************************************
* Get upstream database pointer and current depth
* Rules are sorted by their PHYSICAL source port. 
* Downstream rules all have the same source port WAN, therfor are all considered as ONE group
* Upstream rules have 4 different sources and are sorted in groups
* That's why this function only relates to upstream database
* This function is used to limit the databse seach setting the start point of a group.
******************************************************************************/

uint8_t find_first_l2_rule_by_src_upstream(tpm_src_port_type_t src,  uint32_t *rule_num){

    tpma_l2_rule_t *db;
    uint32_t        current_rules_count; 
    uint32_t        i; 
    uint8_t         found = FALSE;

    /*Get upstream database pointer and current depth*/
    if(get_l2_db_parameters(TPMA_DIRECTION_US, &db, &current_rules_count, NULL) == FALSE){
        DBG_PRINT("fail to  get_l2_db_parameters");
        return FALSE;
    }
   
    found = FALSE;
  /*Rules are sorted by their PHYSICAL source port. 
    Downstream rules all have the same source port WAN, therfor are all considered as ONE group
    Upstream rules have 4 different sources and are sorted in groups
     That's why this function only relates to upstream database
    */ 
    *rule_num = 0;
    for (i=0; i<current_rules_count; i++) {
        if(db[i].src_port == src){
           *rule_num = db[i].rule_number;
           found = TRUE;
           break;/*find first record with the same source port*/
        }
    }
    return (found);
}
/*************** find_l2_rule_by_handle*******************************************
* Dependent on the given direction, get corresponding database pointer and current depth
* On each request from the L2DA to insert a rule, a handle is return (rule's index)
* this handle is unique and is returned from the TPM 
* This function is used to find a specific rule by its handle, mainly in order to remove it
******************************************************************************/

uint8_t find_l2_rule_by_handle(uint32_t handle, tpma_direction_t dir, uint32_t *rule_num){

    tpma_l2_rule_t *db;
    uint32_t        current_rules_count; 
    uint32_t        i; 
    uint8_t         found = FALSE; 

    /*Dependent on the given direction, get corresponding database pointerand current depth*/
    if (get_l2_db_parameters(dir, &db, &current_rules_count, NULL) == FALSE){
        DBG_PRINT("fail to  get_l2_db_parameters");
        return FALSE;
    }
    

    *rule_num = 0;
    found     = FALSE;
    for (i=0; i<current_rules_count; i++) {
        /*On each request from the L2DA to insert a rule, a handle is return (rule's index)
        this handle is unique and is returned from the TPM */
        if (db[i].idx == handle){
            *rule_num = db[i].rule_number;
            found = TRUE;
            break;/*found record with the same rule idx - handle*/
        }
    }
   
    return (found);

}

/*************** find_first_rule_by_length*******************************************
* Dependent on the given direction, get corresponding database pointer and current depth
* In order to allow maximal match, each group of rule (group is determined by the PHISICAL source port)
* is sorted internally by match value length. For example, 2 tag match rules are set BEFORE 1 tag match rules
* Each rule is given a corresponding length.
* Function receives the start rule number indicating the first rule in the group
* and a source port indicating that a group was changed - upstream ONLY 
* downstream rules are all one group - WAN, while upstream rules have 4 different groups for each uni port.
* This function returns the first rule with a smaller or equal length 
* Function is used to determine the rule's position with respect to its length
******************************************************************************/

uint8_t find_first_rule_by_length(tpma_direction_t     dir,
                                  tpma_length_t        length,  
                                  tpm_src_port_type_t  src, 
                                  uint32_t             start_rule_num, 
                                  uint32_t *           rule_num){

    tpma_l2_rule_t *db;
    uint32_t        current_rules_count; 
    uint32_t        i; 
    uint8_t         found = FALSE;

    /*Dependent on the given direction, get corresponding database pointer and current depth*/
    if (get_l2_db_parameters(dir, &db, &current_rules_count, NULL) == FALSE){
        DBG_PRINT("fail to  get_l2_db_parameters");
        return FALSE;
    }
/*
 In order to allow maximal match, each group of rule (group is determined by the PHISICAL source port)
 is sorted internally by match value length. For example, 2 tag match rules are set BEFORE 1 tag match rules
 Each rule is given a corresponding length.*/
    found = FALSE;
    /*first look in L2 US*/
    *rule_num = 0;
    /* Function receives the start rule number indicating the first rule in the group*/
    for (i=start_rule_num; i<current_rules_count; i++) {
/*  source port indicating that a group was changed - upstream ONLY 
    downstream rules are all one group - WAN, while upstream rules have 4 different groups for each uni port.
*/
        if ((dir == TPMA_DIRECTION_US)&&
             (db[i].src_port != src)) {
             /*don't pass to next group*/
             break;
         }

         if (db[i].rule_length <= length){
             found = TRUE;
             *rule_num = db[i].rule_number;
             break;/*found first rule with a smaller or equal length */
         }
         
    }
      
    return (found);
}


/******************** PRINT **************************************************/
/*************** print_l2_rules_by_bridge*******************************************
* Get upstream database pointer and current depth
* bridge_id refers to the OMCI indication of bridge_id, source_tp and destination_tp
* these identifiers are composed of mecless & instance id therefor can't be zero
* When source_tp is set to Zero it is used as an indication to match bridge_id ONLY
* upstream rules are sorted by thier PHYSICAL source ports, which corelates to their bridge_id
* therfore upstream database scan can stop on the missmatch of the bridge
* Get downstream database pointer and current depth
* downstream rules are all one group and are connected to the bridge by thier gem port
* therfore downstream database must be scanned from start to end
******************************************************************************/

void print_l2_rules_by_bridge(uint32_t bridge_id){

    uint32_t       i;
    uint32_t       rule_num;
    tpma_l2_key_t  key;
    tpma_l2_rule_t *db;
    uint32_t        current_rules_count; 


/*
bridge_id refers to the OMCI indication of bridge_id, source_tp and destination_tp
* these identifiers are composed of mecless & instance id therefor can't be zero
* When source_tp is set to Zero it is used as an indication to match bridge_id ONLY
*/
    key.bridge_id = bridge_id;
    key.source_tp = 0;
    key.destination_tp = 0;
    

    /* Get upstream database pointer and current depth*/
    if (get_l2_db_parameters(TPMA_DIRECTION_US, &db, &current_rules_count, NULL) == FALSE){
        DBG_PRINT("fail to  get_l2_db_parameters");
        return ;
    }
    if (find_first_l2_rule_by_key(&key, TPMA_DIRECTION_US, &rule_num)){
        PRINT( "\n\r\n\r-------------------    L2 UPstream rules - Bridge %d   ------------------\n\r", bridge_id); 
        for (i = rule_num; i<(current_rules_count); i++) {
            /*upstream rules are sorted by thier PHYSICAL source ports, which corelates to their bridge_id
                therfore upstream database scan can stop on the missmatch of the bridge*/
            if (db[i].key.bridge_id != bridge_id) {
                break;
            }
            PRINT( "Rule Number --------------------  %d",   db[i].rule_number); 
            PRINT( "TPMA idx -----------------------  %d",   db[i].idx); 
            PRINT( "Bridge ID ----------------------  %x",   db[i].key.bridge_id); 
            PRINT( "source TP ----------------------  %x",   db[i].key.source_tp); 
            PRINT( "destination TP -----------------  %x",   db[i].key.destination_tp); 
            PRINT( "source port --------------------  %x",   db[i].src_port); 
            PRINT( "action -------------------------  0x%x", db[i].rule_action); 
            PRINT( "TPMA parse fields---------------  0x%x", db[i].parse_rule_bm); 
            /*l2 key parameters*/
            print_l2_acl_key("TPMA l2_key", &(db[i].l2_key), db[i].parse_rule_bm);
            PRINT( "*********************************************************************\n\r"); 
            
        }
    }
    /* get downstream database pointer and current depth*/
    if (get_l2_db_parameters(TPMA_DIRECTION_DS, &db, &current_rules_count, NULL) == FALSE){
        DBG_PRINT("fail to  get_l2_db_parameters");
        return ;
    }
    if (find_first_l2_rule_by_key(&key, TPMA_DIRECTION_DS, &rule_num)){
        PRINT( "\n\r\n\r-------------------    L2 DOWNstream rules - Bridge %d   ------------------\n\r", bridge_id); 
        for (i = rule_num; i<(current_rules_count); i++) {
            /*downstream rules are all one group and are connected to the bridge by thier gem port
             therfore downstream database must be scanned from start to end*/
            if (db[i].key.bridge_id == bridge_id) {
            
               PRINT( "Rule Number --------------------  %d",   db[i].rule_number); 
               PRINT( "TPMA idx -----------------------  %d",   db[i].idx); 
               PRINT( "Bridge ID ----------------------  %x",   db[i].key.bridge_id); 
               PRINT( "source TP ----------------------  %x",   db[i].key.source_tp); 
               PRINT( "destination TP -----------------  %x",   db[i].key.destination_tp); 
               PRINT( "source port --------------------  %x",   db[i].src_port); 
               PRINT( "action -------------------------  0x%x", db[i].rule_action); 
               PRINT( "TPMA parse fields---------------  0x%x", db[i].parse_rule_bm); 
               /*l2 key parameters*/
               print_l2_acl_key("TPMA l2_key",&(db[i].l2_key), db[i].parse_rule_bm);
               PRINT( "*********************************************************************\n\r"); 
            }
        }
    }
}
/*************** print_l2_db*******************************************
* Dependent on the given direction, get corresponding database pointer and current depth
* if there ARE rules to be print, print headline and start loop
* Function receives a rule number to start from and number of rules to be printed
* Called from print_all_l2_us & print_all_l2_ds
******************************************************************************/

uint8_t print_l2_db(tpma_direction_t dir,uint32_t start_rule_num, uint32_t rules_count){

    tpma_l2_rule_t *db;
    uint32_t        current_rules_count; 
    uint32_t        i; 

    /*Dependent on the given direction, get corresponding database pointer and current depth*/
    if( get_l2_db_parameters(dir, &db, &current_rules_count, NULL) == FALSE){
        DBG_PRINT("Fail to get_l2_db_parameters");
        return FALSE;
    }
    

    /*if there ARE rules to be print, print headline and start loop*/
    if (current_rules_count > 0) {
        if (dir == TPMA_DIRECTION_US) {
            PRINT( "\n\r\n\r-------------------    L2 UPstream rules    ------------------\n\r"); 
        }else if (dir == TPMA_DIRECTION_DS) {
            PRINT( "\n\r\n\r-------------------    L2 DOWNstream rules    ------------------\n\r"); 
        }
    }    
    else
        return FALSE;/*REAL error*/

    for (i = start_rule_num; i<(start_rule_num+rules_count)&&i<current_rules_count; i++) {
        PRINT( "Rule Number --------------------  %d",   db[i].rule_number); 
        PRINT( "Rule length --------------------  %d",   db[i].rule_length); 
        PRINT( "TPMA idx -----------------------  %d",   db[i].idx); 
        PRINT( "Bridge ID ----------------------  %x",   db[i].key.bridge_id); 
        PRINT( "source TP ----------------------  %x",   db[i].key.source_tp); 
        PRINT( "destination TP -----------------  %x",   db[i].key.destination_tp); 
        PRINT( "source port --------------------  %x",   db[i].src_port); 
        PRINT( "action -------------------------  0x%x", db[i].rule_action); 
        PRINT( "TPMA parse fields---------------  0x%x", db[i].parse_rule_bm); 
        /*l2 key parameters*/

        print_l2_acl_key("TPMA l2_key", &(db[i].l2_key), db[i].parse_rule_bm);
        
        PRINT( "*********************************************************************\n\r"); 
    }
    return TRUE;

}

void print_all_l2_us()
{
   uint32_t current_rule_count;
   if (get_current_l2_rule_count(TPMA_DIRECTION_US, &current_rule_count)==FALSE) {
       DBG_PRINT("Fail to get_current_l2_rule_count");
       return;
   }
   print_l2_db(TPMA_DIRECTION_US,0,current_rule_count); 
}
void print_all_l2_ds()
{
    uint32_t current_rule_count;
    if (get_current_l2_rule_count(TPMA_DIRECTION_DS, &current_rule_count)==FALSE) {
        DBG_PRINT("Fail to get_current_l2_rule_count");
        return;
    }
    print_l2_db(TPMA_DIRECTION_DS,0,current_rule_count); 
}
void print_all_l2()
{
   print_all_l2_us(); 
   print_all_l2_ds(); 
}
/******************************************************************************/
/********************L3 access functions***************************************/
/******************************************************************************/

/******************** INSERT **************************************************/

/*************** insert_L3_rule ***********************************************
* Dependent on the given direction, get corresponding database pointer, current depth and max depth
* Verify that there IS room, and that requested rule's position is IN range
* Rules are set one after the other WITHOUT gaps, and are sorted by thier PHYSICAL source port
* When a rule is set in already populated position, I copy records one by one from the end to make room
* and update their rule number field.
* Zero the record that needs to be set
* populate the new record 
* update current database depth
******************************************************************************/
uint8_t insert_l3_rule(tpma_direction_t dir, uint32_t rule_num, tpma_l3_rule_t* new_rule){

    tpma_l3_rule_t *db;
    uint32_t        current_rules_count; 
    uint32_t        max_rules_count; 
    uint32_t        i; 

    /*Dependent on the given direction, get corresponding database pointer, current depth and max depth*/
    if (get_l3_db_parameters(dir,&db,&current_rules_count,&max_rules_count)==FALSE) {
        DBG_PRINT("FAIL to get_l3_db_parameters");
        return FALSE;
    }
    
    /*check if has room*/
    if (current_rules_count >= max_rules_count) {
        DBG_PRINT("no room in direction = %d l3 db", dir);
        return FALSE;
    }
    /*check if rule_num in range 
    Rules are set one after the other WITHOUT gaps, and are sorted by thier PHYSICAL source port*/
    if ((rule_num >= max_rules_count)||(rule_num > current_rules_count)) {
        DBG_PRINT("rule number is out of range");
        return FALSE;
    }
    /*check if NOT last */
     /*When a rule is set in already populated position*/
    if (rule_num < current_rules_count) {
       /*copy records one by one from the end to make room*/
        for (i = current_rules_count ; i > rule_num; i--) {
            memcpy(&db[i], &db[i-1], sizeof(tpma_l3_rule_t));
            /*and update their rule number field*/
            db[i].rule_number = i;
        }
    }
    /*Zero the record that needs to be set*/
     memset(&db[rule_num],0, sizeof(tpma_l3_rule_t));

    /*just add it */
    memmove(&db[rule_num].key,&(new_rule->key), sizeof(tpma_l3_rule_t));
    db[rule_num].rule_number     = rule_num;
    db[rule_num].src_port        = new_rule->src_port;
    db[rule_num].parse_rule_bm   = new_rule->parse_rule_bm;
    memmove(&db[rule_num].l3_key, &(new_rule->l3_key), sizeof(tpm_l3_type_key_t));
    db[rule_num].rule_type       = new_rule->rule_type;

    current_rules_count ++;

    /*increment current_rule_count */
    if (set_current_l3_rule_count(dir,current_rules_count)==FALSE) {
        DBG_PRINT("Fail to set_current_l3_rule_count");
        return FALSE;
    }
    
    return TRUE;
}
/******************** REMOVE *************************************************/
/*************** remove_l3_rule ***********************************************
* Dependent on the given direction, get corresponding database pointer, current depth and max depth
* Verify that requested rule's position is IN range and that there ARE rules to be removed
* When a rule is removed NOT from the end of table, 
* copy records one by one from the removed position to the end overwriting eachother
* and update their rule number field.
* Zero the last record in the database 
* update current database depth
******************************************************************************/
uint8_t remove_l3_rule(tpma_direction_t dir, uint32_t rule_num){

    tpma_l3_rule_t *db;
    uint32_t        current_rules_count; 
    uint32_t        max_rules_count; 
    uint32_t        i; 
    

    /*Dependent on the given direction, get corresponding database pointer, current depth and max depth*/
    if(get_l3_db_parameters(dir, &db, &current_rules_count, &max_rules_count)==FALSE){
        DBG_PRINT("fail to  get_l3_db_parameters");
        return FALSE;
    }
    /*verify rule number is IN range*/
    if (rule_num > current_rules_count) {
        return FALSE;
    }
    /*verify there AER rules to be removed */
    if (current_rules_count == 0) {
        return FALSE;
    }

   /*if not last copy following rules on it  */
    if (rule_num < (current_rules_count -1)) {
        for (i = rule_num; i<current_rules_count; i++) {
            /*copy records one by one from the removed position to the end overwriting eachother*/
            memcpy(&db[i], &db[i+1], sizeof(tpma_l3_rule_t));
            /*and update their rule number field*/
            db[i].rule_number = i;
        }
    }
    /*Zero the last record in the database */
    memset(&db[current_rules_count-1],0, sizeof(tpma_l3_rule_t));

    current_rules_count --;

    if (set_current_l3_rule_count(dir,current_rules_count)==FALSE) {
        DBG_PRINT("Fail to set_current_l3_rule_count");
        return FALSE;
    }
    
    return TRUE;


}
/******************** FIND ****************************************************/
/*************** find_first_l3_rule_by_key*******************************************
* Dependent on the given direction, get corresponding database pointer and current depth
* key refers to the OMCI indication of bridge_id, source_tp and destination_tp
* these identifiers are composed of mecless & instance id therefor can't be zero
* When source_tp is set to Zero it is used as an indication to match bridge_id ONLY
* when a full match is preformed, source_tp and destination_tp can cange their roles
******************************************************************************/

uint8_t find_first_l3_rule_by_key(tpma_l2_key_t *key, tpma_direction_t dir, uint32_t *rule_num){

    tpma_l3_rule_t *db;
    uint32_t        current_rules_count; 
    uint8_t         found = FALSE; 
    uint32_t        i; 

    /*Dependent on the given direction, get corresponding database pointer and current depth*/
    if(get_l3_db_parameters(dir,&db,&current_rules_count,NULL)==FALSE){
        DBG_PRINT("Fail to get_l3_db_parameters");
        return  FALSE;
    }

    /*key refers to the OMCI indication of bridge_id, source_tp and destination_tp
    these identifiers are composed of mecless & instance id therefor can't be zero*/  

    found = FALSE;
    for (i=0; i<current_rules_count; i++) {
        if (key->bridge_id == db[i].key.bridge_id) {

            /*When source_tp is set to Zero it is used as an indication to match bridge_id ONLY*/
            if (key->source_tp == 0) {
                *rule_num = db[i].rule_number;
                found = TRUE; 
                break;/*find first record with the same bridgeId*/
            }
            /*when a full match is preformed, source_tp and destination_tp can cange their roles*/
            if ((key->source_tp == db[i].key.source_tp)|| 
                (key->source_tp == db[i].key.destination_tp)||
                (key->destination_tp == db[i].key.destination_tp)||
                (key->destination_tp == db[i].key.source_tp)){
                *rule_num = db[i].rule_number;
                found = TRUE; 
                break;/*find first record with the same bridgeId*/
            }
        }
    }
    return (found);
}

/******************** PRINT **************************************************/
/*************** print_l2_rules_by_bridge*******************************************
* Get upstream database pointer and current depth
* bridge_id refers to the OMCI indication of bridge_id, source_tp and destination_tp
* these identifiers are composed of mecless & instance id therefor can't be zero
* When source_tp is set to Zero it is used as an indication to match bridge_id ONLY
* upstream rules are sorted by thier PHYSICAL source ports, which corelates to their bridge_id
* therfore upstream database scan can stop on the missmatch of the bridge
* Get downstream database pointer and current depth
* downstream rules are all one group and are connected to the bridge by thier gem port
* therfore downstream database must be scanned from start to end
******************************************************************************/

void print_l3_rules_by_bridge(uint32_t bridge_id){

    uint32_t        i;
    uint32_t        rule_num;
    tpma_l2_key_t   key;
    tpma_l3_rule_t *db;
    uint32_t        current_rules_count; 
    uint32_t        max_rules_count; 

/*
bridge_id refers to the OMCI indication of bridge_id, source_tp and destination_tp
* these identifiers are composed of mecless & instance id therefor can't be zero
* When source_tp is set to Zero it is used as an indication to match bridge_id ONLY
*/
  
    key.bridge_id = bridge_id;
    key.source_tp = 0;
    key.destination_tp = 0;
    

        /*Get upstream database pointer and current depth*/
    if(get_l3_db_parameters(TPMA_DIRECTION_US,&db,&current_rules_count,&max_rules_count)==FALSE){
        DBG_PRINT("Fail to get_l3_db_parameters");
        return ;
    }

    PRINT( "\n\r\n\r-------------------    L3 UPstream rules - Bridge %x   ------------------\n\r", bridge_id); 
    if (find_first_l3_rule_by_key(&key, TPMA_DIRECTION_US, &rule_num)){
        for (i = rule_num; i<current_rules_count; i++) {
            /*upstream rules are sorted by thier PHYSICAL source ports, which corelates to their bridge_id
                therfore upstream database scan can stop on the missmatch of the bridge*/
            if (db[i].key.bridge_id != bridge_id) {
                break;
            }
            PRINT( "Rule Number --------------------  %d", db[i].rule_number); 
            PRINT( "TPMA idx -----------------------  %d", db[i].idx); 
            PRINT( "Bridge ID ----------------------  %d", db[i].key.bridge_id); 
            PRINT( "source TP ----------------------  %d", db[i].key.source_tp); 
            PRINT( "destination TP -----------------  %d", db[i].key.destination_tp); 
            PRINT( "source port --------------------  %x", db[i].src_port); 
            PRINT( "TPMA parse fields---------------  %x", db[i].parse_rule_bm); 
            /*l3 key parameters*/
            if (db[i].parse_rule_bm & TPM_L2_PARSE_ETYPE) {
                PRINT( "TPMA l3 key Ethr type ----------  0x%x", db[i].l3_key.ether_type_key); 
            }
            if (db[i].parse_rule_bm & TPM_L2_PARSE_PPPOE_SES) {
                PRINT( "TPMA l3 key PPPoE session ----------  %d", db[i].l3_key.pppoe_key.ppp_session); 
            }
            if (db[i].parse_rule_bm & TPM_L2_PARSE_PPPOE_SES) {
                PRINT( "TPMA l3 key PPPoE protocol ---------  %d", db[i].l3_key.pppoe_key.ppp_proto); 
            }
            
            PRINT( "rule type ----------------------  %d", db[i].rule_type); 
            PRINT( "*********************************************************************\n\r"); 
        }
    }
    PRINT( "\n\r\n\r-------------------    L3 DOWNstream rules - Bridge %x   ------------------\n\r", bridge_id); 

    /* get downstream database pointer and current depth*/
    if(get_l3_db_parameters(TPMA_DIRECTION_US,&db,&current_rules_count,&max_rules_count)==FALSE){
        DBG_PRINT("Fail to get_l3_db_parameters");
        return ;
    }

    if (find_first_l3_rule_by_key(&key, TPMA_DIRECTION_DS, &rule_num)){
        for (i = rule_num; i<current_rules_count; i++) {
            /*downstream rules are all one group and are connected to the bridge by thier gem port
             therfore downstream database must be scanned from start to end*/
            if (db[i].key.bridge_id == bridge_id) {
                PRINT( "Rule Number --------------------  %d", db[i].rule_number); 
                PRINT( "TPMA idx -----------------------  %d", db[i].idx); 
                PRINT( "Bridge ID ----------------------  %d", db[i].key.bridge_id); 
                PRINT( "source TP ----------------------  %d", db[i].key.source_tp); 
                PRINT( "destination TP -----------------  %d", db[i].key.destination_tp); 
                PRINT( "source port --------------------  %x", db[i].src_port); 
                PRINT( "TPMA parse fields---------------  %x", db[i].parse_rule_bm); 
                /*l3 key parameters*/
                if (db[i].parse_rule_bm & TPM_L2_PARSE_ETYPE) {
                    PRINT( "TPMA l3 key Ethr type ----------  0x%x", db[i].l3_key.ether_type_key); 
                }
                if (db[i].parse_rule_bm & TPM_L2_PARSE_PPPOE_SES) {
                    PRINT( "TPMA l3 key PPPoE session ----------  %d", db[i].l3_key.pppoe_key.ppp_session); 
                }
                if (db[i].parse_rule_bm & TPM_L2_PARSE_PPPOE_SES) {
                    PRINT( "TPMA l3 key PPPoE protocol ---------  %d", db[i].l3_key.pppoe_key.ppp_proto); 
                }
                
                PRINT( "rule type ----------------------  %d", db[i].rule_type); 
                PRINT( "*********************************************************************\n\r"); 
            }
        }
    }
}
/*************** print_l2_db*******************************************
* Dependent on the given direction, get corresponding database pointer and current depth
* if there ARE rules to be print, print headline and start loop
* Function receives a rule number to start from and number of rules to be printed
* Called from print_all_l2_us & print_all_l2_ds
******************************************************************************/
uint8_t print_l3_db(tpma_direction_t dir,uint32_t start_rule_num, uint32_t rules_count){

    tpma_l3_rule_t *db;
    uint32_t        i;
    uint32_t        current_rules_count;

    /*Dependent on the given direction, get corresponding database pointer and current depth*/
    if(get_l3_db_parameters(TPMA_DIRECTION_US,&db,&current_rules_count,NULL)==FALSE){
        DBG_PRINT("Fail to get_l3_db_parameters");
        return  FALSE;
    }


    for (i = start_rule_num; i<(start_rule_num+rules_count)&&i<current_rules_count; i++) {
        PRINT( "Rule Number --------------------  %d", db[i].rule_number); 
        PRINT( "TPMA idx -----------------------  %d", db[i].idx); 
        PRINT( "Bridge ID ----------------------  %d", db[i].key.bridge_id); 
        PRINT( "source TP ----------------------  %d", db[i].key.source_tp); 
        PRINT( "destination TP -----------------  %d", db[i].key.destination_tp); 
        PRINT( "source port --------------------  %x", db[i].src_port); 
        PRINT( "TPMA parse fields---------------  0x%x", db[i].parse_rule_bm); 
        /*l3 key parameters*/
        if (db[i].parse_rule_bm & TPM_L2_PARSE_ETYPE) {
            PRINT( "TPMA l3 key Ethr type ----------  0x%x", db[i].l3_key.ether_type_key); 
        }
        if (db[i].parse_rule_bm & TPM_L2_PARSE_PPPOE_SES) {
            PRINT( "TPMA l3 key PPPoE session ------  %d", db[i].l3_key.pppoe_key.ppp_session); 
        }
        if (db[i].parse_rule_bm & TPM_L2_PARSE_PPPOE_SES) {
            PRINT( "TPMA l3 key PPPoE protocol ----  %d", db[i].l3_key.pppoe_key.ppp_proto); 
        }
        
        PRINT( "rule type ----------------------  %d", db[i].rule_type); 
        PRINT( "*********************************************************************\n\r"); 
    }
    return TRUE;

}
void print_all_l3_us()
{
    uint32_t current_rule_count;
    if (get_current_l3_rule_count(TPMA_DIRECTION_US, &current_rule_count)==FALSE) {
        DBG_PRINT("Fail to get_current_l3_rule_count");
        return;
    }

   print_l3_db(TPMA_DIRECTION_US,0,current_rule_count); 
}
void print_all_l3_ds()
{
    uint32_t current_rule_count;
    if (get_current_l3_rule_count(TPMA_DIRECTION_DS, &current_rule_count)==FALSE) {
        DBG_PRINT("Fail to get_current_l3_rule_count");
        return;
    }

   print_l3_db(TPMA_DIRECTION_DS,0,current_rule_count); 
}
void print_all_l3()
{
   print_all_l3_us(); 
   print_all_l3_ds(); 
}


/******************************************************************************/
/********************L4 access functions***************************************/
/******************************************************************************/
/*********************** MC ***************************************************/
/******************************************************************************/

/******************** INSERT **************************************************/
/*************** insert_L2_rule ***********************************************
* get corresponding database pointer, current depth and max depth
* Verify that there IS room, and that requested rule's position is IN range
* Rules are set one after the other WITHOUT gaps, and are sorted by thier PHYSICAL source port
* When a rule is set in already populated position, I copy records one by one from the end to make room
* and update their rule number field.
* Zero the record that needs to be set
* populate the new record 
* update current database depth
******************************************************************************/
uint8_t insert_IPv4_mc_rule(uint32_t rule_num, tpma_IPv4_mc_rule_t* new_rule){
  
    uint32_t                current_rules_count;
    uint32_t                max_rules_count;
    tpma_IPv4_mc_rule_t*    db;

    /*get corresponding database pointer, current depth and max depth*/
    get_l4_mc_db_parameters(&db,&current_rules_count,&max_rules_count);
    
    /*check if has room*/
    if (current_rules_count >= max_rules_count) {
        DBG_PRINT("no room in IPv4 MC db");
        return FALSE;
    }
     /*check if rule_num in range 
    Rules are set one after the other WITHOUT gaps, and are sorted by thier PHYSICAL source port*/
    if ((rule_num >= max_rules_count)||(rule_num > current_rules_count)) {
        DBG_PRINT("rule number is out of range");
        return FALSE;
    }

    /*just add it ALWAYS last*/
    db[current_rules_count].stream_number   = rule_num;
   
    current_rules_count++;

    set_current_l4_mc_rule_count(current_rules_count);
    
    return TRUE;
}

/*************** remove_IPv4_mc_rule ***********************************************
* get corresponding database pointer, current depth and max depth
* Verify that requested rule's position is IN range and that there ARE rules to be removed
* When a rule is removed NOT from the end of table, 
* copy records one by one from the removed position to the end overwriting eachother
* and update their rule number field.
* Zero the last record in the database 
* update current database depth
******************************************************************************/
uint8_t remove_IPv4_mc_rule(uint32_t rule_num){

    uint32_t                current_rules_count;
    uint32_t                i;
    tpma_IPv4_mc_rule_t*    db;

    /*get corresponding database pointer, current depth and max depth*/
    get_l4_mc_db_parameters(&db,&current_rules_count,NULL);

     /*verify rule number is IN range*/
    if (rule_num > current_rules_count) {
        return FALSE;
    }
    /*verify there AER rules to be removed */
    if (current_rules_count == 0) {
        return FALSE;
    }

   /*if not last copy following rules on it  */
    if (rule_num < (current_rules_count -1)) {
        for (i = rule_num; i<current_rules_count; i++) {
            /*copy records one by one from the removed position to the end overwriting eachother*/
            memcpy(&db[i], &db[i+1], sizeof(tpma_IPv4_mc_rule_t));
            /*and update their rule number field*/
            db[i].stream_number = i;
        }
    }
    /*Zero the last record in the database */
    memset(&db[current_rules_count-1],0, sizeof(tpma_IPv4_mc_rule_t));

    current_rules_count --;
   

    set_current_l4_mc_rule_count(current_rules_count);

    
    return TRUE;
}

/******************** FIND ***************************************************/
/*************** find_IPv4_mc_rule_by_key*******************************************
* get corresponding database pointer and current depth
* key refers to the source and destination IP and the vlan id 
******************************************************************************/

uint8_t find_IPv4_mc_rule_by_key(tpma_l2_key_t *key, uint32_t *rule_num){

    uint32_t                current_rules_count;
    tpma_IPv4_mc_rule_t*    db;
    uint32_t                i;
    uint8_t                 found;

    found = FALSE;
    /*get corresponding database pointer and current depth*/
    get_l4_mc_db_parameters(&db,&current_rules_count,NULL);

    for (i=0; i<current_rules_count; i++) {
        /* key refers to the source and destination IP and the vlan id   */
       if( memcmp(&(db[i].key), key, sizeof(tpma_IPv4_mc_key_t)) == 0){
           *rule_num = i;
           found = TRUE;
           break;
       }
    }
    return found;

}

/*************** find_IPv4_mc_rule_by_handle*******************************************
* get corresponding database pointer and current depth
* On each request from the L2DA to insert a rule, a handle is return (rule's stream number)
* this handle is unique and is returned from the TPM 
* This function is used to find a specific rule by its handle, mainly in order to remove it
******************************************************************************/
uint8_t find_IPv4_mc_rule_by_handle(uint32_t handle, uint32_t *rule_num){

    uint32_t                current_rules_count;
    tpma_IPv4_mc_rule_t*    db;
    uint32_t                i;
    uint8_t                 found;

    found = FALSE;

    /*get corresponding database pointer and current depth*/
    get_l4_mc_db_parameters(&db,&current_rules_count,NULL);

    for (i=0; i<current_rules_count; i++) {
        /*On each request from the L2DA to insert a rule, a handle is return (rule's stream number)*/
       if(db[i].stream_number == handle){
           *rule_num = i;
           found = TRUE;
           break;
       }
    }
    return found;

}

/*************** print_l2_db*******************************************
* Dependent on the given direction, get corresponding database pointer and current depth
* if there ARE rules to be print, print headline and start loop
* Function receives a rule number to start from and number of rules to be printed
******************************************************************************/
void print_l4_db(){

    uint32_t                current_rules_count;
    uint32_t                max_rules_count;
    tpma_IPv4_mc_rule_t*    db;
    uint32_t                i;

    get_l4_mc_db_parameters(&db,&current_rules_count,&max_rules_count);


    if (current_rules_count <= 0 ) {
        return;
    }
    PRINT( "\n\r\n\r-------------------    L4 multicast rules ------------------\n\r"); 
    for (i = 0; i<current_rules_count; i++) {
        print_l4_rule("",&db[i]);
      
    }
    return;

}
void print_l4_rule(char* header, tpma_IPv4_mc_rule_t* l4_rule){
     PRINT( "%s Stream Number ------------------  %d",header, l4_rule->stream_number); 
     PRINT( "%s VID ----------------------------  %d",header, l4_rule->key.vlan_id); 
     PRINT( "%s Source IP ----------------------  %d.%d.%d.%d",header,l4_rule->key.IPv4_src_add.ip[0],
                                                               l4_rule->key.IPv4_src_add.ip[1],
                                                               l4_rule->key.IPv4_src_add.ip[2],
                                                               l4_rule->key.IPv4_src_add.ip[3]); 
     PRINT( "%s Destination IP -----------------  %d.%d.%d.%d", header,l4_rule->key.IPv4_dest_add.ip[0],
                                                                l4_rule->key.IPv4_dest_add.ip[1],
                                                                l4_rule->key.IPv4_dest_add.ip[2],
                                                                l4_rule->key.IPv4_dest_add.ip[3]); 
}

void print_l2_acl_key(char* header, tpm_l2_acl_key_t* l2_key,tpm_parse_fields_t parse_rule_bm){

           /*l2 key parameters*/
        if (parse_rule_bm & TPM_L2_PARSE_MAC_DA) {
             PRINT( "%s MAC dest -----------  %x:%x:%x:%x:%x:%x",header,
                                                                      l2_key->mac.mac_da[0],
                                                                      l2_key->mac.mac_da[1],
                                                                      l2_key->mac.mac_da[2],
                                                                      l2_key->mac.mac_da[3],
                                                                      l2_key->mac.mac_da[4],
                                                                      l2_key->mac.mac_da[5]); 
             PRINT( "%s MAC dest mask ------  %x:%x:%x:%x:%x:%x",header,
                                                                      l2_key->mac.mac_da_mask[0], 
                                                                      l2_key->mac.mac_da_mask[1], 
                                                                      l2_key->mac.mac_da_mask[2], 
                                                                      l2_key->mac.mac_da_mask[3], 
                                                                      l2_key->mac.mac_da_mask[4], 
                                                                      l2_key->mac.mac_da_mask[5]);
        }
        if (parse_rule_bm & TPM_L2_PARSE_MAC_SA) {
            PRINT( "%s MAC source ---------  %x:%x:%x:%x:%x:%x",header,
                                                                     l2_key->mac.mac_sa[0],   
                                                                     l2_key->mac.mac_sa[1], 
                                                                     l2_key->mac.mac_sa[2], 
                                                                     l2_key->mac.mac_sa[3], 
                                                                     l2_key->mac.mac_sa[4], 
                                                                     l2_key->mac.mac_sa[5]);
            PRINT( "%s MAC source mask ----  %x:%x:%x:%x:%x:%x",header,
                                                                     l2_key->mac.mac_sa_mask[0],  
                                                                     l2_key->mac.mac_sa_mask[1],  
                                                                     l2_key->mac.mac_sa_mask[2],  
                                                                     l2_key->mac.mac_sa_mask[3],  
                                                                     l2_key->mac.mac_sa_mask[4],  
                                                                     l2_key->mac.mac_sa_mask[5]); 
        }
        if (parse_rule_bm & TPM_L2_PARSE_ONE_VLAN_TAG) {
            PRINT( "%s VLAN1 type ---------  0x%x",header, l2_key->vlan1.tpid); 
            PRINT( "%s VLAN1 ID -----------  %d"  ,header, l2_key->vlan1.vid); 
            PRINT( "%s VLAN1 ID mask ------  0x%x",header, l2_key->vlan1.vid_mask); 
            PRINT( "%s VLAN1 cfi ----------  %d"  ,header, l2_key->vlan1.cfi); 
            PRINT( "%s VLAN1 cfi mask -----  0x%x",header, l2_key->vlan1.cfi_mask); 
            PRINT( "%s VLAN1 pbit ---------  %d"  ,header, l2_key->vlan1.pbit); 
            PRINT( "%s VLAN1 pbit mask ----  0x%x",header, l2_key->vlan1.pbit_mask); 
        }
        if (parse_rule_bm & TPM_L2_PARSE_TWO_VLAN_TAG) {
            PRINT( "%s VLAN1 type ---------  0x%x",header, l2_key->vlan1.tpid); 
            PRINT( "%s VLAN1 ID -----------  %d"  ,header, l2_key->vlan1.vid); 
            PRINT( "%s VLAN1 ID mask ------  0x%x",header, l2_key->vlan1.vid_mask); 
            PRINT( "%s VLAN1 cfi ----------  %d"  ,header, l2_key->vlan1.cfi); 
            PRINT( "%s VLAN1 cfi mask -----  0x%x",header, l2_key->vlan1.cfi_mask); 
            PRINT( "%s VLAN1 pbit ---------  %d"  ,header, l2_key->vlan1.pbit); 
            PRINT( "%s VLAN1 pbit mask ----  0x%x",header, l2_key->vlan1.pbit_mask); 
            PRINT( "%s VLAN2 type ---------  0x%x",header, l2_key->vlan2.tpid); 
            PRINT( "%s VLAN2 ID -----------  %d"  ,header, l2_key->vlan2.vid); 
            PRINT( "%s VLAN2 ID mask ------  0x%x",header, l2_key->vlan2.vid_mask); 
            PRINT( "%s VLAN2 cfi ----------  %d"  ,header, l2_key->vlan2.cfi); 
            PRINT( "%s VLAN2 cfi mask -----  0x%x",header, l2_key->vlan2.cfi_mask); 
            PRINT( "%s VLAN2 pbit ---------  %d"  ,header, l2_key->vlan2.pbit); 
            PRINT( "%s VLAN2 pbit mask ----  0x%x",header, l2_key->vlan2.pbit_mask); 
        }
        if (parse_rule_bm & TPM_L2_PARSE_ETYPE) {
            PRINT( "%s Ethr type ----------  0x%x",header, l2_key->ether_type); 
        }
        if (parse_rule_bm & TPM_L2_PARSE_GEMPORT) {
            PRINT( "%s gem port -----------  %x", header, l2_key->gem_port); 
        }
}
                        
