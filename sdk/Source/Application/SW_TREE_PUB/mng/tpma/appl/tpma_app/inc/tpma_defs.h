/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _TPMA_DEFS_H_
#define _TPMA_DEFS_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Include Files
------------------------------------------------------------------------------*/

#include "tpma_types.h"

/* Definitions
------------------------------------------------------------------------------*/
#define tpma_max_etries_per_section 100
/* Enums                              
------------------------------------------------------------------------------*/ 

/* Structs
------------------------------------------------------------------------------*/

/* Global variables
------------------------------------------------------------------------------*/

/*ownership*/
extern uint32_t  tpma_owner_id_l2;
extern uint32_t  tpma_owner_id_l3;
extern uint32_t  tpma_owner_id_l4;
/*database*/
extern tpma_l2_rule_t      *tpma_l2_us_arr;
extern tpma_l2_rule_t      *tpma_l2_ds_arr;
extern tpma_l3_rule_t      *tpma_l3_us_arr;
extern tpma_l3_rule_t      *tpma_l3_ds_arr;
extern tpma_IPv4_mc_rule_t *tpma_l4_mc_arr;
/*entries count*/
extern uint32_t tpma_number_of_l2_us_rules;
extern uint32_t tpma_number_of_l2_ds_rules;
extern uint32_t tpma_number_of_l3_us_rules;
extern uint32_t tpma_number_of_l3_ds_rules;
extern uint32_t tpma_number_of_ipv4_mc_rules;
/*maximal count*/
extern uint32_t tpma_max_number_of_l2_us_rules;
extern uint32_t tpma_max_number_of_l2_ds_rules;
extern uint32_t tpma_max_number_of_l3_us_rules;
extern uint32_t tpma_max_number_of_l3_ds_rules;
extern uint32_t tpma_max_number_of_ipv4_mc_rules;

extern uint32_t tpma_omcc_port;
/* Global functions
------------------------------------------------------------------------------*/

/* Prototypes
------------------------------------------------------------------------------*/  

#ifdef __cplusplus
}
#endif


#endif


