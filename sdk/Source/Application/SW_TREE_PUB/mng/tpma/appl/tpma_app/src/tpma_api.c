/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

 

#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>


#include "tpm_api.h"
#include "tpma_api.h"
#include "globals.h"


#define first_phase 1
#define MNG_DEBUG 1



/******************************************************************************/
/********************************** debug functions ***************************/
/******************************************************************************/
/*******************************************************************************

/*******************************************************************************
* print_input_params()
*
* DESCRIPTION:      
*                  prints input parameters given bu omci (l2da) 
*
*
* INPUTS:
*   Get all parameters  received from omci when setting a rule 
*   ***********     for debug   ***************************
*   if MNG_DEBUG is off don't print
* tpma_l2_rule_t* input_rule containing:
*       tpma_l2_key_t          *service_key - bridge_id
*                                           - source tp
*                                           - dest tp
*       tpm_src_port_type_t     src_port    - tpm enum                 
*       tpma_parse_command_t    input_parse_commands_bm  - match labels   
*       tpm_l2_acl_key_t       *parse_values             - match values
*       tpm_pkt_frwd_t         *dest                     - destination (gem, queue, tcont, port)
*   tpm_pkt_mod_t          *mod                      - vlan modification 
*   tpm_pkt_action_t        action                   - drop ...                     
*   uint32_t                tpma_handle              - action id given from omci 4 debug
*
*
* OUTPUTS:
*
* RETURNS:
* void
* COMMENTS:
*
*******************************************************************************/
void print_input_params(tpma_l2_rule_t*         new_l2_rule, 
                        tpm_pkt_mod_t*          mod,
                        tpm_pkt_action_t        action,
                        uint32_t                tpma_handle){

    #ifdef MNG_DEBUG
        return;
    #endif

     PRINT("\n OMCI key: bridge = %x; srcTP = %x; dstTp = %x\n", new_l2_rule->key.bridge_id,
                                                                 new_l2_rule->key.source_tp,
                                                                 new_l2_rule->key.destination_tp);
           
     PRINT("Src port  = %d; parse commands bitmap = %d\n",new_l2_rule->src_port,
                                                          new_l2_rule->parse_rule_bm);  

     print_l2_acl_key("TPMA L2 key", &(new_l2_rule->l2_key), new_l2_rule->parse_rule_bm);
    
     if (mod->vlan_mod.vlan_op != VLANOP_NOOP ) {
         PRINT("\n modification vlan_op = %x",mod->vlan_mod.vlan_op);
         
         PRINT("\n mod v1 out tpid = %x; vid = %d; vid mask = %x; cfi = %d; cfi mask = %d; pbit = %d; pbit = %x",           
                                                                               mod->vlan_mod.vlan1_out.tpid,           
                                                                               mod->vlan_mod.vlan1_out.vid,           
                                                                               mod->vlan_mod.vlan1_out.vid_mask,           
                                                                               mod->vlan_mod.vlan1_out.cfi,           
                                                                               mod->vlan_mod.vlan1_out.cfi_mask,           
                                                                               mod->vlan_mod.vlan1_out.pbit);      

         PRINT("\n mod v2 out tpid = %x; vid = %d; vid mask = %x; cfi = %d; cfi mask = %d; pbit = %d; pbit = %x",           
                                                                               mod->vlan_mod.vlan2_out.tpid,           
                                                                               mod->vlan_mod.vlan2_out.vid,           
                                                                               mod->vlan_mod.vlan2_out.vid_mask,           
                                                                               mod->vlan_mod.vlan2_out.cfi,           
                                                                               mod->vlan_mod.vlan2_out.cfi_mask,           
                                                                               mod->vlan_mod.vlan2_out.pbit); 

     }
     
     PRINT("\n action  = %d; *tpma_handle = %d",action, tpma_handle);

     
                              

}
/******************************************************************************/
/**********************  utility functions    *********************************/
/******************************************************************************/
/******************************************************************************/

/*******************************************************************************
* add_new_l3_rule()
*
* DESCRIPTION:      general procedure that sets a rule in the layer 3 PnC and updates the l3 database
*                   please note that l3 section is NOT implemented in first phase, 
*                   hence if first_phase flag is on the function return right on the start 
*                   First, debug prints on input parameters 
*                   Set rule into the PnC
*                   On success, update return rule index value & rule number
*                   and update TPMA database. 
*
* INPUTS:
* dir               - direction indicationg upstream or downstream
* rule_number       - rule position in the PnC
* parse_flags_bm    - set as a result of the primary L2 filtering - bitmap
* new_rule          - pointer to the newly set rule already holds the:
*                           OMCI key:       bridge_id, source_tp & destination_tp
*                           source port:    TPM enumeration
*                           parse_rule_bm:  match fields bitmap
*                           l3_key:         match up values  
*                           rule_type:      TPMA indicator, since l3 section is configured as an ether type distributer
*                                           l3 section rules either pass the rules onto the ipv4 or ipv6 section and there these rules are matched 
*                                           between multicast and non multicast frames. Otherwise, l3 section ether type rules are ALWAYS set to drop
*                                           and therfore are set as done.
* action_drop      - I pass this parameter as uint32 to maintain alignment to 4 byte, when zero - pass to next phase (section) Otherwise, drop the frame
* next_phase       - First, rules are stripped and matched from the layer 2 and then forwarded to the l3 section to distribute by ether type (ipv4 or ipv6 section). 
*                       Otherwise these frames are dropped. So possible values on this parameter are: STAGE_IPv4, STAGE_IPv6 or STAGE_DONE
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.*
* COMMENTS:
*
*******************************************************************************/
tpma_proc_status_t add_new_l3_rule(tpma_direction_t          dir, 
                                   uint32_t                  rule_num,
                                   tpm_parse_flags_t         parse_flags_bm,
                                   tpma_l3_rule_t*           new_rule,
                                   uint32_t                  action_drop,
                                   tpm_parse_stage_t         next_phase){


        tpm_error_code_t    ret_val;
        uint32_t            rule_idx;
        uint32_t            l3_owner_id;
        /*general procedure that sets a rule in the layer 3 PnC and updates the l3 database*/

        /*please note that l3 section is NOT implemented in first phase, 
         hence if first_phase flag is on the function return right on the start */
         #ifdef first_phase
            return TPMAPROC_OK;
        #endif

        if(get_l3_owner_id(&l3_owner_id)==FALSE){
            DBG_PRINT("Fail to get_l3_owner_id");
            return TPMAPROC_GENERROR;
        }
        
        /*First, debug prints on input parameters */
        DBG_PRINT("\n owner id  = %d",l3_owner_id);
        DBG_PRINT("\n src port  = %d",new_rule->src_port);
        DBG_PRINT("\n rule num  = %d",rule_num);
        DBG_PRINT("\n L3 key ether type = %x",new_rule->l3_key.ether_type_key);
        DBG_PRINT("\n next_phase = %d",next_phase);

        /*Set rule into the PnC*/
        ret_val = tpm_add_l3_type_acl_rule(l3_owner_id,
                                           new_rule->src_port,
                                           rule_num,
                                           &rule_idx,
                                           new_rule->parse_rule_bm,
                                           parse_flags_bm,
                                           &(new_rule->l3_key),
                                           (uint8_t)action_drop,
                                           next_phase);

        if (ret_val != TPM_RC_OK) {
            DBG_PRINT("Fail to tpm_add_l3_type_acl_rule %d ret_val = %d",rule_num, ret_val);
            return ((tpma_proc_status_t)ret_val);
        }
        /*On success, update return rule index value & rule number*/
        new_rule->idx = rule_idx;
        new_rule->rule_number = rule_num;
    
        /*and update TPMA database*/
        if(insert_l3_rule(dir,rule_num, new_rule)== FALSE){
            DBG_PRINT("Fail to insert_l3_rule %d ",rule_num);
            return TPMAPROC_DATABASE_FULL;  
        }
        return TPMAPROC_OK;

}
/*******************************************************************************
* delete_l3_rule()
*
* DESCRIPTION:      general procedure that removes a rule in the layer 3 PnC and updates the l3 database
*                   First retrieve  pointer to the right database with respect to the direction
*                   Retreive parameters regarding the rule to be removed
*                   try to delete the rule from the PnC
*                   On success, update TPMA database
*
* INPUTS:
* dir               - direction indicationg upstream or downstream
* rule_number       - rule position in the PnC
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.*
* COMMENTS:
*
*******************************************************************************/
tpma_proc_status_t delete_l3_rule(tpma_direction_t      dir, 
                                  uint32_t              rule_num){
     tpma_l3_rule_t*     db;
     tpm_error_code_t    ret_val;
     tpma_proc_status_t  tpma_ret_val;
     uint32_t            l3_owner_id;

     /*general procedure that removes a rule in the layer 2 PnC and updates the l2 database*/

     /*First retrieve  pointer to the right database with respect to the direction*/
     if (get_l3_db(dir,&db)== FALSE) {
         DBG_PRINT("Fail to get_l3_db dir = %d",dir);
         return TPMAPROC_GENERROR;
     }
     if(get_l3_owner_id(&l3_owner_id)==FALSE){
         DBG_PRINT("Fail to get_l3_owner_id");
         return TPMAPROC_GENERROR;
     }
     /*Retreive parameters regarding the rule to be removed    
       try to delete the rule from the PnC */

     ret_val = tpm_del_l3_type_acl_rule(l3_owner_id,
                                        db[rule_num].src_port,
                                        rule_num,
                                        db[rule_num].parse_rule_bm,
                                        &(db[rule_num].l3_key));
     if (ret_val != TPM_RC_OK) {
         tpma_ret_val =  ((tpma_proc_status_t)ret_val);
         DBG_PRINT("Fail to tpm_del_l3_type_acl_rule %d", ret_val);
         return (tpma_ret_val);
     }


     /*On success, update TPMA database*/
     if (remove_l3_rule(dir, rule_num) == FALSE){
         DBG_PRINT("fail to remove rule %d", rule_num);
         tpma_ret_val =  TPMAPROC_DELETE_FAILED;
     }
 
     return TPMAPROC_OK;
}
/*******************************************************************************
* delete_l2_rule()
*
* DESCRIPTION:      general procedure that removes a rule in the layer 2 PnC and updates the l2 database
*                   First retrieve  pointer to the right database with respect to the direction
*                   Retreive parameters regarding the rule to be removed
*                   try to delete the rule from the PnC
*                   On success, update TPMA database
*
* INPUTS:
* dir               - direction indicationg upstream or downstream
* rule_number       - rule position in the PnC
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.*
* COMMENTS:
*
*******************************************************************************/
tpma_proc_status_t delete_l2_rule(tpma_direction_t      dir, 
                                  uint32_t              rule_num){
     tpma_l2_rule_t*     db;
     tpm_error_code_t    ret_val;
     tpma_proc_status_t  tpma_ret_val;
     uint32_t            l2_owner_id;   
     struct timeval      currentTimeOfDay_b4;
     struct timeval      currentTimeOfDay_after;
     /*general procedure that removes a rule in the layer 2 PnC and updates the l2 database*/

     /*First retrieve  pointer to the right database with respect to the direction*/
     if (get_l2_db(dir,&db)== FALSE) {
         DBG_PRINT("Fail to get_l2_db dir = %d",dir);
         return TPMAPROC_GENERROR;
     }
     if (get_l2_owner_id(&l2_owner_id)==FALSE) {
         DBG_PRINT("Fail to get_l2_owner_id");
         return TPMAPROC_GENERROR;
     }

    
    if (db[rule_num].parse_rule_bm & PARSECOMMAND_L2_UNTAGGED) {
        db[rule_num].parse_rule_bm &= ~(PARSECOMMAND_L2_UNTAGGED);
    }
    if (db[rule_num].parse_rule_bm & PARSECOMMAND_L2_TAGGED) {
        db[rule_num].parse_rule_bm &= ~(PARSECOMMAND_L2_TAGGED);
        if (db[rule_num].rule_length == TPMA_LENGTH_ANY_ONE_TAG) {
            db[rule_num].parse_rule_bm |= PARSECOMMAND_L2_1VLAN_TAG;
        }
        if (db[rule_num].rule_length == TPMA_LENGTH_ANY_TWO_TAG) {
            db[rule_num].parse_rule_bm |= PARSECOMMAND_L2_2VLAN_TAG;
        }
    }
    #ifndef first_phase
     gettimeofday(&currentTimeOfDay_b4, NULL);
    #endif
     /*Retreive parameters regarding the rule to be removed    
       try to delete the rule from the PnC */
     ret_val =  tpm_del_l2_prim_acl_rule (l2_owner_id,
                                          db[rule_num].src_port,
                                          db[rule_num].idx, 
                                          db[rule_num].parse_rule_bm,
                                          &(db[rule_num].l2_key));

     if (ret_val != TPM_RC_OK) {
         tpma_ret_val =  ((tpma_proc_status_t)ret_val);
         DBG_PRINT("Fail to tpm_del_l2_prim_acl_rule %d", ret_val);
         return (tpma_ret_val);
     }
     #ifndef first_phase
     gettimeofday(&currentTimeOfDay_after, NULL);
     DBG_PRINT( "After :%s %d:%d", __FUNCTION__, currentTimeOfDay_after.tv_sec-currentTimeOfDay_b4.tv_sec, 
                                                 currentTimeOfDay_after.tv_usec-currentTimeOfDay_b4.tv_usec);
     #endif

     /*On success, update TPMA database*/
     if (remove_l2_rule(dir , rule_num) == FALSE){
         DBG_PRINT("fail to remove rule %d", rule_num);
         tpma_ret_val =  TPMAPROC_DELETE_FAILED;
     }
     return TPMAPROC_OK;
}
/*******************************************************************************
* add_new_l2_rule()
*
* DESCRIPTION:      general procedure that adds a rule in the layer 2 PnC and updates the l2 database
*                   First standardize the parse rule bitmap to match the TPM parse rule bitmap
*                   which doesn't support untagged and tagged consects with respect to the rule's length
*                   try to add the rule from the PnC
*                   On success, update return rule index value 
*                   and update TPMA database
*
* INPUTS:
* dir               - direction indicationg upstream or downstream
* rule_number       - rule position in the PnC
* new_rule          - pointer to the newly set rule already holds the:
*                           OMCI key:       bridge_id, source_tp & destination_tp
*                           source port:    TPM enumeration
*                           parse_rule_bm:  match fields bitmap
*                           l2_key:         match up values  
*                           rule_action:    TPMA indicator, whether the rule is allowed in or drooped, this is used to determined the 'color' of the 'any' rules
*                                           each group (by source port) has 3 'any' rules: 2 tag 'any' rule, 1 tag 'any' rule and no tag 'any' rule. 
*                                           'any' refers to no match values in the rule. When 1 tag rles are set the 'color' (dropped black vs. allowed white) 
*                                           of the 1 tag 'any' rule must be opposite to the 'color' of the set rule. 
*                           rule_length:     Rule ordering is determined by length of the match value. length values are given in the tpma_length_t enumeration

*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.*
* COMMENTS:
*
*******************************************************************************/
tpma_proc_status_t add_new_l2_rule(tpma_direction_t       dir, 
                                   uint32_t               rule_num,
                                   tpma_l2_rule_t*        new_rule,
                                   tpm_rule_action_t*     new_action,
                                   tpm_pkt_mod_t*         mod){
    tpm_error_code_t    ret_val;
    uint32_t            rule_idx;
    tpm_parse_fields_t  new_parse_bm;
    uint32_t            l2_owner_id;   
    struct timeval      currentTimeOfDay_b4;
    struct timeval      currentTimeOfDay_after;

    /*general procedure that adds a rule in the layer 2 PnC and updates the l2 database*/

    if (get_l2_owner_id(&l2_owner_id)==FALSE) {
         DBG_PRINT("Fail to get_l2_owner_id");
         return TPMAPROC_GENERROR;
     }
    /*First standardize the parse rule bitmap to match the TPM parse rule bitmap            
    which doesn't support untagged and tagged consects with respect to the rule's length  */
    new_parse_bm = (tpm_parse_fields_t)new_rule->parse_rule_bm;
    if (new_parse_bm & PARSECOMMAND_L2_UNTAGGED) {
        new_parse_bm &= ~(PARSECOMMAND_L2_UNTAGGED);
    }
    if (new_parse_bm & PARSECOMMAND_L2_TAGGED) {
        new_parse_bm &= ~(PARSECOMMAND_L2_TAGGED);
        if (new_rule->rule_length == TPMA_LENGTH_ANY_ONE_TAG) {
            new_parse_bm |= PARSECOMMAND_L2_1VLAN_TAG;
        }
        if (new_rule->rule_length == TPMA_LENGTH_ANY_TWO_TAG) {
            new_parse_bm |= PARSECOMMAND_L2_2VLAN_TAG;
        }
    }
    /*try to add the rule from the PnC*/
      
    #ifndef first_phase
    gettimeofday(&currentTimeOfDay_b4, NULL);
    #endif
      ret_val = tpm_add_l2_prim_acl_rule(l2_owner_id,
                                         new_rule->src_port,
                                         rule_num,
                                         &rule_idx,
                                         new_parse_bm,
                                         &(new_rule->l2_key),
                                         &(new_rule->pkt_frwd),
                                         mod,
                                         new_action);

      if (ret_val != TPM_RC_OK) {
           DBG_PRINT("fail tpm_add_l2_prim_acl_rule %d ",  ret_val);
           return ((tpma_proc_status_t)ret_val);
       }
      #ifndef first_phase
       gettimeofday(&currentTimeOfDay_after, NULL);
       DBG_PRINT( "After :%s %d:%d", __FUNCTION__, currentTimeOfDay_after.tv_sec-currentTimeOfDay_b4.tv_sec, 
                                                   currentTimeOfDay_after.tv_usec-currentTimeOfDay_b4.tv_usec);
      #endif
      /*On success, update return rule index value */
     /*   #ifdef first_phase
        new_rule->idx = rand();
       #else
     */   new_rule->idx  = rule_idx;
     /*  #endif
     */  /*and update TPMA database*/
       if(insert_l2_rule(dir, rule_num, new_rule)==FALSE){
            DBG_PRINT("fail insert_l2_rule ");
            return TPMAPROC_DATABASE_FULL;
       }

       return TPMAPROC_OK;

}
/******************************************************************************/
/**********************  TPM l3 configuration  ********************************/
/******************************************************************************/
/******************************************************************************/

/*******************************************************************************
* add_l3_distributing_rules()
*
* DESCRIPTION:      configuring L3 section as a distribution layer :
*                   On set of an L2 rule, TPMA check if it is the first rule in the group (by source port). 
*                   If so, TPMA adds 3 rules to the L3 section parsing the Ether Type field on the same source port:
*                              Ether Type == 0x0800 , set next parsing stage to IPv4
*                              Ether Type == 0x86DD , set next parsing stage to IPv6
*                              Otherwise, set next parsing stage to Done. 
*                   please note that l3 section is NOT implemented in first phase, 
*                   hence if first_phase flag is on the function return right on the start 
*                   First, debug prints on input parameters 
*                   Retrieve database depth status   
*                   Verify that there is room for another 3 rules
*                   Construct parse flag bit map with don't care on 
*                   Construct l3 rule entry: zero it and set new values in it
*                   set new rule at the end of the section to avoid redundent transfers
*                   and add it
*                   first add distributer for IPv4 ether type then for IPv6 and then for Other ether type
*                   On success, increment rule index
*                   General TPM configuration:
*                   ALL packets first enter the L2 section (US or DS). 
*                   All L2 rules with related action is other then drop are automatically set to next parsing stage in L3 section. 
*                   There the packet is parsed on the ether Type field and distributed to the corresponding section. 
*
* INPUTS:
* dir               - direction indicationg US or DS
* *key              - Pointer holding identifier for
*                                 bridge, 
*                                 source ANI TP and 
*                                 destination UNI TP 
*                      in a format of ME class ID concutenated with instance ID
* src_port           - port from tpm_src_port_type_t enumewration
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.*
* COMMENTS:
*
*******************************************************************************/

tpma_proc_status_t add_l3_distributing_rules     (tpma_direction_t     dir,
                                                  tpma_l2_key_t        *key, 
                                                  tpm_src_port_type_t  src_port){

 

    uint32_t            rule_num;
    uint32_t            cuurent_rule_count; 
    uint32_t            max_rule_count; 
    tpma_l3_rule_t      new_rule; 
    tpm_parse_flags_t   parse_flags_bm;
    tpma_proc_status_t  ret_val;

    /*configuring L3 section as a distribution layer :                                                        
    On set of an L2 rule, TPMA check if it is the first rule in the group (by source port).                 
    If so, TPMA adds 3 rules to the L3 section parsing the Ether Type field on the same source port:        
           Ether Type == 0x0800 , set next parsing stage to IPv4                                        
           Ether Type == 0x86DD , set next parsing stage to IPv6                                        
           Otherwise, set next parsing stage to Done.                                                   
     */
    /*please note that l3 section is NOT implemented in first phase,        
    hence if first_phase flag is on the function return right on the start*/
    #ifdef first_phase
        return TPMAPROC_OK;
    #endif

    /*First, debug prints on input parameters*/
    DBG_PRINT("dirsction  = %d",dir);
    DBG_PRINT("OMCI key: bridge = %d; srcTP = %d; dstTp = %d", key->bridge_id,key->source_tp, key->destination_tp );
    DBG_PRINT("Src port  = %d",src_port);

    
    /*Retrieve database depth status  */
    if (get_current_l3_rule_count(dir,&cuurent_rule_count) == FALSE){
        DBG_PRINT("Fail to get_current_l3_rule_count");
        return TPMAPROC_GENERROR;
    }
    if (get_max_l3_rule_count(dir,&max_rule_count) == FALSE){
        DBG_PRINT("Fail to get_max_l3_rule_count");
        return TPMAPROC_GENERROR;
    }
    /*Verify that there is room for another 3 rules*/
    if (cuurent_rule_count +3 > max_rule_count) {
        DBG_PRINT("Database is full currently %d out of %d", cuurent_rule_count, max_rule_count);
        return TPMAPROC_DATABASE_FULL;
    }

    /*Construct parse flag bit map with don't care on */
    parse_flags_bm  = TPM_PARSE_FLAG_TRG_PORT_DC | 
                      TPM_PARSE_FLAG_PKT_MOD_DC |
                      TPM_PARSE_FLAG_TO_CPU_DC;

    /*Construct l3 rule entry: zero it and set new values in it*/
    memset(&new_rule,0, sizeof(tpma_l3_rule_t));
    memcpy(&new_rule.key,key,sizeof(tpma_l2_key_t));
    new_rule.parse_rule_bm          = TPM_L2_PARSE_ETYPE;
    new_rule.src_port               = src_port;
    

    /*set new rule at the end of the section to avoid redundent transfers*/
    rule_num = cuurent_rule_count;
    new_rule.rule_number            = rule_num;
    /*first add distributer for IPv4 ether type */
    new_rule.rule_type              = TPMA_L3_RULE_TYPE_IPv4;
    new_rule.l3_key.ether_type_key  = TPMA_IPv4_ETHER_TYPE;
    /*and add it*/
    ret_val =  add_new_l3_rule(dir,rule_num,parse_flags_bm,&new_rule,(uint32_t)FALSE,STAGE_IPv4);

    if (ret_val != TPMAPROC_OK) {
        DBG_PRINT("Fail to add_new_l3_rule rule No = %d ret_val = %d", rule_num,ret_val);
        return (ret_val);
    }

    rule_num++;
    new_rule.rule_number            = rule_num;
    /*then for IPv6 */
    new_rule.rule_type              = TPMA_L3_RULE_TYPE_IPv6;
    new_rule.l3_key.ether_type_key  = TPMA_IPv6_ETHER_TYPE;

    ret_val =  add_new_l3_rule(dir,rule_num,parse_flags_bm,&new_rule,(uint32_t)FALSE,STAGE_IPv6);

    if (ret_val != TPMAPROC_OK) {
        DBG_PRINT("Fail to add_new_l3_rule rule No = %d ret_val = %d", rule_num,ret_val);
        return (ret_val);
    }


    rule_num++;
    new_rule.rule_number            = rule_num;
    /*and then for Other ether type*/
    new_rule.rule_type              = TPMA_L3_RULE_TYPE_DONE;
    new_rule.l3_key.ether_type_key  = 0;

    ret_val =  add_new_l3_rule(dir,rule_num,parse_flags_bm,&new_rule,(uint32_t)FALSE,STAGE_DONE);

    if (ret_val != TPMAPROC_OK) {
        DBG_PRINT("Fail to add_new_l3_rule rule No = %d ret_val = %d", rule_num,ret_val);
        return (ret_val);
    }

 
}

/******************************************************************************/
/**********************  rule ordering logic  ********************************/
/******************************************************************************/
/******************************************************************************/

/*******************************************************************************
* set_1tag_rule_length()
*
* DESCRIPTION:      Rule ordering is based on the concept of longer first
*                   rules with more data to match should be placed BEFORE rules with less match data
*                   1 tag vlan frames are matched on thier vid and pbit 
*                   cfi is NOT a match parameter
*                   it is agreed that vid is 'longer' then pbit BUT this SHOULD be altered as a configurable decision
*                   First print input values (on debug satate)
*                   construct current input values bit map:
*                   turn bits on / off with respect to the parameter's mask 
*                   mask == 0 -> bit off
*                   mask != 0 -> bit on
*                   compare current state bit mask to predefined masks to determine the input value length
*
* INPUTS:
* input_parse_commands_bm - match feilds
* *input_parse_values   - Pointer to match values
*
* OUTPUTS:
*
* RETURNS:
*   rule's length
*
* COMMENTS:
*
*******************************************************************************/
tpma_length_t set_1tag_rule_length(tpma_parse_command_t  input_parse_commands_bm, tpm_l2_acl_key_t*  input_parse_values){
    /*1 tag vlan frames are matched on thier vid and pbit 
      cfi is NOT a match parameter                        */
    /*
    Rule ordering is based on the concept of longer first                             
    rules with more data to match should be placed BEFORE rules with less match data  


    VID1 *    PBIT1	*    Length	*     description
****************************************************************************************************************************
    X	 *    X	    *    6	    *     Both VID and PBIT are set explicitly
****************************************************************************************************************************
    X	 *    	    *    5	    *     VID is set explicitly and PBIT is don't care
****************************************************************************************************************************
    	 *    X	    *    4	    *     PBIT is set explicitly and VID is don't care
****************************************************************************************************************************
    	 *    	    *    3	    *     Any 1 tag + GEM
****************************************************************************************************************************
    	 *    	    *    2	    *     Default 'Any' 1 tag
****************************************************************************************************************************
         *          *           *     

        analyze input and turn on related bits int input
        TPMA_OUTER_VLAN_PBIT           (0x4)    
        TPMA_OUTER_VLAN_VID            (0x8)    
    */
    /*First print input values (on debug satate)*/
    #ifndef MNG_DEBUG
        print_l2_acl_key("set_1tag_rule_length", input_parse_values, PARSECOMMAND_L2_1VLAN_TAG);
    #endif

    


    uint8_t input = 0;

    /*  construct current input values bit map:                     
        turn bits on / off with respect to the parameter's mask     
        mask == 0 -> bit off                                        
        mask != 0 -> bit on              */

    if (input_parse_values->vlan1.pbit_mask != 0) {
        input |= TPMA_OUTER_VLAN_PBIT;
    }
    if (input_parse_values->vlan1.vid_mask != 0) {
        input |= TPMA_OUTER_VLAN_VID;
    }

    /*compare current state bit mask to predefined masks to determine the input value length*/
    if (input == TPMA_ONLY_OUTER_PBIT_SET) {
        return TPMA_LENGTH_ONE_TAG_ANY_VID;
    }
    if (input == TPMA_ONLY_OUTER_VID_SET) {
        return TPMA_LENGTH_ONE_TAG_ANY_PBIT;
    }
    if (input == TPMA_ONLY_OUTER_VID_OUTER_PBIT_SET) {
        return TPMA_LENGTH_ONE_TAG;
    }
    if (input == TPMA_nothing_SET) {
        if (input_parse_commands_bm & PARSECOMMAND_L2_GEM_PORT) {
            return TPMA_LENGTH_ANY_ONE_TAG_GEM;
        }
        else {
            return TPMA_LENGTH_ANY_ONE_TAG;
        }
    }



}
/*******************************************************************************
* set_2tag_rule_length()
*
* DESCRIPTION:      Rule ordering is based on the concept of longer first
*                   rules with more data to match should be placed BEFORE rules with less match data
*                   2 tag vlan frames are matched on thier outer and inner vid and pbit 
*                   cfi is NOT a match parameter
*                   it is agreed that vid is 'longer' then pbit BUT this SHOULD be altered as a configurable decision
*                   First print input values (on debug satate)
*                   construct current input values bit map:
*                   turn bits on / off with respect to the parameter's mask 
*                   mask == 0 -> bit off
*                   mask != 0 -> bit on
*                   compare current state bit mask to predefined masks to determine the input value length
*
* INPUTS:
* *input_parse_values   - Pointer to match values
* *length               - Pointer return length value
*
* OUTPUTS:
*
* RETURNS:
*   rule's length
*
* COMMENTS:
*
*******************************************************************************/
tpma_length_t set_2tag_rule_length(tpma_parse_command_t  input_parse_commands_bm, tpm_l2_acl_key_t*  input_parse_values){

    /*2 tag vlan frames are matched on thier outer and inner vid and pbit      
        cfi is NOT a match parameter                       */

    /*Rule ordering is based on the concept of longer first                              
      rules with more data to match should be placed BEFORE rules with less match data     */
/*
VID1	*   PBIT1  *   VID2	*   BIT2  *  Length	* description
************************************************************************************************************************************
X	    *   X	   *   X	*   X	  *  16	    * Both inner and outer VLAN tags are explicitly set 
************************************************************************************************************************************
X	    *   X	   *   X	*   	  *  15	    * Outer and inner VID are set explicitly and one PBIT parameter is 'don't care
X	    *   	   *   X	*   X	  *  	    * 
************************************************************************************************************************************
X	    *   X	   *   	    *         *  14	    * Outer and inner PBIT are set explicitly and one VID parameter is 'don't care
	    *   X	   *   X	*   X	  *  	    * 
************************************************************************************************************************************
X	    *   	   *   X	*   	  *  13	    * Outer and inner VID are set explicitly and outer and inner PBIT are don't care 
************************************************************************************************************************************
X	    *   X	   *   	    *         *  12	    * Single VID and a single TPBIT are explicitly set and the others are don't care
	    *   	   *   X	*   X	  *  	    * 
X	    *   	   *   	    *         *         * 
 	    *   X	   *   X	*   	  *  	    * 
************************************************************************************************************************************
	    *   X	   *   	    *         *  11	    * Outer and inner PBIT are set explicitly and two VID parameter is 'don't care
************************************************************************************************************************************
X	    *   	   *   	    *         *  10	    * Single VID is set explicitly
	    *   	   *   X	*   	  *  	    * 
************************************************************************************************************************************
	    *   X	   *   	    *         *  9	    * Single PBIT is set explicitly
		*   	   *   X	*   	  *         *  
************************************************************************************************************************************
		*   	   *   	    *         *  8	    * Any 2 tag + GEM
************************************************************************************************************************************
		*   	   *        *         *  7	    * Default 'Any' 2 tag
***********************************************************************************************************************************

       analyze input and turn on related bits int input
        TPMA_INNER_VLAN_PBIT           (0x1)    
        TPMA_INNER_VLAN_VID            (0x2)    
        TPMA_OUTER_VLAN_PBIT           (0x4)    
        TPMA_OUTER_VLAN_VID            (0x8)    
     
*/
    uint8_t input = 0;

    /*
        construct current input values bit map:                   
        turn bits on / off with respect to the parameter's mask   
        mask == 0 -> bit off                                      
        mask != 0 -> bit on                                           */

        if (input_parse_values->vlan2.pbit_mask != 0) {
            input |= TPMA_INNER_VLAN_PBIT;
        }
        if (input_parse_values->vlan2.vid_mask != 0) {
            input |= TPMA_INNER_VLAN_VID;
        }
        if (input_parse_values->vlan1.pbit_mask != 0) {
            input |= TPMA_OUTER_VLAN_PBIT;
        }
        if (input_parse_values->vlan1.vid_mask != 0) {
            input |= TPMA_OUTER_VLAN_VID;
        }


        /*compare current state bit mask to predefined masks to determine the input value length*/

        /*Single PBIT is set explicitly
        (*,*,;*,pb) OR (*,pb,;*,*)*/
        if ((input == TPMA_ONLY_INNER_PBIT_SET)||
            (input == TPMA_ONLY_OUTER_PBIT_SET)){
            return TPMA_LENGTH_TWO_TAG_SINGLE_PBIT_SET;
        }

        /*Single VID is set explicitly
        (*,*,;vid,*) OR (vid,*,;*,*)*/
        if ((input == TPMA_ONLY_INNER_VID_SET)||
            (input == TPMA_ONLY_OUTER_VID_SET)){
            return TPMA_LENGTH_TWO_TAG_SINGLE_VID_SET;
        }

        /*Outer and inner PBIT are set explicitly and two VID parameter is 'don't care
        (*,pb,;*,pb)*/
         if (input == TPMA_ONLY_OUTER_INNER_PBIT_SET){
             return  TPMA_LENGTH_TWO_TAG_TWO_PBIT_SET;
         }
        
        /*Single VID and a single TPBIT are explicitly set and the others are don't care*
        (*,*,;vid,pb) OR (*,pb,;vid,*) OR (vid,*,;*,pb) OR (vid,pb,;*,*) */
        if ((input == TPMA_ONLY_INNER_VID_INNER_PBIT_SET)||
            (input == TPMA_ONLY_INNER_VID_OUTER_PBIT_SET)||
            (input == TPMA_ONLY_OUTER_VID_INNER_PBIT_SET)||
            (input == TPMA_ONLY_OUTER_VID_OUTER_PBIT_SET)){
             return  TPMA_LENGTH_TWO_TAG_SINGLE_VID_SINGLE_PBIT_SET;
        }
        
        /*Outer and inner VID are set explicitly and outer and inner PBIT are don't care 
        (vid,*,;vid,*)*/
        if (input == TPMA_ONLY_OUTER_VID_INNER_VID_SET){
            return TPMA_LENGTH_TWO_TAG_TWO_VID_SET;
        }

        /*Outer and inner PBIT are set explicitly and one VID parameter is 'don't care
        (*,pb,;vid,pb) OR (vid,pb,;*,pb)*/
        if ((input == TPMA_ONLY_OUTER_PBIT_INNER_VID_PBIT_SET)||
            (input == TPMA_ONLY_OUTER_VID_PBIT_INNER_PBIT_SET)){
            return  TPMA_LENGTH_TWO_TAG_TWO_PBIT_SINGLE_VID_SET;
        }

        /*Outer and inner VID are set explicitly and one PBIT parameter is 'don't care
        (vid,*,;vid,pb) OR (vid,pb,;vid,*) */
         if ((input == TPMA_ONLY_OUTER_VID_INNER_VID_PBIT_SET)||
             (input == TPMA_ONLY_OUTER_VID_PBIT_INNER_VID_SET)){
            return TPMA_LENGTH_TWO_TAG_TWO_VID_SINGLE_PBIT_SET;
        }

         /*(vid,pb,;vid,pb) */
        if (input == TPMA_all_SET) {
            return  TPMA_LENGTH_TWO_TAG;
        }

        /*(*,*;*,*) */
        if(input == TPMA_nothing_SET){
            if (input_parse_commands_bm & PARSECOMMAND_L2_GEM_PORT) {
                return TPMA_LENGTH_ANY_TWO_TAG_GEM;
            }
            else {
                return TPMA_LENGTH_ANY_TWO_TAG;
            }
        }

}
/*******************************************************************************
* determine_rule_length()
*
* DESCRIPTION:      Rule ordering is based on the concept of longer first
*                   rules with more data to match should be placed BEFORE rules with less match data
*                   rules matching mac address are ALWAYS dropped and therfore are orthogonal 
*                   to other rules in the group that why these are set first
*                   rules matching 2 tag are ordered after MAC rules
*                   rules matching 1 tag are ordered after 2 tag rules 
*                   rules matching gem ports (ONLY) are ordered after 1 tag rules 
*                   rules not matching anything (untagged  frames) are ordered last in the group
*
* INPUTS:
* input_parse_commands_bm - match fields bitmap
* *input_parse_values   - Pointer to match values
* *length               - Pointer return length value
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TRUE.
* Otherwise FALSE 
* COMMENTS:
*
*******************************************************************************/
bool determine_rule_length(tpma_parse_command_t      input_parse_commands_bm, 
                           tpm_l2_acl_key_t*         input_parse_values,
                           tpma_length_t*            length){

/*Rule ordering is based on the concept of longer first                                       
  rules with more data to match should be placed BEFORE rules with less match data            */

    /*rules with more data to match should be placed BEFORE rules with less match data 
      rules matching mac address are ALWAYS dropped and therfore are orthogonal            */
    if ((input_parse_commands_bm & PARSECOMMAND_L2_MAC_DEST)|| 
        (input_parse_commands_bm & PARSECOMMAND_L2_MAC_SRC)){
        *length = TPMA_LENGTH_TWO_MAC;
        return TRUE;
    }

    /*rules matching 2 tag are ordered after MAC rules*/
    if (input_parse_commands_bm & PARSECOMMAND_L2_2VLAN_TAG) {
        *length = set_2tag_rule_length(input_parse_commands_bm,input_parse_values);
        return TRUE;  
    }
    /*rules matching 1 tag are ordered after 2 tag rules */
    if (input_parse_commands_bm & PARSECOMMAND_L2_1VLAN_TAG) {
        *length = set_1tag_rule_length(input_parse_commands_bm,input_parse_values);
        return TRUE;  
         
    }
    /*rules matching gem ports (ONLY) are ordered after 1 tag rules */
    if (input_parse_commands_bm == PARSECOMMAND_L2_GEM_PORT) {
         *length = TPMA_LENGTH_NO_TAG_GEM; 
         return TRUE;  
    }
    /*rules not matching anything (untagged  frames) are ordered last in the group*/
    if ((input_parse_commands_bm & PARSECOMMAND_L2_UNTAGGED)|| 
        (input_parse_commands_bm == 0)){
         *length = TPMA_LENGTH_NO_TAG; 
         return TRUE;  
     }

    DBG_PRINT("determine_rule_length FAIL");
    return FALSE;

}

/******************************************************************************/
/****************************  spesific cases   *******************************/
/******************************************************************************/
/******************************************************************************/
/*******************************************************************************                            
* remove_all_l3_rules_by_key()                                                                                       
*                                                                                                           
* DESCRIPTION:  Search L3 database for rules with matching bridge_id and related tp_id
*               If found,                                                                                   
*               Remove corresponding rule from TPM DB
*               Remove corresponding rule from TPMA DB
*               and search for the next one
*               There should be AT LEAST 3 L3 rules per bridge_id and related tp_id:
*               On set of an L2 rule, TPMA check if it is the first rule in the group (by source port).           
*               If so, TPMA adds 3 rules to the L3 section parsing the Ether Type field on the same source port:  
*                          Ether Type == 0x0800 , set next parsing stage to IPv4                                  
*                          Ether Type == 0x86DD , set next parsing stage to IPv6                                  
*                          Otherwise, set next parsing stage to Done.                                             
*
*     
*               If found and removed less the 3 rule function returns FALSE
*                                                                                                           
* INPUTS:                                                                                                   
* bridge_ID - ME class concatenated with instance ID  
* TP_ID      - ME class concatenated with instance ID 
* isBridgeOnly - should remove be done
*                                                                                                           
* OUTPUTS:                                                                                                  
*                                                                                                           
* RETURNS:                                                                                                  
* On success, the function returns TRUE. On error, FALSE is returned.                    
*                                                                                                           
* COMMENTS:                                                                                                 
*******************************************************************************/   

bool remove_all_l3_rules_by_key(uint32_t bridge_id ,uint32_t tp_id, bool isBridgeOnly, tpma_direction_t dir){

    bool               ret_val;
    uint32_t           rules_counter;
    uint32_t           rule_num;
    tpma_proc_status_t tpma_ret_val;
    tpma_l2_key_t      omci_key;

    ret_val         = FALSE;
    rules_counter   = 0;

     #ifdef first_phase
        return (TRUE);
    #endif

    omci_key.bridge_id = bridge_id;
    omci_key.source_tp = isBridgeOnly?0:tp_id;

    /*Search L3 DS database for rules with the same bridge id */
    ret_val = find_first_l3_rule_by_key(&omci_key,dir, &rule_num);

    while (ret_val == TRUE) {
        /*start remove */
        tpma_ret_val = delete_l3_rule(dir, rule_num);
        if (tpma_ret_val != TPMAPROC_OK) {
            DBG_PRINT("Fail to delete_l3_rule %d ret_val %d", rule_num, tpma_ret_val);
            return FALSE;
        }
        rules_counter ++;
        
        ret_val = find_first_l3_rule_by_key(&omci_key,dir, &rule_num);

    }
    if (rules_counter < 3) {
        DBG_PRINT("Fail to delete at least 3 rules, removed only %d", rules_counter);
        return  FALSE;
    }

    return TRUE;

}

/*******************************************************************************                            
* remove_by_key()                                                                                       
*                                                                                                           
* DESCRIPTION:  Search L2 database (both US and DS) for rules with matching bridge ID                       
*               If found,                                                                                   
*               Reset participating ports configuration (source and destination)                            
*               Go through the DS, US database and find rule numbers to be removed,                         
*               call  tpm_del_l2_prim_acl_rule (notice  that rules index updates as the rules are deleted)  
*                                                                                                           
* INPUTS:                                                                                                   
* bridge_ID - ME class concatenated with instance ID  
* TP_ID      - ME class concatenated with instance ID 
*                                                                                                           
* OUTPUTS:                                                                                                  
*                                                                                                           
* RETURNS:                                                                                                  
* On success, the function returns TPMAPROC_OK. On error, TPMAPROC_GENERROR is returned.                    
*                                                                                                           
* COMMENTS:                                                                                                 
*******************************************************************************/                            

tpma_proc_status_t remove_by_key   (uint32_t bridge_id ,uint32_t tp_id, bool isBridgeOnly){

    tpma_l2_key_t        omci_key;
    tpma_l2_rule_t*      db;
    uint32_t             rule_num;
    tpma_proc_status_t   ret_val;
    bool                 found_group_L2DS=TRUE;
    bool                 found_group_L2US=TRUE;
    bool                 found_group_L3DS=TRUE;
    bool                 found_group_L3US=TRUE;

    

    /*construct search key*/
    omci_key.bridge_id = bridge_id;
    omci_key.source_tp = isBridgeOnly?0:tp_id;
    omci_key.destination_tp = 0;

    /*get downstream database pointer*/
    if (get_l2_db(TPMA_DIRECTION_DS,&db)== FALSE) {
        DBG_PRINT("FAIL to get_l2_db");
        return TPMAPROC_GENERROR;
    }

    /*Search L2 DS database for rules with the same bridge id */
    found_group_L2DS = find_first_l2_rule_by_key(&omci_key,TPMA_DIRECTION_DS, &rule_num);

    while (found_group_L2DS == TRUE) {
        /*start remove */
        ret_val = delete_l2_rule(TPMA_DIRECTION_DS, rule_num);
        if (ret_val != TPMAPROC_OK){
            DBG_PRINT("FAIL to delete_l2_rule %d ret_val %d", rule_num , ret_val);
            return (ret_val);
        }
        /*look 4 next rule*/
        found_group_L2DS = find_first_l2_rule_by_key(&omci_key,TPMA_DIRECTION_DS, &rule_num);
    }

    /*Search L2 US database for rules with the same bridge id */
    if (get_l2_db(TPMA_DIRECTION_US,&db)== FALSE) {
        DBG_PRINT("FAIL to get_l2_db");
        return TPMAPROC_GENERROR;
    }

    /*Search L2 US database for rules with the same bridge id */
    found_group_L2US = find_first_l2_rule_by_key(&omci_key,TPMA_DIRECTION_US, &rule_num);

    while (found_group_L2US == TRUE) {
        /*start remove */
        ret_val = delete_l2_rule(TPMA_DIRECTION_US, rule_num);
        if (ret_val != TPMAPROC_OK){
            DBG_PRINT("FAIL to delete_l2_rule %d ret_val %d", rule_num , ret_val);
            return (ret_val);
        }
        /*look 4 next rule*/
        found_group_L2US = find_first_l2_rule_by_key(&omci_key,TPMA_DIRECTION_US, &rule_num);
    }

    found_group_L3DS = remove_all_l3_rules_by_key (bridge_id,tp_id,isBridgeOnly,TPMA_DIRECTION_DS);
    found_group_L3US = remove_all_l3_rules_by_key (bridge_id,tp_id,isBridgeOnly,TPMA_DIRECTION_US);

    
    if ((!found_group_L2DS)&&
        (!found_group_L2US)&&
        (!found_group_L3DS)&&
        (!found_group_L3US)) {

        return TPMAPROC_NOT_FOUND;
    }


    return TPMAPROC_OK;

}


/*******************************************************************************
* set_1tag_any_rule_color()
*
* DESCRIPTION:      1 tag match rules have a spesific 'color' (dropped black vs. allowed white) 
*                   When set a 1 tag match rule, this determines the 'color'  of the  1 tag 'any' rule
*                   THAT MUST BE with the  opposite 'color'.
*                   UNLESS 'COLOR' OF THE 'ANY 1 TAG RULE AS SET EXPLICITLY BY TAGGED RULE
*                   each group (by source port) has 3 'any' rules: 2 tag 'any' rule, 1 tag 'any' rule and no tag 'any' rule. 
*                   'any' refers to no match values in the rule.  
*                   If the 'color' of the 'any' 1 tag rule should be switched
*                   delete current 1 tag 'any' rule and re-set with the correct 'color'
*
*
* INPUTS:
*  dir              : upstream vs. downstream of the new 1 tag rule to receive datbase pointer and to re-set new rule if needed           
* rule holding:
*  omci_key         : ONCI identifier (bridge_id, source_tp, destination_tp) of the new 1 tag rule to re-set new rule if needed           
*  src_port         : Source port by TPM enumeration of the new 1 tag rule , to re-set new rule if needed
*  dest             : frame target of the new 1 tag rule , to re-set new rule if needed
*
*  action           : action of the new 1 tag rule to determine whether the 'any' 1 tag rule should be re-set or not
* start_rule_num    : start rule number of current group

*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK.
* On error different types are returned according to the case.*
* COMMENTS:
*
*******************************************************************************/
tpma_proc_status_t set_1tag_any_rule_color(tpma_direction_t          dir, 
                                           tpma_l2_rule_t*           input_rule, 
                                           tpm_pkt_action_t          action,
                                           uint32_t                  start_rule_num){
     

      uint32_t             rule_num;
      tpm_rule_action_t    new_action;
      tpma_l2_rule_t       new_rule;
      tpma_length_t        rule_length;
      tpma_proc_status_t   tpma_ret_val;
      tpma_l2_rule_t*      db; 


      /*
        1 tag match rules have a spesific 'color' (dropped black vs. allowed white)        
        When set a 1 tag match rule, this determines the 'color'  of the  1 tag 'any' rule 
        THAT MUST BE with the  opposite 'color'.                                           
      */

      /*get l2 database pointer*/
      if (get_l2_db(dir,&db)== FALSE) {
         DBG_PRINT("Fail to get_l2_db");
         return TPMAPROC_GENERROR;
       }

       /*find 1 tag 'any' rule rule number */
       if (find_first_rule_by_length(dir, TPMA_LENGTH_ANY_ONE_TAG, input_rule->src_port,start_rule_num,&rule_num)== FALSE){
           return TPMAPROC_GENERROR;
       }
       /*if the 'color' of the 'any' 1 tag rule as set explicitly by tagged rule DON'T CHANGE IT*/
       if (db[rule_num].parse_rule_bm & PARSECOMMAND_L2_TAGGED ) {
           return TPMAPROC_OK;
       }
       /* the color of the any 1 tag VLAN rule should be opposite the newly given specific 1 tag VLAN rule*/
       if (((action & TPM_ACTION_DROP_PK) && 
            (db[rule_num].rule_action == TPMA_PCKT_ACTION_ALLOW))||
           (!(action & TPM_ACTION_DROP_PK) && 
            (db[rule_num].rule_action == TPMA_PCKT_ACTION_DROP))){
           /*1 tag 'any' vlan rule is in right color*/
           return TPMAPROC_OK;
       }

      /*re-add it in the SAME position with the right color*/
         /*prepare to re-set the 1 tag rule*/
       memcpy(&new_rule, &(db[rule_num]), sizeof(tpma_l2_rule_t));
       memset(&(new_rule.l2_key), 0, sizeof(tpm_l2_acl_key_t));
      
      
       /*remove  */
       tpma_ret_val = delete_l2_rule(dir,rule_num);
       
       if (tpma_ret_val != TPMAPROC_OK){
           DBG_PRINT("Fail to delete_l2_rule ret_val = %d, rule No = %d", tpma_ret_val, rule_num);
           return tpma_ret_val;
       }
                     
       
      
       /*set color*/
       if (action & TPM_ACTION_DROP_PK){
           new_action.pkt_act     = (TPM_ACTION_SET_TARGET_PORT | TPM_ACTION_SET_TARGET_QUEUE |TPM_ACTION_TO_CPU);
           #ifndef first_phase
               new_action.next_phase  = STAGE_L3_TYPE;
           #else
               new_action.next_phase  = STAGE_DONE;
           #endif
           new_rule.rule_action   = TPMA_PCKT_ACTION_ALLOW;
       }
       else{
           new_action.pkt_act     = TPM_ACTION_DROP_PK;
           new_action.next_phase  = STAGE_DONE;
           new_rule.rule_action   = TPMA_PCKT_ACTION_DROP;
       }
    
       /*add in the SAME position*/
        /*set new rule*/
       new_rule.parse_rule_bm  = (tpm_parse_fields_t)PARSECOMMAND_L2_1VLAN_TAG;
       new_rule.rule_length    = TPMA_LENGTH_ANY_ONE_TAG;
       /*insert record to database*/
    
       tpma_ret_val = add_new_l2_rule(dir,rule_num,&new_rule,&new_action, NULL);

      

       return tpma_ret_val;
}
/*******************************************************************************
* add_2tag_rule_only_outer_tag_set()
*
* DESCRIPTION:      1 tag match rules WITHOUT modification cause TPMA to add 
*                   a 2 tag rule with the same values as in the 1 tag in the OUTER vlan as the received values
*                   When set a 1 tag match rule, this determines the 'color'  of the  corresponding 2 tag rule
*                   set the new 2 tag rule length 
*                   determine the new 2 tag rule's position 
*                   and add it 
*
*
* INPUTS:
*  dir                      : upstream vs. downstream of the received 1 tag rule to receive datbase pointer and to add a new 2 ta rule           
* tpma_l2_rule_t* input_rule containing:
*          omci_key                 : ONCI identifier (bridge_id, source_tp, destination_tp) of the received 1 tag rule to add a new 2 tag rule
*          input_parse_commands_bm  : parse fields bitmap of the received 1 tag rule , to add a new 2 tag rule
*          input_parse_values       : parse fields values of the received 1 tag rule , to add a new 2 tag rule
*          src_port                 : Source port by TPM enumeration of the received 1 tag rule  , to add a new 2 tag rule
*          dest                     : frame target of the received 1 tag rule , to add a new 2 tag rule
*  action                   : action of the received 1 tag rule to add a new 2 tag rule
* start_rule_num            : start rule number of current group

*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK.
* On error different types are returned according to the case.*
* COMMENTS:
*
*******************************************************************************/
 tpma_proc_status_t add_2tag_rule_only_outer_tag_set(tpma_direction_t          dir, 
                                                     tpma_l2_rule_t*           input_rule,
                                                     tpm_pkt_mod_t*            mod,
                                                     tpm_pkt_action_t          action,
                                                     uint32_t                  start_rule_num){
 
      uint32_t             rule_num;
      tpm_rule_action_t    new_action;
      tpma_l2_rule_t       new_rule;
      tpm_l2_acl_key_t     match_key;
      tpma_parse_command_t new_rule_bm;
      tpma_length_t        rule_length;
      tpma_proc_status_t   tpma_ret_val;


      /*prepare to add the 2 tag rule*/
      memcpy (&new_rule, input_rule, sizeof(tpma_l2_rule_t));


      /*When set a 1 tag match rule, this determines the 'color'  of the  corresponding 2 tag rule*/
      new_action.pkt_act     = action;
      if (action & TPM_ACTION_DROP_PK){
          new_action.next_phase  = STAGE_DONE;
          new_rule.rule_action   = TPMA_PCKT_ACTION_DROP;
      }
      else{
          #ifndef first_phase
              new_action.next_phase  = STAGE_L3_TYPE;
          #else
              new_action.next_phase  = STAGE_DONE;
          #endif
          new_rule.rule_action   = TPMA_PCKT_ACTION_ALLOW;
      }


      /*copy the outer tag restriction
      with the same values as in the 1 tag in the OUTER vlan as the received values*/
      memcpy (&match_key, &(input_rule->l2_key), sizeof(tpm_l2_acl_key_t));
      /*zero the inner tag */
      match_key.vlan2.tpid      = input_rule->l2_key.vlan1.tpid;  
      match_key.vlan2.vid_mask  = 0;   
      match_key.vlan2.cfi_mask  = 0;   
      match_key.vlan2.pbit_mask = 0;  
    
      new_rule_bm = input_rule->parse_rule_bm;
      new_rule_bm &= ~(PARSECOMMAND_L2_1VLAN_TAG);
      new_rule_bm |= PARSECOMMAND_L2_2VLAN_TAG;   

      /*set the new 2 tag rule length */
      if (determine_rule_length(new_rule_bm, &match_key,&rule_length)== FALSE){
          DBG_PRINT("Fail to determine_rule_length");
          return TPMAPROC_GENERROR;
      }
      /*determine the new 2 tag rule's position */
      if (find_first_rule_by_length(dir, rule_length, input_rule->src_port,start_rule_num,&rule_num)== FALSE){
          DBG_PRINT("Fail to find_first_rule_by_length");
          return TPMAPROC_GENERROR;
      }

        /*set new rule*/
      new_rule.parse_rule_bm  = (tpm_parse_fields_t)new_rule_bm;
      new_rule.rule_length    =  rule_length;
      memcpy(&new_rule.l2_key,&match_key,sizeof(tpm_l2_acl_key_t));
      /*insert record to database*/
      tpma_ret_val = add_new_l2_rule(dir,rule_num,&new_rule,&new_action,NULL);


            

      return tpma_ret_val;
}

 /*******************************************************************************
* add_2tag_rule_only_outer_tag_set()
*
* DESCRIPTION:      1 tag match rules WITHOUT modification cause TPMA to add 
*                   a 2 tag rule with the same values as in the 1 tag in the OUTER vlan as the received values
*                   When set a 1 tag match rule, this determines the 'color'  of the  corresponding 2 tag rule
*                   set the new 2 tag rule length 
*                   determine the new 2 tag rule's position 
*                   and add it 
*
*
* INPUTS:
*  dir                      : upstream vs. downstream of the received 1 tag rule to receive datbase pointer and to add a new 2 ta rule           
* tpma_l2_rule_t* input_rule containing:
*          l2_key                   : match values holding the relevant outer tag
*          src_port                 : Source port by TPM enumeration of the received 1 tag rule  , to remove a new 2 tag rule
*  action                   : action of the received 1 tag rule to add a new 2 tag rule
* start_rule_num            : start rule number of current group

*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK.
* On error different types are returned according to the case.*
* COMMENTS:
*
*******************************************************************************/
 tpma_proc_status_t remove_2tag_rule_only_outer_tag_set(tpma_direction_t          dir, 
                                                        tpma_l2_rule_t*           input_rule,
                                                        uint32_t                  start_rule_num){
 
      uint32_t             rule_length;
      uint32_t             current_start;
      uint32_t             rule_num;
      uint8_t              found_match; 
      uint8_t              ret_val;
      tpma_l2_rule_t*      l2_db;
      tpm_vlan_key_t       tmp_inner_vlan;
      tpma_proc_status_t   tpma_ret_val;

      /*I get a dattabase pointer*/
        if (get_l2_db(dir,&l2_db) == FALSE) {
            DBG_PRINT("Fail to get_l2_db");
            return TPMAPROC_GENERROR;
        }


      memset(&tmp_inner_vlan, 0, sizeof(tpm_vlan_key_t));

     /*match the rule's length (2 tag) matching to the requsted (1 tag) rule to be removed */
        switch (input_rule->rule_length) {
        case TPMA_LENGTH_ONE_TAG_ANY_VID:
            /*(*,pb;*,*)*/
            rule_length = TPMA_LENGTH_TWO_TAG_SINGLE_PBIT_SET;
            break;
        case TPMA_LENGTH_ONE_TAG_ANY_PBIT:
            /*(vid,*;*,*)*/
            rule_length = TPMA_LENGTH_TWO_TAG_SINGLE_VID_SET;
            break;
        case TPMA_LENGTH_ONE_TAG:
            /*(vid,pb;*,*)*/
            rule_length = TPMA_LENGTH_TWO_TAG_SINGLE_VID_SINGLE_PBIT_SET;
            break;
        default:
            DBG_PRINT("Fail to match length 4 2 tag rules");
            rule_length = 0;
            break;

        }
        
        if (rule_length == 0) {
            return TPMAPROC_GENERROR;
        }
       
        /*set inner vlan corresponding to the 2 tag rule I'm looking 4*/
        tmp_inner_vlan.tpid = input_rule->l2_key.vlan1.tpid;
        found_match         = FALSE;
        ret_val             = FALSE;
        current_start       = start_rule_num;
        rule_num            = 0;


        ret_val = find_first_rule_by_length(dir,rule_length,input_rule->src_port,current_start,&rule_num);
        
        while((ret_val == TRUE) && (found_match == FALSE)){
            /*ONLY after inner VLAN match was verified start comparing the outer vlan*/
            if (memcmp(&(l2_db[rule_num].l2_key.vlan2), &tmp_inner_vlan, sizeof(tpm_vlan_key_t)) == 0) {

                /*compare outer vlan by length value*/
                switch (rule_length) {
                case TPMA_LENGTH_TWO_TAG_SINGLE_PBIT_SET:
                    if((l2_db[rule_num].l2_key.vlan1.pbit      == input_rule->l2_key.vlan1.pbit)&&
                       (l2_db[rule_num].l2_key.vlan1.pbit_mask == input_rule->l2_key.vlan1.pbit_mask)){
                        found_match = TRUE;
                    }
                        
                    break;
                case TPMA_LENGTH_TWO_TAG_SINGLE_VID_SET:
                    if((l2_db[rule_num].l2_key.vlan1.vid      == input_rule->l2_key.vlan1.vid)&&
                       (l2_db[rule_num].l2_key.vlan1.vid_mask == input_rule->l2_key.vlan1.vid_mask)){
                        found_match = TRUE;
                    }
                        
                    break;
                case TPMA_LENGTH_TWO_TAG_SINGLE_VID_SINGLE_PBIT_SET:
                    if((l2_db[rule_num].l2_key.vlan1.pbit      == input_rule->l2_key.vlan1.pbit)&&
                       (l2_db[rule_num].l2_key.vlan1.pbit_mask == input_rule->l2_key.vlan1.pbit_mask)&&
                       (l2_db[rule_num].l2_key.vlan1.vid       == input_rule->l2_key.vlan1.vid)&&
                       (l2_db[rule_num].l2_key.vlan1.vid_mask  == input_rule->l2_key.vlan1.vid_mask)){
                        found_match = TRUE;
                    }
                        
                    break;
                }
            }/*inner vlan match*/
            if (found_match == TRUE) {
                break;
            }
            /*look for next rule*/
            current_start = rule_num +1;
            ret_val = find_first_rule_by_length(dir,rule_length,input_rule->src_port,current_start,&rule_num);
        }
        
        if(found_match == TRUE){
            /*try to remove the 2 tag rule added due to the 1 tag rule*/
            tpma_ret_val = delete_l2_rule(dir, rule_num);
            if (tpma_ret_val != TPMAPROC_OK) {
                DBG_PRINT("fail to delete_l2_rule %d, ret_val = %d", rule_num, tpma_ret_val);
                return (tpma_ret_val);
            }
        }/*there might be a casse when a rule was added WITH a modification command and a 2 tag rule */
        

      return TPMAPROC_OK;
}
/*******************************************************************************
* set_1_2_tag_any_GEM_rules()
*
* DESCRIPTION:      Setting rule with ONLY GEM port match is actually means that 
*                   any traffic (2 tags, 1 tag or no tag) WITH this GEM port should be allowd (or dropped)
*                   In this function I add rules regarding 2 and 1 tag frames  WITH this GEM port.
*                   I create new rule with the SAME action as the received rule with ONLY GEM port match
*                   first I construct a 2 tag any rule WITH the given GEM port, 
*                   find its position and set it 
*                   Second I construct a 1 tag any rule WITH the given GEM port, 
*                   find its position and set it 
*                   The no tag WITH GEM port rule is added in the main function TPMA_add_l2_rule
*
*
* INPUTS:
*  dir                      : upstream vs. downstream of the received rule with ONLY GEM port match to add a new 1 & 2 tag PLUS GEM rules           
* tpma_l2_rule_t* input_rule containing:
*         omci_key                 : ONCI identifier (bridge_id, source_tp, destination_tp) the received rule with ONLY GEM port match to add a new 1 & 2 tag PLUS GEM rules
*         src_port                 : Source port by TPM enumeration of the received rule with ONLY GEM port match , to add a new 1 & 2 tag PLUS GEM rules
*         input_parse_values       : parse fields values of the received rule with ONLY GEM port match , to add a new 1 & 2 tag PLUS GEM rules
*         dest                     : frame target of the received rule with ONLY GEM port match , to add a new 1 & 2 tag PLUS GEM rules
*  mod                      : frame modification command of the received rule with ONLY GEM port match , to add a new 1 & 2 tag PLUS GEM rules
*  action                   : action of as the received rule with ONLY GEM port match to add a new 1 & 2 tag PLUS GEM rules
* start_rule_num            : start rule number of current group

*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK.
* On error different types are returned according to the case.*
* COMMENTS:
*   The no tag WITH GEM port rule is added in the main function TPMA_add_l2_rule
*
*******************************************************************************/
 
tpma_proc_status_t set_1_2_tag_any_GEM_rules(tpma_direction_t          dir, 
                                             tpma_l2_rule_t*           input_rule, 
                                             tpm_pkt_mod_t*            mod,
                                             tpm_pkt_action_t          action,
                                             uint32_t                  start_rule_num){
      uint32_t             rule_num;
      tpm_rule_action_t    new_action;
      tpma_l2_rule_t       new_rule;
      tpm_l2_acl_key_t     match_key;
      tpma_proc_status_t   tpma_ret_val;

    
     
         /*
            Setting rule with ONLY GEM port match is actually means that                            
            any traffic (2 tags, 1 tag or no tag) WITH this GEM port should be allowd (or dropped)  
            In this function I add rules regarding 2 and 1 tag frames  WITH this GEM port.          
         */

      memcpy (&new_rule, input_rule, sizeof(tpma_l2_rule_t));


      /*I create new rule with the SAME action as the received rule with ONLY GEM port match*/
      /*set color*/
      new_action.pkt_act  = action;
      if (action & TPM_ACTION_DROP_PK){
          new_action.next_phase  = STAGE_DONE;
          new_rule.rule_action   = TPMA_PCKT_ACTION_DROP;
      }
      else{
          #ifndef first_phase
              new_action.next_phase = STAGE_L3_TYPE;
          #else
              new_action.next_phase = STAGE_DONE;
          #endif
          new_rule.rule_action      = TPMA_PCKT_ACTION_ALLOW;
      }

      /*first I construct a 2 tag any rule WITH the given GEM port, */
      rule_num = 0; 

       /*set GEM port  &  any 2 tag restrictions */
      memset (&match_key, 0, sizeof(tpm_l2_acl_key_t));
      /*set 'any' 2 tag values */
      match_key.vlan1.tpid      = TPMA_VLAN_ETHER_TYPE_DEFAULT; 
      match_key.vlan2.tpid      = TPMA_VLAN_ETHER_TYPE_DEFAULT; 
      /*set GEM value*/
      match_key.gem_port = input_rule->l2_key.gem_port;


       /*find its position and set it */  
      if (find_first_rule_by_length(dir, TPMA_LENGTH_ANY_TWO_TAG_GEM, new_rule.src_port,start_rule_num,&rule_num)== FALSE){
          DBG_PRINT("fail find_first_rule_by_length TPMA_LENGTH_ANY_TWO_TAG_GEM");
          return TPMAPROC_GENERROR;
      }

      /*set new rule*/
      new_rule.parse_rule_bm  = (tpm_parse_fields_t)(PARSECOMMAND_L2_2VLAN_TAG | PARSECOMMAND_L2_GEM_PORT);
      new_rule.rule_length    = TPMA_LENGTH_ANY_TWO_TAG_GEM;
      memcpy(&new_rule.l2_key,&match_key,sizeof(tpm_l2_acl_key_t));
      /*insert record to database*/
      tpma_ret_val = add_new_l2_rule(dir,rule_num,&new_rule,&new_action,mod);
      if (tpma_ret_val != TPMAPROC_OK) {
          DBG_PRINT("fail add_new_l2_rule %d ",  tpma_ret_val);
          return  tpma_ret_val;
      }
      
     


      /*Second I construct a 1 tag any rule WITH the given GEM port, */
      /*set 1 tag vlan & GEM rule */
      match_key.vlan2.tpid  = 0;
      rule_num = 0; 

      /*find its position and set it */
      if (find_first_rule_by_length(dir, TPMA_LENGTH_ANY_ONE_TAG_GEM, input_rule->src_port,start_rule_num,&rule_num)== FALSE){
          DBG_PRINT("fail find_first_rule_by_length TPMA_LENGTH_ANY_ONE_TAG_GEM ");
          return TPMAPROC_GENERROR;
      }
      /*set new rule*/
      new_rule.parse_rule_bm  = (tpm_parse_fields_t)(PARSECOMMAND_L2_1VLAN_TAG | PARSECOMMAND_L2_GEM_PORT);
      new_rule.rule_length    = TPMA_LENGTH_ANY_ONE_TAG_GEM;
      memcpy(&new_rule.l2_key,&match_key,sizeof(tpm_l2_acl_key_t));
      /*insert record to database*/

      tpma_ret_val = add_new_l2_rule(dir,rule_num,&new_rule,&new_action,mod);
      
      return  tpma_ret_val;
        
         
     
}
/*******************************************************************************
* remove_1_2_tag_any_GEM_rules()
*
* DESCRIPTION:      When removing a rule with ONLY GEM port match it actually means that               
*                   both the any traffic (2 tags, 1 tag or no tag) WITH this GEM port should be removed
*                   In this function I remove rules regarding 2 and 1 tag frames  WITH this GEM port.  
*                   I get a dattabase pointer and look 4 'any' 1 tag rule with the same gem port
*                   When found, I try to remove it. 
*                   Then, I look 4 'any' 2 tag rule with the same gem port
*                   When found, I try to remove it too 
*
*
* INPUTS:
*  dir                      : upstream vs. downstream of the received rule with ONLY GEM port match to add a new 1 & 2 tag PLUS GEM rules           
* tpma_l2_rule_t* input_rule containing:
*         l2_key                   : match values holding the relevant GEM port
*         src_port                 : Source port by TPM enumeration of the received rule with ONLY GEM port match , to remove the 1 & 2 tag PLUS GEM rules
* start_rule_num            : start rule number of current group

*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK.
* On error different types are returned according to the case.*
* COMMENTS:
*   The no tag WITH GEM port rule is added in the main function TPMA_add_l2_rule
*
*******************************************************************************/
 
tpma_proc_status_t remove_1_2_tag_any_GEM_rules(tpma_direction_t          dir, 
                                                tpma_l2_rule_t*           input_rule, 
                                                uint32_t                  start_rule_num){
      uint32_t             gem_port;
      uint32_t             current_start;
      uint32_t             rule_num;
      uint8_t              found_match; 
      uint8_t              ret_val;
      tpma_l2_rule_t*      l2_db;
      tpma_proc_status_t   tpma_ret_val;

      /*I get a dattabase pointer*/
        if (get_l2_db(dir,&l2_db) == FALSE) {
            DBG_PRINT("Fail to get_l2_db");
            return TPMAPROC_GENERROR;
        }

    
     
         /*
            When removing a rule with ONLY GEM port match it actually means that                            
            both the any traffic (2 tags, 1 tag or no tag) WITH this GEM port should be removed
            In this function I remove rules regarding 2 and 1 tag frames  WITH this GEM port.          
         */

        gem_port        = input_rule->l2_key.gem_port;
        found_match     = FALSE;
        ret_val         = FALSE;
        current_start   = start_rule_num;
        rule_num        = 0;
        /*look 4 'any' 1 tag rule with the same gem port*/
        ret_val = find_first_rule_by_length(dir,TPMA_LENGTH_ANY_ONE_TAG_GEM,input_rule->src_port,current_start,&rule_num);

        while((ret_val == TRUE) && (found_match == FALSE)){
            if (gem_port == l2_db[rule_num].l2_key.gem_port) {
                found_match = TRUE;
                break;
            }
            current_start = rule_num +1;
            ret_val = find_first_rule_by_length(dir,TPMA_LENGTH_ANY_ONE_TAG_GEM,input_rule->src_port,current_start,&rule_num);
        }
        if(found_match == TRUE){
            /*found 1 tag rule with the same gem port, 
            now, try to remove it*/
            tpma_ret_val = delete_l2_rule(dir, rule_num);
            if (tpma_ret_val != TPMAPROC_OK) {
                DBG_PRINT("fail to delete_l2_rule %d, ret_val = %d", rule_num, tpma_ret_val);
                return (tpma_ret_val);
            }
        }

        found_match     = FALSE;
        ret_val         = FALSE;
        current_start   = start_rule_num;
        rule_num        = 0;

        /*look 4 'any' 2 tag rule with the same gem port*/
        ret_val = find_first_rule_by_length(dir,TPMA_LENGTH_ANY_TWO_TAG_GEM,input_rule->src_port,current_start,&rule_num);
        while((ret_val == TRUE) && (found_match == FALSE)){
            if (gem_port == l2_db[rule_num].l2_key.gem_port) {
                found_match = TRUE;
                break;
            }
            current_start = rule_num +1;
            ret_val = find_first_rule_by_length(dir,TPMA_LENGTH_ANY_TWO_TAG_GEM,input_rule->src_port,current_start,&rule_num);
        }
        if(found_match == TRUE){
            /*found 2 tag rule with the same gem port, 
            now, try to remove it*/
            tpma_ret_val = delete_l2_rule(dir, rule_num);
            if (tpma_ret_val != TPMAPROC_OK) {
                DBG_PRINT("fail to delete_l2_rule %d, ret_val = %d", rule_num, tpma_ret_val);
                return (tpma_ret_val);
            }
        }
      return  TPMAPROC_OK;
        
         
     
}
/*******************************************************************************
* update_tag_any_tagged_rules()
*
* DESCRIPTION:      Setting tagged rules actually refers to the 1 & 2 tag 'any' rules.
*                   In this function I update (remove and re-add) the (1 or 2) tag 'any rule. 
*                   In set_rule_position I remove the 1 tag 'any rule 
*                   and I add it in the main function TPMA_add_l2_rule
*
*
* INPUTS:
*  dir                      : upstream vs. downstream of the received rule with tagged match to get pointer to the database add to update 2 tag 'any' rule
* tpma_l2_rule_t* input_rule containing:
*         omci_key                 : ONCI identifier (bridge_id, source_tp, destination_tp) of the received rule with tagged match, to update 2 tag 'any' rule
*         src_port                 : Source port by TPM enumeration of the received rule with tagged match, to update 2 tag 'any' rule
*         dest                     : frame target of the received rule with tagged match, to update 2 tag 'any' rule
*  mod                      : frame modification command of the received rule with tagged match, to update 2 tag 'any' rule
*  action                   : action of the received rule with tagged match, to update 2 tag 'any' rule
* start_rule_num            : start rule number of current group

*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK.
* On error different types are returned according to the case.*
* COMMENTS:
* In set_rule_position I remove the 1 tag 'any rule    
* and I add it in the main function TPMA_add_l2_rule   
*
*******************************************************************************/
tpma_proc_status_t update_tag_any_tagged_rules  (tpma_direction_t        dir, 
                                                 tpma_l2_rule_t*         input_rule,
                                                 tpm_pkt_mod_t*          mod,
                                                 tpm_pkt_action_t        action,
                                                 uint32_t                start_rule_num){


    uint32_t             rule_num;
    tpm_rule_action_t    new_action;
    tpma_l2_rule_t       new_rule;
    tpm_l2_acl_key_t     match_key;
    tpma_proc_status_t   tpma_ret_val;
    tpma_l2_rule_t*      db;
    tpma_length_t        length;



    /*prepare to create new rule */
    memcpy (&new_rule, input_rule, sizeof(tpma_l2_rule_t));
    /*get database pointer in order to retrieve rules parameters that enables deletion*/
    if (get_l2_db(dir,&db)== FALSE) {
         DBG_PRINT("fail to set db pointer ");
         return TPMAPROC_GENERROR;
    }

    /* Setting tagged rules actually refers to the 1 & 2 tag 'any' rules.*/


    /*set updated rule's length */
    if (input_rule->parse_rule_bm & PARSECOMMAND_L2_2VLAN_TAG) {
          length = TPMA_LENGTH_ANY_TWO_TAG;
    }
    else {
        if (input_rule->parse_rule_bm & PARSECOMMAND_L2_1VLAN_TAG) {
            length = TPMA_LENGTH_ANY_ONE_TAG;
        }
        else{
            length = TPMA_LENGTH_NO_TAG;
        }
    }
    /*and find it*/
    if (find_first_rule_by_length(dir, length, input_rule->src_port,start_rule_num,&rule_num)== FALSE){
          DBG_PRINT("fail to set position on length %d", length);
          return TPMAPROC_GENERROR;
    }
    
    /*remove it*/
    tpma_ret_val = delete_l2_rule(dir,rule_num);
    if (tpma_ret_val != TPMAPROC_OK) {
        DBG_PRINT("fail to delete_l2_rule %d", tpma_ret_val);
        return (tpma_ret_val);
    }

    /*and reset it*/
    /*set color*/
    if (!(action & TPM_ACTION_DROP_PK)){
        new_action.pkt_act        = (TPM_ACTION_SET_TARGET_PORT | TPM_ACTION_SET_TARGET_QUEUE |TPM_ACTION_TO_CPU);
        #ifndef first_phase
            new_action.next_phase = STAGE_L3_TYPE;
        #else
            new_action.next_phase = STAGE_DONE;
        #endif
        new_rule.rule_action      = TPMA_PCKT_ACTION_ALLOW;
    }
    else{
        new_action.pkt_act     = TPM_ACTION_DROP_PK;
        new_action.next_phase  = STAGE_DONE;
        new_rule.rule_action   = TPMA_PCKT_ACTION_DROP;
    }

     /* set tagged bit up and 1 tag VLAN bit up with any vlan*/
    memset (&match_key, 0, sizeof(tpm_l2_acl_key_t));
   
    if (input_rule->parse_rule_bm & PARSECOMMAND_L2_2VLAN_TAG) {
        match_key.vlan2.tpid = TPMA_VLAN_ETHER_TYPE_DEFAULT;
    }
    else{
        if (input_rule->parse_rule_bm & PARSECOMMAND_L2_1VLAN_TAG) {
            match_key.vlan1.tpid = TPMA_VLAN_ETHER_TYPE_DEFAULT;
        }
    }
   
     /*set new rule*/
    new_rule.rule_length     = length;
    memcpy(&new_rule.l2_key,&match_key,sizeof(tpm_l2_acl_key_t));
    tpma_ret_val = add_new_l2_rule(dir,rule_num,&new_rule,&new_action,mod);

    if (tpma_ret_val != TPMAPROC_OK) {
        DBG_PRINT("fail to add_new_l2_rule %d", tpma_ret_val);
        return  tpma_ret_val;
    }


    return TPMAPROC_OK;

    


}
/******************************************************************************/
/************************ main stream add rule operations   *******************/
/******************************************************************************/
/******************************************************************************/

/*******************************************************************************
* set_rule_position()
*
* DESCRIPTION:      
*                    
*                   Tagged rules refers to 1 & 2 tag 'any' rules. set_rule_position returns the position of the 1 tag 'any' rule 
*                   first I call update_2_tag_any_tagged_rules to update 2 tag 'any' rule 
*                   then I remove the 1 tag 'any rule and returns its position 
*                   1 tag 'any' rule is added in tpma_add_l2_rule
*                   In any other case, first set rule's length and get a position for it.
*                   There are 3 special cases:
*                       1) 'any' tag rules (1 or 2 or no tag) 
*                               these rules ALREADY exsits (must be created in open_connection function)
*                               Therefore, when I get the 'any' rule's position I remove it and return the removed rule position
*                               The NEW 'any' rule is addded in tpma_add_l2_rule
*                       2)spesific 1 tag rules
*                               If no modification action is requierd on this frame a 2 tag rule with the SAME outer tag should be added 
*                               After the new 2 tag rule is added need to recalculate the 1 tag rule's position
*                               At any rate, the 'color' of the 'any' 1 tag rule should be opposite to the color of the spesific 1 tag rule
*                       3)rules with ONLY GEM port match
*                               These rules refer to ANY traffic WITH this GEM port, in set_1_2_tag_any_GEM_rules I add 1 & 2 tag 'any' rules WITH this GEM port
*                               After the new 2 rules are added need to recalculate the NO tag PLUS GEM rule's position
*           
*
* INPUTS:
*  dir                     - upstream vs. downstream                      
* tpma_l2_rule_t* input_rule containing:
*     *omci_key                - bridge_id
*                              - source tp
*                              - dest tp   
*      src_port                - tpm enum; 
*      input_parse_commands_bm - to determine rule's length which sets the rule's position 
*      input_parse_values      - to determine rule's length which sets the rule's position and enable update if needed
*      dest                    - enable update if needed
*  mod                     - enable update if needed
*  action                  - enable update if needed                
*  start_rule_num - US section is devided to groups per service_key => 
*                              start_rule_num will be the first in this group
*                                          - DS section is not => start_rule_num will be 0
* OUTPUTS:
uint32_t* rule_num_pt - return's rule's position in the PnC
uint32_t* rule_length_pt - return's rule's length
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t set_rule_position (tpma_direction_t          dir, 
                                      tpma_l2_rule_t*           input_rule, 
                                      tpm_pkt_mod_t*            mod,
                                      tpm_pkt_action_t          action,
                                      uint32_t                  start_rule_num,
                                      uint32_t*                 rule_num_pt,
                                      tpma_length_t*            rule_length_pt){

    
                         
    
   
    uint32_t             rule_num;
    tpma_proc_status_t   tpma_ret_val = TPMAPROC_OK; 
    tpma_length_t        rule_length;
    tpma_l2_rule_t       new_rule;


    memcpy(&new_rule, input_rule , sizeof(tpma_l2_rule_t));
  

    /*Tagged rules refers to 1 & 2 tag 'any' rules. set_rule_position returns the position of the 1 tag 'any' rule         */
    /*
    (*,*) - any 2 tag rules
    (*)   - any 1 tag rule  
    Here, I update the 2 tag rule: delete and re-set the (*,*) rule
    AND delete the (*) rule 
    IN tpma_add_l2_rule I add the (*) rule in the SAME position from which it was removed
    */
    if (input_rule->parse_rule_bm &PARSECOMMAND_L2_TAGGED) {
        /*first I call update_2_tag_any_tagged_rules to update 2 tag 'any' rule*/
        new_rule.parse_rule_bm = (PARSECOMMAND_L2_2VLAN_TAG | PARSECOMMAND_L2_TAGGED);
        tpma_ret_val = update_tag_any_tagged_rules(dir,&new_rule,mod,action,start_rule_num);
        if (tpma_ret_val != TPMAPROC_OK) {
            DBG_PRINT("fail to update_tag_any_tagged_rules %d", tpma_ret_val);
            return (tpma_ret_val);
        }
        /*then I remove the 1 tag 'any rule and returns its position*/
        if (find_first_rule_by_length(dir, TPMA_LENGTH_ANY_ONE_TAG, input_rule->src_port,start_rule_num,&rule_num)== FALSE){
            DBG_PRINT("fail to set position on length %d", rule_length);
          return TPMAPROC_GENERROR;
        }
        tpma_ret_val = delete_l2_rule(dir,rule_num);
        
        if (tpma_ret_val != TPMAPROC_OK) {
            DBG_PRINT("fail to delete_l2_rule %d", tpma_ret_val);
            return (tpma_ret_val);
        }
        /*1 tag 'any' rule is added in tpma_add_l2_rule*/
        *rule_num_pt = rule_num;
        *rule_length_pt = TPMA_LENGTH_ANY_ONE_TAG;
        return TPMAPROC_OK;

    }

    /* In any other case, first set rule's length and get a position for it. */

    if (determine_rule_length(input_rule->parse_rule_bm, &(input_rule->l2_key),&rule_length)== FALSE){
        DBG_PRINT("fail to set length on BM %x", input_rule->parse_rule_bm);
        return TPMAPROC_GENERROR;
    }
    
    if (find_first_rule_by_length(dir, rule_length, input_rule->src_port,start_rule_num,&rule_num)== FALSE){
        DBG_PRINT("fail to set position on length %d", rule_length);
        return TPMAPROC_GENERROR;
    }

     /*There are 3 special cases:*/
    switch(rule_length){
    case TPMA_LENGTH_NO_TAG:
    case TPMA_LENGTH_ANY_ONE_TAG:
    case TPMA_LENGTH_ANY_TWO_TAG:
        /*any' tag rules (1 or 2 or no tag) */
        /*  these rules ALREADY exsits (must be created in open_connection function)                         
            Therefore, when I get the 'any' rule's position I remove it and return the removed rule position 
            The NEW 'any' rule is addded in tpma_add_l2_rule                                                 
      
         (*,*) - any 2 tag rules
         (*)   - any 1 tag rule  
         ()    - any untagged (No tag) rule 
         Here, I delete the (*,*) OR the (*) OR the () rule (depending on the length)
         IN tpma_add_l2_rule I add the new (same type) rule at the SAME position from which it was removed
    */
        tpma_ret_val = delete_l2_rule(dir,rule_num);
        break;
    case TPMA_LENGTH_ONE_TAG:
    case TPMA_LENGTH_ONE_TAG_ANY_PBIT:
    case TPMA_LENGTH_ONE_TAG_ANY_VID:
        /*
        When 1 tag rule is added WITHOUT a modification command, 
        I add a 2 tag rule (vid, pb; *) with the same vlan tag 
        last, I recalculate the position to add the 1 tag rule (vid,pb) - after the addition
        */
        /*spesific 1 tag rules*/

        if ((mod->vlan_mod.vlan_op == VLANOP_NOOP)||
            (mod == NULL)) {
        /*iIf no modification action is requierd on this frame a 2 tag rule with the SAME outer tag should be added */    
    
            tpma_ret_val = add_2tag_rule_only_outer_tag_set(dir, 
                                                            &new_rule,
                                                            mod,
                                                            action,
                                                            start_rule_num);
            if (tpma_ret_val != TPMAPROC_OK) {
                DBG_PRINT("Fail to add_2tag_rule_only_outer_tag_set %d", tpma_ret_val);
                break;
            }
            /*After the new 2 tag rule is added need to recalculate the 1 tag rule's position*/
            if (find_first_rule_by_length(dir, rule_length, input_rule->src_port,start_rule_num,&rule_num)== FALSE){
                DBG_PRINT("Fail to find_first_rule_by_length");
                tpma_ret_val =   TPMAPROC_GENERROR;
                break;
            }

        }
        /*At any rate, the 'color' of the 'any' 1 tag rule should be opposite to the color of the spesific 1 tag rule*/
        /*Since filter command is related to the vlan TaggingFilterData ME
        it is derived from the ME structure that all vlan tags in the vlan list are either allowed OR dropped
        Therefore, when set, the color of the 1 tag 'any rule (*) should be the OPPOSITE color from the vlan list*/
        tpma_ret_val = set_1tag_any_rule_color(dir,&new_rule,action,start_rule_num);
        if (tpma_ret_val != TPMAPROC_OK) {
            DBG_PRINT("Fail to set_1tag_any_rule_color %d", tpma_ret_val);
            break;
        }
       
       
        break;
    case TPMA_LENGTH_NO_TAG_GEM:
        /*rules with ONLY GEM port match
         These rules refer to ANY traffic WITH this GEM port, in set_1_2_tag_any_GEM_rules*/
        /*I add 1 & 2 tag 'any' rules WITH this GEM port
    
         (*,*)+GEM - any 2 tag rule with this gem port
         (*)  +GEM - any 1 tag rule with this gem port 
         ()   +GEM - any untagged (No tag) rule with this gem port
         Here, I add the (*,*)+GEM AND the (*)+GEM 
         IN tpma_add_l2_rule I add the new rule ()+GEM  (after recalculatong the position AFTER the 2 rules addition
        */
        tpma_ret_val = set_1_2_tag_any_GEM_rules(dir,&new_rule,mod,action,0);
        if (tpma_ret_val != TPMAPROC_OK) {
            DBG_PRINT("Fail set_1_2_tag_any_GEM_rules %d", tpma_ret_val);
            break;
        }

        /*After the new 2 rules are added need to recalculate the NO tag PLUS GEM rule's position*/
        if (find_first_rule_by_length(dir, rule_length, input_rule->src_port,start_rule_num,&rule_num)== FALSE){
            DBG_PRINT("Fail find_first_rule_by_length %d", rule_length);
            tpma_ret_val =   TPMAPROC_GENERROR;
            break;
        }
        break;
    }



    *rule_num_pt = rule_num;
    *rule_length_pt = rule_length;
    return tpma_ret_val;

    

}
/*******************************************************************************
* open_connection()
*
* DESCRIPTION:      
*                    
*                   Each new group in the TPM MUST have at least 3 'any' rules: 
*                           'any' 2 tag rule
*                           'any' 1 tag rule
*                           'any' NO tag rule
*                   This is for future use when the allowed frames will be passed to l3 section 
*                   there, their ether type is investigated.
*                   In order to move the cursor correctly AFTER the layer 2 header, 'any' traffic is 
*                   actually constructed by the above 3 rules 
*                   When no match restrictions (o or JUST mac address - which are orthogonal to other rules) 
*                   are added, the connection, 'any' traffic is set as allowed
*                   Otherwise,  the connection, 'any' traffic is set as dropped
*
* INPUTS:
*  dir            - upstream vs. downstream                      
* *omci_key       - bridge_id
*                 - source tp
*                 - dest tp   
*  src_port       - tpm enum; 
*  dest           - enable update if needed
*  start_rule_num - US section is devided to groups per service_key => 
*                       start_rule_num will be the first in this group
*                 - DS section is not => start_rule_num will be 0
*  open           - enumeration whether the connection should be set as open (allowed) or closed (dropped)   
*   
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t open_connection (tpma_direction_t          dir, 
                                    tpma_l2_rule_t*           input_rule, 
                                    uint32_t                  start_rule_num,  
                                    tpma_connection_type_t    open){

     tpma_l2_rule_t       new_l2_rule; 
     tpm_rule_action_t    any_rule_action;
     tpma_proc_status_t   tpma_ret_val; 
     /*
        Each new group in the TPM MUST have at least 3 'any' rules:               
            'any' 2 tag rule                                                  
            'any' 1 tag rule                                                  
            'any' NO tag rule                                                 
     */

     /*prepare new l2 rule*/
     memcpy(&new_l2_rule, input_rule, sizeof(tpma_l2_rule_t));
     memset(&(new_l2_rule.l2_key), 0, sizeof(tpm_l2_acl_key_t));

     /*set actoion (allow vs. drop) with respect to the connection type*/
     if (open == TPMA_COONECTION_OPEN) {
         new_l2_rule.rule_action = TPMA_PCKT_ACTION_ALLOW;
         any_rule_action.pkt_act = (TPM_ACTION_SET_TARGET_PORT | TPM_ACTION_SET_TARGET_QUEUE |TPM_ACTION_TO_CPU);
         #ifndef first_phase
            any_rule_action.next_phase = STAGE_L3_TYPE;
        #else 
            any_rule_action.next_phase = STAGE_DONE;
        #endif
     }
     else{
         new_l2_rule.rule_action = TPMA_PCKT_ACTION_DROP;
         any_rule_action.pkt_act = TPM_ACTION_DROP_PK;
         any_rule_action.next_phase = STAGE_DONE;
     }
     

     /*********************************************************/
     /********      second, set the 2 tag rule      ***********/
     /*********************************************************/
     new_l2_rule.l2_key.vlan2.tpid  = TPMA_VLAN_ETHER_TYPE_DEFAULT; 
     new_l2_rule.l2_key.vlan1.tpid  = TPMA_VLAN_ETHER_TYPE_DEFAULT;
     new_l2_rule.parse_rule_bm      = PARSECOMMAND_L2_2VLAN_TAG;
     new_l2_rule.rule_length        = TPMA_LENGTH_ANY_TWO_TAG;
     
     tpma_ret_val = add_new_l2_rule(dir,start_rule_num,&new_l2_rule,&any_rule_action,NULL);
     if (tpma_ret_val != TPMAPROC_OK) {
         DBG_PRINT("Fail add_new_l2_rule %d", tpma_ret_val);
          return (tpma_ret_val);
      }

     

     /*********************************************************/
     /********      second, set the 1 tag rule      ***********/
     /*********************************************************/
     start_rule_num ++; /*increment rule*/
     new_l2_rule.parse_rule_bm      = PARSECOMMAND_L2_1VLAN_TAG;
     new_l2_rule.rule_length        = TPMA_LENGTH_ANY_ONE_TAG;
     new_l2_rule.l2_key.vlan2.tpid  = 0; 

     tpma_ret_val = add_new_l2_rule(dir,start_rule_num,&new_l2_rule,&any_rule_action,NULL);
     if (tpma_ret_val != TPMAPROC_OK) {
         DBG_PRINT("Fail add_new_l2_rule %d", tpma_ret_val);
          return (tpma_ret_val);
      }
 

     /*********************************************************/
    /********   first set the NO tag allow rule     **********/
    /*********************************************************/
     start_rule_num ++; /*increment rule*/
     new_l2_rule.parse_rule_bm       = PARSECOMMAND_L2_UNTAGGED;
     new_l2_rule.rule_length         = TPMA_LENGTH_NO_TAG; 
     new_l2_rule.l2_key.vlan1.tpid   = 0; 

     tpma_ret_val = add_new_l2_rule(dir,start_rule_num,&new_l2_rule,&any_rule_action,NULL);
     if (tpma_ret_val != TPMAPROC_OK) {
         DBG_PRINT("Fail add_new_l2_rule %d", tpma_ret_val);
          return (tpma_ret_val);
      }


     
     
      return TPMAPROC_OK;
     

}
/*******************************************************************************
* validate_input()
*
* DESCRIPTION:      
*                    
*                   Validate input parameters:
*                   1) GEM port match can ONLY be set on downstream direction
*                   2) MAC address match can ONLY be set as dropped
*                   **** FIRST PHASE ****
*                   3) Ether type match (dependent on l3 implementation) can ONLY be set as dropped
*                   4) Ether type match (dependent on l3 implementation) can not be set 
*
* INPUTS:
*  dir            - upstream vs. downstream     
*  parse_cmd_bm   - fields to match         
*  action         - rule's relaed action
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t  validate_input (tpma_direction_t dir, tpma_parse_command_t parse_cmd_bm, tpma_pckt_action_t action){

    /* GEM port match can ONLY be set on downstream direction*/
    if ((dir == TPMA_DIRECTION_US) && 
        (parse_cmd_bm & PARSECOMMAND_L2_GEM_PORT)){
         /*gem match on upstream is meaningless*/
         DBG_PRINT("FAIL validate_input set GEM port in upstream");
         return TPMAPROC_MATCH_NOT_SUPPORTED;
    }

    /* MAC address match can ONLY be set as dropped*/
    if(((parse_cmd_bm & PARSECOMMAND_L2_MAC_DEST)|| 
       (parse_cmd_bm & PARSECOMMAND_L2_MAC_SRC))&&
       (action != TPMA_PCKT_ACTION_DROP)){
        /*mac white list is not supported */
        DBG_PRINT("FAIL validate_input set mac address not drop");
           return TPMAPROC_MATCH_NOT_SUPPORTED;
    }
    
    #ifdef first_phase
    /*Ether type match can ONLY be set as dropped*/
    if ((parse_cmd_bm & PARSECOMMAND_L2_ETH_TYPE)&&
        (action != TPMA_PCKT_ACTION_DROP)) {
         DBG_PRINT("FAIL validate_input set ether type not drop");
         return TPMAPROC_MATCH_NOT_SUPPORTED;
    }
    /*Ether type match (dependent on l3 implementation) can not be set */
    if (parse_cmd_bm & PARSECOMMAND_L2_ETH_TYPE) {
        DBG_PRINT("FAIL validate_input ether type ");
        return TPMAPROC_MATCH_NOT_SUPPORTED;
    }
    #endif

    return TPMAPROC_OK;
}
/*******************************************************************************
* tpma_add_l2_rule()
*
* DESCRIPTION:      
*                    
*                   Called from tpma_add_l2_US & from tpma_add_l2_DS
*                   first print input
*                   set tpma action
*                   find first rule in the group, upstream rules are sorted by their source port (4 uni ports) 
*                   while downstream ports are all one group (WAN)
*                   If could not find the first rule in the group, tuen on a flag  and call open_connection
*                   to set the 3 'any' rules: 
*                                   'any' 2 tag rule 
*                                   'any' 1 tag rule 
*                                   'any' NO tag rule
*
*                   If the match field is zero or contains ONLY MAC match then the connection is set to open (all 3 rules are allowed)
*                   Otherwise, the connection is set to closed (all 3 rules are dropped)
*                   When parse_cmd_bm contains ethertype match it means that l3 section should be configured
*                   Otherwise, the rule should be set in the l2 section.
*                   In l2 section configuration, there might be a case where a given rule was used JUST to open a connection
*                   THEN all the requierd rules were added in open_connection function
*                   At any other case, call set_rule_position which configures other related rules (like sets 2 tag rule on a 1 tag rule without a modification)
*                   determines the rule's length and retuns (via pointer) the rule's position and length 
*                   Populate rule structure, and add it into the dataase
*                   *********** NEXT PHASE ***********************
*                   When an ethertype command is added, set rule in the l3 section.
*                   populate rule's structure and since ether type rules can ONLY be set as dropped, 
*                   I automatically sets the rule first (position 0)
*                    and add it into the dataase
*                   When first rule is entered in a group (open connecion) I also add 3 distributing rules in the l3 section which matched the ether type 
*                   to IPv4 & IPv6 and other. 
*                       The IPv4 rule sets the next phase to l4 section. 
*                       The IPv6 rule sets the next phase to l6 section. 
*                       The other rule sets the next phase to Done. 
*           
*
* INPUTS:
*  dir           - upstream vs. downstream                      
* *full_bridge_key     - bridge_id
*                      - source tp
*                      - dest tp   
*  src_port      - tpm enum; 
*  parse_cmd_bm  - bitmap to indicate which values to match in the *parse_values 
*  *parse_values - structure holding values to match on an incoming frame
*  dest          - On match, sets frame's destination
*  action        - On match, sets frame's action (drop, set to target, set to cpu...)              
*  *tpma_handle  - US section is devided to groups per service_key => 
*                              start_rule_num will be the first in this group
*                                          - DS section is not => start_rule_num will be 0
* OUTPUTS:
uint32_t*                 tpma_handle - return value, last rule's index
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_add_l2_rule (tpma_direction_t     dir, 
                                     tpma_l2_key_t       *full_bridge_key,  
                                     tpm_src_port_type_t  src_port,      
                                     tpma_parse_command_t parse_cmd_bm,  
                                     tpm_l2_acl_key_t    *parse_values, 
                                     tpm_pkt_frwd_t      *dest,         
                                     tpm_pkt_mod_t       *mod,          
                                     tpm_pkt_action_t     action,        
                                     uint32_t            *tpma_handle){
    bool                    is_first;
    tpma_connection_type_t  is_open;

    uint32_t                rule_num;
    uint32_t                rule_length;
    uint32_t                start_rule_num;
                            
    tpma_l2_rule_t          new_l2_rule; 
    tpma_l3_rule_t          new_l3_rule; 
                            
    tpm_parse_flags_t       parse_flags_bm;
    tpm_l3_type_key_t       l3_key;
    tpm_rule_action_t       rule_action;
    tpma_proc_status_t      tpma_ret_val;


    /*prepare rules*/
    memset (&new_l2_rule            , 0 ,               sizeof(tpma_l2_rule_t));
    memcpy (&(new_l2_rule.key)      , full_bridge_key,  sizeof(tpma_l2_key_t));
    memcpy (&(new_l2_rule.l2_key)   , parse_values,     sizeof(tpm_l2_acl_key_t));
    memcpy (&(new_l2_rule.pkt_frwd) , dest,             sizeof(tpm_pkt_frwd_t));
    new_l2_rule.parse_rule_bm = parse_cmd_bm;
    new_l2_rule.src_port      = src_port;

    memset (&new_l3_rule, 0 , sizeof(tpma_l3_rule_t));

    /* first print input*/
    print_input_params(&new_l2_rule,mod,action, *tpma_handle);

   
    /*set packet action */
    rule_action.pkt_act = action; 

    if (action & TPM_ACTION_DROP_PK) {
        rule_action.next_phase = STAGE_DONE;
        new_l2_rule.rule_action = TPMA_PCKT_ACTION_DROP;
    }else{
        #ifndef first_phase
        rule_action.next_phase = STAGE_L3_TYPE;
        #else
        rule_action.next_phase = STAGE_DONE;
        #endif
        new_l2_rule.rule_action = TPMA_PCKT_ACTION_ALLOW;
    }


    tpma_ret_val = validate_input(dir, parse_cmd_bm,new_l2_rule.rule_action);
    if (tpma_ret_val != TPMAPROC_OK) {
        return tpma_ret_val;
    }

    /*find first rule in the group, upstream rules are sorted by their source port (4 uni ports) 
        while downstream ports are all one group (WAN)                   */

    is_first = FALSE;
    if (dir == TPMA_DIRECTION_US) {
        /*upstream traffic is arranged by the source port*/
         if (find_first_l2_rule_by_src_upstream(src_port, &start_rule_num)== FALSE){
             is_first = TRUE;
             /*bridge id was NOT found insert as last rule */
             if(get_current_l2_rule_count(dir,&start_rule_num)==FALSE){
                 return TPMAPROC_GENERROR;
             }
        }
    }
    else {/*all ds trafficis refered to as a single unit*/
        if(get_current_l2_rule_count(dir,&start_rule_num)==FALSE){
                    return TPMAPROC_GENERROR;
        }
        if (start_rule_num == 0) {
            is_first = TRUE;
        }
        start_rule_num = 0;
    }
    /*
    If could not find the first rule in the group, then rurn on a flag  and call open_connection     
    to set the 3 'any' rules:                                                                   
                    'any' 2 tag rule                                                            
                    'any' 1 tag rule                                                            
                    'any' NO tag rule                                                           
    */



    if (is_first == TRUE) {
        /*when no match is set connection (or just MAC match) is open on 2 tag, 1 tag and no tag frames
         Otherwise, ONLY explicitly allowed traffic can pass and all the 'any' rules  2 tag, 1 tag and no tag are set to drop*/
        is_open = ((parse_cmd_bm == 0)||
                   (parse_cmd_bm == PARSECOMMAND_L2_MAC_DEST)||
                   (parse_cmd_bm == PARSECOMMAND_L2_MAC_SRC)||
                   (parse_cmd_bm == (PARSECOMMAND_L2_MAC_DEST | PARSECOMMAND_L2_MAC_SRC))) ? TPMA_COONECTION_OPEN : TPMA_COONECTION_CLOSED;

        /*add 3 'any' (2 tag, 1 tag and no tag) default rules*/
        tpma_ret_val =  open_connection(dir,
                                        &new_l2_rule,
                                        start_rule_num,
                                        is_open);
        if (tpma_ret_val != TPMAPROC_OK) {
            return (tpma_ret_val);
        }
    }

     /*
        When parse_cmd_bm contains ethertype match it means that l3 section should be configured      
        Otherwise, the rule should be set in the l2 section.                                          
     */

    if (!(parse_cmd_bm & PARSECOMMAND_L2_ETH_TYPE)) {
            /*get rule number in L2 section */
        if ((is_open == TPMA_COONECTION_CLOSED)|| (is_first == FALSE))  {
        /*
        At any other case, call set_rule_position which configures other related rules 
        (like sets 2 tag rule on a 1 tag rule without a modification)  
        determines the rule's length and retuns (via pointer) the rule's position and length  */
            tpma_ret_val =  set_rule_position (dir, 
                                               &new_l2_rule,
                                               mod,
                                               action,
                                               start_rule_num,
                                               &rule_num,
                                               &rule_length);
            if (tpma_ret_val != TPMAPROC_OK) {
                DBG_PRINT("fail set_rule_position tpma_ret_val = %d", tpma_ret_val);
                return (tpma_ret_val);
            }
            
                      
            /*Populate rule structure, and add it into the dataase*/
            memcpy(&new_l2_rule.key, full_bridge_key, sizeof(tpma_l2_key_t));
            new_l2_rule.src_port       = src_port;
            new_l2_rule.parse_rule_bm  = parse_cmd_bm;
            new_l2_rule.rule_length    = rule_length; 
            memcpy(&new_l2_rule.l2_key,parse_values,sizeof(tpm_l2_acl_key_t));
            memcpy(&new_l2_rule.pkt_frwd,dest,sizeof(tpm_pkt_frwd_t));

            tpma_ret_val = add_new_l2_rule(dir,rule_num,&new_l2_rule,&rule_action,mod);
    
            if (tpma_ret_val != TPMAPROC_OK) {
                return (tpma_ret_val);
            }
            *tpma_handle = new_l2_rule.idx;

        }
        
    }
    else{
        /*When an ethertype command is added, set rule in the l3 section.*/
        /*set in L3 section */
        #ifndef first_phase
        is_first = FALSE;

        parse_flags_bm  = (TPM_PARSE_FLAG_TRG_PORT_DC | TPM_PARSE_FLAG_PKT_MOD_DC |TPM_PARSE_FLAG_TO_CPU_DC);

        /*populate rule's structure*/
        new_l3_rule.l3_key.ether_type_key = parse_values->ether_type;
        memcpy(&new_l3_rule.key, full_bridge_key, sizeof(tpma_l2_key_t));
        /*since ether type rules can ONLY be set as dropped, 
        I automatically sets the rule first (position 0)*/
        new_l3_rule.rule_number    = 0;
        new_l3_rule.src_port       = src_port;
        new_l3_rule.parse_rule_bm  = TPM_L2_PARSE_ETYPE;
        new_l3_rule.rule_type      = TPMA_L3_RULE_TYPE_DONE;


        tpma_ret_val =  add_new_l3_rule(dir,rule_num,parse_flags_bm,&new_l3_rule,(uint32_t)TRUE,STAGE_DONE);

        if (tpma_ret_val != TPMAPROC_OK) {
            return tpma_ret_val;
        }
                                   
        *tpma_handle = new_l3_rule.idx;
        
        #endif

    }
    


    if (is_first == FALSE)/*found rule group and rule was inserted first */
            return TPMAPROC_OK;



    /*************************************************/
    /*code section deals with rule first is it group*/
    /*************************************************/

    /*TPMA adds 3 rules to the L3 section parsing the Ether Type field on the same source port*/
    /*
        When first rule is entered in a group (open connecion) I also add 3 distributing rules in the l3 section which matched the ether type  
        to IPv4 & IPv6 and other.                                                                                                              
            The IPv4 rule sets the next phase to l4 section.                                                                                   
            The IPv6 rule sets the next phase to l6 section.                                                                                   
            The other rule sets the next phase to Done.                                                                                        
    */

    tpma_ret_val = add_l3_distributing_rules(dir,
                                             full_bridge_key,
                                             src_port); 
  
    return (tpma_ret_val);

}
/******************************************************************************/
/************************   TPMA API    ***************************************/
/******************************************************************************/
/******************************************************************************/

/******************************************************************************/
/************************   INSERT API  ***************************************/
/******************************************************************************/
/******************************************************************************/

/*******************************************************************************
* tpma_add_l2_US()
*
* DESCRIPTION:      Verify there is room for another rule in the L2US section
*                   When Ether type bit is set in the parse_cmd_bm, verify there is room for another rule in the L3US section
*                   Call tpma_add_l2_rule.
*                   On success, set *tpma_handle with rule's index.
*
* INPUTS:
* *service_key     - bridge_id
*                  - source tp
*                  - dest tp   
*  src_port      - tpm enum; 
*  parse_cmd_bm  - bitmap to indicate which values to match in the *parse_values 
*  *parse_values - structure holding values to match on an incoming frame
*  dest          - On match, sets frame's destination
*  action        - On match, sets frame's action (drop, set to target, set to cpu...)              
*                
* OUTPUTS:
uint32_t*                 tpma_handle - return value, last rule's index
* OUTPUTS:
* *tpma_handle       - rule identifier .
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_add_l2_US   (tpma_l2_key_t          *service_key, 
                                     tpm_src_port_type_t     src_port,
                                     uint32_t                parse_cmd_bm,
                                     tpm_l2_acl_key_t       *parse_values,
                                     tpm_pkt_frwd_t         *dest,
                                     tpm_pkt_mod_t          *mod,
                                     tpm_pkt_action_t        action,
                                     uint32_t               *tpma_handle){

    tpma_proc_status_t   ret_val; 
    uint32_t             max_l2_rule_count;
    uint32_t             current_l2_rule_count;
    uint32_t             max_l3_rule_count;
    uint32_t             current_l3_rule_count;
    uint32_t             rule_idx;
    struct timeval      currentTimeOfDay_b4;
    struct timeval      currentTimeOfDay_after;

    #ifndef first_phase
    gettimeofday(&currentTimeOfDay_b4, NULL);
    #endif
    
    
    /*When Ether type bit is set in the parse_cmd_bm, rule should be set in l3 section*/
    if (parse_cmd_bm & PARSECOMMAND_L2_ETH_TYPE){
        if (get_max_l3_rule_count(TPMA_DIRECTION_US,&max_l3_rule_count)==FALSE){
            DBG_PRINT("Fail to get_max_l2_rule_count");
            return TPMAPROC_GENERROR;
        }
        if (get_current_l3_rule_count(TPMA_DIRECTION_US,&current_l3_rule_count)==FALSE){
            DBG_PRINT("Fail to get_max_l3_rule_count");
            return TPMAPROC_GENERROR;
        }
        /*verify there is room for another rule in the L3US section*/
        if (current_l3_rule_count >= max_l3_rule_count){
            DBG_PRINT("L3 US DB is FULL");
            return (TPMAPROC_DATABASE_FULL); 
        }

    }
    else{
        /*Otherwise, rule is set in the l2 UP section*/
        if (get_max_l2_rule_count(TPMA_DIRECTION_US,&max_l2_rule_count)==FALSE){
            DBG_PRINT("Fail to get_max_l2_rule_count");
            return TPMAPROC_GENERROR;
        }
        if (get_current_l2_rule_count(TPMA_DIRECTION_US,&current_l2_rule_count)==FALSE){
            DBG_PRINT("Fail to get_max_l2_rule_count");
            return TPMAPROC_GENERROR;
        }
        /* Verify there is room for another rule in the L2US section*/
        if (current_l2_rule_count >= max_l2_rule_count) {
            DBG_PRINT("L2 US DB is FULL");
            return (TPMAPROC_DATABASE_FULL); 
        }
    }

    ret_val = tpma_add_l2_rule (TPMA_DIRECTION_US,
                                service_key,
                                src_port,
                                (tpma_parse_command_t)parse_cmd_bm,
                                parse_values,
                                dest,
                                mod,
                                action,
                                &rule_idx);
    if (ret_val == TPMAPROC_OK) {
        /*On success, set *tpma_handle with rule's index*/
        *tpma_handle = rule_idx;
    }
    #ifndef first_phase
    gettimeofday(&currentTimeOfDay_after, NULL);
    DBG_PRINT( "After :%s %d:%d", __FUNCTION__, currentTimeOfDay_after.tv_sec-currentTimeOfDay_b4.tv_sec, 
                                                   currentTimeOfDay_after.tv_usec-currentTimeOfDay_b4.tv_usec);
    #endif

    return (ret_val);



}

/*******************************************************************************
* tpma_add_l2_DS()
*
* DESCRIPTION:      Verify there is room for another rule in the L2DS section
*                   When Ether type bit is set in the parse_cmd_bm, verify there is room for another rule in the L3DS section
*                   Call tpma_add_l2_rule.
*                   On success, set *tpma_handle with rule's index.
*
* INPUTS:
* *service_key     - bridge_id
*                  - source tp
*                  - dest tp   
*  src_port      - tpm enum; 
*  parse_cmd_bm  - bitmap to indicate which values to match in the *parse_values 
*  *parse_values - structure holding values to match on an incoming frame
*  dest          - On match, sets frame's destination
*  action        - On match, sets frame's action (drop, set to target, set to cpu...)              
*                
* OUTPUTS:
uint32_t*                 tpma_handle - return value, last rule's index
* OUTPUTS:
* *tpma_handle       - rule identifier .
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/ 

tpma_proc_status_t tpma_add_l2_DS (tpma_l2_key_t          *service_key,  
                                   tpm_src_port_type_t    src_port,      
                                   uint32_t               parse_cmd_bm,  
                                   tpm_l2_acl_key_t       *parse_values, 
                                   tpm_pkt_frwd_t         *dest,         
                                   tpm_pkt_mod_t          *mod,          
                                   tpm_pkt_action_t       action,        
                                   uint32_t               *tpma_handle){

 

    tpma_proc_status_t   ret_val; 
    uint32_t             max_l2_rule_count;
    uint32_t             current_l2_rule_count;
    uint32_t             max_l3_rule_count;
    uint32_t             current_l3_rule_count;
    uint32_t             rule_idx;
    struct timeval      currentTimeOfDay_b4;
    struct timeval      currentTimeOfDay_after;

    #ifndef first_phase
    gettimeofday(&currentTimeOfDay_b4, NULL);
    #endif
    
   /*When Ether type bit is set in the parse_cmd_bm, rule should be set in l3 section*/
    if (parse_cmd_bm & PARSECOMMAND_L2_ETH_TYPE){
        if (get_max_l3_rule_count(TPMA_DIRECTION_DS,&max_l3_rule_count)==FALSE){
            DBG_PRINT("Fail to get_max_l2_rule_count");
            return TPMAPROC_GENERROR;
        }
        if (get_current_l3_rule_count(TPMA_DIRECTION_DS,&current_l3_rule_count)==FALSE){
            DBG_PRINT("Fail to get_max_l3_rule_count");
            return TPMAPROC_GENERROR;
        }
        /*verify there is room for another rule in the L3DS section*/
        if (current_l3_rule_count >= max_l3_rule_count){
            DBG_PRINT("L3 DS DB is FULL");
            return (TPMAPROC_DATABASE_FULL); 
        }

    }
    else{
        /*Otherwise, rule is set in the l2 UP section*/
        if (get_max_l2_rule_count(TPMA_DIRECTION_DS,&max_l2_rule_count)==FALSE){
            DBG_PRINT("Fail to get_max_l2_rule_count");
            return TPMAPROC_GENERROR;
        }
        if (get_current_l2_rule_count(TPMA_DIRECTION_DS,&current_l2_rule_count)==FALSE){
            DBG_PRINT("Fail to get_max_l2_rule_count");
            return TPMAPROC_GENERROR;
        }
        /* Verify there is room for another rule in the L2DS section*/
        if (current_l2_rule_count >= max_l2_rule_count) {
            DBG_PRINT("L2 DS DB is FULL");
            return (TPMAPROC_DATABASE_FULL); 
        }
    }

     
    ret_val = tpma_add_l2_rule (TPMA_DIRECTION_DS,
                                service_key,
                                src_port,
                                (tpma_parse_command_t)parse_cmd_bm,
                                parse_values,
                                dest,
                                mod,
                                action,
                                &rule_idx);
    if (ret_val == TPMAPROC_OK) {
        /*On success, set *tpma_handle with rule's index.*/
        *tpma_handle = rule_idx;
    }
    #ifndef first_phase
    gettimeofday(&currentTimeOfDay_after, NULL);
    DBG_PRINT( "After :%s %d:%d", __FUNCTION__, currentTimeOfDay_after.tv_sec-currentTimeOfDay_b4.tv_sec, 
                                                   currentTimeOfDay_after.tv_usec-currentTimeOfDay_b4.tv_usec);
    #endif

    return (ret_val);
                                   

}

/*******************************************************************************
* tpma_add_ipv4_MC()
*
* DESCRIPTION:      Search MC database for rules with the same key (VLAN ID, source IP and destination IP) 
*                              If found, get the stream number and try to update the bitmap of target ports. 
*                           call tpm_updt_ipv4_mc_stream
*                              Otherwise, check if there is more room in the database for another rule. 
*                              If so, find new rule number and try to set it (call tpm_add_ipv4_mc_stream). 
*
* INPUTS:
* vid               - VLAN ID      0 - priority tag
*                           4096 - untagged
*                           0xFFFF - don't care 
* IPv4_src_add     - IPv4 source IP address in network order.      
*                           0.0.0.0 - if should be ignored
* IPv4_dest_add  - IPv4 destination IP address in network order.             
* dest_port_bm   - bitmap which includes all destination UNI ports or destination ports with a loopback to CPU       
*
* OUTPUTS:
* *tpma_handle       - rule identifier .
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
* COMMENTS:
* Rule's order is not important 
*******************************************************************************/

tpma_proc_status_t tpma_add_ipv4_MC (uint32_t    vid, 
                                     tpma_ip_t   IPv4_src_add,
                                     tpma_ip_t   IPv4_dest_add,
                                     uint32_t    dest_port_bm,
                                     uint32_t    *tpma_handle){

    tpma_IPv4_mc_rule_t new_rule;
    tpm_error_code_t    ret_val;
    uint32_t            rule_num;
    uint32_t            owner_id;
    uint32_t            current_rules_count;
    uint32_t            max_rules_count;
    bool                src_ignore;  

    #ifdef first_phase
        return TPMAPROC_OK;
    #endif

    get_l4_owner_id(&owner_id);
    get_current_l4_mc_rule_count(&current_rules_count);
    get_max_l4_mc_rule_count(&max_rules_count);

    memset(&new_rule, 0, sizeof(tpma_IPv4_mc_rule_t));
    new_rule.key.vlan_id = vid;
    memcpy(&new_rule.key.IPv4_src_add.ip ,IPv4_src_add.ip , sizeof(tpma_ip_t));
    memcpy(&new_rule.key.IPv4_dest_add.ip,IPv4_dest_add.ip, sizeof(tpma_ip_t));

    #ifdef MNG_DEBUG
    print_l4_rule("tpma_add_ipv4_MC", &new_rule);
    DBG_PRINT("\n dest_port_bm = %d", dest_port_bm );
    #endif

    /*Search MC database for rules with the same key (VLAN ID, source IP and destination IP)*/
    if (find_IPv4_mc_rule_by_key(&new_rule.key, &rule_num)){
        DBG_PRINT("\n owner id = %d", owner_id );
        DBG_PRINT("\n rule No = %d", rule_num );

    /*found, get the stream number and try to update the bitmap of target ports. */
        ret_val = tpm_updt_ipv4_mc_stream(owner_id,
                                          rule_num,
                                          dest_port_bm);

        if (ret_val != TPM_RC_OK) {
            return ((tpma_proc_status_t)ret_val);
        }
    }
    else{
        /*Otherwise, check if there is more room in the database for another rule. 
        If so, find new rule number and try to set it (call tpm_add_ipv4_mc_stream). */

        if (current_rules_count < max_rules_count) {
            src_ignore = ((IPv4_src_add.ip[0]==0) && 
                          (IPv4_src_add.ip[1]==0) &&
                          (IPv4_src_add.ip[2]==0) &&
                          (IPv4_src_add.ip[3]==0))?1:0;

            /*add to last */
             DBG_PRINT("\n rule No = %d", current_rules_count );
            DBG_PRINT("\n src_ignore = %d", src_ignore );

            ret_val = tpm_add_ipv4_mc_stream(owner_id, 
                                             current_rules_count, 
                                             vid, 
                                             IPv4_src_add.ip, 
                                             IPv4_dest_add.ip, 
                                             (uint8_t)src_ignore,
                                             dest_port_bm);

            if (ret_val != TPM_RC_OK) {
                return ((tpma_proc_status_t)ret_val);
            }

            new_rule.stream_number = current_rules_count;

            if(insert_IPv4_mc_rule(rule_num,&new_rule)== FALSE){
                DBG_PRINT("Fail to insert_IPv4_mc_rule");
                return TPMAPROC_DATABASE_FULL;
            }
                
        }
        else{
            return TPMAPROC_DATABASE_FULL;
        }
    }

    return TPMAPROC_OK;

}

 

/******************************************************************************/
/********************************** Delete APIs *******************************/
/******************************************************************************/

/*******************************************************************************
* tpma_reset_all()
*
* DESCRIPTION:  First, reset all ports configuration:
*                   Loop on all ports, for each call a TPM reset function depending on the port type (ANI, UNI)
*              Second, clear PnC:
*                              Starting with index ==0 call to tpm_get_next_valid_rule on l2 (upstream and downstream). 
*                   Starting with index ==0 call to tpm_get_next_valid_rule on L4MC . 
*
* INPUTS:
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. On error, TPMAPROC_GENERROR is returned.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_reset_all(){

    uint32_t                owner_id;
    uint32_t                max_depth =  0;
    uint32_t                cur_depth = 0;
    tpma_l2_rule_t*         l2_db;
    tpma_l3_rule_t*         l3_db;
    tpma_IPv4_mc_rule_t*    l4_mc_db;

    get_l2_owner_id(&owner_id);

    tpm_mib_reset(owner_id,0);

     if (get_l2_db_parameters (TPMA_DIRECTION_US,&l2_db, &cur_depth ,&max_depth) == FALSE){
        DBG_PRINT("Fail to get_l2_db_parameters TPMA_DIRECTION_US");
        return TPMAPROC_GENERROR;

    }

    if (cur_depth != 0 ) {

       memset (l2_db,0,sizeof(tpma_l2_rule_t)*max_depth);

       if (set_current_l2_rule_count(TPMA_DIRECTION_US, 0) == FALSE){
           DBG_PRINT("Fail to set_current_l2_rule_count TPMA_DIRECTION_US");
           return TPMAPROC_GENERROR;
    
       }
    }
     

    cur_depth = 0;

    if (get_l2_db_parameters (TPMA_DIRECTION_DS,&l2_db, &cur_depth ,&max_depth) == FALSE){
        DBG_PRINT("Fail to get_l2_db_parameters TPMA_DIRECTION_DS");
        return TPMAPROC_GENERROR;

    }

    if (cur_depth != 0 ) {

       memset (l2_db,0,sizeof(tpma_l2_rule_t)*max_depth);

       if (set_current_l2_rule_count(TPMA_DIRECTION_DS, 0) == FALSE){
           DBG_PRINT("Fail to set_current_l2_rule_count TPMA_DIRECTION_DS");
           return TPMAPROC_GENERROR;

       }
    }

    cur_depth = 0;

    if (get_l3_db_parameters (TPMA_DIRECTION_US,&l3_db, &cur_depth ,&max_depth) == FALSE){
        DBG_PRINT("Fail to get_l3_db_parameters TPMA_DIRECTION_US");
        return TPMAPROC_GENERROR;

    }

    if (cur_depth != 0 ) {

       memset (l3_db,0,sizeof(tpma_l3_rule_t)*max_depth);

       if (set_current_l3_rule_count(TPMA_DIRECTION_US, 0) == FALSE){
            DBG_PRINT("Fail to set_current_l3_rule_count TPMA_DIRECTION_US");
            return TPMAPROC_GENERROR;
    
        }
    }

    cur_depth = 0;

    if (get_l3_db_parameters (TPMA_DIRECTION_DS,&l3_db, &cur_depth ,&max_depth) == FALSE){
        DBG_PRINT("Fail to get_l3_db_parameters TPMA_DIRECTION_DS");
        return TPMAPROC_GENERROR;

    }

    if (cur_depth != 0 ) {

       memset (l3_db,0,sizeof(tpma_l3_rule_t)*max_depth);

       if (set_current_l3_rule_count(TPMA_DIRECTION_DS, 0) == FALSE){
            DBG_PRINT("Fail to set_current_l3_rule_count TPMA_DIRECTION_DS");
            return TPMAPROC_GENERROR;
    
        }
    }

     #ifdef first_phase
        return TPMAPROC_OK;
    #endif 

   
    if (tpm_erase_section(owner_id, APIG_L2_ACL) != TPM_RC_OK){
        DBG_PRINT("Fail to tpm_erase_section APIG_L2_ACL");
        return TPMAPROC_GENERROR;
    }

    if (get_l2_db_parameters (TPMA_DIRECTION_US,&l2_db, &cur_depth ,&max_depth) == FALSE){
        DBG_PRINT("Fail to get_l2_db_parameters TPMA_DIRECTION_US");
        return TPMAPROC_GENERROR;

    }
    memset (l2_db,0,sizeof(tpma_l2_rule_t)*max_depth);

    if (set_current_l2_rule_count(TPMA_DIRECTION_US, 0) == FALSE){
        DBG_PRINT("Fail to set_current_l2_rule_count TPMA_DIRECTION_US");
        return TPMAPROC_GENERROR;

    }

    if (get_l2_db_parameters (TPMA_DIRECTION_DS,&l2_db, &cur_depth ,&max_depth) == FALSE){
        DBG_PRINT("Fail to get_l2_db_parameters TPMA_DIRECTION_DS");
        return TPMAPROC_GENERROR;

    }
    memset (l2_db,0,sizeof(tpma_l2_rule_t)*max_depth);
    if (set_current_l2_rule_count(TPMA_DIRECTION_DS, 0) == FALSE){
        DBG_PRINT("Fail to set_current_l2_rule_count TPMA_DIRECTION_DS");
        return TPMAPROC_GENERROR;

    }
    memset (l2_db,0,sizeof(tpma_l2_rule_t)*max_depth);
   
    get_l3_owner_id(&owner_id);

    if (tpm_erase_section(owner_id, APIG_L3TYPE_ACL) != TPM_RC_OK){
        DBG_PRINT("Fail to tpm_erase_section APIG_L3TYPE_ACL");
        return TPMAPROC_GENERROR;
    }

    if (get_l3_db_parameters (TPMA_DIRECTION_US,&l3_db, &cur_depth ,&max_depth) == FALSE){
        DBG_PRINT("Fail to get_l3_db_parameters TPMA_DIRECTION_US");
        return TPMAPROC_GENERROR;

    }
    memset (l3_db,0,sizeof(tpma_l3_rule_t)*max_depth);

    if (set_current_l3_rule_count(TPMA_DIRECTION_US, 0) == FALSE){
        DBG_PRINT("Fail to set_current_l2_rule_count TPMA_DIRECTION_US");
        return TPMAPROC_GENERROR;

    }

    if (get_l3_db_parameters (TPMA_DIRECTION_DS,&l3_db, &cur_depth ,&max_depth) == FALSE){
        DBG_PRINT("Fail to get_l3_db_parameters TPMA_DIRECTION_DS");
        return TPMAPROC_GENERROR;

    }
    memset (l3_db,0,sizeof(tpma_l3_rule_t)*max_depth);
    if (set_current_l3_rule_count(TPMA_DIRECTION_DS, 0) == FALSE){
        DBG_PRINT("Fail to set_current_l3_rule_count TPMA_DIRECTION_DS");
        return TPMAPROC_GENERROR;

    }
  

    get_l4_mc_owner_id(&owner_id);

    if (tpm_erase_section(owner_id, APIG_IPv4_MC) != TPM_RC_OK){
        DBG_PRINT("Fail to tpm_erase_section APIG_IPv4_MC");
        return TPMAPROC_GENERROR;
    }
    get_l4_mc_db_parameters(&l4_mc_db, &cur_depth, &max_depth);

    memset (l4_mc_db,0,sizeof(tpma_IPv4_mc_rule_t)*max_depth);

    set_current_l4_mc_rule_count(0);

 

    /*reset switch */

    /*what is the API????*/
    return TPMAPROC_OK;

 

}


 

/*******************************************************************************
* tpma_remove_bridge()
*
* DESCRIPTION:  Search l2 database (both US and DS) for rules with matching bridge ID
*              If found,
*              Reset participating ports configuration (source and destination)
*              Go through the DS, US database and find rule numbers to be removed, 
*               call  tpm_del_l2_prim_acl_rule (notice  that rules index updates as the rules are deleted)
*
* INPUTS:
* bridge_id               - ME class concatenated with instance ID 
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. On error, TPMAPROC_GENERROR is returned.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_remove_bridge   (uint32_t bridge_id){

   return (remove_by_key(bridge_id, 0, TRUE));

}

/*******************************************************************************
* tpma_remove_leg()
*
* DESCRIPTION:  Search l2 database (both US and DS) for rules with matching TP ID
*              If found,
*              Reset port configuration
*              Go through the DS, US database and find rule numbers to be removed, 
*               call  tpm_del_l2_prim_acl_rule (notice  that rules index updates as the rules are deleted)
*
* INPUTS:
* bridge_id               - ME class concatenated with instance ID 
* tp_id          - ME class concatenated with instance ID 
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. On error, TPMAPROC_GENERROR is returned.
*
* COMMENTS:
* notice  that rules index updates as the rules are deleted
*******************************************************************************/

tpma_proc_status_t tpma_remove_leg   (uint32_t bridge_id ,uint32_t tp_id){

    return (remove_by_key(bridge_id, tp_id, FALSE));

}

/*******************************************************************************
* tpma_remove_by_handle()
*
* DESCRIPTION: When a rule is set by the L2DA, the rule's index is returned as a handle to the L2DA
*              As we can see in set_rule_position, there are cases where TPMA adds or updates more then the requestde rule alone. 
*              So generally, I search the upstream and downstrea databases from a rule witha matching index as the given handle. 
*              When found, I inspect the found rule. 
*              If the rule attached to the handle is a tagged rule it means that it refers to 2 rules the 1 tag 'any rule and the 2 tag 'any' rule
*              I update both to drop without modifications (sort of default)
*              If the rule is one of the 'any' rules (2 tag 1 tag and NO tag)   
*              If found,
*              Call  tpm_del_l2_prim_acl_rule or tpm_del_ipv4_mc_stream
*
* INPUTS:
* tpma_handler - rule idetifier 
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. On error, TPMAPROC_GENERROR is returned.
*
* COMMENTS:
* notice  that rules index updates as the rules are deleted
*******************************************************************************/

tpma_proc_status_t tpma_remove_by_handle   (uint32_t tpma_handle){

  

    uint32_t            rule_num;
    uint32_t            removed_rule_num;
    uint32_t            start_rule_num;
    tpma_l2_rule_t*     l2_db;
    tpma_direction_t    dir;
    tpma_proc_status_t  tpma_ret_val;


    /*first look into l2 downstream database*/
    if(find_l2_rule_by_handle(tpma_handle, TPMA_DIRECTION_DS, &rule_num)==FALSE){
        if(find_l2_rule_by_handle(tpma_handle, TPMA_DIRECTION_US, &rule_num)==FALSE){
            DBG_PRINT("Did not find %d in US AND DS", tpma_handle);
            return TPMAPROC_NOT_FOUND;
        }
        else{
            dir = TPMA_DIRECTION_US;
        }
    }
    else{
        dir = TPMA_DIRECTION_DS;
    }

    if (get_l2_db(dir,&l2_db) == FALSE) {
        DBG_PRINT("Fail to get_l2_db");
        return TPMAPROC_GENERROR;
    }

     if (dir == TPMA_DIRECTION_US) {
        /*upstream traffic is arranged by the source port*/
         if (find_first_l2_rule_by_src_upstream(l2_db[rule_num].src_port, &start_rule_num)== FALSE){
             DBG_PRINT("Fail to find first rule in this group");
             return TPMAPROC_GENERROR;
        }
    }
    else {/*all ds trafficis refered to as a single unit*/
        start_rule_num = 0;
    }


    /*treat special cases*/

    /*when the setting a tagged rule, 2 rules are updated the 'any' 2 tag rule and the 'any' 1 tag rule 
    The INDEX retured is of the 1 tag 'any' rule. 
    BOTH rules should be updated back to default: action drop and NO modification
    */
    if (l2_db[rule_num].parse_rule_bm & PARSECOMMAND_L2_TAGGED){
        /*First, look 4 the 2 tag 'any rule*/
        if (find_first_rule_by_length(dir,TPMA_LENGTH_ANY_TWO_TAG,l2_db[rule_num].src_port,start_rule_num,&removed_rule_num)==FALSE){
            DBG_PRINT("Fail to find 2 tag 'any rule to update tagged related handle");
            return TPMAPROC_GENERROR;
        }
        /*re-set 2 tag 'any' rule */
        /*set 2 tagged 'any' rule back to drop WIHOUT modification - default state*/
        /*remove tagged spesification*/
        if (l2_db[removed_rule_num].parse_rule_bm & PARSECOMMAND_L2_TAGGED) {
            l2_db[rule_num].parse_rule_bm &= ~(PARSECOMMAND_L2_TAGGED);
        }
        tpma_ret_val = update_tag_any_tagged_rules(dir,
                                                   &(l2_db[removed_rule_num]),
                                                   NULL,
                                                   TPM_ACTION_DROP_PK,
                                                   start_rule_num);
        if (tpma_ret_val != TPMAPROC_OK) {
            DBG_PRINT("fail to update_tag_any_tagged_rules 2 tag rule %d ret_val %d", removed_rule_num, tpma_ret_val);
            return (tpma_ret_val);
        }
        /*re-set 1 tag 'any' rule */
        /*set 1 tagged 'any' rule back to drop WIHOUT modification - default state*/
        /*remove tagged spesification*/
        l2_db[rule_num].parse_rule_bm &= ~(PARSECOMMAND_L2_TAGGED);
        tpma_ret_val = update_tag_any_tagged_rules(dir,
                                                   &(l2_db[rule_num]),
                                                   NULL,
                                                   TPM_ACTION_DROP_PK,
                                                   start_rule_num);

        if (tpma_ret_val != TPMAPROC_OK) {
            DBG_PRINT("fail to update_tag_any_tagged_rules 1 tag rule %d ret_val %d", rule_num, tpma_ret_val);
            return (tpma_ret_val);
        }

        return TPMAPROC_OK;

    }
    if ((l2_db[rule_num].rule_length ==  TPMA_LENGTH_NO_TAG)||
        (l2_db[rule_num].rule_length ==  TPMA_LENGTH_ANY_ONE_TAG)||
        (l2_db[rule_num].rule_length ==  TPMA_LENGTH_ANY_TWO_TAG)){
        /*remove tagged spesification*/
        if (l2_db[rule_num].parse_rule_bm & PARSECOMMAND_L2_UNTAGGED) {
            l2_db[rule_num].parse_rule_bm &= ~(PARSECOMMAND_L2_UNTAGGED);
        }
        if (l2_db[rule_num].parse_rule_bm & PARSECOMMAND_L2_TAGGED) {
            l2_db[rule_num].parse_rule_bm &= ~(PARSECOMMAND_L2_TAGGED);
        }
        
         tpma_ret_val = update_tag_any_tagged_rules(dir,
                                                    &(l2_db[rule_num]),
                                                    NULL,
                                                    TPM_ACTION_DROP_PK,
                                                    start_rule_num);
    
         if (tpma_ret_val != TPMAPROC_OK) {
             DBG_PRINT("fail to update_tag_any_tagged_rules %d", tpma_ret_val);
             return (tpma_ret_val);
         }
         return TPMAPROC_OK;
    }
    
    /*when the removed rule has a bit on of 1 tag rule, search the 2 tag rules for matching rule
    meaning with the SAME outer tag and with any inner tag */
    if (l2_db[rule_num].parse_rule_bm & PARSECOMMAND_L2_1VLAN_TAG)  {
        tpma_ret_val = remove_2tag_rule_only_outer_tag_set(dir,&(l2_db[rule_num]), start_rule_num);
       
    }/*the found 1 tag rule with the matching handle is removed @ the end of the function */


    if ((l2_db[rule_num].parse_rule_bm & PARSECOMMAND_L2_GEM_PORT) && (l2_db[rule_num].rule_length == TPMA_LENGTH_NO_TAG_GEM)) {
       /* when ONLY a gem port restriction is added TPMA adds 2 rule: 'any' 2 tag with this gem port and 'any' 1 tag with this gem port*/
        tpma_ret_val = remove_1_2_tag_any_GEM_rules(dir,&(l2_db[rule_num]), start_rule_num);
        
        if (tpma_ret_val != TPMAPROC_OK) {
            DBG_PRINT("fail to remove_1_2_tag_any_GEM_rules  ret_val = %d",  tpma_ret_val);
            return (tpma_ret_val);
        }
    }/*the found 1 tag rule with the matching handle is removed @ the end of the function */

    


    /*re-search look into l2 downstream database*/
    rule_num = 0;
    if(find_l2_rule_by_handle(tpma_handle, dir, &rule_num)==FALSE){
       DBG_PRINT("*********WOW ERROR**********");
       return TPMAPROC_GENERROR;
    }
    

    tpma_ret_val = delete_l2_rule(dir, rule_num);
    if (tpma_ret_val != TPMAPROC_OK) {
        DBG_PRINT("fail to delete_l2_rule %d, ret_val = %d", rule_num, tpma_ret_val);
        return (tpma_ret_val);
    }


    return TPMAPROC_OK;

}

 

/******************************************************************************/
/********************************** ANI APIs **********************************/
/******************************************************************************/

/*******************************************************************************
* tpma_ani_queue_set_scheduling()
*
* DESCRIPTION:   Configures the scheduling mode per WAN queue. 
*                For strict priority configuration, the priority is hardcoded by the Queue number.
*                For WRR priority configuration, the queue weight is set
*                   Call to tpm_tm_set_wan_egr_queue_sched
* INPUTS:
*   tp       -              ANI entity:            DestPrt_WAN, 
*                               DestPort_TCONT_0 - 7 (GPON) 
*                               DestPort_LLID_0 - 7 (EPON)
*   sched                -  scheduler mode per port:              strict = 0x01
*                                           WRR    = 0x02
*   queue_id -       Queue   0 - number of queues per TCON - 1
*   wrr_weight      The weight per queue when scheduling mode is WRR              1-256
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_ani_queue_set_scheduling (tpm_trg_port_type_t  tp, 
                                                  tpm_sw_sched_type_t  sched,
                                                  uint8_t              queue_id,
                                                  uint8_t              wrr_weight){


    tpm_error_code_t ret_val;
    uint32_t         owner_id;
    get_l2_owner_id(&owner_id);

    ret_val = tpm_tm_set_wan_egr_queue_sched(owner_id,
                                             tp,
                                             sched,
                                             queue_id,
                                             wrr_weight);

   
    return ((tpma_proc_status_t)ret_val);

}
/*******************************************************************************
* tpma_ani_queue_reset_scheduling()
*
* DESCRIPTION:   Configures the scheduling mode per WAN queue back to startup values. 
*                   Call to tpm_tm_reset_wan_queue_sched
*
* INPUTS:
*   tp       -              ANI entity:            DestPrt_WAN, 
*                               DestPort_TCONT_0 - 7 (GPON) 
*                               DestPort_LLID_0 - 7 (EPON)
*   queue_id -       Queue   0 - number of queues per TCON - 1
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_ani_queue_reset_scheduling (tpm_trg_port_type_t   tp, 
                                                    uint8_t               queue_id){

    /*return(translate_ret_val(tpm_tm_reset_wan_queue_sched(tpma_owner_id_l2,
                                                          tp,
                                                          queue_id)));
    */

    return TPMAPROC_OK;
}
/*******************************************************************************
* tpma_ani_set_egress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit of upstream traffic for 
*                upstream scheduling entity, - WAN port or TCONT or LLID. 
*                
*                When a queue_id is set to -1, call to tpm_tm_set_wan_sched_egr_rate_lim. 
*               Otherwise, call to tpm_tm_set_wan_queue_egr_rate_lim
*
* INPUTS:
*   tp       -              ANI entity:            DestPrt_WAN, 
*                               DestPort_TCONT_0 - 7 (GPON) 
*                               DestPort_LLID_0 - 7 (EPON)
*   queue_id -       Queue           0 - number of queues per TCON - 1
*                               OR -1 for all queues related to this TCONT
*   rate_val            Rate limitation value          
*   bucket_size     Bucket size value                
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_ani_set_egress_rate_limit (tpm_trg_port_type_t  tp, 
                                                   int8_t               queue_id,
                                                   uint32_t             rate_val,
                                                   uint32_t             bucket_size){


    tpm_error_code_t ret_val;
    uint32_t         owner_id;
    get_l2_owner_id(&owner_id);

    if (queue_id<0) {
        ret_val = tpm_tm_set_wan_sched_egr_rate_lim(owner_id,
                                                    tp,
                                                    rate_val,
                                                    bucket_size);

    }
    else{
        ret_val = tpm_tm_set_wan_queue_egr_rate_lim(owner_id,
                                                    tp,
                                                    queue_id,
                                                    rate_val,
                                                    bucket_size);

    }


    return ((tpma_proc_status_t)ret_val);

}

/*******************************************************************************
* tpma_ani_reset_eggress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit of upstream back to startup values. 
*                
*                When a queue_id is set to -1, call to tpm_tm_reset_wan_sched_egr_rate_lim. 
*              Otherwise, call to tpm_tm_reset_wan_queue_egr_rate_lim
* INPUTS:
*   tp       -              ANI entity:            DestPrt_WAN, 
*                               DestPort_TCONT_0 - 7 (GPON) 
*                               DestPort_LLID_0 - 7 (EPON)
*   queue_id -       Queue           0 - number of queues per TCON - 1
*                               OR -1 for all queues related to this TCONT
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_ani_reset_eggress_rate_limit (tpm_trg_port_type_t tp, 
                                                      int8_t          queue_id){
  /*  if (queue_id<0) {
        return(translate_ret_val(tpm_tm_reset_wan_sched_egr_rate_lim(tpma_owner_id_l2,
                                                                     tp)));
    }
    else{
        return(translate_ret_val(tpm_tm_reset_wan_queue_egr_rate_lim(tpma_owner_id_l2,
                                                                     tp,
                                                                     queue_id)));
    }
    
*/
    return TPMAPROC_OK;

}
/*******************************************************************************
* tpma_ani_set_ingress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit on a specific queue for downstream traffic. 
*                
*                When a queue_id is set to -1, call to tpm_tm_set_wan_ingr_rate_lim. 
*               Otherwise, call to tpm_tm_set_wan_q_ingr_rate_lim
* INPUTS:
*   queue_id -       Queue           0 - number of queues per TCON - 1
*                               OR -1 all downstream traffic from the WAN
*   rate_val            Rate limitation value          
*   bucket_size     Bucket size value                
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_ani_set_ingress_rate_limit (int8_t       queue_id,
                                                    uint32_t     rate_val,
                                                    uint32_t     bucket_size){

    tpm_error_code_t ret_val;
    uint32_t         owner_id;
    get_l2_owner_id(&owner_id);

    if (queue_id<0) {
        ret_val = tpm_tm_set_wan_ingr_rate_lim(owner_id,
                                               rate_val,
                                               bucket_size);
    }
    else{
        ret_val = tpm_tm_set_wan_q_ingr_rate_lim(owner_id,
                                                 queue_id,
                                                 rate_val,
                                                 bucket_size);
    }

    

    return ((tpma_proc_status_t)ret_val);

}

/*******************************************************************************
* tpma_ani_reset_ingress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit on a specific queue back to startup values.  
*                
*                When a queue_id is set to -1, call to tpm_tm_reset_wan_ingr_rate_lim. 
*               Otherwise, call to tpm_tm_reset_wan_q_ingr_rate_lim
*
* 
* INPUTS:
*   queue_id -       Queue           0 - number of queues per TCON - 1
*                               OR -1 all downstream traffic from the WAN
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_ani_reset_ingress_rate_limit (int8_t queue_id){

   /*  if (queue_id<0) {
        return(translate_ret_val(tpm_tm_reset_wan_ingr_rate_lim(tpma_owner_id_l2)));
    }
    else{
        return(translate_ret_val(tpm_tm_reset_wan_q_ingr_rate_lim(tpma_owner_id_l2,
                                                                  queue_id)));
    }

  */  return TPMAPROC_OK;

}
/******************************************************************************/
/********************************** UNI APIs **********************************/
/******************************************************************************/
tpma_proc_status_t tpma_sw_set_port_tagged (tpm_src_port_type_t port,
                                            uint8_t             allow_tagged){
    tpm_error_code_t ret_val;
    uint32_t        owner_id;
    get_l2_owner_id(&owner_id);

    ret_val = tpm_sw_set_port_tagged(owner_id, 
                                     port,
                                     allow_tagged);

    return ((tpma_proc_status_t)ret_val);
}
/******************************************************************************/
tpma_proc_status_t tpma_sw_set_port_untagged (tpm_src_port_type_t port,
                                              uint8_t             allow_untagged){
    tpm_error_code_t ret_val;
    uint32_t        owner_id;
    get_l2_owner_id(&owner_id);

    ret_val = tpm_sw_set_port_untagged(owner_id, 
                                       port,
                                       allow_untagged);

    return ((tpma_proc_status_t)ret_val);
}
/******************************************************************************/
tpma_proc_status_t tpma_sw_port_add_vid (tpm_src_port_type_t port,
                                         uint16_t            vid){

    tpm_error_code_t ret_val;
    uint32_t        owner_id;
    get_l2_owner_id(&owner_id);

    ret_val = tpm_sw_port_add_vid(owner_id,
                                  port,
                                  vid);

    return ((tpma_proc_status_t)ret_val);
}
/******************************************************************************/
tpma_proc_status_t tpma_sw_port_del_vid (tpm_src_port_type_t port,
                                         uint16_t            vid){

    tpm_error_code_t ret_val;
    uint32_t        owner_id;
    get_l2_owner_id(&owner_id);

    ret_val = tpm_sw_port_del_vid(owner_id,
                                  port,
                                  vid);

    return ((tpma_proc_status_t)ret_val);

}
/******************************************************************************/
tpma_proc_status_t tpma_sw_port_set_vid_filter (tpm_src_port_type_t src_port,
                                                uint8_t             vid_filter){


    tpm_error_code_t ret_val;
    uint32_t        owner_id;
    get_l2_owner_id(&owner_id);

    ret_val = tpm_sw_port_set_vid_filter(owner_id,
                                         src_port,
                                         vid_filter);

    return ((tpma_proc_status_t)ret_val);



}
/*******************************************************************************
* tpma_uni_queue_set_scheduling()
*
* DESCRIPTION:   Configures the scheduling mode per Ethernet port. 
*                With respect the value given in scheduling mode, 
*                   call to tpm_sw_set_uni_q_weight with weight values from weight_arr, 0 == unset. 
*                At any case generate a call to tpm_sw_set_uni_sched.
*
* INPUTS:
*   tp       -              uni port for configuring the scheduler type (limited to a specific UNI port)
*   sched                -  0x0 -  WRR priority on ALL queues
*               0x1 - strict on Q3 and WRR on Q0-2
*               0x2 - strict on Q2-3 and WRR on Q0-1
*               0x3 - strict priority on ALL queues 
*   weight_arr       The weight per queue when scheduling mode is WRR              1-8, 0 == unset
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_uni_queue_set_scheduling (tpm_trg_port_type_t     tp, 
                                                  tpm_sw_sched_type_t     sched,
                                                  uint8_t                 *weight_arr){

    uint32_t         i;
    tpm_error_code_t ret_val;
    uint32_t         owner_id;

    get_l2_owner_id(&owner_id);

    for (i=0; i<4;i++, weight_arr++) {
        if (*weight_arr != 0) {
            ret_val = tpm_sw_set_uni_q_weight(owner_id, i,*weight_arr);
            if (ret_val != TPM_RC_OK) {
                return ((tpma_proc_status_t)ret_val);
            }
        }
    }

    ret_val = tpm_sw_set_uni_sched(owner_id, 
                                   tp,
                                   sched);

    return ((tpma_proc_status_t)ret_val);
  

}

/*******************************************************************************
* tpma_uni_queue_reset_scheduling()
*
* DESCRIPTION:   Configures the scheduling mode per Ethernet port back to startup values. 
*                Call to tpm_tm_reset_uni_sched 
*                Followed by a call to tpm_sw_reset_uni_q_weight with all queues
*
* INPUTS:
*   tp       -              uni port for configuring the scheduler type (limited to a specific UNI port)
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_uni_queue_reset_scheduling (tpm_trg_port_type_t tp){

    /*

    uint32_t         i;
    tpm_error_code_t ret_val;

    ret_val = tpm_tm_reset_uni_sched(tpma_owner_id_l2, 
                                     tp);

     if (ret_val != TPM_RC_OK) {
         return ((tpma_proc_status_t)ret_val);
     }

     for (i=0; i<4;i++) {
          ret_val = tpm_sw_reset_uni_q_weight(tpma_owner_id_l2, i);
          if (ret_val != TPM_RC_OK) {
              return ((tpma_proc_status_t)ret_val);
          }
    }

    return TPMAPROC_OK;

    */

    return TPMAPROC_OK;

}

/*******************************************************************************
* tpma_uni_set_egress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit of an Ethernet UNI port. 
*                Call to tpm_sw_set_uni_egr_rate_limit 
*
* INPUTS:
*   uni_port   -       Related UNI port
*   rate_val   -        Rate limitation value
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_uni_set_egress_rate_limit (tpm_trg_port_type_t uni_port,
                                                   uint32_t            rate_val){



    tpm_error_code_t ret_val;
    uint32_t         owner_id;

    get_l2_owner_id(&owner_id);
    
    ret_val = tpm_sw_set_uni_egr_rate_limit(owner_id,
                                            uni_port,
                                            TPM_SW_LIMIT_LAYER2, rate_val);

    return ((tpma_proc_status_t)ret_val);

}

/*******************************************************************************
* tpma_uni_reset_egress_rate_limit()
*
* DESCRIPTION:   Configures the egress rate limit of an Ethernet UNI port back to startup values.  
*                Call to tpm_sw_reset_uni_egr_rate_limit 
*
* INPUTS:
*   uni_port   -       Related UNI port
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_uni_reset_egress_rate_limit (tpm_trg_port_type_t uni_port){

/*
    return(translate_ret_val(tpm_sw_reset_uni_egr_rate_limit(tpma_owner_id_l2,
                                                             uni_port)));

*/

    return TPMAPROC_OK;

}

/*******************************************************************************
* tpma_uni_set_ingress_rate_limit()
*
* DESCRIPTION:   Configures a policer function for a traffic class for an Ethernet UNI port. 
*                There are 4 globally defined traffic classes in the integrated switch. 
*                When tc < 0 
*                              call to tpm_sw_set_uni_ingr_police_rate
*               When tc > 0 
*                              call to tpm_sw_set_uni_tc_ingr_police_rate
*
* INPUTS:
*   uni_port   -       Related UNI port
*   cir             -    Committed information rate
*   cbs            -    Committed burst size
*   tc              -    Traffic class
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_uni_set_ingress_rate_limit (tpm_trg_port_type_t uni_port,
                                                    uint32_t       cir,
                                                    uint32_t       cbs,
                                                    int32_t        tc){


    tpm_error_code_t ret_val;
    uint32_t         owner_id;
    uint32_t         ebs = 0;

    get_l2_owner_id(&owner_id);

    if (tc<0) {
        ret_val = tpm_sw_set_uni_ingr_police_rate(owner_id,
                                                  uni_port,
                                                  TPM_SW_LIMIT_LAYER2,
                                                  cir,
                                                  cbs,
                                                  ebs);
        
    }
    else{
        ret_val = tpm_sw_set_uni_tc_ingr_police_rate(owner_id,
                                                     uni_port,
                                                     tc,
                                                     cir,
                                                     cbs);
    }

    

    return ((tpma_proc_status_t)ret_val);

}
/*******************************************************************************
* tpma_uni_reset_ingress_rate_limit()
*
* DESCRIPTION:   Configures a policer function for a traffic class for an Ethernet UNI port back to startup values.   
*
* INPUTS:
*   uni_port   -       Related UNI port
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_uni_reset_ingress_rate_limit (tpm_trg_port_type_t uni_port){

    /*
        return(translate_ret_val(tpm_sw_reset_uni_ingr_police_rate(tpma_owner_id_l2,
                                                                   uni_port)));

    */

    return TPMAPROC_OK;

}

/*******************************************************************************

* tpma_uni_set_max_mac_depth()
*
* DESCRIPTION:   Limits the number of MAC addresses per port. 
*                call to tpm_sw_set_port_max_macs
*
* INPUTS:
*   uni_port     -     Related UNI port
*   mac_per_port -  Maximum number of MAC addresses per port 1-255
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_uni_set_max_mac_depth (tpm_trg_port_type_t  uni_port,
                                               uint8_t              mac_per_port){

    tpm_error_code_t ret_val;
    uint32_t         owner_id;

    get_l2_owner_id(&owner_id);

    ret_val = tpm_sw_set_port_max_macs(owner_id,
                                       uni_port,
                                       mac_per_port);
    return ((tpma_proc_status_t)ret_val);

}

/*******************************************************************************
* tpma_uni_reset_max_mac_depth()
*
* DESCRIPTION:   Limits the number of MAC addresses per port back to startup values. 
*                call to tpm_sw_reset_port_max_macs
*
* INPUTS:
*   uni_port   -       Related UNI port
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_uni_reset_max_mac_depth (tpm_trg_port_type_t uni_port){

/*

    return(translate_ret_val(tpm_sw_reset_port_max_macs(tpma_owner_id_l2,
                                                        uni_port)));

*/
    return TPMAPROC_OK;

}

/*******************************************************************************
* tpma_uni_set_egress_flooding()
*
* DESCRIPTION:   Control the flooding behavior (unknown Dest MAC address) per port. 
*                call to tpm_sw_set_port_flooding
*
* INPUTS:
*   uni_port           - Egress port to apply flooding          
*   allow_flood - Flooding permission   0 - Don't permit unknown MAC destination flooding 
*                                       1 - Permit unknown MAC destination flooding
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_uni_set_egress_flooding (tpm_trg_port_type_t uni_port,
                                                 uint8_t     allow_flood){


    tpm_error_code_t ret_val;
    uint32_t         owner_id;

    get_l2_owner_id(&owner_id);

    ret_val = tpm_sw_set_port_flooding(owner_id,
                                       uni_port,
                                       TPM_FLOOD_UNKNOWN_UNICAST,
                                       allow_flood);
    return ((tpma_proc_status_t)ret_val);


}

/*******************************************************************************
* tpma_uni_reset_egress_flooding()
*
* DESCRIPTION:   Control the flooding behavior (unknown Dest MAC address) per port back to startup values.   
*                call to tpm_sw_reset_port_flooding
*
* INPUTS:
*   uni_port           - Egress port to apply flooding          
*
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/

tpma_proc_status_t tpma_uni_reset_egress_flooding (tpm_trg_port_type_t uni_port){

/*
    return(translate_ret_val(tpm_sw_reset_port_flooding(tpma_owner_id_l2,
                                                        uni_port)));
*/

    return TPMAPROC_OK;

}

/*******************************************************************************
* tpma_set_OMCI_channel()
*
* DESCRIPTION:      Establishes a communication channel for the OMCI management protocol.
*                   The API sets the gemportid, the Rx input queue in the CPU, and the
*                   Tx T-CONT and queue parameters, which are configured in the driver.
*
* INPUTS:
* gem_port           - for OMCI Rx frames - the gem port wherefrom the OMCI frames are received.
* cpu_rx_queue       - for OMCI Rx frames - the CPU rx queue number.
* tcont_num          - for OMCI Tx frames - the TCONT number where to send the OMCI frames.
* cpu_tx_queue       - for OMCI Tx frames - the CPU tx queue number.
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPMAPROC_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
tpma_proc_status_t tpma_set_OMCI_channel (uint32_t              gem_port,
                                          uint32_t              cpu_rx_queue,
                                          tpm_trg_port_type_t   tcont_num,
                                          uint32_t              cpu_tx_queue){

    uint32_t         owner_id, rc;
    uint32_t         omcc_port;
    uint32_t         config_bitmap;

    config_bitmap = 0x1;  /* blocking socket*/

/*    get_l2_owner_id(&owner_id);*/
    tpma_get_omcc_port(&omcc_port);

    /*onu id wasn't changed - don't reconfigure*/
   if (gem_port == omcc_port) {
        return TPMAPROC_OK;
    }

   /*-1 represent an unset state - don't remove the omcc rule (it doesn't exsits)*/
   if (omcc_port != TPMA_OMCC_PORT_INIT_VALUE) {
       /* perform update - init was done already */
       rc = mvGponOmciChannelUpdate( gem_port, tcont_num, cpu_tx_queue, cpu_rx_queue);
       if (rc != 0) {
           printf("%s: mvGponOmciChannelUpdate FAILED gem_port(%d) tcont_num(%d) cpu_tx_queue(%d) \n\r", 
                  __FUNCTION__, gem_port, tcont_num, cpu_tx_queue);
           return TPMAPROC_GENERROR;
       }
   }
   else {
        rc = mvGponOmciChannelInit( gem_port, tcont_num, cpu_tx_queue, cpu_rx_queue, config_bitmap);
        if (rc != 0) {
            printf("%s: mvGponOmciChannelInit FAILED gem_port(%d) tcont_num(%d) cpu_tx_queue(%d) config_bitmap(0x%x) \n\r", 
                   __FUNCTION__, gem_port, tcont_num, cpu_tx_queue, config_bitmap);
            return TPMAPROC_GENERROR;
        }
   }

    /*update TPMA databse with new omcc port */
    tpma_set_omcc_port(gem_port);

    return TPMAPROC_OK;

}
bool syncTPMvsTPMAdb(){

    tpm_error_code_t tpm_ret_val;
    uint32_t         rule_number;   
    uint32_t         rule_idx;  
    uint32_t         next_rule_number;   
    tpm_rule_entry_t tpm_rule;
    bool             is_match;

    /*start with retreiving l2 upstream rules*/

    #ifndef first_phase
    rule_number = 0;
   /* tpm_ret_val = tpm_get_next_valid_rule ( tpma_owner_id_l2,
                                            1,
                                            -1,
                                            TPM_RULE_L2_PRIM,
                                            &next_rule_number,
                                            &rule_idx,
                                            &tpm_rule);
   
  */  while (tpm_ret_val == TPM_RC_OK) {

         /*compare output*/
        if (tpma_l2_us_arr[rule_number].idx != rule_idx){
    
            /* compare key identifier*/
            DBG_PRINT("\n DB mismatch in L2 upstream rule No  = %d;\n tpma rule idx = %d tpm rule idx = %d \n",rule_number, 
                                                                                                                              rule_idx, 
                                                                                                                              tpma_l2_us_arr[rule_number].idx);
            is_match =  FALSE;
            break;
        }
        else{
            /*once key matches compare the reset*/
            if (tpma_l2_us_arr[rule_number].src_port != tpm_rule.l2_prim_key.src_port){
                /* compare src port*/
                DBG_PRINT("\n DB mismatch in L2 upstream rule No  = %d;\n tpma src port = %d tpm src port = %d \n",rule_number, 
                                                                                                                              tpm_rule.l2_prim_key.src_port, 
                                                                                                                              tpma_l2_us_arr[rule_number].src_port);
            }
            if (tpma_l2_us_arr[rule_number].parse_rule_bm != tpm_rule.l2_prim_key.parse_rule_bm){
                /* compare parse_rule_bm*/
                DBG_PRINT("\n DB mismatch in L2 upstream rule No  = %d;\n tpma parse_rule_bm = %d tpm parse_rule_bm = %d \n",rule_number, 
                                                                                                                              tpm_rule.l2_prim_key.parse_rule_bm, 
                                                                                                                              tpma_l2_us_arr[rule_number].parse_rule_bm);
            }
            if (memcmp(&(tpma_l2_us_arr[rule_number].l2_key),&(tpm_rule.l2_prim_key.l2_key),sizeof(tpm_l2_acl_key_t))!=0){
                /* compare l2_key values*/
                DBG_PRINT("\n DB mismatch in L2 upstream rule No  = %d;",rule_number);
                print_l2_acl_key("TPMA L2 key", &(tpma_l2_us_arr[rule_number].l2_key), tpma_l2_us_arr[rule_number].parse_rule_bm);
                print_l2_acl_key("TPM L2 key", &(tpm_rule.l2_prim_key.l2_key), tpm_rule.l2_prim_key.parse_rule_bm);

     
            }/*end of l2_key compare*/
        }/*end of else key rule idx matches*/
        rule_number = next_rule_number;
      /*  tpm_ret_val = tpm_get_next_valid_rule ( tpma_owner_id_l2,
                                                1,
                                                rule_number,
                                                TPM_RULE_L2_PRIM,
                                                &next_rule_number,
                                                &rule_idx,
                                                &tpm_rule);*/
    }/*end of while loop*/



    /*start with retreiving l2 downstream rules*/


    rule_number = 0;
    /*tpm_ret_val = tpm_get_next_valid_rule ( tpma_owner_id_l2,
                                            0,
                                            -1,
                                            TPM_RULE_L2_PRIM,
                                            &next_rule_number,
                                            &rule_idx,
                                            &tpm_rule);
   
    */while (tpm_ret_val == TPM_RC_OK) {

         /*compare output*/
        if (tpma_l2_ds_arr[rule_number].idx != rule_idx){
    
            /* compare key identifier*/
            DBG_PRINT("\n DB mismatch in L2 downstream rule No  = %d;\n tpma rule idx = %d tpm rule idx = %d \n",rule_number, 
                                                                                                                                rule_idx, 
                                                                                                                                tpma_l2_ds_arr[rule_number].idx);
            is_match =  FALSE;
            break;
        }
        else{
            /*once key matches compare the reset*/
            if (tpma_l2_ds_arr[rule_number].src_port != tpm_rule.l2_prim_key.src_port){
                /* compare src port*/
                DBG_PRINT("\n DB mismatch in L2 downstream rule No  = %d;\n tpma src port = %d tpm src port = %d \n",rule_number, 
                                                                                                                              tpm_rule.l2_prim_key.src_port, 
                                                                                                                              tpma_l2_ds_arr[rule_number].src_port);
            }
            if (tpma_l2_us_arr[rule_number].parse_rule_bm != tpm_rule.l2_prim_key.parse_rule_bm){
                /* compare parse_rule_bm*/
                DBG_PRINT("\n DB mismatch in L2 downstream rule No  = %d;\n tpma parse_rule_bm = %d tpm parse_rule_bm = %d \n",rule_number, 
                                                                                                                              tpm_rule.l2_prim_key.parse_rule_bm, 
                                                                                                                              tpma_l2_ds_arr[rule_number].parse_rule_bm);
            }
            if (memcmp(&(tpma_l2_ds_arr[rule_number].l2_key),&(tpm_rule.l2_prim_key.l2_key),sizeof(tpm_l2_acl_key_t))!=0){
                /* compare l2_key values*/
                DBG_PRINT("\n DB mismatch in L2 downstream rule No  = %d;",rule_number);
                
                print_l2_acl_key("TPMA L2 key", &(tpma_l2_ds_arr[rule_number].l2_key), tpma_l2_ds_arr[rule_number].parse_rule_bm);
                print_l2_acl_key("TPM L2 key", &(tpm_rule.l2_prim_key.l2_key), tpm_rule.l2_prim_key.parse_rule_bm);

            }/*end of l2_key compare*/
        }/*end of else key rule idx matches*/
        rule_number = next_rule_number;
  /*      tpm_ret_val = tpm_get_next_valid_rule ( tpma_owner_id_l2,
                                                0,
                                                rule_number,
                                                TPM_RULE_L2_PRIM,
                                                &next_rule_number,
                                                &rule_idx,
                                                &tpm_rule);
 */   }/*end of while loop*/



    #ifdef first_phase
        return (is_match);
    #endif

    rule_number = 0;
  /*  tpm_ret_val = tpm_get_next_valid_rule ( tpma_owner_id_l3,
                                            1,
                                            -1,
                                            TPM_RULE_L3_TYPE,
                                            &next_rule_number,
                                            &rule_idx,
                                            &tpm_rule);
 */  
    while (tpm_ret_val == TPM_RC_OK) {

         /*compare output*/
        if (tpma_l3_us_arr[rule_number].idx != rule_idx){
    
            /* compare key identifier*/
            DBG_PRINT("\n DB mismatch in L3 upstream rule No  = %d;\n tpma rule idx = %d tpm rule idx = %d \n",rule_number, 
                                                                                                                              rule_idx, 
                                                                                                                              tpma_l3_us_arr[rule_number].idx);
            is_match =  FALSE;
            break;
        }
        else{
            /*once key matches compare the reset*/

            if (tpma_l3_us_arr[rule_number].src_port != tpm_rule.l3_type_key.src_port){
                /* compare src port*/
                DBG_PRINT("\n DB mismatch in L3 upstream rule No  = %d;\n tpma src port = %d tpm src port = %d \n",rule_number, 
                                                                                                                              tpm_rule.l3_type_key.src_port, 
                                                                                                                              tpma_l3_us_arr[rule_number].src_port);
            }
            
            if (tpma_l3_us_arr[rule_number].parse_rule_bm != tpm_rule.l3_type_key.parse_rule_bm){
                /* compare parse_rule_bm*/
                DBG_PRINT("\n DB mismatch in L3 upstream rule No  = %d;\n tpma parse_rule_bm = %d tpm parse_rule_bm = %d \n",rule_number, 
                                                                                                                              tpm_rule.l3_type_key.parse_rule_bm, 
                                                                                                                              tpma_l3_us_arr[rule_number].parse_rule_bm);
            }
            if (memcmp(&(tpma_l3_us_arr[rule_number].l3_key),&(tpm_rule.l3_type_key.l3_key),sizeof(tpm_rule_l3_type_key_t))!=0){
                /* compare l2_key values*/
                DBG_PRINT("\n DB mismatch in L3 upstream rule No  = %d;",rule_number);
                
                if (tpma_l3_us_arr[rule_number].parse_rule_bm & TPM_L2_PARSE_ETYPE){
              
                    DBG_PRINT("\n TPMA L3 key upstream ether type = %x ", tpma_l3_us_arr[rule_number].l3_key.ether_type_key);
                }
                if (tpm_rule.l3_type_key.parse_rule_bm & TPM_L2_PARSE_ETYPE){
              
                    DBG_PRINT("\n TPM L3 key upstream ether type = %x ", tpm_rule.l3_type_key.l3_key.ether_type_key);
                }

                if ((tpma_l3_us_arr[rule_number].parse_rule_bm & TPM_L2_PARSE_PPPOE_SES)||
                    (tpma_l3_us_arr[rule_number].parse_rule_bm & TPM_L2_PARSE_PPP_PROT)){
              
                    DBG_PRINT("\n TPMA L3 key upstream PPPoE session = %d PPPoE protocol = %d", tpma_l3_us_arr[rule_number].l3_key.pppoe_key.ppp_session
                                                                                                    , tpma_l3_us_arr[rule_number].l3_key.pppoe_key.ppp_proto);
                }
                if ((tpm_rule.l3_type_key.parse_rule_bm & TPM_L2_PARSE_PPPOE_SES)||
                    (tpm_rule.l3_type_key.parse_rule_bm & TPM_L2_PARSE_PPP_PROT)){
              
                    DBG_PRINT("\n TPM L3 key upstream PPPoE session = %d PPPoE protocol = %d", tpm_rule.l3_type_key.l3_key.pppoe_key.ppp_session
                                                                                                   , tpm_rule.l3_type_key.l3_key.pppoe_key.ppp_proto);
                }

            }/*end of l3_key compare*/
        }/*end of else key rule idx matches*/
        rule_number = next_rule_number;
    /*    tpm_ret_val = tpm_get_next_valid_rule ( tpma_owner_id_l2,
                                                0,
                                                rule_number,
                                                TPM_RULE_L3_TYPE,
                                                &next_rule_number,
                                                &rule_idx,
                                                &tpm_rule);
 */  }/*end of while loop*/

    

    rule_number = 0;
   /* tpm_ret_val = tpm_get_next_valid_rule ( tpma_owner_id_l3,
                                            0,
                                            -1,
                                            TPM_RULE_L3_TYPE,
                                            &next_rule_number,
                                            &rule_idx,
                                            &tpm_rule);

  */  while (tpm_ret_val == TPM_RC_OK) {

         /*compare output*/
        if (tpma_l3_ds_arr[rule_number].idx != rule_idx){

            /* compare key identifier*/
            DBG_PRINT("\n DB mismatch in L3 downstream rule No  = %d;\n tpma rule idx = %d tpm rule idx = %d \n",rule_number, 
                                                                                                                                rule_idx, 
                                                                                                                                tpma_l3_ds_arr[rule_number].idx);
            is_match =  FALSE;
            break;
        }
        else{
            /*once key matches compare the reset*/

            if (tpma_l3_ds_arr[rule_number].src_port != tpm_rule.l3_type_key.src_port){
                /* compare src port*/
                DBG_PRINT("\n DB mismatch in L3 downstream rule No  = %d;\n tpma src port = %d tpm src port = %d \n",rule_number, 
                                                                                                                              tpm_rule.l3_type_key.src_port, 
                                                                                                                              tpma_l3_us_arr[rule_number].src_port);
            }

            if (tpma_l3_ds_arr[rule_number].parse_rule_bm != tpm_rule.l3_type_key.parse_rule_bm){
                /* compare parse_rule_bm*/
                DBG_PRINT("\n DB mismatch in L3 downstream rule No  = %d;\n tpma parse_rule_bm = %d tpm parse_rule_bm = %d \n",rule_number, 
                                                                                                                              tpm_rule.l3_type_key.parse_rule_bm, 
                                                                                                                              tpma_l3_ds_arr[rule_number].parse_rule_bm);
            }
            if (memcmp(&(tpma_l3_ds_arr[rule_number].l3_key),&(tpm_rule.l3_type_key.l3_key),sizeof(tpm_rule_l3_type_key_t))!=0){
                /* compare l2_key values*/
                DBG_PRINT("\n DB mismatch in L3 downstream rule No  = %d;",rule_number);

                if (tpma_l3_us_arr[rule_number].parse_rule_bm & TPM_L2_PARSE_ETYPE){

                    DBG_PRINT("\n TPMA L3 key downstream ether type = %x ", tpma_l3_ds_arr[rule_number].l3_key.ether_type_key);
                }
                if (tpm_rule.l3_type_key.parse_rule_bm & TPM_L2_PARSE_ETYPE){

                    DBG_PRINT("\n TPM L3 key downstream ether type = %x ", tpm_rule.l3_type_key.l3_key.ether_type_key);
                }

                if ((tpma_l3_us_arr[rule_number].parse_rule_bm & TPM_L2_PARSE_PPPOE_SES)||
                    (tpma_l3_us_arr[rule_number].parse_rule_bm & TPM_L2_PARSE_PPP_PROT)){

                    DBG_PRINT("\n TPMA L3 key downstream PPPoE session = %d PPPoE protocol = %d", tpma_l3_ds_arr[rule_number].l3_key.pppoe_key.ppp_session
                                                                                                               , tpma_l3_ds_arr[rule_number].l3_key.pppoe_key.ppp_proto);
                }
                if ((tpm_rule.l3_type_key.parse_rule_bm & TPM_L2_PARSE_PPPOE_SES)||
                    (tpm_rule.l3_type_key.parse_rule_bm & TPM_L2_PARSE_PPP_PROT)){

                    DBG_PRINT("\n TPM L3 key downstream PPPoE session = %d PPPoE protocol = %d", tpm_rule.l3_type_key.l3_key.pppoe_key.ppp_session
                                                                                                              , tpm_rule.l3_type_key.l3_key.pppoe_key.ppp_proto);
                }

            }/*end of l3_key compare*/
        }/*end of else key rule idx matches*/
        rule_number = next_rule_number;
        tpm_ret_val = tpm_get_next_valid_rule ( tpma_owner_id_l2,
                                                0,
                                                rule_number,
                                                TPM_RULE_L3_TYPE,
                                                &next_rule_number,
                                                &rule_idx,
                                                &tpm_rule);
    }/*end of while loop*/



    rule_number = 0;
  /*  tpm_ret_val = tpm_get_next_valid_rule ( tpma_owner_id_l4,
                                            0,
                                            -1,
                                            TPM_RULE_IPv4_MC,
                                            &next_rule_number,
                                            &rule_idx,
                                            &tpm_rule);

  */  while (tpm_ret_val == TPM_RC_OK) {

         /*compare output*/
        if (tpma_l4_mc_arr[rule_number].stream_number != rule_idx){

            /* compare key identifier*/
            DBG_PRINT("\n DB mismatch in IPv4 mc downstream rule No  = %d;\n tpma rule idx = %d tpm rule idx = %d \n",rule_number, 
                                                                                                                      rule_idx, 
                                                                                                                      tpma_l3_ds_arr[rule_number].idx);
            is_match =  FALSE;
            break;
        }
        else{
            /*once key matches compare the reset*/

            if (memcmp(&(tpma_l4_mc_arr[rule_number].key.IPv4_dest_add),&(tpm_rule.ipv4_mc_key.ipv4_dest_add),sizeof(uint8_t)* 4)!=0){                /* compare l2_key values*/
                DBG_PRINT("\n DB mismatch in IPv4 mc downstream rule No  = %d;",rule_number);

                DBG_PRINT("\n TPMA IPv4 mc src IPv4 = %d.%d.%d.%d", tpma_l4_mc_arr[rule_number].key.IPv4_dest_add[0],           
                                                                                    tpma_l4_mc_arr[rule_number].key.IPv4_dest_add[1],           
                                                                                    tpma_l4_mc_arr[rule_number].key.IPv4_dest_add[2],           
                                                                                    tpma_l4_mc_arr[rule_number].key.IPv4_dest_add[3]);

                DBG_PRINT("\n TPM IPv4 mc src IPv4 = %d.%d.%d.%d", tpm_rule.ipv4_mc_key.ipv4_dest_add[0],           
                                                                                    tpm_rule.ipv4_mc_key.ipv4_dest_add[1],           
                                                                                    tpm_rule.ipv4_mc_key.ipv4_dest_add[2],           
                                                                                    tpm_rule.ipv4_mc_key.ipv4_dest_add[3]);
            }
            if (memcmp(&(tpma_l4_mc_arr[rule_number].key.IPv4_src_add),&(tpm_rule.ipv4_mc_key.ipv4_src_add),sizeof(uint8_t)* 4)!=0){

                DBG_PRINT("\n TPMA IPv4 mc src IPv4 = %d.%d.%d.%d", tpma_l4_mc_arr[rule_number].key.IPv4_src_add[0],           
                                                                                    tpma_l4_mc_arr[rule_number].key.IPv4_src_add[1],           
                                                                                    tpma_l4_mc_arr[rule_number].key.IPv4_src_add[2],           
                                                                                    tpma_l4_mc_arr[rule_number].key.IPv4_src_add[3]);

                DBG_PRINT("\n TPM IPv4 mc src IPv4 = %d.%d.%d.%d", tpm_rule.ipv4_mc_key.ipv4_src_add[0],           
                                                                                    tpm_rule.ipv4_mc_key.ipv4_src_add[1],           
                                                                                    tpm_rule.ipv4_mc_key.ipv4_src_add[2],           
                                                                                    tpm_rule.ipv4_mc_key.ipv4_src_add[3]);
            }
            if (tpma_l4_mc_arr[rule_number].key.vlan_id != tpm_rule.ipv4_mc_key.vid){
                 DBG_PRINT("\n TPMA IPv4 mc src IPv4 = %d", tpma_l4_mc_arr[rule_number].key.vlan_id);

                 DBG_PRINT("\n TPM  IPv4 mc src IPv4 = %d", tpm_rule.ipv4_mc_key.vid);
            }

        }/*end of else key rule idx matches*/
        rule_number = next_rule_number;
   /*     tpm_ret_val = tpm_get_next_valid_rule ( tpma_owner_id_l4,
                                                0,               
                                                rule_number,              
                                                TPM_RULE_IPv4_MC,
                                                &next_rule_number,
                                                &rule_idx,
                                                &tpm_rule);      
 */   }/*end of while loop*/



    
    
    if (!is_match) {
        return (is_match);
    }
    #endif

}


#ifdef __cplusplus

}

#endif

 

 

 

 

