/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : I2C                                                       **/
/**                                                                          **/
/**  FILE        : I2cMain.c                                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file contains top level XVR I2C functions            **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "errorCode.h"
#include "globals.h"

#include "clish/shell.h"     // For extracting parameters
//#include "I2cMain.h"
//#include "i2c_cli.h"


bool_t i2c_cli_show_xvr_a2d_values (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    if (BOOL_TRUE != I2c_cli_show_xvr_a2d_values(NULL))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;  
}

bool_t i2c_cli_show_xvr_thresholds (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    if (BOOL_TRUE != I2c_cli_show_xvr_thresholds(NULL))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;   
}

bool_t i2c_cli_show_xvr_alarms_and_warnings (const clish_shell_t    *instance,
                                           const lub_argv_t       *argv)
{
    if (BOOL_TRUE != I2c_cli_show_xvr_alarms_and_warnings(NULL))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;   
}


bool_t i2c_cli_show_xvr_inventory (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    if (BOOL_TRUE != I2c_cli_show_xvr_inventory(NULL))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;   
}

bool_t i2c_cli_show_xvr_capability (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    if (BOOL_TRUE != I2c_cli_show_xvr_capability(NULL))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;   
}


