/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : I2C                                                       **/
/**                                                                          **/
/**  FILE        : I2cMain.c                                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file contains top level XVR I2C functions            **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "errorCode.h"
#include "globals.h"

#include "I2cMain.h"

#define BOOL_TRUE  1
#define BOOL_FALSE 0


bool I2c_cli_show_xvr_a2d_values (char* name)
{
    showXvrA2dValues(name);

    return BOOL_TRUE; 
}

bool I2c_cli_show_xvr_thresholds (char* name)
{
    showXvrThresholds(name);

    return BOOL_TRUE; 
}

bool I2c_cli_show_xvr_alarms_and_warnings (char* name)
{
    showXvrAlarmsAndWarnings(name);

    return BOOL_TRUE; 
}

bool I2c_cli_show_xvr_inventory (char* name)
{
    showXvrInventory(name);

    return BOOL_TRUE; 
}

bool I2c_cli_show_xvr_capability (char* name)
{
    showXvrCapability(name);

    return BOOL_TRUE; 
}


