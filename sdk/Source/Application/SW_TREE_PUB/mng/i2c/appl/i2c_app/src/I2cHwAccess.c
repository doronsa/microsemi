/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : I2C                                                       **/
/**                                                                          **/
/**  FILE        : I2cHwAccess.c                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file contains top level XVR I2C functions            **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **
 *   MODIFICATION HISTORY:
 *
 *
 ******************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>

#include "myi2c-dev.h"    // Should be <linux/i2c-dev.h>
#include "I2cHwAccess.h"

extern signed long is_support_onu_xvr_I2c;
extern unsigned long onu_xvr_i2c_funcs;
#if 0
static int check_funcs(int file, int size, int daddress, int pec)
{
	unsigned long funcs;

	/* check adapter functionality */
	if (ioctl(file, I2C_FUNCS, &funcs) < 0) {
		fprintf(stderr, "Error: Could not get the adapter "
			"functionality matrix: %s\n", strerror(errno));
		return -1;
	}

	switch (size) {
	case I2C_SMBUS_BYTE:
		if (!(funcs & I2C_FUNC_SMBUS_READ_BYTE)) {
			fprintf(stderr, MISSING_FUNC_FMT, "SMBus receive byte");
			return -1;
		}
		if (daddress >= 0
		 && !(funcs & I2C_FUNC_SMBUS_WRITE_BYTE)) {
			fprintf(stderr, MISSING_FUNC_FMT, "SMBus send byte");
			return -1;
		}
		break;

	case I2C_SMBUS_BYTE_DATA:
		if (!(funcs & I2C_FUNC_SMBUS_READ_BYTE_DATA)) {
			fprintf(stderr, MISSING_FUNC_FMT, "SMBus read byte");
			return -1;
		}
		break;

	case I2C_SMBUS_WORD_DATA:
		if (!(funcs & I2C_FUNC_SMBUS_READ_WORD_DATA)) {
			fprintf(stderr, MISSING_FUNC_FMT, "SMBus read word");
			return -1;
		}
		break;
	}

	if (pec
	 && !(funcs & (I2C_FUNC_SMBUS_PEC | I2C_FUNC_I2C))) {
		fprintf(stderr, "Warning: Adapter does "
			"not seem to support PEC\n");
	}

	return 0;
}
#endif


/******************************************************************************
 *
 * Function   : set_slave
 *
 * Description: This function sets the address of the device to be referenced
 *
 * Parameters :
 *
 * Returns    : bool
 *
 ******************************************************************************/

static bool set_slave(int file, int xvrBusAddress)
{
    if (ioctl(file, I2C_SLAVE, xvrBusAddress) < 0)
    {
        I2C_ERROR_PRINT("%s: ioctl I2C_SLAVE failed for device address 0x%x, %s(%d)\n",
                __FUNCTION__, xvrBusAddress, strerror(errno), errno);
        return false;
    }
    return true;
}

/******************************************************************************
 *
 * Function   : i2cslowdown
 *
 * Description: This function sleeps specified milliseconds
 *
 * Parameters :
 *
 * Returns    : void
 *
 ******************************************************************************/

static void i2cslowdown(unsigned int milliseconds)
{
    struct timespec tmReq;

    tmReq.tv_sec = (time_t)(milliseconds / 1000);
    tmReq.tv_nsec = (milliseconds % 1000) * 1000 * 1000;

    // return value not important here
    (void)nanosleep(&tmReq, NULL);
}

/******************************************************************************
 *
 * Function   : i2cHwRegionRead
 *
 * Description: This function reads in bytes from I2C hardware
 *
 * Parameters :
 *
 * Returns    : bool
 *
 ******************************************************************************/

bool i2cHwRegionRead(int xvrBusAddress, RegionDb_S *p_RegionDb, uint8_t *buf, int bufsize, int *numBytesRead)
{
    int            regIndx;
    int            indx;
    char           *i2cBusfilename = "/dev/i2c-0";
    int            file;
    RegionDescr_S  *p_RegDescr;
    int            res=0;
    int            insertIndx = 0;
    int len = 0;
    uint8_t * p_buf = buf;
    uint8_t address;

	if (xvrBusAddress < 0x03 || xvrBusAddress > 0x77)
    {
		I2C_ERROR_PRINT("%s: I2C device address 0x%x out of range (0x03-0x77)\n", __FUNCTION__, xvrBusAddress);
		return false;
	}

    // open device
	if ((file = open(i2cBusfilename, O_RDWR)) < 0)
    {
        I2C_ERROR_PRINT("%s: Could not open I2C bus file %s, %s(%d)\n", __FUNCTION__, i2cBusfilename, strerror(errno), errno);
        return false;
    }

    // Now set I2C device address
    if (set_slave(file, xvrBusAddress) == false)
    {
        close(file);
        return false;
    }

    for (regIndx = 0; regIndx < p_RegionDb->numEntries; regIndx++)
    {
        p_RegDescr = &p_RegionDb->regionDescrAra[regIndx];


        //printf("%s: i2c_smbus_write_byte value %d\n", __FUNCTION__, p_RegDescr->address);
        i2cslowdown(ICS_SLOW_DOWN_MILLI_SECONDS);
        if ((res = i2c_smbus_write_byte(file, p_RegDescr->address)) < 0)
        {

            I2C_ERROR_PRINT("%s: i2c_smbus_write_byte for start address %d, %s(%d)\n",
                    __FUNCTION__, p_RegDescr->address, strerror(errno), errno);
            close(file);
            return false;
        }

        /*#if 1 for T&W, #if 0 for delta*/
        if(SUPPORT_READ_BUFF == is_support_onu_xvr_I2c)
        {
            len = p_RegDescr->length;
            p_buf = buf;
            address = p_RegDescr->address;

            while(len>32)
            {
                if ((res = i2c_smbus_read_i2c_block_data(file,address,32,p_buf)) < 0)
                {
                    I2C_ERROR_PRINT("%s: i2c_smbus_read_i2c_block_data at address %d, %s(%d) index %d buf %s res %d\n",
                            __FUNCTION__, p_RegDescr->address + indx, strerror(errno), errno, indx, buf, res);
                    close(file);
                    return false;
                }

                p_buf += res;
                address +=res;
                len -= res;

            }

            if ((res = i2c_smbus_read_i2c_block_data(file,address,len,p_buf)) < 0)
            {
                I2C_ERROR_PRINT("%s: i2c_smbus_read_i2c_block_data at address %d, %s(%d) index %d buf %s res %d\n",
                        __FUNCTION__, p_RegDescr->address + indx, strerror(errno), errno, indx, buf, res);
                close(file);
                return false;
            }

            buf += p_RegDescr->length;
        }
        else if(SUPPORT_READ_BYTE == is_support_onu_xvr_I2c)
        {
            for (indx = 0; indx < p_RegDescr->length; indx++)
            {
                // read in bytes
                i2cslowdown(ICS_SLOW_DOWN_MILLI_SECONDS);
                if ((res = i2c_smbus_read_byte(file)) < 0)
                {
                    I2C_ERROR_PRINT("%s: i2c_smbus_read_byte at address %d, %s(%d) index %d buf %s res %d\n",
                            __FUNCTION__, p_RegDescr->address + indx, strerror(errno), errno, indx, buf, res);
                    close(file);
                    return false;
                }

                if (insertIndx < bufsize)
                {
                    //printf("%s: Inserting 0x%02x at indx %d\n", __FUNCTION__, (uint8_t)res, insertIndx);
                    buf[insertIndx++] = (uint8_t)res;
                }
                else
                {
                    I2C_ERROR_PRINT("%s: buffer overrun. Buffer size is %d. (Region #%d)\n", __FUNCTION__, bufsize, regIndx);
                    close(file);
                    return false;
                }
            }
        }
        else
        {
            fprintf(stderr,"is_support_onu_xvr_I2c is %d, no reading method!!!\n");
            close(file);
            return false;
        }
    }

    close(file);
    *numBytesRead = insertIndx;
    return true;
}


/******************************************************************************
 *
 * Function   : i2cHwRegionWrite
 *
 * Description: This function write in bytes to I2C hardware
 *
 * Parameters :
 *
 * Returns    : bool
 *
 ******************************************************************************/

bool i2cHwRegionWrite(int xvrBusAddress, RegionDb_S *p_RegionDb, uint8_t *buf, int bufsize, int *numBytesWrite)
{
    int            regIndx;
    int            indx;
    char           *i2cBusfilename = "/dev/i2c-0";
    int            file;
    RegionDescr_S  *p_RegDescr;
    int            res;
    int            insertIndx = 0;

	if (xvrBusAddress < 0x03 || xvrBusAddress > 0x77)
    {
		I2C_ERROR_PRINT("%s: I2C device address 0x%x out of range (0x03-0x77)\n", __FUNCTION__, xvrBusAddress);
		return false;
	}

    // open device
	if ((file = open(i2cBusfilename, O_RDWR)) < 0)
    {
        I2C_ERROR_PRINT("%s: Could not open I2C bus file %s, %s(%d)\n", __FUNCTION__, i2cBusfilename, strerror(errno), errno);
        return false;
    }

    // Now set I2C device address
    if (set_slave(file, xvrBusAddress) == false)
    {
        close(file);
        return false;
    }

    for (regIndx = 0; regIndx < p_RegionDb->numEntries; regIndx++)
    {
        p_RegDescr = &p_RegionDb->regionDescrAra[regIndx];


        //printf("%s: i2c_smbus_write_byte value %d\n", __FUNCTION__, p_RegDescr->address);
        i2cslowdown(ICS_SLOW_DOWN_MILLI_SECONDS);
        if ((res = i2c_smbus_write_byte(file, p_RegDescr->address)) < 0)
        {
            /*
            I2C_ERROR_PRINT("%s: i2c_smbus_write_byte for start address %d, %s(%d)\n",
                    __FUNCTION__, p_RegDescr->address, strerror(errno), errno);
                    */
            close(file);
            return false;
        }

        for (indx = 0; indx < p_RegDescr->length; indx++)
        {
            // read in bytes
            i2cslowdown(ICS_SLOW_DOWN_MILLI_SECONDS);
            if ((res = i2c_smbus_write_byte(file,buf[insertIndx])) < 0)
            {
                I2C_ERROR_PRINT("%s: i2c_smbus_read_byte at address %d, %s(%d)\n",
                        __FUNCTION__, p_RegDescr->address + indx, strerror(errno), errno);
                close(file);
                return false;
            }

            if (insertIndx < bufsize)
            {
                //printf("%s: Inserting 0x%02x at indx %d\n", __FUNCTION__, (uint8_t)res, insertIndx);
                insertIndx++;
            }
            else
            {
                I2C_ERROR_PRINT("%s: buffer overrun. Buffer size is %d. (Region #%d)\n", __FUNCTION__, bufsize, regIndx);
                close(file);
                return false;
            }
        }
    }

    close(file);
    *numBytesWrite = insertIndx;
    return true;
}


/*added by ken*/
signed long i2cHwRegionIsSupported(int xvrBusAddress, int first, int last)
{
    char           *i2cBusfilename = "/dev/i2c-0";
    int res;
    int            file;
    char buf[100];

    if (xvrBusAddress < first || xvrBusAddress > last)
    {
		fprintf(stderr, "%s: I2C device address 0x%x out of range (0x03-0x77)\n", __FUNCTION__, xvrBusAddress);
		return -1;
	}

    // open device
	if ((file = open(i2cBusfilename, O_RDWR)) < 0)
    {
        fprintf(stderr, "In this SFU could not open I2C bus file %s, %s(%d)\n", i2cBusfilename, strerror(errno), errno);
        return -1;
    }


    if (set_slave(file, xvrBusAddress) == false)
    {
        close(file);
        return -1;
    }

    if ((xvrBusAddress >= 0x30 && xvrBusAddress <= 0x37)
	 || (xvrBusAddress >= 0x50 && xvrBusAddress <= 0x5F))
    {
		res = i2c_smbus_read_byte(file);
        if(res >= 0)
        {
            close(file);
            return SUPPORT_READ_BYTE;
        }

        res =i2c_smbus_read_i2c_block_data(file,0,1,buf);
        if(res >= 0)
        {
            close(file);
            return SUPPORT_READ_BUFF;
        }

    }
	else
    {
		res = i2c_smbus_write_quick(file,I2C_SMBUS_WRITE);
        if(res >= 0)
        {
            close(file);
            return SUPPORT_WRITE_QUICK;
        }
    }

    //printf("This SFU does not support I2C XCVR function.\n");
    close(file);
    return res;

}

void i2cHwRegionGetI2CFuncs(unsigned long *p_funcs)
{
    char           *i2cBusfilename = "/dev/i2c-0";
    int            file;

    if ((file = open(i2cBusfilename, O_RDWR)) < 0)
    {
        I2C_ERROR_PRINT("%s: Could not open I2C bus file %s, %s(%d)\n", __FUNCTION__, i2cBusfilename, strerror(errno), errno);
        return;
    }

	if (ioctl(file, I2C_FUNCS, p_funcs) < 0)
    {
		I2C_ERROR_PRINT("Error: Could not get the adapter "
			"functionality matrix: %s\n", strerror(errno));
		close(file);
		return;

	}
    close(file);
    return;
}



