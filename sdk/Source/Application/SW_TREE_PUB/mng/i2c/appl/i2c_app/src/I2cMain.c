/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : I2C                                                       **/
/**                                                                          **/
/**  FILE        : I2cMain.c                                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file contains top level XVR I2C functions            **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>

#include "globals.h"
#include "MiscUtils.h"
#include "I2cHwAccess.h"
#include "I2cMain.h"
#include "apm_types.h"
#include "mipc.h"


INT32 is_support_onu_xvr_I2c = 0;
UINT32 onu_xvr_i2c_funcs = 0;
const char * i2c_mipc_name = "i2c";

OnuXvrA2D_S onu_xvr_a2d_values;
OnuXvrThresholds_S onu_xvr_thresholds;
OnuXvrThresholds_S onu_xvr_clear_thresholds;

unsigned int i2c_get_xvr_values_time;

extern void i2c_mipc_server_handler(char* inMsg, unsigned int size);

/*added by ken*/

void I2C_init()
{
    
    is_support_onu_xvr_I2c = i2cHwRegionIsSupported(OPERATIONAL_XVR_BUS_ADDRESS, 0x03, 0x77);
       
    i2cHwRegionGetI2CFuncs(&onu_xvr_i2c_funcs);

    memset(&onu_xvr_thresholds,0,sizeof(onu_xvr_thresholds));
    memset(&onu_xvr_clear_thresholds,0,sizeof(onu_xvr_clear_thresholds));
    localI2cApi_getOnuXvrThresholds(&onu_xvr_thresholds);
    i2c_get_xvr_values_time = osSecondsGet();
    localI2cApi_getOnuXvrA2dValues(&onu_xvr_a2d_values);    
    
}

void localI2cApi_whetherOnuXvrSupportI2c(signed long *support_onu_xvr_I2c)
{
    *support_onu_xvr_I2c = is_support_onu_xvr_I2c;
}

void localI2cApi_getOnuXvrI2CFunc(unsigned long *p_funcs)
{
    *p_funcs = onu_xvr_i2c_funcs;
    return;
}

void i2c_main(void)
{
	int mipc_fd;

	mipc_fd = mipc_init(i2c_mipc_name, 0, 0);
    if (MIPC_ERROR == mipc_fd)  
    {       
        return; 
    }  

    I2C_init();

    mthread_register_mq(mipc_fd, i2c_mipc_server_handler);
    mthread_start();
}


/******************************************************************************
 *
 * Function   : unpackA2dValues
 *              
 * Description: This function unpacks A/D values
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

static void unpackA2dValues(OnuXvrA2D_S *p_OnuXvrA2D, RegionDb_S *p_RegionDb, uint8_t *buf)
{
    uint16_t temp16;

    getN2HShort(&buf[0], &temp16);
    p_OnuXvrA2D->temperature = temp16;

    getN2HShort(&buf[2], &temp16);
    p_OnuXvrA2D->supplyVoltage = temp16;

    getN2HShort(&buf[4], &temp16);
    p_OnuXvrA2D->txBiasCurrent = temp16;

    getN2HShort(&buf[6], &temp16);
    p_OnuXvrA2D->txOpticalPower = temp16;

    getN2HShort(&buf[8], &temp16);
    p_OnuXvrA2D->rxReceivedPower = temp16;
}

/******************************************************************************
 *
 * Function   : localI2cApi_getOnuXvrA2dValues
 *              
 * Description: This function retrieves A/D values from XVR device
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

bool localI2cApi_getOnuXvrA2dValues(OnuXvrA2D_S *p_OnuXvrA2D)
{
    uint8_t       xvrBusAddress = OPERATIONAL_XVR_BUS_ADDRESS;
    int           numBytesRead;
    RegionDescr_S regionDescr[] = 
        { 
            {96, 10} 
        };
    RegionDb_S    regionDb =  {sizeof(regionDescr)/sizeof(regionDescr[0]), regionDescr};
    uint8_t       buf[100];

    memset(p_OnuXvrA2D, 0, sizeof(OnuXvrA2D_S));

    if (i2cHwRegionRead(xvrBusAddress, &regionDb, buf, sizeof(buf), &numBytesRead) == true)
    {
        unpackA2dValues(p_OnuXvrA2D, &regionDb, buf);
        return true;
    }

    return false;
}

/******************************************************************************
 *
 * Function   : unpackThresholds
 *              
 * Description: This function unpacks threshold values
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

static void unpackThresholds(OnuXvrThresholds_S *p_OnuXvrThresholds, RegionDb_S *p_RegionDb, uint8_t *buf)
{
    uint16_t temp16;

    getN2HShort(&buf[ 0], &temp16);   p_OnuXvrThresholds->temperature_hi_alarm       = temp16;
    getN2HShort(&buf[ 2], &temp16);   p_OnuXvrThresholds->temperature_lo_alarm       = temp16;
    getN2HShort(&buf[ 4], &temp16);   p_OnuXvrThresholds->temperature_hi_warning     = temp16;
    getN2HShort(&buf[ 6], &temp16);   p_OnuXvrThresholds->temperature_lo_warning     = temp16;
    getN2HShort(&buf[ 8], &temp16);   p_OnuXvrThresholds->supplyVoltage_hi_alarm     = temp16;
    getN2HShort(&buf[10], &temp16);   p_OnuXvrThresholds->supplyVoltage_lo_alarm     = temp16;
    getN2HShort(&buf[12], &temp16);   p_OnuXvrThresholds->supplyVoltage_hi_warning   = temp16;
    getN2HShort(&buf[14], &temp16);   p_OnuXvrThresholds->supplyVoltage_lo_warning   = temp16;
    getN2HShort(&buf[16], &temp16);   p_OnuXvrThresholds->txBiasCurrent_hi_alarm     = temp16;
    getN2HShort(&buf[18], &temp16);   p_OnuXvrThresholds->txBiasCurrent_lo_alarm     = temp16;
    getN2HShort(&buf[20], &temp16);   p_OnuXvrThresholds->txBiasCurrent_hi_warning   = temp16;
    getN2HShort(&buf[22], &temp16);   p_OnuXvrThresholds->txBiasCurrent_lo_warning   = temp16;
    getN2HShort(&buf[24], &temp16);   p_OnuXvrThresholds->txOpticalPower_hi_alarm    = temp16;
    getN2HShort(&buf[26], &temp16);   p_OnuXvrThresholds->txOpticalPower_lo_alarm    = temp16;
    getN2HShort(&buf[28], &temp16);   p_OnuXvrThresholds->txOpticalPower_hi_warning  = temp16;
    getN2HShort(&buf[30], &temp16);   p_OnuXvrThresholds->txOpticalPower_lo_warning  = temp16;
    getN2HShort(&buf[32], &temp16);   p_OnuXvrThresholds->rxReceivedPower_hi_alarm   = temp16;
    getN2HShort(&buf[34], &temp16);   p_OnuXvrThresholds->rxReceivedPower_lo_alarm   = temp16;
    getN2HShort(&buf[36], &temp16);   p_OnuXvrThresholds->rxReceivedPower_hi_warning = temp16;
    getN2HShort(&buf[38], &temp16);   p_OnuXvrThresholds->rxReceivedPower_lo_warning = temp16;
}

/******************************************************************************
 *
 * Function   : localI2cApi_getOnuXvrThresholds
 *              
 * Description: This function retrieves thresholds from XVR I2C device
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

bool localI2cApi_getOnuXvrThresholds(OnuXvrThresholds_S *p_OnuXvrThresholds)
{
    uint8_t       xvrBusAddress = OPERATIONAL_XVR_BUS_ADDRESS;
    int           numBytesRead;
    RegionDescr_S regionDescr[] = 
        { 
            {0, 40} 
        };
    RegionDb_S    regionDb =  {sizeof(regionDescr)/sizeof(regionDescr[0]), regionDescr};
    uint8_t       buf[100];

    memset(p_OnuXvrThresholds, 0, sizeof(OnuXvrThresholds_S));

    if (i2cHwRegionRead(xvrBusAddress, &regionDb, buf, sizeof(buf), &numBytesRead) == true)
    {
        unpackThresholds(p_OnuXvrThresholds, &regionDb, buf);
        return true;
    }

    return false;
}

/******************************************************************************
 *
 * Function   : packThresholds
 *              
 * Description: This function packs threshold values
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

static void packThresholds(OnuXvrThresholds_S *p_OnuXvrThresholds, RegionDb_S *p_RegionDb, uint8_t *buf)
{
    uint16_t temp16;

    temp16 = (uint16_t)p_OnuXvrThresholds->temperature_hi_alarm;    
    setShortH2N(&buf[ 0], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->temperature_lo_alarm;    
    setShortH2N(&buf[ 2], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->temperature_hi_warning;    
    setShortH2N(&buf[ 4], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->temperature_lo_warning;    
    setShortH2N(&buf[ 6], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->supplyVoltage_hi_alarm;    
    setShortH2N(&buf[ 8], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->supplyVoltage_lo_alarm;    
    setShortH2N(&buf[10], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->supplyVoltage_hi_warning;    
    setShortH2N(&buf[12], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->supplyVoltage_lo_warning;    
    setShortH2N(&buf[14], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->txBiasCurrent_hi_alarm;    
    setShortH2N(&buf[16], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->txBiasCurrent_lo_alarm;    
    setShortH2N(&buf[18], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->txBiasCurrent_hi_warning;    
    setShortH2N(&buf[20], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->txBiasCurrent_lo_warning;    
    setShortH2N(&buf[22], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->txOpticalPower_hi_alarm;    
    setShortH2N(&buf[24], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->txOpticalPower_lo_alarm;    
    setShortH2N(&buf[26], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->txOpticalPower_hi_warning;    
    setShortH2N(&buf[28], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->txOpticalPower_lo_warning;    
    setShortH2N(&buf[30], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->rxReceivedPower_hi_alarm;    
    setShortH2N(&buf[32], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->rxReceivedPower_lo_alarm;    
    setShortH2N(&buf[34], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->rxReceivedPower_hi_warning;
    setShortH2N(&buf[36], &temp16);
    temp16 = (uint16_t)p_OnuXvrThresholds->rxReceivedPower_lo_warning;    
    setShortH2N(&buf[38], &temp16);
}


/******************************************************************************
 *
 * Function   : localI2cApi_setOnuXvrThresholds
 *              
 * Description: This function set thresholds to XVR I2C device
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

bool localI2cApi_setOnuXvrThresholds(OnuXvrThresholds_S *p_OnuXvrThresholds)
{
    uint8_t       xvrBusAddress = OPERATIONAL_XVR_BUS_ADDRESS;
    int           numBytesWrite;
    //int i=0;
    RegionDescr_S regionDescr[] = 
        { 
            {0, 40} 
        };
    RegionDb_S    regionDb =  {sizeof(regionDescr)/sizeof(regionDescr[0]), regionDescr};
    uint8_t       buf[100];

    memset(buf, 0, sizeof(buf));
    packThresholds(p_OnuXvrThresholds, &regionDb, buf);

    #if 0
    for(i=0;i<40;i++)
    {
        printf("%d ",buf[i]);
    }
    printf("\n");
    #endif
    
    if (i2cHwRegionWrite(xvrBusAddress, &regionDb, buf, sizeof(buf), &numBytesWrite) == true)
    {        
        return true;
    }

    return false;
}

/******************************************************************************
 *
 * Function   : unpackAlarmsAndWarnings
 *              
 * Description: This function unpacks alarm and warning values
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

static void unpackAlarmsAndWarnings(OnuXvrAlarmsAndWarnings_S *p_OnuXvrAlarmsAndWarnings, RegionDb_S *p_RegionDb, uint8_t *buf)
{
    p_OnuXvrAlarmsAndWarnings->alarmFlags   = buf[0] | (buf[1] << 8);
    p_OnuXvrAlarmsAndWarnings->warningFlags = buf[2] | (buf[3] << 8);

}

/******************************************************************************
 *
 * Function   : localI2cApi_getOnuXvrAlarmsAndWarnings
 *              
 * Description: This function retrieves alarms and warnings from XVR I2C device
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

bool localI2cApi_getOnuXvrAlarmsAndWarnings(OnuXvrAlarmsAndWarnings_S *p_OnuXvrAlarmsAndWarnings)
{
    uint8_t       xvrBusAddress = OPERATIONAL_XVR_BUS_ADDRESS;
    int           numBytesRead;
    RegionDescr_S regionDescr[] = 
        { 
            {112, 2},
            {116, 2},
        };
    RegionDb_S    regionDb =  {sizeof(regionDescr)/sizeof(regionDescr[0]), regionDescr};
    uint8_t       buf[100];

    memset(p_OnuXvrAlarmsAndWarnings, 0, sizeof(OnuXvrAlarmsAndWarnings_S));

    //printf("%s: regionDb has %d entries\n", __FUNCTION__, regionDb.numEntries);

    if (i2cHwRegionRead(xvrBusAddress, &regionDb, buf, sizeof(buf), &numBytesRead) == true)
    {
        unpackAlarmsAndWarnings(p_OnuXvrAlarmsAndWarnings, &regionDb, buf);
        return true;
    }

    return false;
}

/******************************************************************************
 *
 * Function   : unpackInventory
 *              
 * Description: This function unpacks inventory
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

static void unpackInventory(OnuXvrInventory_S *p_OnuXvrInventory, RegionDb_S *p_RegionDb, uint8_t *buf)
{
    int            regIndx;
    RegionDescr_S  *p_RegDescr;
    int            extractIndx = 0;

    for (regIndx = 0; regIndx < p_RegionDb->numEntries; regIndx++)
    {
        p_RegDescr = &p_RegionDb->regionDescrAra[regIndx]; 

        switch (regIndx)
        {
        case 0:
            memcpy(p_OnuXvrInventory->vendorName, &buf[extractIndx], p_RegDescr->length);
            break;

        case 1:
            memcpy(p_OnuXvrInventory->oui, &buf[extractIndx], p_RegDescr->length);
            break;

        case 2:
            memcpy(p_OnuXvrInventory->partName, &buf[extractIndx], p_RegDescr->length);
            break;

        case 3:
            memcpy(p_OnuXvrInventory->revision, &buf[extractIndx], p_RegDescr->length);
            break;

        case 4:
            memcpy(p_OnuXvrInventory->serialNumber, &buf[extractIndx], p_RegDescr->length);
            break;

        case 5:
            memcpy(p_OnuXvrInventory->dateCode, &buf[extractIndx], p_RegDescr->length);
            break;
        }
        extractIndx += p_RegDescr->length;
    }
}

/******************************************************************************
 *
 * Function   : localI2cApi_getOnuXvrInventory
 *              
 * Description: This function retrieves inventory data from XVR I2C device
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

bool localI2cApi_getOnuXvrInventory(OnuXvrInventory_S *p_OnuXvrInventory)
{
    uint8_t       xvrBusAddress = INVENTORY_XVR_BUS_ADDRESS;
    int           numBytesRead;
    RegionDescr_S regionDescr[] = 
        { 
            {20, VENDORNAMELENGTH},
            {37, OUILENGTH},
            {40, PARTNAMELENGTH},
            {56, REVISIONLENGTH},
            {68, SERIALNUMBERLENGTH},
            {84, DATECODELENGTH},
        };
    RegionDb_S    regionDb =  {sizeof(regionDescr)/sizeof(regionDescr[0]), regionDescr};
    uint8_t       buf[100];

    memset(p_OnuXvrInventory, 0, sizeof(OnuXvrInventory_S));

    if (i2cHwRegionRead(xvrBusAddress, &regionDb, buf, sizeof(buf), &numBytesRead) == true)
    {
        unpackInventory(p_OnuXvrInventory, &regionDb, buf);
        return true;
    }

    return false;
}



//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/******************************************************************************
 *
 * Function   : showXvrA2dValues
 *              
 * Description: This function displays optical XVR A/D values
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

void showXvrA2dValues(char* name)
{
    OnuXvrA2D_S onuXvrA2D;
    OnuXvrA2D_S *p_OnuXvrA2D = &onuXvrA2D;

    if (localI2cApi_getOnuXvrA2dValues(p_OnuXvrA2D) == true)
    {
        I2C_CLI_PRINT("Temperature       : %d\n", (signed short)p_OnuXvrA2D->temperature);      // Still need to took at high byte low byte, sign etc
        I2C_CLI_PRINT("Supply voltage    : %d\n", p_OnuXvrA2D->supplyVoltage);
        I2C_CLI_PRINT("Tx bias current   : %d\n", p_OnuXvrA2D->txBiasCurrent);
        I2C_CLI_PRINT("Tx optical power  : %d\n", p_OnuXvrA2D->txOpticalPower);
        I2C_CLI_PRINT("Rx received power : %d\n", p_OnuXvrA2D->rxReceivedPower);
    }
}

/******************************************************************************
 *
 * Function   : showXvrThresholds
 *              
 * Description: This function displays optical XVR thresholds
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

void showXvrThresholds(char* name)
{
    OnuXvrThresholds_S onuXvrThresholds;
    OnuXvrThresholds_S *p_OnuXvrThresholds = &onuXvrThresholds;

    if (localI2cApi_getOnuXvrThresholds(p_OnuXvrThresholds) == true)
    {
        I2C_CLI_PRINT("HW Temperature       high alarm   : %d\n", p_OnuXvrThresholds->temperature_hi_alarm);
        I2C_CLI_PRINT("HW Temperature       low  alarm   : %d\n", p_OnuXvrThresholds->temperature_lo_alarm);
        I2C_CLI_PRINT("HW Temperature       high warning : %d\n", p_OnuXvrThresholds->temperature_hi_warning);
        I2C_CLI_PRINT("HW Temperature       low  warning : %d\n", p_OnuXvrThresholds->temperature_lo_warning);

        I2C_CLI_PRINT("HW Supply voltage    high alarm   : %d\n", p_OnuXvrThresholds->supplyVoltage_hi_alarm);
        I2C_CLI_PRINT("HW Supply voltage    low  alarm   : %d\n", p_OnuXvrThresholds->supplyVoltage_lo_alarm);
        I2C_CLI_PRINT("HW Supply voltage    high warning : %d\n", p_OnuXvrThresholds->supplyVoltage_hi_warning);
        I2C_CLI_PRINT("HW Supply voltage    low  warning : %d\n", p_OnuXvrThresholds->supplyVoltage_lo_warning);

        I2C_CLI_PRINT("HW Tx bias current   high alarm   : %d\n", p_OnuXvrThresholds->txBiasCurrent_hi_alarm);
        I2C_CLI_PRINT("HW Tx bias current   low  alarm   : %d\n", p_OnuXvrThresholds->txBiasCurrent_lo_alarm);
        I2C_CLI_PRINT("HW Tx bias current   high warning : %d\n", p_OnuXvrThresholds->txBiasCurrent_hi_warning);
        I2C_CLI_PRINT("HW Tx bias current   low  warning : %d\n", p_OnuXvrThresholds->txBiasCurrent_lo_warning);

        I2C_CLI_PRINT("HW Tx optical power  high alarm   : %d\n", p_OnuXvrThresholds->txOpticalPower_hi_alarm);
        I2C_CLI_PRINT("HW Tx optical power  low  alarm   : %d\n", p_OnuXvrThresholds->txOpticalPower_lo_alarm);
        I2C_CLI_PRINT("HW Tx optical power  high warning : %d\n", p_OnuXvrThresholds->txOpticalPower_hi_warning);
        I2C_CLI_PRINT("HW Tx optical power  low  warning : %d\n", p_OnuXvrThresholds->txOpticalPower_lo_warning);

        I2C_CLI_PRINT("HW Rx received power high alarm   : %d\n", p_OnuXvrThresholds->rxReceivedPower_hi_alarm);
        I2C_CLI_PRINT("HW Rx received power low  alarm   : %d\n", p_OnuXvrThresholds->rxReceivedPower_lo_alarm);
        I2C_CLI_PRINT("HW Rx received power high warning : %d\n", p_OnuXvrThresholds->rxReceivedPower_hi_warning);
        I2C_CLI_PRINT("HW Rx received power low  warning : %d\n", p_OnuXvrThresholds->rxReceivedPower_lo_warning);
    }

        I2C_CLI_PRINT("APM Temperature       high alarm   : %d\n", (signed short)onu_xvr_thresholds.temperature_hi_alarm);
        I2C_CLI_PRINT("APM Temperature       low  alarm   : %d\n", (signed short)onu_xvr_thresholds.temperature_lo_alarm);
        I2C_CLI_PRINT("APM Temperature       high warning : %d\n", (signed short)onu_xvr_thresholds.temperature_hi_warning);
        I2C_CLI_PRINT("APM Temperature       low  warning : %d\n", (signed short)onu_xvr_thresholds.temperature_lo_warning);

        I2C_CLI_PRINT("APM Supply voltage    high alarm   : %d\n", onu_xvr_thresholds.supplyVoltage_hi_alarm);
        I2C_CLI_PRINT("APM Supply voltage    low  alarm   : %d\n", onu_xvr_thresholds.supplyVoltage_lo_alarm);
        I2C_CLI_PRINT("APM Supply voltage    high warning : %d\n", onu_xvr_thresholds.supplyVoltage_hi_warning);
        I2C_CLI_PRINT("APM Supply voltage    low  warning : %d\n", onu_xvr_thresholds.supplyVoltage_lo_warning);

        I2C_CLI_PRINT("APM Tx bias current   high alarm   : %d\n", onu_xvr_thresholds.txBiasCurrent_hi_alarm);
        I2C_CLI_PRINT("APM Tx bias current   low  alarm   : %d\n", onu_xvr_thresholds.txBiasCurrent_lo_alarm);
        I2C_CLI_PRINT("APM Tx bias current   high warning : %d\n", onu_xvr_thresholds.txBiasCurrent_hi_warning);
        I2C_CLI_PRINT("APM Tx bias current   low  warning : %d\n", onu_xvr_thresholds.txBiasCurrent_lo_warning);

        I2C_CLI_PRINT("APM Tx optical power  high alarm   : %d\n", onu_xvr_thresholds.txOpticalPower_hi_alarm);
        I2C_CLI_PRINT("APM Tx optical power  low  alarm   : %d\n", onu_xvr_thresholds.txOpticalPower_lo_alarm);
        I2C_CLI_PRINT("APM Tx optical power  high warning : %d\n", onu_xvr_thresholds.txOpticalPower_hi_warning);
        I2C_CLI_PRINT("APM Tx optical power  low  warning : %d\n", onu_xvr_thresholds.txOpticalPower_lo_warning);

        I2C_CLI_PRINT("APM Rx received power high alarm   : %d\n", onu_xvr_thresholds.rxReceivedPower_hi_alarm);
        I2C_CLI_PRINT("APM Rx received power low  alarm   : %d\n", onu_xvr_thresholds.rxReceivedPower_lo_alarm);
        I2C_CLI_PRINT("APM Rx received power high warning : %d\n", onu_xvr_thresholds.rxReceivedPower_hi_warning);
        I2C_CLI_PRINT("APM Rx received power low  warning : %d\n", onu_xvr_thresholds.rxReceivedPower_lo_warning);

        I2C_CLI_PRINT("APM Temperature       high alarm   clear threshold: %d\n", (signed short)onu_xvr_clear_thresholds.temperature_hi_alarm);
        I2C_CLI_PRINT("APM Temperature       low  alarm   clear threshold: %d\n", (signed short)onu_xvr_clear_thresholds.temperature_lo_alarm);
        I2C_CLI_PRINT("APM Temperature       high warning clear threshold: %d\n", (signed short)onu_xvr_clear_thresholds.temperature_hi_warning);
        I2C_CLI_PRINT("APM Temperature       low  warning clear threshold: %d\n", (signed short)onu_xvr_clear_thresholds.temperature_lo_warning);

        I2C_CLI_PRINT("APM Supply voltage    high alarm   clear threshold: %d\n", onu_xvr_clear_thresholds.supplyVoltage_hi_alarm);
        I2C_CLI_PRINT("APM Supply voltage    low  alarm   clear threshold: %d\n", onu_xvr_clear_thresholds.supplyVoltage_lo_alarm);
        I2C_CLI_PRINT("APM Supply voltage    high warning clear threshold: %d\n", onu_xvr_clear_thresholds.supplyVoltage_hi_warning);
        I2C_CLI_PRINT("APM Supply voltage    low  warning clear threshold: %d\n", onu_xvr_clear_thresholds.supplyVoltage_lo_warning);

        I2C_CLI_PRINT("APM Tx bias current   high alarm   clear threshold: %d\n", onu_xvr_clear_thresholds.txBiasCurrent_hi_alarm);
        I2C_CLI_PRINT("APM Tx bias current   low  alarm   clear threshold: %d\n", onu_xvr_clear_thresholds.txBiasCurrent_lo_alarm);
        I2C_CLI_PRINT("APM Tx bias current   high warning clear threshold: %d\n", onu_xvr_clear_thresholds.txBiasCurrent_hi_warning);
        I2C_CLI_PRINT("APM Tx bias current   low  warning clear threshold: %d\n", onu_xvr_clear_thresholds.txBiasCurrent_lo_warning);

        I2C_CLI_PRINT("APM Tx optical power  high alarm   clear threshold: %d\n", onu_xvr_clear_thresholds.txOpticalPower_hi_alarm);
        I2C_CLI_PRINT("APM Tx optical power  low  alarm   clear threshold: %d\n", onu_xvr_clear_thresholds.txOpticalPower_lo_alarm);
        I2C_CLI_PRINT("APM Tx optical power  high warning clear threshold: %d\n", onu_xvr_clear_thresholds.txOpticalPower_hi_warning);
        I2C_CLI_PRINT("APM Tx optical power  low  warning clear threshold: %d\n", onu_xvr_clear_thresholds.txOpticalPower_lo_warning);

        I2C_CLI_PRINT("APM Rx received power high alarm   clear threshold: %d\n", onu_xvr_clear_thresholds.rxReceivedPower_hi_alarm);
        I2C_CLI_PRINT("APM Rx received power low  alarm   clear threshold: %d\n", onu_xvr_clear_thresholds.rxReceivedPower_lo_alarm);
        I2C_CLI_PRINT("APM Rx received power high warning clear threshold: %d\n", onu_xvr_clear_thresholds.rxReceivedPower_hi_warning);
        I2C_CLI_PRINT("APM Rx received power low  warning clear threshold: %d\n", onu_xvr_clear_thresholds.rxReceivedPower_lo_warning);
}

/******************************************************************************
 *
 * Function   : 
 *              
 * Description: This function displays optical XVR alarms And warnings
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

void showXvrAlarmsAndWarnings(char* name)
{
    OnuXvrAlarmsAndWarnings_S onuXvrAlarmsAndWarnings;
    OnuXvrAlarmsAndWarnings_S *p_OnuXvrAlarmsAndWarnings = &onuXvrAlarmsAndWarnings;

    if (localI2cApi_getOnuXvrAlarmsAndWarnings(p_OnuXvrAlarmsAndWarnings) == true)
    {
        I2C_CLI_PRINT("Alarm   flags   : 0x%04x\n", p_OnuXvrAlarmsAndWarnings->alarmFlags);
        I2C_CLI_PRINT("Warning flags   : 0x%04x\n", p_OnuXvrAlarmsAndWarnings->warningFlags);
    }
}

/******************************************************************************
 *
 * Function   : 
 *              
 * Description: This function displays optical XVR inventory
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

void showXvrInventory(char* name)
{
    OnuXvrInventory_S onuXvrInventory;
    OnuXvrInventory_S *p_OnuXvrInventory = &onuXvrInventory;

    memset(p_OnuXvrInventory, 0, sizeof(OnuXvrInventory_S));

    if (localI2cApi_getOnuXvrInventory(p_OnuXvrInventory) == true)
    {
        I2C_CLI_PRINT("Vendor name   : %s\n", p_OnuXvrInventory->vendorName);
        I2C_CLI_PRINT("OUI           : %02x %02x %02x\n", p_OnuXvrInventory->oui[0], p_OnuXvrInventory->oui[1], p_OnuXvrInventory->oui[2]);
        I2C_CLI_PRINT("Part   name   : %s\n", p_OnuXvrInventory->partName);
        I2C_CLI_PRINT("Revision      : %s\n", p_OnuXvrInventory->revision);
        I2C_CLI_PRINT("Serial number : %s\n", p_OnuXvrInventory->serialNumber);
        I2C_CLI_PRINT("Date code     : %s\n", p_OnuXvrInventory->dateCode);
    }
}

void showXvrCapability(char* name)
{

    if(SUPPORT_READ_BYTE==is_support_onu_xvr_I2c)
    {
        I2C_CLI_PRINT("XVCR support I2C byte-read method.\n");
    }
    else if(SUPPORT_READ_BUFF==is_support_onu_xvr_I2c)
    {
        I2C_CLI_PRINT("XVCR support I2C buff-read method.\n");
    }
    else if(SUPPORT_WRITE_QUICK==is_support_onu_xvr_I2c)
    {
        I2C_CLI_PRINT("XVCR support I2C write quick method.\n");
    }
    else
    {
        I2C_CLI_PRINT("XVCR does not support I2C method.\n");
    }
}

/******************************************************************************
 *
 * Function   : I2cApi_apmSetOnuXvrThreshold
 *              
 * Description: This function set i2c threshold for apm alarm type
 *              because localI2cApi_setOnuXvrThresholds does not take effective, so use this function
 *              
 * Parameters : 
 *  alarm_type(in)      -   apm alarm type
 *  threshold(in)       -   threshold
 *  clear_threshold(in) -   clear threshold
 *
 * Returns    : bool
 *              
 ******************************************************************************/

bool I2cApi_apmSetOnuXvrThreshold(UINT32 alarm_type, UINT32 threshold, UINT32 clear_threshold)
{
    switch(alarm_type)
    {
        case ALARM_PON_RX_POWER_HIGH_ALARM:
            onu_xvr_thresholds.rxReceivedPower_hi_alarm = threshold;
            onu_xvr_clear_thresholds.rxReceivedPower_hi_alarm = clear_threshold;            
            return true;
        case ALARM_PON_RX_POWER_LOW_ALARM:
            onu_xvr_thresholds.rxReceivedPower_lo_alarm = threshold;
            onu_xvr_clear_thresholds.rxReceivedPower_lo_alarm = clear_threshold; 
            return true;
        case ALARM_PON_TX_POWER_HIGH_ALARM:
            onu_xvr_thresholds.txOpticalPower_hi_alarm = threshold;
            onu_xvr_clear_thresholds.txOpticalPower_hi_alarm = clear_threshold; 
            return true;
        case ALARM_PON_TX_POWER_LOW_ALARM:
            onu_xvr_thresholds.txOpticalPower_lo_alarm = threshold;
            onu_xvr_clear_thresholds.txOpticalPower_lo_alarm = clear_threshold; 
            return true;
        case ALARM_PON_TX_BIAS_HIGH_ALARM:
            onu_xvr_thresholds.txBiasCurrent_hi_alarm = threshold;
            onu_xvr_clear_thresholds.txBiasCurrent_hi_alarm = clear_threshold; 
            return true;
        case ALARM_PON_TX_BIAS_LOW_ALARM:
            onu_xvr_thresholds.txBiasCurrent_lo_alarm = threshold;
            onu_xvr_clear_thresholds.txBiasCurrent_lo_alarm = clear_threshold; 
            return true;
        case ALARM_PON_VCC_HIGH_ALARM:
            onu_xvr_thresholds.supplyVoltage_hi_alarm = threshold;
            onu_xvr_clear_thresholds.supplyVoltage_hi_alarm = clear_threshold; 
            return true;
        case ALARM_PON_VCC_LOW_ALARM:
            onu_xvr_thresholds.supplyVoltage_lo_alarm = threshold;
            onu_xvr_clear_thresholds.supplyVoltage_lo_alarm = clear_threshold; 
            return true;
        case ALARM_PON_TEMP_HIGH_ALARM:
            onu_xvr_thresholds.temperature_hi_alarm = threshold;
            onu_xvr_clear_thresholds.temperature_hi_alarm = clear_threshold;
            
            if(I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH == threshold)
            {
                onu_xvr_thresholds.temperature_hi_alarm = I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH;
            }
            if(I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH == clear_threshold)
            {
                onu_xvr_clear_thresholds.temperature_hi_alarm = I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH;
            }
            
            return true;
        case ALARM_PON_TEMP_LOW_ALARM:
            onu_xvr_thresholds.temperature_lo_alarm = threshold;
            onu_xvr_clear_thresholds.temperature_lo_alarm = clear_threshold; 

            if(I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW== threshold)
            {
                onu_xvr_thresholds.temperature_lo_alarm = I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW;
            }
            if(I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW == clear_threshold)
            {
                onu_xvr_clear_thresholds.temperature_lo_alarm = I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW;
            }
            
            return true;
        case ALARM_PON_RX_POWER_HIGH_WARNING:
            onu_xvr_thresholds.rxReceivedPower_hi_warning = threshold;
            onu_xvr_clear_thresholds.rxReceivedPower_hi_warning = clear_threshold; 
            return true;
        case ALARM_PON_RX_POWER_LOW_WARNING:
            onu_xvr_thresholds.rxReceivedPower_lo_warning= threshold;
            onu_xvr_clear_thresholds.rxReceivedPower_lo_warning = clear_threshold; 
            return true;
        case ALARM_PON_TX_POWER_HIGH_WARNING:
            onu_xvr_thresholds.txOpticalPower_hi_warning= threshold;
            onu_xvr_clear_thresholds.txOpticalPower_hi_warning = clear_threshold; 
            return true;
        case ALARM_PON_TX_POWER_LOW_WARNING:
            onu_xvr_thresholds.txOpticalPower_lo_warning = threshold;
            onu_xvr_clear_thresholds.txOpticalPower_lo_warning = clear_threshold; 
            return true;
        case ALARM_PON_TX_BIAS_HIGH_WARNING:
            onu_xvr_thresholds.txBiasCurrent_hi_warning = threshold;
            onu_xvr_clear_thresholds.txBiasCurrent_hi_warning = clear_threshold; 
            return true;
        case ALARM_PON_TX_BIAS_LOW_WARNING:
            onu_xvr_thresholds.txBiasCurrent_lo_warning = threshold;
            onu_xvr_clear_thresholds.txBiasCurrent_lo_warning = clear_threshold; 
            return true;
        case ALARM_PON_VCC_HIGH_WARNING:
            onu_xvr_thresholds.supplyVoltage_hi_warning = threshold;
            onu_xvr_clear_thresholds.supplyVoltage_hi_warning = clear_threshold; 
            return true;
        case ALARM_PON_VCC_LOW_WARNING:
            onu_xvr_thresholds.supplyVoltage_lo_warning = threshold;
            onu_xvr_clear_thresholds.supplyVoltage_lo_warning = clear_threshold; 
            return true;
        case ALARM_PON_TEMP_HIGH_WARNING:
            onu_xvr_thresholds.temperature_hi_warning = threshold;
            onu_xvr_clear_thresholds.temperature_hi_warning = clear_threshold;

            if(I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH == threshold)
            {
                onu_xvr_thresholds.temperature_hi_warning = I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH;
            }
            if(I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH == clear_threshold)
            {
                onu_xvr_clear_thresholds.temperature_hi_warning = I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH;
            }
            
            return true;
        case ALARM_PON_TEMP_LOW_WARNING:
            onu_xvr_thresholds.temperature_lo_warning = threshold;
            onu_xvr_clear_thresholds.temperature_lo_warning = clear_threshold; 

            if(I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW== threshold)
            {
                onu_xvr_thresholds.temperature_lo_warning = I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW;
            }
            if(I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW == clear_threshold)
            {
                onu_xvr_clear_thresholds.temperature_lo_warning = I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW;
            }
            
            return true;        
        default:
            return false;
    }
}

/******************************************************************************
 *
 * Function   : I2cApi_apmGetOnuXvrStateAndValue
 *              
 * Description: This function set i2c threshold for apm alarm type
 *              because localI2cApi_setOnuXvrThresholds does not take effective, so use this function
 *              
 * Parameters : 
 *  alarm_type(in)      -   apm alarm type
 *  p_state(out)        -   alarm state
 *  P_value(out)        -   i2c parameter
 *
 * Returns    : bool
 *              
 ******************************************************************************/

bool I2cApi_apmGetOnuXvrStateAndValue(UINT32 alarm_type, UINT8 *p_state, UINT32 *P_value)
{
    unsigned int cur_time;

    cur_time = osSecondsGet();
    if(cur_time - i2c_get_xvr_values_time >= I2C_GET_ONU_XVR_VALUES_INTERVAL)
    {
        localI2cApi_getOnuXvrA2dValues(&onu_xvr_a2d_values);
        i2c_get_xvr_values_time = cur_time;
    }

    switch(alarm_type)
    {
        case ALARM_PON_RX_POWER_HIGH_ALARM:
            *P_value = onu_xvr_a2d_values.rxReceivedPower;
            if(onu_xvr_thresholds.rxReceivedPower_hi_alarm != 0 && onu_xvr_a2d_values.rxReceivedPower > onu_xvr_thresholds.rxReceivedPower_hi_alarm)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.rxReceivedPower_hi_alarm != 0 && onu_xvr_a2d_values.rxReceivedPower <= onu_xvr_clear_thresholds.rxReceivedPower_hi_alarm)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_RX_POWER_LOW_ALARM:
            *P_value = onu_xvr_a2d_values.rxReceivedPower;
            if(onu_xvr_thresholds.rxReceivedPower_lo_alarm != 0 && onu_xvr_a2d_values.rxReceivedPower < onu_xvr_thresholds.rxReceivedPower_lo_alarm)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.rxReceivedPower_lo_alarm != 0 && onu_xvr_a2d_values.rxReceivedPower >= onu_xvr_clear_thresholds.rxReceivedPower_lo_alarm)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TX_POWER_HIGH_ALARM:
            *P_value = onu_xvr_a2d_values.txOpticalPower;
            if(onu_xvr_thresholds.txOpticalPower_hi_alarm != 0 && onu_xvr_a2d_values.txOpticalPower > onu_xvr_thresholds.txOpticalPower_hi_alarm)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.txOpticalPower_hi_alarm != 0 && onu_xvr_a2d_values.txOpticalPower <= onu_xvr_clear_thresholds.txOpticalPower_hi_alarm)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TX_POWER_LOW_ALARM:
            *P_value = onu_xvr_a2d_values.txOpticalPower;
            if(onu_xvr_thresholds.txOpticalPower_lo_alarm != 0 && onu_xvr_a2d_values.txOpticalPower < onu_xvr_thresholds.txOpticalPower_lo_alarm)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.txOpticalPower_lo_alarm != 0 && onu_xvr_a2d_values.txOpticalPower >= onu_xvr_clear_thresholds.txOpticalPower_lo_alarm)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TX_BIAS_HIGH_ALARM:
            *P_value = onu_xvr_a2d_values.txBiasCurrent;
            if(onu_xvr_thresholds.txBiasCurrent_hi_alarm != 0 && onu_xvr_a2d_values.txBiasCurrent > onu_xvr_thresholds.txBiasCurrent_hi_alarm)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.txBiasCurrent_hi_alarm != 0 && onu_xvr_a2d_values.txBiasCurrent <= onu_xvr_clear_thresholds.txBiasCurrent_hi_alarm)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TX_BIAS_LOW_ALARM:
            *P_value = onu_xvr_a2d_values.txBiasCurrent;
            if(onu_xvr_thresholds.txBiasCurrent_lo_alarm != 0 && onu_xvr_a2d_values.txBiasCurrent < onu_xvr_thresholds.txBiasCurrent_lo_alarm)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.txBiasCurrent_lo_alarm != 0 && onu_xvr_a2d_values.txBiasCurrent >= onu_xvr_clear_thresholds.txBiasCurrent_lo_alarm)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_VCC_HIGH_ALARM:
            *P_value = onu_xvr_a2d_values.supplyVoltage;
            if(onu_xvr_thresholds.supplyVoltage_hi_alarm != 0 && onu_xvr_a2d_values.supplyVoltage > onu_xvr_thresholds.supplyVoltage_hi_alarm)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.supplyVoltage_hi_alarm != 0 && onu_xvr_a2d_values.supplyVoltage <= onu_xvr_clear_thresholds.supplyVoltage_hi_alarm)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_VCC_LOW_ALARM:
            *P_value = onu_xvr_a2d_values.supplyVoltage;
            if(onu_xvr_thresholds.supplyVoltage_lo_alarm != 0 && onu_xvr_a2d_values.supplyVoltage < onu_xvr_thresholds.supplyVoltage_lo_alarm)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.supplyVoltage_lo_alarm != 0 && onu_xvr_a2d_values.supplyVoltage >= onu_xvr_clear_thresholds.supplyVoltage_lo_alarm)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TEMP_HIGH_ALARM:
            *P_value = onu_xvr_a2d_values.temperature;
            if(onu_xvr_thresholds.temperature_hi_alarm != I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH && (signed short)onu_xvr_a2d_values.temperature > (signed short)onu_xvr_thresholds.temperature_hi_alarm)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.temperature_hi_alarm != I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH && (signed short)onu_xvr_a2d_values.temperature <= (signed short)onu_xvr_clear_thresholds.temperature_hi_alarm)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TEMP_LOW_ALARM:
            *P_value = onu_xvr_a2d_values.temperature;
            if(onu_xvr_thresholds.temperature_lo_alarm != I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW && (signed short)onu_xvr_a2d_values.temperature < (signed short)onu_xvr_thresholds.temperature_lo_alarm)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.temperature_lo_alarm != I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW && (signed short)onu_xvr_a2d_values.temperature >= (signed short)onu_xvr_clear_thresholds.temperature_lo_alarm)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;            
        case ALARM_PON_RX_POWER_HIGH_WARNING:
            *P_value = onu_xvr_a2d_values.rxReceivedPower;
            if(onu_xvr_thresholds.rxReceivedPower_hi_warning != 0 && onu_xvr_a2d_values.rxReceivedPower > onu_xvr_thresholds.rxReceivedPower_hi_warning)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.rxReceivedPower_hi_warning != 0 && onu_xvr_a2d_values.rxReceivedPower <= onu_xvr_clear_thresholds.rxReceivedPower_hi_warning)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_RX_POWER_LOW_WARNING:
            *P_value = onu_xvr_a2d_values.rxReceivedPower;
            if(onu_xvr_thresholds.rxReceivedPower_lo_warning != 0 && onu_xvr_a2d_values.rxReceivedPower < onu_xvr_thresholds.rxReceivedPower_lo_warning)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.rxReceivedPower_lo_warning != 0 && onu_xvr_a2d_values.rxReceivedPower >= onu_xvr_clear_thresholds.rxReceivedPower_lo_warning)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TX_POWER_HIGH_WARNING:
            *P_value = onu_xvr_a2d_values.txOpticalPower;
            if(onu_xvr_thresholds.txOpticalPower_hi_warning != 0 && onu_xvr_a2d_values.txOpticalPower > onu_xvr_thresholds.txOpticalPower_hi_warning)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.txOpticalPower_hi_warning != 0 && onu_xvr_a2d_values.txOpticalPower <= onu_xvr_clear_thresholds.txOpticalPower_hi_warning)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TX_POWER_LOW_WARNING:
            *P_value = onu_xvr_a2d_values.txOpticalPower;
            if(onu_xvr_thresholds.txOpticalPower_lo_warning != 0 && onu_xvr_a2d_values.txOpticalPower < onu_xvr_thresholds.txOpticalPower_lo_warning)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.txOpticalPower_lo_warning != 0 && onu_xvr_a2d_values.txOpticalPower >= onu_xvr_clear_thresholds.txOpticalPower_lo_warning)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TX_BIAS_HIGH_WARNING:
            *P_value = onu_xvr_a2d_values.txBiasCurrent;
            if(onu_xvr_thresholds.txBiasCurrent_hi_warning != 0 && onu_xvr_a2d_values.txBiasCurrent > onu_xvr_thresholds.txBiasCurrent_hi_warning)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.txBiasCurrent_hi_warning != 0 && onu_xvr_a2d_values.txBiasCurrent <= onu_xvr_clear_thresholds.txBiasCurrent_hi_warning)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TX_BIAS_LOW_WARNING:
            *P_value = onu_xvr_a2d_values.txBiasCurrent;
            if(onu_xvr_thresholds.txBiasCurrent_lo_warning != 0 && onu_xvr_a2d_values.txBiasCurrent < onu_xvr_thresholds.txBiasCurrent_lo_warning)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.txBiasCurrent_lo_warning != 0 && onu_xvr_a2d_values.txBiasCurrent >= onu_xvr_clear_thresholds.txBiasCurrent_lo_warning)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_VCC_HIGH_WARNING:
            *P_value = onu_xvr_a2d_values.supplyVoltage;
            if(onu_xvr_thresholds.supplyVoltage_hi_warning != 0 && onu_xvr_a2d_values.supplyVoltage > onu_xvr_thresholds.supplyVoltage_hi_warning)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.supplyVoltage_hi_warning != 0 && onu_xvr_a2d_values.supplyVoltage <= onu_xvr_clear_thresholds.supplyVoltage_hi_warning)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_VCC_LOW_WARNING:
            *P_value = onu_xvr_a2d_values.supplyVoltage;
            if(onu_xvr_thresholds.supplyVoltage_lo_warning != 0 && onu_xvr_a2d_values.supplyVoltage < onu_xvr_thresholds.supplyVoltage_lo_warning)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.supplyVoltage_lo_warning != 0 && onu_xvr_a2d_values.supplyVoltage >= onu_xvr_clear_thresholds.supplyVoltage_lo_warning)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TEMP_HIGH_WARNING:
            *P_value = onu_xvr_a2d_values.temperature;
            if(onu_xvr_thresholds.temperature_hi_warning != I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH && (signed short)onu_xvr_a2d_values.temperature > (signed short)onu_xvr_thresholds.temperature_hi_warning)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.temperature_hi_warning != I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH && (signed short)onu_xvr_a2d_values.temperature <= (signed short)onu_xvr_clear_thresholds.temperature_hi_warning)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;
        case ALARM_PON_TEMP_LOW_WARNING:
            *P_value = onu_xvr_a2d_values.temperature;
            if(onu_xvr_thresholds.temperature_lo_warning != I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW && (signed short)onu_xvr_a2d_values.temperature < (signed short)onu_xvr_thresholds.temperature_lo_warning)
            {
                *p_state = APM_STATE_ON;                
            }
            else if(onu_xvr_clear_thresholds.temperature_lo_warning != I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW && (signed short)onu_xvr_a2d_values.temperature >= (signed short)onu_xvr_clear_thresholds.temperature_lo_warning)
            {
                *p_state = APM_STATE_OFF;                
            }
            return true;

        default:
            return false;
    }
}


