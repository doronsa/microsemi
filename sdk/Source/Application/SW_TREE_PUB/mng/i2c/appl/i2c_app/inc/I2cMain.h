/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : I2C                                                       **/
/**                                                                          **/
/**  FILE        : I2cMain.h                                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file contains definitions and prototypes for I2c top **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCI2cMainh
#define __INCI2cMainh

#define I2C_CLI_PRINT(...) mipc_printf(name, ##__VA_ARGS__)
#define I2C_GET_ONU_XVR_VALUES_INTERVAL     (3)


struct OnuXvrA2D_S {
	uint32_t temperature;
	uint32_t supplyVoltage;
	uint32_t txBiasCurrent;
	uint32_t txOpticalPower;
	uint32_t rxReceivedPower;
};
typedef struct OnuXvrA2D_S OnuXvrA2D_S;

struct OnuXvrThresholds_S {
	uint32_t temperature_hi_alarm;
	uint32_t temperature_lo_alarm;
	uint32_t temperature_hi_warning;
	uint32_t temperature_lo_warning;
	uint32_t supplyVoltage_hi_alarm;
	uint32_t supplyVoltage_lo_alarm;
	uint32_t supplyVoltage_hi_warning;
	uint32_t supplyVoltage_lo_warning;
	uint32_t txBiasCurrent_hi_alarm;
	uint32_t txBiasCurrent_lo_alarm;
	uint32_t txBiasCurrent_hi_warning;
	uint32_t txBiasCurrent_lo_warning;
	uint32_t txOpticalPower_hi_alarm;
	uint32_t txOpticalPower_lo_alarm;
	uint32_t txOpticalPower_hi_warning;
	uint32_t txOpticalPower_lo_warning;
	uint32_t rxReceivedPower_hi_alarm;
	uint32_t rxReceivedPower_lo_alarm;
	uint32_t rxReceivedPower_hi_warning;
	uint32_t rxReceivedPower_lo_warning;
};
typedef struct OnuXvrThresholds_S OnuXvrThresholds_S;

#define BM_TEMPERATURE_HI_ALARM          (0x80)
#define BM_TEMPERATURE_LO_ALARM          (0x40)
#define BM_SUPPLYVOLTAGE_HI_ALARM        (0x20)
#define BM_SUPPLYVOLTAGE_LO_ALARM        (0x10)
#define BM_TXBIASCURRENT_HI_ALARM        (0x8)
#define BM_TXBIASCURRENT_LO_ALARM        (0x4)
#define BM_TXOPTICALPOWER_HI_ALARM       (0x2)
#define BM_TXOPTICALPOWER_LO_ALARM       (0x1)
#define BM_RXRECEIVEDPOWER_HI_ALARM      (0x8000)
#define BM_RXRECEIVEDPOWER_LO_ALARM      (0x4000)

#define BM_TEMPERATURE_HI_WARNING        (0x80)
#define BM_TEMPERATURE_LO_WARNING        (0x40)
#define BM_SUPPLYVOLTAGE_HI_WARNING      (0x20)
#define BM_SUPPLYVOLTAGE_LO_WARNING      (0x10)
#define BM_TXBIASCURRENT_HI_WARNING      (0x8)
#define BM_TXBIASCURRENT_LO_WARNING      (0x4)
#define BM_TXOPTICALPOWER_HI_WARNING     (0x2)
#define BM_TXOPTICALPOWER_LO_WARNING     (0x1)
#define BM_RXRECEIVEDPOWER_HI_WARNING    (0x8000)
#define BM_RXRECEIVEDPOWER_LO_WARNING    (0x4000)

struct OnuXvrAlarmsAndWarnings_S {
	uint32_t alarmFlags;
	uint32_t warningFlags;
};
typedef struct OnuXvrAlarmsAndWarnings_S OnuXvrAlarmsAndWarnings_S;

#define VENDORNAMELENGTH 16
#define OUILENGTH 3
#define PARTNAMELENGTH 16
#define REVISIONLENGTH 4
#define SERIALNUMBERLENGTH 16
#define DATECODELENGTH 8

struct OnuXvrInventory_S {
	char vendorName[VENDORNAMELENGTH+1];
	char oui[OUILENGTH];
	char partName[PARTNAMELENGTH+1];
	char revision[REVISIONLENGTH+1];
	char serialNumber[SERIALNUMBERLENGTH+1];
	char dateCode[DATECODELENGTH+1];
};
typedef struct OnuXvrInventory_S OnuXvrInventory_S;

extern void showXvrA2dValues(char* name);
extern void showXvrThresholds(char* name);
extern void showXvrAlarmsAndWarnings(char* name);
extern void showXvrInventory(char* name);
extern void showXvrCapability(char* name);


extern bool localI2cApi_getOnuXvrA2dValues        (OnuXvrA2D_S               *p_OnuXvrA2D);
extern bool localI2cApi_getOnuXvrThresholds       (OnuXvrThresholds_S        *p_OnuXvrThresholds);
extern bool localI2cApi_getOnuXvrAlarmsAndWarnings(OnuXvrAlarmsAndWarnings_S *p_OnuXvrAlarmsAndWarnings);
extern bool localI2cApi_getOnuXvrInventory        (OnuXvrInventory_S         *p_OnuXvrInventory);
extern void localI2cApi_whetherOnuXvrSupportI2c(INT32 *support_onu_xvr_I2c);
extern void localI2cApi_getOnuXvrI2CFunc(UINT32 *p_funcs);
extern void I2C_init();

extern bool I2cApi_apmSetOnuXvrThreshold(UINT32 alarm_type, UINT32 threshold, UINT32 clear_threshold);
extern bool I2cApi_apmGetOnuXvrStateAndValue(UINT32 alarm_type, UINT8 *p_state, UINT32 *P_value);

extern  bool   I2c_cli_show_xvr_a2d_values(char* name);
extern  bool   I2c_cli_show_xvr_thresholds(char* name);
extern  bool   I2c_cli_show_xvr_alarms_and_warnings(char* name);
extern  bool   I2c_cli_show_xvr_inventory(char* name);
extern  bool   I2c_cli_show_xvr_capability(char* name);

#define I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_HIGH     0xEFFFFFFF
#define I2C_NOT_EFFECTIVE_SIGNED_THRESHOLD_LOW      0xFFFFFFFF

#endif


