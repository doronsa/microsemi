/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : I2C                                                       **/
/**                                                                          **/
/**  FILE        : I2cHwAccess.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file contains definitions for I2c HW access          **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCI2cHwAccessh
#define __INCI2cHwAccessh

#define INVENTORY_XVR_BUS_ADDRESS        (0x50)
#define OPERATIONAL_XVR_BUS_ADDRESS      (0x51)
#define ICS_SLOW_DOWN_MILLI_SECONDS      (1)

#define SUPPORT_READ_BYTE                 1     //DELTA_XCVR
#define SUPPORT_READ_BUFF                 2     //TW_XCVR 
#define SUPPORT_WRITE_QUICK               3

#if 0
#define I2C_ERROR_PRINT(msg, arg...) do{if(is_support_onu_xvr_I2c > 0) {printf(msg, ##arg);}}while(0)
#endif
#define I2C_ERROR_PRINT(msg, arg...) printf(msg, ##arg);

typedef struct 
{
    uint8_t address;
    int     length;
} RegionDescr_S;


typedef struct 
{
    int           numEntries;
    RegionDescr_S *regionDescrAra;
} RegionDb_S;


extern bool i2cHwRegionRead(int xvrBusAddress, RegionDb_S *p_RegionDb, uint8_t *buf, int bufsize, int *numBytesRead);
extern bool i2cHwRegionWrite(int xvrBusAddress, RegionDb_S *p_RegionDb, uint8_t *buf, int bufsize, int *numBytesWrite);
extern signed long i2cHwRegionIsSupported(int xvrBusAddress, int first, int last);
extern void i2cHwRegionGetI2CFuncs(unsigned long *p_funcs);

#endif


