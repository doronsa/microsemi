/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : I2C                                                       **/
/**                                                                          **/
/**  FILE        : i2c_cli.h                                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file has the CLISH routines                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 * 
 ******************************************************************************/

#ifndef __i2c_clih__
#define __i2c_clih__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"


/********************************************************************************/
/*                            i2c debug CLI functions                         */
/********************************************************************************/
bool_t i2c_cli_show_xvr_a2d_values          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t i2c_cli_show_xvr_thresholds          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t i2c_cli_show_xvr_alarms_and_warnings (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t i2c_cli_show_xvr_inventory           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t i2c_cli_show_xvr_capability           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
#endif
