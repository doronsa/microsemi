/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : I2C                                                       **/
/**                                                                          **/
/**  FILE        : i2c_cli_cmd_list_file.h                                   **/
/**                                                                          **/
/**  DESCRIPTION : This file has the CLISH action/routine matchup            **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 * 
 ******************************************************************************/

/********************************************************************************/
/*                            I2C debug CLI functions                           */
/********************************************************************************/
  {"i2c_cli_show_xvr_a2d_values",               i2c_cli_show_xvr_a2d_values},
  {"i2c_cli_show_xvr_thresholds",               i2c_cli_show_xvr_thresholds},
  {"i2c_cli_show_xvr_alarms_and_warnings",      i2c_cli_show_xvr_alarms_and_warnings},
  {"i2c_cli_show_xvr_inventory",                i2c_cli_show_xvr_inventory},
  {"i2c_cli_show_xvr_capability",               i2c_cli_show_xvr_capability},  

