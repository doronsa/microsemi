/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : DatapathDb.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the L2DA database                    **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCDataServiceDbh
#define __INCDataServiceDbh

#include "ListUtils.h"

#define NULL_EXTPARTITIONID                (0xFFFFFFFF)
#define NULL_GEMPORT                       (0xFFFFFFFF)

#define EXTRACT_VID(tci)                   ((tci) & 0xFFF)
#define EXTRACT_PBITS(tci)                 (((tci) >> 13) & 0x7)
#define MAKE_TCI(p,c,v)                    ((((p) & 0x7) << 13) | (((c) & 0x1) << 12) | ((v) & 0xFFF))

// L2DA TPKEY from OMCI: UNI port is (slot,portnum)
#define EXTRACT_PORTNUM_FROM_UNITPINST(meid)    ((meid) & 0xFF)      

#define MAX_L2DA_VLAN_TAGGING_OPS          12
#define MAX_L2DA_MCAST_VLAN_TAGGING_OPS    16

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  Common tagged/untagged definitions
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#define MAXDSCP_VALUES          64
typedef struct
{
    bool             dscpMode;
    int              untaggedFrameDefaultPriority;
    UINT8            dscpToPbitsMapping[MAXDSCP_VALUES];
} UsUntaggedPrioConfig_S;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  VLAN Filter attributes
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

typedef struct
{
    UINT16               tciAra[NUM_ODA_TPFILTERVLANS];
    int                  numEntries;
    L2DATAGGEDFILTERMODE taggedFilterMode;
    bool                 isUntaggedPassed;

} VlanTagFilter_S;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  VLAN tagging operation DB
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

typedef struct
{
    bool            inUse;
    UINT32          selfIndx;
    int             opKey;
    VlanTaggingOp_S vlanTaggingOp;
} VlanTaggingRecord_S;

typedef struct
{
    VlanTaggingRecord_S vlanTaggingRecordAra[MAX_L2DA_VLAN_TAGGING_OPS];
} VlanTaggingOpsDb_S;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  The UniTp DB definitions
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

typedef struct
{
    OMNODE               node;

    TPKEY                tpKey;
                        
    bool                 isVlanFilter;
    VlanTagFilter_S      vlanTagFilter;

    bool                 isVlanTagging;
    VlanTaggingOpsDb_S   vlanTaggingOpsDb;

    bool                 isDscpMapping;
    UINT8                dscpToPbitMapping[MAXDSCP_VALUES];  
} UniTp_S;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  The AniTp/GemPort DB
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

typedef struct
{
    bool    inUse;
    UINT16  gemInstance;    
    UINT32  gemPort;
    bool    dsValid;
    bool    usValid;
    bool    multicast;
    UINT32  dsQueue;
} GemPortCfg_S;


typedef struct
{
    bool    inUse;
    UINT16  gemInstance;
    UINT32  gemPort;
    UINT32  usQTcont;
} GemUsQMapping_S;


typedef struct
{
    GemPortCfg_S      gemPortCfgAra[MAX_PBITS];
    GemUsQMapping_S   gemUsQMapAra[MAX_PBITS];
} AllGemPortsData_S;


typedef struct
{
    OMNODE                 node;

    TPKEY                  tpKey;

    CASTTYPE               castType;

    bool                   isVlanFilter;
    VlanTagFilter_S        vlanTagFilter;

    bool                   isVlanTagging;
    VlanTaggingOpsDb_S     vlanTaggingOpsDb;

    bool                   isDscpMapping;
    UINT8                  dscpToPbitMapping[MAXDSCP_VALUES];
    
    AllGemPortsData_S      allGemPortsData;

    UsUntaggedPrioConfig_S usUntaggedPrioConfig;
} AniTp_S;


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  The McastData (UNI side)
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

typedef struct
{
    bool             inUse;
    UINT32           selfIndx;
    int              opKey;
    McastVlanTagOp_S mcastVlanTagOp;
} McastVlanTaggingRecord_S;

typedef struct
{
    McastVlanTaggingRecord_S usMcastVlanTaggingRecordAra[MAX_L2DA_MCAST_VLAN_TAGGING_OPS];
    McastVlanTaggingRecord_S dsMcastVlanTaggingRecordAra[MAX_L2DA_MCAST_VLAN_TAGGING_OPS];
} McastVlanTaggingOpsDb_S;

typedef struct
{
    OMNODE                  node;

    TPKEY                   tpKey;
    McastVlanTaggingOpsDb_S mcastVlanTaggingOpsDb;
} McastData_S;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  The common GemPort mapping DB definitions
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#if 0
typedef struct
{
    UINT32         gemPort;
    UINT32         tpmaHandle;
} DsGemHandlePair_S;

typedef struct
{
    UINT32         gemPort;
    UINT32         tpmaHandle;
    UINT32         usqTcont;
} UsGemHandleTple_S;

typedef struct
{
    DsGemHandlePair_S  dsTpmaGemHandleAra[MAX_PBITS];
    UsGemHandleTple_S  usTpmaGemHandleAra[MAX_PBITS];   // No of bits in 802.1p p-bit Mapper
    UsGemHandleTple_S  usUntaggedGemTpmaHandle;              // For US untagged frame traffic
} GemFlowHandles_S;
#endif

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  The Data Partition DB
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#define MAX_ANITPS               24                    // 
#define MAX_UNITPS               6                     // 
#define MAX_MCASTDATA            8
typedef struct
{
    bool         inUse;
    int          selfIndx;
    bool         calledTl;
    UINT32       extPartitionId;                 // Identification of external (OMCI) identification of this partition

    OMLIST       aniTpList;
    OMLIST       uniTpList;
    OMLIST       mcastDataList;

} DataPartitionDb_S;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  The full Data Service DB
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#define MAX_PARTITIONS            (16)
typedef struct
{
    DataPartitionDb_S dataPartitionDb[MAX_PARTITIONS];
} FullDataServiceDb_S;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  The Datapath for Data/Multicast API
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//
// DataService

extern void initL2DADb();
extern void l2da_resetDb();

extern DataPartitionDb_S *findPartitionByUniTpKey    (TPKEY uniTpKey);
extern DataPartitionDb_S *findPartitionByAniTpKey    (TPKEY aniTpKey);

extern void    deallocateDbAniTp         (DataPartitionDb_S *p_DataPartitionDb, AniTp_S *p_AniTp);
extern AniTp_S *findFreeAniTp            (DataPartitionDb_S *p_DataPartitionDb, TPKEY aniTpKey);
extern AniTp_S *findFirstUsedAniTp       (DataPartitionDb_S *p_DataPartitionDb);
extern AniTp_S *findNextUsedAniTp        (DataPartitionDb_S *p_DataPartitionDb, AniTp_S *p_AniTp);
extern AniTp_S *findAniTpByKey           (TPKEY aniTpKey);
 
 
extern void    deallocateDbUniTp           (DataPartitionDb_S *p_DataPartitionDb, UniTp_S *p_UniTp);
extern UniTp_S *findFreeUniTp              (DataPartitionDb_S *p_DataPartitionDb, TPKEY  uniTpKey);
extern UniTp_S *findUniTpByKey             (DataPartitionDb_S *p_DataPartitionDb, TPKEY  uniTpKey);
extern UniTp_S *findFirstUsedUniTp         (DataPartitionDb_S *p_DataPartitionDb);
extern UniTp_S *findNextUsedUniTp          (DataPartitionDb_S *p_DataPartitionDb, UniTp_S *p_UniTp);
extern int numberOfUsedUniTps(DataPartitionDb_S *p_DataPartitionDb);


extern McastData_S *findFirstUsedMcastData     (DataPartitionDb_S *p_DataPartitionDb);
extern McastData_S *findNextUsedMcastData      (DataPartitionDb_S *p_DataPartitionDb, McastData_S *p_McastData);
extern McastData_S *findFreeMcastData          (DataPartitionDb_S *p_DataPartitionDb, TPKEY mcastDataKey);
extern McastData_S *findMcastDataByKey         (DataPartitionDb_S  *p_DataPartitionDb, TPKEY mcastDataKey);
extern void        deallocateDbMcastData       (DataPartitionDb_S *p_DataPartitionDb, McastData_S *p_McastData);
extern int         numberOfConfiguredMcastDatas(DataPartitionDb_S *p_DataPartitionDb);


extern GemPortCfg_S *findGemPortCfgByKeys  (AniTp_S *p_AniTp, UINT16 gemPort, UINT16 gemInstance);
extern GemPortCfg_S *findFreeGemPortCfg    (AniTp_S *p_AniTp);



extern VlanTaggingRecord_S *findFreeVlanTaggingRecord      (VlanTaggingOpsDb_S  *p_VlanTaggingOpsDb);
extern VlanTaggingRecord_S *findVlanTaggingRecordByOpKey   (VlanTaggingOpsDb_S  *p_VlanTaggingOpsDb, int opKey);
extern VlanTaggingRecord_S *findFirstUsedVlanTaggingRecord (VlanTaggingOpsDb_S  *p_VlanTaggingOpsDb);
extern VlanTaggingRecord_S *findNextUsedVlanTaggingRecord  (VlanTaggingOpsDb_S  *p_VlanTaggingOpsDb, VlanTaggingRecord_S *p_VlanTaggingRecord);
extern int                  getNumberUsedTaggingRecords    (VlanTaggingOpsDb_S  *p_VlanTaggingOpsDb);
extern void                 freeVlanTaggingRecord          (VlanTaggingRecord_S *p_VlanTaggingRecord);
 
extern DataPartitionDb_S *findPartitionByKey         (UINT32 key);
extern DataPartitionDb_S *findFreePartition          (UINT32 extPartitionId);
 
extern int numberOfConfiguredAniTps(DataPartitionDb_S *p_DataPartitionDb);
extern int numberOfConfiguredBidiAniTps(DataPartitionDb_S *p_DataPartitionDb);
 
extern void deallocateDbPartition(DataPartitionDb_S *p_DataPartitionDb);
 
extern void doVlanFilterAnalysis_AniTp(VlanFilterInfo_S  *p_VlanFilterInfo, AniTp_S *p_AniTp, bool filterCreated);

extern void doVlanFilterAnalysis_UniTp(VlanFilterInfo_S  *p_VlanFilterInfo, UniTp_S *p_UniTp, bool filterCreated);

extern void initAniGemSettings(AniTp_S *p_AniTp);

extern L2DAPROCSTATUS copyGemPortQsPriorityInfo(AniTp_S *p_AniTp, ParamAniGem_S *p_ParamAniGem);

extern DataPartitionDb_S *getDataPartitionDb(int indx);

extern void unpackDscpValuesIntoArray(UINT8 *packedDscpToPbitsMapping, UINT8 *tgt_dscpToPbitsMappingAra);


McastVlanTaggingRecord_S *findFreeMcastVlanTaggingRecord     (McastVlanTaggingOpsDb_S *p_McastVlanTaggingOpsDb, bool isDs);
McastVlanTaggingRecord_S *findFirstUsedMcastVlanTaggingRecord(McastVlanTaggingOpsDb_S *p_McastVlanTaggingOpsDb, bool isDs);
McastVlanTaggingRecord_S *findNextUsedMcastVlanTaggingRecord (McastVlanTaggingOpsDb_S *p_McastVlanTaggingOpsDb, McastVlanTaggingRecord_S  *p_McastVlanTaggingRecord, bool isDs);
McastVlanTaggingRecord_S *findMcastVlanTaggingRecordByOpKey  (McastVlanTaggingOpsDb_S *p_McastVlanTaggingOpsDb, int opKey);
void freeMcastVlanTaggingRecord(McastVlanTaggingRecord_S *p_McastVlanTaggingRecord);
int getNumberUsedMcastTaggingRecords(McastVlanTaggingOpsDb_S *p_McastVlanTaggingOpsDb);

#endif

