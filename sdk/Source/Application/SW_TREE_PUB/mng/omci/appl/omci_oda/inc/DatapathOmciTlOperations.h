/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : DatapathOmciTlOperations.h                                **/
/**                                                                          **/
/**  DESCRIPTION : This file contains OMCI TL operations definitions         **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    25Aug11     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCDatapathOmciTlOperationsh
#define __INCDatapathOmciTlOperationsh

#include "omci_tl_expo.h"

#define CONVERT_OMCI_TL_RC(rc)                 (((rc) == OMCI_TL_RET_OK) ? L2DAPROC_DONE : L2DAPROC_ERROR)

// ptr  should be DataPartitionDb_S *
#define GET_OMCI_TL_SERVICE_ID(ptr)            ((ptr)->extPartitionId)

#define TRANSLATE_CASTTYPE_L2DA_TO_TL(c)       (((c) == CASTTYPE_BIDI) ? OMCI_TL_CAST_UNICAST : ((c) == CASTTYPE_BROADCAST) ? OMCI_TL_CAST_BROADCAST : OMCI_TL_CAST_MULTICAST)

typedef struct 
{
    int numAniBidi;
    int numAniBidiWithoutVlan;
    int numAniBc;
    int numAniMc;
    int numAniVlanFilter;
    int numAniTaggingOp;
    int numUni;
    int numUniWithoutVlan;
    int numUniVlanFilter;
    int numUniTaggingOp;
    int numMcast;
} PartitionSummary_S;


extern L2DAPROCSTATUS callOmciTlCreateService  (DataPartitionDb_S *p_DataPartitionDb);
extern L2DAPROCSTATUS callOmciTlDeleteService  (DataPartitionDb_S *p_DataPartitionDb);
extern L2DAPROCSTATUS callOmciTlUpdateService  (DataPartitionDb_S *p_DataPartitionDb);

#endif

