/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : DatapathDisplay.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the Datapath display functions       **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    14Jun10     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCDatapathDisplayh
#define __INCDatapathDisplayh

#include <stdio.h>

void omciDisplayDataService(char* name, FILE *fildes);


#endif

