/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : CellManager.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file deals with frame control for alarm and          **/
/**                MIB upload                                                **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *     2Mar04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCCellManager
#define __INCCellManager


#define CELLPOOL_SIZE        (350)
#define ALARMCELLPOOL_SIZE   (50)


void startNewMibSnapshot();
void startNewAlarmSnapshot();

OmciCell_S *giveMibUploadCell(UINT16 cellNum);
OmciCell_S *giveAlarmCell(UINT16 cellNum);

OmciCell_S *findMibUploadCell(UINT16 cellNum);
OmciCell_S *findAlarmCell(UINT16 cellNum);

void printMIBSnapshot(char* name);
void printAlarmSnapshot(char* name);


#endif


