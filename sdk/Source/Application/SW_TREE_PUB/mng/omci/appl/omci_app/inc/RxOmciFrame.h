/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : RxOmciFrame.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file has the OMCI frame Tx and Rx prototypes         **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    25Mar04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/



#ifndef __INCRxOmciFrameh
#define __INCRxOmciFrameh



//void omciGenCrcTable();

typedef void (*OMCIFRAMENOTIFYFUNC)(UINT8 *omciMsg, GL_QUEUE_ID omciMsgQueueId); 
E_ErrorCodes omciReceivedFrameNotifySet(VOIDFUNCPTR omciCallback);

void         pumpReceivedOmciFrames();


#endif
  
