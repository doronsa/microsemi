/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : GponTestCell.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file has the OMCI test frame routines                **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Feb04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/


#ifndef __INCGponTestCellh
#define __INCGponTestCellh

extern void TestCell(char* name, int sampleNum);
extern void SeqTestCell(char* name, int lowTestCellNum, int highTestCellNum);

extern void setupVoIPService(int potsNum);
extern void setupBLMVoIPService();

extern void omciTestSwDownload(UINT32 imageSize, UINT32 windowSize, UINT16 meInst);

extern void cli_mib_reset();
extern void cli_delete_me            (int meClass, int meInst);
extern void cli_set_me               (int meClass, int meInst, char *attribs);
extern void cli_get_me               (int meClass, int meInst, char *attribs);
extern void cli_create_me            (int meClass, int meInst, char *attribs);
extern void cli_add_ext_tagging_op   (int meInstance, 
                                      int filt_outer_pri,  int filt_outer_vid,  
                                      int filt_inner_pri,  int filt_inner_vid, 
                                      int tags_to_remove, 
                                      int treat_outer_pri, int treat_outer_vid, 
                                      int treat_inner_pri, int treat_inner_vid);
extern void cli_delete_ext_tagging_op(int meInst, 
                                      int filt_outer_pri,  int filt_outer_vid,  
                                      int filt_inner_pri,  int filt_inner_vid);
extern void cli_set_vlan_filters     (int meInst, char *tcis);

extern void cli_full_add_ext_tagging_op   (int meInstance,
                                           int filt_outer_pri,  int filt_outer_vid,  int filt_outer_tpid,
                                           int filt_inner_pri,  int filt_inner_vid,  int filt_inner_tpid,
                                           int ethertype,
                                           int tags_to_remove,
                                           int treat_outer_pri, int treat_outer_vid, int treat_outer_tpid,
                                           int treat_inner_pri, int treat_inner_vid, int treat_inner_tpid);
extern void cli_full_delete_ext_tagging_op(int meInstance,
                                           int filt_outer_pri,  int filt_outer_vid,  int filt_outer_tpid,
                                           int filt_inner_pri,  int filt_inner_vid,  int filt_inner_tpid,
                                           int filt_ethertype);

#endif


