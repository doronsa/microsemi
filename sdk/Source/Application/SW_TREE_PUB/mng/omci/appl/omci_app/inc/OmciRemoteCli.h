/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciRemoteCli.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : This file is the Remote CLI include file                  **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    29May06     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCOmciRemoteClih
#define __INCOmciRemoteClih


#define REMOTECLIRESPBUFFER_LEN   1000

// Routines used by Remote CLI
typedef E_ErrorCodes (*REMOTECLIHANDLERFUNC)( char *cliCommand, int cmdLength);
extern void registerRemoteCliHandler(REMOTECLIHANDLERFUNC remoteCliHandler);

extern void   copyResultToOmciRemCliRespBuffer(UINT8 *srcAddr, int length);


// Routines used by OMCI
extern void         initOmciRemCliRespBuffer();
extern void         setOmciRemCliRespBufferTimestamp();
extern int          getNumCharsInOmciRemCliRespBuffer();
extern STATUS       copyOmciRemCliRespBufferCharsToDest(int seqNum, int maxDestChars, UINT8 *destAddr);
extern E_ErrorCodes runRemCliCommand(char *cliCommand, int cmdLength);

typedef struct
{
    REMOTECLIHANDLERFUNC remoteCliHandler;
    char                 responseBuffer[REMOTECLIRESPBUFFER_LEN];
    int                  numCharsInResponseBuffer;
    UINT32               currentTime;
} S_RemoteCli;


#endif



