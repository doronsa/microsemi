/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciDefs.h                                                **/
/**                                                                          **/
/**  DESCRIPTION : This file is the MAIN OMCI include file                   **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *    22Feb04     zeev  - initial version created.                            *
 * ========================================================================== *
 *                                                                          
 *                                                                     
 ******************************************************************************/

#ifndef __INCOmciDefsh
#define __INCOmciDefsh

#include <sys/time.h>
#include <netinet/in.h>

#include "OmciExtern.h"

#define OMCI_SW_VERSION  "2.5.25"

//#define OMCI_CONNECT_TO_MIDWARE

// For Alarm, PM
#define OMCI_ALARM_BIT_MAP_LEN  28
#define OMCI_TCA_BIT_MAP_LEN    2
#define OMCI_BITS_PER_BYTE      8

typedef enum
{
    APMMONITORTYPE_ALARMAVC, APMMONITORTYPE_PMTCA
} APMMONITORTYPE;

//
//  General return code used in OMCI module
//
typedef enum 
{
    OMCIPROC_ERROR, OMCIPROC_CONTINUE, OMCIPROC_DONE
} OMCIPROCSTATUS;


//
//  Managed Entity Identifiers
//
typedef enum
{
    MIN_ENTITY_CLASS=1,
    ONT_Data=2,
    PON_IF_Line_Cardholder=3,                          Subscriber_Line_Cardholder=5,
    Subscriber_Line_Card=6,                            Software_Image=7,                                  
    PPTP_Ethernet_UNI=11,                              Ethernet_Performance_Monitoring_History_Data=24,
    MAC_Bridge_Service_Profile=45,
    MAC_Bridge_Configuration_Data=46,                  MAC_Bridge_Port_Configuration=47, 
    MAC_Bridge_Designation_Data=48,                    MAC_Bridge_Port_Filter_Table=49, 
    MAC_Bridge_Port_Bridge_Table=50,                   MAC_Bridge_PM_History_Data=51, 
    MAC_Bridge_Port_PM_History=52,                     PPTP_POTS_UNI=53, 
    Voice_Service_Profile_AAL=58,           
    Vlan_Tagging_Operation_Config=78,                  PPTP_Video_UNI=82,
    Vlan_Tagging_Filter=84,                            Ethernet_Pm_History_Data_2=89,
    PPTP_Video_ANI=90,
    PPTP_xDSL_UNI_Part1=98,                            PPTP_xDSL_UNI_Part2=99,
    xDSL_Line_Inventory_Status_Data_Part1=100,         xDSL_Line_Inventory_Status_Data_Part2=101,
    xDSL_Channel_Downstream_Status_Data=102,           xDSL_Channel_Upstream_Status_Data=103,
    xDSL_Line_Config_Profile_Part1=104,                xDSL_Line_Config_Profile_Part2=105,
    xDSL_Line_Config_Profile_Part3=106,                xDSL_Channel_Configuration_Profile=107,
    xDSL_Subcarrier_Masking_Downstream_Profile=108,    xDSL_Subcarrier_Masking_Upstream_Profile=109,
    xDSL_PSD_Mask_Profile=110,                         xDSL_Downstream_RFI_Bands_Profile=111,
    xDSL_xTUC_Pm_History_Data=112,                     xDSL_xTUR_Pm_History_Data=113,
    xDSL_xTUC_Channel_Pm_History_Data=114,             xDSL_xTUR_Channel_Pm_History_Data=115,
    Mapper_8021p_Service_Profile=130,                  OLT_G=131,
    IP_Host_Config_Data=134,                           IP_Host_Monitoring_Data=135,
    TCP_UDP_Config_Data=136,                           Network_Address=137,
    VoIP_Config_Data=138,                              VoIP_Voice_CTP=139,
    Call_Control_PM_History_Data=140,                  VoIP_Line_Status=141,
    VoIP_Media_Profile=142,                            RTP_Profile_Data=143,
    RTP_Monitoring_Data=144,                           Network_Dial_Plan_Table=145,
    VoIP_Application_Service_Profile=146,              VoIP_Feature_Access_Codes=147,
    Authentication_Security_Method=148,                SIP_Config_Portal=149,
    SIP_Agent_Config_Data=150,                         SIP_Agent_Monitoring_Data=151,
    SIP_Call_Initiation_Pm_History_Data=152,           SIP_User_Data=153,
    MGC_Config_Portal=154,                             MGC_Config_Data=155,
    MGC_Monitoring_Data=156,                           

    Large_String=157,
    ONT_Remote_Debug=158,
//  Entrisphere        Large_String=158,
//  Entrisphere        ONT_Remote_Debug=160,
    PPTP_MOCA_UNI=162,                                 MoCA_Ethernet_PM_History=163,
    MoCA_Interface_PM_History=164,                     VDSL2_Line_Configuration_Extensions=165,
    Ext_Vlan_Tagging_Op_Cfg_Data=171,        
    Ext_8021p_Mapper_Service_Profile=240,

    BASE_GPON_MECLASS=256,
    ONT_G=256,                                         ONT2_G=257,
    PON_IF_Line_Card_G=260,                            PON_TC_Adapter_G=261,
    TCONT_G=262,                                       ANI_G=263,
    UNI_G=264,                                         GEM_Interworking_TP=266,
    GEM_Port_PM_History_Data=267,                      GEM_Port_Network_CTP=268,
    GAL_Ethernet_Profile=272,                          Threshold_Data1=273,
    Threshold_Data2=274,                               GAL_Ethernet_PM_History_Data=276,
    Priority_Queue_G=277,                              Traffic_Scheduler_G=278,
    GEM_Traffic_Descriptor=280,                        Multicast_GEM_Interworking_TP=281,
    OMCI_ME=287,                                       Managed_Entity_ME=288,
    Attribute_ME=289,
    Ethernet_Pm_History_Data_3=296,
    Multicast_Operations_Profile=309,                  Multicast_Subscriber_Config_Info=310,
    Multicast_Subscriber_Monitor=311,
    Ethernet_Frame_Pm_History_Data_Ds=321,             Ethernet_Frame_Pm_History_Data_Us=322,
    Virtual_Ethernet_IP=329,
    Ethernet_Frame_Extended_Pm=334,                    Tr069_Management_Server=340, 
    GEM_Port_Network_CTP_Pm=341,
    MAX_G988_MECLASS=341,

    BASE_CTC_MECLASS=65528,
    CTC_Lbdt_Detection=65528,                          CTC_Onu_Capability=65529,
    CTC_LOID_Authentication=65530,                     CTC_Ext_Mcast_Operations_Profile=65531,
    MAX_CTC_MECLASS=65531
} MECLASS;



//  Message Type Identifiers
//
typedef enum 
{
    MT_Create=4,                   MT_CreateCompleteConnection=5,         MT_Delete=6, 
    MT_DeleteCompleteConnection=7, MT_Set=8,                              MT_Get=9, 
    MT_GetCompleteConnection=10,   MT_GetAllAlarms=11,                    MT_GetAllAlarmsNext=12, 
    MT_MIBUpload=13,               MT_MIBUploadNext=14,                   MT_MIBReset=15, 
    MT_AlarmNotification=16,       MT_AVCNotification=17,                 MT_Test=18,  
    MT_StartSoftwaredownload=19,   MT_DownloadSection=20,                 MT_EndSoftwaredownload=21,
    MT_ActivateSoftware=22,        MT_CommitSoftware=23,                  MT_SynchronizeTime=24,  
    MT_Reboot=25,                  MT_GetNext=26,                         MT_TestResultNotification=27,
    MT_GetCurrentData=28
} OMCI_MSGTYPE;


// OMCI completion codes
//
typedef enum 
{
    MSGCOMPL_SUCCESS=0,    MSGCOMPL_CMDPROCERROR=1,  MSGCOMPL_CMDNOTSUPPORTED=2, MSGCOMPL_PARAMERROR=3, 
    MSGCOMPL_UNKNOWNME=4,  MSGCOMPL_UNKNOWNMEINST=5, MSGCOMPL_DEVICEBUSY=6,
    MSGCOMPL_INSTEXISTS=7, MSGCOMPL_ATTRIBFAIL=9
} MSGCOMPLCODE;


typedef enum
{
    TRAFOPT_PRIORITY=0, TRAFOPT_CELLRATE
} TRAFFICMGTOPTION;


typedef enum 
{
    ADMINSTATUS_UNLOCK=0, ADMINSTATUS_LOCK=1
} ADMINISTRATIVESTATUS;


typedef enum 
{
    OPERSTATUS_ENABLED=0, OPERSTATUS_DISABLED=1, OPERSTATUS_UNKNOWN=2
} OPERATIONALSTATUS;


typedef enum
{
    SWIMCOMMIT_UNCOMMITTED=0, SWIMCOMMIT_COMMITTED=1
} SWIMAGECOMMIT;


typedef enum
{
    SWIMACTIVITY_INACTIVE=0, SWIMACTIVITY_ACTIVE=1
} SWIMAGEACTIVITY;

typedef enum
{
    SWIMVALIDITY_INVALID=0, SWIMVALIDITY_VALID=1
} SWIMAGEVALIDITY;


typedef enum
{
    LANFCSIND_FORWARDED=0, LANFCSIND_DISCARDED=1
} LANFCSIND;


typedef enum
{
    ENCAPSMETHOD_ATMVC=0, ENCAPSMETHOD_LLC=1
} ENCAPSULATIONMETHOD;



typedef enum
{
    CT_noLIM=0,          CT_10_T=22,      CT_100B_T=23,    CT_10_100B_T=24,     
    CT_POTS=32,          CT_Gigabit=34,   CT_xDSL=35,      CT_Video=38,     CT_WIFI=40, CT_MOCA=46,
    CT_Gig_FE_10_BTx=47, CT_Veip=48,      CT_GPON24881244=248, 
    CT_PandP_Unknown=255
} CARDTYPE;


typedef enum
{
    BRIP_BRIDGED=0, BRIP_IPROUTER=1, BRIP_BRIDGEDANDIPROUTER=2
} BRIDGEDORIPIND;


typedef enum
{
    CESLOOPBACK_NOLOOPBACK=0, CESLOOPBACK_PAYLOAD=1,     CESLOOPBACK_LINE=2,   CESLOOPBACK_OPS1ATM=3, 
    CESLOOPBACK_OPS2UNI=4,    CESLOOPBACK_OPS3ATMUNI=5,  CESLOOPBACK_MANUAL=6, CESLOOPBACK_NWCODEINBAND=7, 
    CESLOOPBACK_SMARTJACK=8,  CESLOOPBACK_NWCODEINBAND_ARMED=9 
} CESLOOPBACK;


typedef enum
{
    CESFRAMING_EXTSUPERFRAME=0, CESFRAMING_SUPERFRAME=1, CESFRAMING_UNFRAME=2, CESFRAMING_G704=3, 
    CESFRAMING_JTG704=4 
} CESFRAMING;


typedef enum
{
    CESENCODING_B8ZS=0, CESENCODING_AMI=1, CESENCODING_HDB3=2
} CESENCODING;


typedef enum
{
    LINELENGTH_V0=0,   LINELENGTH_V1=1,   LINELENGTH_V2=2,  LINELENGTH_V3=3,  LINELENGTH_V4=4,    LINELENGTH_V5=5, 
    LINELENGTH_V6=6,   LINELENGTH_V7=7,   LINELENGTH_V8=8,  LINELENGTH_V9=9,  LINELENGTH_V10=10,  LINELENGTH_V11=11, 
    LINELENGTH_V12=12, LINELENGTH_V13=13, LINELENGTH_V14=14
} LINELENGTH;


typedef enum
{
    DS1MODE_MODE1=0, DS1MODE_MODE2=1, DS1MODE_MODE3=2, DS1MODE_MODE4=3
} DS1MODE;


typedef enum 
{
    IWOPTION_CES=0,         IWOPTION_MCASTNOOP=0,       IWOPTION_MACBRIDGELAN=1, IWOPTION_MAPPER8021P=5,
    IWOPTION_DSBROADCAST=6, IWOPTION_MPLSDATASERVICE=7

} INTERWORKINGOPTION;


typedef enum 
{
    OMCIBOOL_FALSE=0,  OMCIBOOL_TRUE=1
} OMCIBOOLEAN;


typedef enum 
{
    ARC_OFF=0,  ARC_ON=1
} ARCVALUE;


typedef enum 
{
    TPTYPE_LAN_SIDE=1,  TPTYPE_ATM_SIDE=2,        TPTYPE_802_1P_MAPPER=3,  TPTYPE_IPHOST_CONFIG_DATA=4,
    TPTYPE_GEM_IW_TP=5, TPTYPE_MCAST_GEM_IW_TP=6, TPTYPE_XDSL_UNI_PART1=7, TPTYPE_VEIP=11
} TPTYPE;


// VP Link direction
typedef enum
{
    VPLINKDIRECT_UNI2ANI=1, VPLINKDIRECT_ANI2UNI=2, VPLINKDIRECT_BIDIRECTIONAL
} VPLINKDIRECTION;


// Ethernet autodetection/default configuration
typedef enum
{
    ETHER_AUTODPX_AUTOSP=0,     ETHER_FDX_10=1,      ETHER_FDX_100=2,     ETHER_FDX_1000=3,    ETHER_FDX_AUTOSP=4, 
    ETHER_AUTODPX_10=0x10,      ETHER_HDX_10=0x11,   ETHER_HDX_100=0x12,  ETHER_HDX_1000=0x13, ETHER_HDX_AUTOSP=0x14, 
    ETHER_AUTODPX_1000=0x20,    ETHER_AUTODPX_100=0x30
} ETHERNETCONFIG;


// Ethernet DTE/DCE 
typedef enum
{
    ETHERWIRE_DCE=0, ETHERWIRE_DTE=1, ETHERWIRE_AUTOSELECT
} ETHERWIRINGCFG;


// POTS Impedance
typedef enum
{
    IMPEDANCE_600=0, IMPEDANCE_900=1, IMPEDANCE_270=2, IMPEDANCE_220=3, IMPEDANCE_320=4
} IMPEDANCE;


// POTS Transmission path
typedef enum
{
    TRANSMISSIONPATH_FULLTIME=0, TRANSMISSIONPATH_PARTTIME=1
} TRANSMISSIONPATH;


// PCM encoding type
typedef enum
{
    PCMENCTYPE_MULAW=1, PCMENCTYPE_ALPHALAW=2
} PCMENCTYPE;


// TCONT-G mode
typedef enum
{
    TCONTGMODE_ATM=0, TCONTGMODE_GEM=1
} TCONTGMODE;


// TCONT-G policy
typedef enum
{
    TCONTGPOLICY_NULL=0, TCONTGPOLICY_HOL=1, TCONTGPOLICY_WRR=2 
} TCONTGPOLICY;


// ONT2-G mode
typedef enum
{
    ONT2GMODE_ATM=0, ONT2GMODE_GEM=1, ONT2GMODE_GEMANDATM=2
} ONT2GMODE;


// General capability mode
typedef enum
{
    OMCI_DISABLED=0, OMCI_ENABLED=1
} OMCICAPABILITYMODE;


// Piggyback DBA reporting
typedef enum
{
    PIGGYBACK_00=0, PIGGYBACK_01=1, PIGGYBACK_02=2, PIGGYBACK_012=3, PIGGYBACK_NONE=4
} PIGGYBACKDBAREPORTING;


// Whole ONU DBA reporting
typedef enum
{
    WHOLEONUDBAREPORTING_OFF=0, WHOLEONUDBAREPORTING_ON=1
} WHOLEONUDBAREPORTING;


// Security
typedef enum
{
    SECURITY_RSVD=0, SECURITY_AES=1
} SECURITY;


// Priority Queue-G queue option
typedef enum
{
    QUEUEOPTION_SHARED=0, QUEUEOPTION_INDIVIDUAL=1
} QUEUEOPTION;


// Signaling Protocol used (VOIP)
typedef enum
{
    SIGPROTOUSED_NONE=0, SIGPROTOUSED_SIP=1, SIGPROTOUSED_H248=2, SIGPROTOUSED_MGCP=3, SIGPROTOUSED_NONOMCI=0xFF  
} SIGNALINGPROTOCOLUSED;


// Signaling Protocol used (VOIP)
typedef enum
{
    VOIPCFGMETHOD_NOCFG=0, VOIPCFGMETHOD_OMCI=1, VOIPCFGMETHOD_FILE=2, VOIPCFGMETHOD_TR69=3, VOIPCFGMETHOD_IETF=4  
} VOIPCONFIGMETHODUSED;


typedef enum
{
    LOOPBACKCFG_NONE= 0, LOOPBACKCFG_LOOP3=3
} LOOPBACKCFG;


typedef enum
{
    XDSLLOOPBACKCFG_NONE= 0, XDSLLOOPBACKCFG_LPBK2=1
} XDSLLOOPBACKCFG;


typedef enum 
{
    HOOKSTATE_ONHOOK=0, HOOKSTATE_OFFHOOK=1
} HOOKSTATE;


typedef enum 
{
    MAP8021PTPTYPE_BRIDGE=0, MAP8021PTPTYPE_ETHERNET=1, MAP8021PTPTYPE_IPHOST=2, MAP8021PTPTYPE_XDSL=4, MAP8021PTPTYPE_VEIP=7
} MAP8021PTPTYPE;


typedef enum 
{
    VTAGOPASSOCTYPE_DEFAULT=0,    VTAGOPASSOCTYPE_IPHOST=1, VTAGOPASSOCTYPE_MBPCD=3, VTAGOPASSOCTYPE_MAPPER8021P=4, 
    VTAGOPASSOCTYPE_ETHERUNI=10,  VTAGOPASSOCTYPE_VEIP=11
} VTAGOPASSOCTYPE;


typedef enum 
{
    UNMARKEDFRAMEOPTION_DSCPTO8021P=0, UNMARKEDFRAMEOPTION_DEFAULTPRIORITY=1
} UNMARKEDFRAMEOPTION;


typedef enum 
{
    UPSTRVLANTAGGINGMODE_ASIS=0, UPSTRVLANTAGGINGMODE_TAGWITHOVERWRITE=1, UPSTRVLANTAGGINGMODE_TAGWITHQINQ=2
} UPSTRVLANTAGGINGMODE;


typedef enum 
{
    DOWNSTRVLANTAGGINGMODE_ASIS=0, DOWNSTRVLANTAGGINGMODE_UNTAG=1
} DOWNSTRVLANTAGGINGMODE;


typedef enum 
{
    MNGTCAPABILITY_OMCI=0, MNGTCAPABILITY_NONOMCI=1, MNGTCAPABILITY_OMCI_AND_NONOMCI=2
} MANAGEMENTCAPABILITY;


#define ISPOTSTESTMODE_NORMAL(t)     ( (((t) & 0x80) == 0) ? true : false)
#define POTSTEST_SELECTOR(t)         ((t) & 0x0F)

typedef enum 
{
    POTSTEST_ALLMLT=0,           POTSTEST_HAZARDOUS_POTENTIAL=1, POTSTEST_FOREIGN_EMF=2, 
    POTSTEST_RESISTIVE_FAULTS=3, POTSTEST_RECEIVER_OFFHOOK=4,    POTSTEST_RINGER=5, 
    POTSTEST_NT1_DC_SIGNATURE=6, POTSTEST_SELFTEST=7,            POTSTEST_DIALTONE_MAKEBREAK=8
} POTSTEST;


typedef enum 
{
    MLTTESTTYPE_HAZARDOUS_POTENTIAL=0x1,  MLTTESTTYPE_FOREIGN_EMF=0x2,  MLTTESTTYPE_RESISTIVE_FAULTS=0x4, 
    MLTTESTTYPE_RECEIVER_OFFHOOK=0x8,     MLTTESTTYPE_RINGER=0x10,      MLTTESTTYPE_NT1_DC_SIGNATURE=0x20
} MLTTESTTYPE;


typedef enum 
{
    MLTTESTREPORT_TESTNOTRUN=0,       MLTTESTREPORT_FAIL_NOMEASURE=2,  MLTTESTREPORT_FAIL_WITHMEASURE=3, 
    MLTTESTREPORT_PASS_NOMEASURE=6,   MLTTESTREPORT_PASS_WITHMEASURE=7
} MLTTESTREPORT;


typedef enum
{
    XDSLMODEMTYPE_UNDEFINED=0,  XDSLMODEMTYPE_ATM=1,  XDSLMODEMTYPE_PTM=2
} XDSLMODEMTYPE;


typedef enum
{
    ALARMREPORT_ALLALARMS=0,  ALARMREPORT_NOARCALARMS=1
} ALARMREPORT;

typedef enum
{
    OMCI_TCA_1 = 0,
    OMCI_TCA_2,
    OMCI_TCA_3,
    OMCI_TCA_4,
    OMCI_TCA_5,
    OMCI_TCA_6,
    OMCI_TCA_7,
    OMCI_TCA_8,
    OMCI_TCA_9,
    OMCI_TCA_10,
    OMCI_TCA_11,
    OMCI_TCA_12,
    OMCI_TCA_13,
    OMCI_TCA_14    
} OMCI_TCA_NUM_E;

//------------------------------------------------------------------------------

//---------------------    messageContent Layout   -----------------------------


// messageContents - MIB Get All Alarms Command
#pragma pack(1)
typedef struct 
{
    UINT8  alarmRetrievalMode;
} GetAllAlarmsCmd_S;
#pragma pack(0)



// messageContents - MIB Get All Alarms Response
#pragma pack(1)
typedef struct 
{
    UINT16 numSubsequentCommands;
    UINT8  padding[30];
} GetAllAlarmsResp_S;
#pragma pack(0)



// messageContents - MIB Get All Alarms Next Command
#pragma pack(1)
typedef struct 
{
    UINT16 commandSequenceNum;
} GetAllAlarmsNextCmd_S;
#pragma pack(0)



// messageContents - MIB Get All Alarms Next Response
// NB GPON vs BPON
#pragma pack(1)
typedef struct 
{
    UINT16 entityClass;
    UINT16 entityInstance;
    UINT8  bitmapAlarms[OMCI_ALARM_BIT_MAP_LEN];
} GetAllAlarmsNextResp_S;
#pragma pack(0)



// messageContents - MIB Upload Response
#pragma pack(1)
typedef struct 
{
    UINT16 numSubsequentCommands;
    UINT8  padding[30];
} MibUploadResp_S;
#pragma pack(0)



// messageContents - MIB Upload Next Command
#pragma pack(1)
typedef struct 
{
    UINT16 commandSequenceNum;
} MibUploadNextCmd_S;
#pragma pack(0)



// messageContents - MIB Upload Next Response
// NB GPON vs BPON
#pragma pack(1)
typedef struct 
{
    UINT16 entityClass;
    UINT16 entityInstance;
    UINT8  msbAttribMask;
    UINT8  lsbAttribMask;
    UINT8  attribData[26];
} MibUploadNextResp_S;
#pragma pack(0)



// messageContents - Get Command
#pragma pack(1)
typedef struct 
{
    UINT8  msbAttribMask;
    UINT8  lsbAttribMask;
} GetCmd_S;
#pragma pack(0)



// messageContents - Get Response
#pragma pack(1)
typedef struct 
{
    UINT8  result;
    UINT8  msbAttribMask;
    UINT8  lsbAttribMask;
    UINT8  attribData[25];
    UINT8  msbUnsupportedAttribMask;
    UINT8  lsbUnsupportedAttribMask;
    UINT8  msbFailedAttribMask;
    UINT8  lsbFailedAttribMask;
} GetResp_S;
#pragma pack(0)



// messageContents - Get Command
#pragma pack(1)
typedef struct 
{
    UINT8  msbAttribMask;
    UINT8  lsbAttribMask;
    UINT16 cmdSeqNum;
} GetNextCmd_S;
#pragma pack(0)



// messageContents - Get Response
#pragma pack(1)
typedef struct 
{
    UINT8  result;
    UINT8  msbAttribMask;
    UINT8  lsbAttribMask;
    UINT8  attribData[29];
} GetNextResp_S;
#pragma pack(0)



// messageContents - Set Command
#pragma pack(1)
typedef struct 
{
    UINT8  msbAttribMask;
    UINT8  lsbAttribMask;
    UINT8  attribData[30];
} SetCmd_S;
#pragma pack(0)



// messageContents - Set Response
#pragma pack(1)
typedef struct 
{
    UINT8  result;
    UINT8  msbUnsupportedAttribMask;
    UINT8  lsbUnsupportedAttribMask;
    UINT8  msbFailedAttribMask;
    UINT8  lsbFailedAttribMask;
    UINT8  padding[27];
} SetResp_S;
#pragma pack(0)


// messageContents - Start software download Command
#pragma pack(1)
typedef struct 
{
    UINT8  windowSizeM1;
    UINT32 imageSize;
} StartSoftwareDownloadCmd_S;
#pragma pack(0)


// messageContents - Start software download Response
#pragma pack(1)
typedef struct 
{
    UINT8  result;
    UINT8  windowSizeM1;
    UINT8  padding[30];
} StartSoftwareDownloadResp_S;
#pragma pack(0)



// messageContents - Download section Command
#pragma pack(1)
#define DWNLD_SECTION_SIZE                 (31)
typedef struct 
{
    UINT8  sectionNumber;
    UINT8  data[DWNLD_SECTION_SIZE];
} DownloadSectionCmd_S;
#pragma pack(0)


// messageContents - Download section Response
#pragma pack(1)
typedef struct 
{
    UINT8  result;
    UINT8  sectionNumber;
    UINT8  padding[30];
} DownloadSectionResp_S;
#pragma pack(0)



// messageContents - End software download Command
#pragma pack(1)
typedef struct 
{
    UINT32 crc32;
    UINT32 imageSize;
} EndSoftwareDownloadCmd_S;
#pragma pack(0)


// messageContents - End software download Response
#pragma pack(1)
typedef struct 
{
    UINT8  result;
    UINT8  padding[31];
} EndSoftwareDownloadResp_S;
#pragma pack(0)


// messageContents - Create Response
#pragma pack(1)
typedef struct 
{
    UINT8  result;
    UINT8  msbAttribExecMask;
    UINT8  lsbAttribExecMask;
    UINT8  padding[29];
} CreateResp_S;
#pragma pack(0)



// messageContents - Test request
#pragma pack(1)
typedef struct 
{
    UINT8   testSelectAndMode;
    UINT8   dbdtTimer1;
    UINT8   dbdtTimer2;
    UINT8   dbdtTimer3;
    UINT8   dbdtTimer4;
    UINT8   dbdtControl;
    UINT8   digitDialed;
    UINT16  dialToneFrequency1;
    UINT16  dialToneFrequency2;
    UINT16  dialToneFrequency3;
    UINT8   dialTonePowerThreshold;
    UINT8   idleChannelPowerThreshold;
    UINT8   dcHazardousVoltageThreshold;
    UINT8   acHazardousVoltageThreshold;
    UINT8   dcForeignVoltageThreshold;
    UINT8   acForeignVoltageThreshold;
    UINT8   tipGroundRingGroundResistanceThreshold;
    UINT8   tipRingResistanceThreshold;
    UINT16  ringerEquivalenceMinimumThreshold;
    UINT16  ringerEquivalenceMaximumThreshold;
    UINT8   padding[7];
} TestPotsUniReq_S;
#pragma pack(0)

// messageContents - Test request
#pragma pack(1)
typedef struct 
{
    UINT8   testSelect;
    UINT8   padding[31];
} TestAnigReq_S;
#pragma pack(0)

// messageContents - Alarm Notification
#pragma pack(1)
typedef struct 
{
    UINT8  alarmBitMaskAra[30];
    UINT8  padding[1];
    UINT8  alarmSequenceNumber;
} AlarmNotification_S;
#pragma pack(0)



// messageContents - Attribute Value Change Notification
#pragma pack(1)
typedef struct 
{
    UINT8  msbAttribMask;
    UINT8  lsbAttribMask;
    UINT8  attribsAra[30];
} AttributeValueChangeNotification_S;
#pragma pack(0)




// messageContents - General Response
#pragma pack(1)
typedef struct 
{
    UINT8  result;
    UINT8  padding[31];
} GeneralResp_S;
#pragma pack(0)



//---------------------    OMCI  Frame  Layout   -----------------------------


// OMCI Frame 
#define MSGCONTENTS_LEN          (32)
#define MSGTRAILER_LEN           (4) /* This is only the first 4 bytes of OMCI trailer, the last 4 bytes is generated by HW*/
#pragma pack(1)
typedef struct
{
    UINT16 transactionCorrelationId;
    UINT8  messageType;
    UINT8  deviceId;
    UINT16 meId;
    UINT16 meInstance;
    union
    {
        UINT8                              messageContents[MSGCONTENTS_LEN];
        CreateResp_S                       createResp;
        GetAllAlarmsCmd_S                  getAllAlarmsCmd;
        GetAllAlarmsResp_S                 getAllAlarmsResp;
        GetAllAlarmsNextCmd_S              getAllAlarmsNextCmd;
        GetAllAlarmsNextResp_S             getAllAlarmsNextResp;
        MibUploadResp_S                    mibUploadResp;
        MibUploadNextCmd_S                 mibUploadNextCmd;
        MibUploadNextResp_S                mibUploadNextResp;
        GetCmd_S                           getCmd;
        GetResp_S                          getResp;
        SetCmd_S                           setCmd;
        SetResp_S                          setResp;
        StartSoftwareDownloadCmd_S         startSoftwareDownloadCmd;
        StartSoftwareDownloadResp_S        startSoftwareDownloadResp;
        DownloadSectionCmd_S               downloadSectionCmd;
        DownloadSectionResp_S              downloadSectionResp;
        EndSoftwareDownloadCmd_S           endSoftwareDownloadCmd;
        EndSoftwareDownloadResp_S          endSoftwareDownloadResp;
        TestPotsUniReq_S                   testPotsUniReq;
        TestPotsUniResultsResp_s           testPotsUniResultsResp;
        TestAnigReq_S                      testAnigReq; 
        TestAnigResultsResp_s              testAnigResultsResp;        
        AlarmNotification_S                alarmNotification;
        AttributeValueChangeNotification_S attributeValueChangeNotification;
        GetNextCmd_S                       getNextCmd;
        GetNextResp_S                      getNextResp;
        GeneralResp_S                      generalResp;
    } u;

    UINT8 trailer[MSGTRAILER_LEN];
} OmciCell_S;
#pragma pack(0)


#pragma pack(1)
typedef struct
{
    UINT8          omciContent[sizeof(OmciCell_S )];
    struct timeval enqueuedTime;
} TimedOmciFrame_S;
#pragma pack(0)


// Miscellaneous definitions
#define VERSION_LEN                (14)
#define SERIALNUMBER_LEN           (8)
#define VENDORID_LEN               (4)


// Software Image ME Ids
#define SWIMAGEMEID_BANKA          (0)
#define SWIMAGEMEID_BANKB          (1)

// Index in messageContents for completion status
#define MSGCOMPLSTATUS_INDX        0

// Index in messageContents for incoming Get command MSB/LSB Attribute Masks
#define MSB_RCV_GETATTRIBS_INDX    0
#define LSB_RCV_GETATTRIBS_INDX    1

// Index in messageContents for outgoing Get command MSB/LSB Attribute Masks
#define MSB_XMIT_GETATTRIBS_INDX   1
#define LSB_XMIT_GETATTRIBS_INDX   2

// Index in messageContents for incoming Set command MSB/LSB Attribute Masks
#define MSB_RCV_SETATTRIBS_INDX   0
#define LSB_RCV_SETATTRIBS_INDX   1

// Start Index in messageContents for outgoing Get Response attribute data
#define GETRESP_MSGCONTENTS_START_INDX         3
// Last Index for content in Get Response
#define GETRESP_MSGCONTENTS_LASTCONTENT_INDX   28

// Index in messageContents for outgoing Get Response MSB/LSB unsupported Attribute Masks
#define MSB_XMIT_GETATTRIBS_UNSUPPORT_INDX   MSGCONTENTS_LEN-4
#define LSB_XMIT_GETATTRIBS_UNSUPPORT_INDX   MSGCONTENTS_LEN-3

// Index in messageContents for outgoing Get Response MSB/LSB failed Attribute Masks
#define MSB_XMIT_GETATTRIBS_FAILED_INDX   MSGCONTENTS_LEN-2
#define LSB_XMIT_GETATTRIBS_FAILED_INDX   MSGCONTENTS_LEN-1


// Index in messageContents for outgoing Set Response MSB/LSB unsupported Attribute Masks
#define MSB_XMIT_SETATTRIBS_UNSUPPORT_INDX   1
#define LSB_XMIT_SETATTRIBS_UNSUPPORT_INDX   2

// Index in messageContents for outgoing Set Response MSB/LSB failed Attribute Masks
#define MSB_XMIT_SETATTRIBS_FAILED_INDX   3
#define LSB_XMIT_SETATTRIBS_FAILED_INDX   4

// Start Index in messageContents for outgoing Set Response padding
#define SETRESP_MSGCONTENTS_PADDING_START_INDX   5


// Index in messageContents for outgoing MIB Upload number of subseqeunt commands
#define MSB_XMIT_UPLOAD_INDX   0
#define LSB_XMIT_UPLOAD_INDX   1


// Index in messageContents for incoming MIB Upload Next command sequence number
#define MSB_RCV_UPLOADNEXT_INDX   0
#define LSB_RCV_UPLOADNEXT_INDX   1


// Index for content in MIB Upload Next Response
#define MIBUPLOADNEXTRESP_ENTITY_CLASS                   0
#define MIBUPLOADNEXTRESP_ENTITY_INSTANCE                1
#define MIBUPLOADNEXTRESP_MSB_ATTRIBMASK                 3
#define MIBUPLOADNEXTRESP_LSB_ATTRIBMASK                 4

// Start Index in messageContents for outgoing MIB Upload Response attribute data
#define MIBUPLOADNEXTRESP_MSGCONTENTS_START_INDX         5
// Last Index for content in Get Response
#define MIBUPLOADNEXTRESP_MSGCONTENTS_LASTCONTENT_INDX   32




#define BM_XACTION_CORR_PRIORITY  (0x8000)
#define PRIORITY_INDEX(x)         ( (((x) & BM_XACTION_CORR_PRIORITY) == 0) ? 0 : 1 )
#define ISHIGHPRIORITY(pcell)     ( ((ntohs(((pcell)->transactionCorrelationId)) & BM_XACTION_CORR_PRIORITY) == 0) ? false : true )

#define DEVICEID_BPON             (0xA)

#define BM_MSGTYPE_DB             (0x80)
#define BM_MSGTYPE_AR             (0x40)
#define BM_MSGTYPE_AK             (0x20)
#define BM_MSGTYPE_MT             (0x1F)

#define MSGTYPE(x)                ((x) & BM_MSGTYPE_MT)


// Upstream queues have msb of their ME Id asserted - this is in fact ANI slot Id
// According to G983.2 7.1.5 a PON IF Line Cardholder is not created for 
// integrated PON interfaces - so the 0x80 part represents the "Slot #"
#define UPSTREAM_Q_FLAG           (0x8000)
#define ISQUEUE_UPSTREAMQ(x)      ( ((x) & UPSTREAM_Q_FLAG) ? true : false )
#define EXTRACT_Q_NUM(x)          ( (x) & 0x7FFF )

#define MAX_QUEUESIZE             0x4000
#define ALLOCATED_QUEUESIZE       0x1000


// The ANI side indicator : the 0x80 part represents the "Slot #"
#define ISUNIANIPTR_ANISIDE(x)    ( ((x) > 255) ? true : false ) 

// The ANI PON IF
#define ANI_PON_IF                (0x8001)


// Entity instance NULL Pointer - 14-Jun-04 - still in discussion
#define OMCI_NULLINSTANCE         (0xFFFF)

#define OMCI_ZEROINSTANCE         (0)


//  Circuit pack/Cardholder instance/managed entity id
#define MAKE_SLOT_CARD_MEID(sl)                     (( (1) <<8) | ((sl) & 0xFF))

#define EXTRACT_SLOTNUM_FROM_CARDMEID(meid)         ((meid) & 0xFF)      

#define EXTRACT_PORTNUM_FROM_UNIMEID(meid)          ((meid) & 0xFF)      
#define EXTRACT_SLOTNUM_FROM_UNIMEID(meid)          (((meid)>>8) & 0xFF)      

#define MAKE_UNI_MEINSTANCE(s,p)                    (( ((s) & 0xFF) <<8) | ((p) & 0xFF))

#define MAKE_ME(meClass, meInstance)                (((meClass) << 16) | ((meInstance) & 0xFFFF))

// TCONT instance/managed entity id
#define BASETCONTMEID                               (0x8000)
#define DEFAULT_TCONTG_ALLOCID                      (0xFF)

// ANI-G instance/managed entity id
#define ANIG_GEMBLOCKLENGTH                         (0x30)


typedef struct 
{
    UINT32 tcParam[16];
} ThresholdCrossingApmParams_s;


// Max p-bits priorities
#define MAX_PRIORITIES             8


// Rx Gain range
#define RXGAIN_MIN                 (-120)
#define RXGAIN_MAX                 (60)


// Tx Gain range
#define TXGAIN_MIN                 (-60)
#define TXGAIN_MAX                 (120)


#define ETHERDEFAULT_MAXFRAMESIZE  1518
#define ETHERDEFAULT_ADMINSTATUS   ADMINSTATUS_UNLOCK

#define POTSDEFAULT_ADMINSTATUS    ADMINSTATUS_UNLOCK

#define DEFAULT_BM_AVAILVOIPSIGPROTOCOL     1    // 1 is SIP
#define DEFAULT_BM_AVAILVOIPCONFIGMETHOD    1    // OMCI

#define ARCINTERVAL_FOREVER        0xFF

#define IPADDR_SIZE             4
#define MACADDR_SIZE            6
#define V6_IPADDR_SIZE          (16)


typedef enum
{
    ATTRIB_BYTE, ATTRIB_SHORT, ATTRIB_LONG, ATTRIB_ARRAY, ATTRIB_DOUBLELONG
} ATTRIBTYPE;


typedef struct 
{
    UINT8      mask;
    short      size;
    ATTRIBTYPE type;
    int        tableRecordSize;
    char       *name;
    UINT32     apm_avc_type;
} AttribDefs_S;


// Used in Get/MIB upload
typedef struct 
{
    UINT8      attribMask;
    char       *pAttribValue;
    short      attrib_size;
    ATTRIBTYPE attribType;
} AttribData_S;


typedef enum
{
    OMCI_ONU_TYPE_SFU,
    OMCI_ONU_TYPE_HGU,
    OMCI_ONU_TYPE_SBU,
    OMCI_ONU_TYPE_CBU,
    OMCI_ONU_TYPE_MDU,
    OMCI_ONU_TYPE_MTU,
    OMCI_ONU_TYPE_1FE_1_POTS_SFU,
    OMCI_ONU_TYPE_MIXED_HGU
} OMCI_ONU_TYPE_E;


#define EXTRACT_PBITS(tci)            (((tci) >> 13) & 0x7)
#define EXTRACT_CFI(tci)              (((tci) >> 12) & 0x1)
#define EXTRACT_VLANID(tci)           ((tci)  & 0xFFF)
#define MAKE_TCI(p,c,v)               ((((p) & 0x7) << 13) | (((c) & 0x1) << 12) | ((v) & 0xFFF))


//########################################################################
//########################################################################
//##########    CASTING FOR ENTITY FIELD IN CELL 
//##########    BPON is UINT8     GPON is UINT16
//########################################################################
//########################################################################
#define MECLASS_CAST           UINT16


#endif
