/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciEvents.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file contains OMCI events definitions, prototypes    **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *    22Feb04     zeev  - initial version created.                            *
 *                                                                          
 *                                                                     
 ******************************************************************************/


#ifndef __INCOmciEventsh
#define __INCOmciEventsh

#include <signal.h>
#include <stdbool.h>

//
//  Managed Entity Identifiers
//
typedef enum
{
    EVMSG_PMREPORT=1,   EVMSG_ALARMREPORT,   EVMSG_TCAREPORT,    EVMSG_FLASHWRITECOMPLETION,
    EVMSG_CLIOUTPUT,    EVMSG_TIMEREXPIRY,   EVMSG_VOIPREADY,    EVMSG_OMCIREADY,
    EVMSG_POTSTEST,     EVMSG_ANIGTEST,      EVMSG_AVCREPORT
} EVENTMSGTYPE;


typedef enum
{
    CLI_FULLENTITYDB=1,   CLI_FULLSERVICEDB,             CLI_ENTITY,            CLI_SERVICE, 
    CLI_bridgeSummary,    CLI_briefSwDownloadProgress,   CLI_configFlags,       CLI_debugFlags,              
    CLI_flows,            CLI_l2db,                      CLI_osresources,       CLI_print_me_class_info,     
    CLI_profileCache,     CLI_statistics,                CLI_supportedMEs,      CLI_swDownloadProgress,
    CLI_cust_rule_map,    CLI_gponInfo,
} CLIOUTPUTTYPE;

typedef enum
{
    CLIDEBUG_allDebug,           CLIDEBUG_answerOkToAll,        CLIDEBUG_briefFrameDump,    
    CLIDEBUG_createSetDump,      CLIDEBUG_downloadProgress,     CLIDEBUG_framedump,
    CLIDEBUG_memoryUse,          CLIDEBUG_mibUploadNext,        CLIDEBUG_omciTlDisplay,
    CLIDEBUG_outOfOrderMeCreate, CLIDEBUG_zeroMapperIwTpPtr,  
} CLIDEBUGFLAG;


//------------------------------------------------------------------------------

//---------------------    Event message content layout   -----------------------------




// messageContents - PM Report event message
#define MAX_COUNTERS      16
typedef struct 
{
    UINT32 counter[MAX_COUNTERS];
} PmReport_S;

typedef enum
{
    ALARMSTATE_CLEARED=0, ALARMSTATE_ASSERTED
} ALARMSTATE;

// messageContents - Alarm Report event message
#define ALARM_BITARRAY_MASK         0x7
typedef struct 
{
    UINT32     alarmId;         /*alarm index in omci me*/
    UINT32     alarmState;
} AlarmReport_S;


// messageContents - TCA Report event message
#if 1
typedef struct
{
    UINT8 bitMask[2];
} TcaReport_S;
#else
typedef struct 
{
    UINT32     tcaId;         /*alarm index in omci me*/
    UINT32     tcaState;
} TcaReport_S;
#endif


// messageContents - Flash write completion message
typedef struct 
{
    bool  result;
    char  version[VERSION_LEN];
} FlashWriteComplEvent_S;


// messageContents - CLI output message
typedef struct
{ 
    CLIDEBUGFLAG debugFlag;
    bool         state;
} CliDebugFlagControl_S;

typedef struct 
{
    CLIOUTPUTTYPE cliOutput;
    union
    {
        CliDebugFlagControl_S cliDebugFlagControl;
    } u;
} CliOutputEvent_S;


// messageContents - CLI output message
typedef struct 
{
    UINT16 nothing;
} TimerExpiryEvent_S;


// messageContents - VoIP Ready message
typedef struct 
{
    UINT16 nothing;
} VoIPReadyEvent_S;


// messageContents - VoIP Ready message
typedef struct 
{
    UINT32 omcc;
} OmciReadyEvent_S;


// messageContents - single POTS Test result message
typedef struct 
{
    TestPotsUniResultsResp_s  testPotsUniResultsResp;
    bool                      testPassed;
    bool                      lastTest;
} PotsTest_S;

// messageContents - single POTS Test result message
typedef struct 
{
    TestAnigResultsResp_s     testAniGResultsResp;
    bool                      testPassed;
    bool                      lastTest;
} AnigTest_S;

typedef struct 
{
    /*UINT32      avcId;*/       /*avc index in omci me*/
    UINT8       msbAttribMask;
    UINT8       lsbAttribMask;
    UINT16      size;        
    UINT8       value[OMCI_AVC_VALUE_LENGTH];
} AVCReport_S;

//---------------------    Event Layout   -----------------------------

// Event message 
typedef struct
{
    UINT8     eventMessageType;
    MECLASS   meId;
    UINT32    meInstance;
    union
    {
        UINT8                  eventContents[64];    // Assume that PM has largest size for union (16 UINT32 counters
        PmReport_S             pmReport;
        AlarmReport_S          alarmReport;
        TcaReport_S            tcaReport;
        FlashWriteComplEvent_S flashWriteComplEvent;
        CliOutputEvent_S       cliOutputEvent;
        VoIPReadyEvent_S       voipReadyEvent;
        OmciReadyEvent_S       omciReadyEvent;
        PotsTest_S             potsTest;
        AnigTest_S             anigTest;
        AVCReport_S            avcReport;
    } u;
    struct timeval enqueuedTime;         
    struct timeval startExecutionTime;
    struct timeval endExecutionTime;  
} OmciEvent_S;


OMCIPROCSTATUS eventProcessing(OmciEvent_S *pevent);

// This function is registered for when OMCC channel is up
extern void sendOmciReadyEvent();

// This function is registered for when POTS test complete event
extern void sendPotsTestCompleteEvent(int potsPortId,   TestPotsUniResultsResp_s *p_TestPotsUniResultsResp,
                                      bool testPassed,  bool lastTest);

// This function is registered for when POTS test complete event
extern void sendAnigTestCompleteEvent(void);
    
// This function is registered for APM event delivery
/*extern bool omciApmEventRcvFunction(ApmReportMsg_S *papmev);*/

// Used by Flash write to notify completion status
void omciFlashWriteCompletion(char swBank, bool completionStatus, char *version);

// Used when timer set for entity expires
void omciTimerExpiryEntity(int sigNum, siginfo_t *info, void *context);

// Used received notification on a new ONU ID
void omciSetOMCCPort(void);

#endif
