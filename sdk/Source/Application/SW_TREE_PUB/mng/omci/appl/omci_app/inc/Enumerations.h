/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : Enumerations.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file has the mapped string/enum lookup routines      **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *     2Mar04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/


#ifndef __INCEnumerationsh
#define __INCEnumerationsh

typedef struct
{
    int  enumValue;
    char *enumStr;
} MapEntry;


typedef struct
{
    int      numEntries;
    MapEntry *mapEntryAra;
} EnumMap;


extern char *enumStrLookup(EnumMap *penummap, int enumValue);

extern char *adminStateLookup(int enumValue);
extern char *operStatusLookup(int enumValue);
extern char *lanFcsIndLookup(int enumValue);
extern char *encapsMethodLookup(int enumValue);
extern char *bridgedOrIpIndLookup(int enumValue);
extern char *cesLoopbackLookup(int enumValue);
extern char *cesFramingLookup(int enumValue);
extern char *ds1ModeLookup(int enumValue);
extern char *cesEncodingLookup(int enumValue);
extern char *cardIfTypeLookup(int enumValue);
extern char *iwOptionLookup(int enumValue);
extern char *omciBooleanLookup(int enumValue);
extern char *arcLookup(int enumValue);
extern char *tpTypeLookup(int enumValue);

extern char *swImageCommitLookup(int enumValue);
extern char *swImageActiveLookup(int enumValue);
extern char *swImageValidLookup(int enumValue);

extern char *ethernetConfigurationLookup(int enumValue);

extern char *impedanceLookup(int enumValue);
extern char *transmissionPathLookup(int enumValue);
extern char *pcmEncTypeLookup(int enumValue);
extern char *tcontGModeLookup(int enumValue);
extern char *ont2GModeLookup(int enumValue);
extern char *omciCapabilityLookup(int enumValue);
extern char *tcontgPolicyLookup(int enumValue);

extern char *omciCmdLookup(int enumValue);
extern char *respCellResultLookup(int enumValue);
extern char *hookStateLookup(int enumValue);
extern char *regYesNoLookup(int enumValue);

extern char *castTypeLookup(int enumValue);

extern char *tagOpClassLookup(int enumValue);

extern char *mapperTpTypeLookup(int enumValue);
extern char *vlanTagAssocTypeLookup(int enumValue);
extern char *vlanTagUpstreamOpModeLookup(int enumValue);
extern char *vlanTagDownstreamOpModeLookup(int enumValue);
extern char *forwardOperationLookup(int enumValue);
extern char *taggedForwardModeLookup(int enumValue);
extern char *gemportDirectionLookup(int enumValue);

extern char *eventMsgTypeLookup(int enumValue);

extern char *vlanModActionLookup(int enumValue);
extern char *igmpUsTagMatchLookup(int enumValue);
extern char *mngtCapabilityLookup(int enumValue);

extern char *onuTypeLookup(int enumValue);

#endif
