/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciEntityDb.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file contains implementation of OMCI MIB DB          **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *    22Feb04     zeev  - initial version created.                            *
 *                                                                          
 *                                                                     
 ******************************************************************************/

#ifndef __INCOmciEntityDb
#define __INCOmciEntityDb


#include <stdio.h>

#include "ListUtils.h"
#include "OmciEvents.h"


#define MAX_ALARM_NUM 32


//
//  Managed Entity Structures
//

typedef struct 
{
    OMNODE    node;
    MECLASS   meClass;
    UINT16    meInstance;
} MeInst_S;

typedef struct 
{
    MeInst_S meInst;
    UINT8    alarmsBitmap[OMCI_ALARM_BIT_MAP_LEN];    
} AlarmMeInst_S;

typedef struct 
{
    MeInst_S    meInst;
    UINT8       tcaBitMask[OMCI_TCA_BIT_MAP_LEN];
    UINT8       intervalEndTime;
    /*UINT16      thresholdDataId;*/
} PmMeInst_S;

#if 0
typedef struct 
{
    MeInst_S           meInst;
    UINT8              tcaBitMask[OMCI_TCA_BIT_MAP_LEN];
    UINT8              intervalEndTime;
    ControlBlockPm_S   controlBlock;
} ExtendedPmMeInst_S;
#endif

    


typedef struct
{
  UINT16 attribMask;
  long   value;
} MaskValue_S;


typedef struct
{
    MaskValue_S msbAttribMV[8];
    MaskValue_S lsbAttribMV[8];
} MatchAttribAras_S;



typedef struct 
{
    UINT8 msbAttribMask;
    UINT8 lsbAttribMask;
} MaskPair_S;


// Command handler - ME and method
typedef struct 
{
    int            mtCmd;
    OMCIPROCSTATUS (*CmdHandler)(OmciCell_S *);
} CmdHandler_S;


// Command DB: handlers, msb/lsb get/set attribute masks
typedef struct 
{
    CmdHandler_S *cmdHandlerAra;
    int          cmdHandlerAra_length;
    UINT8        msbGetAttribs;
    UINT8        lsbGetAttribs;
    UINT8        msbSetAttribs;
    UINT8        lsbSetAttribs;
    UINT8        msbCreateAttribs;
    UINT8        lsbCreateAttribs;
} MeCommandDb_S;

typedef struct
{
    int                numMsbAttribs;
    AttribDefs_S       *pMsbAttribDefs;
    int                numLsbAttribs;
    AttribDefs_S       *pLsbAttribDefs;
} MeAttribInfo_S;

typedef struct{
    UINT8               meAlarmIndex; /*alarm/tca index in omci me*/
    UINT32              apmAlarmTypes; /*alarm types in apm*/
} MeAlarmMap_S;

typedef struct{
    UINT32              apmPmType;
    UINT8               alarmNum;           /*there are how many alarms/tcas */
    MeAlarmMap_S        *alarmMap;          /*an array of MeAlarmMap_S */
}MeApmMapTable_S;


// ME Class - Id, name, methods for display, make snapshot for alsrms and MIB, APM handlers
// generic, table driven approach to ME handling 
typedef struct MngEntityDb_tag
{
    int            meClassId;
    char           *meClassName;
    OMLIST         *mngEntityList;
    MeCommandDb_S  *pMeCommandDb;
    MeApmMapTable_S *meAlarmPmMapTable;
    void           (*entityDestructor)(MeInst_S *pmeinst);
    bool           (*entityMatch)(MeInst_S *pmeinst, MatchAttribAras_S *pma);
    int            (*getEntity_MSBbAttribData)(MeInst_S *pmeinst, AttribData_S *pAttribData);
    int            (*getEntity_LSBbAttribData)(MeInst_S *pmeinst, AttribData_S *pAttribData);
    void           (*displayEntityDetails)(MeInst_S *pmeinst);
    OMCIPROCSTATUS (*makeEntitySnapshotForMibUpload)(OmciCell_S *pSnapCell,        int seqNum,  
                                                     MeInst_S *pmeinst,            MECLASS mngEntity,
                                                     AttribData_S *pMsbAttribData, int numMsbAttribs,
                                                     AttribData_S *pLsbAttribData, int numLsbAttribs);
    OMCIPROCSTATUS (*apmReportHandler)(MeInst_S *pmeinst, OmciEvent_S *pevent); 
    OMCIPROCSTATUS (*makeSnapshotForAlarmUpload)(OmciCell_S *pSnapCell, MeInst_S *pmeinst, UINT8 arcIgnoreFlag);
    OMCIPROCSTATUS (*updateTcParams)(MeInst_S *pmeinst, UINT16 thresholdMeInstance, ThresholdCrossingApmParams_s *ptcparams);
    OMCIPROCSTATUS (*timerExpiryHandler)(MeInst_S * pmeinst); 
    void           (*updateIntervalEndTime)(MeInst_S *pmeinst, UINT8 intervalEndTime);
    void           (*getMeAttribDefs)(MeAttribInfo_S *pMeAttribInfo);
} MeClassDef_S;


typedef struct 
{
    MECLASS meClass;
    UINT8   bitmask;
} ThresholdDataClassBitmask_S;



bool     isOmciPmMeClass(MECLASS meClass);
bool     isOmciTcaMeClass(MECLASS meClass);
bool     isOmciAlarmMeClass(MECLASS meClass);
bool     isOmciAvcMeClass(MECLASS meClass);

bool     doesOmciMeInstExist(MECLASS meClass, UINT16 meInstance);
bool     addOmciMeInstToDb(MECLASS meClass, MeInst_S *pmeinst);
void     removeOmciMeInstFromDb(MECLASS meClass, UINT16 meInstance);
MeInst_S *lookupOmciMeInst(MECLASS meClass, UINT16 meInstance);
void     purgeOmciMeDb();
void     initOmciMeDb();
MeInst_S *findMatchingOmciMeInst(MECLASS meClass, MatchAttribAras_S *pma);
MeInst_S *findNextMatchingOmciMeInst(MeInst_S *pmeinst, MatchAttribAras_S *pma);
void     initMatchAttribAras(MatchAttribAras_S *pma );
void     dumpOmciMeDb(FILE *fildes);
void     displayOmciMeInst(UINT32 meClass, UINT32 meInstance);

MeInst_S *findFirstOmciMeInst();
MeInst_S *findNextOmciMeInst(MeInst_S *pcurrmeinst);
MeInst_S *findFirstOmciMeInstByMeClass(MECLASS meClass);
MeInst_S *findNextOmciMeInstByMeClass(MeInst_S *pcurrmeinst);
MeInst_S *findFirstPmOmciMeInst();
MeInst_S *findNextPmOmciMeInst(MeInst_S *pcurrmeinst);
MeInst_S *findFirstAlarmOmciMeInst();
MeInst_S *findNextAlarmOmciMeInst(MeInst_S *pcurrmeinst);

int      getPmOmciMeClassAra(ThresholdDataClassBitmask_S **classBmPairArray);
bool     verifyAttributeLengthForSetCmd(int        maxSizeSetAttribs, AttribDefs_S *p_msbAttribDefs, int numMsbAttribs, UINT8 msbSetMask,
					AttribDefs_S *p_lsbAttribDefs,  int numLsbAttribs,           UINT8 lsbSetMask);
void     dumpHex(UINT8 *ara, int araSize);

// ME Class iteration
MeClassDef_S *getFirstOmciMeClassRef();
MeClassDef_S *getNextOmciMeClassRef(MECLASS meClass);


MeClassDef_S  *getOmciMeClassRef(MECLASS meClass);
MeCommandDb_S *getMeCommandDbRef(MECLASS meClass);


OMCIPROCSTATUS timerExpiryHandler(MeInst_S *pmeinst, OmciEvent_S *pevent); 
OMCIPROCSTATUS apmReportHandler(MeInst_S *pmeinst, OmciEvent_S *pevent); 

OMCIPROCSTATUS initApmClassDb();
OMCIPROCSTATUS updateApmTcBlock(MECLASS meClassId, UINT16 meClassInstance, ThresholdCrossingApmParams_s *tcApmParams);

void           updateThresholdsPmTcEntities(UINT16 meInstance);
bool           isThresholdDataIdValid(UINT16 thresholdDataId);
OMCIPROCSTATUS setApmPMThresholds(UINT16 meClass, UINT16 meInstance, UINT16 thresholdDataId);


// Generic OMCI command handlers

OMCIPROCSTATUS genericGetME(OmciCell_S *pcell);
OMCIPROCSTATUS genericDeleteME(OmciCell_S *pcell);


// ME class attribute info display
extern void displayMeAttributeDefs(int meClass, FILE *fildes);
extern void displaySupportedMes(FILE *fildes);

extern OMCIPROCSTATUS omci_apm_alarm_report_handler(MeInst_S *pmeinst, OmciEvent_S *pevent);
extern OMCIPROCSTATUS omci_apm_avc_report_handler(MeInst_S *pmeinst, OmciEvent_S *pevent);
extern OMCIPROCSTATUS omci_apm_report_handler(MeInst_S *pmeinst, OmciEvent_S *pevent);
extern void omci_apm_synchronize();
extern void omci_apm_global_clear(MeInst_S *pmeinst);
extern bool omci_apm_is_referenced_by_extended_pm(UINT16 meClass, UINT16 meInstance, char *buf);
#endif
