/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciStartup.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file is the OMCI startup include file                **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Feb04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/


#ifndef __INCOmciStartuph
#define __INCOmciStartuph

#include "OsGlueLayer.h"

/* Defines
------------------------------------------------------------------------------*/
// Queue size must be able to take in maximum number of
// download section cells (these cells are unacknowledged -
// queue size must be greater than number of cells we
// are prepared to buffer. We accept 256 such cells
#define MAX_OMCIMSGQ_ENTRIES           288
#define MAX_EVENTMSGQ_ENTRIES          200

// The largest message size for the OMCI queue 
// Gretaer than or equal to max (sizeof(TimedOmciCell_S), sizeof(OmciEvent_S))
#define OMCIMSGSIZE_FRAME_EVENT        120


#define ONT_OMCI_HIGHLOW_STACK_SIZE         (0x2800) /* 10Kbytes */
#define ONT_OMCI_RXFRAME_STACK_SIZE         (0x2800) /* 10Kbytes */


/* Typedefs
------------------------------------------------------------------------------*/



/* Global variables
------------------------------------------------------------------------------*/
#define OMCI_MSGQ             "/omciQ"

extern void initMsgQRefForCliTask  ();
extern void initMsgQRefForApmTask  ();
extern void initMsgQRefForPonTask  ();
extern void initMsgQRefForFlashTask();
extern void initMsgQRefForVoIPTask ();
extern void initMsgQRefForPotsTask ();
extern void initMsgQRefForTimerTask();
extern void initMsgQRefForFrameRx  ();

#endif
