/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciGlobalData.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : This file contains OMCI startup and top level processing  **/
/**                                                                          **/
/******************************************************************************/
/**                                                                           */
/**  MODIFICATION HISTORY:                                                    */
/**                                                                           */
/**                                                                           */
/******************************************************************************/

#ifndef __INCOmciGlobalDatah
#define __INCOmciGlobalDatah


/* Typedefs
------------------------------------------------------------------------------*/


#include "OsGlueLayer.h"

/* OMCI resources */
typedef struct
{
    GL_TASK_ID      omciTaskId;          /* OMCI - task Id */  
    GL_QUEUE_ID     omciMsgQueueId;      /* OMCI - msg queue Id */  
    GL_QUEUE_ID     refProcessMsgQId;    /* OMCI processing: Reference to OMCI queue = OMCI task */  
    GL_QUEUE_ID     refCliMsgQId;        /* CLI:   Reference to OMCI queue = CLI task */  
    GL_QUEUE_ID     refApmMsgQId;        /* Event: Reference to OMCI queue = APM task */  
    GL_QUEUE_ID     refFlashMsgQId;      /* Event: Reference to OMCI queue = flask task */  
    GL_QUEUE_ID     refPonMsgQId;        /* Event: Reference to OMCI queue = PON task */  
    GL_QUEUE_ID     refVoIPMsgQId;       /* Event: Reference to OMCI queue = VoIP task */  
    GL_QUEUE_ID     refPotsMsgQId;       /* Event: Reference to OMCI queue = POTS task */  
    GL_QUEUE_ID     refTimerMsgQId;      /* Event: Reference to OMCI queue = timer task */  
    GL_QUEUE_ID     refFrameRxMsgQId;    /* OMCI Frame Rx: Reference to OMCI queue = timer task */  
    GL_QUEUE_ID     refOmciThreadFlashMsgQId; /* Reference OMCI thread to Flash process queue */  

    GL_TASK_ID      omciRxFrameTaskId;   /* OMCI - Rx Frame pump task Id */  
}OmciResources_S;


typedef struct
{
    struct
    {
        UINT32 totalFrames;
        UINT32 queuedFrames;
        UINT32 unqueuedFrames;
        UINT32 readFailures;
        UINT32 invalidRxCallBackFunc;
    } rxFrame;

    struct
    {
        UINT32 totalFrames;
        UINT32 responseFrames;
        UINT32 alarmAvcFrames;
        UINT32 transmitFailures;
    } txFrame;

    struct
    {
        UINT32 totalEvents;
        UINT32 unqueuedEvents;
        UINT32 queuedEvents;
        UINT32 pmEvents;
        UINT32 tcaEvents;
        UINT32 alarmEvents;
        UINT32 avcEvents;
        UINT32 flashWriteEvents;
        UINT32 cliEvents;
    } events;

    struct
    {
        UINT32 msgsDequeued;
        UINT32 dequeueFailures;
        UINT32 vitalityCounter;
        UINT32 duplicateFrames;
        UINT32 invalidDbAkBits;
        UINT32 invalidDeviceId;
    } omciThread;

    struct
    {
        UINT32 allocs;
        UINT32 allocFailures;
        UINT32 frees;
    } mem;

    struct
    {
        UINT32 allocs;
        UINT32 allocFailures;
        UINT32 frees;
    } l2da;

    struct
    {
        UINT32 txOKs;
        UINT32 txErrors;
    } flashMsg;
} OmciStatistics_S;


typedef struct
{
    bool celldumpFlag;
    bool memoryUsageFlag;
    bool serviceControlFlag;
    bool mibUploadNextFlag;
    bool downloadProgressFlag;
    bool briefCellFlag;
    bool createSetCellDumpFlag;
    bool omciTlDisplay;
    bool noHwCallsFlag;
    bool answerOkToAll;
    bool outOfOrderMeCreate;
    bool zeroMapperIwTpPtr;
} OmciDebug_S;


/* Global variables
------------------------------------------------------------------------------*/

extern char* get_OMCI_sw_version(void);

// Zeroes the Alarm Sequence Number
extern void resetAlarmSequenceNumber ();

// Get the next Alarm Sequence Number
extern UINT8 getNextAlarmSequenceNumber ();

// Get and Set the PM sync time
extern UINT32 getPmSyncGong ();
extern void   setPmSyncGong (UINT32 gongTime);

// Return pointer to globals
extern OmciStatistics_S *getOmciGlobalStatistics();
extern OmciResources_S  *getOmciResources();
extern OmciDebug_S      *getOmciDebug();

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//---------------------                Debug flags             -----------------
//------------------------------------------------------------------------------
// Config cell dump flag
void configCelldump (bool celldumpFlag);

// Config create/delete frame dump flag
void configCreateSetCellDump (bool celldumpFlag);

// Config brief frame flag
void configBriefCell (bool briefCellFlag);

// Config memory usage flag
void configMemoryUsage (bool memoryUsageFlag);
 
// Config service control flag
void configServiceControl (bool serviceControlFlag);
 
// Config Mib upload next flag
void configMibUploadNext (bool mibUploadNextFlag);

// Config download progress flag
void configDownloadProgress (bool downloadProgressFlag);

// Config hardware calls flag
void configNoHwCalls (bool noHwCalls);
bool getNoHwCallsFlag ();

// Config Pass unsupported MEs flag
void configAnswerOkToAll (bool answerOkToAll);
bool getAnswerOkToAllFlag ();

// Config the out of order ME create flag - e.g. MBPCD before mapper
void configOutOfOrderMeCreate (bool outOfOrderMeCreate);
bool getOutOfOrderMeCreateFlag ();

// Config the 802.1p mapper uses 0 as NULL/default/clean IwTpPtr value.
void configZeroMapperIwTpPtr (bool zeroMapperIwTpPtr);
bool getZeroMapperIwTpPtrFlag ();

// Config the TPM API call display 
void configOmciTlDisplay (bool omciTlDisplay);
bool getOmciTlDisplayFlag ();

// Config all debug flags
void configAllDebug (bool allFlag);

// Init all global data
void initOmciGlobalData();

#endif

