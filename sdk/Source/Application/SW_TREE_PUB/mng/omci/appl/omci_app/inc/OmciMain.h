/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciMain.h                                                **/
/**                                                                          **/
/**  DESCRIPTION : This file is the MAIN OMCI include file                   **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Feb04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/


#ifndef __INCOmciMainh
#define __INCOmciMainh

#include "OsGlueLayer.h"
#include "OmciEvents.h"

typedef enum
{
    MSGQPRIORITY_EVENT= 0, MSGQPRIORITY_LOFRAME, MSGQPRIORITY_HIFRAME
} MSGQPRIORITY;


// Used for timing OMCI execution
typedef struct
{
    struct timeval  enqueuedTime;       // Copied from frame, else overwriiten when transmission takes place    
    struct timeval  startExecutionTime;
    struct timeval  endExecutionTime;  
    struct timeval  endTxTime;         
} FrameTiming_S;

// Return pointer to frame timing 
extern FrameTiming_S *getFrameTimingPtr();

// Used in TestCell simulation - make it visible
void omciFrameRcvFunction(OmciCell_S *pcell, GL_QUEUE_ID omciMsgQueueId);

// Used by OMCI and others to report CLI, Flash write completion
bool omciEventRcvFunction(OmciEvent_S *pevent, GL_QUEUE_ID omciMsgQueueId);

// Transmit OMCI cell
void dealWithOmciFrameTransmission(OmciCell_S *pcell);

// OMCI frame and event processing
void processOmciFramesAndEvents();

// OMCI common frame handler
void commonFrameProcessing(OmciCell_S *pcell);


#endif
