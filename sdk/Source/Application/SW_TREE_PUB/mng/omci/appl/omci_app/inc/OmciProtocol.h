/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciProtocol.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file contains OMCI protocol method prototypes        **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Feb04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/


#ifndef __INCOmciProtocolh
#define __INCOmciProtocolh


// Common attribute mask array for Get/Set commands
extern UINT8 attribMaskAra[];
extern int ATTRIBMASKARA_LEN;



/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
// 
//      Routines for handling lst transmitted buffers
// 
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

void initLastTransmittedOmciFrames();

// Last high/low priority cells transmitted upstream
void storeFrameInLastTransmittedBuffer(OmciCell_S *pcell);



/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
// 
//      Routines for setting normal and error completion fields of response frame
// 
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

void setCellMessageContentsForError_General  (OmciCell_S *pcell, MSGCOMPLCODE complCode);

void setCellMessageContentsForSuccess_General(OmciCell_S *pcell);

void setCellMessageContentsForSuccess_GetCmd (OmciCell_S *pcell, int firstPaddingIndex);

void setCellMessageContentsForAttribError_GetCmd(OmciCell_S *pcell, int firstPaddingIndex,
                                                 UINT8 msbUnsupportedAttribMask, UINT8 lsbUnsupportedAttribMask,
                                                 UINT8 msbFailedAttribMask,      UINT8 lsbFailedAttribMask);

void setCellMessageContentsForAttribError_SetCmd(OmciCell_S *pcell,
                                                 UINT8 msbUnsupportedAttribMask, UINT8 lsbUnsupportedAttribMask,
                                                 UINT8 msbFailedAttribMask,      UINT8 lsbFailedAttribMask);

void setCellMessageContentsForAttribError_CreateCmd(OmciCell_S *pcell,UINT8 msbAttribExecMask, UINT8 lsbAttribExecMask);

void setCellMessageContentsForSuccess_MibUploadResponse(OmciCell_S *pcell);

void setCellMessageContentsForError_MibUploadAndAlarmNextResponse(OmciCell_S *pcell);
void setCellMessageContentsForSuccess_MibUploadNextResponse      (OmciCell_S *pcell, int firstPaddingIndex);


void setCellMessageContentsForSuccess_StartSoftwareDownloadResponse(OmciCell_S *pcell, UINT8 windowSizeM1);
void setCellMessageContentsForError_StartSoftwareDownloadResponse  (OmciCell_S *pcell, UINT8 result);

void setCellMessageContentsForSuccess_DownloadSectionResponse(OmciCell_S *pcell);
void setCellMessageContentsForError_DownloadSectionResponse  (OmciCell_S *pcell, UINT8 result);

void setCellMessageContentsForSuccess_EndSoftwareDownloadResponse(OmciCell_S *pcell);
void setCellMessageContentsForError_EndSoftwareDownloadResponse  (OmciCell_S *pcell, UINT8 result);


void setCellMessageContentsForSuccess_GetNext(OmciCell_S *pcell, UINT8 msbAttribMask, UINT8 lsbAttribMask);

void setCellMessageContents_GetAllAlarmsResponse(OmciCell_S *pcell);

void IncomingFrameHandler(OmciCell_S *pcell);

OMCIPROCSTATUS commonGetResponse(OmciCell_S *pcell, 
                                 AttribData_S *pMsbAttribData, int numMSBAttribs, 
                                 AttribData_S *pLsbAttribData, int numLSBAttribs);


/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
// 
//      Routines for transmitting alarm, AVC, test result messages
// 
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

void alarmNotificationCellTransmit(MECLASS meId, UINT16 meInstance, UINT8 *bitMaskAra, int bitMaskAra_len);

void avcNotificationCellTransmit(MECLASS meId, UINT16 meInstance, UINT8  msbAttribMask, UINT8  lsbAttribMask, 
                                 UINT8 *bitMaskAra, int bitMaskAra_len);

void testResultNotificationCellTransmit(OmciCell_S *pcell, MECLASS meId, UINT16 meInstance);


/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
// 
//      Routines for extracting parametric fields of request frames
// 
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

void extractAttribMasksContentLength_SetCmd(OmciCell_S *pcell, UINT8 *msbAttribMask, UINT8 *lsbAttribMask, UINT8 **pAttribData, int *contentLength);
void extractContentLength_CreateCmd        (OmciCell_S *pcell, void **pCreateData, int *contentLength);


#endif
