/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciMemMgr.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file has ME/service memory routine prototypes        **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Feb04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INComciMemMgrh
#define __INComciMemMgrh

void *allocMeMemory(MECLASS  meClass, UINT16 size);
void freeMeMemory  (MECLASS  meClass, void *mePtr);

void *allocServiceMemory(int serviceClass, int size);
void freeServiceMemory  (int serviceClass, void *servicePtr);

#define OMCI_64POOL_SZ      300
#define OMCI_128POOL_SZ     300
#define OMCI_256POOL_SZ     300
#define OMCI_512POOL_SZ     100
#define OMCI_1024POOL_SZ    20
#define OMCI_2048POOL_SZ    2


#endif
