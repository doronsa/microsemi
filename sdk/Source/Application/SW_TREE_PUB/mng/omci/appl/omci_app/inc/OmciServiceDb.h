/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciServiceDb.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the service DB and interface         **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *     2Mar04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/


#ifndef __INCOmciServiceDb
#define __INCOmciServiceDb

#include <stdio.h>

#include "ListUtils.h"


//
//  Managed Service Structures
//

typedef enum 
{
    SERVICETYPE_VOICE=2
} SERVICETYPE;

  
typedef struct
{
    UINT16 meId;
    short  meInstance;
} PK;
  


typedef struct 
{
    OMNODE   node;
    UINT16   servType;
    UINT32   servInstance;
} NodeAndKey;


typedef struct 
{
    NodeAndKey nodekey;
    PK         iwEntity;
} ServiceItem_S;


// Service DB item
typedef struct
{
    SERVICETYPE        serviceType;
    char               *serviceName;
    OMLIST             *serviceItemList;
    void               (*serviceItemDestructor)(NodeAndKey *pnodekey);
    void               (*displayServiceDetails)(NodeAndKey *pnodekey);
} ServiceDb;


#define SERVINSTANCE(p) (UINT32)(((((MeInst_S *)p)->meClass) << 16) | ((((MeInst_S *)p)->meInstance)))
  
bool doesServiceInstanceExist    (SERVICETYPE serviceTypeId, UINT32 servInstance);
bool addServiceInstanceToDb      (SERVICETYPE serviceTypeId, NodeAndKey *pnodekey);
void removeServiceInstanceFromDb (SERVICETYPE serviceTypeId, UINT32 servInstance);
NodeAndKey *lookupServiceItem    (SERVICETYPE serviceTypeId, UINT32 servInstance);
void displayService(UINT32 serviceType, UINT32 serviceInstance);

void initOmciServiceDb();
void purgeOmciServiceDb();
void dumpOmciServiceDb(FILE *fildes);

NodeAndKey *findFirstServiceItemByServiceType(UINT32 serviceType);
NodeAndKey *findNextServiceItemByServiceType(NodeAndKey *pnodekey);

#endif
