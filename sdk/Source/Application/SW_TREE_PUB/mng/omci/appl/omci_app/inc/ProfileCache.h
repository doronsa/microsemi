/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        :  ProfileCache.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : This file contains Profile Cache definitions              **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    25Dec07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCProfileCacheh
#define __INCProfileCacheh

#include <stdio.h>

// Utility for reading U-Boot environment
#define FW_PRINTENVSTR                   "fw_printenv"
#define FW_SETENVSTR                     "fw_setenv"
#define FALLBACK_ACTIVE_BANK             'A'
#define FALLBACK_COMMITTED_BANK          'A'
#define FALLBACK_ISVALID_A               true
#define FALLBACK_ISVALID_B               false
                                      
#define UBVSTR_COMMITTED                 "committedBank"
#define UBVSTR_ISVALID_A                 "isValidA"
#define UBVSTR_ISVALID_B                 "isValidB"
#define UBVSTR_VERSION_A                 "versionA"
#define UBVSTR_VERSION_B                 "versionB"
#define UBVSTR_ACTIVATION_TEST           "act_test"
#define UBVSTR_ACT_BOOT_COMPLETE         "act_boot_complete"

#define PROFCACHE_BANK_A                 'A'
#define PROFCACHE_BANK_B                 'B'

typedef enum
{
    ENUM_UBV_COMMITTED, ENUM_UBV_ISVALID_A, ENUM_UBV_ISVALID_B, ENUM_UBV_VERSION_A, ENUM_UBV_VERSION_B,
    ENUM_UBV_ACTIVATION_TEST
} ENUM_UBOOTVARS;

typedef enum
{
    ENUM_HFWTYPE, 
    ENUM_SFWTYPE
} ENUM_FWTYPE;

typedef enum
{
    ENUM_UPSTREAM, 
    ENUM_DOWNSTREAM
} ENUM_STREAMDIRECTION;

typedef enum
{
    ENUM_PASS_PACKET,
    ENUM_DISCARD_PACKET 
} ENUM_DEFAULTACTION;

typedef struct
{
    ENUM_STREAMDIRECTION dir;
    ENUM_FWTYPE          fwType;
    UINT8                tcont;
    UINT8                queue;
} VirtualQueue_S;

typedef struct
{
    char  *varName;
    int   varEnum;
} UBootVarParseInfo_S;


// Default values - in case of missing XML parameter file
#define DEFAULT_OMCC_RX_QUEUE_NUM   (6)
#define DEFAULT_OMCC_TX_QUEUE_NUM   (6)
#define DEFAULT_OMCI_SWF_ONLY       (false)

#define DEFAULT_NUMTCONTS           (5)     // OMCI TCONT + 4 data traffic TCONTs
#define DEFAULT_NUMDSQUEUES         (4)
#define DEFAULT_USQUEUESPERTCONT    (4)
#define DEFAULT_FIRSTNONOMCITCONTID (1)     // Usually OMCI TCONT is not reported and other TCONTs are numbered from 1 onward
#define MAX_TCONTS                  (8)
#define MAX_USQUEUESPERTCONT        (8)
                                   
#define DEFAULT_NUMETHERNETPORTS    (4)
#define DEFAULT_NUMPOTSPORTS        (2)
#define DEFAULT_ETHERNETCARDTYPE    (CT_Gig_FE_10_BTx)

#define DEFAULT_ETHERNETSLOTNUMBER  (1)
#define DEFAULT_POTSSLOTNUMBER      (2)
#define DEFAULT_WIFISLOTNUMBER      (6)
#define DEFAULT_VEIPSLOTNUMBER      (14)
#define DEFAULT_ANISLOTNUMBER       (0x80)

#define DEFAULT_ETHERTYPEOMCISOCKET (0xBABA)
#define DEFAULT_OMCIHWCALLSON       (1)

#define DEFAULT_RESETGEMPORTS       (false)
#define DEFAULT_RESETTCONTALLOCID   (true)

#define MAX_SWDOWNLOAD_WINDOW_SIZE  (256)  
#define SAFE_SWDOWNLOAD_WINDOW_SIZE (32)

typedef struct
{
    bool celldumpFlag;
    bool memoryUsageFlag;
    bool serviceControlFlag;
    bool mibUploadNextFlag;
    bool downloadProgressFlag;
    bool briefCellFlag;
    bool createSetCellDumpFlag;
    bool omciTlDisplay;
} ConsoleOutputFlags_S;

typedef struct
{
  bool    valid;
  UINT32  queueNum; 
} QueueInfo_S;

typedef struct
{
  bool         valid;
  UINT32       virtualQueueNum;
  QueueInfo_S  hwfQMap[MAX_USQUEUESPERTCONT];
  QueueInfo_S  swfQMap[MAX_USQUEUESPERTCONT]; 
} QueueCfg_S;

typedef struct
{
  bool         valid;   
  bool         isHwfQueue[MAX_USQUEUESPERTCONT];
  bool         isSwfQueue[MAX_USQUEUESPERTCONT];
} GmacQueueInfo_S;

typedef struct
{
  UINT32  numTconts;
  UINT32  numUsQueues;
  UINT32  numDsQueues;
  UINT32  tcontsMap;
  UINT32  firstNonOmciTcontId;
} AniTrafficMngtConfig_S;

// These should match the OMCI definitions - try decouple Profilecache from OmciDefs.h
#define INV_VERSION_LEN                (14)
#define INV_SERIALNUMBER_LEN           (8)
#define INV_VENDORID_LEN               (4)
typedef struct
{
    UINT8    serialNumber[INV_SERIALNUMBER_LEN];
    UINT8    version[INV_VERSION_LEN];
    UINT8    vendorId[INV_VENDORID_LEN];
} Inventory_S;


typedef struct
{
    UINT8    ethernet;
    UINT8    pots;
    UINT8    wifi;    
    UINT8    ani;
    UINT8    veip;
} SlotNumbers_S;

typedef struct
{
    bool     isValidA;
    bool     isValidB;
    UINT8    committedBank;
    UINT8    activeBank;
    UINT8    versionA[15];
    UINT8    versionB[15];
    bool     altImageActivationVarSet;
} SwImage_S;


/* CTC authentification length */
#define CTC_AUTH_OPERATORID_LEN                (4)
#define CTC_AUTH_LOID_LEN                      (24)
#define CTC_AUTH_PASSWORD_LEN                  (12)
#define MAX_USQUEUES          (64)
typedef struct
{

    bool                      hwCalls;
    bool                      isCustVlanMod;
    bool                      isMacLearn;
    bool                      isMcFlooding;
    bool                      isUniMtu;    
    UINT32                    defaultAction;
    UINT32                    cpuRxQueue;
    UINT32                    cpuTxQueue;
    bool                      isSwfOnly;
    bool                      batteryBackup;
    int                       numEthernetPorts;
    int                       numPotsPorts;
    int                       ethernetCardType;
    bool                      isVoiceSupported;
    bool                      isVoipSupported;
    bool                      isIpHostDataServiceSupported;
    bool                      isVoipConfigByOmci;
    bool                      isVoipProtocolSip;
    UINT32                    ontType;
    bool                      isNativeHguMode;
    bool                      isStandardVeipNumbering;
    bool                      isEthPortIsolated;
    bool                      isSupportCtc;
    UINT8                     ctcVersion;
    UINT8                     xvrType;    
    bool                      isCapCell;
    bool                      isGponInKernel;
    bool                      gemReset;
    bool                      tcontReset;
    bool                      isMcastServicePackageG988;
    bool                      isMgmtVlanSupport;
    UINT32                    mgmtVlanValue;
    int                       swDownloadWindowSize;    
    
    UINT8                     ctcAuthOperator[CTC_AUTH_OPERATORID_LEN];
    UINT8                     ctcAuthLoid[CTC_AUTH_LOID_LEN];
    UINT8                     ctcAuthPwd[CTC_AUTH_PASSWORD_LEN];
    QueueCfg_S                usQueueCfgAra[MAX_TCONTS];
    QueueCfg_S                dsQueueCfgAra;
    GmacQueueInfo_S           usTpmQueueCfg[MAX_TCONTS];
    GmacQueueInfo_S           dsTpmQueueCfg;
    AniTrafficMngtConfig_S    aniTrafficMngtConfig;
    Inventory_S               inventory;
    SlotNumbers_S             slotNumbers;
    UINT16                    etherTypeOmciSocket;
    SwImage_S                 rawSwImage;
    SwImage_S                 swImage;
    ConsoleOutputFlags_S      consoleOutputFlags;
} ProfileCacheDb_S;


extern bool         profileCache_getCustVlanMod();
extern UINT8        profileCache_setCustVlanMod(bool state);

extern bool         profileCache_getMacLearn();
extern bool         profileCache_getMcFlooding();
extern bool         profileCache_getUniMtuFlag();

extern bool         profileCache_getOmciHwCalls();
extern UINT8        profileCache_getOmccRxQueue();
extern UINT8        profileCache_getOmccTxQueue();
extern bool         profileCache_isSwfOnly();
extern UINT8        profileCache_getEthernetSlotNumber();
extern UINT8        profileCache_getPotsSlotNumber();
extern UINT8        profileCache_getWifiSlotNumber();
extern UINT8        profileCache_getVeipSlotNumber();
extern UINT8        profileCache_getAniSlotNumber();
extern bool         profileCache_haveBatteryBackup();
extern int          profileCache_getNumEthernetPorts();
extern int          profileCache_getNumPotsPorts();
extern int          profileCache_getEthernetCardType();
extern bool         profileCache_isSaveMib();
extern bool         profileCache_isVoiceSupported();
extern bool         profileCache_isVoipSupported();
extern bool         profileCache_isIpHostDataServiceSupported();
extern bool         profileCache_isVoipConfigByOmci();
extern bool         profileCache_isVoipProtocolSip();
extern UINT32       profileCache_getOntType();
extern UINT32       profileCache_getDefaultAction();

extern bool         profileCache_isNativeHguMode();
extern bool         profileCache_isMcastServicePackageG988();
extern bool         profileCache_isStandardVeipNumbering();
extern bool         profileCache_isEthPortIsolated();

extern bool         profileCache_isSupportCtc();

extern bool         profileCache_isCapCell();
extern bool         profileCache_setCapCell(bool state);

extern UINT32       profileCache_getMgmtVlan(bool *isSupport, UINT32 *vid);
extern UINT32       profileCache_getSwDownloadWindowSize();

extern bool         profileCache_isGponInKernel();
extern bool         profileCache_isGemReset();
extern bool         profileCache_isTcontReset();
extern bool         profileCache_isGemPortPmByApmActivation();

extern int          profileCache_getNumTconts();
extern int          profileCache_getNumUsQueues();
extern int          profileCache_getNumDsQueues();

extern UINT32       profileCache_getTcontsMap();
extern UINT32       profileCache_getFirstNonOmciTcontId();
extern QueueCfg_S*  profileCache_getUsQueue();
extern QueueCfg_S*  profileCache_getDsQueue();

extern UINT16       profileCache_GetEtherTypeOmciSocket();

extern char        *profileCache_getOntVersion();
extern char        *profileCache_getOntVendorId();
extern char        *profileCache_getOntSerialNumber();

extern bool         profileCache_getOntBankVersion(UINT8 bank, char *version);

extern bool         profileCache_getCtcCapability(unsigned char *ctcVersion, unsigned char *xvrType);

extern bool         profileCache_getCtcLoidInfo(unsigned char *operatorId, unsigned char *loid, unsigned char *pwd);

extern UINT8        profileCache_getCommittedSoftwareImage();
extern UINT8        profileCache_getActiveSoftwareImage();
extern bool         profileCache_isSoftwareImageValid(UINT8 bank);
extern UINT8       *profileCache_getSoftwareImageVersion(UINT8 bank);

extern bool         profileCache_getConsoleOutputCelldumpFlag();        
extern bool         profileCache_getConsoleOutputMemoryUsageFlag();     
extern bool         profileCache_getConsoleOutputServiceControlFlag();  
extern bool         profileCache_getConsoleOutputMibUploadNextFlag();   
extern bool         profileCache_getConsoleOutputDownloadProgressFlag();
extern bool         profileCache_getConsoleOutputBriefCellFlag();       
extern bool         profileCache_getConsoleOutputCreateSetCellDumpFlag();
extern bool         profileCache_getConsoleOutputOmciTlDisplay();       

extern OMCIPROCSTATUS adjustTrgQueue(VirtualQueue_S *p_VirtualQueue, UINT8 *p_Queue);


extern OMCIPROCSTATUS initOmciProfileCache();
extern void           printProfileCache(char* name, FILE *fildes);

extern bool         isBoardMC();
extern bool         isBoardKW2();

// XML init file support: elements and attributes
// <OMCI_MIB>
#define ELM_OMCI_MIB                 "OMCI_MIB"
#define ELM_PON_Section              "PON"
#define ELM_PON_GemReset             "PON_gem_reset"
#define ELM_PON_TcontReset           "PON_tcont_reset"

#define ELM_custVlanMod              "custVlanMod"
#define ELM_defaultAction            "defaultAction"

#define ELM_macLearn                 "macLearn"
#define ELM_mcFlooding               "mcFlooding"
#define ELM_uniMtu                   "uniMtu"

#define ELM_voiceSupport             "voiceSupport"
#define ELM_voipSupport              "voipSupport"
#define ELM_ipHostDataSupport        "ipHostDataSupport"
#define ELM_voipProtocolSip          "voipProtocolSip"
#define ELM_voipConfigByOmci         "voipConfigByOmci"
#define ELM_ontType                  "ontType"
#define ELM_nativeHguMode            "nativeHguMode"
#define ELM_mcastServicePackageG988  "mcastServicePackageG988"
#define ELM_standardVeipNumbering    "standardVeipNumbering"
#define ELM_ethPortIsolated          "ethPortIsolated"
#define ELM_omciCellCap              "omciCellCap"

#define ELM_mgmtVlanSupport          "mgmtVlanSupport"
#define ATTR_vid                     "vid"

#define ATTR_enabled                 "enabled"

#define ELM_virtualQueue             "virtualQueue"
#define ELM_gmac                     "gmac"
#define ATTR_portNum                 "v_q_num"
#define ATTR_hwfQMap                 "hwf_q_map"
#define ATTR_swfQMap                 "swf_q_map"

#define ELM_ethernetInfo             "ethernetInfo"
#define ELM_potsInfo                 "potsInfo"
#define ELM_ethernetCardType         "ethernetCardType"

#define ELM_ethernetSlotInfo         "ethernetSlotInfo"
#define ELM_potsSlotInfo             "potsSlotInfo"
#define ELM_wifiSlotInfo             "wifiSlotInfo"
#define ELM_veipSlotInfo             "veipSlotInfo"
#define ELM_aniSlotInfo              "aniSlotInfo"

#define ELM_firstNonOmciTcontInfo    "firstNonOmciTcontInfo"

#define ELM_ctcParameter             "ctcParameter"
#define ATTR_version                 "version"
#define ATTR_xrvType                 "xrvType"

#define ELM_ctcLOIDAuth              "ctcLOIDAuth"
#define ATTR_operator                "operator"
#define ATTR_loid                    "loid"
#define ATTR_password                "password"

#define ATTR_numPorts                "numPorts"
#define ATTR_type                    "type"

#define ATTR_slotNumber              "slotNumber"

#define ATTR_tcontId                 "tcontId"

#define ELM_swDownloadWindowSize     "swDownloadWindowSize"
#define ATTR_windowSize              "windowSize"


// <OMCI>
#define ELM_OMCI                     "OMCI"
#define ELM_OMCI_ETY                 "OMCI_ETY"
#define ELM_OMCI_HW_CALLS            "hwCalls"
#define ELM_OMCI_RX_Q                "cpuRxQueue"
#define ELM_OMCI_TX_Q                "cpuTxQueue"
#define ELM_OMCI_SWF_ONLY            "swForwardingOnly"


// <OMCI_DEBUG>
#define ELM_OMCI_DEBUG               "OMCI_DEBUG"
#define ELM_consoleOutput            "consoleOutput"
#define ATTR_flags                   "flags"

#define ELM_port_init                "port_init"
#define ELM_GMAC_parameters          "tx_module_parameters"
#define ELM_GMAC                     "tx_mod"
#define ELM_queue_map                "queue_map"
#define ELM_queue                    "queue"

// The XLM TCONT numbering via PMAC_0, PMAC_1...
#define XMLPMAC_0                    (2)
#define XMLPMAC_7                    (9)
#define XMLGMAC_0                    (0)
#define XMLGMAC_1                    (1)
#define OWNER_CPU                    (0)
#define OWNER_GMAC0                  (1)
#define OWNER_GMAC1                  (2)
#define OWNER_PON0                   (3)

#define ATTR_owner                   "owner"

#define ATTR_numTconts               "numTconts"
#define ATTR_numDsQueues             "numDsQueues"
#define ATTR_defaultUsQueuesPerTcont "defaultUsQueuesPerTcont"
#define ATTR_id                      "id"
#define ATTR_numUsQueuesThisTcont    "numUsQueuesThisTcont"

#define SOC_MC_STRING                "6601"

#endif
