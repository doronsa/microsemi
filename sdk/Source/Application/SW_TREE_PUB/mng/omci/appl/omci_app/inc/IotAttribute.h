/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : IotAttribute.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file contains methods, structures for IOT attributes **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *     28Mar12     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCIotAttributeh
#define __INCIotAttributeh

#define ELM_OMCI_IOT                 "OMCI_IOT"
#define ELM_me                       "me"
#define ELM_attrib                   "attrib"

#define ATTR_name                    "name"
#define ATTR_instance                "instance"
#define ATTR_enabled                 "enabled"
#define ATTR_value                   "value"



#define MAX_BYTE_ARA_SIZE         (30)
typedef struct
{
    uint8_t bm_msb;
    uint8_t bm_lsb;
    bool    isEnabled;
    union
    {
        uint8_t  byteVal;
        uint16_t shortVal;
        uint32_t longVal;
        uint8_t  byteAraVal[MAX_BYTE_ARA_SIZE];
    } u;
} IotAttribValue_S;

#define MAX_ME_ATTRIBUTES        (16)
typedef struct
{
    uint16_t         meClass;
    uint16_t         meInstance;
    IotAttribValue_S iotAttribValueAra[MAX_ME_ATTRIBUTES];
    int              numAttribs;
} IotMeAttribs_S;

#define MAX_IOT_MES              (20)
typedef struct
{
    int            numMes;
    IotMeAttribs_S iotMeAttribsAra[MAX_IOT_MES];
} IotMeAttribDb_S;


OMCIPROCSTATUS loadIotAttributes();

#endif
