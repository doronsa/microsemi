/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciDebugIfo.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file contains debug display routines                 **/
/**                                                                          **/
/******************************************************************************/
/**                                                                           */
/**  MODIFICATION HISTORY:                                                    */
/**                                                                           */
/**                                                                           */
/******************************************************************************/

#ifndef __INCOmciDebugControlh
#define __INCOmciDebugControlh

#include <stdio.h>
#include "OmciEvents.h"

#define OMCI_CELL_FILE_MAX_LINE (10000)
#define OMCI_FRAME_SCRIPT_FILE "/tmp/omci_cells.vbs"

// Displays OMCI debug flags
extern void showOmciDebug(char* name, FILE *fildes);
extern void showOmciOsResources(char* name, FILE *fildes);

// Frame dump routines
extern void dumpFrameToConsole (char* name, char *directionStr, OmciCell_S *pcell);
extern void briefFrameToConsole(char* name, OmciCell_S *pcell);
extern void briefEventToConsole(char* name, OmciEvent_S *pevent);

// GPON Info
extern void showGponInfo (char* name, FILE *fildes);

// Displays cell statistics
extern void displayCellStatistics(char* name, FILE *fildes);

// Save OMCI frames to a script file
void initOmciCellFile(void);
void saveFrameToFile(OmciCell_S *pcell);

#endif
