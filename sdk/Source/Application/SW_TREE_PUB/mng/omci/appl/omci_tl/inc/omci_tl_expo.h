/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_expo.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file declare OMCI translation layer APIs and define  **/
/**                data structures.                                          **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   15/Aug/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_EXPO_HEADER__
#define __OMCI_TL_EXPO_HEADER__

#include <stdio.h>
#include "tpm_types.h"

/****************************************************************************** 
 *                        Marco and Type Definition                                                                                                                                  
 ******************************************************************************/
#define OMCI_TL_MAX_SERVICE_NUM    8   /* Maximum service number                                 */
#define OMCI_TL_MAX_LAN_NUM        8   /* Maximum LAN side TP number in one service              */
#define OMCI_TL_MAX_WAN_NUM        128 /* Maximum WAN side TP number in one service              */
#define OMCI_TL_MAX_FILTER_TCI_NUM 12  /* Maximum TCI entry number in one VLAN tag filter        */
#define OMCI_TL_MAX_OP_NUM         12  /* Maximum VLAN OP entry number in one VLAN tag operation */
#define OMCI_TL_DSCP_PBIT_MAP_LEN  64  /* Lenght in byte of mapping from DSCP to pbits           */

typedef uint32_t OMCI_TL_LAN_KEY;
typedef uint32_t OMCI_TL_WAN_KEY;

/*Definition of returned value*/
typedef uint32_t  OMCI_TL_STATUS;
#define OMCI_TL_RET_OK       0
#define OMCI_TL_ERR_GENERAL  1


/****************************************************************************** 
 *                        Dummy definition, to be removed in future                                                                                                                                  
 ******************************************************************************/
//#define     TPM_PARSE_VLAN_UNTAG         (0)
//#define     TPM_PARSE_VLAN_SINGLE_TAG    (1)
//#define     TPM_PARSE_VLAN_DOUBLE_TAG    (2)



/****************************************************************************** 
 *                        Data Enum and Structure                                                                                                                                  
 ******************************************************************************/

typedef enum
{
    OMCI_TL_TAGGED_PASS              = 0,
    OMCI_TL_TAGGED_DISCARD           = 1,
    OMCI_TL_TAGGED_POS_PARSE_TCI     = 2,
    OMCI_TL_TAGGED_POS_PARSE_VID     = 3,
    OMCI_TL_TAGGED_POS_PARSE_PRI     = 4,
    OMCI_TL_TAGGED_POS_PARSE_VID_PRI = 5, /* OMCI does not define this filter type, just use for merging LAN ans WAn side VLAN tag filters */ 
    OMCI_TL_TAGGED_NEG_PARSE_TCL     = 6,
    OMCI_TL_TAGGED_NEG_PARSE_VID     = 7,
    OMCI_TL_TAGGED_NEG_PARSE_PRI     = 8,
    OMCI_TL_TAGGED_NEG_PARSE_VID_PRI = 9  /* OMCI does not define this filter type, just use for merging LAN ans WAn side VLAN tag filters */ 
} OMCI_TL_TAGGED_FILTER_MODE_E;

/* OMCI TL VLAN filter structure*/
typedef struct
{
    bool       inUse;                           /* Whether this structure is in use                         */
    bool       untaggedFilter;                  /* ture: pass, false: discard                               */
    uint16_t   numOfTci;
    uint16_t   tci[OMCI_TL_MAX_FILTER_TCI_NUM];     
    uint16_t   numOfPri;                        /* Only used for merging LAN and WAN side VLAN tag filters. */       
    uint16_t   pri[OMCI_TL_MAX_FILTER_TCI_NUM]; /* Only used for merging LAN and WAN side VLAN tag filters. */
    uint16_t   dummy;
    OMCI_TL_TAGGED_FILTER_MODE_E  fwdMode;      /* fwd mode of tagged frames                                */    
} OMCI_TL_VLAN_FILTER_T;

typedef enum
{
    OMCI_TL_PARSE_ETHTYPE_PASS   = 0,
    OMCI_TL_PARSE_ETHTYPE_FILTER = 1
} OMCI_TL_PARSE_ETHTYPE_MODE_E;

typedef enum
{
    OMCI_TL_VLAN_OP_ASIS                               = 0,
    OMCI_TL_VLAN_OP_DISCARD                            = 1,      
    OMCI_TL_VLAN_OP_ADD                                = 2,  
    OMCI_TL_VLAN_OP_ADD_COPY_DSCP                      = 3,
    OMCI_TL_VLAN_OP_ADD_COPY_OUTER_PBIT                = 4,
    OMCI_TL_VLAN_OP_ADD_COPY_INNER_PBIT                = 5,
    OMCI_TL_VLAN_OP_ADD_2_TAGS                         = 6,
    OMCI_TL_VLAN_OP_ADD_2_TAGS_COPY_DSCP               = 7,
    OMCI_TL_VLAN_OP_ADD_2_TAGS_COPY_PBIT               = 8,
    OMCI_TL_VLAN_OP_REM                                = 9,
    OMCI_TL_VLAN_OP_REM_2_TAGS                         = 10,
    OMCI_TL_VLAN_OP_REPLACE                            = 11,
    OMCI_TL_VLAN_OP_REPLACE_VID                        = 12,
    OMCI_TL_VLAN_OP_REPLACE_PBIT                       = 13,
    OMCI_TL_VLAN_OP_REPLACE_INNER_ADD_OUTER            = 14,
    OMCI_TL_VLAN_OP_REPLACE_INNER_ADD_OUTER_COPY_PBIT  = 15,
    OMCI_TL_VLAN_OP_REPLACE_INNER_REM_OUTER            = 16,
    OMCI_TL_VLAN_OP_REPLACE_2TAGS                      = 17,
    OMCI_TL_VLAN_OP_REPLACE_2TAGS_VID                  = 18,
    OMCI_TL_VLAN_OP_SWAP                               = 19
} OMCI_TL_VLAN_OP_MODE_E;

typedef struct
{
    uint32_t   ingressInnerTag;
    uint32_t   ingressInnerMask;
    uint32_t   ingressOuterTag;
    uint32_t   ingressOuterMask;
    uint16_t   ingressEtherType;
    uint16_t   dummy;
    uint32_t   egressInnerTag;
    uint32_t   egressInnerMask;    
    uint32_t   egressOuterTag;
    uint32_t   egressOuterMask;    
    OMCI_TL_PARSE_ETHTYPE_MODE_E ethTypeMode;
    OMCI_TL_VLAN_OP_MODE_E       opMode;
} OMCI_TL_VLAN_OP_CFG_T;

/* Default VLAN tag operation */
typedef struct
{
    uint32_t              inUse;
    OMCI_TL_VLAN_OP_CFG_T defaultVlanOp;
} OMCI_TL_DEFAULT_TAG_OP_T;

/* OMCI TL VLAN tag operation structure*/
typedef struct
{
    bool                       inUse;         /* Whether this structure is in use */
    uint8_t                    numOfUnTaggedOp;    
    uint8_t                    numOfTaggedOp;
    uint8_t                    numOfdoubleTaggedOp;
    OMCI_TL_VLAN_OP_CFG_T      untaggedOp[OMCI_TL_MAX_OP_NUM];
    OMCI_TL_VLAN_OP_CFG_T      taggedOp[OMCI_TL_MAX_OP_NUM];
    OMCI_TL_VLAN_OP_CFG_T      doubleTaggedOp[OMCI_TL_MAX_OP_NUM];
    OMCI_TL_DEFAULT_TAG_OP_T   defaultUnTaggedOp;    
    OMCI_TL_DEFAULT_TAG_OP_T   defaultTaggedOp;
    OMCI_TL_DEFAULT_TAG_OP_T   defaultDoubleTaggedOp;
} OMCI_TL_VLAN_OP_T;

typedef enum
{
    OMCI_TL_LAN_TYPE_ETH    = 0,
    OMCI_TL_LAN_TYPE_VOIP   = 1,
    OMCI_TL_LAN_TYPE_VEIP   = 2,
    OMCI_TL_LAN_TYPE_IGMP   = 3    
} OMCI_TL_LAN_TYPE_E;

#define OMCI_TL_DSCP_PBIT_FROM_8021P_MAPPER    0x0001
#define OMCI_TL_DSCP_PBIT_FROM_EVLAN_TAG_OP    0x0002

/* DSCP Mapping to pbits used by untagged frames */
typedef struct
{
    bool       inUse;
    uint8_t    dscpPbitMap[OMCI_TL_DSCP_PBIT_MAP_LEN];
} OMCI_TL_DSCP_PBIT_MAP_T;

/* OMCI TL layer 2 data service LAN side UNI data structure*/
typedef struct
{
    OMCI_TL_LAN_KEY         lanKey;
    OMCI_TL_LAN_TYPE_E      lanType; 
    OMCI_TL_VLAN_FILTER_T   vlanFilter;
    OMCI_TL_VLAN_OP_T       usVlanOp;
    OMCI_TL_VLAN_OP_T       dsVlanOp;
    OMCI_TL_DSCP_PBIT_MAP_T dscpMap;
} OMCI_TL_LAN_T;

typedef enum
{
    OMCI_TL_CAST_UNICAST   = 0,
    OMCI_TL_CAST_MULTICAST = 1,
    OMCI_TL_CAST_BROADCAST = 2
} OMCI_TL_CAST_TYPE_E;

typedef struct
{
    OMCI_TL_WAN_KEY       wanKey;
    OMCI_TL_CAST_TYPE_E   castType;
    uint8_t               pbitBm;
    bool                  allowUntagged;
    uint16_t              tcont;
    uint16_t              gemPort;
    uint8_t               usQueue;
    uint8_t               dsQueue;
} OMCI_TL_WAN_INFO_T;

/* OMCI TL layer 2 data service WAN side ANI data structure*/
typedef struct
{
    OMCI_TL_WAN_INFO_T      wanInfo;
    OMCI_TL_VLAN_FILTER_T   vlanFilter;
    OMCI_TL_VLAN_OP_T       usVlanOp;
    OMCI_TL_VLAN_OP_T       dsVlanOp;
    OMCI_TL_DSCP_PBIT_MAP_T dscpMap;    
} OMCI_TL_WAN_T;

/* Default VLAN tag operation for untagged, tagged, double tagged */
typedef enum
{
    OMCI_TL_DEFAULT_VLAN_OP_PASS    = 0,
    OMCI_TL_DEFAULT_VLAN_OP_DISCARD = 1
} OMCI_TL_DEFAULT_VLAN_OP_E;

typedef struct
{
    OMCI_TL_DEFAULT_VLAN_OP_E  untaggedOp;
    OMCI_TL_DEFAULT_VLAN_OP_E  taggedOp;
    OMCI_TL_DEFAULT_VLAN_OP_E  doubleTaggedOp;
} OMCI_TL_DEFAULT_VLAN_OP_T;

/* OMCI TL layer 2 data service structure */
typedef struct
{
    uint32_t      serviceId;
    uint32_t      numOfLan;
    uint32_t      numOfWan;
    OMCI_TL_LAN_T lan[OMCI_TL_MAX_LAN_NUM];
    OMCI_TL_WAN_T wan[OMCI_TL_MAX_WAN_NUM];
    OMCI_TL_DEFAULT_VLAN_OP_T defVlanOp;
} OMCI_TL_SERVICE_T;

/* DSCP pibt mapping data structure */
typedef struct
{
    bool       inUse;
    uint32_t   type; /* configured by 802.1p mapper or by extended VLAN OP */  
    uint8_t    dscpPbitMap[OMCI_TL_DSCP_PBIT_MAP_LEN];
} OMCI_TL_DSCP_MAP_T;

/* Define VLAN translation parameters */
typedef struct
{
    uint32_t ingressVlan;
    uint32_t ingressPbit;
    uint32_t egressVlan;
    uint32_t egressPbit;
} OMCI_TL_VLAN_TRANS_ENTRY_T;

/****************************************************************************** 
 *                        Function Declaration                                                                                                                                  
 ******************************************************************************/
/*******************************************************************************
* omci_tl_get_vlan_trans_mode()
*
* DESCRIPTION:    Get VLAN modification mode in OMCI and mv_cust.
*
* INPUTS:
*
* OUTPUTS:
*  vlan_trans_mode   - VLAN modification mode, 0:disable, 1:enable   
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_get_vlan_trans_mode(uint32_t *vlan_trans_mode);

/*******************************************************************************
* omci_tl_set_vlan_trans_mode()
*
* DESCRIPTION:    Enable/disable VLAN modification in OMCI and mv_cust.
*
* INPUTS:
*  vlan_trans_mode   - VLAN modification mode, 0:disable, 1:enable
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_set_vlan_trans_mode(uint32_t vlan_trans_mode);

/*******************************************************************************
*  omci_tl_get_vlan_trans_params()
*
* DESCRIPTION:   Get cust flow area generated by OMCI.
*
* INPUTS:
*  dir                - Direction.
*
* OUTPUTS:
*  vlan_entry_params  - The pointer to the beginning of cust flow area.
*  num                - Number of total cust flows, including valid and invalid one.
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_get_vlan_trans_params(uint32_t dir, OMCI_TL_VLAN_TRANS_ENTRY_T *vlan_entry_params, uint32_t *num);

/*******************************************************************************
* omci_tl_create_service()
*
* DESCRIPTION:      Create layer 2 data service.
*
* INPUTS:
*  service          - layer 2 data service information, inluding LAN side UNI 
*                     configuration and WAN side ANI configuration.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_create_service(OMCI_TL_SERVICE_T* service);


/*******************************************************************************
* omci_tl_del_service()
*
* DESCRIPTION:      Delete layer 2 data service.
*
* INPUTS:
*  serviceId        - layer 2 data service ID.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_del_service(uint32_t serviceId);


/*******************************************************************************
* omci_tl_update_service()
*
* DESCRIPTION:      Update layer 2 data service.
*
* INPUTS:
*  service          - layer 2 data service information, inluding LAN side UNI 
*                     configuration and WAN side ANI configuration.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_update_service(OMCI_TL_SERVICE_T* service);


/*******************************************************************************
* omci_tl_init()
*
* DESCRIPTION:      Initialize OMCI TL layer.
*
* INPUTS:
*  None
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_init(void);

/*******************************************************************************
* omci_tl_reset()
*
* DESCRIPTION:      Reset OMCI TL layer.
*
* INPUTS:
*  None
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_reset(void);

/*******************************************************************************
* omci_tl_display_flow_db()
*
* DESCRIPTION:      Displays flow entry DB. 
*
* INPUTS:
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  void
*
* COMMENTS:
*
*******************************************************************************/
void omci_tl_display_flow_db(FILE *fildes);

/*******************************************************************************
* omci_tl_display_pnc_flow_db(void)
*
* DESCRIPTION:      Displays PnC flow entry DB. 
*
* INPUTS:
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  void
*
* COMMENTS:
*
*******************************************************************************/
void omci_tl_display_pnc_flow_db(void);

/*******************************************************************************
* omci_tl_set_omci_channel()
*
* DESCRIPTION:      Establishes a communication channel for the OMCI management protocol.
*                   The API sets the gemportid, the Rx input queue in the CPU, and the
*                   Tx T-CONT and queue parameters, which are configured in the driver.
*
* INPUTS:
* gem_port           - for OMCI Rx frames - the gem port wherefrom the OMCI frames are received.
* cpu_rx_queue       - for OMCI Rx frames - the CPU rx queue number.
* tcont_num          - for OMCI Tx frames - the TCONT number where to send the OMCI frames.
* cpu_tx_queue       - for OMCI Tx frames - the CPU tx queue number.
* 
* OUTPUTS:
*
* RETURNS:
* On success, the function returns OMCI_TL_RET_OK. 
* On error different types are returned according to the case.
*
* COMMENTS:
*******************************************************************************/
OMCI_TL_STATUS omci_tl_set_omci_channel (uint32_t              gem_port,
                                         uint32_t              cpu_rx_queue,
                                         uint32_t              tcont_num,
                                         uint32_t              cpu_tx_queue);

#endif /*__OMCI_TL_EXPO_HEADER__*/
