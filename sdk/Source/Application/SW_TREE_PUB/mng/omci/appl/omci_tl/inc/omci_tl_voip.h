/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_voip.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file encapsulates VoIP functions for OMCI            **/
/**                translation layer adaption module .                       **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   25/Aug/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_VOIP_HEADER__
#define __OMCI_TL_VOIP_HEADER__

#include <stdint.h>
#include <stdbool.h>
#include "omci_tl_expo.h"

/****************************************************************************** 
 *                        Macro and Type Definition                                                                                                                                  
 ******************************************************************************/






/****************************************************************************** 
 *                        Data Enum and Structure                                                                                                                                  
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/////////       SIPAGENT_T       ///////////////////////////////////////////////
/////////               AUTHANDADDRESS_T  AUTHENTICATIONSECURITY_T     /////////
/////////               SIPRESPONSEENTRY_T                             /////////
////////////////////////////////////////////////////////////////////////////////


#define URI_SIZE                               80

#define SIPRESPONSEMSG_SIZE                    50
typedef struct
{
    uint16_t sipResponseCode;
    uint8_t  tone;
    uint8_t  textMessage[SIPRESPONSEMSG_SIZE];
} SIPRESPONSEENTRY_T;

#define USERNAME_SIZE                          50
#define PASSWORD_SIZE                          25
#define REALM_SIZE                             25
typedef struct
{
    uint8_t  validationScheme;      // 0: validation disabled, 1: MD5 digest, 3: RFC 2617
    uint8_t  username[USERNAME_SIZE];
    uint8_t  password[PASSWORD_SIZE];
    uint8_t  realm[REALM_SIZE];
} AUTHENTICATIONSECURITY_T;

typedef struct
{
    AUTHENTICATIONSECURITY_T authenticationSecurity;
    uint8_t                  address[URI_SIZE];
} AUTHANDADDRESS_T;


// OMCI Entity DB
#define IPADDR_SIZE                            4
#define SOFTSWITCH_SIZE                        4
#define NUM_SIPRESPONSETABLE_ENTRIES           10
typedef struct
{
    int               sipAgentId;
    // SIP server addresses
    uint8_t           proxyServerAddress[URI_SIZE];
    uint8_t           outboundProxyAddress[URI_SIZE];
    uint8_t           hostPartUri[URI_SIZE];
    AUTHANDADDRESS_T  sipRegistrar;

    uint8_t           primarySipDns[IPADDR_SIZE];
    uint8_t           secondarySipDns[IPADDR_SIZE];

    // TCP/UDP
    uint8_t           proitocol;        // IANA: UDP=0x11
    uint16_t          tcpUdpPort;       // The TCP/UDP port number
    uint8_t           dscp;             // The DSCP value number      

    // SIP timers
    uint32_t          sipRegExpTime;
    uint32_t          sipReRegHeadStartTime;

    // SIP response
    int                numSipResponseEntries;
    SIPRESPONSEENTRY_T sipResponseAra[NUM_SIPRESPONSETABLE_ENTRIES];

    // Other SIP options, parameters
    uint8_t            softswitch[SOFTSWITCH_SIZE];
    bool               sipOptionTransmitControl;
    uint8_t            sipUriFormat;    // 0 - TEL URI, 1- SIP URI 
} SIPAGENT_T;

typedef struct
{
    int status;        // enum to be added
} SIPAGENTSTATUS_T;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/////////       VOIPMEDIAPROFILE_T       ///////////////////////////////////////
/////////             MEDIAPROCESSING_T                    /////////////////////
/////////             RTPPROFILE_T                         /////////////////////
/////////             VOICESERVICEPROFILE_T                /////////////////////
/////////                          RINGINGEVENTENTRY_T       ///////////////////
/////////                          RINGINGPATTERNENTRY_T     ///////////////////
/////////                          TONEEVENTENTRY_T          ///////////////////
/////////                          TONEPATTERNENTRY_T        ///////////////////
////////////////////////////////////////////////////////////////////////////////

typedef struct
{
    uint8_t  index;
    uint8_t  toneOn;
    uint16_t frequency1;
    uint8_t  power1;
    uint16_t frequency2;
    uint8_t  power2;
    uint16_t frequency3;
    uint8_t  power3;
    uint16_t frequency4;
    uint8_t  power4;
    uint16_t modulationFrequency;
    uint8_t  modulationPower;
    uint16_t duration;
    uint8_t  nextEntry;
} TONEPATTERNENTRY_T;

typedef struct
{
    uint8_t  event;
    uint8_t  tonePattern;
    uint16_t toneFile;
    uint8_t  toneFileRepetitions;
    uint8_t  reserved[2];
} TONEEVENTENTRY_T;

typedef struct
{
    uint8_t  index;
    uint8_t  ringingOn;
    uint16_t duration;
    uint8_t  nextEntry;
} RINGINGPATTERNENTRY_T;

typedef struct
{
    uint8_t  event;
    uint8_t  ringingPattern;
    uint16_t ringingFile;
    uint8_t  ringingFileRepetitions;
    uint16_t ringingText;
} RINGINGEVENTENTRY_T;

#define TONEPATTERNARA_ENTRIES            (36)
#define TONEEVENTARA_ENTRIES              (16)
#define RINGINGPATTERNARA_ENTRIES         (16)
#define RINGINGEVENTARA_ENTRIES           (16)
typedef struct
{
    uint8_t               announcementType;
    uint16_t              jitterTarget;
    uint16_t              jitterBufferMax;
    uint8_t               echoCancelInd;
    uint16_t              pstnProtocolVariant; 
    uint16_t              dtmfDigitLevels;
    uint16_t              dtmfDigitDuration;
    uint16_t              hookflashMinimumTime;
    uint16_t              hookflashMaximumTime;

    int                   numTonePatternEntries;
    TONEPATTERNENTRY_T    tonePatternAra[TONEPATTERNARA_ENTRIES];

    int                   numToneEventEntries;
    TONEEVENTENTRY_T      toneEventAra[TONEEVENTARA_ENTRIES];

    int                   numRingingPatternEntries;
    RINGINGPATTERNENTRY_T ringingPatternAra[RINGINGPATTERNARA_ENTRIES];

    int                   numRingingEventEntries;
    RINGINGEVENTENTRY_T   ringingEventAra[RINGINGPATTERNARA_ENTRIES];
} VOICESERVICEPROFILE_T;


typedef struct
{
    uint16_t  localPortMin;
    uint16_t  localPortMax;
    uint8_t   dscpMark;
    uint8_t   piggybackEvents;
    uint8_t   toneEvents;
    uint8_t   dtmfEvents;
    uint8_t   casEvents;
} RTPPROFILE_T;

typedef struct
{
    uint8_t   faxMode;
    uint8_t   codecSelection1;
    uint8_t   packetPeriodSelection1;
    uint8_t   silenceSuppression1;
    uint8_t   codecSelection2;
    uint8_t   packetPeriodSelection2;
    uint8_t   silenceSuppression2;
    uint8_t   codecSelection3;
    uint8_t   packetPeriodSelection3;
    uint8_t   silenceSuppression3;
    uint8_t   codecSelection4;
    uint8_t   packetPeriodSelection4;
    uint8_t   silenceSuppression4;
    uint8_t   oobDtmf;
} MEDIAPROCESSING_T;

typedef struct
{
    MEDIAPROCESSING_T      *p_MediaProcessing;
    VOICESERVICEPROFILE_T  *p_VoiceServiceProfile;
    RTPPROFILE_T           *p_RtpProfile;
} VOIPMEDIAPROFILE_T;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/////////       SIPLINE_T       ////////////////////////////////////////////////
/////////             SIPUSER_T                            /////////////////////
/////////             NETWORKDIALPLANTABLE_T               /////////////////////
/////////                      DIALPLAN_T                        ///////////////
/////////             VOIPFEATUREACCESSCODES_T             /////////////////////
/////////             VOIPAPPLICSERVICEPROFILE_T           /////////////////////
////////////////////////////////////////////////////////////////////////////////

#define DIALPLANTOKEN_SIZE         28

typedef struct
{
    uint8_t  dialPlanId;
    uint8_t  action;
    uint8_t  dialPlanToken[DIALPLANTOKEN_SIZE];
} DIALPLAN_T;

// OMCI Entity DB
#define NUM_DIALPLAN_ENTRIES       10
typedef struct
{
    uint16_t   dialPlanNumber;
    uint16_t   dialPlanTableMaxSize;
    uint16_t   criticalDialTimeout;
    uint16_t   partialDialTimeout;
    uint8_t    dialPlanFormat;
    DIALPLAN_T dialPlanTable[NUM_DIALPLAN_ENTRIES];
} NETWORKDIALPLANTABLE_T;

#define AXCODE_SIZE      5
typedef struct
{
    uint8_t  cancelCallWaiting[AXCODE_SIZE];
    uint8_t  callHold[AXCODE_SIZE];
    uint8_t  callPark[AXCODE_SIZE];
    uint8_t  cidsActivate[AXCODE_SIZE];
    uint8_t  cidsDeactivate[AXCODE_SIZE];
    uint8_t  doNotDisturbActivation[AXCODE_SIZE];
    uint8_t  doNotDisturbDeactivation[AXCODE_SIZE];
    uint8_t  doNotDisturbPinChange[AXCODE_SIZE];
    uint8_t  emergencyServiceNumber[AXCODE_SIZE];
    uint8_t  intercomService[AXCODE_SIZE];
    uint8_t  blindCallTransfer[AXCODE_SIZE];
    uint8_t  attendedCallTransfer[AXCODE_SIZE];
} VOIPFEATUREACCESSCODES_T;

typedef struct
{
    uint8_t            cidFeatures;
    uint8_t            callWaitingFeatures;
    uint16_t           callProgressOrTransferFeatures;
    uint16_t           callPresentationFeatures;
    uint8_t            directConnectFeature;
    AUTHANDADDRESS_T   directConnectUri;
    AUTHANDADDRESS_T   bridgedLineAgentUri;
    AUTHANDADDRESS_T   conferenceFactoryUri;
} VOIPAPPLICSERVICEPROFILE_T;

#define USERPARTAOR_SIZE                80
#define SIPDISPLAYNAME_SIZE             25
typedef struct
{
    uint8_t                  userPartAor[USERPARTAOR_SIZE];          // User part of Address of Record
    uint8_t                  sipDisplayName [SIPDISPLAYNAME_SIZE];   // SIP display name
    AUTHENTICATIONSECURITY_T usernamePassword;
    AUTHANDADDRESS_T         voiceMailServer;
    int                      voicemailExpirationTime;
    int                      releaseTimer;
    int                      rohTimer;
} SIPUSER_T;

typedef struct
{
    int                        sipLineId;
    SIPUSER_T                  sipUser;
    VOIPAPPLICSERVICEPROFILE_T *p_VoIPApplicServiceProfile;
    VOIPFEATUREACCESSCODES_T   *p_VoIPFeatureAccessCodes;
    NETWORKDIALPLANTABLE_T     *p_NetworkDialPlanTable;
    VOIPMEDIAPROFILE_T         *p_VoipMediaProfile;
} SIPLINE_T;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/////////     VOIPLINESTATUS_T      ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// TBD add in enums

#define VOIPCALLDESTADDR_SIZE                25
typedef struct
{
    uint16_t voipCodecUsed;
    uint8_t  voipServerStatus;
    uint8_t  voipPortSessionType;
    uint16_t voipCall1PacketPeriod;
    uint16_t voipCall2PacketPeriod;
    uint8_t  voipCall1DestAddr[VOIPCALLDESTADDR_SIZE];
    uint8_t  voipCall2DestAddr[VOIPCALLDESTADDR_SIZE];
} VOIPLINESTATUS_T;


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/////////     VOIPSERVICE_T      ///////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

#define SIPAGENTSARA_SIZE         2
#define NUM_POTS_PORTS            2                 // Will make flexible later!!
typedef struct
{
    int         voiceServiceId;
    int         numSipAgents;
    SIPAGENT_T  sipAgent[SIPAGENTSARA_SIZE];       // When numSipAgents=2, second entry is for redundant SIP agent
    int         numSipLines;
    SIPLINE_T   sipLine[NUM_POTS_PORTS];
} VOIPSERVICE_T;


/****************************************************************************** 
 *                        Function Declaration                                                                                                                                  
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_voip_update_ip_host(uint32_t ipaddr, uint32_t netmask, uint32_t gateway);
OMCI_TL_STATUS omci_tl_voip_update_ip_host_vid_pbit            (uint16_t vid, uint8_t pbits);
OMCI_TL_STATUS omci_tl_voip_reset_voip_service                 ();
OMCI_TL_STATUS omci_tl_voip_create_voip_service                (VOIPSERVICE_T *p_VoIPService);
OMCI_TL_STATUS omci_tl_voip_delete_voip_service                (int           voiceServiceId);

OMCI_TL_STATUS omci_tl_voip_add_sip_line                       (int voiceServiceId, SIPLINE_T *p_SipLine);
OMCI_TL_STATUS omci_tl_voip_delete_sip_line                    (int voiceServiceId, int       sipLineId);

OMCI_TL_STATUS omci_tl_voip_update_sip_user                    (int voiceServiceId, int sipLineId, SIPUSER_T                  *p_SipUser);

OMCI_TL_STATUS omci_tl_voip_add_voip_applic_service_profile    (int voiceServiceId, int sipLineId, VOIPAPPLICSERVICEPROFILE_T *p_VoIPApplicServiceProfile);
OMCI_TL_STATUS omci_tl_voip_update_voip_applic_service_profile (int voiceServiceId, int sipLineId, VOIPAPPLICSERVICEPROFILE_T *p_VoIPApplicServiceProfile);
OMCI_TL_STATUS omci_tl_voip_delete_voip_applic_service_profile (int voiceServiceId, int sipLineId);

OMCI_TL_STATUS omci_tl_voip_add_voip_feature_access_codes      (int voiceServiceId, int sipLineId, VOIPFEATUREACCESSCODES_T   *p_VoIPFeatureAccessCodes);
OMCI_TL_STATUS omci_tl_voip_update_voip_feature_access_codes   (int voiceServiceId, int sipLineId, VOIPFEATUREACCESSCODES_T   *p_VoIPFeatureAccessCodes);
OMCI_TL_STATUS omci_tl_voip_delete_voip_feature_access_codes   (int voiceServiceId, int sipLineId);

OMCI_TL_STATUS omci_tl_voip_add_network_dial_plan_table        (int voiceServiceId, int sipLineId, NETWORKDIALPLANTABLE_T     *p_NetworkDialPlanTable);
OMCI_TL_STATUS omci_tl_voip_update_network_dial_plan_table     (int voiceServiceId, int sipLineId, NETWORKDIALPLANTABLE_T     *p_NetworkDialPlanTable);
OMCI_TL_STATUS omci_tl_voip_delete_network_dial_plan_table     (int voiceServiceId, int sipLineId);

OMCI_TL_STATUS omci_tl_voip_add_media_processing               (int voiceServiceId, int sipLineId, MEDIAPROCESSING_T          *p_MediaProcessing);
OMCI_TL_STATUS omci_tl_voip_update_media_processing            (int voiceServiceId, int sipLineId, MEDIAPROCESSING_T          *p_MediaProcessing);
OMCI_TL_STATUS omci_tl_voip_delete_media_processing            (int voiceServiceId, int sipLineId);

OMCI_TL_STATUS omci_tl_voip_add_voice_service_profile          (int voiceServiceId, int sipLineId, VOICESERVICEPROFILE_T      *p_VoiceServiceProfile);
OMCI_TL_STATUS omci_tl_voip_update_voice_service_profile       (int voiceServiceId, int sipLineId, VOICESERVICEPROFILE_T      *p_VoiceServiceProfile);
OMCI_TL_STATUS omci_tl_voip_delete_voice_service_profile       (int voiceServiceId, int sipLineId);

OMCI_TL_STATUS omci_tl_voip_add_rtp_profile                    (int voiceServiceId, int sipLineId, RTPPROFILE_T               *p_RtpProfile);
OMCI_TL_STATUS omci_tl_voip_update_rtp_profile                 (int voiceServiceId, int sipLineId, RTPPROFILE_T               *p_RtpProfile);
OMCI_TL_STATUS omci_tl_voip_delete_rtp_profile                 (int voiceServiceId, int sipLineId);

OMCI_TL_STATUS omci_tl_voip_get_sip_line_status                (int voiceServiceId, int sipLineId, VOIPLINESTATUS_T           *p_VoIPLineStatus);

OMCI_TL_STATUS omci_tl_voip_get_sip_agent_status               (int voiceServiceId, SIPAGENTSTATUS_T *p_SipAgentStatus);


#endif /*__OMCI_TL_VOIP_HEADER__*/
