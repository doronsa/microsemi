/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_misc.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file encapsulates misc functions for OMCI            **/
/**                translation layer, such as UNI cfg, IP cfg, QoS.          **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   25/Aug/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_MISC_HEADER__
#define __OMCI_TL_MISC_HEADER__

#include "CtcLoopbackDetection.h"
/****************************************************************************** 
 *                        Marco and Type Definition                                                                                                                                  
 ******************************************************************************/






/****************************************************************************** 
 *                        Data Enum and Structure                                                                                                                                  
 ******************************************************************************/



/****************************************************************************** 
 *                        Function Declaration                                                                                                                                  
 ******************************************************************************/
/******************************************************************************
 *
 * Function   : omci_tl_eth_2_mrvl_srcport
 *              
 * Description: This function convert OMCI UNI port index to TPM port index
 *
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_eth_2_mrvl_srcport(UINT32 port, tpm_src_port_type_t *src_port);

extern OMCI_TL_STATUS omci_tl_set_tcont_policy(TcontGME *ptcontg, UINT8 policy);
extern OMCI_TL_STATUS omci_tl_set_priority_queue_weight(PriorityQueueGME *pprioq, UINT8 weight);
extern OMCI_TL_STATUS omci_tl_set_mac_admin_state(UINT32 virtualGbEportNum, UINT8   AdminState);
extern OMCI_TL_STATUS omci_tl_set_mtu_size(UINT32  size);
extern OMCI_TL_STATUS omci_tl_set_mac_auto_detec(UINT32 virtualGbEportNum, UINT8 autoDetec);
extern OMCI_TL_STATUS omci_tl_set_phy_eth_loopback(UINT32 virtualGbEportNum, LOOPBACKCFG loopback);
extern OMCI_TL_STATUS omci_tl_get_pptp_ethernet_uni(PptpEthernetUniME *peuni);
extern OMCI_TL_STATUS omci_tl_get_ip_host_config_data(IpHostConfigDataME *peuni);
extern OMCI_TL_STATUS omci_tl_synch_ctc_lbdt(CtcLpbkDetectionME *loopBackStr);
#ifdef OMCI_CONNECT_TO_MIDWARE
extern OMCI_TL_STATUS omci_tl_set_ip_host_config_data(MIDWARE_TABLE_ONU_IP_T *p_mid_table);
extern OMCI_TL_STATUS omci_tl_set_ip_host_ip_option(MIDWARE_TABLE_ONU_IP_T *p_mid_table);
extern OMCI_TL_STATUS omci_tl_set_ip_host_primary_dns(MIDWARE_TABLE_ONU_IP_T *p_mid_table);
extern OMCI_TL_STATUS omci_tl_set_ip_host_secondary_dns(MIDWARE_TABLE_ONU_IP_T *p_mid_table);
#endif
#endif /*__OMCI_TL_MISC_HEADER__*/
