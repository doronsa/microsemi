/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_os.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file contains Operation System related definition    **/
/**                                                                          **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   25/Aug/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_OS_HEADER__
#define __OMCI_TL_OS_HEADER__

/****************************************************************************** 
 *                        Marco and Type Definition                                                                                                                                  
 ******************************************************************************/






/****************************************************************************** 
 *                        Data Enum and Structure                                                                                                                                  
 ******************************************************************************/



/****************************************************************************** 
 *                        Function Declaration                                                                                                                                  
 ******************************************************************************/
/*******************************************************************************
* omci_tl_get_device_mac_addr()
*
* DESCRIPTION: Get device MAC address.
*
* INPUTS:
*  device: Device name
*
* OUTPUTS:
*  pMac: The MAC address of input device      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS  omci_tl_get_device_mac_addr(char *device, unsigned char *pMac);



#endif /*__OMCI_TL_OS_HEADER__*/
