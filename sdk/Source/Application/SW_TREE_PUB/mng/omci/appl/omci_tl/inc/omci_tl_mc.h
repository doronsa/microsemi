/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_mc.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file declare OMCI translation layer multicast        **/
/**                related functions.                                        **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   25/Aug/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_MC_HEADER__
#define __OMCI_TL_MC_HEADER__

#include <stdint.h>
#include <stdbool.h>
#include "omci_tl_expo.h"

/****************************************************************************** 
 *                        Marco and Type Definition                                                                                                                                  
 ******************************************************************************/






/****************************************************************************** 
 *                        Data Enum and Structure                                                                                                                                  
 ******************************************************************************/

// MULTICAST_CONFIG_T
// 
// Values from g.988 Multicast Operations Profile
typedef struct
{
    int   igmpVersion;
    int   multicastMode;     // G.988   0 - transparent snooping, 1 - snooping + proxy reporting, 2- IGMP proxy
    int   filterMode;        // OMCI: always 0
    bool  fastLeaveAbility;  // OMCI: always true
    bool  fastLeaveEnable;
} MULTICAST_CONFIG_T;


// MULTICAST_PORT_CONFIG_T
// 
// The following is a per UNI port structure. The values from G.988 Ext. Multicast Operations Profile pointed at from 
// Multicast Subscriber Config via an entry in Multicast Service Package table (done for first record)
#define V6_IPADDR_SIZE        (16)
typedef struct 
{
    uint32_t     maxGroupNum;
    uint32_t     usIgmpRate;   
    uint16_t     usTagOper;
    uint16_t     dsTagOper;
    uint32_t     usTagDefaultTci;
    uint32_t     dsTagDefaultTci;
    uint32_t     robust;   
    uint32_t     queryIp;  
    uint32_t     queryInterval;  
    uint32_t     queryMaxRespTime;  
    uint32_t     lastQueryTime;   
    uint32_t     unauthJoinBehaviour;        
}MULTICAST_PORT_CFG_T;


// MULTICAST_PORT_STATUS_T
// 
// Used with Multicast Subscriber Monitor
typedef struct
{
    uint32_t currentMulticastBandwidth;
    uint32_t joinMessagesCounter;
    uint32_t bandwidthExceededCounter;
} MULTICAST_PORT_STATUS_T;


// MULTICAST_PORT_ACTIVE_GROUP_T
// 
// Used with Multicast Subscriber Monitor
#define V4_IPADDR_SIZE        (4)
typedef struct
{
    uint16_t  vlanId;
    uint8_t   srcIpAddress[V4_IPADDR_SIZE];
    uint8_t   mcastDestIpAddress[V4_IPADDR_SIZE];
    uint32_t  bestEffortActualBandwidth;
    uint8_t   clientIpAddress[V4_IPADDR_SIZE];
    uint32_t  timeSinceLastJoin;
    uint8_t   reserved[2];
} MULTICAST_PORT_ACTIVE_GROUP_T;


/*Structure of MC port control table*/
typedef struct 
{ 
    uint16_t     tablType;
    uint16_t     setCtrl;   
    uint16_t     rowKey;
    uint16_t     gemPort;
    uint16_t     aniVid;    
    uint32_t     srcIp;
    uint32_t     dstIpStart;   
    uint32_t     dstIpEnd;
    uint32_t     imputedGroupBw;  
    uint8_t      srcIpv6[V6_IPADDR_SIZE];  
    uint8_t      dstIpv6[V6_IPADDR_SIZE];   
    uint16_t     previewLen;      
    uint16_t     previewRepeatTime;   
    uint16_t     previewRepeatCount;  
    uint16_t     previewResetTime; 
    uint32_t     vendorSpec;      
    
}MULTICAST_PORT_ACL_T;

/*Structure of MC port service table*/
typedef struct 
{
    uint16_t     set_ctrl;   
    uint16_t     row_key;
    uint16_t     vid;
    uint32_t     max_sum_group;
    uint32_t     max_mc_bw;   
    
}MULTICAST_PORT_SERV_T;


/*Structure of MC port preview table*/
typedef struct 
{  
    uint16_t     setCtrl;   
    uint16_t     rowKey;
    uint8_t      srcIp[V6_IPADDR_SIZE];  
    uint8_t      dstIp[V6_IPADDR_SIZE];   
    uint16_t     aniVid;
    uint16_t     uniVid;   
    uint32_t     duration;
    uint32_t     timeLeft;    
    
}MULTICAST_PORT_PREVIEW_T;


/****************************************************************************** 
 *                        Function Declaration                                                                                                                                  
 ******************************************************************************/
 /******************************************************************************
 *
 * Function   : omci_tl_set_igmp_version
 *              
 * Description: This function set IGMP version to midware
 *             
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_igmp_version(uint16_t meInstance, uint8_t igmpVersion);

/******************************************************************************
 *
 * Function   : omci_tl_set_igmp_function
 *              
 * Description: This function set IGMP function to midware
 *            from midware table.   
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_igmp_function(uint16_t meInstance, uint8_t igmpFunction);

/******************************************************************************
 *
 * Function   : omci_tl_set_immediate_leave
 *              
 * Description: This function set Immediate leave to midware
 *            from midware table.   
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_immediate_leave(uint16_t meInstance, uint8_t immediateLeave);

/******************************************************************************
 *
 * Function   : omci_tl_set_upstream_igmp_tci_to_midware
 *              
 * Description: This function set upstream IGMP Tci to midware
 *            from midware table.   
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_upstream_igmp_tci(uint16_t meInstance, uint16_t upstreamIGMPTci);

/******************************************************************************
 *
 * Function   : omci_tl_set_upstream_igmp_tag_control
 *              
 * Description: This function set upstream IGMP Tag Control to midware
 *            from midware table.   
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_upstream_igmp_tag_control(uint16_t meInstance, uint8_t upstreamIGMPTagControl);

/******************************************************************************
 *
 * Function   : omci_tl_set_downstream_igmp_tag_control
 *              
 * Description: This function set downstream IGMP Tag Control to midware
 *            from midware table.   
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_downstream_igmp_tag_control(uint16_t meInstance, uint8_t downstreamIGMPTagControl);

/******************************************************************************
 *
 * Function   : omci_tl_set_downstream_igmp_mcast_tci
 *              
 * Description: This function set Downstream IGMP Mcast Tci to midware
 *            from midware table.   
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_downstream_igmp_mcast_tci(uint16_t meInstance, uint16_t downstreamIGMPMcastTci);

/******************************************************************************
 *
 * Function   : omci_tl_set_max_simultaneous_groups
 *              
 * Description: This function set Max Simultaneous Groups to midware
 *            from midware table.   
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_max_simultaneous_groups(uint16_t meInstance, uint8_t meType, uint16_t maxSimultaneousGroups);

/******************************************************************************
 *
 * Function   : omci_tl_set_us_igmp_rate
 *              
 * Description: This function set IGMP U/S rate limitation
 *
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_us_igmp_rate(uint16_t meInstance, uint32_t igmpRate);

/******************************************************************************
 *
 * Function   : omci_tl_set_robust
 *              
 * Description: This function set multicast robustness
 *
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_robust(uint16_t meInstance, uint8_t mc_robust);

/******************************************************************************
 *
 * Function   : omci_tl_set_query_ip
 *              
 * Description: This function set multicast query IP address
 *
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_query_ip(uint16_t meInstance, uint32_t query_ip);

/******************************************************************************
 *
 * Function   : omci_tl_set_query_interval
 *              
 * Description: This function set multicast query interval
 *
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_query_interval(uint16_t meInstance, uint32_t query_interval);

/******************************************************************************
 *
 * Function   : omci_tl_set_query_max_resp_time
 *              
 * Description: This function set multicast query max response time
 *
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_query_max_resp_time(uint16_t meInstance, uint32_t query_max_resp_time);

/******************************************************************************
 *
 * Function   : omci_tl_set_last_query_time
 *              
 * Description: This function set multicast last query time
 *
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_last_query_time(uint16_t meInstance, uint32_t last_query_time);

/******************************************************************************
 *
 * Function   : omci_tl_set_unauth_join_behaviour
 *              
 * Description: This function set multicast  unauth join behaviour
 *
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_set_unauth_join_behaviour(uint16_t meInstance, uint32_t unauth_join_behaviour);

/******************************************************************************
 *
 * Function   : omci_tl_update_multicast_acl
 *              
 * Description: This function set multicast ACL rule operation table.   
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_update_multicast_acl(int portId, MULTICAST_PORT_ACL_T *p_MulticastAcl);

/******************************************************************************
 *
 * Function   : omci_tl_update_multicast_service
 *              
 * Description: This function set multicast service table.   
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_update_multicast_service(int portId, MULTICAST_PORT_SERV_T *p_McService);

/******************************************************************************
 *
 * Function   : omci_tl_update_allowed_preview
 *              
 * Description: This function set multicast preview rule operation table.   
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_update_allowed_preview(int portId, MULTICAST_PORT_PREVIEW_T *p_AllowedPreview);

/****************************************************************************** 
 *
 * Function   : omci_tl_reset_mc_tables
 *              
 * Description: This function reset all multicast tables in middleware to default values.   
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
OMCI_TL_STATUS omci_tl_reset_mc_tables(void);


/****************************************************************************** 
 *                       global multicast configuration                                                                                                                                  
 ******************************************************************************/ 
extern OMCI_TL_STATUS omci_tl_update_multicast_configuration   (MULTICAST_CONFIG_T *p_MulticastConfig);

/****************************************************************************** 
 *                       Current multicast port configuration                                                                                                                                  
 ******************************************************************************/ 
extern OMCI_TL_STATUS omci_tl_update_multicast_port_configuration   (int portId, MULTICAST_PORT_CFG_T *p_MulticastPortConfig);


/****************************************************************************** 
 *                       Current multicast port status                                                                                                                                  
 ******************************************************************************/ 
extern OMCI_TL_STATUS omci_tl_get_multicast_port_status   (int portId, MULTICAST_PORT_STATUS_T *p_MulticastPortStatus);


/****************************************************************************** 
 *                       Current active multicast group                                                                                                                                  
 ******************************************************************************/ 
extern OMCI_TL_STATUS omci_tl_get_multicast_port_active_group (int portId, MULTICAST_PORT_ACTIVE_GROUP_T *p_MulticastPortActiveGroup);

#endif /*__OMCI_TL_MC_HEADER__*/
