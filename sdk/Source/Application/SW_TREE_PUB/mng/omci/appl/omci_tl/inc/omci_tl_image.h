/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_midware.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : This file encapsulates midware functions for OMCI         **/
/**                translation layer.                                        **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   25/Aug/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_IMAGE_HEADER__
#define __OMCI_TL_IMAGE_HEADER__

/****************************************************************************** 
 *                        Marco and Type Definition                                                                                                                                  
 ******************************************************************************/






/****************************************************************************** 
 *                        Data Enum and Structure                                                                                                                                  
 ******************************************************************************/



/****************************************************************************** 
 *                        Function Declaration                                                                                                                                  
 ******************************************************************************/
extern OMCI_TL_STATUS omci_tl_set_image_committed(UINT16 meInstance);
extern OMCI_TL_STATUS omci_tl_set_image_activation(UINT16 meInstance);
extern OMCI_TL_STATUS omci_tl_set_image_to_flash(UINT16 meInstance, char *filename);
extern OMCI_TL_STATUS omci_tl_set_image_abort();
extern OMCI_TL_STATUS omci_tl_set_image_isvalid(UINT16 meInstance, UINT8 is_valid);
#ifdef OMCI_CONNECT_TO_MIDWARE
extern OMCI_TL_STATUS omci_tl_get_image_data(MIDWARE_TABLE_IMAGE_T  *mid_table);
extern OMCI_TL_STATUS omci_tl_get_image_status(MIDWARE_TABLE_IMAGE_T    *mid_table);
#endif
#endif /*__OMCI_TL_MIDWARE_HEADER__*/
