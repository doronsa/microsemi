/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_print.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : This file encapsulates print functions for OMCI           **/
/**                translation layer adaption module .                       **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   25/Aug/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_PRINT_HEADER__
#define __OMCI_TL_PRINT_HEADER__

#include "OmciGlobalData.h"
/****************************************************************************** 
 *                        Marco and Type Definition                                                                                                                                  
 ******************************************************************************/
#ifdef qprintf
#undef qprintf
#define qprintf(attribute, submodule, msg, arg...)  do{if (getOmciTlDisplayFlag() == true) {printf(msg, ##arg);}}while(0)
#else
#define qprintf(attribute, submodule, msg, arg...) do{if (getOmciTlDisplayFlag() == true) {printf(msg, ##arg);}}while(0)
#endif





/****************************************************************************** 
 *                        Data Enum and Structure                                                                                                                                  
 ******************************************************************************/



/****************************************************************************** 
 *                        Function Declaration                                                                                                                                  
 ******************************************************************************/

#endif /*__OMCI_TL_PRINT_HEADER__*/
