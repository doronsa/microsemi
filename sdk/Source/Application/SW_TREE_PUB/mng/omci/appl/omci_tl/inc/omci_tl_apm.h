/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_apm.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file encapsulates APM functions for OMCI translation **/
/**                layer adaption module .                                   **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         ken  - initial version created.   31/March/2012          
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_APM_HEADER__
#define __OMCI_TL_APM_HEADER__

/****************************************************************************** 
 *                        Marco and Type Definition                                                                                                                                  
 ******************************************************************************/ 
extern bool omci_tl_me_instance_2_apm_param1(UINT16 meClass, UINT16 meInstance, UINT32 *param1);
extern bool omci_tl_get_me_alarm_index_from_apm_alarm_type(UINT16 meClass, UINT16 meInstance, UINT32 apm_alarm_type, UINT32 *me_alarm_index);
extern bool omci_tl_get_mask_size_from_apm_avc_type(UINT16 meClass, UINT16 meInstance, UINT32 apm_avc_type, UINT8 *msbAttribMask, UINT8 *lsbAttribMask, UINT16 *size);
extern bool omci_tl_get_value_from_apm_avc_type(UINT16 meClass, UINT16 meInstance, UINT32 apm_avc_type, UINT8 *in_value, UINT8* out_value);
extern bool omci_tl_get_apm_pm_type(UINT16 meClass, UINT16 meInstance, UINT32 *apm_pm_type);
extern bool omci_tl_get_current_pm_data(UINT16 meClass, UINT16 meInstance, void* dataBlock, UINT32 size);

extern OMCI_TL_STATUS omci_tl_create_apm_entities_general(MeInst_S *pmeinst);
extern OMCI_TL_STATUS omci_tl_delete_apm_entities_general(MECLASS meClass, UINT16 meInstance);
extern OMCI_TL_STATUS omci_tl_set_tca_thresholds(MECLASS meClassId, UINT16 meClassInstance, ThresholdCrossingApmParams_s *ptcApmParams);
extern OMCI_TL_STATUS omci_tl_set_alarm_thresholds(UINT32 apm_alarm_type, MECLASS meClassId, UINT16 meClassInstance, UINT32 threshold, UINT32 clear_threshold);
extern OMCI_TL_STATUS omci_tl_create_apm_entities_extended(MeInst_S *pmeinstance);
extern OMCI_TL_STATUS omci_tl_delete_apm_entities_extended(MECLASS meClass, UINT16 meInstance);
extern OMCI_TL_STATUS omci_tl_set_pm_interval(UINT32 pm_interval);
extern OMCI_TL_STATUS omci_tl_mib_reset(void);
extern OMCI_TL_STATUS omci_tl_pm_synchronize(void);
extern OMCI_TL_STATUS omci_tl_set_pm_global_clear(MeInst_S *pmeinst);

#endif /*__OMCI_TL_TPM_HEADER__*/
