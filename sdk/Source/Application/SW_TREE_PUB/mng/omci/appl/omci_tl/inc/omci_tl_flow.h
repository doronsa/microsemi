/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_flow.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file includes translation layer layer2 generic flow  **/
/**                data definition and function declaration .                **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   25/Aug/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_FLOW_HEADER__
#define __OMCI_TL_FLOW_HEADER__

/****************************************************************************** 
 *                        Marco and Type Definition                                                                                                                                  
 ******************************************************************************/
#define OMCI_TL_MAX_FLOW_NUM (4*24*8)
#define OMCI_TL_VID_MASK     0x0fff
#define OMCI_TL_PBIT_MASK    (0x7<<13)
#define OMCI_TL_TCI_MASK     0x0000ffff
#define OMCI_TL_NO_MASK      0x0

#define OMCI_TL_PBIT_SHIFT    13

#define OMCI_TL_MAX_PBIT_VALUE 7

#define OMCI_TL_SRC_PORT_MASK     0x001f
#define OMCI_TL_TGT_PORT_ANY      0x001f
#define OMCI_TL_TGT_FOUR_PORT_BM  0x000f


#define OMCI_IF_ERROR(ret) if (ret!= OMCI_TL_RET_OK) { qprintf(QPROMCI_ERROR, QPROMCIMOD_SUB1, "%s:Line[%d] error, recvd ret_code[%d]\n\r",__FUNCTION__, __LINE__, ret); return(ret); }



/****************************************************************************** 
 *                        Data Enum and Structure                                                                                                                                  
 ******************************************************************************/

/* VLAN tag filter mode*/
typedef enum
{
    OMCI_TL_TAG_FILTER_MODE_TCI   = 0,
    OMCI_TL_TAG_FILTER_MODE_VID   = 1,
    OMCI_TL_TAG_FILTER_MODE_PRI   = 2
} OMCI_TL_TAG_FILTER_MODE_E;


typedef enum
{
    OMCI_TL_TAG_TYPE_NO_TAG       = 0,
    OMCI_TL_TAG_TYPE_SINGLE_TAG   = 1,
    OMCI_TL_TAG_TYPE_DOUBLE_TAG   = 2,
    OMCI_TL_TAG_TYPE_ANY_TAG      = 3    

} OMCI_TL_TAG_TYPE_E;


typedef enum
{
    OMCI_TL_FLOW_DIR_UPSTREAM    = 0,
    OMCI_TL_FLOW_DIR_DOWNSTREAM  = 1,
    OMCI_TL_FLOW_DIR_NUM         = 2

} OMCI_TL_FLOW_DIR_E;


/* OMCI TL generic flow data structure. */
typedef struct
{
    uint32_t                     in_use;          /* Whether current flow is in use                                           */
    uint32_t                     serviceId;       /* Unique OMCI layer 2 service ID                                           */
    OMCI_TL_FLOW_DIR_E           flowDir;         /* Flow direction, upsteam or downstream                                    */
    OMCI_TL_CAST_TYPE_E          castType;        /* Cast type, binary-unicast, multicast, broadcast                          */
    uint8_t                      srcPort;         /* Source UNI port number, used for upstream   unicast                      */
    uint8_t                      tgtPortBm;       /* Target UNI port bitmap, used for downstream unicast and multicast        */
    uint16_t                     isDefault;       /* Whether this rule is default rule                                        */
    OMCI_TL_LAN_TYPE_E           lanType;         /* LAN side UNI type: PPTP UNI, IP host or VEIP                             */
    OMCI_TL_TAG_TYPE_E           tagType;         /* Current Tag type : untagged, single tagged, double tagged                */
    OMCI_TL_VLAN_OP_MODE_E       opMode;          /* VLAN operation type                                                      */    
    OMCI_TL_PARSE_ETHTYPE_MODE_E ethTypeMode;     /* Ingress Ethernet filter mode : pass or filtered by ingress Ethernet type */
    uint32_t                     pbitsBm;         /* P-bits bitmap for untagged frame used by 802.1p mapper                   */    
    uint32_t                     ingressInnerTag; /* Ingress inner tag                                                        */
    uint32_t                     ingressInnerMask;/* Ingress inner tag mask                                                   */
    uint32_t                     ingressOuterTag; /* Ingress outer tag                                                        */
    uint32_t                     ingressOuterMask;/* Ingress outer tag mask                                                   */
    uint16_t                     ingressEtherType;/* Ingress Ethernet type                                                    */
    uint16_t                     dummy1;          /* dummy for byte alignment                                                 */
    uint32_t                     egressInnerTag;  /* Egress inner tag                                                         */
    uint32_t                     egressInnerMask; /* Egress inner tag mask                                                    */    
    uint32_t                     egressOuterTag;  /* Egress out tag                                                           */
    uint32_t                     egressOuterMask; /* Egress out tag  mask                                                     */    
    uint16_t                     tcont;           /* TCONT number                                                             */
    uint16_t                     gemPort;         /* GEM port number                                                          */
    uint8_t                      usQueue;         /* Upstream queue number                                                    */
    uint8_t                      dsQueue;         /* Downstream queue number                                                  */
    uint8_t                      isSetPnc;        /* Whether has set to PnC                                                   */
    uint8_t                      isSetCust;       /* Whether has set to mv_cust for flow mapping                              */

} OMCI_TL_FLOW_T;

/* Flow set */
typedef OMCI_TL_FLOW_T OMCI_TL_FLOW_SET_T[OMCI_TL_FLOW_DIR_NUM][OMCI_TL_MAX_FLOW_NUM]; 




/****************************************************************************** 
 *                        Function Declaration                                                                                                                                  
 ******************************************************************************/
/*******************************************************************************
* omci_tl_calc_unicast_flow()
*
* DESCRIPTION:      Calculate OMCI layer 2 generic flows and save to global 
*                   flow data arry.
*
* INPUTS:
*  serviceId        - Unique ID to layer 2 data service.
*  lan              - LAN side configuration, such as VLAN tag filter, 
*                     VLAN tag operation.
*  wan              - WAN side configuration, such as VLAN tag filter, 
*                     VLAN tag operation.
*  defVlanOp        - Default VLAN tag operation for untagged, tagged and double
*                     tagged frames, pass or discard them.

* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_calc_unicast_flow(uint32_t serviceId, OMCI_TL_LAN_T *lan, OMCI_TL_WAN_T *wan, OMCI_TL_DEFAULT_VLAN_OP_T *defVlanOp);

/*******************************************************************************
* omci_tl_calc_mc_flow()
*
* DESCRIPTION:      Calculate OMCI layer 2 multicast flows and save to global 
*                   flow data arry.
*
* INPUTS:
*  serviceId        - Unique ID to layer 2 data service.
*  lan              - LAN side configuration, such as VLAN tag filter, 
*                     VLAN tag operation.
*  wan              - WAN side configuration, such as VLAN tag filter, 
*                     VLAN tag operation.
*  defVlanOp        - Default VLAN tag operation for untagged, tagged and double
*                     tagged frames, pass or discard them.

* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_calc_mc_flow(uint32_t serviceId, OMCI_TL_LAN_T *lan, OMCI_TL_WAN_T *wan, OMCI_TL_DEFAULT_VLAN_OP_T *defVlanOp);

/*******************************************************************************
* omci_tl_del_flow()
*
* DESCRIPTION:      Clear OMCI generic flows.
*
* INPUTS:
*  serviceId        - Unique ID to layer 2 data service.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_del_flow(uint32_t serviceId);

/*******************************************************************************
* omci_tl_init_flow_db()
*
* DESCRIPTION:      Initilize OMCI generic flow database.
*
* INPUTS:
*  None
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_init_flow_db(void);

/*******************************************************************************
* omci_tl_optimise_flow()
*
* DESCRIPTION:      Optimise layer 2 flows, remove some redundant default rules. 
*
* INPUTS:
*  serviceId        - Unique ID to layer 2 data service.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_optimise_flow(uint32_t serviceId);

/*******************************************************************************
*  omci_tl_update_flow_map_set()
*
* DESCRIPTION:   Update GPON flow mapping data service.
*
* INPUTS:
*  serviceId    - Unique ID to layer 2 data service.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_update_flow_map_set(uint32_t serviceId);

/*******************************************************************************
* omci_tl_config_flow()
*
* DESCRIPTION:      Configure layer 2 generic flow to lower layer, 
*                   such as PnC flow.
*
* INPUTS:
*  serviceId        - Unique ID to layer 2 data service.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_config_flow(uint32_t serviceId);

/*******************************************************************************
* omci_tl_calc_dscp_map()
*
* DESCRIPTION:      Calculate DSCP pbits mapping.
*
*  serviceId        - Unique ID to layer 2 data service.
*  lan              - LAN side configuration, such as VLAN tag filter, 
*                     VLAN tag operation.
*  wan              - WAN side configuration, such as VLAN tag filter, 
*                     VLAN tag operation.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.

*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_calc_dscp_map(uint32_t serviceId, OMCI_TL_LAN_T *lan, OMCI_TL_WAN_T *wan);

/*******************************************************************************
* omci_tl_get_free_dscp_map_entry()
*
* DESCRIPTION:      Get a free DSCP pbits mapping entry.
*
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.

*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_DSCP_MAP_T *omci_tl_get_dscp_map_entry(void);

#endif /*__OMCI_TL_FLOW_HEADER__*/
