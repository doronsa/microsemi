/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_pon.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file encapsulates PON functions for OMCI translation **/
/**                layer adaption module .                                   **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   25/Aug/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_PON_HEADER__
#define __OMCI_TL_PON_HEADER__

/****************************************************************************** 
 *                        Marco and Type Definition                                                                                                                                  
 ******************************************************************************/






/****************************************************************************** 
 *                        Data Enum and Structure                                                                                                                                  
 ******************************************************************************/

/*Structure of optical transceiver table*/
typedef struct 
{
    UINT16     temperature;
    UINT16     supply_voltage;
    UINT16     bias_current;
    UINT16     tx_power;
    UINT16     rx_power;
} OMCI_OPTICAL_XVR_PARAM_T;


/****************************************************************************** 
 *                        Function Declaration                                                                                                                                  
 ******************************************************************************/
/*******************************************************************************
* omci_tl_set_gem_port_valid()
*
* DESCRIPTION:      Save service ID to local db.
*
* INPUTS:
*           gemPort - GPON GEM port ID.
8           status  - GPON GEM port status, 1:enable it, 0:disable GEM port
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
extern OMCI_TL_STATUS omci_tl_set_gem_port_valid(uint32_t gemPort, uint32_t status);
extern OMCI_TL_STATUS omci_tl_set_optical_level_alarm_threshold(UINT32 alarm_id, MECLASS meClassId, UINT16 meClassInstance, INT8 powerThreshold);
/******************************************************************************
 *
 * Function   : omci_tl_get_optical_param
 *              
 * Description: This function get Optical parameters
 *              
 * Parameters :  
 *
 * Returns    : OMCI_TL_STATUS
 *              
 ******************************************************************************/
extern OMCI_TL_STATUS omci_tl_get_optical_param(OMCI_OPTICAL_XVR_PARAM_T *xvr_param);

#endif /*__OMCI_TL_PON_HEADER__*/
