/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_cli.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file declare OMCI translation layer CLIs             **/
/**                                                                          **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   25/Aug/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_CLI_HEADER__
#define __OMCI_TL_CLI_HEADER__

/****************************************************************************** 
 *                        Macro and Type Definition                                                                                                                                  
 ******************************************************************************/

#define EXTRACT_VID(tci)     ((tci) & 0xFFF)
#define EXTRACT_PBITS(tci)   (((tci) >> 13) & 0x7)



/****************************************************************************** 
 *                        Data Enum and Structure                                                                                                                                  
 ******************************************************************************/
typedef enum
{
    OMCITLTABLETYPE_VLAN_OP, OMCITLTABLETYPE_VLAN_FILTER, OMCITLTABLETYPE_WAN, OMCITLTABLETYPE_LAN,
    OMCITLTABLETYPE_SERVICE
} OMCITLTABLETYPE_E;

//////////////////////////////////////////////////////////////////////////////////////
//                     VLAN Op definitions
//////////////////////////////////////////////////////////////////////////////////////
typedef enum 
{
    VLANOPTYPE_UNTAGGED, VLANOPTYPE_SINGLE_TAG, VLANOPTYPE_DOUBLE_TAG
} VLANOPTYPE_E;

// Use empty name as exist-not-exist. 
typedef struct
{
    char                   name[16];
    OMCI_TL_TAG_TYPE_E     tagType;
    OMCI_TL_VLAN_OP_CFG_T  vlanOpCfg;
} CliOmciTlVlanOp_S;
 
#define MAX_CLI_VLAN_OPS         64
typedef struct
{
    CliOmciTlVlanOp_S recordAra[MAX_CLI_VLAN_OPS];
} CliOmciTlVlanOpDb_S;
 
extern CliOmciTlVlanOp_S *findFree_CliOmciTlVlanOp();
extern CliOmciTlVlanOp_S *findByKey_CliOmciTlVlanOp(char *name);
extern bool deleteByKey_CliOmciTlVlanOp(char *name);
extern void displayByKey_CliOmciTlVlanOp(char *name);

//////////////////////////////////////////////////////////////////////////////////////
//                     VLAN Filter Definitions
//////////////////////////////////////////////////////////////////////////////////////

// Use empty name as exist-not-exist. 
typedef struct
{
    char                   name[16];
    OMCI_TL_VLAN_FILTER_T  vlanFilter;
} CliOmciTlVlanFilter_S;

#define MAX_CLI_VLAN_FILTERS         32
typedef struct
{
    CliOmciTlVlanFilter_S recordAra[MAX_CLI_VLAN_FILTERS];
} CliOmciTlVlanFilterDb_S;
 
extern CliOmciTlVlanFilter_S *findFree_CliOmciTlVlanFilter();
extern CliOmciTlVlanFilter_S *findByKey_CliOmciTlVlanFilter(char *name);
extern bool deleteByKey_CliOmciTlVlanFilter(char *name);
extern void displayByKey_CliOmciTlVlanFilter(char *name);

//////////////////////////////////////////////////////////////////////////////////////
//                     WAN Definitions
//////////////////////////////////////////////////////////////////////////////////////

// Use empty name as exist-not-exist. 
typedef struct
{
    char                   name[16];
    OMCI_TL_WAN_T          wan;
} CliOmciTlWan_S;

#define MAX_CLI_WANS                32
typedef struct
{
    CliOmciTlWan_S  recordAra[MAX_CLI_WANS];
} CliOmciTlWanDb_S;


extern CliOmciTlWan_S *findFree_CliOmciTlWan();
extern CliOmciTlWan_S *findByKey_CliOmciTlWan(char *name);
extern bool deleteByKey_CliOmciTlWan(char *name);
extern void displayByKey_CliOmciTlWan(char *name);

//////////////////////////////////////////////////////////////////////////////////////
//                     LAN Definitions
//////////////////////////////////////////////////////////////////////////////////////

// Use empty name as exist-not-exist. 
typedef struct
{
    char                   name[16];
    OMCI_TL_LAN_T          lan;
} CliOmciTlLan_S;

#define MAX_CLI_LANS                12
typedef struct
{
    CliOmciTlLan_S  recordAra[MAX_CLI_LANS];
} CliOmciTlLanDb_S;


extern CliOmciTlLan_S *findFree_CliOmciTlLan();
extern CliOmciTlLan_S *findByKey_CliOmciTlLan(char *name);
extern bool deleteByKey_CliOmciTlLan(char *name);
extern void displayByKey_CliOmciTlLan(char *name);

//////////////////////////////////////////////////////////////////////////////////////
//                     Service Definitions
//////////////////////////////////////////////////////////////////////////////////////

// Use empty name as exist-not-exist. 
typedef struct
{
    char                   name[16];
    OMCI_TL_SERVICE_T      service;
} CliOmciTlService_S;

#define MAX_CLI_SERVICES     8
typedef struct
{
    CliOmciTlService_S  recordAra[MAX_CLI_SERVICES];
} CliOmciTlServiceDb_S;


extern CliOmciTlService_S *findFree_CliOmciTlService();
extern CliOmciTlService_S *findByKey_CliOmciTlService(char *name);
extern bool deleteByKey_CliOmciTlService(char *name);
extern void displayByKey_CliOmciTlService(char *name);

/****************************************************************************** 
 *                        Function Declaration                                                                                                                                  
 ******************************************************************************/

extern void cliOmciTlDbInit();

/******************************************************************************
 *
 * Function   : displayOmciTlService 
 *              
 * Description: This function displays a OMCI_TL_SERVICE_T structure
 *
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/
extern void displayOmciTlService(OMCI_TL_SERVICE_T *pservice);

/******************************************************************************
 *
 * Function   : displayVlanOp 
 *              
 * Description: This function displays VLAN tag operation
 *
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/
extern void displayVlanOp(OMCI_TL_VLAN_OP_T *p_dsVlanOp, OMCI_TL_VLAN_OP_T *p_usVlanOp);


#endif /*__OMCI_TL_CLI_HEADER__*/
