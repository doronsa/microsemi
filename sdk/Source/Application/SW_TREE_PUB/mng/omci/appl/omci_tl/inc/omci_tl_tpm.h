/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI translation layer                                    **/
/**                                                                          **/
/**  FILE        : omci_tl_tpm.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file encapsulates TPM functions for OMCI translation **/
/**                layer adaption module .                                   **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *
 *         Victor  - initial version created.   25/Aug/2011           
 *                                                                              
 ******************************************************************************/
#ifndef __OMCI_TL_TPM_HEADER__
#define __OMCI_TL_TPM_HEADER__

#include "mv_cust_api.h"

/****************************************************************************** 
 *                        Marco and Type Definition                                                                                                                                  
 ******************************************************************************/
#define OMCI_TL_VLAN_VID_OFFS             (0)
#define OMCI_TL_VLAN_VID_BITS             (12)
#define OMCI_TL_VLAN_VID_MASK             ((1 << OMCI_TL_VLAN_VID_BITS) - 1)
#define OMCI_TL_VLAN_VID_SHIFT_MASK       (((1 << OMCI_TL_VLAN_VID_BITS) - 1) << OMCI_TL_VLAN_VID_OFFS)

#define OMCI_TL_VLAN_CFI_OFFS             (12)
#define OMCI_TL_VLAN_CFI_BITS             (1)
#define OMCI_TL_VLAN_CFI_MASK             ((1 << OMCI_TL_VLAN_CFI_BITS) - 1)
#define OMCI_TL_VLAN_CFI_SHIFT_MASK       (1 << OMCI_TL_VLAN_CFI_OFFS)

#define OMCI_TL_VLAN_PBIT_OFFS            (13)
#define OMCI_TL_VLAN_PBIT_BITS            (3)
#define OMCI_TL_VLAN_PBIT_MASK            ((1 << OMCI_TL_VLAN_PBIT_BITS) - 1)
#define OMCI_TL_VLAN_PBIT_SHIFT_MASK      (((1 << OMCI_TL_VLAN_PBIT_BITS) - 1) << OMCI_TL_VLAN_PBIT_OFFS)
    
#define OMCI_TL_VLAN_TPID_OFFS            (16)
#define OMCI_TL_VLAN_TPID_BITS            (16)
#define OMCI_TL_VLAN_TPID_MASK            ((1 << OMCI_TL_VLAN_TPID_BITS) - 1)
#define OMCI_TL_VLAN_TPID_SHIFT_MASK      (((1 << OMCI_TL_VLAN_TPID_BITS) - 1) << OMCI_TL_VLAN_TPID_OFFS)

#define OMCI_TL_IPV4_DSCP_MASK            (0x3f)
#define OMCI_TL_VLAN_TCI_MASK             (0xffff)

#define OMCI_TL_VLAN_VID_PBIT_MASK        (0xefff)

#define OMCI_TL_NEW_VID                   (0xfff)
#define OMCI_TL_NEW_CFI                   (0x1)
#define OMCI_TL_NEW_PBIT                  (0x7)
#define OMCI_TL_NEW_TPID                  (0xffff)

/* Followings are TPM modification related definitions */
#define OMCI_TL_MOD_NEW_VID               (0xffff)
#define OMCI_TL_MOD_NEW_CFI               (0xff)
#define OMCI_TL_MOD_NEW_PBIT              (0xff)
#define OMCI_TL_MOD_NEW_TPID              (0xffff)

#define OMCI_TL_TGT_PORT_SHIFT            (16)

#define TPM_OMCC_PORT_INIT_VALUE          (0xffff)

#define OMCI_TL_DEFAULT_PNC_RULE_NUM      (2)
#define OMCI_TL_PON_DEVICE_NAME           "pon0"
#define OMCI_TL_ETH_DEVICE_NAME           "eth0"

#define OMCI_TL_PNC_TYPE_L2               (0)
#define OMCI_TL_PNC_TYPE_ETH_TYPE         (1)
#define OMCI_TL_PNC_TYPE_IPV4             (2)


/****************************************************************************** 
 *                        Data Enum and Structure                                                                                                                                  
 ******************************************************************************/
/* OMCI TL PnC rule definition, to save data and */
typedef struct
{
    uint32_t                 in_use;     /* Whether current entry has been used                       */
    uint32_t                 service_id; /* Unique OMCI layer 2 service ID                            */                                 
    tpm_src_port_type_t      src_port;   /* Source port                                               */                                 
    uint16_t                 gem_port;   /* GEM port number                                           */
    uint16_t                 pnc_type;   /* dummy for byte alignment                                  */
    uint32_t                 rule_idx;   /* Rule index returned by TPM API                            */
    uint32_t                 is_default; /* Whether it is a default rule                              */
    OMCI_TL_CAST_TYPE_E      cast_type;  /* Flow type which has priority when create PnC rules,       */ 
                                         /* priority: multicast > unicast > default transparent rules */
}OMCI_TL_PNC_RULE_T;

/* OMCI TL mv_cust flow mapping structure */
typedef struct
{
    uint32_t                 service_id; /* Unique OMCI layer 2 service ID       */                                 
    mv_cust_ioctl_flow_map_t flow_map;   /* mv_cust flow mapping                 */                                 

}OMCI_TL_FLOW_MAP_RULE_T;

/****************************************************************************** 
 *                        Function Declaration                                                                                                                                  
 ******************************************************************************/
/*******************************************************************************
*  omci_tl_get_cust_flow_area()
*
* DESCRIPTION:   Get cust flow area generated by OMCI.
*
* INPUTS:
*  dir               - Direction.
*
* OUTPUTS:
*  vlan_entry_params - The pointer to the beginning of cust flow area.
*  num               - Number of total cust flows, including valid and invalid one.
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS  *omci_tl_get_cust_flow_area(uint32_t dir, OMCI_TL_VLAN_TRANS_ENTRY_T *vlan_entry_params, uint32_t *num);

/*******************************************************************************
*  omci_tl_create_pnc_flow_set()
*
* DESCRIPTION:      Create layer 2 data service.
*
* INPUTS:
*  flowSet          - The set of  PnC rules.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_create_pnc_flow_set(OMCI_TL_FLOW_SET_T *flowSet);

/*******************************************************************************
* omci_tl_del_pnc_flow_set()
*
* DESCRIPTION:      Delete PnC flows by serviceID.
*
* INPUTS:
*  serviceId        - layer 2 data service ID.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_del_pnc_flow_set(uint32_t serviceId);

/*******************************************************************************
*  omci_tl_init_flow_map_db()
*
* DESCRIPTION:      Init GPON flow mapping data.
*
* INPUTS:
*  None.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_init_flow_map_db(void);

/*******************************************************************************
*  omci_tl_create_flow_map_set()
*
* DESCRIPTION:      Create GPON flow mapping data service.
*
* INPUTS:
*  flowSet          - The set of  generic rules.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_create_flow_map_set(OMCI_TL_FLOW_SET_T *flowSet);

/*******************************************************************************
* omci_tl_display_flow_map_db()
*
* DESCRIPTION:      Displays flow mapping entry DB. 
*
* INPUTS:
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  void
*
* COMMENTS:
*
*******************************************************************************/
void omci_tl_display_flow_map_db(FILE *fildes);

/*******************************************************************************
* omci_tl_add_default_pnc_rule()
*
* DESCRIPTION: Add default PnC rules depends on ONU mode.
*
* INPUTS:
*  None
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_add_default_pnc_rule(void);

/*******************************************************************************
* omci_tl_set_port_isolation()
*
* DESCRIPTION: Set UNI port isolation.
*
* INPUTS:
*  None
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_set_port_isolation(void);

/*******************************************************************************
* omci_tl_set_mac_learn()
*
* DESCRIPTION: Set UNI port mac address learning.
*
* INPUTS:
*  None
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_set_mac_learn(void);

/*******************************************************************************
* omci_tl_set_mc_flood()
*
* DESCRIPTION: Set UNI port multicast flooding.
*
* INPUTS:
*  None
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_set_mc_flood(void);

/*******************************************************************************
*  omci_tl_config_ip_host_virt_port()
*
* DESCRIPTION:      Create GPON VoIP virtual interface.
*
* INPUTS:
*  flowSet          - The set of  generic rules.
*
* OUTPUTS:
*  None      
*
* RETURNS:
*  On success, the function returns OMCI_TL_RET_OK. On error different types are 
*  returned according to the case.
*
* COMMENTS:
*
*******************************************************************************/
OMCI_TL_STATUS omci_tl_config_ip_host_virt_port(OMCI_TL_FLOW_SET_T *flowSet);


#endif /*__OMCI_TL_TPM_HEADER__*/
