/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        :  GponSystemCalls.h                                        **/
/**                                                                          **/
/**  DESCRIPTION : This file contains GPON wrapper prototypes                **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    25Dec07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCGponSystemCallsh
#define __INCGponSystemCallsh


extern bool gponApi_getOnuId       (UINT32 *onuId);
extern bool gponApi_getOmccPort    (UINT32 *omccPort);
extern bool gponApi_setTcontAllocId(UINT32 tcontNum, UINT32 allocId);
extern bool gponApi_resetTconts    ();
extern bool gponApi_isPonInKernel  ();
extern bool gponApi_isPonSynced    (bool *isPonSynced);

#endif


