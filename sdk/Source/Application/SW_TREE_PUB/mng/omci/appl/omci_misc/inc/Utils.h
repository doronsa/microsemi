/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : Utils.h                                                   **/
/**                                                                          **/
/**  DESCRIPTION : This file has utility function prototypes                 **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/


#ifndef __INCUtilsh
#define __INCUtilsh


void convertN2HShort(void *srcValPtr, void *destValPtr);
void convertN2HLong (void *srcValPtr, void *destValPtr);
void convertN2HDoubleLong(void *srcValPtr, void *destValPtr);



#endif
