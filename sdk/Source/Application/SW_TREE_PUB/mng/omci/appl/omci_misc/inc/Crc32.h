/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        :  Crc32.h                                                  **/
/**                                                                          **/
/**  DESCRIPTION : This file contains CRC32 prototypes                       **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    25Dec07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCCrc32h
#define __INCCrc32h


#define crc_t unsigned int

#if 0
/* Returns crc32 of data block */
crc_t crc32_sz(char *buf, int size);
#endif



/* Incremental crc32 */
void incrementalCrc32_sz(char *buf, int size, unsigned int *accumCrc32);

/* Initialize the CRC table */
void newGenCrcTable();

#endif

