/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VoIPVoiceCtp.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file has VoIP Voice CTP definitions                  **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVoIPVoiceCtph
#define __INCVoIPVoiceCtph



// VOIP Voice CTP Attribute bits
#define BM_MSB_userProtocolPointer         (0x80)
#define BM_MSB_pptpPointer                 (0x40) 
#define BM_MSB_voipMediaProfilePointer     (0x20)
#define BM_MSB_signalingCode               (0x10)



#define BM_MSB_GETATTRIBS_VOIPVOICECTP (BM_MSB_userProtocolPointer | BM_MSB_pptpPointer | BM_MSB_voipMediaProfilePointer | BM_MSB_signalingCode)

#define BM_MSB_SETATTRIBS_VOIPVOICECTP (BM_MSB_voipMediaProfilePointer)

#define BM_MSB_CREATEATTRIBS_VOIPVOICECTP (BM_MSB_userProtocolPointer | BM_MSB_pptpPointer | BM_MSB_voipMediaProfilePointer | BM_MSB_signalingCode)

// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT16   userProtocolPointer;
    UINT16   pptpPointer;
    UINT16   voipMediaProfilePointer;
    UINT8    signalingCode;
} VoIPVoiceCtpME;


// messageContents - Create VoIP Voice CTP Command
#pragma pack(1)
typedef struct
{
    UINT16   userProtocolPointer;
    UINT16   pptpPointer;
    UINT16   voipMediaProfilePointer;
    UINT8    signalingCode;
} CreateVoIPVoiceCtp_s;
#pragma pack(0)

MeClassDef_S voIPVoiceCtp_MeClassDef;

#endif
