/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : GalEthernetPmHistoryData.h                                **/
/**                                                                          **/
/**  DESCRIPTION : This file implements GAL Ethernet PM History Data ME      **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCGalEthernetPmHistoryDatah
#define __INCGalEthernetPmHistoryDatah



// GAL Ethernet PM History Data Attribute bits
#define BM_MSB_intervalEndTime                      (0X80)
#define BM_MSB_thresholdData12Id                    (0X40)
#define BM_MSB_discardedFrames                      (0X20)

#define BM_MSB_GETATTRIBS_GALETHERNETPMHISTORYDATA (BM_MSB_intervalEndTime | BM_MSB_thresholdData12Id | BM_MSB_discardedFrames)

#define BM_MSB_SETATTRIBS_GALETHERNETPMHISTORYDATA (BM_MSB_thresholdData12Id)

#define BM_MSB_CREATEATTRIBS_GALETHERNETPMHISTORYDATA (BM_MSB_thresholdData12Id)


// messageContents - GAL Ethernet Prot Mon Create Command
#pragma pack(1)
typedef struct
{
    UINT16 thresholdData12Id;
} CreateGalEthernetPmHistoryDataCmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    tcaBitMask[2];
    UINT8    intervalEndTime;
    UINT16   thresholdData12Id;
    UINT32   discardedFrames;
} GalEthernetPmHistoryDataME;


extern MeClassDef_S galEthernetPmHistoryData_MeClassDef;


#endif
