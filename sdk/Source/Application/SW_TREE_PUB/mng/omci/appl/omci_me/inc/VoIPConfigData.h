/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VoIPConfigData.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : This file has VoIP Config Data definitions                **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVoIPConfigDatah
#define __INCVoIPConfigDatah



// VOIP Config Data Attribute bits
#define BM_MSB_availableSignalingProtocols (0x80)
#define BM_MSB_signalingProtocolUsed       (0x40) 
#define BM_MSB_availableVoIPConfigMethods  (0x20)
#define BM_MSB_voipConfigMethodUsed        (0x10)
#define BM_MSB_voipConfigAddressPointer    (0x08)
#define BM_MSB_voipConfigState             (0x04)
#define BM_MSB_retrieveProfile             (0x02)
#define BM_MSB_profileVersion              (0x01)



#define BM_MSB_GETATTRIBS_VOIPCONFIGDATA BM_MSB_availableSignalingProtocols | BM_MSB_signalingProtocolUsed | BM_MSB_availableVoIPConfigMethods | BM_MSB_voipConfigMethodUsed | BM_MSB_voipConfigAddressPointer | BM_MSB_voipConfigState | BM_MSB_profileVersion

#define BM_MSB_SETATTRIBS_VOIPCONFIGDATA BM_MSB_signalingProtocolUsed | BM_MSB_voipConfigMethodUsed | BM_MSB_voipConfigAddressPointer | BM_MSB_retrieveProfile

// Used in MIB Upload snapshot 
#define VOIPCONFIGDATA_UPLOAD_CELL1_MSB_MASK (BM_MSB_availableSignalingProtocols | BM_MSB_signalingProtocolUsed | BM_MSB_availableVoIPConfigMethods | BM_MSB_voipConfigMethodUsed | BM_MSB_voipConfigAddressPointer | BM_MSB_voipConfigState)

#define VOIPCONFIGDATA_UPLOAD_CELL2_MSB_MASK (BM_MSB_profileVersion)


// Bitmaps for available signaling protocols
#define BM_AVAILSIGPROTO_NOVOIP            0
#define BM_AVAILSIGPROTO_SIP               0x1
#define BM_AVAILSIGPROTO_H248              0x2
#define BM_AVAILSIGPROTO_MGCP              0x4


// OMCI Entity DB

#define VOIPCONFIGMETHODS_SIZE             4
#define PROFILEVERSION_SIZE                25
#define VOIPCONFIGDATA_ALARMARA_SIZE       2
typedef struct
{
    MeInst_S meInst;
    UINT8    alarmsBitmap[OMCI_ALARM_BIT_MAP_LEN];
    UINT8    availableSignalingProtocols;
    UINT8    signalingProtocolUsed;
    UINT8    availableVoIPConfigMethods[VOIPCONFIGMETHODS_SIZE];
    UINT8    voipConfigMethodUsed;
    UINT16   voipConfigAddressPointer;
    UINT8    voipConfigState;
    UINT8    retrieveProfile;
    UINT8    profileVersion[PROFILEVERSION_SIZE];
} VoIPConfigDataME;



typedef struct
{
    UINT8   availableSignalingProtocols;
    UINT8   availableVoIPConfigMethods[VOIPCONFIGMETHODS_SIZE];
    UINT8   signalingProtocolUsed;
    UINT8   voipConfigMethodUsed;
} CreateVoIPConfigData_s;

OMCIPROCSTATUS createVoIPConfigDataME(UINT16 meInstance, CreateVoIPConfigData_s *pcreateVoIPCfgData);


MeClassDef_S voIPConfigData_MeClassDef;


#endif
