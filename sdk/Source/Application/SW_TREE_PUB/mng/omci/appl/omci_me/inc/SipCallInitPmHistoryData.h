/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : SipCallInitPmHistoryData.h                                **/
/**                                                                          **/
/**  DESCRIPTION : This file has SIP Call Init PM History Data definitions   **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCSipCallInitPmHistoryDatah
#define __INCSipCallInitPmHistoryDatah


// Sip Call Initiation PM History Data Attribute bits
#define BM_MSB_sipcall_intervalEndTime              (0X80)
#define BM_MSB_sipcall_thresholdDataId              (0X40)
#define BM_MSB_failedToValidateCounter              (0X20)
#define BM_MSB_timeoutCounter                       (0X10)
#define BM_MSB_failureReceivedCounter               (0X08)
#define BM_MSB_failedToAuthenticateCounter          (0X04)



#define BM_MSB_GETATTRIBS_SIPCALLINITPMHISTORYDATA (BM_MSB_sipcall_intervalEndTime | BM_MSB_sipcall_thresholdDataId | BM_MSB_failedToValidateCounter | BM_MSB_timeoutCounter | BM_MSB_failureReceivedCounter | BM_MSB_failedToAuthenticateCounter)

#define BM_MSB_SETATTRIBS_SIPCALLINITPMHISTORYDATA (BM_MSB_sipcall_thresholdDataId)

#define BM_MSB_CREATEATTRIBS_SIPCALLINITPMHISTORYDATA (BM_MSB_sipcall_thresholdDataId)


// Crea message contents
#pragma pack(1)
typedef struct
{
    UINT16 thresholdDataId;
} CreateSipCallInitPmHistoryDataCmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    tcaBitMask[2];
    UINT8    intervalEndTime;
    UINT16   thresholdDataId;
    UINT32   failedToValidateCounter;
    UINT32   timeoutCounter;
    UINT32   failureReceivedCounter;
    UINT32   failedToAuthenticateCounter;
} SipCallInitPmHistoryDataME;


extern MeClassDef_S sipCallInitPmHistoryData_MeClassDef;


#endif
