/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : ThresholdData2.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : This file has Threshold Data 2 definitions                **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCThresholdData2h
#define __INCThresholdData2h



// Threshold Data Attribute bits
#define BM_MSB_threshold2Value8                      (0X80)
#define BM_MSB_threshold2Value9                      (0X40)
#define BM_MSB_threshold2Value10                     (0X20)
#define BM_MSB_threshold2Value11                     (0X10)
#define BM_MSB_threshold2Value12                     (0X08)
#define BM_MSB_threshold2Value13                     (0X04)
#define BM_MSB_threshold2Value14                     (0X02)


#define BM_MSB_GETATTRIBS_THRESHOLDDATA2 (BM_MSB_threshold2Value8 | BM_MSB_threshold2Value9 | BM_MSB_threshold2Value10 | BM_MSB_threshold2Value11 | BM_MSB_threshold2Value12 | BM_MSB_threshold2Value13 | BM_MSB_threshold2Value14)

#define BM_MSB_SETATTRIBS_THRESHOLDDATA2 (BM_MSB_threshold2Value8 | BM_MSB_threshold2Value9 | BM_MSB_threshold2Value10 | BM_MSB_threshold2Value11 | BM_MSB_threshold2Value12 | BM_MSB_threshold2Value13 | BM_MSB_threshold2Value14)

#define BM_MSB_CREATEATTRIBS_THRESHOLDDATA2 (BM_MSB_threshold2Value8 | BM_MSB_threshold2Value9 | BM_MSB_threshold2Value10 | BM_MSB_threshold2Value11 | BM_MSB_threshold2Value12 | BM_MSB_threshold2Value13 | BM_MSB_threshold2Value14)


#define TD2CELL2_MSB_MASK BM_MSB_threshold2Value8 | BM_MSB_threshold2Value9 | BM_MSB_threshold2Value10 | BM_MSB_threshold2Value11 | BM_MSB_threshold2Value12 | BM_MSB_threshold2Value13

#define TD2CELL3_MSB_MASK BM_MSB_threshold2Value14


// messageContents - Create Threshold Data Command
#pragma pack(1)
typedef struct
{
    UINT32 thresholdValue8;
    UINT32 thresholdValue9;
    UINT32 thresholdValue10;
    UINT32 thresholdValue11;
    UINT32 thresholdValue12;
    UINT32 thresholdValue13;
    UINT32 thresholdValue14;
} CreateThresholdData2Cmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT32   thresholdValue8;
    UINT32   thresholdValue9;
    UINT32   thresholdValue10;
    UINT32   thresholdValue11;
    UINT32   thresholdValue12;
    UINT32   thresholdValue13;
    UINT32   thresholdValue14;
} ThresholdData2ME;


MeClassDef_S thresholdData2_MeClassDef; 


#endif
