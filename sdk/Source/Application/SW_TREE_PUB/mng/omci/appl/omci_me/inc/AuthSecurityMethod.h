/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : AuthSecurityMethod.h                                      **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Auth Security Method ME              **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCAuthSecurityMethodh
#define __INCAuthSecurityMethodh


// AuthSecurityMethod Attribute bits
#define BM_MSB_validationScheme   (0x80)
#define BM_MSB_username1          (0x40) 
#define BM_MSB_password           (0x20) 
#define BM_MSB_realm              (0x10) 
#define BM_MSB_username2          (0x08) 


#define BM_MSB_GETATTRIBS_AUTHSECURITYMETHOD (BM_MSB_validationScheme | BM_MSB_username1 | BM_MSB_password | BM_MSB_realm | BM_MSB_username2)

#define BM_MSB_SETATTRIBS_AUTHSECURITYMETHOD (BM_MSB_validationScheme | BM_MSB_username1 | BM_MSB_password | BM_MSB_realm | BM_MSB_username2)


// Used in MIB Upload snapshot 
#define AUTHSECURITYMETHOD_UPLOAD_CELL1_MSB_MASK  (BM_MSB_validationScheme)
#define AUTHSECURITYMETHOD_UPLOAD_CELL2_MSB_MASK  (BM_MSB_username1)
#define AUTHSECURITYMETHOD_UPLOAD_CELL3_MSB_MASK  (BM_MSB_password)
#define AUTHSECURITYMETHOD_UPLOAD_CELL4_MSB_MASK  (BM_MSB_realm)
#define AUTHSECURITYMETHOD_UPLOAD_CELL5_MSB_MASK  (BM_MSB_username2)




// OMCI Entity DB

#define PART_SIZE                        25

typedef struct
{
    MeInst_S meInst;
    UINT8    validationScheme;
    UINT8    username1[PART_SIZE];
    UINT8    password[PART_SIZE];
    UINT8    realm[PART_SIZE];
    UINT8    username2[PART_SIZE];
} AuthSecurityMethodME;



MeClassDef_S authSecurityMethod_MeClassDef;


#endif
