/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : MngdEntity.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the Managed Entity ME                **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    9Mar08      zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCMngdEntityh
#define __INCMngdEntityh


typedef enum
{
    MECREATEDBY_ONT=1, MECREATEDBY_OLT=2, MECREATEDBY_BOTH=3
} MECREATEDBY;


typedef enum
{
    MESUPPORT_FULL=1, MESUPPORT_UNSUPPORTED=2, MESUPPORT_PARTIAL=3, MESUPPORT_NOUNDERLYING=4
} MESUPPORT;



// Managed Entity Attribute bits
#define BM_MSB_name                                 (0X80)
#define BM_MSB_attributesTable                      (0X40)
#define BM_MSB_access                               (0X20)
#define BM_MSB_alarmsTable                          (0X10)
#define BM_MSB_avcsTable                            (0X08)
#define BM_MSB_actions                              (0X04)
#define BM_MSB_instancesTable                       (0X02)
#define BM_MSB_support                              (0X01)

#define BM_MSB_GETATTRIBS_MNDGENTITY (BM_MSB_name | BM_MSB_attributesTable | BM_MSB_access | BM_MSB_alarmsTable | BM_MSB_avcsTable | BM_MSB_actions | BM_MSB_instancesTable | BM_MSB_support)


#define NAMEARA_LENGTH                  25
#define ATTRIBUTESTABLEARA_LENGTH       32   // Max. 16 attributes
#define ALARMSTABLEARA_LENGTH           12   // Max. supported alarms
#define AVCSTABLEARA_LENGTH             6    // Max. AVCs per any ME
#define ACTIONSARA_LENGTH               4    // Max. OMCI commans  is 32
#define INSTANCESTABLEARA_LENGTH        240  // This value divide by 2 is the maximum expected # MEs of any one ME class

// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    char     name[NAMEARA_LENGTH];
    UINT8    attributesTable[ATTRIBUTESTABLEARA_LENGTH];
    UINT32   attributesTableSize;
    UINT8    access;
    UINT8    alarmsTable[ALARMSTABLEARA_LENGTH];
    UINT32   alarmsTableSize;
    UINT8    avcsTable[AVCSTABLEARA_LENGTH];
    UINT32   avcsTableSize;
    UINT8    actions[ACTIONSARA_LENGTH];
    UINT8    instancesTable[INSTANCESTABLEARA_LENGTH];
    UINT32   instancesTableSize;
    UINT8    support;
    UINT16   lastMeClassInvoked;
} MngdEntityMe;


extern MeClassDef_S mngdEntityMe_MeClassDef;

OMCIPROCSTATUS createMngdEntityMe(UINT16 meInstance);


#endif
