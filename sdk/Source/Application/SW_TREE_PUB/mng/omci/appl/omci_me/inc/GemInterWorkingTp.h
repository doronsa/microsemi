/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : GemInterWorkingTp.h                                       **/
/**                                                                          **/
/**  DESCRIPTION : This file implements GEM InterWorking TP ME               **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCGemInterWorkingTph
#define __INCGemInterWorkingTph


// GEM InterWorking TP Attribute bits
#define BM_MSB_gemPortNetworkCtpConnectivityPointer (0x80)
#define BM_MSB_interworkingOption                   (0x40)
#define BM_MSB_serviceProfilePointer                (0x20) 
#define BM_MSB_interworkingTerminationPointPointer  (0x10)
#define BM_MSB_pptpCounter                          (0x08) 
#define BM_MSB_operationalState                     (0x04)  
#define BM_MSB_galProfilePointer                    (0x02)
#define BM_MSB_galLoopbackConfiguration             (0x01)



#define BM_MSB_GETATTRIBS_GEMINTERWORKINGTP (BM_MSB_gemPortNetworkCtpConnectivityPointer | BM_MSB_interworkingOption | BM_MSB_serviceProfilePointer | BM_MSB_interworkingTerminationPointPointer | BM_MSB_pptpCounter | BM_MSB_operationalState | BM_MSB_galProfilePointer | BM_MSB_galLoopbackConfiguration)
 
#define BM_MSB_SETATTRIBS_GEMINTERWORKINGTP (BM_MSB_gemPortNetworkCtpConnectivityPointer | BM_MSB_interworkingOption | BM_MSB_serviceProfilePointer | BM_MSB_interworkingTerminationPointPointer | BM_MSB_galProfilePointer | BM_MSB_galLoopbackConfiguration)

#define BM_MSB_CREATEATTRIBS_GEMINTERWORKINGTP (BM_MSB_gemPortNetworkCtpConnectivityPointer | BM_MSB_interworkingOption | BM_MSB_serviceProfilePointer | BM_MSB_interworkingTerminationPointPointer | BM_MSB_galProfilePointer)


typedef enum
{
    GALLOOPBACKCFG_NONE =0, GALLOOPBACKCFG_LPBK
} GALLOOPBACKCFG;

// OMCI Entity DB

#define GEMIWTP_ALARMARA_SIZE        1
typedef struct
{
    MeInst_S meInst;
    UINT8    alarmsBitmap[OMCI_ALARM_BIT_MAP_LEN];
    UINT16   gemPortNetworkCtpConnectivityPointer;
    UINT8    interworkingOption;
    UINT16   serviceProfilePointer;
    UINT16   interworkingTerminationPointPointer;
    UINT8    pptpCounter;
    UINT8    operationalState;
    UINT16   galProfilePointer;
    UINT8    galLoopbackConfiguration;
} GemInterWorkingTpME;



// messageContents - Create InterWorking VCC TP Command
#pragma pack(1)
typedef struct
{
    UINT16 gemPortNetworkCtpConnectivityPointer;
    UINT8  interworkingOption;
    UINT16 serviceProfilePointer;
    UINT16 interworkingTerminationPointPointer;
    UINT16 galProfilePointer;
} CreateGemInterWorkingTp_s;
#pragma pack(0)



extern GemInterWorkingTpME *getGemIwTpByGalProfilePointer(UINT16 aalProfilePointer, UINT8 interworkingOption);
extern GemInterWorkingTpME *getGemIwTpByServiceProfilePointer(UINT16 serviceProfilePointer, UINT8 interworkingOption);

extern bool dealWithGemIwTpDeletion(GemInterWorkingTpME *piw);

MeClassDef_S gemInterworkingTp_MeClassDef;


#endif
