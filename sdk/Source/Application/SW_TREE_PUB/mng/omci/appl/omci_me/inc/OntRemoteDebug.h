/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OntRemoteDebug.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : This file is the Remote Debug ME include                  **/
/**                                                                          **/
/******4************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    4May06     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCOntRemoteDebugh
#define __INCOntRemoteDebugh


// ONT Remote Debug Attribute bits
#define BM_MSB_commandFormat                        (0x80)
#define BM_MSB_command                              (0x40)
#define BM_MSB_reply                                (0x20)

#define BM_MSB_GETATTRIBS_ONTREMOTEDEBUG BM_MSB_commandFormat | BM_MSB_reply

#define BM_MSB_SETATTRIBS_ONTREMOTEDEBUG BM_MSB_command


// OMCI Entity DB

#define COMMAND_SIZE         25
#define DUMMY_REPLY_SIZE     4
typedef struct
{
    MeInst_S meInst;
    UINT8    commandFormat;
    UINT8    command[COMMAND_SIZE+1];
    UINT8    reply[DUMMY_REPLY_SIZE];                // Remote CLI reply buffer is implemented elsewhere
    UINT32   replySize;
} OntRemoteDebugME;



OMCIPROCSTATUS createOntRemoteDebugME(UINT16 meInstance);


MeClassDef_S ontRemoteDebug_MeClassDef;


#endif
