/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : Mapper8021pServiceProfile.h                               **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Mapper 8021p Service Profile ME      **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    19Jun06     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCMapper8021pServiceProfileh
#define __INCMapper8021pServiceProfileh


// Mapper 8021p Service Profile Attribute bits
#define BM_MSB_pptpUniPointer                       (0x80)
#define BM_MSB_iwTpPtrPriority0                     (0x40)
#define BM_MSB_iwTpPtrPriority1                     (0x20) 
#define BM_MSB_iwTpPtrPriority2                     (0x10)
#define BM_MSB_iwTpPtrPriority3                     (0x08) 
#define BM_MSB_iwTpPtrPriority4                     (0x04)  
#define BM_MSB_iwTpPtrPriority5                     (0x02)
#define BM_MSB_iwTpPtrPriority6                     (0x01)


#define BM_LSB_iwTpPtrPriority7                     (0x80)
#define BM_LSB_unmarkedFrameOption                  (0x40)
#define BM_LSB_dscpToPBitMapping                    (0x20) 
#define BM_LSB_defaultPBitMarking                   (0x10)
#define BM_LSB_tpType                               (0x08) 


#define BM_MSB_GETATTRIBS_MAPPER8021PSERVICEPROFILE (BM_MSB_pptpUniPointer | BM_MSB_iwTpPtrPriority0 | BM_MSB_iwTpPtrPriority1 | BM_MSB_iwTpPtrPriority2 | BM_MSB_iwTpPtrPriority3 | BM_MSB_iwTpPtrPriority4 | BM_MSB_iwTpPtrPriority5 | BM_MSB_iwTpPtrPriority6)
#define BM_LSB_GETATTRIBS_MAPPER8021PSERVICEPROFILE (BM_LSB_iwTpPtrPriority7 | BM_LSB_unmarkedFrameOption | BM_LSB_dscpToPBitMapping | BM_LSB_defaultPBitMarking | BM_LSB_tpType)


#define BM_MSB_SETATTRIBS_MAPPER8021PSERVICEPROFILE (BM_MSB_pptpUniPointer | BM_MSB_iwTpPtrPriority0 | BM_MSB_iwTpPtrPriority1 | BM_MSB_iwTpPtrPriority2 | BM_MSB_iwTpPtrPriority3 | BM_MSB_iwTpPtrPriority4 | BM_MSB_iwTpPtrPriority5 | BM_MSB_iwTpPtrPriority6)
#define BM_LSB_SETATTRIBS_MAPPER8021PSERVICEPROFILE (BM_LSB_iwTpPtrPriority7 | BM_LSB_unmarkedFrameOption | BM_LSB_dscpToPBitMapping | BM_LSB_defaultPBitMarking | BM_LSB_tpType)

#define BM_MSB_CREATEATTRIBS_MAPPER8021PSERVICEPROFILE (BM_MSB_pptpUniPointer | BM_MSB_iwTpPtrPriority0 | BM_MSB_iwTpPtrPriority1 | BM_MSB_iwTpPtrPriority2 | BM_MSB_iwTpPtrPriority3 | BM_MSB_iwTpPtrPriority4 | BM_MSB_iwTpPtrPriority5 | BM_MSB_iwTpPtrPriority6)
#define BM_LSB_CREATEATTRIBS_MAPPER8021PSERVICEPROFILE (BM_LSB_iwTpPtrPriority7 | BM_LSB_unmarkedFrameOption | BM_LSB_defaultPBitMarking | BM_LSB_tpType)

// Bit masks for Iw Tp Ptr attributes
#define BM_MSB_SETATTRIBS_IWTPPTRS (BM_MSB_iwTpPtrPriority0 | BM_MSB_iwTpPtrPriority1 | BM_MSB_iwTpPtrPriority2 | BM_MSB_iwTpPtrPriority3 | BM_MSB_iwTpPtrPriority4 | BM_MSB_iwTpPtrPriority5 | BM_MSB_iwTpPtrPriority6)

#define BM_LSB_SETATTRIBS_IWTPPTRS BM_LSB_iwTpPtrPriority7



// Bit masks for MIB Upload
#define MAPPER8021PCELL1_MSB_MASK  BM_MSB_pptpUniPointer | BM_MSB_iwTpPtrPriority0 | BM_MSB_iwTpPtrPriority1 | BM_MSB_iwTpPtrPriority2 | BM_MSB_iwTpPtrPriority3 | BM_MSB_iwTpPtrPriority4 | BM_MSB_iwTpPtrPriority5 | BM_MSB_iwTpPtrPriority6

#define MAPPER8021PCELL1_LSB_MASK BM_LSB_iwTpPtrPriority7 | BM_LSB_unmarkedFrameOption | BM_LSB_defaultPBitMarking | BM_LSB_tpType

#define MAPPER8021PCELL2_LSB_MASK BM_LSB_dscpToPBitMapping



#define EXTRACT_PRIORITY(x)                  ((x) & 0x7)

// OMCI Entity DB
#define MAX_PRIORITIES                    8
#define DSCPTOPBITMAPPING_ARA_SIZE        24
typedef struct
{
    MeInst_S meInst;;
    UINT16   pptpUniPointer;
    union
    {
        struct
        {
            UINT16  iwTpPtrPriority0;
            UINT16  iwTpPtrPriority1;
            UINT16  iwTpPtrPriority2;
            UINT16  iwTpPtrPriority3;
            UINT16  iwTpPtrPriority4;
            UINT16  iwTpPtrPriority5;
            UINT16  iwTpPtrPriority6;
            UINT16  iwTpPtrPriority7;
        };
        UINT16 iwTpPtrAra[MAX_PRIORITIES];
    };
    UINT8   unmarkedFrameOption;
    UINT8   dscpToPBitMapping[DSCPTOPBITMAPPING_ARA_SIZE];
    UINT8   defaultPBitMarking;
    UINT8   tpType;
} Mapper8021pServiceProfileME;



// messageContents - Create InterWorking VCC TP Command
#pragma pack(1)
typedef struct
{
    UINT16  pptpUniPointer;
    UINT16  iwTpPtrPriority0;
    UINT16  iwTpPtrPriority1;
    UINT16  iwTpPtrPriority2;
    UINT16  iwTpPtrPriority3;
    UINT16  iwTpPtrPriority4;
    UINT16  iwTpPtrPriority5;
    UINT16  iwTpPtrPriority6;
    UINT16  iwTpPtrPriority7;
    UINT8   unmarkedFrameOption;
    UINT8   defaultPBitMarking;
    UINT8   tpType;
} CreateMapper8021pServiceProfile_s;
#pragma pack(0)




MeClassDef_S     mapper8021pServiceProfile_MeClassDef;

MeInst_S *getMeForAniTp(Mapper8021pServiceProfileME *pmapper);

OMCIPROCSTATUS activateMulticastServiceIwOptionZero(UINT32 partitionKey);

OMCIPROCSTATUS doNewHardwareMapperConfiguration(Mapper8021pServiceProfileME *pmapper, UINT16 *newIwTpPtrAra, char *buf);

#endif
