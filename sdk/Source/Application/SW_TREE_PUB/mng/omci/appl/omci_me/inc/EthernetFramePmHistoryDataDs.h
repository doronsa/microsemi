
/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : EthernetFramePmHistoryDataDs.h                            **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the Ethernet Frame PM History Data   **/
/**                Upstream ME                                               **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    11Dec12      Victor  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCEthernetFramePmHistoryDataDsh
#define __INCEthernetFramePmHistoryDataDsh


// Ethernet Frame PM History Data Upstream ME 
#define BM_MSB_pmDs_IntervalEndTime                 (0X80)
#define BM_MSB_pmDs_thresholdDataId                 (0X40)
#define BM_MSB_dropEvents                           (0X20)
#define BM_MSB_octets                               (0X10)
#define BM_MSB_frames                               (0X08)
#define BM_MSB_broadcastFrames                      (0X04)
#define BM_MSB_multicastFrames                      (0X02)
#define BM_MSB_crcErroredFrames                     (0X01)

#define BM_LSB_undersizeFrames                      (0X80)
#define BM_LSB_oversizeFrames                       (0X40)
#define BM_LSB_frames_64Octets                      (0X20)
#define BM_LSB_frames_65_127Octets                  (0X10)
#define BM_LSB_frames_128_255Octets                 (0X08)
#define BM_LSB_frames_256_511Octets                 (0X04)
#define BM_LSB_frames_512_1023Octets                (0X02)
#define BM_LSB_frames_1024_1518Octets               (0X01)

#define BM_MSB_GETATTRIBS_ETHERNETFRPMDS (BM_MSB_pmDs_IntervalEndTime | BM_MSB_pmDs_thresholdDataId | BM_MSB_dropEvents | BM_MSB_octets| BM_MSB_frames | BM_MSB_broadcastFrames | BM_MSB_multicastFrames | BM_MSB_crcErroredFrames)

#define BM_LSB_GETATTRIBS_ETHERNETFRPMDS (BM_LSB_undersizeFrames | BM_LSB_oversizeFrames | BM_LSB_frames_64Octets | BM_LSB_frames_65_127Octets | BM_LSB_frames_128_255Octets | BM_LSB_frames_256_511Octets | BM_LSB_frames_512_1023Octets | BM_LSB_frames_1024_1518Octets)

#define BM_MSB_SETATTRIBS_ETHERNETFRPMDS BM_MSB_pmDs_thresholdDataId

#define BM_MSB_CREATEATTRIBS_ETHERNETFRPMDS BM_MSB_pmDs_thresholdDataId

#pragma pack(1)
typedef struct
{
    UINT16 thresholdDataId;
} CreateEthernetFramePmDsCmd_s;
#pragma pack(0)


// OMCI Entity DB
typedef struct
{
    MeInst_S           meInst;
    UINT8              tcaBitMask[OMCI_TCA_BIT_MAP_LEN];    
    UINT8              intervalEndTime;    
    UINT16             thresholdDataId;
    UINT32             dropEvents;
    UINT32             octets;
    UINT32             frames;
    UINT32             broadcastFrames;
    UINT32             multicastFrames;
    UINT32             crcErroredFrames;
    UINT32             undersizeFrames;
    UINT32             oversizeFrames;
    UINT32             frames_64Octets;
    UINT32             frames_65_127Octets;
    UINT32             frames_128_255Octets;
    UINT32             frames_256_511Octets;
    UINT32             frames_512_1023Octets;
    UINT32             frames_1024_1518Octets;
} EthernetFramePmDsME;


extern MeClassDef_S    ethernetFramePmDs_MeClassDef;
extern MeApmMapTable_S ethernetFramePmDs_ApmMapTable;

#endif
