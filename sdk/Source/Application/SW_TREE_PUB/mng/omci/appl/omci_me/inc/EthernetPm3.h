/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : EthernetPm3.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the Ethernet PM History Data 3 ME    **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    9Mar08      zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCEthernetPm3h
#define __INCEthernetPm3h



// Ethernet PM History Data 3 Attribute bits
#define BM_MSB_ethpm3IntervalEndTime                (0X80)
#define BM_MSB_ethpm3ThresholdDataId                (0X40)
#define BM_MSB_dropEvents                           (0X20)
#define BM_MSB_octets                               (0X10)
#define BM_MSB_packets                              (0X08)
#define BM_MSB_broadcastPackets                     (0X04)
#define BM_MSB_multicastPackets                     (0X02)
#define BM_MSB_undersizePackets                     (0X01)

#define BM_LSB_fragments                            (0X80)
#define BM_LSB_jabbers                              (0X40)
#define BM_LSB_packets_64Octets                     (0X20)
#define BM_LSB_packets_65_127Octets                 (0X10)
#define BM_LSB_packets_128_255Octets                (0X08)
#define BM_LSB_packets_256_511Octets                (0X04)
#define BM_LSB_packets_512_1023Octets               (0X02)
#define BM_LSB_packets_1024_1518Octets              (0X01)

#define BM_MSB_GETATTRIBS_ETHERNETPM3 BM_MSB_ethpm3IntervalEndTime | BM_MSB_ethpm3ThresholdDataId | BM_MSB_dropEvents | BM_MSB_octets | BM_MSB_packets | BM_MSB_broadcastPackets | BM_MSB_multicastPackets | BM_MSB_undersizePackets

#define BM_LSB_GETATTRIBS_ETHERNETPM3 BM_LSB_fragments | BM_LSB_jabbers | BM_LSB_packets_64Octets | BM_LSB_packets_65_127Octets | BM_LSB_packets_128_255Octets | BM_LSB_packets_256_511Octets | BM_LSB_packets_512_1023Octets | BM_LSB_packets_1024_1518Octets


#define BM_MSB_SETATTRIBS_ETHERNETPM3 BM_MSB_ethpm3ThresholdDataId

#define BM_MSB_CREATEATTRIBS_ETHERNETPM3 BM_MSB_ethpm3ThresholdDataId


#pragma pack(1)
typedef struct
{
    UINT16 thresholdDataId;
} CreateEthernetPm3Cmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    tcaBitMask[2];
    UINT8    intervalEndTime;
    UINT16   thresholdDataId;
    UINT32   dropEvents;
    UINT32   octets;
    UINT32   packets;
    UINT32   broadcastPackets;
    UINT32   multicastPackets;
    UINT32   undersizePackets;
    UINT32   fragments;
    UINT32   jabbers;
    UINT32   packets_64Octets;
    UINT32   packets_65_127Octets;
    UINT32   packets_128_255Octets;
    UINT32   packets_256_511Octets;
    UINT32   packets_512_1023Octets;
    UINT32   packets_1024_1518Octets;
} EthernetPm3ME;


extern MeClassDef_S ethernetPm3_MeClassDef;


#endif
