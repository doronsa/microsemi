/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : GemPortNetworkCtpPm.h                                     **/
/**                                                                          **/
/**  DESCRIPTION : This file is GEM Port Network CTP PM include file         **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    13Dec10     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCGemPortNetworkCtpPmh
#define __INCGemPortNetworkCtpPmh



// GEM Port Network CTP PM Attribute bits
#define BM_MSB_networkCtpPm_intervalEndTime       (0X80)
#define BM_MSB_networkCtpPm_thresholdData12Id     (0X40)
#define BM_MSB_transmittedGemFrames               (0x20)
#define BM_MSB_receivedGemFrames                  (0x10)
#define BM_MSB_receivedPayloadBytes               (0x08)
#define BM_MSB_transmittedPayloadBytes            (0x04)

#define BM_MSB_GETATTRIBS_GEMPORTNETWORKCTPPM     (BM_MSB_networkCtpPm_intervalEndTime | BM_MSB_networkCtpPm_thresholdData12Id | \
                                                   BM_MSB_transmittedGemFrames         | BM_MSB_receivedGemFrames | \
                                                   BM_MSB_receivedPayloadBytes         | BM_MSB_transmittedPayloadBytes)

#define BM_MSB_SETATTRIBS_GEMPORTNETWORKCTPPM      (BM_MSB_networkCtpPm_thresholdData12Id)

#define BM_MSB_CREATEATTRIBS_GEMPORTNETWORKCTPPM   (BM_MSB_networkCtpPm_thresholdData12Id)


// messageContents - Gem Port Network CTP PM Create Command
#pragma pack(1)
typedef struct
{
    UINT16 thresholdData12Id;
} CreateGemPortNetworkCtpPmCmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    tcaBitMask[2];
    UINT8    intervalEndTime;
    UINT16   thresholdData12Id;
    UINT32   transmittedGemFrames;
    UINT32   receivedGemFrames;
    UINT64   receivedPayloadBytes;
    UINT64   transmittedPayloadBytes;
} GemPortNetworkCtpPmME;


extern MeClassDef_S gemPortNetworkCtpPm_MeClassDef; 

extern bool findGemPort(UINT16 meInstance, UINT16 *gemPort);


#endif
