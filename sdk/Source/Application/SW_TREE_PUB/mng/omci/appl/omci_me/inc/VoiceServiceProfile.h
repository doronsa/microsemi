/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VoiceServiceProfile.h                                     **/
/**                                                                          **/
/**  DESCRIPTION : This file has Voice Service Profile ME definitions        **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVoiceServiceProfileh
#define __INCVoiceServiceProfileh



// Voice Service Profile Attribute bits
#define BM_MSB_announcementType                      (0X80)
#define BM_MSB_jitterTarget                          (0X40)
#define BM_MSB_jitterBufferMax                       (0X20)
#define BM_MSB_echoCancelInd                         (0X10)
#define BM_MSB_pstnProtocolVariant                   (0X08)
#define BM_MSB_dtmfDigitLevels                       (0X04)
#define BM_MSB_dtmfDigitDuration                     (0X02)
#define BM_MSB_hookflashMinimumTime                  (0X01)


#define BM_LSB_hookflashMaximumTime                  (0X80)
#define BM_LSB_tonePatternTable                      (0X40)
#define BM_LSB_toneEventTable                        (0X20)
#define BM_LSB_ringingPatternTable                   (0X10)
#define BM_LSB_ringingEventTable                     (0X08)

#define BM_MSB_GETATTRIBS_VOICESERVICEPROFILE (BM_MSB_announcementType | BM_MSB_jitterTarget | BM_MSB_jitterBufferMax | BM_MSB_echoCancelInd | \
    BM_MSB_pstnProtocolVariant | BM_MSB_dtmfDigitLevels | BM_MSB_dtmfDigitDuration | BM_MSB_hookflashMinimumTime)

#define BM_LSB_GETATTRIBS_VOICESERVICEPROFILE (BM_LSB_hookflashMaximumTime | BM_LSB_tonePatternTable | BM_LSB_toneEventTable | \
    BM_LSB_ringingPatternTable | BM_LSB_ringingEventTable)


#define BM_MSB_SETATTRIBS_VOICESERVICEPROFILE (BM_MSB_announcementType | BM_MSB_jitterTarget | BM_MSB_jitterBufferMax | BM_MSB_echoCancelInd | \
    BM_MSB_pstnProtocolVariant | BM_MSB_dtmfDigitLevels | BM_MSB_dtmfDigitDuration | BM_MSB_hookflashMinimumTime)

#define BM_LSB_SETATTRIBS_VOICESERVICEPROFILE (BM_LSB_hookflashMaximumTime | BM_LSB_tonePatternTable | BM_LSB_toneEventTable | \
    BM_LSB_ringingPatternTable | BM_LSB_ringingEventTable)


#define BM_MSB_CREATEATTRIBS_VOICESERVICEPROFILE (BM_MSB_announcementType | BM_MSB_jitterTarget | BM_MSB_jitterBufferMax | BM_MSB_echoCancelInd | \
    BM_MSB_pstnProtocolVariant | BM_MSB_dtmfDigitLevels | BM_MSB_dtmfDigitDuration | BM_MSB_hookflashMinimumTime)

#define BM_LSB_CREATEATTRIBS_VOICESERVICEPROFILE (BM_LSB_hookflashMaximumTime)


#define VOICESERVICEPROFILE_UPLOAD_CELL1_MSB_MASK (BM_MSB_announcementType | BM_MSB_jitterTarget | BM_MSB_jitterBufferMax | BM_MSB_echoCancelInd | \
    BM_MSB_pstnProtocolVariant | BM_MSB_dtmfDigitLevels | BM_MSB_dtmfDigitDuration | BM_MSB_hookflashMinimumTime)

#define VOICESERVICEPROFILE_UPLOAD_CELL2_LSB_MASK (BM_LSB_hookflashMaximumTime | BM_LSB_tonePatternTable | BM_LSB_toneEventTable | \
    BM_LSB_ringingPatternTable | BM_LSB_ringingEventTable)




#pragma pack(1)
typedef struct
{
    UINT8  index;
    UINT8  toneOn;
    UINT16 frequency1;
    UINT8  power1;
    UINT16 frequency2;
    UINT8  power2;
    UINT16 frequency3;
    UINT8  power3;
    UINT16 frequency4;
    UINT8  power4;
    UINT16 modulationFrequency;
    UINT8  modulationPower;
    UINT16 duration;
    UINT8  nextEntry;

} TonePatternEntry_S;
#pragma pack(0)



#pragma pack(1)
typedef struct
{
    UINT8  event;
    UINT8  tonePattern;
    UINT16 toneFile;
    UINT8  toneFileRepetitions;
    UINT8  reserved[2];

} ToneEventEntry_S;
#pragma pack(0)



#pragma pack(1)
typedef struct
{
    UINT8  index;
    UINT8  ringingOn;
    UINT16 duration;
    UINT8  nextEntry;

} RingingPatternEntry_S;
#pragma pack(0)



#pragma pack(1)
typedef struct
{
    UINT8  event;
    UINT8  ringingPattern;
    UINT16 ringingFile;
    UINT8  ringingFileRepetitions;
    UINT16 ringingText;

} RingingEventEntry_S;
#pragma pack(0)


#define TONEPATTERN_ENTRIES            (36)
#define TONEEVENT_ENTRIES              (16)
#define RINGINGPATTERN_ENTRIES         (16)
#define RINGINGEVENT_ENTRIES           (16)
// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8                 announcementType;
    UINT16                jitterTarget;
    UINT16                jitterBufferMax;
    UINT8                 echoCancelInd;
    UINT16                pstnProtocolVariant; 
    UINT16                dtmfDigitLevels;
    UINT16                dtmfDigitDuration;
    UINT16                hookflashMinimumTime;
    UINT16                hookflashMaximumTime;
    TonePatternEntry_S    tonePatternAra[TONEPATTERN_ENTRIES];
    ToneEventEntry_S      toneEventAra[TONEEVENT_ENTRIES];
    RingingPatternEntry_S ringingPatternAra[RINGINGPATTERN_ENTRIES];
    RingingEventEntry_S   ringingEventAra[RINGINGPATTERN_ENTRIES];

    UINT32                tonePatternTable_Size;
    UINT32                toneEventTable_Size;
    UINT32                ringingPatternTable_Size;
    UINT32                ringingEventTable_Size;

    struct
    {
        UINT32              currentTicks;
        TonePatternEntry_S  tonePatternAra[TONEPATTERN_ENTRIES];
    } getNextBufTonePattern;

    struct
    {
        UINT32              currentTicks;
        ToneEventEntry_S    toneEventAra[TONEEVENT_ENTRIES];
    } getNextBufToneEvent;

    struct
    {
        UINT32                currentTicks;
        RingingPatternEntry_S ringingPatternAra[RINGINGPATTERN_ENTRIES];
    } getNextBufRingingPattern;

    struct
    {
        UINT32                currentTicks;
        RingingEventEntry_S   ringingEventAra[RINGINGEVENT_ENTRIES];
    } getNextBufRingingEvent;


} VoiceServiceProfileME;



// messageContents - Create Voice Service Profile Command
#pragma pack(1)
typedef struct
{
    UINT8   announcementType;
    UINT16  jitterTarget;
    UINT16  jitterBufferMax;
    UINT8   echoCancelInd;
    UINT16  pstnProtocolVariant; 
    UINT16  dtmfDigitLevels;
    UINT16  dtmfDigitDuration;
    UINT16  hookflashMinimumTime;
    UINT16  hookflashMaximumTime;
} CreateVoiceServiceProfileCmd_s;
#pragma pack(0)




MeClassDef_S voiceServiceProfile_MeClassDef;


#endif
