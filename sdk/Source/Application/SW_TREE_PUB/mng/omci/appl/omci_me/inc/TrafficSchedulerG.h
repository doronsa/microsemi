/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : TrafficScheduler.h                                        **/
/**                                                                          **/
/**  DESCRIPTION : This file has Traffic Scheduler definitions               **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCTrafficSchedulerGh
#define __INCTrafficSchedulerGh



// Traffic Scheduler Attribute bits
#define BM_MSB_tcontPointer                         (0X80)
#define BM_MSB_trafficSchedulerPointer              (0X40)
#define BM_MSB_policy                               (0X20)
#define BM_MSB_priorityWeight                       (0X10)


#define BM_MSB_GETATTRIBS_TRAFSCHEDG BM_MSB_tcontPointer | BM_MSB_trafficSchedulerPointer | BM_MSB_policy | BM_MSB_priorityWeight

#define BM_MSB_SETATTRIBS_TRAFSCHEDG BM_MSB_priorityWeight


// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT16   tcontPointer;
    UINT16   trafficSchedulerPointer;
    UINT8    policy;
    UINT8    priorityWeight;
} TrafficSchedulerGME;


typedef struct
{
    UINT16   tcontPointer;
    UINT16   trafficSchedulerPointer;
    UINT8    policy;
    UINT8    priorityWeight;
} CreateTrafficSchedulerG_s;


OMCIPROCSTATUS createTrafficSchedulerGME(UINT16 meInstance, CreateTrafficSchedulerG_s *pCreate);


MeClassDef_S trafficSchedulerG_MeClassDef;


#endif
