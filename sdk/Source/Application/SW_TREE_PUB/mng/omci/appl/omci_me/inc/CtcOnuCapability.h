
/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : CtcOnuCapability.h                                        **/
/**                                                                          **/
/**  DESCRIPTION : This file contains CTC ONU Capability definitions         **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    8Aug11     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCCtcOnuCapabilityh
#define __INCCtcOnuCapabilityh


// CTC ONU Capability Attribute bits
#define BM_MSB_operatorId                          (0x80)
#define BM_MSB_ctcSpecVersion                      (0x40)
#define BM_MSB_onuType                             (0x20)
#define BM_MSB_onuPowerSupplyControl               (0x10)


#define BM_MSB_GETATTRIBS_ONUCAPAB (BM_MSB_operatorId | BM_MSB_ctcSpecVersion | BM_MSB_onuType | BM_MSB_onuPowerSupplyControl)

/* CTC spec version */
typedef enum
{
    CTC_SPEC_VERSION_2_0,      /* Support CTC GPON spec 2.0 requirement       */
    CTC_SPEC_VERSION_2_0_AMD1, /* Support CTC GPON spec 2.0 Amd 1 requirement */
} CTC_SPEC_VERSION_E;

/* CTC Transceiver TX/RX power control */
typedef enum
{
    ONU_XRV_POWER_NOT_SUPPORT,/* Does not support ONU transceiver power management           */
    ONU_XRV_POWER_TX_ONLY,    /* Only support setting TX of ONU transceiver power            */
    ONU_XRV_POWER_TX_RX,      /* support setting TX/RX of ONU transceiver power respectively */    
} ONU_XRV_POWER_E;

// OMCI Entity DB
#define OPERATORID_LEN                (4)
typedef struct 
{
    MeInst_S  meInst;
    UINT8     operatorId[OPERATORID_LEN];
    UINT8     ctcSpecVersion;
    UINT8     onuType;
    UINT8     onuPowerSupplyControl;
} CtcOnuCapabilityME;


typedef struct
{
    UINT8     operatorId[OPERATORID_LEN];
    UINT8     ctcSpecVersion;
    UINT8     onuType;
    UINT8     onuPowerSupplyControl;
} CreateCtcOunuCapability_S;


OMCIPROCSTATUS createCtcOnuCapabilityME(UINT16 meInstance, CreateCtcOunuCapability_S *pcreate);


MeClassDef_S ctcOnuCapability_MeClassDef;


#endif
