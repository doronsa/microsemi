/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : SipAgentConfigData.h                                      **/
/**                                                                          **/
/**  DESCRIPTION : This file has SIP Agent Config Data definitions           **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCSipAgentConfigDatah
#define __INCSipAgentConfigDatah

#define DEFAULT_TCPUDPPTR                  0xFFFF
#define DEFAULT_SIPREGEXPTIME              3600
#define DEFAULT_SIPREREGHEADSTARTTIME      360


// SIP Agent Config Data Attribute bits
#define BM_MSB_proxyServerAddressPointer   (0x80)
#define BM_MSB_outboundProxyAddressPointer (0x40) 
#define BM_MSB_primarySipDns               (0x20)
#define BM_MSB_secondarySipDns             (0x10)
#define BM_MSB_sipagent_udptcpPointer      (0x08)
#define BM_MSB_sipRegExpTime               (0x04)
#define BM_MSB_sipReRegHeadStartTime       (0x02)
#define BM_MSB_hostPartUri                 (0x01)
#define BM_LSB_sipStatus                   (0x80)
#define BM_LSB_sipRegistrar                (0x40) 
#define BM_LSB_softswitch                  (0x20)
#define BM_LSB_sipResponseTable            (0x10)
#define BM_LSB_sipOptionTransmitControl    (0x08)
#define BM_LSB_sipUriFormat                (0x04)
#define BM_LSB_redundantSipAgentPointer    (0x02)


#define BM_MSB_GETATTRIBS_SIPAGENTCONFIGDATA (BM_MSB_proxyServerAddressPointer | BM_MSB_outboundProxyAddressPointer | BM_MSB_primarySipDns | BM_MSB_secondarySipDns | BM_MSB_sipagent_udptcpPointer | BM_MSB_sipRegExpTime | BM_MSB_sipReRegHeadStartTime | BM_MSB_hostPartUri)
#define BM_LSB_GETATTRIBS_SIPAGENTCONFIGDATA (BM_LSB_sipStatus | BM_LSB_sipRegistrar | BM_LSB_softswitch | BM_LSB_sipResponseTable | BM_LSB_sipOptionTransmitControl | BM_LSB_sipUriFormat | BM_LSB_redundantSipAgentPointer)

#define BM_MSB_SETATTRIBS_SIPAGENTCONFIGDATA (BM_MSB_proxyServerAddressPointer | BM_MSB_outboundProxyAddressPointer | BM_MSB_primarySipDns | BM_MSB_secondarySipDns | BM_MSB_sipagent_udptcpPointer | BM_MSB_sipRegExpTime | BM_MSB_sipReRegHeadStartTime | BM_MSB_hostPartUri)
#define BM_LSB_SETATTRIBS_SIPAGENTCONFIGDATA (BM_LSB_sipRegistrar | BM_LSB_softswitch  | BM_LSB_sipResponseTable | BM_LSB_sipOptionTransmitControl | BM_LSB_sipUriFormat | BM_LSB_redundantSipAgentPointer)

#define BM_MSB_CREATEATTRIBS_SIPAGENTCONFIGDATA (BM_MSB_proxyServerAddressPointer | BM_MSB_outboundProxyAddressPointer | BM_MSB_primarySipDns | BM_MSB_secondarySipDns | BM_MSB_hostPartUri)
#define BM_LSB_CREATEATTRIBS_SIPAGENTCONFIGDATA (BM_LSB_sipRegistrar | BM_LSB_softswitch | BM_LSB_sipOptionTransmitControl | BM_LSB_sipUriFormat | BM_LSB_redundantSipAgentPointer)

// Used in MIB Upload snapshot 
#define SIPAGENTCONFIGDATA_UPLOAD_CELL1_MSB_MASK (BM_MSB_proxyServerAddressPointer | BM_MSB_outboundProxyAddressPointer | BM_MSB_primarySipDns | BM_MSB_secondarySipDns | BM_MSB_hostPartUri)

#define SIPAGENTCONFIGDATA_UPLOAD_CELL2_LSB_MASK (BM_LSB_sipStatus | BM_LSB_sipRegistrar | BM_LSB_softswitch  | BM_LSB_sipResponseTable | BM_LSB_sipOptionTransmitControl | BM_LSB_sipUriFormat | BM_LSB_redundantSipAgentPointer)


// messageContents - Create SIP Agent Config Data Command
#pragma pack(1)
typedef struct
{
    UINT16   sipResponseCode;
    UINT8    tone;
    UINT16   textMessage;
} SipResponseEntry_S;
#pragma pack(0)



// OMCI Entity DB
#define SOFTSWITCH_SIZE                        4

#define SIPAGENTCONFIGDATA_ALARMARA_SIZE       1
#define NUM_SIPRESPONSETABLE_ENTRIES                10
typedef struct
{
    MeInst_S           meInst;
    UINT8              alarmsBitmap[OMCI_ALARM_BIT_MAP_LEN];
    UINT16             proxyServerAddressPointer;
    UINT16             outboundProxyAddressPointer;
    UINT8              primarySipDns[IPADDR_SIZE];
    UINT8              secondarySipDns[IPADDR_SIZE];
    UINT16             udptcpPointer;
    UINT32             sipRegExpTime;
    UINT32             sipReRegHeadStartTime;
    UINT16             hostPartUri;
    UINT8              sipStatus;
    UINT16             sipRegistrar;
    UINT8              softswitch[SOFTSWITCH_SIZE];
    SipResponseEntry_S sipResponseAra[NUM_SIPRESPONSETABLE_ENTRIES];
    struct
    {
        UINT32             currentTicks;
        SipResponseEntry_S sipResponseAra[NUM_SIPRESPONSETABLE_ENTRIES];
    } getNextBuf;

    UINT32             sipResponseAra_size;
    UINT8              sipOptionTransmitControl;
    UINT8              sipUriFormat;
    UINT16             redundantSipAgentPointer;
} SipAgentConfigDataME;



// messageContents - Create SIP Agent Config Data Command
#pragma pack(1)
typedef struct
{
    UINT16   proxyServerAddressPointer;
    UINT16   outboundProxyAddressPointer;
    UINT8    primarySipDns[IPADDR_SIZE];
    UINT8    secondarySipDns[IPADDR_SIZE];
    UINT16   hostPartUri;
    UINT16   sipRegistrar;
    UINT8    softswitch[SOFTSWITCH_SIZE];
    UINT8    sipOptionTransmitControl;
    UINT8    sipUriFormat;
    UINT16   redundantSipAgentPointer;
} CreateSipAgentConfigData_s;
#pragma pack(0)



MeClassDef_S sipAgentConfigData_MeClassDef;


#endif

