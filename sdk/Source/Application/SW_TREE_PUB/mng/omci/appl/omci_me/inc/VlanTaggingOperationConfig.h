/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VlanTaggingOperationConfig.h                              **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Vlan Tagging Operation Config entity **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    19Jun06     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVlanTaggingOperationConfigh
#define __INCVlanTaggingOperationConfigh

#include "OmciDatapathAdapter.h"

// Vlan Tagging Operation Config Attribute bits
#define BM_MSB_upstreamVlanTaggingOpMode            (0x80)
#define BM_MSB_upstreamVlanTagTciValue              (0x40)
#define BM_MSB_downstreamVlanTaggingOpMode          (0x20) 
#define BM_MSB_vlanTagAssociationType               (0x10) 
#define BM_MSB_associatedMePointer                  (0x08) 



#define BM_MSB_GETATTRIBS_VLANTAGGINGOPERATIONCONFIG (BM_MSB_upstreamVlanTaggingOpMode | BM_MSB_upstreamVlanTagTciValue | BM_MSB_downstreamVlanTaggingOpMode | BM_MSB_vlanTagAssociationType | BM_MSB_associatedMePointer)
 
#define BM_MSB_SETATTRIBS_VLANTAGGINGOPERATIONCONFIG (BM_MSB_upstreamVlanTaggingOpMode | BM_MSB_upstreamVlanTagTciValue | BM_MSB_downstreamVlanTaggingOpMode | BM_MSB_vlanTagAssociationType | BM_MSB_associatedMePointer)

#define BM_MSB_CREATEATTRIBS_VLANTAGGINGOPERATIONCONFIG (BM_MSB_upstreamVlanTaggingOpMode | BM_MSB_upstreamVlanTagTciValue | BM_MSB_downstreamVlanTaggingOpMode | BM_MSB_vlanTagAssociationType | BM_MSB_associatedMePointer)


// OMCI Entity DB
#define MAX_REG_VLANTAGGING_ENTRIES                 (6)
typedef struct
{
    MeInst_S meInst;
    UINT8            upstreamVlanTaggingOpMode;
    UINT16           upstreamVlanTagTciValue;
    UINT8            downstreamVlanTaggingOpMode;
    UINT8            associationType;
    UINT16           associatedMePointer;
    VlanTaggingOp_S  vlanTaggingOp[MAX_REG_VLANTAGGING_ENTRIES];
    bool             entryInUse[MAX_REG_VLANTAGGING_ENTRIES];
} VlanTaggingOperationConfigME;



// messageContents
#pragma pack(1)
typedef struct
{
    UINT8   upstreamVlanTaggingOpMode;
    UINT16  upstreamVlanTagTciValue;
    UINT8   downstreamVlanTaggingOpMode;
    UINT8   associationType;
    UINT16  associatedMePointer;
} CreateVlanTaggingOperationConfig_s;
#pragma pack(0)


extern MeClassDef_S     vlanTaggingOperationConfig_MeClassDef;


#endif
