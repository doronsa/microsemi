/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        :  PriorityQueueG.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : This file contains Priority Queue-G definitions           **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    20Jun06     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCPriorityQueueGh
#define __INCPriorityQueueGh

#include "ProfileCache.h"

#define TCONTFROMRELATEDPORT(x)       (((x) >> 16) & 0xFFFF)
#define PRIORITYFROMRELATEDPORT(x)    ((x) & 0xFFFF)



// Priority Queue Attribute bits
#define BM_MSB_queueConfigurationOption             (0x80) 
#define BM_MSB_maxQueueSize                         (0x40)
#define BM_MSB_allocatedQueueSize                   (0x20) 
#define BM_MSB_discardCellCounterResetInterval      (0x10)
#define BM_MSB_thresholdValueDiscardedCells         (0x08)
#define BM_MSB_relatedPort                          (0x04)
#define BM_MSB_trafficSchedulerGPointer             (0x02)
#define BM_MSB_weight                               (0x01)
#define BM_LSB_backPressureOperation                (0x80)
#define BM_LSB_backPressureTime                     (0x40)
#define BM_LSB_backPressureOccurQueueThreshold      (0x20)
#define BM_LSB_backPressureClearQueueThreshold      (0x10)
#define BM_LSB_packetDropQeueThresholds             (0x08)
#define BM_LSB_packetDropMaxP                       (0x04)
#define BM_LSB_queueDropWQ                          (0x02)
#define BM_LSB_dropPrecedenceColourMarking          (0x01)


#define BM_MSB_GETATTRIBS_PRIORITYQUEUEG (BM_MSB_queueConfigurationOption | BM_MSB_maxQueueSize | BM_MSB_allocatedQueueSize | BM_MSB_discardCellCounterResetInterval | \
    BM_MSB_thresholdValueDiscardedCells | BM_MSB_relatedPort | BM_MSB_trafficSchedulerGPointer | BM_MSB_weight) 

#define BM_LSB_GETATTRIBS_PRIORITYQUEUEG (BM_LSB_backPressureOperation | BM_LSB_backPressureTime | BM_LSB_backPressureOccurQueueThreshold | \
    BM_LSB_backPressureClearQueueThreshold | BM_LSB_packetDropQeueThresholds | BM_LSB_packetDropMaxP | BM_LSB_queueDropWQ | BM_LSB_dropPrecedenceColourMarking)



#define BM_MSB_SETATTRIBS_PRIORITYQUEUEG (BM_MSB_allocatedQueueSize | BM_MSB_discardCellCounterResetInterval | BM_MSB_thresholdValueDiscardedCells | \
    BM_MSB_trafficSchedulerGPointer | BM_MSB_relatedPort | BM_MSB_weight)

#define BM_LSB_SETATTRIBS_PRIORITYQUEUEG (BM_LSB_backPressureOperation | BM_LSB_backPressureTime | BM_LSB_backPressureOccurQueueThreshold | \
    BM_LSB_backPressureClearQueueThreshold | BM_LSB_packetDropQeueThresholds | BM_LSB_packetDropMaxP | BM_LSB_queueDropWQ | BM_LSB_dropPrecedenceColourMarking)



#define PRIORITYQUEUEG_UPLOAD_CELL1_MSB_MASK (BM_MSB_queueConfigurationOption | BM_MSB_maxQueueSize | BM_MSB_allocatedQueueSize | BM_MSB_discardCellCounterResetInterval | \
    BM_MSB_thresholdValueDiscardedCells | BM_MSB_relatedPort | BM_MSB_trafficSchedulerGPointer | BM_MSB_weight)

#define PRIORITYQUEUEG_UPLOAD_CELL2_LSB_MASK (BM_LSB_backPressureOperation | BM_LSB_backPressureTime | BM_LSB_backPressureOccurQueueThreshold | \
    BM_LSB_backPressureClearQueueThreshold | BM_LSB_packetDropQeueThresholds | BM_LSB_packetDropMaxP | BM_LSB_queueDropWQ | BM_LSB_dropPrecedenceColourMarking)




#pragma pack(1)
typedef struct
{
    UINT8   slotNum;
    UINT8   tcontNum;
    UINT16  priority;
} RelatedPort_S;
#pragma pack(0)


#pragma pack(1)
typedef struct
{
    UINT16 minimumThresholdForQueueEntry;
    UINT16 maximumThresholdForDrop;
} QeueThresholdsPair_S;
#pragma pack(0)


#pragma pack(1)
typedef struct
{
    QeueThresholdsPair_S green;
    QeueThresholdsPair_S yellow;
} PacketDropQeueThresholds_S;
#pragma pack(0)


#pragma pack(1)
typedef struct
{
    UINT8  green;
    UINT8  yellow;
} PacketDropMaxP_S;
#pragma pack(0)




// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8                      queueConfigurationOption;
    UINT16                     maxQueueSize;
    UINT16                     allocatedQueueSize;
    UINT16                     discardCellCounterResetInterval;
    UINT16                     thresholdValueDiscardedCells;
    RelatedPort_S              relatedPort;                     // In network order
    UINT16                     trafficSchedulerGPointer;
    UINT8                      weight;
    UINT16                     backPressureOperation;
    UINT32                     backPressureTime;
    UINT16                     backPressureOccurQueueThreshold;
    UINT16                     backPressureClearQueueThreshold;
    PacketDropQeueThresholds_S packetDropQeueThresholds;        // In network order
    PacketDropMaxP_S           packetDropMaxP;
    UINT8                      queueDropWQ;
    UINT8                      dropPrecedenceColourMarking;
    UINT16                     internalQueueId;                 // relative to other US queues related to the same T-CONT
} PriorityQueueGME;


OMCIPROCSTATUS createPriorityQueueGME(UINT16 meInstance, RelatedPort_S *queueCfg);


MeClassDef_S priorityQueueG_MeClassDef;
 

#endif
