/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : GalEthernetProfile.h                                      **/
/**                                                                          **/
/**  DESCRIPTION : This file implements GAL Ethernet Profile ME              **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCGalEthernetProfileh
#define __INCGalEthernetProfileh



// AAL1 Profile Attribute bits
#define BM_MSB_maximumGemPayloadSize             (0X80)


#define BM_MSB_GETATTRIBS_GALETHERNETPROFILE BM_MSB_maximumGemPayloadSize

#define BM_MSB_SETATTRIBS_GALETHERNETPROFILE BM_MSB_maximumGemPayloadSize

#define BM_MSB_CREATEATTRIBS_GALETHERNETPROFILE BM_MSB_maximumGemPayloadSize


// messageContents - Create GAL Ethernet Profile Command
#pragma pack(1)
typedef struct
{
    UINT16  maximumGemPayloadSize;
} CreateGalEthernetProfileCmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT16   maximumGemPayloadSize;
} GalEthernetProfileME;



MeClassDef_S galEthernetProfile_MeClassDef;


#endif
