/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : UniG.h                                                    **/
/**                                                                          **/
/**  DESCRIPTION : This file has UNI-G definitions                           **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCUniGh
#define __INCUniGh


// Physical Path TP - UNI G Attribute bits
#define BM_MSB_configurationOptionStatus            (0x80)
#define BM_MSB_uniG_administrativeState             (0x40)
#define BM_MSB_managementCapability                 (0x20)
#define BM_MSB_nonOmciManagementIdentifier          (0x10)


#define BM_MSB_GETATTRIBS_UNIG (BM_MSB_configurationOptionStatus | BM_MSB_uniG_administrativeState | BM_MSB_managementCapability | BM_MSB_nonOmciManagementIdentifier)

#define BM_MSB_SETATTRIBS_UNIG (BM_MSB_configurationOptionStatus | BM_MSB_uniG_administrativeState | BM_MSB_nonOmciManagementIdentifier)


// OMCI Entity DB
#define CFGOPTIONSTATUS_SIZE     2
typedef struct 
{
    MeInst_S meInst;
    UINT8    configurationOptionStatus[CFGOPTIONSTATUS_SIZE];
    UINT8    administrativeState;
    UINT8    managementCapability;
    UINT16   nonOmciManagementIdentifier;
} UniGME;


OMCIPROCSTATUS createUniGME(UINT16 meInstance, UINT8 managementCapability);


OMCIPROCSTATUS updateUniGAdminState(UINT16 meInstance, UINT8 administrativeState);


MeClassDef_S uniG_MeClassDef;


#endif
