
/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : McastHelper.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file has Multicast helper definitions                **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    20Jan09     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCMcastHelperh
#define __INCMcastHelperh

#include "McastOperationsProfile.h"
#include "CtcExtMcastOperationsProfile.h"

extern void fillOmciTlPortAcl(bool isDynamicAcl, bool *inUseAra, AccessControlEntry_S *pdbAclAra, int ara_size, MULTICAST_PORT_ACL_T *p_McastPortAcl, UINT16 rowId);
extern int  findNextAclEntry (bool *inUseAra, AccessControlEntry_S *paclAra, int ara_size, int *rowId, int prevIndx);

extern void fillOmciTlPortAcl_ctc(bool isDynamicAcl, bool *inUseAra, CtcAccessControlEntry_S *pdbAclAra, int ara_size, MULTICAST_PORT_ACL_T *p_McastPortAcl, UINT16 rowId);
extern int  findNextAclEntry_ctc (bool *inUseAra, CtcAccessControlEntry_S *paclAra, int ara_size, int *rowId, int prevIndx);


extern bool fillOmciTlSetPortPreview(bool *inUseAra, AllowedPreviewGroupsEntry_S *pdbAclAra, int ara_size, MULTICAST_PORT_PREVIEW_T *p_McastPortPreview, UINT16 rowId);
extern int findNextPreviewEntry     (bool *inUseAra, AllowedPreviewGroupsEntry_S *paclAra,   int ara_size, int *rowId, int prevIndx);

extern OMCIPROCSTATUS getPortIdForOmciTl(McastSubscriberConfigInfoME *pmcastsub, int *portId, char *buf);

#endif
