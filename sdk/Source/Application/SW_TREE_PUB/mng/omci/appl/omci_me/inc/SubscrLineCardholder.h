/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : Cardholder.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file has Card Holder definitions                     **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCSubscrLineCardholderh
#define __INCSubscrLineCardholderh

#include "OsGlueLayer.h"

// Cardholder Attribute bits
#define BM_MSB_actualPluginUnitType                 (0x80) 
#define BM_MSB_expectedPluginUnitType               (0x40) 
#define BM_MSB_expectedPortCount                    (0x20)
#define BM_MSB_expectedEquipmentId                  (0x10)
#define BM_MSB_actualEquipmentId                    (0x08)
#define BM_MSB_protectionProfilePointer             (0x04)
#define BM_MSB_invokeProtectionSwitch               (0x02)

#define BM_LSB_cardholderArc                        (0x80) 
#define BM_LSB_cardholderArcInterval                (0x40) 

#define BM_MSB_GETATTRIBS_CARDHOLDER  (BM_MSB_actualPluginUnitType | BM_MSB_expectedPluginUnitType | BM_MSB_expectedPortCount | BM_MSB_expectedEquipmentId | BM_MSB_actualEquipmentId | BM_MSB_protectionProfilePointer | BM_MSB_invokeProtectionSwitch)
#define BM_LSB_GETATTRIBS_CARDHOLDER  (BM_LSB_cardholderArc | BM_LSB_cardholderArcInterval)

#define BM_MSB_SETATTRIBS_CARDHOLDER  (BM_MSB_expectedPluginUnitType | BM_MSB_expectedPortCount | BM_MSB_expectedEquipmentId | BM_MSB_invokeProtectionSwitch)
#define BM_LSB_SETATTRIBS_CARDHOLDER  (BM_LSB_cardholderArc | BM_LSB_cardholderArcInterval)


// Used in MIB Upload snapshot 
#define CARDHOLDER_UPLOAD_CELL1_MSB_MASK  (BM_MSB_actualPluginUnitType | BM_MSB_expectedPluginUnitType | BM_MSB_expectedPortCount | BM_MSB_expectedEquipmentId | BM_MSB_protectionProfilePointer | BM_MSB_invokeProtectionSwitch)

#define CARDHOLDER_UPLOAD_CELL2_MSB_MASK  BM_MSB_actualEquipmentId
#define CARDHOLDER_UPLOAD_CELL2_LSB_MASK  (BM_LSB_cardholderArc | BM_LSB_cardholderArcInterval)



// OMCI Entity DB

#define EQUIPMENTID_SIZE       20
typedef struct
{
    MeInst_S meInst;
    UINT8    actualPluginUnitType;
    UINT8    expectedPluginUnitType;
    UINT8    expectedPortCount;
    UINT8    expectedEquipmentId[EQUIPMENTID_SIZE];
    UINT8    actualEquipmentId[EQUIPMENTID_SIZE];
    UINT8    protectionProfilePointer;
    UINT8    invokeProtectionSwitch;
    UINT8    arc;
    UINT8    arcInterval;
    GL_TIMER_ID      wdId;
} CardholderME;


typedef struct
{
    UINT8  actualPluginUnitType;
    UINT8  expectedPluginUnitType;
    UINT8  expectedPortCount;
} CreateCardholder_s;


OMCIPROCSTATUS createCardholderME(UINT16 meInstance, CreateCardholder_s *pCreate);


extern MeClassDef_S     cardholder_MeClassDef;



#endif
