/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : SoftwareImage.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : This file has Software Image definitions                  **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCSoftwareImageh
#define __INCSoftwareImageh

#include <stdio.h>

#define IMAGEFILE_LOCATION                         "/tmp/download.img"

// Initialization of CRC accumulator
#define CRC_INITVALUE                               0xFFFFFFFF

// Back pressure - delay response on download section responses. Units are osTaskDelay ms
#define DOWNLOADSECTION_RESP_DELAY                  0


// This value for memory allocation - array size
#define MAX_SECTIONSINSEGMENT                       256

// Software Image Attribute bits
#define BM_MSB_softwareImage_version                (0x80)
#define BM_MSB_isCommitted                          (0x40)
#define BM_MSB_isActive                             (0x20)
#define BM_MSB_isValid                              (0x10)
#define BM_MSB_productCode                          (0x08)


#define BM_MSB_GETATTRIBS_SOFTWAREIMAGE   (BM_MSB_softwareImage_version | BM_MSB_isCommitted | BM_MSB_isActive | BM_MSB_isValid | BM_MSB_productCode)    

#define SWIMAGE_UPLOAD_CELL1_MSB_MASK (BM_MSB_softwareImage_version | BM_MSB_isCommitted | BM_MSB_isActive | BM_MSB_isValid)
#define SWIMAGE_UPLOAD_CELL2_MSB_MASK (BM_MSB_productCode)

// Changed the flash write timer - simply make large and let OLT decide when to stop
// KW2 ~30 secs, MC ~3 minutes
#define FLASHWRITE_TIMER_SECS                       (300)

#define SWIMAGE_VERSION_LEN                         (14)
#define PRODUCT_CODE_LEN                            (25)
#define TRANSFER_OMCI_ME_INSTANCE_TO_MIDWARE_SW_IMAGE_ID(meInstance)     (meInstance)


// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    version[SWIMAGE_VERSION_LEN];
    UINT8    isCommitted;
    UINT8    isActive;
    UINT8    isValid;
    UINT8    productCode[PRODUCT_CODE_LEN];
} SoftwareImageME;


typedef struct
{
    UINT8    version[SWIMAGE_VERSION_LEN];
    UINT8    isCommitted;
    UINT8    isActive;
    UINT8    isValid;
} CreateSoftwareImage_s;


#define CHUNK_SIZE     8192
typedef struct
{
    int                fd;
    int                downloadPriority;
    UINT8              windowSizeM1;
    UINT32             startCmdImageSize;
    UINT16             imageInstance;
    UINT32             totalReceivedSections;
    UINT32             nextExpectedSectionInSegment;
    UINT32             expectedMaxSections;
    UINT8              leftoverBytesInLastSection;
    unsigned int       accumCrc32;                     /* "unsigned int" instead of UINT32??! GNU compiler shouts */
    UINT32             accumCrc32AtEndOfLastSegment;
    OMCIPROCSTATUS     segmentStatus;
    OMCIPROCSTATUS     dlStatus;
    UINT8              defaultWindowSizeM1;
    struct
    {
        UINT32             endSwCmdCounter;            /* No. of times EndSoftwareDownload command is received */
        UINT32             timeWhenFirstEndSwReceived; /* Time when first End Software Download command was received */
        OMCIPROCSTATUS     endSwStatus;                /* _CONTINUE during download, _ERROR or _DONE when the flashWrite completes */
    } endSwCntl;
    struct
    {
        UINT8 segmentBuffer[MAX_SECTIONSINSEGMENT * DWNLD_SECTION_SIZE];
        int   bufSize;
        int   numBytesInSegmentBuffer;
        int   seqNum;
        UINT8 flashFragmentBuffer[CHUNK_SIZE];
        int   numBytesInFlashFragmentBuffer;
    } int100;
} SoftwareDownloadControl_S;



OMCIPROCSTATUS createSoftwareImageME(UINT16 meInstance, CreateSoftwareImage_s *pCreate);

void fullInitSoftwareDownloadControl();
void stopDownloadActivity_MibReset();

void displaySoftwareDownloadControlParameters(char* name, FILE *fildes);
void briefDisplaySoftwareDownload(char* name, FILE *fildes);


MeClassDef_S softwareImage_MeClassDef;

UINT16 mapSwBankEmbeddedToOmci(UINT8 embedInstance);

void flashWriteCompletionNotification(OmciEvent_S *pevent);

void updateDefaultSegmentWindowSize(UINT8 defaultWindowSizeM1);

#endif
