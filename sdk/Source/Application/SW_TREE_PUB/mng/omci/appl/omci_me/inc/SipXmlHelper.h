/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : SipXmlHelper.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : The helper file for MMP SIP parameters in XML format      **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *    21Oct10     zeev  - initial version created.                            *
 *                                                                          
 *   $Log:   
 *                                                                     
 ******************************************************************************/

#ifndef __INCSipXmlHelperh
#define __INCSipXmlHelperh


struct voip_xml_node
{
	char *tagname;
	char tagvalue[64];	
	struct voip_xml_node *child;	
	int depth;
};

typedef struct voip_xml_node *voip_xml_node_t;



#ifdef DEBUG
#define VOIP_DBG(format, ...)  printf("%s(%d)" format "\n",__FUNCTION__,__LINE__, ##__VA_ARGS__)
#else
#define VOIP_DBG(format, ...)
#endif


#define SPRINT_TECHNIQUE 1

#ifdef SPRINT_TECHNIQUE
#define BLANK(n) {int j=n*4; for(j--;j>=0;j--) sprintf(xmlBuffer, "%s%c", xmlBuffer, 32);}
#else
#define BLANK(n) {int j; for(j=0; j < n; j++) fprintf(xmlFile, "    ");}
#endif

#define BUFSZ 64



OMCIPROCSTATUS updateXmlSipFile();


#endif
