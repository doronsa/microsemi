/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : CallControlPmHistoryData.h                                **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Call Control PM History Data ME      **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCCallControlPmHistoryDatah
#define __INCCallControlPmHistoryDatah


// Call Control PM History Data Attribute bits
#define BM_MSB_callctl_intervalEndTime              (0X80)
#define BM_MSB_callctl_thresholdDataId              (0X40)
#define BM_MSB_callSetupFailures                    (0X20)
#define BM_MSB_callSetupTimer                       (0X10)
#define BM_MSB_callTerminateFailures                (0X08)
#define BM_MSB_analogPortReleases                   (0X04)
#define BM_MSB_analogPortOffHookTimer               (0X02)



#define BM_MSB_GETATTRIBS_CALLCONTROLPMHISTORYDATA (BM_MSB_callctl_intervalEndTime | BM_MSB_callctl_thresholdDataId | BM_MSB_callSetupFailures | BM_MSB_callSetupTimer | BM_MSB_callTerminateFailures | BM_MSB_analogPortReleases | BM_MSB_analogPortOffHookTimer)

#define BM_MSB_SETATTRIBS_CALLCONTROLPMHISTORYDATA (BM_MSB_callctl_thresholdDataId)

#define BM_MSB_CREATEATTRIBS_CALLCONTROLPMHISTORYDATA (BM_MSB_callctl_thresholdDataId)

// Create message contents
#pragma pack(1)
typedef struct
{
    UINT16 thresholdDataId;
} CreateCallControlPmHistoryDataCmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    tcaBitMask[2];
    UINT8    intervalEndTime;
    UINT16   thresholdDataId;
    UINT32   callSetupFailures;
    UINT32   callSetupTimer;
    UINT32   callTerminateFailures;
    UINT32   analogPortReleases;
    UINT32   analogPortOffHookTimer;
} CallControlPmHistoryDataME;


extern MeClassDef_S callControlPmHistoryData_MeClassDef;


#endif
