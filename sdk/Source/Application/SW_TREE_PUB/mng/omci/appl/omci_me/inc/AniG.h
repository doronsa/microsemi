/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : AniG.h                                                    **/
/**                                                                          **/
/**  DESCRIPTION : This file implements ANI-G ME                             **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCAniGh
#define __INCAniGh

#include "OsGlueLayer.h"


// ANI Attribute bits
#define BM_MSB_srIndication                      (0X80)
#define BM_MSB_totalTContNumber                  (0X40)
#define BM_MSB_gemBlockLength                    (0X20)
#define BM_MSB_piggybackDbaReporting             (0X10)
#define BM_MSB_wholeOnuDbaReporting              (0X08)
#define BM_MSB_sfThreshold                       (0x04)
#define BM_MSB_sdThreshold                       (0x02)
#define BM_MSB_anigARC                           (0x01)

#define BM_LSB_anigArcInterval                   (0X80)
#define BM_LSB_opticalSignalLevel                (0X40)
#define BM_LSB_lowerOpticalThreshold             (0X20)
#define BM_LSB_upperOpticalThreshold             (0X10)
#define BM_LSB_ontResponseTime                   (0X08)
#define BM_LSB_transmitOpticalLevel              (0X04)
#define BM_LSB_lowerTransmitPowerThreshold       (0X02)
#define BM_LSB_upperTransmitPowerThreshold       (0X01)

#define BM_MSB_GETATTRIBS_ANIG (BM_MSB_srIndication | BM_MSB_totalTContNumber | BM_MSB_gemBlockLength | BM_MSB_piggybackDbaReporting | BM_MSB_wholeOnuDbaReporting | BM_MSB_sfThreshold | BM_MSB_sdThreshold | BM_MSB_anigARC)
#define BM_LSB_GETATTRIBS_ANIG (BM_LSB_anigArcInterval | BM_LSB_opticalSignalLevel | BM_LSB_lowerOpticalThreshold | BM_LSB_upperOpticalThreshold | BM_LSB_ontResponseTime | BM_LSB_transmitOpticalLevel | BM_LSB_lowerTransmitPowerThreshold | BM_LSB_upperTransmitPowerThreshold)

#define BM_MSB_SETATTRIBS_ANIG (BM_MSB_gemBlockLength | BM_MSB_sfThreshold | BM_MSB_sdThreshold | BM_MSB_anigARC)
#define BM_LSB_SETATTRIBS_ANIG (BM_LSB_anigArcInterval | BM_LSB_lowerOpticalThreshold | BM_LSB_upperOpticalThreshold | BM_LSB_lowerTransmitPowerThreshold | BM_LSB_upperTransmitPowerThreshold)


#define DEFAULT_ANIG_SFTHRESHOLD                 5
#define DEFAULT_ANIG_SDTHRESHOLD                 9

#define DEFAULT_ANIG_LOWEROPTICALTHRESHOLD          0xff
#define DEFAULT_ANIG_UPPEROPTICALTHRESHOLD          0xff
#define DEFAULT_ANIG_LOWERTRANSMITPOWERTHRESHOLD    0x81
#define DEFAULT_ANIG_UPPERTRANSMITPOWERTHRESHOLD    0x81

#define ANIG_SELFTEST    (0x07)

#if 0
#define ALARM_ID_PON_RX_POWER_HIGH  0x0101
#define ALARM_ID_PON_RX_POWER_LOW   0x0102 
#define ALARM_ID_PON_TX_POWER_HIGH  0x0103
#define ALARM_ID_PON_TX_POWER_LOW   0x0104
#endif

#define ANIG_ALARMARA_SIZE         (2)

typedef enum
{
    OMCI_ANIG_ALARM_INDEX_RX_POWER_LOW = 0,
    OMCI_ANIG_ALARM_INDEX_RX_POWER_HIGH,
    OMCI_ANIG_ALARM_INDEX_SF,
    OMCI_ANIG_ALARM_INDEX_SD,
    OMCI_ANIG_ALARM_INDEX_TX_POWER_LOW,
    OMCI_ANIG_ALARM_INDEX_TX_POWER_HIGH,
    OMCI_ANIG_ALARM_INDEX_TX_BIAS_HIGH,
} OMCI_ANIG_ALARM_INDEX_E;

// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    alarmsBitmap[OMCI_ALARM_BIT_MAP_LEN];
    UINT8    srIndication;
    UINT16   totalTContNumber;
    UINT16   gemBlockLength;
    UINT8    piggybackDbaReporting;
    UINT8    wholeOnuDbaReporting;
    UINT8    sfThreshold;
    UINT8    sdThreshold;
    UINT8    ARC;
    UINT8    arcInterval;
    UINT16   opticalSignalLevel;
    UINT8    lowerOpticalThreshold;
    UINT8    upperOpticalThreshold;
    UINT16   ontResponseTime;
    UINT16   transmitOpticalLevel;
    UINT8    lowerTransmitPowerThreshold;
    UINT8    upperTransmitPowerThreshold;
    GL_TIMER_ID      wdId;
    TimedOmciFrame_S timedOmciFrame;
    bool             testInProgress;    
} AniGME;



typedef struct
{
    UINT8   srIndication;
    UINT16  totalTContNumber;
    UINT16  gemBlockLength;
    UINT8   piggybackDbaReporting;
    UINT8   wholeOnuDbaReporting;
    UINT8   sfThreshold;
    UINT8   sdThreshold;
    UINT8   lowerTransmitPowerThreshold;
    UINT8   upperTransmitPowerThreshold;
} CreateAniGME_s;


OMCIPROCSTATUS createAniGME(UINT16 meInstance, CreateAniGME_s *pCreateAniGME);


MeClassDef_S aniG_MeClassDef;

OMCIPROCSTATUS anigTestReportHandler(MeInst_S *pmeinst, OmciEvent_S *pevent);

#endif
