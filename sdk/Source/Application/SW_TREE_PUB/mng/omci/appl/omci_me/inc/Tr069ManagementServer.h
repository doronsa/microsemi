/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : Tr069ManagementServer.h                                   **/
/**                                                                          **/
/**  DESCRIPTION : This file contains TR-069 Management Server               **/
/**                definitions and prototypes                                **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    18May12     victor  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __OMCI_TR069_HEADER__
#define __OMCI_TR069_HEADER__


// TR-069 management server parameters
#define BM_MSB_tr069_adminState                  (0x80)
#define BM_MSB_tr069_acsNetworkAddr              (0x40)
#define BM_MSB_tr069_associatedTag               (0x20)


#define BM_MSB_GETATTRIBS_TR069 (BM_MSB_tr069_adminState | BM_MSB_tr069_acsNetworkAddr | BM_MSB_tr069_associatedTag)

#define BM_MSB_SETATTRIBS_TR069 (BM_MSB_tr069_adminState | BM_MSB_tr069_acsNetworkAddr | BM_MSB_tr069_associatedTag)


// Used in MIB Upload snapshot 
#define TR069_UPLOAD_CELL1_MSB_MASK (BM_MSB_tr069_adminState | BM_MSB_tr069_acsNetworkAddr | BM_MSB_tr069_associatedTag)


// OMCI Entity DB
typedef struct Tr069ME_tag
{
    MeInst_S  meInst;
    UINT8     adminState;         
    UINT16    acsNetworkAddr;
    UINT16    associatedTag;
} Tr069ME;


OMCIPROCSTATUS createTr069ME(UINT16 meInstance);


MeClassDef_S tr069_MeClassDef;


#endif /* __OMCI_TR069_HEADER__ */
