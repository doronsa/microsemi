/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VoiceServiceItem.h                                        **/
/**                                                                          **/
/**  DESCRIPTION : This file contains Voice Service Item definitions         **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Aug04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVoiceServiceItemh
#define __INCVoiceServiceItemh

#define MAX_LARGESTRINGPARTS     15
#define LARGESTRING_SIZE         (MAX_LARGESTRINGPARTS * PART_SIZE)

typedef struct 
{
    ServiceItem_S  si;
    UINT16         pptpPotsUni;
    UINT16         uaInstance;
    UINT8          uAor[LARGESTRING_SIZE+1];
} VoiceServiceItem_S;

OMCIPROCSTATUS createVoiceServiceItem(VoIPVoiceCtpME *pvoipvoicectp);

extern void voipReadyHandler();

extern ServiceDb voice_ServiceDb;

#endif
  
