/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : entityInclude.h                                           **/
/**                                                                          **/
/**  DESCRIPTION : This file contains #includes for all the MEs              **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    15Apr04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCentityIncludeh
#define __INCentityIncludeh



#include "OntG.h"
#include "Ont2G.h"
#include "OntData.h"
#include "PriorityQueueG.h"
#include "PptpEthernetUni.h"
#include "GemInterWorkingTp.h"
#include "GemPortNetworkCtp.h"
#include "MacBridgePortConfigurationData.h"
#include "MacBridgeServiceProfile.h"
#include "SoftwareImage.h"
#include "SubscrLineCard.h"
#include "SubscrLineCardholder.h"
#include "PptpPotsUni.h"
#include "EthernetPmHistoryData.h"
#include "UniG.h"
#include "MacBridgeConfigData.h"
#include "MacBridgePortDesignationData.h"
#include "MacBridgePortBridgeTable.h"
#include "MacBridgePortFilterTable.h"
#include "TcontG.h"
#include "AniG.h"
#include "GalEthernetProfile.h"
#include "GalEthernetPmHistoryData.h"
#include "ThresholdData1.h"
#include "ThresholdData2.h"
#include "TrafficSchedulerG.h"
#include "GemPortPmHistoryData.h"
#include "GemPortNetworkCtpPm.h"
#include "VoiceServiceProfile.h"
#include "VoIPApplicServiceProfile.h"
#include "VoIPConfigData.h"
#include "VoIPFeatureAccessCodes.h"
#include "VoIPLineStatus.h"
#include "VoIPMediaProfile.h"
#include "VoIPVoiceCtp.h"
#include "TcpUdpConfigData.h"
#include "RtpProfileData.h"
#include "SipAgentConfigData.h"
#include "SipConfigPortal.h"
#include "SipUserData.h"
#include "NetworkAddress.h"
#include "NetworkDialPlanTable.h"
#include "NetworkAddress.h"
#include "NetworkDialPlanTable.h"
#include "AuthSecurityMethod.h"
#include "LargeString.h"
#include "IpHostConfigData.h"
#include "OntRemoteDebug.h"
#include "IpHostPmHistoryData.h"
#include "SipAgentMonitoringData.h"
#include "SipCallInitPmHistoryData.h"
#include "CallControlPmHistoryData.h"
#include "RtpMonitoringData.h"
#include "Mapper8021pServiceProfile.h"
#include "MulticastGemInterworkingTp.h"
#include "EthernetPm2.h"
#include "VlanTaggingOperationConfig.h"
#include "ExtVlanTaggingOpCfg.h"
#include "VlanTaggingFilterData.h"
#include "EthernetPm3.h"
#include "TrafDesc.h"
#include "OmciMe.h"
#include "MngdEntity.h"
#include "VirtualEthernetIP.h"
#include "McastOperationsProfile.h"
#include "McastSubscriberConfigInfo.h"
#include "McastSubscriberMonitor.h"
#include "OltG.h"
#include "CtcExtMcastOperationsProfile.h"
#include "CtcOnuCapability.h"
#include "CtcLoidAuthentication.h"
#include "CtcLoopbackDetection.h"
#include "EthernetFrameExtendedPm.h"
#include "Tr069ManagementServer.h"
#include "EthernetFramePmHistoryDataUs.h"
#include "EthernetFramePmHistoryDataDs.h"

#endif
