/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VoIPLineStatus.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : This file has VoIP Line Status ME definitions             **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVoIPLineStatush
#define __INCVoIPLineStatush


// Voice Line Status Attribute bits
#define BM_MSB_voipCodecUsed                  (0x80)
#define BM_MSB_voipServerStatus               (0x40)
#define BM_MSB_voipPortSessionType            (0x20)
#define BM_MSB_voipCall1PacketPeriod          (0x10)
#define BM_MSB_voipCall2PacketPeriod          (0x08) 
#define BM_MSB_voipCall1DestAddr              (0x04)
#define BM_MSB_voipCall2DestAddr              (0x02) 


#define BM_MSB_GETATTRIBS_VOIPLINESTATUS BM_MSB_voipCodecUsed | BM_MSB_voipServerStatus | BM_MSB_voipPortSessionType | BM_MSB_voipCall1PacketPeriod | BM_MSB_voipCall2PacketPeriod | BM_MSB_voipCall1DestAddr | BM_MSB_voipCall2DestAddr


// Used in MIB Upload snapshot 
#define VOIPLINESTATUS_UPLOAD_CELL1_MSB_MASK (BM_MSB_voipCodecUsed | BM_MSB_voipServerStatus | BM_MSB_voipPortSessionType | BM_MSB_voipCall1PacketPeriod | BM_MSB_voipCall2PacketPeriod)

#define VOIPLINESTATUS_UPLOAD_CELL2_MSB_MASK (BM_MSB_voipCall1DestAddr)
#define VOIPLINESTATUS_UPLOAD_CELL3_MSB_MASK (BM_MSB_voipCall2DestAddr)


// OMCI Entity DB

#define VOIPCALLDESTADDR_SIZE                25
typedef struct
{
    MeInst_S meInst;
    UINT16   voipCodecUsed;
    UINT8    voipServerStatus;
    UINT8    voipPortSessionType;
    UINT16   voipCall1PacketPeriod;
    UINT16   voipCall2PacketPeriod;
    UINT8    voipCall1DestAddr[VOIPCALLDESTADDR_SIZE];
    UINT8    voipCall2DestAddr[VOIPCALLDESTADDR_SIZE];
} VoIPLineStatusME;


OMCIPROCSTATUS createVoIPLineStatusME(UINT16 meInstance);


MeClassDef_S voIPLineStatus_MeClassDef;


#endif
