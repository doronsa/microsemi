/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : PptpEthernetUni.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : This file has PPTP Ethernet definitions and prototypes    **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCPptpEthernetUnih
#define __INCPptpEthernetUnih

#include "OsGlueLayer.h"

// Physical Path TP - Ethernet UNI Attribute bits
#define BM_MSB_expectedType                         (0x80) 
#define BM_MSB_sensedType                           (0x40) 
#define BM_MSB_autoDetectionConfiguration           (0x20) 
#define BM_MSB_ethernetLoopbackConfiguration        (0x10)
#define BM_MSB_pptp_ethernetUni_administrativeState (0x08)
#define BM_MSB_pptp_ethernetUni_operationalState    (0x04) 
#define BM_MSB_configurationInd                     (0x02) 
#define BM_MSB_maxframeSize                         (0x01) 
#define BM_LSB_dteOrDceInd                          (0x80) 
#define BM_LSB_pauseTime                            (0x40) 
#define BM_LSB_bridgeOrIPInd                        (0x20) 
#define BM_LSB_pptp_ethernetUni_ARC                 (0x10) 
#define BM_LSB_pptp_ethernetUni_arcInterval         (0x08) 
#define BM_LSB_pppoEFilter                          (0x04) 
#define BM_LSB_powerControl                         (0x02) 


#define BM_MSB_GETATTRIBS_PPTPETHERNETUNI BM_MSB_expectedType | BM_MSB_sensedType | BM_MSB_autoDetectionConfiguration | BM_MSB_ethernetLoopbackConfiguration |	BM_MSB_pptp_ethernetUni_administrativeState | BM_MSB_pptp_ethernetUni_operationalState | BM_MSB_configurationInd | BM_MSB_maxframeSize

#define BM_LSB_GETATTRIBS_PPTPETHERNETUNI BM_LSB_dteOrDceInd | BM_LSB_pauseTime | BM_LSB_bridgeOrIPInd | BM_LSB_pptp_ethernetUni_ARC | BM_LSB_pptp_ethernetUni_arcInterval | BM_LSB_pppoEFilter | BM_LSB_powerControl


#define BM_MSB_SETATTRIBS_PPTPETHERNETUNI BM_MSB_expectedType | BM_MSB_pptp_ethernetUni_administrativeState | BM_MSB_autoDetectionConfiguration | BM_MSB_maxframeSize | BM_MSB_ethernetLoopbackConfiguration
#define BM_LSB_SETATTRIBS_PPTPETHERNETUNI BM_LSB_dteOrDceInd | BM_LSB_pauseTime | BM_LSB_bridgeOrIPInd | BM_LSB_pptp_ethernetUni_ARC | BM_LSB_pptp_ethernetUni_arcInterval | BM_LSB_pppoEFilter | BM_LSB_powerControl


// OMCI Entity DB

#define ETHERNETUNI_ALARMARA_SIZE    1

typedef enum
{
    OMCI_PPTPETHERNETUNI_ALARM_INDEX_LAN_LOS = 0,
} OMCI_PPTPETHERNETUNI_ALARM_INDEX_E;

typedef struct PptpEthernetUniME_tag
{
    MeInst_S     meInst;
    UINT8        alarmsBitmap[OMCI_ALARM_BIT_MAP_LEN];
    UINT8        expectedType;
    UINT8        sensedType;
    UINT8        autoDetectionConfiguration;
    UINT8        ethernetLoopbackConfiguration;
    UINT8        administrativeState;
    UINT8        operationalState;
    UINT8        configurationInd;
    UINT16       maxframeSize;
    UINT8        dteOrDceInd;
    UINT16       pauseTime;
    UINT8        bridgeOrIPInd;
    UINT8        ARC;
    UINT8        arcInterval;
    UINT8        pppoEFilter;
    UINT8        powerControl;
    GL_TIMER_ID  wdId;
} PptpEthernetUniME;


OMCIPROCSTATUS embedEthernetUniAdminStatus(UINT16 ethernetUniInstance, ADMINISTRATIVESTATUS administrativeState);
OMCIPROCSTATUS embedEthernetMaxframeSize(UINT16 ethernetUniInstance, UINT16 maxframeSize);

OMCIPROCSTATUS createPptpEthernetUniME(UINT16 meInstance);


MeClassDef_S pptpEthernetUni_MeClassDef;


#endif
