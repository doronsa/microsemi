/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : EthernetPm2.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file is the Ethernet PM History Data 2 include file  **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *    27Aug06     zeev  - initial version created.                            *
 * ========================================================================== *
 *                                                                          
 *   $Log:   
 *                                                                     
 ******************************************************************************/

#ifndef __INCEthernetPm2h
#define __INCEthernetPm2h



// Ethernet PM History Data Attribute bits
#define BM_MSB_ethpm2_intervalEndTime               (0X80)
#define BM_MSB_ethpm2_thresholdDataId               (0X40)
#define BM_MSB_pppOEFilteredFrameCounter            (0X20)

#define BM_MSB_GETATTRIBS_ETHERNETPM2 BM_MSB_ethpm2_intervalEndTime | BM_MSB_ethpm2_thresholdDataId | BM_MSB_pppOEFilteredFrameCounter

#define BM_MSB_SETATTRIBS_ETHERNETPM2 BM_MSB_ethpm2_thresholdDataId

#define BM_MSB_CREATEATTRIBS_ETHERNETPM2 BM_MSB_ethpm2_thresholdDataId


// messageContents - Ethernet Mon 2 Create Command
#pragma pack(1)
typedef struct
{
    UINT16 thresholdDataId;
} CreateEthernetPm2Cmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    tcaBitMask[2];
    UINT8    intervalEndTime;
    UINT16   thresholdDataId;
    UINT32   pppOEFilteredFrameCounter;
} EthernetPm2ME;


extern MeClassDef_S ethernetPm2_MeClassDef;


#endif
