/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : MacBridgeServiceProfile.h                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Mac Bridge Service Profile ME        **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCMacBridgeServiceProfileh
#define __INCMacBridgeServiceProfileh



// MAC Bridge Service Profile Attribute bits
#define BM_MSB_spanningTreeInd                      (0x80)
#define BM_MSB_learningInd                          (0x40) 
#define BM_MSB_atmPortBridgingInd                   (0x20)
#define BM_MSB_priority                             (0x10)
#define BM_MSB_maxAge                               (0x08)      
#define BM_MSB_helloTime                            (0x04) 
#define BM_MSB_forwardDelay                         (0x02)     
#define BM_MSB_unknownMacAddressDiscard             (0x01)

#define BM_LSB_macLearningDepth                     (0x80)
#define BM_LSB_dynamicFilteringAgeingTime           (0x40)

#define BM_MSB_GETATTRIBS_MACBRIDGESERVICEPROFILE (BM_MSB_spanningTreeInd | BM_MSB_learningInd | BM_MSB_atmPortBridgingInd | BM_MSB_priority | BM_MSB_maxAge | BM_MSB_helloTime | BM_MSB_forwardDelay | BM_MSB_unknownMacAddressDiscard)
#define BM_MSB_SETATTRIBS_MACBRIDGESERVICEPROFILE (BM_MSB_spanningTreeInd | BM_MSB_learningInd | BM_MSB_atmPortBridgingInd | BM_MSB_priority | BM_MSB_maxAge | BM_MSB_helloTime | BM_MSB_forwardDelay | BM_MSB_unknownMacAddressDiscard)

#define BM_LSB_GETATTRIBS_MACBRIDGESERVICEPROFILE (BM_LSB_macLearningDepth | BM_LSB_dynamicFilteringAgeingTime)
#define BM_LSB_SETATTRIBS_MACBRIDGESERVICEPROFILE (BM_LSB_macLearningDepth | BM_LSB_dynamicFilteringAgeingTime)

#define BM_MSB_CREATEATTRIBS_MACBRIDGESERVICEPROFILE (BM_MSB_spanningTreeInd | BM_MSB_learningInd | BM_MSB_atmPortBridgingInd | BM_MSB_priority | BM_MSB_maxAge | BM_MSB_helloTime | BM_MSB_forwardDelay | BM_MSB_unknownMacAddressDiscard)
#define BM_LSB_CREATEATTRIBS_MACBRIDGESERVICEPROFILE (BM_LSB_macLearningDepth | BM_LSB_dynamicFilteringAgeingTime)


// OMCI Entity DB
typedef struct MacBridgeServiceProfileME_tag
{
    MeInst_S meInst;
    UINT8    spanningTreeInd;
    UINT8    learningInd;
    UINT8    atmPortBridgingInd;
    UINT16   priority;
    UINT16   maxAge;
    UINT16   helloTime;
    UINT16   forwardDelay;
    UINT8    unknownMacAddressDiscard;
    UINT8    macLearningDepth;
    UINT32   dynamicFilteringAgeingTime;
} MacBridgeServiceProfileME;



// messageContents - Create MAC Bridge Service Profile Command
#pragma pack(1)
typedef struct
{
    UINT8   spanningTreeInd;
    UINT8   learningInd;
    UINT8   atmPortBridgingInd;
    UINT16  priority;
    UINT16  maxAge;
    UINT16  helloTime;
    UINT16  forwardDelay;
    UINT8   unknownMacAddressDiscard;
    UINT8   macLearningDepth;
    UINT32  dynamicFilteringAgeingTime;
} CreateMacBridgeServiceProfile_s;
#pragma pack(0)


MeClassDef_S macBridgeServiceProfile_MeClassDef;


#endif
