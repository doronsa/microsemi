/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : CtcLoopbackDetection.h                                    **/
/**                                                                          **/
/**  DESCRIPTION : This file contains CTL loopback detection definitions     **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    24May12     Victor  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __OMCI_CTC_LPBK_DETECTION__
#define __OMCI_CTC_LPBK_DETECTION__


/* CTC loopback detection Attribute bits */
#define BM_MSB_lpbk_operator_id           (0x80)
#define BM_MSB_lpbk_management            (0x40)
#define BM_MSB_lpbk_port_down             (0x20)
#define BM_MSB_lpbk_mesg_frequency        (0x10)
#define BM_MSB_lpbk_recovery_interval     (0x08)
#define BM_MSB_lpbk_port_table            (0x04)


#define BM_MSB_GETATTRIBS_LPBKDETECTION (BM_MSB_lpbk_operator_id | BM_MSB_lpbk_management | BM_MSB_lpbk_port_down | BM_MSB_lpbk_mesg_frequency | BM_MSB_lpbk_recovery_interval | BM_MSB_lpbk_port_table)

#define BM_MSB_SETATTRIBS_LPBKDETECTION (BM_MSB_lpbk_management | BM_MSB_lpbk_port_down | BM_MSB_lpbk_mesg_frequency | BM_MSB_lpbk_recovery_interval | BM_MSB_lpbk_port_table)


/* Used in MIB Upload snapshot */
#define LPBKDETECTION_UPLOAD_CELL1_MSB_MASK (BM_MSB_lpbk_operator_id | BM_MSB_lpbk_management | BM_MSB_lpbk_port_down | BM_MSB_lpbk_mesg_frequency | BM_MSB_lpbk_recovery_interval)
#define LPBKDETECTION_UPLOAD_CELL2_MSB_MASK (BM_MSB_lpbk_port_table)

/* OMCI Entity DB */
#define LPBK_DETECTION_OPERATORID_LEN    (4)
#define LPBK_MAX_TABLE_NUM               (4)

/* loopback detection state */
typedef enum
{
    LPBK_DETECTION_MANAGEMENT_INACTIVE = 0,      
    LPBK_DETECTION_MANAGEMENT_ACTIVE   = 1,
    
} LPBK_DETECTION_MANAGEMENT_STATE_E;

/* loopback detection port down mode */
typedef enum
{
    OMCI_LPBK_DETECTION_ALARM_INDEX_PORT_1 = 0,
    OMCI_LPBK_DETECTION_ALARM_INDEX_PORT_2 = 1,
    OMCI_LPBK_DETECTION_ALARM_INDEX_PORT_3 = 2,
    OMCI_LPBK_DETECTION_ALARM_INDEX_PORT_4 = 3,
    OMCI_LPBK_DETECTION_ALARM_INDEX_PORT_5 = 4,
    OMCI_LPBK_DETECTION_ALARM_INDEX_PORT_6 = 5,
    OMCI_LPBK_DETECTION_ALARM_INDEX_PORT_7 = 6,
    OMCI_LPBK_DETECTION_ALARM_INDEX_PORT_8 = 7,

} OMCI_LPBK_DETECTION_ALARM_INDEX_E;


/* loopback detection port down mode */
typedef enum
{
    LPBK_DETECTION_PORTDOWN_MANUAL = 0,      
    LPBK_DETECTION_PORTDOWN_AUTO   = 1,
    
} LPBK_DETECTION_PORTDOWN_MODE_E;

#define LPBK_DETECTION_DEFAULT_FREQUENCY (1)
#define LPBK_DETECTION_RECOVERY_INTERVAL (300)

#pragma pack(1)
typedef struct
{
    UINT8  index;
    UINT16 meID;    
    UINT16 svlan;
    UINT16 cvlan;
} LpbkPortVid_S;
#pragma pack(0)

typedef struct 
{
    MeInst_S      meInst;
    UINT8         alarmsBitmap[OMCI_ALARM_BIT_MAP_LEN];
    UINT8         operatorId[LPBK_DETECTION_OPERATORID_LEN];
    UINT16        management;
    UINT16        portDownOption;
    UINT16        msgFrequency;
    UINT16        recoveryInterval;
    LpbkPortVid_S portTable[LPBK_MAX_TABLE_NUM];  

    UINT32        portTableActualSize;
    bool          portTableInUse[LPBK_MAX_TABLE_NUM];    
} CtcLpbkDetectionME;

typedef struct 
{
    UINT8         operatorId[LPBK_DETECTION_OPERATORID_LEN];
    UINT16        management;
    UINT16        portDownOption;
    UINT16        msgFrequency;
    UINT16        recoveryInterval;
    LpbkPortVid_S portTable[LPBK_MAX_TABLE_NUM];  
} CreateCtcLpbkDetection_S;


OMCIPROCSTATUS createCtcLpbkDetectionME(UINT16 meInstance, CreateCtcLpbkDetection_S *pcreate);

MeClassDef_S ctcLpbkDetection_MeClassDef;


#endif /* __OMCI_CTC_LPBK_DETECTION__ */
