/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : EthernetPmHistoryData.h                                   **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Ethernet PM History Data ME          **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCEthernetPmHistoryDatah
#define __INCEthernetPmHistoryDatah



// Ethernet PM History Data Attribute bits
#define BM_MSB_intervalEndTime                      (0X80)
#define BM_MSB_thresholdDataId                      (0X40)
#define BM_MSB_fcsErrors                            (0X20)
#define BM_MSB_excessiveCollisionCounter            (0X10)
#define BM_MSB_lateCollisionCounter                 (0X08)
#define BM_MSB_frameTooLongs                        (0X04)
#define BM_MSB_bufferOverflowsOnReceive             (0X02)
#define BM_MSB_bufferOverflowsOnTransmit            (0X01)

#define BM_LSB_singleCollisionFrameCounter          (0X80)
#define BM_LSB_multipleCollisionsFrameCounter       (0X40)
#define BM_LSB_sqeCounter                           (0X20)
#define BM_LSB_deferredTransmissionCounter          (0X10)
#define BM_LSB_internalMacTransmitErrorCounter      (0X08)
#define BM_LSB_carrierSenseErrorCounter             (0X04)
#define BM_LSB_alignmentErrorCounter                (0X02)
#define BM_LSB_internalMacReceiveErrorCounter       (0X01)

#define BM_MSB_GETATTRIBS_ETHERNETPMHISTORYDATA BM_MSB_intervalEndTime | BM_MSB_thresholdDataId | BM_MSB_fcsErrors | BM_MSB_excessiveCollisionCounter | BM_MSB_lateCollisionCounter | BM_MSB_frameTooLongs | BM_MSB_bufferOverflowsOnReceive | BM_MSB_bufferOverflowsOnTransmit

#define BM_LSB_GETATTRIBS_ETHERNETPMHISTORYDATA BM_LSB_singleCollisionFrameCounter | BM_LSB_multipleCollisionsFrameCounter | BM_LSB_sqeCounter | BM_LSB_deferredTransmissionCounter | BM_LSB_internalMacTransmitErrorCounter | BM_LSB_carrierSenseErrorCounter | BM_LSB_alignmentErrorCounter | BM_LSB_internalMacReceiveErrorCounter


#define BM_MSB_SETATTRIBS_ETHERNETPMHISTORYDATA BM_MSB_thresholdDataId

#define BM_MSB_CREATEATTRIBS_ETHERNETPMHISTORYDATA BM_MSB_thresholdDataId

// messageContents - AAL5 Prot Mon Create Command
#pragma pack(1)
typedef struct
{
    UINT16 thresholdDataId;
} CreateEthernetPmHistoryDataCmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    tcaBitMask[2];
    UINT8    intervalEndTime;
    UINT16   thresholdDataId;
    UINT32   fcsErrors;
    UINT32   excessiveCollisionCounter;
    UINT32   lateCollisionCounter;
    UINT32   frameTooLongs;
    UINT32   bufferOverflowsOnReceive;
    UINT32   bufferOverflowsOnTransmit;
    UINT32   singleCollisionFrameCounter;
    UINT32   multipleCollisionsFrameCounter;
    UINT32   sqeCounter;
    UINT32   deferredTransmissionCounter;
    UINT32   internalMacTransmitErrorCounter;
    UINT32   carrierSenseErrorCounter;
    UINT32   alignmentErrorCounter;
    UINT32   internalMacReceiveErrorCounter;
} EthernetPmHistoryDataME;


extern MeClassDef_S ethernetPmHistoryData_MeClassDef;


#endif
