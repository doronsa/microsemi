/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : LargeString.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Large String ME                      **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCLargeStringh
#define __INCLargeStringh


// LargeString Attribute bits
#define BM_MSB_numberOfParts   (0x80)
#define BM_MSB_part1           (0x40) 
#define BM_MSB_part2           (0x20) 
#define BM_MSB_part3           (0x10) 
#define BM_MSB_part4           (0x08)
#define BM_MSB_part5           (0x04)
#define BM_MSB_part6           (0x02)
#define BM_MSB_part7           (0x01)
#define BM_LSB_part8           (0x80)
#define BM_LSB_part9           (0x40)
#define BM_LSB_part10          (0x20)
#define BM_LSB_part11          (0x10) 
#define BM_LSB_part12          (0x08)
#define BM_LSB_part13          (0x04)
#define BM_LSB_part14          (0x02)
#define BM_LSB_part15          (0x01)


#define BM_MSB_GETATTRIBS_LARGESTRING BM_MSB_numberOfParts | BM_MSB_part1 | BM_MSB_part2 | BM_MSB_part3 | BM_MSB_part4 | BM_MSB_part5 | BM_MSB_part6 | BM_MSB_part7
#define BM_LSB_GETATTRIBS_LARGESTRING BM_LSB_part8 | BM_LSB_part9 | BM_LSB_part10 | BM_LSB_part11 | BM_LSB_part12 | BM_LSB_part13 | BM_LSB_part14 | BM_LSB_part15

#define BM_MSB_SETATTRIBS_LARGESTRING BM_MSB_numberOfParts | BM_MSB_part1 | BM_MSB_part2 | BM_MSB_part3 | BM_MSB_part4 | BM_MSB_part5 | BM_MSB_part6 | BM_MSB_part7
#define BM_LSB_SETATTRIBS_LARGESTRING BM_LSB_part8 | BM_LSB_part9 | BM_LSB_part10 | BM_LSB_part11 | BM_LSB_part12 | BM_LSB_part13 | BM_LSB_part14 | BM_LSB_part15


// Used in MIB Upload snapshot 
#define LARGESTRING_UPLOAD_CELL1_MSB_MASK  (BM_MSB_numberOfParts)
#define LARGESTRING_UPLOAD_CELL2_MSB_MASK  (BM_MSB_part1)
#define LARGESTRING_UPLOAD_CELL3_MSB_MASK  (BM_MSB_part2)
#define LARGESTRING_UPLOAD_CELL4_MSB_MASK  (BM_MSB_part3)
#define LARGESTRING_UPLOAD_CELL5_MSB_MASK  (BM_MSB_part4)
#define LARGESTRING_UPLOAD_CELL6_MSB_MASK  (BM_MSB_part5)
#define LARGESTRING_UPLOAD_CELL7_MSB_MASK  (BM_MSB_part6)
#define LARGESTRING_UPLOAD_CELL8_MSB_MASK  (BM_MSB_part7)
#define LARGESTRING_UPLOAD_CELL9_LSB_MASK  (BM_LSB_part8)
#define LARGESTRING_UPLOAD_CELL10_LSB_MASK (BM_LSB_part9)
#define LARGESTRING_UPLOAD_CELL11_LSB_MASK (BM_LSB_part10)
#define LARGESTRING_UPLOAD_CELL12_LSB_MASK (BM_LSB_part11)
#define LARGESTRING_UPLOAD_CELL13_LSB_MASK (BM_LSB_part12)
#define LARGESTRING_UPLOAD_CELL14_LSB_MASK (BM_LSB_part13)
#define LARGESTRING_UPLOAD_CELL15_LSB_MASK (BM_LSB_part14)
#define LARGESTRING_UPLOAD_CELL16_LSB_MASK (BM_LSB_part15)




// OMCI Entity DB

#define PART_SIZE                        25
#define MAX_NUMPARTS                     15
typedef struct
{
    MeInst_S meInst;
    UINT8    numberOfParts;
    UINT8    part1[PART_SIZE];
    UINT8    part2[PART_SIZE];
    UINT8    part3[PART_SIZE];
    UINT8    part4[PART_SIZE];
    UINT8    part5[PART_SIZE];
    UINT8    part6[PART_SIZE];
    UINT8    part7[PART_SIZE];
    UINT8    part8[PART_SIZE];
    UINT8    part9[PART_SIZE];
    UINT8    part10[PART_SIZE];
    UINT8    part11[PART_SIZE];
    UINT8    part12[PART_SIZE];
    UINT8    part13[PART_SIZE];
    UINT8    part14[PART_SIZE];
    UINT8    part15[PART_SIZE];
} LargeStringME;


OMCIPROCSTATUS copyLargeString(UINT8 *ptarget, LargeStringME *pls);


MeClassDef_S largeString_MeClassDef;


#endif
