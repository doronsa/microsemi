/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : McastSubscriberMonitor.h                                  **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Mcast Subscriber Monitor ME          **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCMcastSubscriberMonitorh
#define __INCMcastSubscriberMonitorh



// IpHost PM History Data Attribute bits
#define BM_MSB_mcsubmon_meType               (0X80)
#define BM_MSB_currentMulticastBandwidth     (0X40)
#define BM_MSB_joinMessagesCounter           (0X20)
#define BM_MSB_bandwidthExceededCounter      (0X10)
#define BM_MSB_activeGroupListTable          (0X08)

#define BM_MSB_GETATTRIBS_MCASTSUBSCRIBERMONITOR BM_MSB_mcsubmon_meType | BM_MSB_currentMulticastBandwidth | BM_MSB_joinMessagesCounter | BM_MSB_bandwidthExceededCounter | BM_MSB_activeGroupListTable

#define BM_MSB_SETATTRIBS_MCASTSUBSCRIBERMONITOR BM_MSB_mcsubmon_meType

#define BM_MSB_CREATEATTRIBS_MCASTSUBSCRIBERMONITOR BM_MSB_mcsubmon_meType

typedef enum
{
    MCSUBMONETYPE_MACBRIDGE, MCSUBMONETYPE_8021PMAPPER
} MCSUBMONETYPE;


#pragma pack(1)
typedef struct
{
    UINT16  vlanId;
    UINT8   srcIpAddress[4];
    UINT8   mcastDestIpAddress[4];
    UINT32  bestEffortActualBandwidth;
    UINT8   clientIpAddress[4];
    UINT32  timeSinceLastJoin;
    UINT8   reserved[2];
} ActiveGroupEntry_S;


#define MAX_ACTIVE_GROUP_ENTRIES        10
// OMCI Entity DB
typedef struct
{
    MeInst_S           meInst;
    UINT16             meType;
    UINT32             currentMulticastBandwidth;
    UINT32             joinMessagesCounter;
    UINT32             bandwidthExceededCounter;
    ActiveGroupEntry_S activeGroupEntryAra[MAX_ACTIVE_GROUP_ENTRIES];

    UINT32             activeGroupActualSize;
    bool               activeGroupEntryInUse[MAX_ACTIVE_GROUP_ENTRIES];

    struct
    {
        UINT32  currentTicks;
        ActiveGroupEntry_S ara[MAX_ACTIVE_GROUP_ENTRIES];
    } getNextBufActiveGroup;

} McastSubscriberMonitorME;


// messageContents - Create Multicast Subscriber Monitor
#pragma pack(1)
typedef struct
{
    UINT8      meType;
} CreateMcastSubscriberMonitor_S;
#pragma pack(0)


extern MeClassDef_S mcastSubscriberMonitor_MeClassDef;


#endif
