/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : GemPortNetworkCtp.c                                       **/
/**                                                                          **/
/**  DESCRIPTION : This file contains GEM Port Network CTP ME definitions    **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    20May05     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCGemPortNetworkCtph
#define __INCGemPortNetworkCtph


// GEM Port Network CTP Attribute bits
#define BM_MSB_portIdValue                          (0x80)
#define BM_MSB_gemnetwctp_tcontPointer              (0x40)
#define BM_MSB_direction                            (0x20)
#define BM_MSB_priorityQueuePointerForUpstream      (0x10)
#define BM_MSB_trafficDescriptorProfilePointer_US   (0x08)
#define BM_MSB_uniCounter                           (0x04)
#define BM_MSB_priorityQueuePointerForDownstream    (0x02)
#define BM_MSB_encryptionState                      (0x01)

#define BM_LSB_trafficDescriptorProfilePointer_DS   (0x80)

#define BM_MSB_GETATTRIBS_GEMPORTNETWORKCTP (BM_MSB_portIdValue | BM_MSB_gemnetwctp_tcontPointer | BM_MSB_direction | BM_MSB_priorityQueuePointerForUpstream | BM_MSB_trafficDescriptorProfilePointer_US | BM_MSB_uniCounter | BM_MSB_priorityQueuePointerForDownstream | BM_MSB_encryptionState)
#define BM_LSB_GETATTRIBS_GEMPORTNETWORKCTP (BM_LSB_trafficDescriptorProfilePointer_DS)

#define BM_MSB_SETATTRIBS_GEMPORTNETWORKCTP (BM_MSB_portIdValue | BM_MSB_gemnetwctp_tcontPointer | BM_MSB_direction | BM_MSB_priorityQueuePointerForUpstream | BM_MSB_trafficDescriptorProfilePointer_US | BM_MSB_priorityQueuePointerForDownstream)
#define BM_LSB_SETATTRIBS_GEMPORTNETWORKCTP (BM_LSB_trafficDescriptorProfilePointer_DS)

#define BM_MSB_CREATEATTRIBS_GEMPORTNETWORKCTP (BM_MSB_portIdValue | BM_MSB_gemnetwctp_tcontPointer | BM_MSB_direction | BM_MSB_priorityQueuePointerForUpstream | BM_MSB_trafficDescriptorProfilePointer_US | BM_MSB_priorityQueuePointerForDownstream)
#define BM_LSB_CREATEATTRIBS_GEMPORTNETWORKCTP (BM_LSB_trafficDescriptorProfilePointer_DS)


// OMCI Entity DB
#define GEMPORTNETWORKCTP_MAX_GEM_PORTS    4096
#define GEMPORTNETWORKCTP_ALARMARA_SIZE    1
typedef struct GemPortNetworkCtpME_tag
{
    MeInst_S meInst;
    UINT8    alarmsBitmap[OMCI_ALARM_BIT_MAP_LEN];
    UINT16   portIdValue;
    UINT16   tcontPointer;
    UINT8    direction;
    UINT16   priorityQueuePointerForUpstream;
    UINT16   trafficDescriptorProfilePointer_US;
    UINT8    uniCounter;
    UINT16   priorityQueuePointerForDownstream;
    UINT8    encryptionState;
    UINT16   trafficDescriptorProfilePointer_DS;
    UINT16   originalTcontPointer;
    UINT16   originalUsQueuePtr;
} GemPortNetworkCtpME;



// messageContents - Create GEM Port Network CTP Command
#pragma pack(1)
typedef struct
{
    UINT16  portIdValue;
    UINT16  tcontPointer;
    UINT8   direction;
    UINT16  priorityQueuePointerForUpstream;
    UINT16  trafficDescriptorProfilePointer_US;
    UINT16  priorityQueuePointerForDownstream;
    UINT16  trafficDescriptorProfilePointer_DS;
} CreateGemPortNetworkCtp_s;
#pragma pack(0)


MeClassDef_S gemPortNetworkCtp_MeClassDef;

void refreshGemPortSettings ();

#endif
