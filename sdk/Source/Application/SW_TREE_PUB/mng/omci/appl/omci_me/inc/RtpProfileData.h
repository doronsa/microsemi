/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : RtpProfileData.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : This file has RTP Profile Data definitions                **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCRtpProfileDatah
#define __INCRtpProfileDatah


// RTP Profile Data Attribute bits
#define BM_MSB_localPortMin                (0x80)
#define BM_MSB_localPortMax                (0x40) 
#define BM_MSB_dscpMark                    (0x20)
#define BM_MSB_piggybackEvents             (0x10)
#define BM_MSB_toneEvents                  (0x08)
#define BM_MSB_dtmfEvents                  (0x04) 
#define BM_MSB_casEvents                   (0x02)



#define BM_MSB_GETATTRIBS_RTPPROFILEDATA (BM_MSB_localPortMin | BM_MSB_localPortMax |  BM_MSB_dscpMark | BM_MSB_piggybackEvents | BM_MSB_toneEvents | BM_MSB_dtmfEvents | BM_MSB_casEvents)

#define BM_MSB_SETATTRIBS_RTPPROFILEDATA (BM_MSB_localPortMin | BM_MSB_localPortMax |  BM_MSB_dscpMark | BM_MSB_piggybackEvents | BM_MSB_toneEvents | BM_MSB_dtmfEvents | BM_MSB_casEvents)

#define BM_MSB_CREATEATTRIBS_RTPPROFILEDATA (BM_MSB_localPortMin | BM_MSB_localPortMax |  BM_MSB_dscpMark | BM_MSB_piggybackEvents | BM_MSB_toneEvents | BM_MSB_dtmfEvents | BM_MSB_casEvents)


// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT16   localPortMin;
    UINT16   localPortMax;
    UINT8    dscpMark;
    UINT8    piggybackEvents;
    UINT8    toneEvents;
    UINT8    dtmfEvents;
    UINT8    casEvents;
} RtpProfileDataME;



// messageContents - Create RTP Profile Data Command
#pragma pack(1)
typedef struct
{
    UINT16   localPortMin;
    UINT16   localPortMax;
    UINT8    dscpMark;
    UINT8    piggybackEvents;
    UINT8    toneEvents;
    UINT8    dtmfEvents;
    UINT8    casEvents;
} CreateRtpProfileData_s;
#pragma pack(0)


MeClassDef_S rtpProfileData_MeClassDef;


#endif
