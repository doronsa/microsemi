/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VoIPMediaProfile.h                                        **/
/**                                                                          **/
/**  DESCRIPTION : This file has VoIP Media Profile definitions              **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVoIPMediaProfileh
#define __INCVoIPMediaProfileh


typedef enum
    {	
	FAXMODE_PASSTHRU=0, FAXMODE_T38=1
    } FAXMODE;


typedef enum
    {	
	SILSUPPR_OFF=0, SILSUPPR_ON=1
    } SILENCESUPPRESSION;

// Packet period selection interval
#define PKTTPERIODSELECTINTERVAL_MIN          10
#define PKTTPERIODSELECTINTERVAL_MAX          30


// VoIP Media Profile Attribute bits
#define BM_MSB_faxMode                       (0x80)
#define BM_MSB_voiceServiceProfileAalPointer (0x40) 
#define BM_MSB_codecSelection1               (0x20)
#define BM_MSB_packetPeriodSelection1        (0x10)
#define BM_MSB_silenceSuppression1           (0x08)
#define BM_MSB_codecSelection2               (0x04)
#define BM_MSB_packetPeriodSelection2        (0x02)
#define BM_MSB_silenceSuppression2           (0x01)
#define BM_LSB_codecSelection3               (0x80)
#define BM_LSB_packetPeriodSelection3        (0x40) 
#define BM_LSB_silenceSuppression3           (0x20)
#define BM_LSB_codecSelection4               (0x10)
#define BM_LSB_packetPeriodSelection4        (0x08) 
#define BM_LSB_silenceSuppression4           (0x04)
#define BM_LSB_oobDtmf                       (0x02)
#define BM_LSB_rtpProfilePointer             (0x01)


#define BM_MSB_GETATTRIBS_VOIPMEDIAPROFILE (BM_MSB_faxMode | BM_MSB_voiceServiceProfileAalPointer | BM_MSB_codecSelection1 | BM_MSB_packetPeriodSelection1 | BM_MSB_silenceSuppression1 | BM_MSB_codecSelection2 | BM_MSB_packetPeriodSelection2 | BM_MSB_silenceSuppression2)

#define BM_LSB_GETATTRIBS_VOIPMEDIAPROFILE (BM_LSB_codecSelection3 | BM_LSB_packetPeriodSelection3 | BM_LSB_silenceSuppression3 | BM_LSB_codecSelection4 | BM_LSB_packetPeriodSelection4 | BM_LSB_silenceSuppression4 | BM_LSB_oobDtmf | BM_LSB_rtpProfilePointer)


#define BM_MSB_SETATTRIBS_VOIPMEDIAPROFILE (BM_MSB_faxMode | BM_MSB_voiceServiceProfileAalPointer | BM_MSB_codecSelection1 | BM_MSB_packetPeriodSelection1 | BM_MSB_codecSelection2 | BM_MSB_packetPeriodSelection2)

#define BM_LSB_SETATTRIBS_VOIPMEDIAPROFILE (BM_LSB_codecSelection3 | BM_LSB_packetPeriodSelection3 | BM_LSB_codecSelection4 | BM_LSB_packetPeriodSelection4 | BM_LSB_oobDtmf)


#define BM_MSB_CREATEATTRIBS_VOIPMEDIAPROFILE (BM_MSB_faxMode | BM_MSB_voiceServiceProfileAalPointer | BM_MSB_codecSelection1 | BM_MSB_packetPeriodSelection1 | BM_MSB_silenceSuppression1 | BM_MSB_codecSelection2 | BM_MSB_packetPeriodSelection2 | BM_MSB_silenceSuppression2)

#define BM_LSB_CREATEATTRIBS_VOIPMEDIAPROFILE (BM_LSB_codecSelection3 | BM_LSB_packetPeriodSelection3 | BM_LSB_silenceSuppression3 | BM_LSB_codecSelection4 | BM_LSB_packetPeriodSelection4 | BM_LSB_silenceSuppression4 | BM_LSB_oobDtmf | BM_LSB_rtpProfilePointer)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    faxMode;
    UINT16   voiceServiceProfileAalPointer;
    UINT8    codecSelection1;
    UINT8    packetPeriodSelection1;
    UINT8    silenceSuppression1;
    UINT8    codecSelection2;
    UINT8    packetPeriodSelection2;
    UINT8    silenceSuppression2;
    UINT8    codecSelection3;
    UINT8    packetPeriodSelection3;
    UINT8    silenceSuppression3;
    UINT8    codecSelection4;
    UINT8    packetPeriodSelection4;
    UINT8    silenceSuppression4;
    UINT8    oobDtmf;
    UINT16   rtpProfilePointer;
} VoIPMediaProfileME;



// messageContents - Create SIP User Data Command
#pragma pack(1)
typedef struct
{
    UINT8    faxMode;
    UINT16   voiceServiceProfileAalPointer;
    UINT8    codecSelection1;
    UINT8    packetPeriodSelection1;
    UINT8    silenceSuppression1;
    UINT8    codecSelection2;
    UINT8    packetPeriodSelection2;
    UINT8    silenceSuppression2;
    UINT8    codecSelection3;
    UINT8    packetPeriodSelection3;
    UINT8    silenceSuppression3;
    UINT8    codecSelection4;
    UINT8    packetPeriodSelection4;
    UINT8    silenceSuppression4;
    UINT8    oobDtmf;
    UINT16   rtpProfilePointer;
} CreateVoIPMediaProfile_s;
#pragma pack(0)



MeClassDef_S voIPMediaProfile_MeClassDef;


#endif
