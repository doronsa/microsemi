/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : IpHostConfigData.h                                        **/
/**                                                                          **/
/**  DESCRIPTION : This file implements IP Host Config Data ME               **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCIpHostConfigDatah
#define __INCIpHostConfigDatah


// IP Host Config Data Attribute bits
#define BM_MSB_ipOptions                      (0x80)
#define BM_MSB_macAddress                     (0x40)
#define BM_MSB_ontIdentifier                  (0x20)
#define BM_MSB_ipAddress                      (0x10)
#define BM_MSB_mask                           (0x08) 
#define BM_MSB_gateway                        (0x04)
#define BM_MSB_primaryDns                     (0x02) 
#define BM_MSB_secondaryDns                   (0x01)
#define BM_LSB_currentAddress                 (0x80)
#define BM_LSB_currentMask                    (0x40)
#define BM_LSB_currentGateway                 (0x20)
#define BM_LSB_currentPrimaryDns              (0x10)
#define BM_LSB_currentSecondaryDns            (0x08)
#define BM_LSB_domainName                     (0x04)
#define BM_LSB_hostName                       (0x02)


#define BM_MSB_GETATTRIBS_IPHOSTCONFIGDATA BM_MSB_ipOptions | BM_MSB_macAddress | BM_MSB_ontIdentifier | BM_MSB_ipAddress | BM_MSB_mask | BM_MSB_gateway | BM_MSB_primaryDns | BM_MSB_secondaryDns

#define BM_LSB_GETATTRIBS_IPHOSTCONFIGDATA BM_LSB_currentAddress | BM_LSB_currentMask | BM_LSB_currentGateway | BM_LSB_currentPrimaryDns | BM_LSB_currentSecondaryDns | BM_LSB_domainName | BM_LSB_hostName

#define BM_MSB_SETATTRIBS_IPHOSTCONFIGDATA BM_MSB_ipOptions | BM_MSB_ontIdentifier | BM_MSB_ipAddress | BM_MSB_mask | BM_MSB_gateway | BM_MSB_primaryDns | BM_MSB_secondaryDns


// Used in MIB Upload snapshot 
#define IPHOSTCONFIGDATA_UPLOAD_CELL1_MSB_MASK (BM_MSB_ipOptions | BM_MSB_macAddress | BM_MSB_ipAddress | BM_MSB_mask | BM_MSB_gateway | BM_MSB_primaryDns)

#define IPHOSTCONFIGDATA_UPLOAD_CELL2_MSB_MASK (BM_MSB_secondaryDns)
#define IPHOSTCONFIGDATA_UPLOAD_CELL2_LSB_MASK (BM_LSB_currentAddress | BM_LSB_currentMask | BM_LSB_currentGateway | BM_LSB_currentPrimaryDns | BM_LSB_currentSecondaryDns)

#define IPHOSTCONFIGDATA_UPLOAD_CELL3_MSB_MASK (BM_MSB_ontIdentifier)

#define IPHOSTCONFIGDATA_UPLOAD_CELL4_LSB_MASK (BM_LSB_domainName)

#define IPHOSTCONFIGDATA_UPLOAD_CELL5_LSB_MASK (BM_LSB_hostName)



// OMCI Entity DB

#define BM_IPOPTIONS_DHCPENABLE 1

#define DOMHOSTONTID_SIZE       25

typedef struct
{
    MeInst_S meInst;
    UINT8    ipOptions;
    UINT8    macAddress[MACADDR_SIZE];
    UINT8    ontIdentifier[DOMHOSTONTID_SIZE];
    UINT8    ipAddress[IPADDR_SIZE];
    UINT8    mask[IPADDR_SIZE];
    UINT8    gateway[IPADDR_SIZE];
    UINT8    primaryDns[IPADDR_SIZE];
    UINT8    secondaryDns[IPADDR_SIZE];
    UINT8    currentAddress[IPADDR_SIZE];
    UINT8    currentMask[IPADDR_SIZE];
    UINT8    currentGateway[IPADDR_SIZE];
    UINT8    currentPrimaryDns[IPADDR_SIZE];
    UINT8    currentSecondaryDns[IPADDR_SIZE];
    UINT8    domainName[DOMHOSTONTID_SIZE];
    UINT8  hostName[DOMHOSTONTID_SIZE];
} IpHostConfigDataME;



OMCIPROCSTATUS createIpHostConfigDataME(UINT16 meInstance);

MeClassDef_S ipHostConfigData_MeClassDef;


#endif
