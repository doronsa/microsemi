/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : ThresholdData1.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : This file has Threshold Data 1 definitions                **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCThresholdData1h
#define __INCThresholdData1h



// Threshold Data Attribute bits
#define BM_MSB_threshold1Value1                      (0X80)
#define BM_MSB_threshold1Value2                      (0X40)
#define BM_MSB_threshold1Value3                      (0X20)
#define BM_MSB_threshold1Value4                      (0X10)
#define BM_MSB_threshold1Value5                      (0X08)
#define BM_MSB_threshold1Value6                      (0X04)
#define BM_MSB_threshold1Value7                      (0X02)


#define BM_MSB_GETATTRIBS_THRESHOLDDATA1 (BM_MSB_threshold1Value1 | BM_MSB_threshold1Value2 | BM_MSB_threshold1Value3 | BM_MSB_threshold1Value4 | BM_MSB_threshold1Value5 | BM_MSB_threshold1Value6 | BM_MSB_threshold1Value7)

#define BM_MSB_SETATTRIBS_THRESHOLDDATA1 (BM_MSB_threshold1Value1 | BM_MSB_threshold1Value2 | BM_MSB_threshold1Value3 | BM_MSB_threshold1Value4 | BM_MSB_threshold1Value5 | BM_MSB_threshold1Value6 | BM_MSB_threshold1Value7)

#define BM_MSB_CREATEATTRIBS_THRESHOLDDATA1 (BM_MSB_threshold1Value1 | BM_MSB_threshold1Value2 | BM_MSB_threshold1Value3 | BM_MSB_threshold1Value4 | BM_MSB_threshold1Value5 | BM_MSB_threshold1Value6 | BM_MSB_threshold1Value7)


#define TD1CELL1_MSB_MASK BM_MSB_threshold1Value1 | BM_MSB_threshold1Value2 | BM_MSB_threshold1Value3 | BM_MSB_threshold1Value4 | BM_MSB_threshold1Value5 | BM_MSB_threshold1Value6

#define TD1CELL2_MSB_MASK BM_MSB_threshold1Value7



// messageContents - Create Threshold Data Command
#pragma pack(1)
typedef struct
{
    UINT32 thresholdValue1;
    UINT32 thresholdValue2;
    UINT32 thresholdValue3;
    UINT32 thresholdValue4;
    UINT32 thresholdValue5;
    UINT32 thresholdValue6;
    UINT32 thresholdValue7;
} CreateThresholdData1Cmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT32   thresholdValue1;
    UINT32   thresholdValue2;
    UINT32   thresholdValue3;
    UINT32   thresholdValue4;
    UINT32   thresholdValue5;
    UINT32   thresholdValue6;
    UINT32   thresholdValue7;
} ThresholdData1ME;


MeClassDef_S thresholdData1_MeClassDef;
 

#endif
