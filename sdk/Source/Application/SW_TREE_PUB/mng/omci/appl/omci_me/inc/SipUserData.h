/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : SipUserDataData.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : This file has SIP User Data definitions                   **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCSipUserDatah
#define __INCSipUserDatah


#define DEFAULT_VOICEMAILSUBSCRIPTEXPTIME  3600
#define DEFAULT_RELEASETIMER               10
#define DEFAULT_ROHTIMER                   15



// SIP User Data Attribute bits
#define BM_MSB_sipAgentPointer              (0x80)
#define BM_MSB_userPartAor                  (0x40) 
#define BM_MSB_sipDisplayName               (0x20)
#define BM_MSB_usernamePassword             (0x10)
#define BM_MSB_voiceMailserverSipUri        (0x08)
#define BM_MSB_voiceMailSubscriptExpTime    (0x04)
#define BM_MSB_networkDialPlanPointer       (0x02)
#define BM_MSB_applicServicesProfilePointer (0x01)
#define BM_LSB_featureCodePointer           (0x80)
#define BM_LSB_pptpPointer                  (0x40) 
#define BM_LSB_releaseTimer                 (0x20)
#define BM_LSB_rohTimer                     (0x10)


#define BM_MSB_GETATTRIBS_SIPUSERDATA (BM_MSB_sipAgentPointer | BM_MSB_userPartAor | BM_MSB_sipDisplayName | BM_MSB_usernamePassword | BM_MSB_voiceMailserverSipUri | BM_MSB_voiceMailSubscriptExpTime | BM_MSB_networkDialPlanPointer | BM_MSB_applicServicesProfilePointer)
#define BM_LSB_GETATTRIBS_SIPUSERDATA (BM_LSB_featureCodePointer | BM_LSB_pptpPointer | BM_LSB_releaseTimer | BM_LSB_rohTimer)

#define BM_MSB_SETATTRIBS_SIPUSERDATA (BM_MSB_sipAgentPointer | BM_MSB_userPartAor | BM_MSB_sipDisplayName | BM_MSB_usernamePassword | BM_MSB_voiceMailserverSipUri | BM_MSB_voiceMailSubscriptExpTime | BM_MSB_networkDialPlanPointer | BM_MSB_applicServicesProfilePointer)
#define BM_LSB_SETATTRIBS_SIPUSERDATA (BM_LSB_featureCodePointer | BM_LSB_pptpPointer | BM_LSB_releaseTimer | BM_LSB_rohTimer)

#define BM_MSB_CREATEATTRIBS_SIPUSERDATA (BM_MSB_sipAgentPointer | BM_MSB_userPartAor | BM_MSB_usernamePassword | BM_MSB_voiceMailserverSipUri | BM_MSB_voiceMailSubscriptExpTime | BM_MSB_networkDialPlanPointer | BM_MSB_applicServicesProfilePointer)
#define BM_LSB_CREATEATTRIBS_SIPUSERDATA (BM_LSB_featureCodePointer | BM_LSB_pptpPointer)

// Used in MIB Upload snapshot 
#define SIPUSERDATA_UPLOAD_CELL1_MSB_MASK (BM_MSB_sipAgentPointer | BM_MSB_userPartAor | BM_MSB_usernamePassword | BM_MSB_voiceMailserverSipUri | BM_MSB_voiceMailSubscriptExpTime | BM_MSB_networkDialPlanPointer | BM_MSB_applicServicesProfilePointer)
#define SIPUSERDATA_UPLOAD_CELL1_LSB_MASK (BM_LSB_featureCodePointer | BM_LSB_pptpPointer | BM_LSB_releaseTimer | BM_LSB_rohTimer)

#define SIPUSERDATA_UPLOAD_CELL2_MSB_MASK (BM_MSB_sipDisplayName)



// OMCI Entity DB

#define SIPDISPLAYNAME_SIZE                 25
typedef struct
{
    MeInst_S meInst;
    UINT16   sipAgentPointer;
    UINT16   userPartAor;
    UINT8    sipDisplayName[SIPDISPLAYNAME_SIZE];
    UINT16   usernamePassword;
    UINT16   voiceMailserverSipUri;
    UINT32   voiceMailSubscriptExpTime;
    UINT16   networkDialPlanPointer;
    UINT16   applicServicesProfilePointer;
    UINT16   featureCodePointer;
    UINT16   pptpPointer;
    UINT8    releaseTimer;
    UINT8    rohTimer;
} SipUserDataME;



// messageContents - Create SIP User Data Command
#pragma pack(1)
typedef struct
{
    UINT16   sipAgentPointer;
    UINT16   userPartAor;
    UINT16   usernamePassword;
    UINT16   voiceMailserverSipUri;
    UINT32   voiceMailSubscriptExpTime;
    UINT16   networkDialPlanPointer;
    UINT16   applicServicesProfilePointer;
    UINT16   featureCodePointer;
    UINT16   pptpPointer;
} CreateSipUserData_s;
#pragma pack(0)



MeClassDef_S sipUserData_MeClassDef;


#endif
