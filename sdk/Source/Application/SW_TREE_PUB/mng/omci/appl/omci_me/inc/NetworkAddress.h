/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : NetworkAddress.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Network Address ME                   **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCNetworkAddressh
#define __INCNetworkAddressh


#define MIN_NETWORKADDRESS_MEID   0x8000
#define MAX_NETWORKADDRESS_MEID   0xFFFE



// NetworkAddress Attribute bits
#define BM_MSB_securityPointer    (0x80)
#define BM_MSB_addressPointer     (0x40) 


#define BM_MSB_GETATTRIBS_NETWORKADDRESS (BM_MSB_securityPointer | BM_MSB_addressPointer)

#define BM_MSB_SETATTRIBS_NETWORKADDRESS (BM_MSB_securityPointer | BM_MSB_addressPointer)

#define BM_MSB_CREATEATTRIBS_NETWORKADDRESS (BM_MSB_securityPointer | BM_MSB_addressPointer)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT16   securityPointer;
    UINT16   addressPointer;
} NetworkAddressME;


// messageContents - Create Network Address Command
#pragma pack(1)
typedef struct
{
    UINT16  securityPointer;
    UINT16  addressPointer;
} CreateNetworkAddress_s;
#pragma pack(0)


MeClassDef_S networkAddress_MeClassDef;


#endif
