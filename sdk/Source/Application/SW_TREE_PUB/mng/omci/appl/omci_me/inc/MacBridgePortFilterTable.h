/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : MacBridgePortFilterData.h                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Mac Bridge Port Filter Data ME       **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCMacBridgePortFilterTableh
#define __INCMacBridgePortFilterTableh



// MAC Bridge Port Filter Table Attribute bits
#define BM_MSB_macFilterTable         (0x80)

#define BM_MSB_GETATTRIBS_MACBRIDGEPORTFILTERTABLE BM_MSB_macFilterTable

#define BM_MSB_SETATTRIBS_MACBRIDGEPORTFILTERTABLE BM_MSB_macFilterTable


// Filter byte bit definitions
#define BM_FILTER1FORWARD0         0x1
#define BM_ADD1REMOVE0             0x80



#pragma pack(1)
typedef struct
{
    UINT8  tableIndex;
    UINT8  filterByte;
    UINT8  macAddress[MACADDR_SIZE];
} MacFilterTableEntry_S;
#pragma pack(0)

#define ISMACFILTENTRYDEFINED(p)   ( ( ( ((p)->filterByte) & BM_ADD1REMOVE0 ) != 0 ) ? true : false )

#define NUM_MACFILTERTABLEENTRIES          16
// OMCI Entity DB
typedef struct
{
    MeInst_S              meInst;
    MacFilterTableEntry_S macFilterTable[NUM_MACFILTERTABLEENTRIES];
    UINT32                macFilterTable_size;
    struct
    {
        UINT32                currentTicks;
	MacFilterTableEntry_S macFilterTable[NUM_MACFILTERTABLEENTRIES];
    } getNextBuf;
} MacBridgePortFilterTableME;


OMCIPROCSTATUS createMacBridgePortFilterTableME(UINT16 meInstance);

MeClassDef_S macBridgePortFilterTable_MeClassDef; 


#endif
