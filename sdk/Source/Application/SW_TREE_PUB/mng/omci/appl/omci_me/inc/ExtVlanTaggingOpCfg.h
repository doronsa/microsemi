/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : ExtVlanTaggingOpCfg.h                                     **/
/**                                                                          **/
/**  DESCRIPTION : This file contains Ext. VLAN Tagging Operation ME         **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *     1Apr07zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/


#ifndef __INCExtVlanTaggingOpCfgh
#define __INCExtVlanTaggingOpCfgh

#include "VlanHelper.h"
#include "OmciDatapathAdapter.h"

typedef enum
{
    ASSOCTYPE_MBPCD=0,    ASSOCTYPE_MAPPER8021P=1, ASSOCTYPE_ETHERUNI=2,     ASSOCTYPE_IPHOSTCONFIGDATA=3, 
    ASSOCTYPE_XDSLUNI=4,  ASSOCTYPE_GEMIWTP=5,     ASSOCTYPE_MCASTGEMIWTP=6, ASSOCTYPE_MOCAUNI=7, 
    ASSOCTYPE_80211UNI=8, ASSOCTYPE_ETHERFLOWTP=9, ASSOCTYPE_VEIP=10 
} ASSOCIATIONTYPE;

typedef enum
{
    EVT_DSMODE_INVERSE=0, EVT_DSMODE_PASS=1, 
    EVT_DSMODE_TCIPASS=2, EVT_DSMODE_VIDPASS=3, EVT_DSMODE_PRIOPASS=4, 
    EVT_DSMODE_TCIDROP=5, EVT_DSMODE_VIDDROP=6, EVT_DSMODE_PRIODROP=7,
    EVT_DSMODE_DROP=8
} EVT_DSMODE;


// Extended VLAN Tagging Operation Configuration Attribute bits
#define BM_MSB_associationType                      (0x80)
#define BM_MSB_recvdFramevlanTaggingOpTableMaxSize  (0x40)
#define BM_MSB_inputTpid                            (0x20) 
#define BM_MSB_outputTpid                           (0x10)
#define BM_MSB_downstreamMode                       (0x08) 
#define BM_MSB_recvdFramevlanTaggingOpTable         (0x04)
#define BM_MSB_extVlanTagAssociatedMePointer        (0x02)
#define BM_MSB_dscpToPbitMapping                    (0x01)



#define BM_MSB_GETATTRIBS_EXTVLANTAGGINGOPCFG (BM_MSB_associationType | BM_MSB_recvdFramevlanTaggingOpTableMaxSize | BM_MSB_inputTpid | BM_MSB_outputTpid | BM_MSB_downstreamMode | BM_MSB_recvdFramevlanTaggingOpTable | BM_MSB_extVlanTagAssociatedMePointer | BM_MSB_dscpToPbitMapping)

#define BM_MSB_SETATTRIBS_EXTVLANTAGGINGOPCFG (BM_MSB_associationType | BM_MSB_inputTpid | BM_MSB_outputTpid | BM_MSB_downstreamMode | BM_MSB_recvdFramevlanTaggingOpTable | BM_MSB_extVlanTagAssociatedMePointer | BM_MSB_dscpToPbitMapping)

#define BM_MSB_CREATEATTRIBS_EXTVLANTAGGINGOPCFG (BM_MSB_associationType | BM_MSB_extVlanTagAssociatedMePointer)

#define EXTVLANTAGGINGOPCFG_UPLOAD_CELL1_MSB_MASK (BM_MSB_associationType | BM_MSB_recvdFramevlanTaggingOpTableMaxSize | BM_MSB_inputTpid | BM_MSB_outputTpid | BM_MSB_downstreamMode | BM_MSB_recvdFramevlanTaggingOpTable | BM_MSB_extVlanTagAssociatedMePointer)
#define EXTVLANTAGGINGOPCFG_UPLOAD_CELL2_MSB_MASK (BM_MSB_dscpToPbitMapping)

// OMCI Entity DB

#define RCVDFRVLANTAGOPTABLE_MAXSIZE         30
#define FIRST_NONDEFAULT_RCVDFRAME_INDX      3
#define NUM_DEFAULTRCVDFRTABLE_ENTRIES       3


#pragma pack(1)
#define RCVDFRVLANTAGOP_ENTRY_SIZE           (16)
typedef struct
{
    UINT8            byteAra[RCVDFRVLANTAGOP_ENTRY_SIZE];
    int              opKey;
    VlanTaggingOp_S  vlanTaggingOp;
} RecvdFrameVlanTaggingOpEntry_S;
#pragma pack(0)

#define VLAN_TPID_8100                     (0x8100)

//Insert macros
//#define Insert_Filter_Outer_Priority(ptr,val)       ((((UINT8 *)ptr)[0]) = (val<<4))
//#define Insert_Filter_Inner_Priority(ptr, val)      ((((UINT8 *)ptr)[4]) = (val<<4))
//#define Insert_Treatment_Tags_To_Remove(ptr,val)    ((((UINT8 *)ptr)[8]) = (val<<6))
//#define Insert_Treatment_Outer_Priority(ptr,val)    ((((UINT8 *)ptr)[9]) = val)
//#define Insert_Treatment_Inner_Priority(ptr,val)    ((((UINT8 *)ptr)[13]) = val)

// Insert macros
#define Insert_Filter_Outer_Priority(ptr,x)         ((((UINT8 *)ptr)[0]) |= (((x) & 0xF) << 4))
#define Insert_Filter_Outer_Vid(ptr,x,y)            ((((UINT8 *)ptr)[0]) |= (((x) >> 9) & 0xF)); (((UINT8 *)ptr)[1]) |= (((x) >> 1) & 0xFF); (((UINT8 *)ptr)[2]) |= (((x) & 0x1) << 7) | (((y) & 0x7) << 4) 

#define Insert_Filter_Inner_Priority(ptr,x)         (((UINT8 *)ptr)[4]) |= (((x) & 0xF) << 4)
#define Insert_Filter_Inner_Vid(ptr,x,y)            (((UINT8 *)ptr)[4]) |= (((x) >> 9) & 0xF); (((UINT8 *)ptr)[5]) |= (((x) >> 1) & 0xFF); (((UINT8 *)ptr)[6]) |= (((x) & 0x1) << 7) | (((y) & 0x7) << 4) 
#define Insert_Filter_Ether_Type(ptr,x)             (((UINT8 *)ptr)[7]) |= ((x) & 0xF)

#define Insert_Treatment_Tags_To_Remove(ptr,x)      (((UINT8 *)ptr)[8]) |= (((x) & 0x3) << 6)
#define Insert_Treatment_Outer_Priority(ptr,x)      (((UINT8 *)ptr)[9]) |= ((x) & 0xF)
#define Insert_Treatment_Outer_Vid(ptr,x)           (((UINT8 *)ptr)[10]) |= (((x) >> 5) &0xFF); (((UINT8 *)ptr)[11]) |= (((x) & 0x1F) << 3)
#define Insert_Treatment_Outer_Tpid_De(ptr,x)       (((UINT8 *)ptr)[11]) |= ((x) & 0x7)

#define Insert_Treatment_Inner_Priority(ptr,x)      (((UINT8 *)ptr)[13]) |= ((x) & 0xF)
#define Insert_Treatment_Inner_Vid(ptr,x)           (((UINT8 *)ptr)[14]) |= (((x) >> 5) &0xFF); (((UINT8 *)ptr)[15]) |= (((x) & 0x1F) << 3)
#define Insert_Treatment_Inner_Tpid_De(ptr,x)       (((UINT8 *)ptr)[15]) |= ((x) & 0x7)



#define DSCPTOPBITS_ARASIZE            (24)
typedef struct
{
    MeInst_S                       meInst;
    UINT8                          associationType;
    UINT16                         recvdFramevlanTaggingOpTableMaxSize;
    UINT16                         inputTpid;
    UINT16                         outputTpid;
    UINT8                          downstreamMode;
    RecvdFrameVlanTaggingOpEntry_S recvdFramevlanTaggingOpTable[RCVDFRVLANTAGOPTABLE_MAXSIZE];
    bool                           entryInUse[RCVDFRVLANTAGOPTABLE_MAXSIZE];
    UINT32                         recvdFramevlanTaggingOpTableSActualSize;
    UINT16                         associatedMePointer;
    UINT8                          dscpToPbitMapping[DSCPTOPBITS_ARASIZE];
    struct
    {
        UINT32  currentTicks;
        RecvdFrameVlanTaggingOpEntry_S recvdFramevlanTaggingOpTable[RCVDFRVLANTAGOPTABLE_MAXSIZE];
    } getNextBuf;
} ExtVlanTaggingOpCfgME;



// messageContents - Create Extended VLAN Tagging Operation Configuration Command
#pragma pack(1)
typedef struct
{
    UINT8   associationType;
    UINT16  associatedMePointer;
} CreateExtVlanTaggingOpCfg_s;
#pragma pack(0)



extern MeClassDef_S     extVlanTaggingOpCfg_MeClassDef;

RecvdFrameVlanTaggingOpEntry_S *findFirstNonDefaultRcvdFrameEntry(ExtVlanTaggingOpCfgME *pxvocd, int *currIndx);
RecvdFrameVlanTaggingOpEntry_S *findNextNonDefaultRcvdFrameEntry (ExtVlanTaggingOpCfgME *pxvocd, int currIndx, int *nextIndx);



#endif
