/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : MulticastGemInterworkingTp.h                              **/
/**                                                                          **/
/**  DESCRIPTION : This file contains Mcast GEM Interworking TP ME           **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *     9Jul06     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCMulticastGemInterworkingTph
#define __INCMulticastGemInterworkingTph


// Multicast GEM InterWorking TP Attribute bits
#define BM_MSB_mcast_gemPortNetworkCtpConnectivityPointer (0x80)
#define BM_MSB_mcast_interworkingOption                   (0x40)
#define BM_MSB_mcast_serviceProfilePointer                (0x20) 
#define BM_MSB_mcast_notUsed1                             (0x10)
#define BM_MSB_mcast_pptpCounter                          (0x08) 
#define BM_MSB_mcast_operationalState                     (0x04)  
#define BM_MSB_mcast_galProfilePointer                    (0x02)
#define BM_MSB_mcast_notUsed2                             (0x01)

#define BM_LSB_multicastAddressTable                      (0x80)


#define BM_MSB_GETATTRIBS_MCASTGEMINTERWORKINGTP (BM_MSB_mcast_gemPortNetworkCtpConnectivityPointer | BM_MSB_mcast_interworkingOption | BM_MSB_mcast_serviceProfilePointer | BM_MSB_mcast_notUsed1 | BM_MSB_mcast_pptpCounter | BM_MSB_mcast_operationalState | BM_MSB_mcast_galProfilePointer | BM_MSB_mcast_notUsed2)
#define BM_LSB_GETATTRIBS_MCASTGEMINTERWORKINGTP (BM_LSB_multicastAddressTable)

#define BM_MSB_SETATTRIBS_MCASTGEMINTERWORKINGTP (BM_MSB_mcast_gemPortNetworkCtpConnectivityPointer)
#define BM_LSB_SETATTRIBS_MCASTGEMINTERWORKINGTP (BM_LSB_multicastAddressTable)

#define BM_MSB_CREATEATTRIBS_MCASTGEMINTERWORKINGTP (BM_MSB_mcast_gemPortNetworkCtpConnectivityPointer | BM_MSB_mcast_interworkingOption | BM_MSB_mcast_serviceProfilePointer)


// OMCI Entity DB

#define MCASTGEMIWTP_ALARMARA_SIZE        1
#define NUM_MCASTADDRESSTABLE_ENTRIES     4

// The McastAddressEntry_S structue is the standard G.984.4 layout
#pragma pack(1)
typedef struct
{
    UINT16 gemPortIndex;
    UINT16 spare;
    UINT8  startIpMcastAddress[IPADDR_SIZE];
    UINT8  stopIpMcastAddress[IPADDR_SIZE];
} McastAddressEntry_S;
#pragma pack(0)



// For ZTE purposes (no IGMP snooping) and addresses set by the OLT
// we use the following size and structure 
#define NUM_OLTSETMCAST_ENTRIES           255

// Used for implementing Get-Next - will be created static
typedef struct
{
    UINT32              currentTime;
    UINT16              meInstance;
    McastAddressEntry_S multicastAddressTable[NUM_OLTSETMCAST_ENTRIES];
} McastMeGetNextBuf_S;


typedef struct
{
    UINT8  startIpMcastAddress[IPADDR_SIZE];
} OltSetMcastEntry_S;



typedef struct
{
    MeInst_S meInst;
    UINT8    alarmsBitmap[OMCI_ALARM_BIT_MAP_LEN];
    UINT16   gemPortNetworkCtpConnectivityPointer;
    UINT8    interworkingOption;
    UINT16   serviceProfilePointer;
    UINT16   notUsed1;
    UINT8    pptpCounter;
    UINT8    operationalState;
    UINT16   galProfilePointer;
    UINT8    notUsed2;
    McastAddressEntry_S multicastAddressTable[NUM_MCASTADDRESSTABLE_ENTRIES];
    OltSetMcastEntry_S  oltSetMcastEntry[NUM_OLTSETMCAST_ENTRIES];
    UINT32 mcastAddressTableSize;
} MulticastGemInterworkingTpME;



// messageContents - Create Multicast GEM InterWorking TP Command
#pragma pack(1)
typedef struct
{
    UINT16 gemPortNetworkCtpConnectivityPointer;
    UINT8  interworkingOption;
    UINT16 serviceProfilePointer;
    UINT16 notUsed1;
    UINT16 galProfilePointer;
    UINT8  notUsed2;
} CreateMulticastGemInterworkingTp_s;
#pragma pack(0)




MulticastGemInterworkingTpME *getMcastGemIwTpByGalProfilePointer(UINT16 aalProfilePointer, UINT8 interworkingOption);
MulticastGemInterworkingTpME *getMcastGemIwTpByServiceProfilePointer(UINT16 serviceProfilePointer, UINT8 interworkingOption);

extern MeClassDef_S     multicastGemInterworkingTp_MeClassDef;


#endif
