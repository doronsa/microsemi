/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VlanHelper.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file contains macros, defs for Ext. VLAN Tagging Ops **/
/**                VLAN Filter forward operation                             **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    3Mar08     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVlanHelperh
#define __INCVlanHelperh


//********************************************************************************
//********************************************************************************
//******************           VLAN FILTER              **************************
//********************************************************************************
//********************************************************************************

typedef enum
{
    FWDOP_0_BR_BR=0,                  
    FWDOP_1_DROPTAGGED_BR=1,
    FWDOP_2_BR_DROPUNTAGGED=2,       
    FWDOP_3_TAGVID_BR=3,        
    FWDOP_4_TAGVID_DROPUNTAGGED=4,        
    FWDOP_5_DROPTAGGEDVID_BR=5,        
    FWDOP_6_DROPTAGGEDVID_DROPUNTAGGED=6,        
    FWDOP_7_TAGPRI_BR=7,        
    FWDOP_8_TAGPRI_DROPUNTAGGED=8,        
    FWDOP_9_DROPTAGGEDPRI_BR=9,        
    FWDOP_10_DROPTAGGEDPRI_DROPUNTAGGED=10,      
    FWDOP_11_TAGTCI_BR=11,      
    FWDOP_12_TAGTCI_DROPUNTAGGED=12,      
    FWDOP_13_DROPTAGGEDTCI_BR=13,      
    FWDOP_14_DROPTAGGEDTCI_DROPUNTAGGED=14,
    FWDOP_15_TAGVID_BR=15, 
    FWDOP_16_TAGVID_DROPUNTAGGED=16, 
    FWDOP_17_TAGPRI_BR=17,  
    FWDOP_18_TAGPRI_DROPUNTAGGED=18,
    FWDOP_19_TAGTCI_BR=19, 
    FWDOP_20_TAGTCI_DROPUNTAGGED=20, 
    FWDOP_21_TAG_DROPUNTAGGED=21,
    FWDOP_22_TAGNOFLOODVID_BR=22, 
    FWDOP_23_TAGNOFLOODVID_DROPUNTAGGED=23, 
    FWDOP_24_TAGNOFLOODPRI_BR=24,  
    FWDOP_25_TAGNOFLOODPRI_DROPUNTAGGED=25,
    FWDOP_26_TAGNOFLOODTCI_BR=26, 
    FWDOP_27_TAGNOFLOODTCI_DROPUNTAGGED=27,
    FWDOP_28_TAGBIDIFILTVID_BR=28, 
    FWDOP_29_TAGBIDIFILTVID_DROPUNTAGGED=29, 
    FWDOP_30_TAGBIDIFILTPRI_BR=30,  
    FWDOP_31_TAGBIDIFILTPRI_DROPUNTAGGED=31,
    FWDOP_32_TAGBIDIFILTTCI_BR=32, 
    FWDOP_33_TAGBIDIFILTTCI_DROPUNTAGGED=33,
    MAX_FORWARDOPERATION=33
} FORWARDOPERATION;



extern bool isUntaggedFrameForwarded    (FORWARDOPERATION forwardOperation);
extern bool isPbitPositiveFiltered      (FORWARDOPERATION forwardOperation);
extern L2DATAGGEDFILTERMODE mapVlanFilterTaggedFrames_omci_to_l2da(FORWARDOPERATION forwardOperation);



//********************************************************************************
//********************************************************************************
//******************           EXT. VLAN TAGGING        **************************
//********************************************************************************
//********************************************************************************

#define MAX_VID                 (4095)


typedef enum
{
    FILTERETHERTYPE_NOFILTER=0,  FILTERETHERTYPE_IPOE=1,  
    FILTERETHERTYPE_PPPOE=2,     FILTERETHERTYPE_ARP=3,
    FILTERETHERTYPE_IPV6_IPOE=4
} FILTERETHERTYPE;


typedef enum
{
    FILTOUTERPRI_NOFILTER=8,  FILTOUTERPRI_DEFAULT_2TAGRULE=14,  FILTOUTERPRI_NOT_2TAGRULE=15
} FILTEROUTERPRIORITY;


typedef enum
{
    FILTEROUTERVID_NOFILTER=4096
} FILTEROUTERVID;


typedef enum
{
    FILTOUTERTPID_NOFILTER=0,  FILTOUTERTPID_8100=4,  FILTOUTERTPID_INPUTID_DEDC=5,  FILTOUTERTPID_INPUTID_DE0=6, FILTOUTERTPID_INPUTID_DE1=7
} FILTEROUTERTPID;


typedef enum
{
    FILTINNERPRI_NOFILTER=8,  FILTINNERPRI_DEFAULT_1TAGRULE=14,  FILTINNERPRI_NOTAGRULE=15
} FILTERINNERPRIORITY;


typedef enum
{
    FILTERINNERVID_NOFILTER=4096
} FILTERINNERVID;


typedef enum
{
    FILTINNERTPID_NOFILTER=0,  FILTINNERTPID_8100=4,  FILTINNERTPID_INPUTID_DEDC=5,  FILTINNERTPID_INPUTID_DE0=6, FILTINNERTPID_INPUTID_DE1=7
} FILTERINNERTPID;


typedef enum
{
    TREATTAGSTOREMOVE_DROP=3
} TREATMENTTAGSTOREMOVE;


typedef enum
{
    TREATOUTERPRI_COPYFROM_INNERPRIORITY=8,  TREATOUTERPRI_COPYFROM_OUTERPRIORITY=9,  TREATOUTERPRI_COPYFROM_DSCP=10,
    TREATOUTERPRI_NOADD=15
} TREATMENTOUTERPRIORITY;


typedef enum
{
    TREATINNERPRI_COPYFROM_INNERPRIORITY=8,  TREATINNERPRI_COPYFROM_OUTERPRIORITY=9,  TREATINNERPRI_COPYFROM_DSCP=10,
    TREATINNERPRI_NOADD=15
} TREATMENTINNERPRIORITY;


typedef enum
{
    TREATOUTERVID_COPYFROM_INNERVID=4096,  TREATOUTERVID_COPYFROM_OUTERVID=4097
} TREATMENTOUTERVID;


typedef enum
{
    TREATINNERVID_COPYFROM_INNERVID=4096,  TREATINNERVID_COPYFROM_OUTERVID=4097
} TREATMENTINNERVID;


typedef enum
{
    TREATOUTERTPID_COPYFROM_INNERTPID=0,          TREATOUTERTPID_COPYFROM_OUTERTPID=1,  TREATOUTERTPID_COPYFROM_OUTPUTTPID_INNERDE=2, 
    TREATOUTERTPID_COPYFROM_OUTPUTTPID_OUTERDE=3, TREATOUTERTPID_8100=4,                TREATOUTERTPID_RESERVED=5,
    TREATOUTERTPID_COPYFROM_OUTPUTTPID_DE0=6,     TREATOUTERTPID_COPYFROM_OUTPUTTPID_DE1=7
} TREATMENTOUTERTPID;


typedef enum
{
    TREATINNERTPID_COPYFROM_INNERTPID=0,          TREATINNERTPID_COPYFROM_OUTERTPID=1,  TREATINNERTPID_COPYFROM_OUTPUTTPID_INNERDE=2, 
    TREATINNERTPID_COPYFROM_OUTPUTTPID_OUTERDE=3, TREATINNERTPID_8100=4,                TREATINNERTPID_RESERVED=5,
    TREATINNERTPID_COPYFROM_OUTPUTTPID_DE0=6,     TREATINNERTPID_COPYFROM_OUTPUTTPID_DE1=7
} TREATMENTINNERTPID;


// Extract macros
#define Filter_Outer_Priority(ptr)               (((((UINT8 *)ptr)[0])  >> 4) & 0xF)
#define Filter_Outer_Vid(ptr)                    ((((((UINT8 *)ptr)[0]) & 0x0F) << 9) | ((((UINT8 *)ptr)[1])  << 1) | ((((((UINT8 *)ptr)[2])  >> 7)) & 0x1) )
#define Filter_Outer_Tpid_De(ptr)                (((((UINT8 *)ptr)[2])  >> 4) & 0x7)

#define Filter_Inner_Priority(ptr)               (((((UINT8 *)ptr)[4])  >> 4) & 0xF)
#define Filter_Inner_Vid(ptr)                    ((((((UINT8 *)ptr)[4]) & 0x0F) << 9) | ((((UINT8 *)ptr)[5])  << 1) | ((((((UINT8 *)ptr)[6])  >> 7)) & 0x1) )
#define Filter_Inner_Tpid_De(ptr)                (((((UINT8 *)ptr)[6])  >> 4) & 0x7)
#define Filter_Ether_Type(ptr)                   ((((UINT8 *)ptr)[7])   & 0xF)

#define Treatment_Tags_To_Remove(ptr)            (((((UINT8 *)ptr)[8]) >> 6) & 0x3)
#define Treatment_Outer_Priority(ptr)            ((((UINT8 *)ptr)[9])  & 0xF)
#define Treatment_Outer_Vid(ptr)                 (((((UINT8 *)ptr)[10]) << 5) | (((((UINT8 *)ptr)[11]) >> 3) & 0x1F) )
#define Treatment_Outer_Tpid_De(ptr)             ((((UINT8 *)ptr)[11]) & 0x7)

#define Treatment_Inner_Priority(ptr)            ((((UINT8 *)ptr)[13])  & 0xF)
#define Treatment_Inner_Vid(ptr)                 (((((UINT8 *)ptr)[14]) << 5) | (((((UINT8 *)ptr)[15]) >> 3) & 0x1F) )
#define Treatment_Inner_Tpid_De(ptr)             ((((UINT8 *)ptr)[15])  & 0x7)


TAGOPCLASS classifyTagOperation(UINT8 *ptr, UINT32 *isDefault);


#endif

