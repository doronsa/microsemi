/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : MacBridgePortConfigurationData.h                          **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Mac Bridge Port Config Data ME       **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCMacBridgePortConfigurationDatah
#define __INCMacBridgePortConfigurationDatah


// MAC Bridge Port Configuration Data Attribute bits
#define BM_MSB_bridgeIdPointer                      (0x80)
#define BM_MSB_portNum                              (0x40)
#define BM_MSB_tpType                               (0x20)
#define BM_MSB_tpPointer                            (0x10)
#define BM_MSB_portPriority                         (0x08) 
#define BM_MSB_portPathCost                         (0x04)
#define BM_MSB_portSpanningTreeInd                  (0x02) 
#define BM_MSB_encapsulationMethod                  (0x01)
#define BM_LSB_lanFcsInd                            (0x80)
#define BM_LSB_portMacAddress                       (0x40)
#define BM_LSB_outboundTdPointer                    (0x20)
#define BM_LSB_inboundTdPointer                     (0x10)
#define BM_LSB_mbpcdMacLearningDepth                (0x08)
                            

#define BM_MSB_GETATTRIBS_MACBRIDGEPORTCONFIGURATION (BM_MSB_bridgeIdPointer | BM_MSB_portNum | BM_MSB_tpType | BM_MSB_tpPointer | BM_MSB_portPriority | BM_MSB_portPathCost | BM_MSB_portSpanningTreeInd | BM_MSB_encapsulationMethod)
#define BM_LSB_GETATTRIBS_MACBRIDGEPORTCONFIGURATION (BM_LSB_lanFcsInd | BM_LSB_portMacAddress | BM_LSB_outboundTdPointer | BM_LSB_inboundTdPointer | BM_LSB_mbpcdMacLearningDepth)

#define BM_MSB_SETATTRIBS_MACBRIDGEPORTCONFIGURATION (BM_MSB_bridgeIdPointer | BM_MSB_portNum | BM_MSB_tpType | BM_MSB_tpPointer | BM_MSB_portPriority | BM_MSB_portPathCost | BM_MSB_portSpanningTreeInd | BM_MSB_encapsulationMethod)
#define BM_LSB_SETATTRIBS_MACBRIDGEPORTCONFIGURATION (BM_LSB_lanFcsInd | BM_LSB_outboundTdPointer | BM_LSB_inboundTdPointer | BM_LSB_mbpcdMacLearningDepth)

#define BM_MSB_CREATEATTRIBS_MACBRIDGEPORTCONFIGURATION (BM_MSB_bridgeIdPointer | BM_MSB_portNum | BM_MSB_tpType | BM_MSB_tpPointer | BM_MSB_portPriority | BM_MSB_portPathCost | BM_MSB_portSpanningTreeInd | BM_MSB_encapsulationMethod)
#define BM_LSB_CREATEATTRIBS_MACBRIDGEPORTCONFIGURATION (BM_LSB_lanFcsInd | BM_LSB_mbpcdMacLearningDepth)



// OMCI Entity DB
typedef struct MacBridgePortConfigurationDataME_tag
{
    MeInst_S meInst;
    UINT16   bridgeIdPointer;
    UINT8    portNum;
    UINT8    tpType;
    UINT16   tpPointer;
    UINT16   portPriority;
    UINT16   portPathCost;
    UINT8    portSpanningTreeInd;
    UINT8    encapsulationMethod;
    UINT8    lanFcsInd;
    UINT8    portMacAddress[MACADDR_SIZE];
    UINT16   outboundTdPointer;
    UINT16   inboundTdPointer;
    UINT8    macLearningDepth;
} MacBridgePortConfigurationDataME;




// messageContents - Create MAC Bridge Port Configuration Data Command
#pragma pack(1)
typedef struct
{
    UINT16  bridgeIdPointer;
    UINT8   portNum;
    UINT8   tpType;
    UINT16  tpPointer;
    UINT16  portPriority;
    UINT16  portPathCost;
    UINT8   portSpanningTreeInd;
    UINT8   encapsulationMethod;
    UINT8   lanFcsInd;
    UINT8   macLearningDepth;
} CreateMacBridgePortConfiguration_s;
#pragma pack(0)




MeClassDef_S macBridgePortConfiguration_MeClassDef;



typedef struct
{
    bool   inUse;
    UINT16 mbspInstance;
    int    numAniMbpcds;
    int    numBidiAniMbpcds;
    int    numUniMbpcds;
    int    numIpHostUniMbpcds;
} BridgeSummary_S;


#define MAX_ASICBRIDGES               (16)
typedef struct
{
    int numBridges;
    BridgeSummary_S bridgeSummaryAra[MAX_ASICBRIDGES];
} AllBridgeSummary_S;


void displayAllBridgeSummary(char* name, FILE *fildes);
OMCIPROCSTATUS  prepareBridgeSummaryData    (AllBridgeSummary_S *p_AllBridgeSummary, char *buf);
BridgeSummary_S *findFirstUsedBridgeSummary (AllBridgeSummary_S *p_AllBridgeSummary);
BridgeSummary_S *findNextUsedBridgeSummary  (AllBridgeSummary_S *p_AllBridgeSummary, UINT16 prevBridgeIdPointer);
BridgeSummary_S *findUsedBridgeSummaryByMbsp(AllBridgeSummary_S *p_AllBridgeSummary, UINT16 bridgeIdPointer);

OMCIPROCSTATUS createServiceAddUniTps(MacBridgePortConfigurationDataME *paniMbpcd);
OMCIPROCSTATUS addAniSideMbpcd       (MacBridgePortConfigurationDataME *paniMbpcd);

OMCIPROCSTATUS dealWithServiceStopFromMBPC(MacBridgePortConfigurationDataME *pmb);

#endif
