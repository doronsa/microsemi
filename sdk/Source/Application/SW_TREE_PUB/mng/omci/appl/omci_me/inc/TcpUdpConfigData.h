/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : TcpUdpConfigData.h                                        **/
/**                                                                          **/
/**  DESCRIPTION : This file has TCP UDP Config Data definitions             **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCTcpUdpConfigDatah
#define __INCTcpUdpConfigDatah



// TCP/UDP Config Data Attribute bits
#define BM_MSB_portId                      (0x80)
#define BM_MSB_protocol                    (0x40) 
#define BM_MSB_tosDiffserv                 (0x20)
#define BM_MSB_ipHostPointer               (0x10)



#define BM_MSB_GETATTRIBS_TCPUDPCONFIGDATA (BM_MSB_portId | BM_MSB_protocol | BM_MSB_tosDiffserv | BM_MSB_ipHostPointer)

#define BM_MSB_SETATTRIBS_TCPUDPCONFIGDATA (BM_MSB_tosDiffserv)

#define BM_MSB_CREATEATTRIBS_TCPUDPCONFIGDATA (BM_MSB_portId | BM_MSB_protocol | BM_MSB_tosDiffserv | BM_MSB_ipHostPointer)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT16   portId;
    UINT8    protocol;
    UINT8    tosDiffserv;
    UINT16   ipHostPointer;
} TcpUdpConfigDataME;



// messageContents - Create TCP/UDP Config Data Command
#pragma pack(1)
typedef struct
{
    UINT16  portId;
    UINT8   protocol;
    UINT8   tosDiffserv;
    UINT16  ipHostPointer;
} CreateTcpUdpConfigData_s;
#pragma pack(0)


MeClassDef_S tcpUdpConfigData_MeClassDef;


#endif
