/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OntData.h                                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file contains ONT Data definitions and prototypes    **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCOntDatah
#define __INCOntDatah


// 60 seconds * 100 ticks/sec 
//#define UPLOAD_INTER_CMD_TICKS                      6000
#define UPLOAD_INTER_CMD_SECS                      (60)

// ONT Data Attribute bits
#define BM_MSB_mibDataSync                          (0X80)

#define BM_MSB_GETATTRIBS_ONTDATA BM_MSB_mibDataSync 

#define BM_MSB_SETATTRIBS_ONTDATA BM_MSB_mibDataSync 


// OMCI Entity DB
typedef struct OntDataME_tag
{
    MeInst_S meInst;
    UINT8    mibDataSync;
} OntDataME;



typedef struct
{
    UINT32 time;
    UINT32 numCells;
    int    snapshotPriority;
} MIBSnapshotInfo;


typedef struct
{
    UINT32 time;
    UINT32 numCells;
    int    snapshotPriority;
} AlarmSnapshotInfo;


extern MIBSnapshotInfo mibSnapshotInfo;
extern AlarmSnapshotInfo alarmSnapshotInfo;


void incrementSequenceNumber();

OMCIPROCSTATUS genericEntitySnapshotForMibUpload(OmciCell_S *pSnapCell,      int seqNum,  
                                                 MeInst_S   *pmeinst,        MECLASS mngEntity,
                                                 AttribData_S *pMsbAttribData, int numMsbAttribs,
                                                 AttribData_S *pLsbAttribData, int numLsbAttribs);

OMCIPROCSTATUS fillMibUploadNextData(OmciCell_S *pcell,      MeInst_S *pmeinst,          MECLASS meClassEnum,
                                     UINT8 msbGetAttribMask, AttribData_S *pMsbAttribData, int numMsbAttribs, 
                                     UINT8 lsbGetAttribMask, AttribData_S *pLsbAttribData, int numLsbAttribs);

OMCIPROCSTATUS fullAutonomousEntityCreation();

void initMibAndAlarmUploadControlInfo();


MeClassDef_S ontData_MeClassDef;

#endif
