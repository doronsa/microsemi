/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : CircuitPack.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file has Circuit Pack definitions                    **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCSubscrLineCardh
#define __INCSubscrLineCardh


// Circuit Pack Attribute bits
#define BM_MSB_type                                 (0x80)
#define BM_MSB_numberOfPorts                        (0x40)
#define BM_MSB_circuitPack_serialNumber             (0x20)
#define BM_MSB_circuitPack_version                  (0x10)
#define BM_MSB_circuitPack_vendorId                 (0x08)
#define BM_MSB_circuitPack_administrativeState      (0x04)
#define BM_MSB_circuitPack_operationalState         (0x02)
#define BM_MSB_bridgedOrIPInd                       (0x01)

#define BM_LSB_equipmentId                          (0x80)
#define BM_LSB_cardConfiguration		            (0x40)
#define BM_LSB_totalTcontBufferNumber               (0x20)
#define BM_LSB_totalPriorityQueueNumber		        (0x10)
#define BM_LSB_totalTrafficSchedulerNumber	        (0x08)
#define BM_LSB_powerShedOverride		            (0x04)



#define BM_MSB_GETATTRIBS_CIRCUITPACK       (BM_MSB_type | BM_MSB_numberOfPorts | BM_MSB_circuitPack_serialNumber | BM_MSB_circuitPack_version | BM_MSB_circuitPack_vendorId | BM_MSB_circuitPack_administrativeState | BM_MSB_circuitPack_operationalState | BM_MSB_bridgedOrIPInd)
#define BM_LSB_GETATTRIBS_CIRCUITPACK       (BM_LSB_equipmentId | BM_LSB_cardConfiguration | BM_LSB_totalTcontBufferNumber | BM_LSB_totalPriorityQueueNumber | BM_LSB_totalTrafficSchedulerNumber | BM_LSB_powerShedOverride)

#define BM_MSB_SETATTRIBS_CIRCUITPACK       (BM_MSB_circuitPack_administrativeState | BM_MSB_bridgedOrIPInd)
#define BM_LSB_SETATTRIBS_CIRCUITPACK       (BM_LSB_powerShedOverride)

// Used in MIB Upload snapshot 
#define CPCELL1_MSB_MASK (BM_MSB_type | BM_MSB_numberOfPorts | BM_MSB_circuitPack_serialNumber | BM_MSB_circuitPack_version)
#define CPCELL2_MSB_MASK (BM_MSB_circuitPack_vendorId | BM_MSB_circuitPack_administrativeState | BM_MSB_circuitPack_operationalState | BM_MSB_bridgedOrIPInd)
#define CPCELL3_LSB_MASK (BM_LSB_cardConfiguration | BM_LSB_totalTcontBufferNumber | BM_LSB_totalPriorityQueueNumber | BM_LSB_totalTrafficSchedulerNumber | BM_LSB_powerShedOverride)
#define CPCELL4_LSB_MASK (BM_LSB_equipmentId)


// OMCI Entity DB
#define POWERSHEDOVERRIDE_SIZE      4
#define EQUIPMENTID_SIZE            20
typedef struct
{
    MeInst_S meInst;
    UINT8    type;
    UINT8    numberOfPorts;
    UINT8    serialNumber[SERIALNUMBER_LEN];
    UINT8    version[VERSION_LEN];
    UINT8    vendorId[VENDORID_LEN];
    UINT8    administrativeState;
    UINT8    operationalState;
    UINT8    bridgedOrIPInd;
    UINT8    equipmentId[EQUIPMENTID_SIZE];
    UINT8    cardConfiguration;
    UINT8    totalTcontBufferNumber;
    UINT8    totalPriorityQueueNumber;
    UINT8    totalTrafficSchedulerNumber;
    UINT8    powerShedOverride[POWERSHEDOVERRIDE_SIZE];
} CircuitPackME;


typedef struct
{
    UINT8    type;
    UINT8    numberOfPorts;
    UINT8    serialNumber[SERIALNUMBER_LEN];
    UINT8    version[VERSION_LEN];
    UINT8    vendorId[VENDORID_LEN];
    UINT8    administrativeState;
    UINT8    operationalState;
    UINT8    bridgedOrIPInd;
    UINT8    totalTcontBufferNumber;
    UINT8    totalPriorityQueueNumber;
    UINT8    totalTrafficSchedulerNumber;
} CreateCircuitPack_s;


OMCIPROCSTATUS createCircuitPackME(UINT16 meInstance, CreateCircuitPack_s *pCreate);


MeClassDef_S circuitPack_MeClassDef;

#endif
