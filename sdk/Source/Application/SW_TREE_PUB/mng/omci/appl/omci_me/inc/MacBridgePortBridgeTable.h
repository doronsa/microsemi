/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : MacBridgePortBridgeTable.h                                **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Mac Bridge Port Bridge Table ME      **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCMacBridgePortBridgeTableh
#define __INCMacBridgePortBridgeTableh



// MAC Bridge Port Bridge Table Attribute bits
#define BM_MSB_bridgeTable         (0x80)



#define BM_MSB_GETATTRIBS_MACBRIDGEPORTBRIDGETABLE BM_MSB_bridgeTable


// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    bridgeTable[8];
} MacBridgePortBridgeTableME;


OMCIPROCSTATUS createMacBridgePortBridgeTableME(UINT16 meInstance);

MeClassDef_S macBridgePortBridgeTable_MeClassDef;


#endif
