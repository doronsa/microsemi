/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VoIPApplicServiceProfile.h                                **/
/**                                                                          **/
/**  DESCRIPTION : This file has VoIP Applic Service Profile definitions     **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVoIPApplicServiceProfileh
#define __INCVoIPApplicServiceProfileh



// VoIP Application Service Profile Attribute bits
#define BM_MSB_cidFeatures                      (0x80)
#define BM_MSB_callWaitingFeatures              (0x40) 
#define BM_MSB_callProgressOrTransferFeatures   (0x20)
#define BM_MSB_callPresentationFeatures         (0x10)
#define BM_MSB_directConnectFeature             (0x08)
#define BM_MSB_directConnectUriPointer          (0x04)
#define BM_MSB_bridgedLineAgentUriPointer       (0x02)
#define BM_MSB_conferenceFactoryUriPointer      (0x01)



#define BM_MSB_GETATTRIBS_VOIPAPPLICSERVICEPROFILE (BM_MSB_cidFeatures | BM_MSB_callWaitingFeatures | BM_MSB_callProgressOrTransferFeatures | BM_MSB_callPresentationFeatures | BM_MSB_directConnectFeature | BM_MSB_directConnectUriPointer | BM_MSB_bridgedLineAgentUriPointer | BM_MSB_conferenceFactoryUriPointer)

#define BM_MSB_SETATTRIBS_VOIPAPPLICSERVICEPROFILE (BM_MSB_cidFeatures | BM_MSB_callWaitingFeatures | BM_MSB_callProgressOrTransferFeatures | BM_MSB_callPresentationFeatures | BM_MSB_directConnectFeature | BM_MSB_directConnectUriPointer | BM_MSB_bridgedLineAgentUriPointer | BM_MSB_conferenceFactoryUriPointer)

#define BM_MSB_CREATEATTRIBS_VOIPAPPLICSERVICEPROFILE (BM_MSB_cidFeatures | BM_MSB_callWaitingFeatures | BM_MSB_callProgressOrTransferFeatures | BM_MSB_callPresentationFeatures | BM_MSB_directConnectFeature | BM_MSB_directConnectUriPointer | BM_MSB_bridgedLineAgentUriPointer | BM_MSB_conferenceFactoryUriPointer)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    cidFeatures;
    UINT8    callWaitingFeatures;
    UINT16   callProgressOrTransferFeatures;
    UINT16   callPresentationFeatures;
    UINT8    directConnectFeature;
    UINT16   directConnectUriPointer;
    UINT16   bridgedLineAgentUriPointer;
    UINT16   conferenceFactoryUriPointer;
} VoIPApplicServiceProfileME;



// messageContents - Create VoIP Application Service Profile Command
#pragma pack(1)
typedef struct
{
    UINT8    cidFeatures;
    UINT8    callWaitingFeatures;
    UINT16   callProgressOrTransferFeatures;
    UINT16   callPresentationFeatures;
    UINT8    directConnectFeature;
    UINT16   directConnectUriPointer;
    UINT16   bridgedLineAgentUriPointer;
    UINT16   conferenceFactoryUriPointer;
} CreateVoIPApplicServiceProfile_s;
#pragma pack(0)


MeClassDef_S voIPApplicServiceProfile_MeClassDef;


#endif
