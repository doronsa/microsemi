/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : MacBridgePortDesignationData.h                            **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Mac Bridge Port Designation Data ME  **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCMacBridgePortDesignationDatah
#define __INCMacBridgePortDesignationDatah



// MAC Bridge Service Profile Attribute bits
#define BM_MSB_designatedBridgeRootCostPort         (0x80)
#define BM_MSB_portState                            (0x40) 



#define BM_MSB_GETATTRIBS_MACBRIDGEPORTDESIGNATIONDATA BM_MSB_designatedBridgeRootCostPort | BM_MSB_portState


// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    designatedBridgeRootCostPort[24];
    UINT8    portState;
} MacBridgePortDesignationDataME;


OMCIPROCSTATUS createMacBridgePortDesignationDataME(UINT16 meInstance);


MeClassDef_S macBridgePortDesignationData_MeClassDef;


#endif
