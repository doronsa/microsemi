/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : PptpPotsUni.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file has PPTP POTS definitions and prototypes        **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCPptpPotsUnih
#define __INCPptpPotsUnih


#include "OsGlueLayer.h"

// Physical Path TP - POTS UNI Attribute bits
#define BM_MSB_pptp_potsUni_administrativeState      (0x80)
#define BM_MSB_interworkingVccPointer                (0x40) 
#define BM_MSB_pptp_potsUni_ARC                      (0x20) 
#define BM_MSB_pptp_potsUni_arcInterval              (0x10) 
#define BM_MSB_impedance                             (0x08) 
#define BM_MSB_transmissionPath                      (0x04) 
#define BM_MSB_rxGain                                (0x02) 
#define BM_MSB_txGain                                (0x01) 

#define BM_LSB_pptp_potsUni_operationalState         (0x80)
#define BM_LSB_hookState                             (0x40) 
#define BM_LSB_potsHoldoverTime                      (0x20)

#define BM_MSB_GETATTRIBS_PPTPPOTSUNI (BM_MSB_pptp_potsUni_administrativeState | BM_MSB_interworkingVccPointer | BM_MSB_pptp_potsUni_ARC | BM_MSB_pptp_potsUni_arcInterval | BM_MSB_impedance | BM_MSB_transmissionPath | BM_MSB_rxGain | BM_MSB_txGain)

#define BM_LSB_GETATTRIBS_PPTPPOTSUNI (BM_LSB_pptp_potsUni_operationalState | BM_LSB_hookState | BM_LSB_potsHoldoverTime)


#define BM_MSB_SETATTRIBS_PPTPPOTSUNI (BM_MSB_pptp_potsUni_administrativeState | BM_MSB_interworkingVccPointer | BM_MSB_pptp_potsUni_ARC | BM_MSB_pptp_potsUni_arcInterval | BM_MSB_impedance | BM_MSB_transmissionPath | BM_MSB_rxGain | BM_MSB_txGain)
#define BM_LSB_SETATTRIBS_PPTPPOTSUNI (BM_LSB_potsHoldoverTime)

#define IsInt8Negative(num)  ((((num) & 0x80) != 0) ? true : false)
#define IsInt8Positive(num)  ((((num) & 0x80) == 0) ? true : false)

// OMCI Entity DB
typedef struct 
{
    MeInst_S         meInst;
    UINT8            administrativeState;
    UINT16           deprecated;
    UINT8            ARC;
    UINT8            arcInterval;
    UINT8            impedance;
    UINT8            transmissionPath;
    INT8             rxGain;
    INT8             txGain;
    UINT8            operationalState;
    UINT8            hookState;
    UINT16           potsHoldoverTime;
    GL_TIMER_ID      wdId;
    TimedOmciFrame_S timedOmciFrame;
    bool             testInProgress;
} PptpPotsUniME;


OMCIPROCSTATUS createPptpPotsUniME(UINT16 meInstance);


MeClassDef_S pptpPotsUni_MeClassDef;


void initEmbedPotsUni(UINT16 meInstance);
OMCIPROCSTATUS embedPotsUniAdminStatus(UINT16 potsUniInstance, ADMINISTRATIVESTATUS administrativeState);

OMCIPROCSTATUS potsTestReportHandler(MeInst_S *pmeinst, OmciEvent_S *pevent);

#endif
