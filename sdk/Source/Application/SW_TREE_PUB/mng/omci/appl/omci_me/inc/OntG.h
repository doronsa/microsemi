/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OntG.h                                                    **/
/**                                                                          **/
/**  DESCRIPTION : This file contains ONT-G definitions and prototypes       **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCOntGh
#define __INCOntGh


// ONT-G Attribute bits
#define BM_MSB_vendorId                             (0x80)
#define BM_MSB_version                              (0x40)
#define BM_MSB_serialNumber                         (0x20)
#define BM_MSB_trafficmanagementOption              (0x10)
#define BM_MSB_vpCrossConnectOption                 (0x08)
#define BM_MSB_batteryBackup                        (0x04)
#define BM_MSB_ontG_administrativeState             (0x02)
#define BM_MSB_ontG_operationalState                (0x01)

#define BM_LSB_ontSurvivalTime                      (0x80)
#define BM_LSB_logicalOnuId                         (0x40)
#define BM_LSB_logicalPassword                      (0x20)
#define BM_LSB_credentialsStatus                    (0x10)



#define BM_MSB_GETATTRIBS_ONTG (BM_MSB_vendorId | BM_MSB_version | BM_MSB_serialNumber | BM_MSB_trafficmanagementOption | BM_MSB_vpCrossConnectOption | BM_MSB_batteryBackup | BM_MSB_ontG_administrativeState | BM_MSB_ontG_operationalState)
#define BM_LSB_GETATTRIBS_ONTG (BM_LSB_ontSurvivalTime | BM_LSB_logicalOnuId | BM_LSB_logicalPassword | BM_LSB_credentialsStatus)

#define BM_MSB_SETATTRIBS_ONTG (BM_MSB_batteryBackup | BM_MSB_ontG_administrativeState)
#define BM_LSB_SETATTRIBS_ONTG (BM_LSB_credentialsStatus)


// Used in MIB Upload snapshot 
#define ONTG_UPLOAD_CELL1_MSB_MASK (BM_MSB_vendorId | BM_MSB_version | BM_MSB_serialNumber)
#define ONTG_UPLOAD_CELL2_MSB_MASK (BM_MSB_trafficmanagementOption | BM_MSB_vpCrossConnectOption | BM_MSB_batteryBackup | BM_MSB_ontG_administrativeState | BM_MSB_ontG_operationalState)
#define ONTG_UPLOAD_CELL2_LSB_MASK (BM_LSB_ontSurvivalTime | BM_LSB_logicalPassword | BM_LSB_credentialsStatus)
#define ONTG_UPLOAD_CELL3_LSB_MASK (BM_LSB_logicalOnuId)



#define OMCI_REBOOTCMD_INTERVAL        APITIMERUNITS_FROM_SECONDS(2)
#define ONU_GPON_TIMER_PON_MNG_NAME    "wdReboot"



// OMCI Entity DB

#define ONTG_ALARMARA_SIZE         (2)
#define ONTG_LOGICAL_ONU_ID_SIZE   (24)
#define ONTG_LOGICAL_PASSWORD_SIZE (12)
typedef struct OntGME_tag
{
    MeInst_S meInst;
    UINT8    alarmsBitmap[OMCI_ALARM_BIT_MAP_LEN];
    UINT8    vendorId[4];
    UINT8    version[14];
    UINT8    serialNumber[8];
    UINT8    trafficmanagementOption;
    UINT8    vpCrossConnectOption;
    UINT8    batteryBackup;
    UINT8    administrativeState;
    UINT8    operationalState;
    UINT8    ontSurvivalTime;
    UINT8    logicalOnuId[ONTG_LOGICAL_ONU_ID_SIZE];
    UINT8    logicalPassword[ONTG_LOGICAL_PASSWORD_SIZE];  
    UINT8    credentialsStatus;
} OntGME;


OMCIPROCSTATUS createOntGME(UINT16 meInstance);


MeClassDef_S ontG_MeClassDef;

OMCIPROCSTATUS doTheReboot();

#endif
