/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : TrafDesc.h                                                **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the Traffic Descriptor ME            **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    9Mar08      zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCTrafDescch
#define __INCTrafDesch


#define BM_MSB_cir                              (0x80)
#define BM_MSB_pir                              (0x40)
#define BM_MSB_cbs                              (0x20)
#define BM_MSB_pbs                              (0x10)
#define BM_MSB_colourMode                       (0x08)
#define BM_MSB_ingressColourMarking             (0x04)
#define BM_MSB_egressColourMarking              (0x02)
#define BM_MSB_meterType                        (0x01)


#define BM_MSB_GETATTRIBS_TRAFDESC (BM_MSB_cir | BM_MSB_pir | BM_MSB_cbs | BM_MSB_pbs | BM_MSB_colourMode | \
                                       BM_MSB_ingressColourMarking | BM_MSB_egressColourMarking | BM_MSB_meterType)

#define BM_MSB_SETATTRIBS_TRAFDESC (BM_MSB_cir | BM_MSB_pir | BM_MSB_cbs | BM_MSB_pbs | BM_MSB_colourMode | \
                                       BM_MSB_ingressColourMarking | BM_MSB_egressColourMarking | BM_MSB_meterType)

#define BM_MSB_CREATEATTRIBS_TRAFDESC (BM_MSB_cir | BM_MSB_pir | BM_MSB_cbs | BM_MSB_pbs | BM_MSB_colourMode | \
                                       BM_MSB_ingressColourMarking | BM_MSB_egressColourMarking | BM_MSB_meterType)


// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT32   cir;
    UINT32   pir;
    UINT32   cbs;
    UINT32   pbs;
    UINT8    colourMode;
    UINT8    ingressColourMarking;
    UINT8    egressColourMarking;
    UINT8    meterType;           

} TrafDescME;



// messageContents - Create VP Network CTP Command
#pragma pack(1)
typedef struct
{
    UINT32   cir;
    UINT32   pir;
    UINT32   cbs;
    UINT32   pbs;
    UINT8    colourMode;
    UINT8    ingressColourMarking;
    UINT8    egressColourMarking;
    UINT8    meterType;           
} CreateTrafDesc_s;
#pragma pack(0)


MeClassDef_S trafDesc_MeClassDef;


#endif
