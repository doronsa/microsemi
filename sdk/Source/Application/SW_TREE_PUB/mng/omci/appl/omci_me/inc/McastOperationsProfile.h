/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : McastOperationsProfile.h                                  **/
/**                                                                          **/
/**  DESCRIPTION : This file has Multicast Operations Profile definitions    **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    20Jan09     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCMcastOperationsProfileh
#define __INCMcastOperationsProfileh


// Multicast Operations Profile Attribute bits
#define BM_MSB_igmpVersion                      (0x80)
#define BM_MSB_igmpFunction                     (0x40) 
#define BM_MSB_immediateLeave                   (0x20)
#define BM_MSB_upstreamIGMPTci                  (0x10)
#define BM_MSB_upstreamIGMPTagControl           (0x08)
#define BM_MSB_upstreamIGMPRate                 (0x04)
#define BM_MSB_dynamicAccessControlListTable    (0x02)
#define BM_MSB_staticAccessControlListTable     (0x01)
#define BM_LSB_lostGroupsListTable              (0x80)
#define BM_LSB_robustness                       (0x40) 
#define BM_LSB_querierIPAddress                 (0x20)
#define BM_LSB_queryInterval                    (0x10)
#define BM_LSB_queryMaxResponseTime             (0x08)
#define BM_LSB_lastMemberQueryInterval          (0x04)
#define BM_LSB_unauthorizedJoinRequestBehaviour (0x02)
#define BM_LSB_downstreamIGMPMcastTci           (0x01)


#define BM_MSB_GETATTRIBS_MCASTOPSPROF (BM_MSB_igmpVersion | BM_MSB_igmpFunction | BM_MSB_immediateLeave | BM_MSB_upstreamIGMPTci | \
            BM_MSB_upstreamIGMPTagControl | BM_MSB_upstreamIGMPRate | BM_MSB_dynamicAccessControlListTable | BM_MSB_staticAccessControlListTable)

#define BM_LSB_GETATTRIBS_MCASTOPSPROF (BM_LSB_lostGroupsListTable | BM_LSB_robustness | BM_LSB_querierIPAddress | BM_LSB_queryInterval | \
            BM_LSB_queryMaxResponseTime | BM_LSB_lastMemberQueryInterval | BM_LSB_unauthorizedJoinRequestBehaviour | BM_LSB_downstreamIGMPMcastTci)


#define BM_MSB_SETATTRIBS_MCASTOPSPROF (BM_MSB_igmpVersion | BM_MSB_igmpFunction | BM_MSB_immediateLeave | BM_MSB_upstreamIGMPTci | \
            BM_MSB_upstreamIGMPTagControl | BM_MSB_upstreamIGMPRate | BM_MSB_dynamicAccessControlListTable | BM_MSB_staticAccessControlListTable)

#define BM_LSB_SETATTRIBS_MCASTOPSPROF (BM_LSB_robustness | BM_LSB_querierIPAddress | BM_LSB_queryInterval | \
            BM_LSB_queryMaxResponseTime | BM_LSB_lastMemberQueryInterval | BM_LSB_unauthorizedJoinRequestBehaviour | BM_LSB_downstreamIGMPMcastTci)


#define BM_MSB_CREATEATTRIBS_MCASTOPSPROF (BM_MSB_igmpVersion | BM_MSB_igmpFunction | BM_MSB_immediateLeave | BM_MSB_upstreamIGMPTci | \
            BM_MSB_upstreamIGMPTagControl | BM_MSB_upstreamIGMPRate)

#define BM_LSB_CREATEATTRIBS_MCASTOPSPROF (BM_LSB_robustness | BM_LSB_querierIPAddress | BM_LSB_queryInterval | BM_LSB_queryMaxResponseTime | BM_LSB_downstreamIGMPMcastTci)


// Used in MIB Upload snapshot 
#define MCASTOPSPROF_UPLOAD_CELL1_MSB_MASK (BM_MSB_igmpVersion | BM_MSB_igmpFunction | BM_MSB_immediateLeave | BM_MSB_upstreamIGMPTci | \
            BM_MSB_upstreamIGMPTagControl | BM_MSB_upstreamIGMPRate | BM_MSB_dynamicAccessControlListTable | BM_MSB_staticAccessControlListTable)

#define MCASTOPSPROF_UPLOAD_CELL2_LSB_MASK (BM_LSB_lostGroupsListTable | BM_LSB_robustness | BM_LSB_querierIPAddress | BM_LSB_queryInterval | \
            BM_LSB_queryMaxResponseTime | BM_LSB_lastMemberQueryInterval | BM_LSB_unauthorizedJoinRequestBehaviour | BM_LSB_downstreamIGMPMcastTci)

typedef enum
{
    MC_IGMP_VERSION_1 = 1,
    MC_IGMP_VERSION_2 = 2,
    MC_IGMP_VERSION_3 = 3,
    MC_MLD_VERSION_1 = 16,
    MC_MLD_VERSION_2 = 17

} OMCI_IGMP_VERION_E;


typedef enum
{
    IGMPFUNCTION_SNOOPING, IGMPFUNCTION_SNOOPING_AND_PROXYREPORT, IGMPFUNCTION_PROXY
} IGMPFUNCTION;


typedef enum
{
    USIGMPTAGCONTROL_TRANSPARENT, USIGMPTAGCONTROL_ADDTAG, USIGMPTAGCONTROL_REPLACETAG, USIGMPTAGCONTROL_REPLACEVLANID
} USIGMPTAGCONTROL;

typedef enum
{
    DSIGMPTAGCONTROL_TRANSPARENT,     DSIGMPTAGCONTROL_STRIPTAG, 
    DSIGMPTAGCONTROL_ADDTAG,          DSIGMPTAGCONTROL_REPLACETAG,          DSIGMPTAGCONTROL_REPLACEVLANID,
    DSIGMPTAGCONTROL_SRVCPACK_ADDTAG, DSIGMPTAGCONTROL_SRVCPACK_REPLACETAG, DSIGMPTAGCONTROL_SRVCPACK_REPLACEVLANID
} DSIGMPTAGCONTROL;

typedef enum
{
    ACLCONTROLFORMAT_0, ACLCONTROLFORMAT_1, ACLCONTROLFORMAT_2, ACLCONTROLFORMAT_3, 
    ACLCONTROLFORMAT_4, ACLCONTROLFORMAT_5, ACLCONTROLFORMAT_6, ACLCONTROLFORMAT_7, 
} ACLCONTROLFORMAT;


// Note clear of test bit since we support Ext Mcast Operations Profile
#define EXTRACT_ACLROWID(x)                  ((x) & 0x3FF)
#define EXTRACT_ACLPARTFORMAT(x)             (((x) >> 11) & 0x7)
#define EXTRACT_ACLROWCONTROL(x)             (((x) >> 14) & 0x3)
#define CLEAN_ACLTABLE_CONTROL(x)            ((x) & 0x3BFF)              

typedef enum
{
    ACLCNTLBITS_ADD=1, ACLCNTLBITS_DELETE, ACLCNTLBITS_CLEARALL 
} ACLCNTLBITS;


#pragma pack(1)
typedef struct
{
    UINT16 gemPort;
    UINT16 vlanId;
    UINT8  srcIpAddress[IPADDR_SIZE];
    UINT8  startDestIpAddress[IPADDR_SIZE];
    UINT8  endDestIpAddress[IPADDR_SIZE];
    UINT32 bandWidth;
    UINT8  rsrvd[2];
} AclFormat0_S;

#define V6_PREFIX_IPADDR_SIZE           (12)
typedef struct
{
    UINT8  ipv6PrefixSrcIpAddress[V6_PREFIX_IPADDR_SIZE];
    UINT16 previewLength;      
    UINT16 previewRepeatTime;   
    UINT16 previewRepeatCount;  
    UINT16 previewResetTime; 
    UINT8  rsrvd[2];
} AclFormat1_S;

typedef struct
{
    UINT8  ipv6PrefixDestIpAddress[V6_PREFIX_IPADDR_SIZE];
    UINT8  rsrvd[10];
} AclFormat2_S;

typedef struct
{
    UINT8  rsrvd[28];
} AclFormat3_S;

typedef union
{
    AclFormat0_S format0;
    AclFormat1_S format1;
    AclFormat2_S format2;
    AclFormat3_S format3;
} AclFormatsEntry_U;

typedef struct
{
    UINT16             tableControl;
    AclFormatsEntry_U  aclFormatsEntry;
} AccessControlEntry_S;
#pragma pack(0)


#pragma pack(1)
typedef struct
{
    UINT16 vlanId;
    UINT8  srcIpAddress[IPADDR_SIZE];
    UINT8  mcastDestIpAddress[IPADDR_SIZE];
} LostGroupsEntry_S;
#pragma pack(0)



#define MAX_DYNAMIC_ACL_ENTRIES            (128)
#define MAX_STATIC_ACL_ENTRIES             (128)
#define MAX_LOST_GROUPS_ENTRIES            (8)

#define DOWNSTREAM_IGMP_TCI_LEN            (3)

// OMCI Entity DB

typedef struct
{
    MeInst_S              meInst;
    UINT8                 igmpVersion;
    UINT8                 igmpFunction;
    UINT8                 immediateLeave;
    UINT16                upstreamIGMPTci;
    UINT8                 upstreamIGMPTagControl;
    UINT32                upstreamIGMPRate;
    AccessControlEntry_S  dynamicACLAra[MAX_DYNAMIC_ACL_ENTRIES];
    AccessControlEntry_S  staticACLAra [MAX_STATIC_ACL_ENTRIES];
    LostGroupsEntry_S     lostGroupsAra[MAX_LOST_GROUPS_ENTRIES];
    UINT8                 robustness;
    UINT8                 querierIPAddress[IPADDR_SIZE];
    UINT32                queryInterval;
    UINT32                queryMaxResponseTime;
    UINT32                lastMemberQueryInterval;
    UINT8                 unauthorizedJoinRequestBehaviour;
    UINT8                 downstreamIGMPMcastTci[DOWNSTREAM_IGMP_TCI_LEN];

    // Fields for actual table size
    UINT32                dynamicACLActualSize;
    bool                  dynamicACLEntryInUse[MAX_DYNAMIC_ACL_ENTRIES];
    UINT32                staticACLActualSize;
    bool                  staticACLEntryInUse[MAX_STATIC_ACL_ENTRIES];
    UINT32                lostGroupsActualSize;
    bool                  lostGroupsEntryInUse[MAX_LOST_GROUPS_ENTRIES];

    struct
    {
        UINT32  currentTicks;
        AccessControlEntry_S  aclAra[MAX_DYNAMIC_ACL_ENTRIES];
    } getNextBufDynamicACL;

    struct
    {
        UINT32  currentTicks;
        AccessControlEntry_S  aclAra[MAX_STATIC_ACL_ENTRIES];
    } getNextBufStaticACL;

    struct
    {
        UINT32  currentTicks;
        AccessControlEntry_S  lostGroupsAra[MAX_LOST_GROUPS_ENTRIES];
    } getNextBufLostGroups;

} McastOperationsProfileME;



// messageContents - Create Multicast Operations Profile Command
#pragma pack(1)
typedef struct
{
    UINT8    igmpVersion;
    UINT8    igmpFunction;
    UINT8    immediateLeave;
    UINT16   upstreamIGMPTci;
    UINT8    upstreamIGMPTagControl;
    UINT32   upstreamIGMPRate;
    UINT8    robustness;
    UINT8    querierIPAddress[IPADDR_SIZE];
    UINT32   queryInterval;
    UINT32   queryMaxResponseTime;
    UINT8    downstreamIGMPMcastTci[3];
} CreateMcastOperationsProfile_S;
#pragma pack(0)



MeClassDef_S mcastOperationsProfile_MeClassDef;


#endif
