
/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VoIPHelper.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file contains VoIP Helper definitions                **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Aug04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVoIPHelperh
#define __INCVoIPHelperh

#include "omci_tl_voip.h"

#define MAX_LARGESTRINGPARTS     15
#define LARGESTRING_SIZE         (MAX_LARGESTRINGPARTS * PART_SIZE)


OMCIPROCSTATUS copyApplicServicesProfile (UINT16 applicServicesProfilePtr, VOIPAPPLICSERVICEPROFILE_T *p_VoIPApplicServiceProfile);
OMCIPROCSTATUS copyFeatureAccessCodes    (UINT16 featureAccessCodesPtr,    VOIPFEATUREACCESSCODES_T   *p_VoIPFeatureAccessCodes);
OMCIPROCSTATUS copySipUser               (SipUserDataME *psipuser,         SIPUSER_T *p_SipUser);

OMCIPROCSTATUS copyMediaProfile          (UINT16 voipMediaProfilePointer, MEDIAPROCESSING_T *p_MediaProcessing);
OMCIPROCSTATUS copyVoiceServiceProfile   (UINT16 voiceServiceProfilePtr, VOICESERVICEPROFILE_T *p_VoiceServiceProfile);
OMCIPROCSTATUS copyRtpProfile            (UINT16 rtpProfilePtr, RTPPROFILE_T *p_RtpProfile);

OMCIPROCSTATUS fillSipLine(SIPLINE_T *p_SipLine, SipUserDataME *psipuser, VoIPVoiceCtpME *pvoipvoicectp);
OMCIPROCSTATUS fillSipAgent(SIPAGENT_T *p_SipAgent, SipAgentConfigDataME *psipagent);


#endif
  
