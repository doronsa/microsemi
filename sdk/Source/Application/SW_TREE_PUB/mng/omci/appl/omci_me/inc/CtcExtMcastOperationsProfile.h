/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : CtcExtMcastOperationsProfile.h                            **/
/**                                                                          **/
/**  DESCRIPTION : This file has Multicast Operations Profile definitions    **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *     7Aug11     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCCtcExtMcastOperationsProfileh
#define __INCCtcExtMcastOperationsProfileh


// Multicast Operations Profile Attribute bits
#define BM_MSB_ctc_igmpVersion                      (0x80)
#define BM_MSB_ctc_igmpFunction                     (0x40) 
#define BM_MSB_ctc_immediateLeave                   (0x20)
#define BM_MSB_ctc_upstreamIgmpTci                  (0x10)
#define BM_MSB_ctc_upstreamIgmpTagControl           (0x08)
#define BM_MSB_ctc_upstreamIgmpRate                 (0x04)
#define BM_MSB_ctc_dynamicAccessControlListTable    (0x02)
#define BM_MSB_ctc_staticAccessControlListTable     (0x01)
#define BM_LSB_ctc_lostGroupsListTable              (0x80)
#define BM_LSB_ctc_robustness                       (0x40) 
#define BM_LSB_ctc_querierIPAddress                 (0x20)
#define BM_LSB_ctc_queryInterval                    (0x10)
#define BM_LSB_ctc_queryMaxResponseTime             (0x08)
#define BM_LSB_ctc_lastMemberQueryInterval          (0x04)
#define BM_LSB_ctc_unauthorizedJoinRequestBehaviour (0x02)
#define BM_LSB_ctc_downstreamIGMPMcastTci           (0x01)


#define BM_MSB_GETATTRIBS_CTCMCASTOPSPROF (BM_MSB_ctc_igmpVersion | BM_MSB_ctc_igmpFunction | BM_MSB_ctc_immediateLeave | BM_MSB_ctc_upstreamIgmpTci | \
            BM_MSB_ctc_upstreamIgmpTagControl | BM_MSB_ctc_upstreamIgmpRate | BM_MSB_ctc_dynamicAccessControlListTable | BM_MSB_ctc_staticAccessControlListTable)

#define BM_LSB_GETATTRIBS_CTCMCASTOPSPROF (BM_LSB_ctc_lostGroupsListTable | BM_LSB_ctc_robustness | BM_LSB_ctc_querierIPAddress | BM_LSB_ctc_queryInterval | \
            BM_LSB_ctc_queryMaxResponseTime | BM_LSB_ctc_lastMemberQueryInterval | BM_LSB_ctc_unauthorizedJoinRequestBehaviour | BM_LSB_ctc_downstreamIGMPMcastTci)


#define BM_MSB_SETATTRIBS_CTCMCASTOPSPROF (BM_MSB_ctc_igmpVersion | BM_MSB_ctc_igmpFunction | BM_MSB_ctc_immediateLeave | BM_MSB_ctc_upstreamIgmpTci | \
            BM_MSB_ctc_upstreamIgmpTagControl | BM_MSB_ctc_upstreamIgmpRate | BM_MSB_ctc_dynamicAccessControlListTable | BM_MSB_ctc_staticAccessControlListTable)

#define BM_LSB_SETATTRIBS_CTCMCASTOPSPROF (BM_LSB_ctc_robustness | BM_LSB_ctc_querierIPAddress | BM_LSB_ctc_queryInterval | \
            BM_LSB_ctc_queryMaxResponseTime | BM_LSB_ctc_lastMemberQueryInterval | BM_LSB_ctc_unauthorizedJoinRequestBehaviour | BM_LSB_ctc_downstreamIGMPMcastTci)


#define BM_MSB_CREATEATTRIBS_CTCMCASTOPSPROF (BM_MSB_ctc_igmpVersion | BM_MSB_ctc_igmpFunction | BM_MSB_ctc_immediateLeave | BM_MSB_ctc_upstreamIgmpTci | \
            BM_MSB_ctc_upstreamIgmpTagControl | BM_MSB_ctc_upstreamIgmpRate)

#define BM_LSB_CREATEATTRIBS_CTCMCASTOPSPROF (BM_LSB_ctc_robustness | BM_LSB_ctc_queryInterval | BM_LSB_ctc_queryMaxResponseTime | \
            BM_LSB_ctc_unauthorizedJoinRequestBehaviour | BM_LSB_ctc_downstreamIGMPMcastTci)



// Used in MIB Upload snapshot 
#define CTCMCASTOPSPROF_UPLOAD_CELL1_MSB_MASK (BM_MSB_ctc_igmpVersion | BM_MSB_ctc_igmpFunction | BM_MSB_ctc_immediateLeave | BM_MSB_ctc_upstreamIgmpTci | \
            BM_MSB_ctc_upstreamIgmpTagControl | BM_MSB_ctc_upstreamIgmpRate | BM_MSB_ctc_dynamicAccessControlListTable | BM_MSB_ctc_staticAccessControlListTable)

#define CTCMCASTOPSPROF_UPLOAD_CELL2_LSB_MASK (BM_LSB_ctc_lostGroupsListTable | BM_LSB_ctc_robustness | BM_LSB_ctc_querierIPAddress | BM_LSB_ctc_queryInterval)

#define CTCMCASTOPSPROF_UPLOAD_CELL3_LSB_MASK (BM_LSB_ctc_queryMaxResponseTime | BM_LSB_ctc_lastMemberQueryInterval | BM_LSB_ctc_unauthorizedJoinRequestBehaviour | BM_LSB_ctc_downstreamIGMPMcastTci)



typedef enum
{
    CTCIGMPFUNC_SNOOPING, CTCIGMPFUNC_SNOOPING_AND_PROXYREPORT, CTCIGMPFUNC_FULL_PROXY
} CTCIGMPFUNCTION;


typedef enum
{
    CTC_USIGMPTAGCONTROL_TRANSPARENT, CTC_USIGMPTAGCONTROL_ADDTAG, CTC_USIGMPTAGCONTROL_REPLACETAG, CTC_USIGMPTAGCONTROL_REPLACEVLANID
} CTC_USIGMPTAGCONTROL;

typedef enum
{
    CTC_DSIGMPTAGCONTROL_TRANSPARENT,     CTC_DSIGMPTAGCONTROL_STRIPTAG, 
    CTC_DSIGMPTAGCONTROL_ADDTAG,          CTC_DSIGMPTAGCONTROL_REPLACETAG,          CTC_DSIGMPTAGCONTROL_REPLACEVLANID,
} CTC_DSIGMPTAGCONTROL;


#define DEF_LAST_MEMBER_QUERY_INTERVAL    (10)

#define EXTRACT_CTCACL_ROWINDEX(x)        ((x) & 0x1FF)
#define EXTRACT_CTCACL_PARTFORMAT(x)      (((x) >> 12) & 0x3)
#define EXTRACT_CTCACL_ROWCONTROL(x)      (((x) >> 14) & 0x3)
#define CLEAN_CTCACL_TABLE_CONTROL(x)     ((x) & 0x31FF)              

typedef enum
{
    ROWCNTL_ADD=1, ROWCNTL_DELETE=2, ROWCNTL_CLEARALL=3 
} ROWCONTROL;
  
typedef enum
{
    ROWFORMAT_PART_0=0, ROWFORMAT_PART_1=1, ROWFORMAT_PART_2=2, ROWFORMAT_PART_3=3
} ROWFORMAT;


#pragma pack(1)
typedef struct
{
    UINT16 gemPort;
    UINT16 vlanId;
    UINT32 imputedGroupBandwidth;
    UINT16 previewLength;
    UINT16 previewRepeatTime;
    UINT16 previewRepeatCount;
    UINT16 previewResetTime;
    UINT8  vendorReserved[4];
    UINT8  rsrvd[8];
} CtcAclFormat0_S;

typedef struct
{
    UINT8  startDestIpAddress[V6_IPADDR_SIZE];
    UINT8  rsrvd[12];
} CtcAclFormat1_S;

typedef struct
{
    UINT8  endDestIpAddress[V6_IPADDR_SIZE];
    UINT8  rsrvd[12];
} CtcAclFormat2_S;

typedef struct
{
    UINT8  rsrvd[28];
} CtcAclFormat3_S;

typedef union
{
    CtcAclFormat0_S format0;
    CtcAclFormat1_S format1;
    CtcAclFormat2_S format2;
    CtcAclFormat3_S format3;
} CtcAclFormatsEntry_U;

typedef struct
{
    UINT16                tableControl;
    CtcAclFormatsEntry_U  aclFormatsEntry;
} CtcAccessControlEntry_S;
#pragma pack(0)


#pragma pack(1)
typedef struct
{
    UINT8  mcastDestIpAddress[V6_IPADDR_SIZE];
} CtcLostGroupsEntry_S;
#pragma pack(0)



#define MAX_CTC_DYNAMIC_ACL_ENTRIES        (64)
#define MAX_CTC_STATIC_ACL_ENTRIES         (64)
#define MAX_CTC_LOST_GROUPS_ENTRIES        (16)
#define DOWNSTREAMIGMPMCASTTCI_SIZE        (3)

// OMCI Entity DB
typedef struct
{
    MeInst_S                 meInst;
    UINT8                    igmpVersion;
    UINT8                    igmpFunction;
    UINT8                    immediateLeave;
    UINT16                   upstreamIgmpTci;
    UINT8                    upstreamIgmpTagControl;
    UINT32                   upstreamIgmpRate;
    CtcAccessControlEntry_S  dynamicAclAra[MAX_CTC_DYNAMIC_ACL_ENTRIES];
    CtcAccessControlEntry_S  staticAclAra [MAX_CTC_STATIC_ACL_ENTRIES];
    CtcLostGroupsEntry_S     ctcLostGroupsAra[MAX_CTC_LOST_GROUPS_ENTRIES];
    UINT8                    robustness;
    UINT8                    querierIPAddress[V6_IPADDR_SIZE];
    UINT32                   queryInterval;
    UINT32                   queryMaxResponseTime;
    UINT32                   lastMemberQueryInterval;
    UINT8                    unauthorizedJoinRequestBehaviour;
    UINT8                    downstreamIgmpMcastTci[DOWNSTREAMIGMPMCASTTCI_SIZE];

    // Fields for actual table size
    UINT32                   dynamicACLActualSize;
    bool                     dynamicACLEntryInUse[MAX_CTC_DYNAMIC_ACL_ENTRIES];
    UINT32                   staticACLActualSize;
    bool                     staticACLEntryInUse[MAX_CTC_STATIC_ACL_ENTRIES];
    UINT32                   lostGroupsActualSize;
    bool                     lostGroupsEntryInUse[MAX_CTC_LOST_GROUPS_ENTRIES];

    struct
    {
        UINT32  currentTicks;
        CtcAccessControlEntry_S  aclAra[MAX_CTC_DYNAMIC_ACL_ENTRIES];
    } getNextBufDynamicACL;

    struct
    {
        UINT32  currentTicks;
        CtcAccessControlEntry_S  aclAra[MAX_CTC_STATIC_ACL_ENTRIES];
    } getNextBufStaticACL;

    struct
    {
        UINT32  currentTicks;
        CtcLostGroupsEntry_S  lostGroupsAra[MAX_CTC_LOST_GROUPS_ENTRIES];
    } getNextBufLostGroups;

} CtcExtMcastOperationsProfileME;



// messageContents - Create Multicast Operations Profile Command
#pragma pack(1)
typedef struct
{
    UINT8     igmpVersion;
    UINT8     igmpFunction;
    UINT8     immediateLeave;
    UINT16    upstreamIgmpTci;
    UINT8     upstreamIgmpTagControl;
    UINT32    upstreamIgmpRate;
    UINT8     robustness;
    UINT32    queryInterval;
    UINT32    queryMaxResponseTime;
    UINT8     unauthorizedJoinRequestBehaviour;
    UINT8     downstreamIgmpMcastTci[3];
} CreateCtcExtMcastOperationsProfile_S;
#pragma pack(0)



MeClassDef_S ctcExtmcastOperationsProfile_MeClassDef;


#endif
