/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : MacBridgeConfigData.h                                     **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Mac Bridge Config Data ME            **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCMacBridgeConfigDatah
#define __INCMacBridgeConfigDatah



// MAC Bridge Config Data Attribute bits
#define BM_MSB_bridgeMacAddress                     (0x80)
#define BM_MSB_bridgePriority                       (0x40) 
#define BM_MSB_designatedRoot                       (0x20) 
#define BM_MSB_rootPathCost                         (0x10)
#define BM_MSB_bridgePortCount                      (0x08)
#define BM_MSB_rootPortNum                          (0x04)      
#define BM_MSB_mbcd_helloTime                       (0x02) 
#define BM_MSB_mbcd_forwardDelay                    (0x01)     



#define BM_MSB_GETATTRIBS_MACBRIDGECONFIGDATA BM_MSB_bridgeMacAddress | BM_MSB_bridgePriority | BM_MSB_designatedRoot | BM_MSB_rootPathCost | BM_MSB_bridgePortCount | BM_MSB_rootPortNum | BM_MSB_mbcd_helloTime | BM_MSB_mbcd_forwardDelay

#define MACBRIDGECONFIGDATA_UPLOAD_CELL1_MSB_MASK (BM_MSB_bridgeMacAddress | BM_MSB_bridgePriority | BM_MSB_designatedRoot | BM_MSB_rootPathCost)
#define MACBRIDGECONFIGDATA_UPLOAD_CELL2_MSB_MASK (BM_MSB_bridgePortCount  | BM_MSB_rootPortNum    | BM_MSB_mbcd_helloTime | BM_MSB_mbcd_forwardDelay)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    bridgeMacAddress[6];
    UINT16   bridgePriority;
    UINT8    designatedRoot[8];
    UINT32   rootPathCost;
    UINT8    bridgePortCount;
    UINT16   rootPortNum;
    UINT16   helloTime;
    UINT16   forwardDelay;
} MacBridgeConfigDataME;


OMCIPROCSTATUS createMacBridgeConfigDataME(UINT16 meInstance);


MeClassDef_S macBridgeConfigData_MeClassDef;

#endif
