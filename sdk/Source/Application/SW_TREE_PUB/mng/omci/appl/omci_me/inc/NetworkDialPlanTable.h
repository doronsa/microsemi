/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : NetworkDialPlanTable.h                                    **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Network Dial Plan Table ME           **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCNetworkDialPlanTableh
#define __INCNetworkDialPlanTableh


#define UNUSED_FLAG                         0xFF

// RTP Profile Data Attribute bits
#define BM_MSB_dialPlanNumber               (0x80)
#define BM_MSB_dialPlanTableMaxSize         (0x40) 
#define BM_MSB_criticalDialTimeout          (0x20)
#define BM_MSB_partialDialTimeout           (0x10)
#define BM_MSB_dialPlanFormat               (0x08)
#define BM_MSB_dialPlanTable                (0x04) 



#define BM_MSB_GETATTRIBS_NETWORKDIALPLANTABLE (BM_MSB_dialPlanNumber | BM_MSB_dialPlanTableMaxSize |  BM_MSB_criticalDialTimeout | BM_MSB_partialDialTimeout | BM_MSB_dialPlanFormat | BM_MSB_dialPlanTable)

#define BM_MSB_SETATTRIBS_NETWORKDIALPLANTABLE (BM_MSB_dialPlanTableMaxSize |  BM_MSB_criticalDialTimeout | BM_MSB_partialDialTimeout | BM_MSB_dialPlanFormat | BM_MSB_dialPlanTable)

#define BM_MSB_CREATEATTRIBS_NETWORKDIALPLANTABLE (BM_MSB_dialPlanTableMaxSize |  BM_MSB_criticalDialTimeout | BM_MSB_partialDialTimeout | BM_MSB_dialPlanFormat)


// Dial Plan entry
#define DIALPLANENTRY_REMOVE       0
#define DIALPLANENTRY_ADD          1
#define DIALPLANTOKEN_SIZE         28

#pragma pack(1)
typedef struct
{
    UINT8   dialPlanId;
    UINT8   action;
    UINT8   dialPlanToken[DIALPLANTOKEN_SIZE];
} DialPlan_S;
#pragma pack(0)




// OMCI Entity DB
#define NUM_DIALPLAN_ENTRIES       10

typedef struct
{
    MeInst_S   meInst;
    UINT16     dialPlanNumber;
    UINT16     dialPlanTableMaxSize;
    UINT16     criticalDialTimeout;
    UINT16     partialDialTimeout;
    UINT8      dialPlanFormat;
    DialPlan_S dialPlanTable[NUM_DIALPLAN_ENTRIES];
    UINT32     dialPlanTableActualSize; 
    struct
    {
        UINT32  currentTicks;
        DialPlan_S dialPlanTable[NUM_DIALPLAN_ENTRIES];
    } getNextBuf;
} NetworkDialPlanTableME;



// messageContents - Create RTP Profile Data Command
#pragma pack(1)
typedef struct
{
    UINT16   dialPlanTableMaxSize;
    UINT16   criticalDialTimeout;
    UINT16   partialDialTimeout;
    UINT8    dialPlanFormat;
} CreateNetworkDialPlanTable_s;
#pragma pack(0)



MeClassDef_S networkDialPlanTable_MeClassDef;


#endif
