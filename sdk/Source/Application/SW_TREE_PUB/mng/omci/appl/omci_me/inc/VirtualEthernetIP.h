/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VirtualEthernetIP.h                                       **/
/**                                                                          **/
/**  DESCRIPTION : This file has Virtual Ethernet Interface Point definitions**/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    18Jan10     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVirtualEthernetIPh
#define __INCVirtualEthernetIPh


// Virtual Ethernet IP Attribute bits
#define BM_MSB_virteth_adminState             (0x80)
#define BM_MSB_virteth_operationalState       (0x40)
#define BM_MSB_interDomainName                (0x20)
#define BM_MSB_virteth_tcpUdpPointer                  (0x10)
#define BM_MSB_ianaAssignedPort               (0x08) 


#define BM_MSB_GETATTRIBS_VIRTUALETHERNETIP (BM_MSB_virteth_adminState | BM_MSB_virteth_operationalState | BM_MSB_interDomainName | BM_MSB_virteth_tcpUdpPointer | BM_MSB_ianaAssignedPort)
#define BM_MSB_SETATTRIBS_VIRTUALETHERNETIP (BM_MSB_virteth_adminState | BM_MSB_interDomainName | BM_MSB_virteth_tcpUdpPointer)


// Used in MIB Upload snapshot 
#define VIRTUALETHERNETIP_UPLOAD_CELL1_MSB_MASK (BM_MSB_virteth_adminState | BM_MSB_virteth_operationalState | BM_MSB_virteth_tcpUdpPointer | BM_MSB_ianaAssignedPort)
#define VIRTUALETHERNETIP_UPLOAD_CELL2_MSB_MASK (BM_MSB_interDomainName)


// OMCI Entity DB

#define INTERDOMAINNAME_SIZE                25
typedef struct
{
    MeInst_S meInst;
    UINT8    adminState;
    UINT8    operationalState; 
    UINT8    interDomainName[INTERDOMAINNAME_SIZE];   
    UINT16   tcpUdpPointer;     
    UINT16   ianaAssignedPort;  
    UINT8    alarmsBitmap[OMCI_ALARM_BIT_MAP_LEN];
} VirtualEthernetIPME;


OMCIPROCSTATUS createVirtualEthernetIPME(UINT16 meInstance);


MeClassDef_S virtualEthernetIP_MeClassDef;


#endif
