/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : RtpMonitoringData.h                                       **/
/**                                                                          **/
/**  DESCRIPTION : This file has RTP Monitoring Data definitions             **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCRtpMonitoringDatah
#define __INCRtpMonitoringDatah


// RTP Monitoring Data Attribute bits
#define BM_MSB_rtpmon_intervalEndTime              (0X80)
#define BM_MSB_rtpmon_thresholdDataId              (0X40)
#define BM_MSB_rtpErrors                           (0X20)
#define BM_MSB_packetLoss                          (0X10)
#define BM_MSB_maximumJitter                       (0X08)
#define BM_MSB_maxTimeBetweenRtcpPackets           (0X04)
#define BM_MSB_bufferUnderflows                    (0X02)
#define BM_MSB_bufferOverflows                     (0X01)



#define BM_MSB_GETATTRIBS_RTPMONITORINGDATA (BM_MSB_rtpmon_intervalEndTime | BM_MSB_rtpmon_thresholdDataId | BM_MSB_rtpErrors | BM_MSB_packetLoss | BM_MSB_maximumJitter | BM_MSB_maxTimeBetweenRtcpPackets | BM_MSB_bufferUnderflows | BM_MSB_bufferOverflows)

#define BM_MSB_SETATTRIBS_RTPMONITORINGDATA (BM_MSB_rtpmon_thresholdDataId)

#define BM_MSB_CREATEATTRIBS_RTPMONITORINGDATA (BM_MSB_rtpmon_thresholdDataId)


// Create message contents
#pragma pack(1)
typedef struct
{
    UINT16 thresholdDataId;
} CreateRtpMonitoringDataCmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    tcaBitMask[2];
    UINT8    intervalEndTime;
    UINT16   thresholdDataId;
    UINT32   rtpErrors;
    UINT32   packetLoss;
    UINT32   maximumJitter;
    UINT32   maxTimeBetweenRtcpPackets;
    UINT32   bufferUnderflows;
    UINT32   bufferOverflows;
} RtpMonitoringDataME;


extern MeClassDef_S rtpMonitoringData_MeClassDef;


#endif
