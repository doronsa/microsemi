/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : McastSubscriberConfigInfo.h                               **/
/**                                                                          **/
/**  DESCRIPTION : This file has Mcast Subscriber Config Info definitions    **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    20Jan09     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCMcastSubscriberConfigInfoh
#define __INCMcastSubscriberConfigInfoh


// Multicast Operations Profile Attribute bits
#define BM_MSB_meType                           (0x80)
#define BM_MSB_mcastOperationsProfilePtr        (0x40) 
#define BM_MSB_maxSimultaneousGroups            (0x20)
#define BM_MSB_maxMulticastBandwidth            (0x10)
#define BM_MSB_bandwidthEnforcement             (0x08)
#define BM_MSB_mcastServicePackageTable         (0x04)
#define BM_MSB_allowedPreviewGroupsTable        (0x02)

#define BM_MSB_GETATTRIBS_MCASTSUBSCRIBCONFIG (BM_MSB_meType | BM_MSB_mcastOperationsProfilePtr | BM_MSB_maxSimultaneousGroups | \
                                               BM_MSB_maxMulticastBandwidth | BM_MSB_bandwidthEnforcement | BM_MSB_mcastServicePackageTable | BM_MSB_allowedPreviewGroupsTable)

#define BM_MSB_SETATTRIBS_MCASTSUBSCRIBCONFIG (BM_MSB_meType | BM_MSB_mcastOperationsProfilePtr | BM_MSB_maxSimultaneousGroups | \
                                               BM_MSB_maxMulticastBandwidth | BM_MSB_bandwidthEnforcement | BM_MSB_mcastServicePackageTable | BM_MSB_allowedPreviewGroupsTable)


#define BM_MSB_CREATEATTRIBS_MCASTSUBSCRIBCONFIG (BM_MSB_meType | BM_MSB_mcastOperationsProfilePtr | BM_MSB_maxSimultaneousGroups | \
                                               BM_MSB_maxMulticastBandwidth | BM_MSB_bandwidthEnforcement)

typedef enum
{
    MCASTSUBSCRMETYPE_MACBRIDGE, MCASTSUBSCRMETYPE_8021PMAPPER
} MCASTSUBSCRMETYPE;

#define EXTRACT_ROWOPERATION(b1)                (((b1) >> 6) & 0x3)
// Note: following definition will do for G.988 Amd 1 (reserved bits taken as is) and CTC GPON 2.0 
#define EXTRACT_ROWKEY(b1,b2)                   ((((b1) & 0x3F) << 8) | (b2))
#define CLEAR_ROWOPERATION(b1)                  ((b1) & 0x3F)
#define INSERT_ROWOPERATION(b1,op)              ((b1) | (((op) && 0x3) << 6))

#define EXTRACT_PREVIEWROWOPERATION(b1)         (((b1) >> 6) & 0x3)
#define EXTRACT_PREVIEWROWID(b1,b2)             ((((b1) & 0xF) << 8) | (b2))
#define EXTRACT_PREVIEW_PART(b1)                (((b1) >> 4) & 0x3)
#define CLEAR_PREVIEWROWOPERATION(b1)           ((b1) & 0x3F)

typedef enum
{
    PREVIEWPART_0=0, PREVIEWPART_1=1
} PREVIEWPARTS;


typedef enum
{
    ROWOP_RESERVED, ROWOP_ADD, ROWOP_DELETE, ROWOP_CLEARALL
} ROWOPERATION;

// Mcast Service Package Entry - has G.988 Amd1 definition AND ALSO a CTC GPON 2.0 definition!!!!
// Different fields and different size structures
// G.988
#pragma pack(1)
typedef struct 
{
    UINT16 vid_uni;
    UINT16 maxSimultaneousGroups;
    UINT32 maxMulticastBandwidth;
    UINT16 mcastOperationsProfilePtr;
    UINT8  reserved[8];
} G988McastServicePackageEntry_S;
#pragma pack(0)

// CTC GPON 2.0
#pragma pack(1)
typedef struct 
{
    UINT16 vid_uni;
    UINT8  sourceIpAddress[V6_IPADDR_SIZE];
    UINT16 ctcMcastOperationsProfilePtr;
    UINT8  reserved[8];
} CtcMcastServicePackageEntry_S;
#pragma pack(0)


#pragma pack(1)
typedef struct 
{
    UINT8  tableControl[2];
    union 
    {
        G988McastServicePackageEntry_S g988;
        CtcMcastServicePackageEntry_S  ctc;
    } u;
} UMcastServicePackageEntry_S;
#pragma pack(0)


#pragma pack(1)
typedef struct 
{
    UINT8  srcIpAddress[16];
    UINT16 vlanIdAni;
    UINT16 vlanIdUni;
} RowPart0Format_S;

typedef struct 
{
    UINT8  destIpAddress[16];
    UINT16 duration;
    UINT16 timeLeft;
} RowPart1Format_S;

typedef struct 
{
    UINT8  tableControl[2];
    union 
    {
        RowPart0Format_S rowPart0Format;
        RowPart1Format_S rowPart1Format;
    } u;
} AllowedPreviewGroupsEntry_S;
#pragma pack(0)


// OMCI Entity DB

#define MAX_MCAST_SERVICE_ENTRIES               16
#define MAX_ALLOWED_PREVIEW_GROUP_ENTRIES       16
typedef struct
{
    MeInst_S                    meInst;
    UINT8                       meType;
    UINT16                      mcastOperationsProfilePtr;
    UINT16                      maxSimultaneousGroups;
    UINT32                      maxMulticastBandwidth;
    UINT8                       bandwidthEnforcement;
    UMcastServicePackageEntry_S mcastServicePackageAra[MAX_MCAST_SERVICE_ENTRIES];
    AllowedPreviewGroupsEntry_S allowedPreviewGroupsAra[MAX_ALLOWED_PREVIEW_GROUP_ENTRIES];

    // Fields for actual table size
    UINT32                      mcastServicePackageActualSize;
    bool                        mcastServicePackageEntryInUse[MAX_MCAST_SERVICE_ENTRIES];
    UINT32                      allowedPreviewGroupsActualSize;
    bool                        allowedPreviewGroupsEntryInUse[MAX_ALLOWED_PREVIEW_GROUP_ENTRIES];

    struct
    {
        UINT32  currentTicks;
        UMcastServicePackageEntry_S ara[MAX_MCAST_SERVICE_ENTRIES];
    } getNextBufMcastServicePackage;

    struct
    {
        UINT32  currentTicks;
        AllowedPreviewGroupsEntry_S ara[MAX_ALLOWED_PREVIEW_GROUP_ENTRIES];
    } getNextBufAllowedPreviewGroups;

} McastSubscriberConfigInfoME;



// messageContents - Create Multicast Subscriber Config Info Command
#pragma pack(1)
typedef struct
{
    UINT8      meType;
    UINT16     mcastOperationsProfilePtr;
    UINT16     maxSimultaneousGroups;
    UINT32     maxMulticastBandwidth;
    UINT8      bandwidthEnforcement;
} CreateMcastSubscriberConfigInfo_S;
#pragma pack(0)



MeClassDef_S mcastSubscriberConfigInfo_MeClassDef;

OMCIPROCSTATUS activateMcastSubscriberConfig(UINT16 sourceMeClass, UINT16 mcastSubscrInst);

extern UMcastServicePackageEntry_S *findFirstUsedMcastServicePackageEntry(McastSubscriberConfigInfoME *pmcastsub, int *araIndx);
extern UMcastServicePackageEntry_S *findNextUsedMcastServicePackageEntry (McastSubscriberConfigInfoME *pmcastsub, int *araIndx);
extern OMCIPROCSTATUS              checkForConfiguredL2Service(McastSubscriberConfigInfoME *pmcastsub, TPKEY *tpKey, char *buf);
extern OMCIPROCSTATUS              doMcastSubscriberConfigurations(McastSubscriberConfigInfoME  *pmcastsub);

#endif
