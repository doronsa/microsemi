/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : SipAgentMonitoringData.h                                  **/
/**                                                                          **/
/**  DESCRIPTION : This file has SIP Agent Monitoring Data definitions       **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCSipAgentMonitoringDatah
#define __INCSipAgentMonitoringDatah



// Sip Agent Monitoring Data Attribute bits
#define BM_MSB_sipagmon_intervalEndTime             (0X80)
#define BM_MSB_sipagmon_thresholdDataId             (0X40)
#define BM_MSB_transactions                         (0X20)
#define BM_MSB_rxInviteReqs                         (0X10)
#define BM_MSB_rxInviteRetrans                      (0X08)
#define BM_MSB_rxNonInviteReqs                      (0X04)
#define BM_MSB_rxNonInviteRetrans                   (0X02)
#define BM_MSB_rxResponse                           (0X01)

#define BM_LSB_rxResponseRetransmissions            (0X80)
#define BM_LSB_txInviteReqs                         (0X40)
#define BM_LSB_txInviteRetrans                      (0X20)
#define BM_LSB_txNonInviteReqs                      (0X10)
#define BM_LSB_txNonInviteRetrans                   (0X08)
#define BM_LSB_txResponse                           (0X04)
#define BM_LSB_txResponseRetransmissions            (0X02)


#define BM_MSB_GETATTRIBS_SIPAGENTMONITORINGDATA (BM_MSB_sipagmon_intervalEndTime | BM_MSB_sipagmon_thresholdDataId | BM_MSB_transactions | BM_MSB_rxInviteReqs | BM_MSB_rxInviteRetrans | BM_MSB_rxNonInviteReqs | BM_MSB_rxNonInviteRetrans | BM_MSB_rxResponse)

#define BM_LSB_GETATTRIBS_SIPAGENTMONITORINGDATA (BM_LSB_rxResponseRetransmissions | BM_LSB_txInviteReqs | BM_LSB_txInviteRetrans | BM_LSB_txNonInviteReqs | BM_LSB_txNonInviteRetrans | BM_LSB_txResponse | BM_LSB_txResponseRetransmissions)


#define BM_MSB_SETATTRIBS_SIPAGENTMONITORINGDATA (BM_MSB_sipagmon_thresholdDataId)

#define BM_MSB_CREATEATTRIBS_SIPAGENTMONITORINGDATA (BM_MSB_sipagmon_thresholdDataId)


// create message contents
#pragma pack(1)
typedef struct
{
    UINT16 thresholdDataId;
} CreateSipAgentMonitoringDataCmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    tcaBitMask[2];
    UINT8    intervalEndTime;
    UINT16   thresholdDataId;
    UINT32   transactions;
    UINT32   rxInviteReqs;
    UINT32   rxInviteRetrans;
    UINT32   rxNonInviteReqs;
    UINT32   rxNonInviteRetrans;
    UINT32   rxResponse;
    UINT32   rxResponseRetransmissions;
    UINT32   txInviteReqs;
    UINT32   txInviteRetrans;
    UINT32   txNonInviteReqs;
    UINT32   txNonInviteRetrans;
    UINT32   txResponse;
    UINT32   txResponseRetransmissions;
} SipAgentMonitoringDataME;


extern MeClassDef_S sipAgentMonitoringData_MeClassDef;


#endif
