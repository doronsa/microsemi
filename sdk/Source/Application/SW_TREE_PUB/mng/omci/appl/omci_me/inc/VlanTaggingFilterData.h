/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VlanTaggingFilterData.h                                   **/
/**                                                                          **/
/**  DESCRIPTION : This file implements Vlan Tagging Filter Data entity      **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    17Apr07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVlanTaggingFilterDatah
#define __INCVlanTaggingFilterDatah


#define NUM_FILTERVLANS                12


// Vlan Tagging Filter Data Attribute bits
#define BM_MSB_vlanFilterTable            (0x80)
#define BM_MSB_forwardOperation           (0x40)
#define BM_MSB_numberOfEntries            (0x20) 



#define BM_MSB_GETATTRIBS_VLANTAGGINGFILTERDATA (BM_MSB_vlanFilterTable | BM_MSB_forwardOperation | BM_MSB_numberOfEntries)
 
#define BM_MSB_SETATTRIBS_VLANTAGGINGFILTERDATA (BM_MSB_vlanFilterTable | BM_MSB_forwardOperation | BM_MSB_numberOfEntries)

#define BM_MSB_CREATEATTRIBS_VLANTAGGINGFILTERDATA (BM_MSB_vlanFilterTable | BM_MSB_forwardOperation | BM_MSB_numberOfEntries)
 

// OMCI Entity DB

typedef struct
{
    MeInst_S meInst;
    UINT16   vlanFilterTable[NUM_FILTERVLANS];
    UINT8    forwardOperation;
    UINT8    numberOfEntries;
} VlanTaggingFilterDataME;



// messageContents - Create InterWorking VCC TP Command
#pragma pack(1)
typedef struct
{
    UINT16 vlanFilterTable[NUM_FILTERVLANS];
    UINT8  forwardOperation;
    UINT8  numberOfEntries;
} CreateVlanTaggingFilterData_s;
#pragma pack(0)



extern MeClassDef_S     vlanTaggingFilterData_MeClassDef;


bool isBridgeFullyConfiguredForVlan(UINT16 mbpcdInstance, char *buf);

#endif
