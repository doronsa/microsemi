/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OltG.h                                                    **/
/**                                                                          **/
/**  DESCRIPTION : This file contains OLT-G definitions and prototypes       **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    3Jan11     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCOltGh
#define __INCOltGh


// OLT-G Attribute bits
#define BM_MSB_oltg_vendorId                          (0x80)
#define BM_MSB_oltg_equipmentId                       (0x40)
#define BM_MSB_oltg_version                          (0x20)
#define BM_MSB_timeOfDayInformation                 (0x10)



#define BM_MSB_GETATTRIBS_OLTG (BM_MSB_oltg_vendorId | BM_MSB_oltg_equipmentId | BM_MSB_oltg_version | BM_MSB_timeOfDayInformation)

#define BM_MSB_SETATTRIBS_OLTG (BM_MSB_oltg_vendorId | BM_MSB_oltg_equipmentId | BM_MSB_oltg_version | BM_MSB_timeOfDayInformation)


// Used in MIB Upload snapshot 
#define OLTG_UPLOAD_CELL1_MSB_MASK (BM_MSB_oltg_vendorId | BM_MSB_oltg_equipmentId)
#define OLTG_UPLOAD_CELL2_MSB_MASK (BM_MSB_oltg_version)
#define OLTG_UPLOAD_CELL3_MSB_MASK (BM_MSB_timeOfDayInformation)


// OMCI Entity DB
#define OLTVENDORID_LEN               (4)
#define OLTEQUIPMENTID_LEN            (20)
#define OLT_VERSION_LEN               (14)
#define TIMEOFDAYINFORMATION_LEN      (14)

typedef struct OltGME_tag
{
    MeInst_S meInst;
    UINT8    vendorId[OLTVENDORID_LEN];         
    UINT8    equipmentId[OLTEQUIPMENTID_LEN];
    UINT8    version[OLT_VERSION_LEN];
    UINT8    timeOfDayInformation[TIMEOFDAYINFORMATION_LEN];
} OltGME;


OMCIPROCSTATUS createOltGME(UINT16 meInstance);


MeClassDef_S oltG_MeClassDef;


#endif
