/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : TcontG.h                                                  **/
/**                                                                          **/
/**  DESCRIPTION : This file has TCONT-G definitions                         **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCTcontGh
#define __INCTcontGh



// TCONT Attribute bits
#define BM_MSB_allocId                    (0X80)
#define BM_MSB_modeIndicator              (0X40)
#define BM_MSB_policy                     (0X20)


#define BM_MSB_GETATTRIBS_TCONTG BM_MSB_allocId | BM_MSB_modeIndicator | BM_MSB_policy

#define BM_MSB_SETATTRIBS_TCONTG BM_MSB_allocId | BM_MSB_policy


#define MAX_ALLOCID                       4095
#define MIN_HIGHRANGE_ALLOCID             256
#define MAX_LOWRANGE_ALLOCID              253

// The following is the default value - illegal Alloc-Id
#define DEF_UNDEFINED_ALLOCID             255

// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT16   allocId;
    UINT8    modeIndicator;
    UINT8    policy;
    UINT8    internalTcontNumber;
} TcontGME;


typedef struct
{
    UINT16   allocId;
    UINT8    modeIndicator;
    UINT8    policy;
    UINT8    internalTcontNumber;
} CreateTcontG_s;


extern OMCIPROCSTATUS createTcontGME(UINT16 meInstance, CreateTcontG_s *pCreateTcontG);
extern void           refreshTcontAllocIdSettings();


MeClassDef_S tcontG_MeClassDef;


#endif
