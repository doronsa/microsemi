/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : VoIPFeatureAccessCodes.h                                  **/
/**                                                                          **/
/**  DESCRIPTION : This file has VoIP Feature Access Codes definitions       **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCVoIPFeatureAccessCodesh
#define __INCVoIPFeatureAccessCodesh


// VoIP Feature Access Codes Attribute bits
#define BM_MSB_cancelCallWaiting          (0x80)
#define BM_MSB_callHold                   (0x40) 
#define BM_MSB_callPark                   (0x20)
#define BM_MSB_cidsActivate               (0x10)
#define BM_MSB_cidsDeactivate             (0x08)
#define BM_MSB_doNotDisturbActivation     (0x04)
#define BM_MSB_doNotDisturbDeactivation   (0x02)
#define BM_MSB_doNotDisturbPinChange      (0x01)
#define BM_LSB_emergencyServiceNumber     (0x80)
#define BM_LSB_intercomService            (0x40) 
#define BM_LSB_blindCallTransfer          (0x20) 
#define BM_LSB_attendedCallTransfer       (0x10) 


#define BM_MSB_GETATTRIBS_VOIPFEATUREACCESSCODES (BM_MSB_cancelCallWaiting | BM_MSB_callHold | BM_MSB_callPark | BM_MSB_cidsActivate | BM_MSB_cidsDeactivate |  BM_MSB_doNotDisturbActivation | BM_MSB_doNotDisturbDeactivation | BM_MSB_doNotDisturbPinChange)
#define BM_LSB_GETATTRIBS_VOIPFEATUREACCESSCODES (BM_LSB_emergencyServiceNumber | BM_LSB_intercomService | BM_LSB_blindCallTransfer | BM_LSB_attendedCallTransfer)

#define BM_MSB_SETATTRIBS_VOIPFEATUREACCESSCODES (BM_MSB_cancelCallWaiting | BM_MSB_callHold | BM_MSB_callPark | BM_MSB_cidsActivate | BM_MSB_cidsDeactivate |  BM_MSB_doNotDisturbActivation | BM_MSB_doNotDisturbDeactivation | BM_MSB_doNotDisturbPinChange)
#define BM_LSB_SETATTRIBS_VOIPFEATUREACCESSCODES (BM_LSB_emergencyServiceNumber | BM_LSB_intercomService | BM_LSB_blindCallTransfer | BM_LSB_attendedCallTransfer)

// Used in MIB Upload snapshot 
#define VOIPFEATUREACCESSCODES_UPLOAD_CELL1_MSB_MASK (BM_MSB_cancelCallWaiting | BM_MSB_callHold | BM_MSB_callPark | BM_MSB_cidsActivate | BM_MSB_cidsDeactivate)

#define VOIPFEATUREACCESSCODES_UPLOAD_CELL2_MSB_MASK (BM_MSB_doNotDisturbActivation | BM_MSB_doNotDisturbDeactivation | BM_MSB_doNotDisturbPinChange)
#define VOIPFEATUREACCESSCODES_UPLOAD_CELL2_LSB_MASK (BM_LSB_emergencyServiceNumber | BM_LSB_intercomService )

#define VOIPFEATUREACCESSCODES_UPLOAD_CELL3_LSB_MASK (BM_LSB_blindCallTransfer | BM_LSB_attendedCallTransfer)



#define AXCODE_SIZE      5
typedef struct
{
    MeInst_S meInst;
    UINT8    cancelCallWaiting[AXCODE_SIZE];
    UINT8    callHold[AXCODE_SIZE];
    UINT8    callPark[AXCODE_SIZE];
    UINT8    cidsActivate[AXCODE_SIZE];
    UINT8    cidsDeactivate[AXCODE_SIZE];
    UINT8    doNotDisturbActivation[AXCODE_SIZE];
    UINT8    doNotDisturbDeactivation[AXCODE_SIZE];
    UINT8    doNotDisturbPinChange[AXCODE_SIZE];
    UINT8    emergencyServiceNumber[AXCODE_SIZE];
    UINT8    intercomService[AXCODE_SIZE];
    UINT8    blindCallTransfer[AXCODE_SIZE];
    UINT8    attendedCallTransfer[AXCODE_SIZE];
} VoIPFeatureAccessCodesME;


MeClassDef_S voIPFeatureAccessCodes_MeClassDef;


#endif
