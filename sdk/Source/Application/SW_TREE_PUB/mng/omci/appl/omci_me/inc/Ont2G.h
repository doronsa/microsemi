/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : Ont2G.h                                                   **/
/**                                                                          **/
/**  DESCRIPTION : This file implements ONTT2-G ME                           **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCOnt2Gh
#define __INCOnt2Gh


// ONT2-G Attribute bits
#define BM_MSB_equipmentId                          (0x80)
#define BM_MSB_omccVersion                          (0x40)
#define BM_MSB_vendorProductCode                    (0x20)
#define BM_MSB_securityCapability                   (0x10)
#define BM_MSB_securityMode                         (0x08)
#define BM_MSB_totalPriorityQueueNumber             (0x04)
#define BM_MSB_totalTrafficSchedulerNumber          (0x02)
#define BM_MSB_mode                                 (0x01)

#define BM_LSB_totalGEMPortIDNumber                 (0x80)
#define BM_LSB_sysUpTime                            (0x40)
#define BM_LSB_connectivityCapability               (0x20)
#define BM_LSB_currentConnectivityMode              (0x10)
#define BM_LSB_qosConfigurationFlexibility          (0x08)
#define BM_LSB_priorityQueueScaleFactor             (0x04)


#define BM_MSB_GETATTRIBS_ONT2G  (BM_MSB_equipmentId | BM_MSB_omccVersion | BM_MSB_vendorProductCode | BM_MSB_securityCapability | BM_MSB_securityMode | BM_MSB_totalPriorityQueueNumber | BM_MSB_totalTrafficSchedulerNumber | BM_MSB_mode)
#define BM_LSB_GETATTRIBS_ONT2G  (BM_LSB_totalGEMPortIDNumber | BM_LSB_sysUpTime | BM_LSB_connectivityCapability | BM_LSB_currentConnectivityMode | BM_LSB_qosConfigurationFlexibility | BM_LSB_priorityQueueScaleFactor)


#define BM_MSB_SETATTRIBS_ONT2G  BM_MSB_securityMode
#define BM_LSB_SETATTRIBS_ONT2G  (BM_LSB_currentConnectivityMode | BM_LSB_priorityQueueScaleFactor)


// Used in MIB Upload snapshot 
#define ONT2G_UPLOAD_CELL1_MSB_MASK (BM_MSB_omccVersion | BM_MSB_vendorProductCode | BM_MSB_securityCapability | BM_MSB_securityMode | BM_MSB_totalPriorityQueueNumber | BM_MSB_totalTrafficSchedulerNumber | BM_MSB_mode)
#define ONT2G_UPLOAD_CELL1_LSB_MASK (BM_LSB_connectivityCapability | BM_LSB_currentConnectivityMode | BM_LSB_qosConfigurationFlexibility | BM_LSB_sysUpTime) 

#define ONT2G_UPLOAD_CELL2_MSB_MASK (BM_MSB_equipmentId) 
#define ONT2G_UPLOAD_CELL2_LSB_MASK (BM_LSB_totalGEMPortIDNumber | BM_LSB_priorityQueueScaleFactor) 

// The only valid value according to G.984.4 is 0x80
#define OMCIVERSION               0x86

#define DEFAULT_CONNECTIVITY_CAPABILITY             (0x7F)

#define CONNECTIVITYCAPABILITYARA_SIZE              (2)
#define QOSCONFIGFLEXIBILITY_SIZE                   (2)

// OMCI Entity DB

typedef struct Ont2GME_tag
{
    MeInst_S meInst;
    UINT8    equipmentId[20];
    UINT8    omccVersion;
    UINT8    vendorProductCode[2];
    UINT8    securityCapability;
    UINT8    securityMode;
    UINT16   totalPriorityQueueNumber;
    UINT8    totalTrafficSchedulerNumber;
    UINT8    mode;
    UINT16   totalGEMPortIDNumber;
    UINT32   sysUpTime;
    UINT8    connectivityCapability[CONNECTIVITYCAPABILITYARA_SIZE];     
    UINT8    currentConnectivityMode;    
    UINT8    qosConfigurationFlexibility[QOSCONFIGFLEXIBILITY_SIZE];
    UINT16   priorityQueueScaleFactor;
} Ont2GME;


OMCIPROCSTATUS createOnt2GME(UINT16 meInstance);


MeClassDef_S ont2G_MeClassDef;

#endif
