/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : GemPortPmHistoryData.h                                    **/
/**                                                                          **/
/**  DESCRIPTION : This file implements GEM Port PM History Data ME          **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCGemPortPmHistoryDatah
#define __INCGemPortPmHistoryDatah



// GEM Port PM History Data Attribute bits
#define BM_MSB_gemPort_intervalEndTime              (0X80)
#define BM_MSB_gemPort_thresholdData12Id            (0X40)
#define BM_MSB_lostPackets                          (0x20)
#define BM_MSB_misInsertedpackets                   (0x10)
#define BM_MSB_receivedPackets                      (0x08)
#define BM_MSB_receivedBlocks                       (0x04)
#define BM_MSB_transmittedBlocks                    (0x02)
#define BM_MSB_impairedBlocks                       (0x01)

#define BM_MSB_GETATTRIBS_GEMPORTPMHISTORYDATA BM_MSB_gemPort_intervalEndTime | BM_MSB_gemPort_thresholdData12Id | BM_MSB_lostPackets | BM_MSB_misInsertedpackets | BM_MSB_receivedPackets | BM_MSB_receivedBlocks | BM_MSB_transmittedBlocks | BM_MSB_impairedBlocks

#define BM_MSB_SETATTRIBS_GEMPORTPMHISTORYDATA BM_MSB_gemPort_thresholdData12Id

#define BM_MSB_CREATEATTRIBS_GEMPORTPMHISTORYDATA BM_MSB_gemPort_thresholdData12Id


// messageContents - GemvPort Prot Mon HistoryData Create Command
#pragma pack(1)
typedef struct
{
    UINT16 thresholdData12Id;
} CreateGemPortPmHistoryDataCmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    tcaBitMask[2];
    UINT8    intervalEndTime;
    UINT16   thresholdData12Id;
    UINT32   lostPackets;
    UINT32   misInsertedpackets;
    UINT32   receivedPackets;
    UINT32   receivedBlocks;
    UINT32   transmittedBlocks;
    UINT32   impairedBlocks;
} GemPortPmHistoryDataME;


extern MeClassDef_S gemPortPmHistoryData_MeClassDef; 


#endif
