/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : IpHostPmHistoryData.h                                     **/
/**                                                                          **/
/**  DESCRIPTION : This file implements IP Host PM History Data ME           **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *                                                                      
 ******************************************************************************/

#ifndef __INCIpHostPmHistoryDatah
#define __INCIpHostPmHistoryDatah



// IpHost PM History Data Attribute bits
#define BM_MSB_iphost_intervalEndTime               (0X80)
#define BM_MSB_iphost_thresholdDataId               (0X40)
#define BM_MSB_icmpErrors                           (0X20)
#define BM_MSB_dnsErrors                            (0X10)

#define BM_MSB_GETATTRIBS_IPHOSTPMHISTORYDATA BM_MSB_iphost_intervalEndTime | BM_MSB_iphost_thresholdDataId | BM_MSB_icmpErrors | BM_MSB_dnsErrors

#define BM_MSB_SETATTRIBS_IPHOSTPMHISTORYDATA BM_MSB_iphost_thresholdDataId

#define BM_MSB_CREATEATTRIBS_IPHOSTPMHISTORYDATA BM_MSB_iphost_thresholdDataId


// messageContents - AAL5 Prot Mon Create Command
#pragma pack(1)
typedef struct
{
    UINT16 thresholdDataId;
} CreateIpHostPmHistoryDataCmd_s;
#pragma pack(0)



// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT8    tcaBitMask[2];
    UINT8    intervalEndTime;
    UINT16   thresholdDataId;
    UINT32   icmpErrors;
    UINT32   dnsErrors;
} IpHostPmHistoryDataME;


extern MeClassDef_S ipHostPmHistoryData_MeClassDef;


#endif
