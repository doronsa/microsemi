/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : SipConfigPortal.h                                         **/
/**                                                                          **/
/**  DESCRIPTION : This file has SIP Config Portal definitions               **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    16Aug07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCSipConfigPortalh
#define __INCSipConfigPortalh


// SIP Config Portal Attribute bits
#define BM_MSB_configText                  (0x80)


#define BM_MSB_GETATTRIBS_SIPCONFIGPORTAL BM_MSB_configText


// OMCI Entity DB

#define CONFIGTEXT_SIZE                    500
typedef struct
{
    MeInst_S meInst;
    UINT32  getRespSize;   // The following is the response to a Get command
    struct
    {
        UINT32  currentTicks;
        UINT8   configText[CONFIGTEXT_SIZE];
    } getNextBuf;
} SipConfigPortalME;



OMCIPROCSTATUS createSipConfigPortalME(UINT16 meInstance);

MeClassDef_S sipConfigPortal_MeClassDef;


#endif
