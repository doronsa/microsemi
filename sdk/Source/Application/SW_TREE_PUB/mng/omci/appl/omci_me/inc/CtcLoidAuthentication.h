/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : CtcLoidAuthentication.h                                   **/
/**                                                                          **/
/**  DESCRIPTION : This file contains CTL LOID Authentication definitions    **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    8Aug11     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCCtcLoidAuthenticationh
#define __INCCtcLoidAuthenticationh


// CTC LOID Authentication Attribute bits
#define BM_MSB_auth_operatorId                      (0x80)
#define BM_MSB_auth_loid                            (0x40)
#define BM_MSB_auth_password                        (0x20)
#define BM_MSB_auth_status                          (0x10)



#define BM_MSB_GETATTRIBS_LOIDAUTH (BM_MSB_auth_operatorId | BM_MSB_auth_loid | BM_MSB_auth_password | BM_MSB_auth_status)

/* The attribute types of operationId, auth_loid, auth_password are changed to "RW" to support set these parameters by local CLI */
#define BM_MSB_SETATTRIBS_LOIDAUTH (BM_MSB_auth_operatorId | BM_MSB_auth_loid | BM_MSB_auth_password | BM_MSB_auth_status)


// Used in MIB Upload snapshot 
#define LOIDAUTH_UPLOAD_CELL1_MSB_MASK (BM_MSB_auth_operatorId | BM_MSB_auth_password | BM_MSB_auth_status)
#define LOIDAUTH_UPLOAD_CELL2_MSB_MASK (BM_MSB_auth_loid)

/* LOID authentication state */
typedef enum
{
    LOID_AUTH_STATE_INIT,      
    LOID_AUTH_STATE_SUCCESS, 
    LOID_AUTH_STATE_LOID_NOT_EXIST, 
    LOID_AUTH_STATE_PWD_WRONG,  
    LOID_AUTH_STATE_LOID_CONFLICT,      
} LOID_AUTH_STATE_E;

// OMCI Entity DB
#define LOIDAUTH_OPERATORID_LEN                (4)
#define LOIDAUTH_LOID_LEN                      (24)
#define LOIDAUTH_PASSWORD_LEN                  (12)

typedef struct 
{
    MeInst_S meInst;
    UINT8    operatorId[LOIDAUTH_OPERATORID_LEN];
    UINT8    loid[LOIDAUTH_LOID_LEN];
    UINT8    password[LOIDAUTH_PASSWORD_LEN];
    UINT8    status;
} CtcLoidAuthenticationME;

typedef struct 
{
    UINT8    operatorId[LOIDAUTH_OPERATORID_LEN];
    UINT8    loid[LOIDAUTH_LOID_LEN];
    UINT8    password[LOIDAUTH_PASSWORD_LEN];
    UINT8    status;
} CreateCtcLoidAuthentication_S;


OMCIPROCSTATUS createCtcLoidAuthenticationME(UINT16 meInstance, CreateCtcLoidAuthentication_S *pcreate);

MeClassDef_S ctcLoidAuthentication_MeClassDef;


#endif
