/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciMe.h                                                  **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the OMCI ME                          **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    9Mar08      zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCOmciMeh
#define __INCOmciMeh



// MOCI ME Attribute bits
#define BM_MSB_meTypeTable                          (0X80)
#define BM_MSB_messageTypeTable                     (0X40)

#define BM_MSB_GETATTRIBS_OMCIME (BM_MSB_meTypeTable | BM_MSB_messageTypeTable)


#define METYPETABLE_SIZE                  120

// OMCI Entity DB
typedef struct
{
    MeInst_S meInst;
    UINT32   meTypeTableSize;
    UINT32   messageTypeTableSize;
    UINT8    currGetNextAttributeMask;
} OmciMe;


extern MeClassDef_S omciMe_MeClassDef;

OMCIPROCSTATUS createOmciMe(UINT16 meInstance);


#endif
