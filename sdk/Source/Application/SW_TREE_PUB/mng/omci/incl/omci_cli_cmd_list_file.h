/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : omci_cli_cmd_list_file.h                                  **/
/**                                                                          **/
/**  DESCRIPTION : This file has the CLISH action/routine matchup            **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 * 
 ******************************************************************************/

/********************************************************************************/
/*                            OMCI debug CLI functions                          */
/********************************************************************************/
  {"omci_cli_debug_set_frame_dump",          Omci_cli_debug_set_frame_dump},
  {"omci_cli_debug_create_set_dump",         Omci_cli_debug_create_set_dump},
  {"omci_cli_debug_brief_frame_dump"         ,Omci_cli_debug_brief_frame_dump},
  {"omci_cli_debug_memory_use_dump",         Omci_cli_debug_memory_use_dump},
  {"omci_cli_debug_serv_ctrl_dump",          Omci_cli_debug_serv_ctrl_dump},
  {"omci_cli_debug_mib_upload",              Omci_cli_debug_mib_upload},
  {"omci_cli_debug_download",                Omci_cli_debug_download},
  {"omci_cli_debug_omci_tl_display",         Omci_cli_debug_omci_tl_display},
  {"omci_cli_debug_no_hw_calls",             Omci_cli_debug_no_hw_calls},
  {"omci_cli_debug_answer_ok_to_all",        Omci_cli_debug_answer_ok_to_all},
  {"omci_cli_debug_out_of_order_me_create",  Omci_cli_debug_out_of_order_me_create},
  {"omci_cli_debug_zero_mapper_iwtpptr",     Omci_cli_debug_zero_mapper_iwtpptr},
  {"omci_cli_debug_all_debug",               Omci_cli_debug_all_debug},
  {"omci_cli_debug_sw_download",             Omci_cli_debug_sw_download},
  {"omci_cli_debug_sw_window_size",          Omci_cli_debug_sw_window_size},
  {"omci_cli_show_test_seq",                 Omci_cli_show_test_seq},
  {"omci_cli_show_test",                     Omci_cli_show_test},
  {"omci_cli_show_debug",                    Omci_cli_show_debug},
  {"omci_cli_show_os_resources",             Omci_cli_show_os_resources},
  {"omci_cli_show_download",                 Omci_cli_show_download},
  {"omci_cli_show_brief_download",           Omci_cli_show_brief_download},
  {"omci_cli_show_mib_snapshot",             Omci_cli_show_mib_snapshot},
  {"omci_cli_show_alarm_snapshot",           Omci_cli_show_alarm_snapshot},
  {"omci_cli_show_test_event",               Omci_cli_show_test_event},
  {"omci_cli_set_voip",                      Omci_cli_set_voip},
  {"omci_cli_set_voip_blm",                  Omci_cli_set_voip_blm},
/********************************************************************************/
/*                            OMCI show/debug CLI functions                     */
/********************************************************************************/
  {"omci_cli_show_db_entity",                Omci_cli_show_db_entity},
  {"omci_cli_show_db_service",               Omci_cli_show_db_service},
  {"omci_cli_show_entity",                   Omci_cli_show_entity},
  {"omci_cli_show_service",                  Omci_cli_show_service},
  {"omci_cli_show_l2_data_service",          Omci_cli_show_l2_data_service},
  {"omci_cli_show_bridge_summary",           Omci_cli_show_bridge_summary},
  {"omci_cli_show_profile_cache",            Omci_cli_show_profile_cache},
  {"omci_cli_show_statistics",               Omci_cli_show_statistics},
  {"omci_cli_show_supported_mes",            Omci_cli_show_supported_mes},
  {"omci_cli_sim_omci_frame",                Omci_cli_sim_omci_frame},
  {"omci_cli_show_gpon_info",                Omci_cli_show_gpon_info},
/********************************************************************************/
/*                            L2 OMCI  CLI functions                            */
/********************************************************************************/
  {"l2omci_cli_print_me_class_info",         L2omci_cli_print_me_class_info},
  {"l2omci_cli_create_me",                   L2omci_cli_create_me},
  {"l2omci_cli_set_me",                      L2omci_cli_set_me},
  {"l2omci_cli_delete_me",                   L2omci_cli_delete_me},
  {"l2omci_cli_display_me",                  L2omci_cli_display_me},
  {"l2omci_cli_get_me",                      L2omci_cli_get_me},
  {"l2omci_cli_get_current_me",              L2omci_cli_get_current_me},
  {"l2omci_cli_mib_reset",                   L2omci_cli_mib_reset},
  {"l2omci_cli_add_ext_tagging_op",          L2omci_cli_add_ext_tagging_op},
  {"l2omci_cli_delete_ext_tagging_op",       L2omci_cli_delete_ext_tagging_op},
  {"l2omci_cli_set_vlan_filters",            L2omci_cli_set_vlan_filters},
  {"l2omci_cli_display_flows",               L2omci_cli_display_flows},
  {"l2omci_cli_display_pnc_flows",           L2omci_cli_display_pnc_flows},  
  {"l2omci_cli_display_cust_rule_map",       L2omci_cli_display_cust_rule_map},  
  {"l2omci_cli_full_add_ext_tagging_op",     L2omci_cli_full_add_ext_tagging_op},
  {"l2omci_cli_full_delete_ext_tagging_op",  L2omci_cli_full_delete_ext_tagging_op},
/********************************************************************************/
/*                            OMCI  TL CLI functions                            */
/********************************************************************************/
  {"tlomci_cli_cfg_untaggedVlanOp",          Tlomci_cli_cfg_untaggedVlanOp},
  {"tlomci_cli_cfg_taggedVlanOp",            Tlomci_cli_cfg_taggedVlanOp},
  {"tlomci_cli_cfg_doubleTaggedVlanOp",      Tlomci_cli_cfg_doubleTaggedVlanOp},
  {"tlomci_cli_cfg_vlanFilter",              Tlomci_cli_cfg_vlanFilter},
  {"tlomci_cli_cfg_wanInfo",                 Tlomci_cli_cfg_wanInfo},
  {"tlomci_cli_cfg_lanInfo",                 Tlomci_cli_cfg_lanInfo},
  {"tlomci_cli_delete_table_entry",          Tlomci_cli_delete_table_entry},
  {"tlomci_cli_display_table_entry",         Tlomci_cli_display_table_entry},
  {"tlomci_cli_set_wan",                     Tlomci_cli_set_wan},
  {"tlomci_cli_set_lan",                     Tlomci_cli_set_lan},
  {"tlomci_cli_service",                     Tlomci_cli_service},


