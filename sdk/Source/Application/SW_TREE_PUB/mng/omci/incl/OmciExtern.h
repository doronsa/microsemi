/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciExtern.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file is the External OMCI include file               **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *    22Feb04     zeev  - initial version created.                            *
 * ========================================================================== *
 *                                                                          
 *   $Log:   P:/work/archives/Projects/ONT/ONT/Common/ExtInc/OmciExtern.h-arc  $   
 * 
 *    Rev 1.0   May 19 2008 15:25:38   oren
 * Initial revision.
 * 
 ******************************************************************************/

#ifndef __INCOmciExternh
#define __INCOmciExternh
#include <stdbool.h>


#define OMCI_AVC_VALUE_LENGTH       (28)
#define OMCI_PM_COUNTERS_MAX_NUM    (16)

// messageContents - Test results response
#pragma pack(1)
typedef struct 
{
    UINT8   mltDropTestResult;
    UINT8   selfTestResult;
    UINT8   dialToneMakeBreakFlags;
    UINT8   dialTonePowerFlags;
    UINT8   loopTestDcVoltageFlags;
    UINT8   loopTestAcVoltageFlags;
    UINT8   loopTestResistanceFlags1;
    UINT8   loopTestResistanceFlags2;
    UINT8   timeToDrawDialTone;
    UINT8   timeToBreakDialTone;
    UINT8   totalDialTonePowerMeasurement;
    UINT8   quietChannelPowerMeasurement;
    UINT16  tipGroundDcVoltage;
    UINT16  ringGroundDcVoltage;
    UINT8   tipGroundAcVoltage;
    UINT8   ringGroundAcVoltage;
    UINT16  tipGroundDcResistance;
    UINT16  ringGroundDcResistance;
    UINT16  tipRingDcResistance;
    UINT8   ringerEquivalence;
    UINT8   padding[7];
} TestPotsUniResultsResp_s;
#pragma pack(0)

// messageContents - Test results response
#pragma pack(1)
typedef struct 
{
    UINT8   type1;
    UINT16  powerFeedVoltage;
    UINT8   type3;
    UINT16  receivedOpticalPower;
    UINT8   type5;
    UINT16  MeanOpticalPower;
    UINT8   type9;
    UINT16  laserBiasCurrent;
    UINT8   type12;
    UINT16  temperatureDegrees;
    UINT8   padding[17];
} TestAnigResultsResp_s;
#pragma pack(0)

//midware report apm alarm message
typedef struct
{
    UINT32  alarm_type;
    UINT32  param1;         /*apm index such as slot/port*/
    UINT32  param2;         /*omci class+instanceid */
    UINT8   alarm_class;    /*alarm(0) tca(1) avc(2) not used*/
    UINT8   state;
} OMCI_REPORT_ALARM_MSG_S;

//midware report apm avc message
typedef struct
{
    UINT32  avc_type;
    UINT32  param1; /*apm index such as slot/port*/
    UINT32  param2; /*omci class+instanceid */
    UINT8   value[OMCI_AVC_VALUE_LENGTH];
} OMCI_REPORT_AVC_MSG_S;

//midware report apm pm message
typedef struct
{
    UINT32  pm_type;
    UINT32  param1; /*apm index such as slot/port*/
    UINT32  param2; /*omci class+instanceid */
    UINT32  pmData[OMCI_PM_COUNTERS_MAX_NUM];
} OMCI_REPORT_PM_MSG_S;


/* Prototypes
------------------------------------------------------------------------------*/

// Used when VoIP subsystem is ready
void sendVoIPReadyEvent();

/*the following 3 ipc functions are called by midware tor eport alarm, avc, pm message to omci*/
extern bool omci_report_alarm(OMCI_REPORT_ALARM_MSG_S *alarmMsg);
extern bool omci_report_avc(OMCI_REPORT_AVC_MSG_S *avcMsg);
extern bool omci_report_pm(OMCI_REPORT_PM_MSG_S *pmMsg);

#endif /* __INCOmciExternh */

