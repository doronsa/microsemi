/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : omci_cli.h                                                **/
/**                                                                          **/
/**  DESCRIPTION : This file has the CLISH routines                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 * 
 ******************************************************************************/

#ifndef __OMCI_CLI_H__
#define __OMCI_CLI_H__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"


/********************************************************************************/
/*                            OMCI debug CLI functions                          */
/********************************************************************************/
bool_t Omci_cli_debug_set_frame_dump          (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_create_set_dump         (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_brief_frame_dump        (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_memory_use_dump         (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_serv_ctrl_dump          (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_mib_upload              (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_download                (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_omci_tl_display         (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_no_hw_calls             (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_answer_ok_to_all        (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_all_debug               (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_out_of_order_me_create  (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_zero_mapper_iwtpptr     (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_sw_download             (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_debug_sw_window_size          (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_show_test_seq                 (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_show_test                     (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_show_debug                    (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_show_os_resources             (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_show_download                 (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_show_brief_download           (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_show_mib_snapshot             (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_show_alarm_snapshot           (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_show_test_event               (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_set_voip                      (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t Omci_cli_set_voip_blm                  (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
/********************************************************************************/
/*                            OMCI show CLI functions                           */
/********************************************************************************/
bool_t Omci_cli_show_db_entity              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Omci_cli_show_db_service             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Omci_cli_show_entity                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Omci_cli_show_service                (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Omci_cli_show_l2_data_service        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Omci_cli_show_bridge_summary         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Omci_cli_show_profile_cache          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Omci_cli_show_statistics             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Omci_cli_show_supported_mes          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Omci_cli_sim_omci_frame              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Omci_cli_show_gpon_info              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                            L2 OMCI CLI functions                             */
/********************************************************************************/
bool_t L2omci_cli_print_me_class_info       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_create_me                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_set_me                    (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_delete_me                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_display_me                (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_get_me                    (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_get_current_me            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

bool_t L2omci_cli_mib_reset                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_add_ext_tagging_op        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_delete_ext_tagging_op     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_set_vlan_filters          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_display_flows             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_display_pnc_flows         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);                                             
bool_t L2omci_cli_display_cust_rule_map     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);                                             
bool_t L2omci_cli_full_add_ext_tagging_op   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t L2omci_cli_full_delete_ext_tagging_op(const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                            OMCI TL CLI functions                             */
/********************************************************************************/
bool_t Tlomci_cli_cfg_untaggedVlanOp        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Tlomci_cli_cfg_taggedVlanOp          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Tlomci_cli_cfg_doubleTaggedVlanOp    (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Tlomci_cli_cfg_vlanFilter            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Tlomci_cli_cfg_wanInfo               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Tlomci_cli_cfg_lanInfo               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Tlomci_cli_delete_table_entry        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Tlomci_cli_display_table_entry       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Tlomci_cli_set_wan                   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Tlomci_cli_set_lan                   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t Tlomci_cli_service                   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);


#endif
