/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : OmciDatapathAdapter.h                                     **/
/**                                                                          **/
/**  DESCRIPTION : This file conatins the OMCI Datapath Adapter API          **/
/**                                                                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    31Dec07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/



#ifndef __INCOmciDatapathAdapterh
#define __INCOmciDatapathAdapterh


typedef enum 
{
    L2DAPROC_ERROR, L2DAPROC_DONE
} L2DAPROCSTATUS;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  typedef enums 
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#define PBITS_MASK                         (0xFF)
#define VID_MASK                           (0xFFFF)

// Number of VLANs per OMCI Datapath Adapter ANI/UNI TP; set to OMCI capability = 12
#define NUM_ODA_TPFILTERVLANS                12


// Definition of SERVICEKEY
typedef  unsigned int SERVICEKEY;

// Useful macro for generating partition ID
#define MAKE_SERVICEKEY(cl,inst)  (((cl & 0xFFFF) << 16) | ((inst) & 0xFFFF))

// This macro cleans gives a T-CONT/Queue combo = assume for now 4 queues per T-CONT
#define MAKE_USQTONT(tcont,usQ)   (((tcont & 0xFF) << 16) | ((usQ) & 0xFFFF))
#define EXTRACT_TCONT(usqTcont)   (((usqTcont) >> 16) & 0xFF)
#define EXTRACT_USQ(usqTcont)     ((usqTcont) & 0xFFFF)
 
 
#define ETHERTYPE_IPOE_V4                  (0x800)
#define ETHERTYPE_ARP                      (0x806)
#define ETHERTYPE_IPOE_V6                  (0x86DD)
#define ETHERTYPE_PPPOE_63                 (0x8863)
#define ETHERTYPE_PPPOE_64                 (0x8864)

typedef enum 
{
    CASTTYPE_BIDI, CASTTYPE_BROADCAST, CASTTYPE_MCAST, CASTTYPE_UNDEFINED=-1
} CASTTYPE;


// Definition of TPKEY
typedef  unsigned int TPKEY;

// Macro for construction of TPKEY - from TP class and TP instance
#define MAKE_TPKEY(tpClass,tpInst) ((((tpClass) & 0xFFFF) << 16) | ((tpInst) & 0xFFFF))
#define EXTRACT_TPCLASS(tpkey)     (((tpkey) >> 16) & 0xFFFF)
#define EXTRACT_TPINST(tpkey)      ((tpkey) & 0xFFFF)


// ANI side TP classes. Note that enum values match ITU-T G.984.4
typedef enum
{
    ANICLASS_MBPCD=47, ANICLASS_MAPPER=130, ANICLASS_MCASTGEMIWTP=281
} ANITPCLASS;


// UNI side TP classes. Note that enum values match ITU-T G.984.4
typedef enum
{
    UNICLASS_ETHERPPTP=11, UNICLASS_IPHOST=134, UNICLASS_VEIP=329
} UNITPCLASS;

// ANI/UNI side differentiation
#define isTpKeyUni(tpKey)      (((EXTRACT_TPCLASS(tpKey) == UNICLASS_ETHERPPTP) || (EXTRACT_TPCLASS(tpKey) == UNICLASS_IPHOST)  || (EXTRACT_TPCLASS(tpKey) == UNICLASS_VEIP)) ? true : false)
#define isTpKeyVoice(tpKey)    ((EXTRACT_TPCLASS(tpKey) == UNICLASS_IPHOST) ? true : false)
#define isTpKeyMapper(tpKey)   ((EXTRACT_TPCLASS(tpKey) == ANICLASS_MAPPER) ? true : false)
#define isTpKeyVeip(tpKey)     ((EXTRACT_TPCLASS(tpKey) == UNICLASS_VEIP) ? true : false)

typedef enum
{
    SERVICEMODE_INDIFFERENT, SERVICEMODE_FILTER, SERVICEMODE_TRANSLATE
} SERVICEMODE;


typedef enum
{
    TOC_UNRECOGNIZED,
    TOC_NOTAG_INSERT_TAG,                 TOC_NOTAG_INSERT_TAG_PRIORITY_FROM_DSCP, TOC_NOTAG_INSERT_2_TAGS,          
    TOC_NOTAG_DEFAULT,                    TOC_NOTAG_DROP_DEFAULT,  
                                         
    TOC_1TAG_INSERT1FULLTAG,              TOC_1TAG_INSERT1TAG_COPYPRIORITY,   TOC_1TAG_INSERT1TAG_PBITXLATION, 
    TOC_1TAG_MODIFYFULLTAG,               TOC_1TAG_MODIFYTAG_KEEPPRIORITY,    TOC_1TAG_MODIFYTAG_PBITXLATION,
    TOC_1TAG_INSERT1TAG_MODIFYINNERTAG,   TOC_1TAG_INSERT1TAG_MODIFYINNERTAG_COPY_PRIORITY, 
    TOC_1TAG_INSERT_2FULL_TAGS,           TOC_1TAG_DEFAULT,                   TOC_1TAG_DROP_DEFAULT,
    TOC_1TAG_DEFAULT_INSERT1FULLTAG,      TOC_1TAG_TRANSPARENT,               TOC_1TAG_INSERT1TAG_PRIORITY_FROM_DSCP,
    TOC_1TAG_MODIFYTAG_PRIORITY_FROM_DSCP,TOC_1TAG_REMOVE_1TAG,
                                         
    TOC_2TAG_INSERT_FULL_TAG,             TOC_2TAG_INSERT_TAG_COPYPRIORITY,
    TOC_2TAG_MODIFY_FULL_TAG,             TOC_2TAG_MODIFY_TAG_COPYOUTERPRIORITY,  TOC_2TAG_MODIFY_TAG_COPYINNERPRIORITY,
    TOC_2TAG_MODIFY_BOTH_TAGS,            TOC_2TAG_MODIFY_BOTH_TAGS_COPYPRIORITY,
    TOC_2TAG_SWAPTAGS,                    TOC_2TAG_INSERT_2FULLTAGS,
    TOC_2TAG_INSERT_2TAGS_COPYPRIORITY,
    TOC_2TAG_DEFAULT,                     TOC_2TAG_DROP_DEFAULT,               TOC_2TAG_DEFAULT_MODIFY_FULL_TAG, 
    TOC_2TAG_TRANSPARENT,                 TOC_2TAG_REMOVE_EXTERNAL_TAG,        TOC_2TAG_REMOVE_BOTH_TAGS,

} TAGOPCLASS;

typedef enum 
{
    VLANMODACTION_ASIS,                                    // asis 
    VLANMODACTION_DISCARD,                                 // discard  
    VLANMODACTION_ADD,                                     // add                       
    VLANMODACTION_ADD_COPY_DSCP,                           // add_copy_dscp             
    VLANMODACTION_ADD_COPY_OUTER_PBIT,                     // add_copy_outer_pbit       
    VLANMODACTION_ADD_COPY_INNER_PBIT,                     // add_copy_inner_pbit       
    VLANMODACTION_ADD_2_TAGS,                              // add_2_tags                
    VLANMODACTION_ADD_2_TAGS_COPY_DSCP,                    // add_2_tags_copy_dscp      
    VLANMODACTION_ADD_2_TAGS_COPY_PBIT,                    // add_2_tags_copy_pbit      
    VLANMODACTION_REM,                                     // rem                       
    VLANMODACTION_REM_2_TAGS,                              // rem_2_tags                
    VLANMODACTION_REPLACE,                                 // replace                   
    VLANMODACTION_REPLACE_VID,                             // replace_vid               
    VLANMODACTION_REPLACE_PBIT,                            // replace_pbit              
    VLANMODACTION_REPLACE_INNER_ADD_OUTER,                 // replace_inner_add_outer   
    VLANMODACTION_REPLACE_INNER_ADD_OUTER_COPY_PBIT,       // replace_inn_add_out_copy_pb
    VLANMODACTION_REPLACE_INNER_REM_OUTER,                 // replace_inner_rem_outer   
    VLANMODACTION_REPLACE_2TAGS,                           // replace_2tags             
    VLANMODACTION_REPLACE_2TAGS_VID,                       // replace_2tags_vid         
    VLANMODACTION_SWAP                                     // swap                        

} VLANMODACTION;

typedef enum
{
    L2DATAGGEDFILTERMODE_PASS,
    L2DATAGGEDFILTERMODE_DISCARD,
    L2DATAGGEDFILTERMODE_POS_TCI,
    L2DATAGGEDFILTERMODE_POS_VID,
    L2DATAGGEDFILTERMODE_POS_PRI,
    L2DATAGGEDFILTERMODE_NEG_TCI,
    L2DATAGGEDFILTERMODE_NEG_VID,
    L2DATAGGEDFILTERMODE_NEG_PRI,
    L2DATAGGEDFILTERMODE_NO_FLOOD_POS_TCI,
    L2DATAGGEDFILTERMODE_NO_FLOOD_POS_VID,
    L2DATAGGEDFILTERMODE_NO_FLOOD_POS_PRI,
    L2DATAGGEDFILTERMODE_BIDI_FILT_POS_TCI,
    L2DATAGGEDFILTERMODE_BIDI_FILT_POS_VID,
    L2DATAGGEDFILTERMODE_BIDI_FILT_POS_PRI,
} L2DATAGGEDFILTERMODE;

// The following enum defines whether the presented tag on the US IGMP frame is don't care (i.e. tagged or untagged), untagged, or 1-tag
// According to this, the frame undergoes a tagging operation (as-is, add-tag, replace-tag, replace-vid)
typedef enum 
{
    IGMPTAGMATCH_UNTAGGED, IGMPTAGMATCH_1TAG
} IGMPTAGMATCH;

// The following enum defines what match must be performed on the US tag before it undergoes a tagging operation
// The options are to VID 0-4095, an untagged frame, a frame with any VID.
typedef enum
{
    IGMP_US_UNISIDE_MATCH_VID_0=0,       IGMP_US_UNISIDE_MATCH_VID_4095=4095, 
    IGMP_US_UNISIDE_MATCH_UNTAGGED=4096, IGMP_US_UNISIDE_MATCH_ANYTAG=4097,
    IGMP_US_UNISIDE_MATCH_DONTCARE
} IGMPUSUNISIDEVIDMATCH;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  Data structures for API 
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



typedef struct
{
    UINT16               vlanFilterTable[NUM_ODA_TPFILTERVLANS];
    UINT8                numberOfEntries;
    L2DATAGGEDFILTERMODE taggedFilterMode;
    bool                 isUntaggedPassed;
} VlanFilterInfo_S;


typedef struct
{
    bool      inUse;
    UINT16    gemInstance;
    UINT32    gemPort;
    UINT32    dsQueue;
    CASTTYPE  castType;
} ParamGemPortDsQ_S;


typedef struct
{
    bool    inUse;
    UINT16  gemInstance;    
    UINT32  gemPort;
    UINT32  usQTcont;
} ParamGemPortPrioUsQ_S;


#define DSCPVALUES_ARA_SIZE        24
typedef struct
{
    bool                  dscpMode;
    int                   untaggedFrameDefaultPriority;
    UINT8                 dscpToPbitsMapping[DSCPVALUES_ARA_SIZE];
} UsUntaggedConfig_S;


#define MAX_PBITS    (8)
typedef struct
{
    ParamGemPortDsQ_S     gemPortDsQueueAra[MAX_PBITS];
    ParamGemPortPrioUsQ_S gemPortPriorityUsQMapping[MAX_PBITS];
    UsUntaggedConfig_S    usUntaggedConfig;
} ParamAniGem_S;

// The NONFILT_TPID value is not a valid TPID value, but != 0 so that tags present can be counted
#define NONFILT_TPID              (1)


typedef struct
{
    UINT16     tpid;
    UINT16     vid;
    UINT16     vidMask;
    UINT8      cfi;
    UINT8      cfiMask;
    UINT8      pbit;
    UINT8      pbitMask;
 } VlanAttribs_S;


 typedef struct 
{
     UINT16        etherType;
    VlanAttribs_S  vlan_match_ara[2];
} NewVlanMatch_S;


typedef struct 
{
    VLANMODACTION    vlanModAction;
    VlanAttribs_S    vlan_mod_ara[2];
} NewVlanMod_S;


typedef struct
{
    UINT32           isDefault;
    NewVlanMatch_S   match;
    NewVlanMod_S     mod;
 } VlanMatchMod_S;


typedef struct
{
     TAGOPCLASS         tagOpClass;
     UINT16             vidOut; 
     UINT16             pbitsOut; 
     VlanMatchMod_S     us_match_mod;
     VlanMatchMod_S     ds_match_mod;
} VlanTaggingOp_S;


typedef struct
{
    bool                dscpMode;
    UINT8               dscpToPbitsMapping[DSCPVALUES_ARA_SIZE];
} DscpInfo_S;


// Similar to Reg. VLAN tagging - e.g. actions can be (1)replace tag and (2)add to an untaggedframe - hence 2 mcast actions
#define MAX_MCASTACTIONS             (2)

typedef struct 
{
    bool             inUse;
    IGMPTAGMATCH     igmpTagMatch;
    VLANMODACTION    vlanModAction;
    VlanAttribs_S    vlan_mod;
} McastVlanMatchMod_S;


typedef struct
{
    McastVlanMatchMod_S   mcastVlanMatchModAra[MAX_MCASTACTIONS];
} McastVlanTagOp_S;


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 
//                  API 
// 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



// MIB reset for L2DA
L2DAPROCSTATUS L2DA_resetL2DA             (void);

// Init L2DA DB etc
L2DAPROCSTATUS L2DA_initL2DA              (void);

// Partition
L2DAPROCSTATUS L2DA_createDataService      (SERVICEKEY              serviceKey,
                                            TPKEY                   aniTpKey,   
                                            TPKEY                   uniTpKey,  
                                            ParamAniGem_S           *p_AniGem);

// Partition
L2DAPROCSTATUS L2DA_deleteDataService      (SERVICEKEY              serviceKey);

// ANI TP
L2DAPROCSTATUS L2DA_addAniTp               (TPKEY                   aniTpKey, 
                                            UINT32                  partitionKey, 
                                            ParamAniGem_S           *p_AniGem);

L2DAPROCSTATUS L2DA_deleteAniTp            (TPKEY                   aniTpKey);

// UNI TP
L2DAPROCSTATUS L2DA_addUniTp               (TPKEY                   uniTpKey, 
                                            UINT32                  partitionKey);

L2DAPROCSTATUS L2DA_deleteUniTp            (TPKEY                   uniTpKey);

// GemPort
L2DAPROCSTATUS L2DA_addGemPort             (TPKEY                   aniTpKey, 
                                            UINT16                  gemPort, 
                                            UINT16                  dsQ, 
                                            bool                    dsValid, 
                                            bool                    usValid, 
                                            bool                    multicastValid);

L2DAPROCSTATUS L2DA_deleteGemPort          (TPKEY                   aniTpKey, 
                                            UINT16                  gemPort,
                                            UINT16                  gemInstance);

// Priority-USQueue-GemPort mapping
L2DAPROCSTATUS L2DA_addGemportPbitMap      (TPKEY                   aniTpKey, 
                                            int                     priority,
                                            UINT16                  gemInstance,
                                            UINT16                  gemPort, 
                                            UINT32                  usQTcont);

L2DAPROCSTATUS L2DA_deleteGemportPbitMap   (TPKEY                   aniTpKey, 
                                            UINT16                  gemPort, 
                                            int                     priority);


// VLAN Filter
L2DAPROCSTATUS L2DA_addVlanFilter          (TPKEY                   tpKey, 
                                            VlanFilterInfo_S        *p_VlanFilterInfo);

L2DAPROCSTATUS L2DA_deleteVlanFilter       (TPKEY                   tpKey);


// VLAN Tagging Operations
L2DAPROCSTATUS L2DA_addTaggingOp           (TPKEY                   tpKey,
                                            int                     opKey,
                                            VlanTaggingOp_S         *p_vlanTaggingOp);

L2DAPROCSTATUS L2DA_deleteTaggingOp        (TPKEY                   tpKey,
                                            int                     opKey);

// DSCP info
L2DAPROCSTATUS L2DA_setDscpConfiguration   (TPKEY                   tpKey,
                                            DscpInfo_S              *p_DscpInfo);

// Multicast IGMP US tagging operation
// Note: the tpKey is a UNI TP
L2DAPROCSTATUS L2DA_addMcastTaggingOp      (TPKEY                   tpKey,
                                            int                     opKey,
                                            McastVlanTagOp_S        *p_McastVlanTagOp,
                                            bool                    isDs);

L2DAPROCSTATUS L2DA_deleteMcastTaggingOp   (TPKEY                   mcastDataTpKey,
                                            int                     opKey);


#endif

