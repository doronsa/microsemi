/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************
**  FILE        : ponOnuInternals.h                                          **
**                                                                           **
**  DESCRIPTION : This file contains ONU GPON Management Interface           **
*******************************************************************************
 *                                                                            *                              
 *  MODIFICATION HISTORY:                                                     *
 *                                                                            *
 *   27Feb11  octaviap          created                                       *  
 * ========================================================================== *      
 *                                                                          
 ******************************************************************************/
#ifndef _ONU_PON_MNG_INTERFACE_INTERNALS_H
#define _ONU_PON_MNG_INTERFACE_INTERNALS_H


#include <sys/socket.h>
#include <linux/if_packet.h>
#include <linux/if.h>
#include <errno.h>

#include "tpm_api.h"


/* ========================================================================== */
/* ============== COMMON EPON/ GPON  ======================================== */
/* ========================================================================== */
/* Definitions
------------------------------------------------------------------------------*/ 
/* socket properties */
#define     MRVL_SOCKET_BLOCKING_MASK   0x00000001

#define	 	ETH_PROTO_eOAM_OMCI     	0xBABA



/* Typedefs
------------------------------------------------------------------------------*/
typedef struct 
{
    uint32_t    rx_total_frames;
    uint32_t    rx_failures;
    uint32_t    tx_total_frames;
    uint32_t    tx_failures;

}mrvl_pon_statistics_type;


/* ========================================================================== */
/* ============== EPON section       ======================================== */
/* ========================================================================== */

/* Definitions
------------------------------------------------------------------------------*/ 
#define     MRVL_EOAM_QUEUE_MASK        0x000F
#define     MRVL_PON_FIFO_MAX_LEN       84      /*the maximum length of EPON FIFO*/


/* Typedefs
------------------------------------------------------------------------------*/
typedef struct
{
  uint8_t       mac[EPON_API_MAC_LEN];
  uint32_t      queue_num;
}mrvl_llid_mac_map_type;

mrvl_llid_mac_map_type      mrvl_llid_mac_q_map_db[EPON_API_MAX_NUM_OF_MAC];



/* ========================================================================== */
/* ============== GPON section       ======================================== */
/* ========================================================================== */

/* Definitions
------------------------------------------------------------------------------*/ 







#endif /* _ONU_PON_MNG_INTERFACE_INTERNALS_H */

