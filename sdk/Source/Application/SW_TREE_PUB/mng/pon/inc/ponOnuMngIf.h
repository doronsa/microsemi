/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : PON                                                      **/
/**                                                                          **/
/**  FILE        : ponOnuMngIf.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file contains ONU GPON Management Interface          **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 *
 ******************************************************************************/


 /******************************************************************************/
#ifndef _ONU_PON_MNG_INTERFACE_H
#define _ONU_PON_MNG_INTERFACE_H

/******************************************************************************/
/******************************************************************************/
/* ========================================================================== */
/* ========================================================================== */
/* ==                                                                      == */
/* ==  ========= ======== ==        == ==        == ======== ===       ==  == */
/* ==  ========= ======== ==        == ==        == ======== ====      ==  == */
/* ==  ==        ==    == ===      === ===      === ==    == == ==     ==  == */
/* ==  ==        ==    == ====    ==== ====    ==== ==    == ==  ==    ==  == */
/* ==  ==        ==    == == ==  == == == ==  == == ==    == ==   ==   ==  == */
/* ==  ==        ==    == ==  ====  == ==  ====  == ==    == ==    ==  ==  == */
/* ==  ==        ==    == ==  ====  == ==  ====  == ==    == ==     == ==  == */
/* ==  ==        ==    == ==   ==   == ==   ==   == ==    == ==      ====  == */
/* ==  ========= ======== ==   ==   == ==   ==   == ======== ==       ===  == */
/* ==  ========= ======== ==   ==   == ==   ==   == ======== ==        ==  == */
/* ==                                                                      == */
/* ========================================================================== */
/* ========================================================================== */
/******************************************************************************/
/******************************************************************************/

/* Definitions
------------------------------------------------------------------------------*/
#define PON_NUM_DEVICES     (1)
#define PON_DEV_NAME        ("pon")
#define MVPON_IOCTL_MAGIC   ('P')

#define MRVL_EXIT_OK                            (0)
#define MRVL_OK                                 (MRVL_EXIT_OK)
#define MRVL_ERROR_BASE                         (-17000)
#define MRVL_ERROR_EXIT                         (MRVL_ERROR_BASE-1)
#define MRVL_ERROR_TIME_OUT                     (MRVL_ERROR_BASE-2)
#define MRVL_ERROR_NOT_IMPLEMENTED              (MRVL_ERROR_BASE-3)
#define MRVL_ERROR_PARAMETER                    (MRVL_ERROR_BASE-4)
#define MRVL_ERROR_HARDWARE                     (MRVL_ERROR_BASE-5)
#define MRVL_ERROR_MEMORY                       (MRVL_ERROR_BASE-6)
#define MRVL_ERROR_NOT_SUPPORT                  (MRVL_ERROR_BASE-7)
#define MRVL_ERROR_QUERY_FAILED	           		(MRVL_ERROR_BASE-8)
#define MRVL_ERROR_NU_NOT_AVAILABLE             (MRVL_ERROR_BASE-9)
#define MRVL_ERROR_OLT_NOT_EXIST                (MRVL_ERROR_BASE-10)
#define MRVL_ERROR_EXIT_ONU_DBA_THRESHOLDS      (MRVL_ERROR_BASE-11)
#define MRVL_ERROR_TFTP_SEND_FAIL               (MRVL_ERROR_BASE-12)
#define MRVL_ERROR_NOT_INITIALIZED			    (MRVL_ERROR_BASE-13)
#define MRVL_ERROR_ALREADY_EXIST                (MRVL_ERROR_BASE-14)
#define	MRVL_ERROR_DB_FULL                      (MRVL_ERROR_BASE-15)
#define	MRVL_ERROR_OUT_OF_RANGE                 (MRVL_ERROR_BASE-19)

/* Enums
------------------------------------------------------------------------------*/
typedef enum
{
  MRVL_SLA_BEST_EFFORT_SCHEDULING_SCHEME_SP         = 0,
  MRVL_SLA_BEST_EFFORT_SCHEDULING_SCHEME_WRR		= 1,
  MRVL_SLA_BEST_EFFORT_SCHEDULING_SCHEME_SP_AND_WRR	= 2,
}E_PonSlaBestEffortSchedulingScheme;

/* Typedefs
------------------------------------------------------------------------------*/
typedef int mrvlErrorCode_t;
typedef mrvlErrorCode_t mrvl_error_code_t;


typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef signed long long int int64_t;
typedef unsigned long long int uint64_t;

typedef struct
{
  uint16_t transceiverTemperature;
  uint16_t supplyVoltage;
  uint16_t txBiasCurrent;
  uint16_t txPower;
  uint16_t rxPower;
}S_PonTransceiverDiagnosis;

typedef struct
{
  uint8_t  highPriorityBoundary;
  uint8_t  queueId;               /* Queue to which service traffic is classified */
  uint16_t fixedPacketSize;		  /* Given in byte units */
  uint16_t fixedBandwidth;		  /* Fixed bandwidth in 256 Kbps units */
  uint16_t guaranteedBandwidth;	  /* Assured bandwidth in 256 Kbps units */
  uint16_t bestEffortBandwidth;	  /* Best effort bandwidth in 256 Kbps units */
  uint8_t  wrrWeight;		      /* Possible values: 0 (SP), 1 - 100 (WRR) */
}S_PonSlaScheme;

/* Global functions
------------------------------------------------------------------------------*/
/*******************************************************************************
* mvPonDrvFdGet()
*
* DESCRIPTION:      return previously created pon char device file descriptor.
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvPonDrvFdGet(void);

/*******************************************************************************
* mvPonDrvFdExist()
*
* DESCRIPTION:      verify pon char device has been established in the kernel.
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvPonDrvFdExist(void);

/*******************************************************************************
* mvPonTransceiverDiagnosisGet()
*
* DESCRIPTION:      return transceiver diagnosis parameters
*
* INPUTS:			E_PonTransceiverDiagnosis *transceiver_diagnosis
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvPonTransceiverDiagnosisGet(S_PonTransceiverDiagnosis *transceiver_diagnosis);

/*******************************************************************************
* mvPonSlaSet()
*
* DESCRIPTION:      configure pon device SLA parameters
*
* INPUTS:		    E_PonSlaBestEffortSchedulingScheme bfSched
*                   uint32_t                           cycleLength
*                   uint32_t                           serviceNum
*                   E_PonSlaScheme                    *slaPara
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvPonSlaSet(E_PonSlaBestEffortSchedulingScheme bfSched,
                            uint32_t                           cycleLength,
                            uint32_t                           serviceNum,
                            S_PonSlaScheme                    *slaPara);

/******************************************************************************/
/******************************************************************************/
/* ========================================================================== */
/* ========================================================================== */
/* ==                                                                      == */
/* ==           =========   =========   =========   ===       ==           == */
/* ==           =========   =========   =========   ====      ==           == */
/* ==           ==          ==     ==   ==     ==   == ==     ==           == */
/* ==           ==          ==     ==   ==     ==   ==  ==    ==           == */
/* ==           =========   =========   ==     ==   ==   ==   ==           == */
/* ==           =========   =========   ==     ==   ==    ==  ==           == */
/* ==           ==     ==   ==          ==     ==   ==     == ==           == */
/* ==           ==     ==   ==          ==     ==   ==      ====           == */
/* ==           =========   ==          =========   ==       ===           == */
/* ==           =========   ==          =========   ==        ==           == */
/* ==                                                                      == */
/* ========================================================================== */
/* ========================================================================== */
/******************************************************************************/
/******************************************************************************/

/* Definitions
------------------------------------------------------------------------------*/
#define MVGPON_IOCTL_INIT               _IOW(MVPON_IOCTL_MAGIC,  1, unsigned int)
#define MVGPON_IOCTL_BEN_INIT           _IOW(MVPON_IOCTL_MAGIC,  2, unsigned int)
#define MVGPON_IOCTL_DATA_TCONT_CONFIG	_IOW(MVPON_IOCTL_MAGIC,  3, unsigned int)
#define MVGPON_IOCTL_DATA_TCONT_CLEAR   _IOR(MVPON_IOCTL_MAGIC,  4, unsigned int)
#define MVGPON_IOCTL_DATA_TCONT_RESET	 _IO(MVPON_IOCTL_MAGIC,  5)
#define MVGPON_IOCTL_INFO               _IOR(MVPON_IOCTL_MAGIC,  6, unsigned int)
#define MVGPON_IOCTL_ALARM              _IOR(MVPON_IOCTL_MAGIC,  7, unsigned int)
#define MVGPON_IOCTL_PM                 _IOR(MVPON_IOCTL_MAGIC,  8, unsigned int)
#define MVGPON_IOCTL_GEM                _IOW(MVPON_IOCTL_MAGIC,  9, unsigned int)
#define MVGPON_IOCTL_GEMPORT_PM_CONFIG  _IOW(MVPON_IOCTL_MAGIC,  10, unsigned int)
#define MVGPON_IOCTL_GEMPORT_PM_GET     _IOR(MVPON_IOCTL_MAGIC,  11, unsigned int)
#define MVGPON_IOCTL_GEMPORT_PM_RESET   _IOW(MVPON_IOCTL_MAGIC,  12, unsigned int)
#define MVGPON_IOCTL_GEMPORT_STATE_GET  _IOW(MVPON_IOCTL_MAGIC,  13, unsigned int)
#define MVGPON_IOCTL_GEMPORT_STATE_SET  _IOW(MVPON_IOCTL_MAGIC,  14, unsigned int)

#define ONU_GPON_DS_MSG_LAST			(0x14)
#define ONU_GPON_US_MSG_LAST            (0x09)
#define ONU_GPON_MAX_NUM_OF_T_CONTS		(8)
#define ONU_GPON_NUM_OF_ALARMS			(14)
#define ONU_GPON_MAX_GEM_PORTS			(4096)
#define ONU_GPON_DEBUG_STATE            (0xFF)

/* Enums
------------------------------------------------------------------------------*/
/* PM */
typedef enum
{
  E_GPON_IOCTL_PM_PLOAM_RX    = 1,
  E_GPON_IOCTL_PM_PLOAM_TX    = 2,
  E_GPON_IOCTL_PM_BW_MAP      = 3,
  E_GPON_IOCTL_PM_FEC         = 4,
  E_GPON_IOCTL_PM_GEM_RX      = 5,
  E_GPON_IOCTL_PM_GEM_TX      = 6
}E_GponIoctlPmSection;

/* Gem */
typedef enum
{
  E_GPON_IOCTL_GEM_ADD        = 1,
  E_GPON_IOCTL_GEM_REMOVE     = 2,
  E_GPON_IOCTL_GEM_CLEARALL   = 3
}E_GponIoctlGemAction;

typedef enum
{
  GEMPORTPMCMD_STOP  = 0,
  GEMPORTPMCMD_START = 1
}E_GponIoctlGemPortCmd;

/* Typedefs
------------------------------------------------------------------------------*/
/* PM */
typedef struct
{
  uint32_t rxIdlePloam;
  uint32_t rxCrcErrorPloam;
  uint32_t rxFifoOverErrorPloam;
  uint32_t rxBroadcastPloam;
  uint32_t rxOnuIdPloam;
  uint32_t rxMsgIdPloam[ONU_GPON_DS_MSG_LAST+1];
  uint32_t rxMsgTotalPloam;
}S_GponIoctlPloamRxPm;

typedef struct
{
  uint32_t txErrMsgIdPloam[ONU_GPON_US_MSG_LAST+1];
  uint32_t txMsgIdPloam[ONU_GPON_US_MSG_LAST+1];
  uint32_t txMsgTotalPloam;
}S_GponIoctlPloamTxPm;

typedef struct
{
  uint32_t allocCrcErr;
  uint32_t allocCorrectableCrcErr;
  uint32_t allocUnCorrectableCrcErr;
  uint32_t allocCorrec;
  uint32_t totalReceivedAllocBytes;
}S_GponIoctlBwMapPm;

typedef struct
{
  uint32_t receivedBytes;
  uint32_t correctedBytes;
  uint32_t correctedBits;
  uint32_t receivedCodeWords;
  uint32_t uncorrectedCodeWords;
}S_GponIoctlFecPm;

typedef struct
{
  uint32_t gemRxIdleGemFrames;
  uint32_t gemRxValidGemFrames;
  uint32_t gemRxUndefinedGemFrames;
  uint32_t gemRxOmciFrames;
  uint32_t gemRxDroppedGemFrames;
  uint32_t gemRxDroppedOmciFrames;
  uint32_t gemRxGemFramesWithUncorrHecErr;
  uint32_t gemRxGemFramesWithOneFixedHecErr;
  uint32_t gemRxGemFramesWithTwoFixedHecErr;
  uint32_t gemRxTotalByteCountOfReceivedValidGemFrames;
  uint32_t gemRxTotalByteCountOfReceivedUndefinedGemFrames;
  uint32_t gemRxGemReassembleMemoryFlush;
  uint32_t gemRxGemSynchLost;
  uint32_t gemRxEthFramesWithCorrFcs;
  uint32_t gemRxEthFramesWithFcsError;
  uint32_t gemRxOmciFramesWithCorrCrc;
  uint32_t gemRxOmciFramesWithCrcError;
}S_GponIoctlGemRxPm;

typedef struct
{
  uint32_t gemTxGemPtiTypeOneFrames;
  uint32_t gemTxGemPtiTypeZeroFrames;
  uint32_t gemTxIdleGemFrames;
  uint32_t gemTxEthFramesViaTconti[ONU_GPON_MAX_NUM_OF_T_CONTS];
  uint32_t gemTxEthBytesViaTconti[ONU_GPON_MAX_NUM_OF_T_CONTS];
  uint32_t gemTxGemFramesViaTconti[ONU_GPON_MAX_NUM_OF_T_CONTS];
  uint32_t gemTxIdleGemFramesViaTconti[ONU_GPON_MAX_NUM_OF_T_CONTS];
}S_GponIoctlGemTxPm;

typedef struct
{
  unsigned int  section;
  union
  {
    S_GponIoctlPloamRxPm ploamRx;
    S_GponIoctlPloamTxPm ploamTx;
    S_GponIoctlBwMapPm   bwMap;
    S_GponIoctlFecPm     fec;
    S_GponIoctlGemRxPm   gemRx;
    S_GponIoctlGemTxPm   gemTx;
  };
}S_GponIoctlPm;

/* Alarm */
typedef enum
{
  /* ASIC Alarms */
  ONU_GPON_ALARM_LOS = 0,
  ONU_GPON_ALARM_LOF,
  ONU_GPON_ALARM_LCDA,
  ONU_GPON_ALARM_LCDG,
  /* SW Alarms */
  ONU_GPON_ALARM_SF,
  ONU_GPON_ALARM_SD,
  ONU_GPON_ALARM_TF,
  ONU_GPON_ALARM_SUF,
  ONU_GPON_ALARM_MEM,
  ONU_GPON_ALARM_DACT,
  ONU_GPON_ALARM_DIS,
  ONU_GPON_ALARM_MIS,
  ONU_GPON_ALARM_PEE,
  ONU_GPON_ALARM_RDI,
  ONU_GPON_MAX_ALARMS
}E_OnuGponAlarmType;

typedef enum
{
  ONU_GPON_ALARM_OFF,
  ONU_GPON_ALARM_ON
}E_OnuGponAlarmState;

typedef struct
{
  uint32_t alarmTbl[ONU_GPON_NUM_OF_ALARMS];
}S_GponIoctlAlarm;

/* Info */
typedef struct
{
  uint32_t onuId;
  uint32_t onuState;
  uint32_t onuSignalDetect;
  uint32_t onuDsSyncOn;
  uint32_t omccPort;
  uint32_t omccValid;
  uint32_t serialNumSource;
  uint8_t  serialNum[8];
  uint8_t  password[10];
  uint32_t disableSn;
  uint32_t clearGem;
  uint32_t clearTcont;
  uint32_t restoreGem;
  uint32_t dgPolarity;
  uint32_t ponXvrBurstEnPolarity;
  uint32_t ponXvrPolarity;
  uint32_t p2pXvrBurstEnPolarity;
  uint32_t p2pXvrPolarity;
  uint32_t fecHyst;
  uint32_t couplingMode;    /* AC Coupling Mode */
}S_GponIoctlInfo;

/* Data */
typedef struct
{
  uint32_t alloc;
  uint32_t tcont;
  uint32_t gemPort;
  uint32_t gemState;
}S_GponIoctlData;

/* Xvr Burst */
typedef struct
{
  uint32_t mask;
  uint32_t polarity;
  uint32_t delay;
  uint32_t enStop;
  uint32_t enStart;
}S_GponIoctlXvr;

/* Gem */
typedef struct
{
  uint32_t              gemmap[ONU_GPON_MAX_GEM_PORTS/32];
  E_GponIoctlGemAction  action;
}S_GponIoctlGem;

typedef struct
{
  E_GponIoctlGemPortCmd command;
  uint16_t              gem_port;
}S_GponIoctlGemPortPmConfig;

typedef struct
{
  uint16_t  gem_port;

  uint64_t  good_octets_received;
  uint32_t  bad_octets_received;
  uint32_t  mac_trans_error;
  uint32_t  good_frames_received;
  uint32_t  bad_frames_received;
  uint32_t  broadcast_frames_received;
  uint32_t  multicast_frames_received;
  uint32_t  frames_64_octets;
  uint32_t  frames_65_to_127_octets;
  uint32_t  frames_128_to_255octets;
  uint32_t  frames_256_to_511_octets;
  uint32_t  frames_512_to_1023_octets;
  uint32_t  frames_1024_to_max_octets;
  uint64_t  good_octets_sent;
  uint32_t  good_frames_sent;
  uint32_t  multicast_frames_sent;
  uint32_t  broadcast_frames_sent;
  uint32_t  bad_crc_received;
}S_GponIoctlGemPortMibCounters;

/* Global functions
------------------------------------------------------------------------------*/
/*******************************************************************************
* mvGponOmciChannelInit()
*
* DESCRIPTION:      Initialization of an OMCI channel for GPON.
*
* INPUTS:	        uint16_t  gemPort
*                   uint8_t   tcontNum
*                   uint8_t   tx_queue
*                   uint32_t  config_bitmap
*                               bit#0   => 0 = non-blocking socket created
*                                          1 = blocking socket
*                               bit1-31 => not used
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponOmciChannelInit(uint16_t gemPort, uint8_t tcontNum, uint8_t tx_queue, uint8_t rx_queue, uint32_t config_bitmap);

/*******************************************************************************
* mvGponOmciChannelUpdate()
*
* DESCRIPTION:      Update the OMCI channel for GPON.
*
* INPUTS:	        uint16_t gemPort
*                   uint8_t  tcontNum
*                   uint8_t  tx_queue
*                   uint8_t  rx_queue
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponOmciChannelUpdate(uint16_t gemPort, uint8_t tcontNum, uint8_t tx_queue, uint8_t rx_queue);

/*******************************************************************************
* mvGponSendOmciFrame()
*
* DESCRIPTION:      Transmit OMCI frame to specific T-CONT.
*
* INPUTS:			uint8_t  *data
*                   uint16_t  length
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponSendOmciFrame(uint8_t *data, uint16_t length);

/*******************************************************************************
* mvGponReceiveOmciFrame()
*
* DESCRIPTION:      Receive OMCI frame
*
* INPUTS:			uint8_t  *data
*                   uint16_t *length
*                   uint32_t timeout     - timeout in msec
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponReceiveOmciFrame(uint8_t *data, uint16_t *length, uint32_t timeout);

/*******************************************************************************
* mvGponInit()
*
* DESCRIPTION:      configure Gpon module init: serial number, password,
*                   disable SN status, clear gem port configuration flag, and
*                   clear tcont configuration flag
*
* INPUTS:			uint8_t  *sn
*                   uint8_t  *pswd
*                   uint32_t  dis
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponInit(uint8_t *sn, uint8_t *pswd, uint32_t dis, uint32_t snSrc);

/*******************************************************************************
* mvGponGetInfo()
*
* DESCRIPTION:      return Gpon module info, including onu Id, state,
*                   signal detect status, DS sync status, omcc port, serial number,
*                   password, disable SN status
*
* INPUTS:			S_GponIoctlInfo *ioctlInfo
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponGetInfo(S_GponIoctlInfo *ioctlInfo);

/*******************************************************************************
* mvGponSetTcont()
*
* DESCRIPTION:      set connectivity between tcont and alloc Id
*
* INPUTS:			uint32_t tcontNum
*                   uint32_t allocId
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponSetTcont(uint32_t tcontNum, uint32_t allocId);

/*******************************************************************************
* mvGponResetTconts()
*
* DESCRIPTION:      clear all tcont configuration
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponResetTconts(void);

/*******************************************************************************
* mvGponAddGemPort()
*
* DESCRIPTION:      monitor specific gem port counters, up to 8 gem port counters
*                   can be monitored, valid only in Rx direction
*
* INPUTS:			uint32_t* gemmap
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponAddGemPort(uint32_t* gemmap);

/*******************************************************************************
* mvGponRemoveGemPort()
*
* DESCRIPTION:      remove gem port form gem port monitor list
*
* INPUTS:			uint32_t* gemmap
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponRemoveGemPort(uint32_t* gemmap);

/*******************************************************************************
* mvGponResetGemPort()
*
* DESCRIPTION:      remove all gem ports from gem port monitor list
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponResetGemPort(void);

/*******************************************************************************
* mvGponReadPmGemPort()
*
* DESCRIPTION:      return monitored gem port rx counters
*
* INPUTS:			uint16_t                       gemPort
*                   S_GponIoctlGemPortMibCounters *gponIoctlGemPortMibCounters
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponReadPmGemPort(uint16_t gemPort,
									S_GponIoctlGemPortMibCounters *gponIoctlGemPortMibCounters);

/*******************************************************************************
* mvGponStartPmGemPort()
*
* DESCRIPTION:      start monitoring gem port counters
*
* INPUTS:			uint16_t gemPort
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponStartPmGemPort(uint16_t gemPort);

/*******************************************************************************
* mvGponStopPmGemPort()
*
* DESCRIPTION:      stop monitoring gem port counters
*
* INPUTS:			uint16_t gemPort
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponStopPmGemPort(uint16_t gemPort);

/*******************************************************************************
* mvGponResetPmAllGemPorts()
*
* DESCRIPTION:      reset gem port monitor list counters
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponResetPmAllGemPorts(void);

/*******************************************************************************
* mvGponTxBurstEnableParamsSet()
*
* DESCRIPTION:      set burst enable parameters
*
* INPUTS:			mask
*                   polarity
*                   delay
*                   enStop
*                   enStart
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponTxBurstEnableParamsSet(uint32_t mask,
                                             uint32_t polarity,
                                             uint32_t delay,
                                             uint32_t enStop,
                                             uint32_t enStart);

/*******************************************************************************
* mvGponGetAlarms()
*
* DESCRIPTION:      get GPON alarms
*
* INPUTS:			S_GponIoctlAlarm *ioctlAlarm
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponGetAlarms(S_GponIoctlAlarm *ioctlAlarm);

/*******************************************************************************
* mvGponGetPMs()
*
* DESCRIPTION:      get GPON PMs
*
* INPUTS:			S_GponIoctlPm *ioctlPm
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponGetPMs(S_GponIoctlPm *ioctlPm);

/*******************************************************************************
**
**  mvGponPortIdGet
**  ____________________________________________________________________________
**
**  DESCRIPTION: The function return port Id valid state
**
**  PARAMETERS:  uint32_t gemPort
**
**  OUTPUTS:     uint32_t state
**
**  RETURNS:     mrvlErrorCode_t
**
*******************************************************************************/
mrvlErrorCode_t mvGponPortIdGet(uint32_t gemPort, uint32_t *state);


/*******************************************************************************
**
**  mvGponPortIdSet
**  ____________________________________________________________________________
**
**  DESCRIPTION: The function set valid for port Id
**
**  PARAMETERS:  uint32_t  gemPort
**               uint32_t  state
**
**  OUTPUTS:     None
**
**  RETURNS:     mrvlErrorCode_t
**
*******************************************************************************/
mrvlErrorCode_t mvGponPortIdSet(uint32_t gemPort, uint32_t state);

/******************************************************************************/
/******************************************************************************/
/* ========================================================================== */
/* ========================================================================== */
/* ==                                                                      == */
/* ==           =========   =========   =========   ===       ==           == */
/* ==           =========   =========   =========   ====      ==           == */
/* ==           ==          ==     ==   ==     ==   == ==     ==           == */
/* ==           ==          ==     ==   ==     ==   ==  ==    ==           == */
/* ==           =========   =========   ==     ==   ==   ==   ==           == */
/* ==           =========   =========   ==     ==   ==    ==  ==           == */
/* ==           ==          ==          ==     ==   ==     == ==           == */
/* ==           ==          ==          ==     ==   ==      ====           == */
/* ==           =========   ==          =========   ==       ===           == */
/* ==           =========   ==          =========   ==        ==           == */
/* ==                                                                      == */
/* ========================================================================== */
/* ========================================================================== */
/******************************************************************************/
/******************************************************************************/

/* Definitions
------------------------------------------------------------------------------*/
#define MVEPON_IOCTL_INIT          _IOW(MVPON_IOCTL_MAGIC, 1, unsigned int)
#define MVEPON_IOCTL_FEC_CONFIG    _IOW(MVPON_IOCTL_MAGIC, 2, unsigned int)
#define MVEPON_IOCTL_ENC_CONFIG    _IOW(MVPON_IOCTL_MAGIC, 3, unsigned int)
#define MVEPON_IOCTL_ENC_KEY       _IOW(MVPON_IOCTL_MAGIC, 4, unsigned int)
#define MVEPON_IOCTL_INFO          _IOR(MVPON_IOCTL_MAGIC, 5, unsigned int)
#define MVEPON_IOCTL_PM            _IOR(MVPON_IOCTL_MAGIC, 6, unsigned int)
#define MVEPON_IOCTL_OAM_TX        _IOW(MVPON_IOCTL_MAGIC, 7, unsigned int)
#define MVEPON_IOCTL_DBA_CFG       _IOW(MVPON_IOCTL_MAGIC, 8, unsigned int)
#define MVEPON_IOCTL_DBA_RPRT      _IOR(MVPON_IOCTL_MAGIC, 8, unsigned int)
#define MVEPON_IOCTL_HOLDOVER_CFG  _IOW(MVPON_IOCTL_MAGIC, 9, unsigned int)
#define MVEPON_IOCTL_HOLDOVER_RPRT _IOW(MVPON_IOCTL_MAGIC, 10, unsigned int)
#define MVEPON_IOCTL_SILENCE       _IOW(MVPON_IOCTL_MAGIC, 11, unsigned int)
#define MVEPON_IOCTL_P2P_SET       _IOW(MVPON_IOCTL_MAGIC, 12, unsigned int)
#define MVEPON_IOCTL_P2P_FORCE_MODE_SET _IOW(MVPON_IOCTL_MAGIC, 13, unsigned int)
#define MVEPON_IOCTL_TDM_QUE_CFG   _IOW(MVPON_IOCTL_MAGIC, 14, unsigned int)
#define MVEPON_IOCTL_ALARM_GET     _IOR(MVPON_IOCTL_MAGIC, 15, unsigned int)
#define MVEPON_IOCTL_ROGUE_ONU_SET _IOW(MVPON_IOCTL_MAGIC, 16, unsigned int)


#define EPON_API_MAX_NUM_OF_MAC          (8)
#define EPON_API_MAC_LEN                 (6)

#define EPON_API_MAX_OAM_FRAME_SIZE      (512)

/* db threshold set */
#define MIN_QUEUE_NUMBER                 (2)
#define MAX_QUEUE_NUMBER                 (4)
#define MAX_THRESHOLD_SET_RER_QUEUE      (8)

/* Enums
------------------------------------------------------------------------------*/
typedef enum
{
  E_EPON_IOCTL_PM_RX                         = 1,
  E_EPON_IOCTL_PM_TX                         = 2,
  E_EPON_IOCTL_PM_SW                         = 3,
  E_EPON_IOCTL_PM_GPM                        = 4
}E_EponIoctlPmSection;

typedef enum
{
  E_EPON_IOCTL_MPCP_TX_FRAME_CNT             = 0,
  E_EPON_IOCTL_MPCP_TX_ERROR_FRAME_CNT       = 1,
  E_EPON_IOCTL_MAX_TX_SW_CNT
}E_EponIoctlTxSwCnt;

typedef enum
{
  E_EPON_IOCTL_MPCP_RX_FRAME_CNT             = 0,
  E_EPON_IOCTL_MPCP_RX_ERROR_FRAME_CNT       = 1,
  E_EPON_IOCTL_MPCP_REGISTER_ACK_CNT         = 2,
  E_EPON_IOCTL_MPCP_REGISTER_NACK_CNT        = 3,
  E_EPON_IOCTL_MPCP_REGISTER_DEREG_FRAME_CNT = 4,
  E_EPON_IOCTL_MPCP_REGISTER_REREG_FRAME_CNT = 5,
  E_EPON_IOCTL_MAX_RX_SW_CNT
}E_EponIoctlRxSwCnt;

typedef enum
{
  E_EPON_IOCTL_STD_MODE,
  E_EPON_IOCTL_P2P_MODE,
  E_EPON_IOCTL_MAX_MODE_NUM
}E_EponIoctlMode;

/* Typedefs
------------------------------------------------------------------------------*/
/* TX Counters */
typedef struct
{
  uint32_t ctrlRegReqFramesCnt; /* Count number of register request frames transmitted     */
  uint32_t ctrlRegAckFramesCnt; /* Count number of register acknowledge frames transmitted */
  uint32_t reportFramesCnt;     /* Count number of report frames transmitted               */
  uint32_t dataFramesCnt;       /* Count number of data frames transmitted                 */
  uint32_t txAllowedBytesCnt;   /* Count number of Tx Byte Allow counter                   */
}S_EponIoctlTxPm;

/* RX Counters */
typedef struct
{
  uint32_t fcsErrorFramesCnt;   /* Count number of received frames with FCS errors */
  uint32_t shortFramesCnt;      /* Count number of short frames received           */
  uint32_t longFramesCnt;       /* Count number of long frames received            */
  uint32_t dataFramesCnt;       /* Count number of data frames received            */
  uint32_t ctrlFramesCnt;       /* Count number of control frames received         */
  uint32_t reportFramesCnt;     /* Count number of report frames received          */
  uint32_t gateFramesCnt;       /* Count number of gate frames received            */
}S_EponIoctlRxPm;

/* SW Counters */
typedef struct
{
  uint32_t swTxCnt[E_EPON_IOCTL_MAX_TX_SW_CNT];
  uint32_t swRxCnt[E_EPON_IOCTL_MAX_RX_SW_CNT];
}S_EponIoctlSwPm;

/* GEM Counters */
typedef struct
{
  uint32_t grantValidCnt;                 /* Count number of valid grant                          */
  uint32_t grantMaxFutureTimeErrorCnt;    /* Count number of grant max future time error          */
  uint32_t minProcTimeErrorCnt;           /* Count number of min proc time error                  */
  uint32_t lengthErrorCnt;                /* Count number of length error                         */
  uint32_t discoveryAndRegCnt;            /* Count number of discovery & register                 */
  uint32_t fifoFullErrorCnt;              /* Count number of fifo full error                      */
  uint32_t opcDiscoveryNotRegBcastCnt;    /* Count number of opc discoveryNotRegBcastCnt          */
  uint32_t opcRegisterNotDiscoveryCnt;    /* Count number of opc register not discovery           */
  uint32_t opcDiscoveryNotRegNotBcastCnt; /* Count number of opc discovery not register not bcast */
  uint32_t opcDropGrantCnt;               /* Count number of opc drop grant                       */
  uint32_t opcHiddenGrantCnt;             /* Count number of opc hidden grant                     */
  uint32_t opcBackToBackCnt;              /* Count number of opc back to back                     */
}S_EponIoctlGpmPm;

/* PM Counters */
typedef struct
{
  uint32_t macId;
  uint32_t section;
  union
  {
    S_EponIoctlRxPm  rxCnt;
    S_EponIoctlTxPm  txCnt;
    S_EponIoctlSwPm  swCnt;
    S_EponIoctlGpmPm gpmCnt;
  };
}S_EponIoctlPm;

/* INFO */
typedef struct
{
  uint32_t macId;
  uint32_t onuEponState;                       /* ONU State                   */
  uint32_t onuEponSignalDetect;                /* ONU Signal Detect           */
  uint32_t onuEponDsSyncOkPcs;                 /* ONU DS Sync OK - PCS        */
  uint32_t onuEponCtrlType;                    /* ONU Control Type            */
  uint8_t  onuEponMacAddr[EPON_API_MAC_LEN];   /* ONU MAC Address             */
  uint8_t  onuEponBcastAddr[EPON_API_MAC_LEN]; /* ONU MAC Broadcast Address   */
  uint32_t onuEponRxLLID;                      /* ONU Rx Packet Rx LLID Array */
  uint32_t onuEponTxLLID;                      /* ONU Rx Packet Tx LLID Array */
  uint32_t onuEponMode;                        /* ONU mode (EPON, P2P)        */
}S_EponIoctlInfo;

/* Init */
typedef struct
{
  uint32_t ponXvrBurstEnPolarity;
  uint32_t ponXvrPolarity;
  uint32_t p2pXvrBurstEnPolarity;
  uint32_t p2pXvrPolarity;
  uint32_t dgPolarity;
  uint32_t pkt2kSupported;
  uint8_t  macAddr[EPON_API_MAX_NUM_OF_MAC][EPON_API_MAC_LEN];
}S_EponIoctlInit;

/* FEC */
typedef struct
{
  uint32_t rxGenFecEn;
  uint32_t txGenFecEn;
  uint32_t txMacFecEn[EPON_API_MAX_NUM_OF_MAC];
}S_EponIoctlFec;

/* Encryption */
typedef struct
{
  uint32_t macId;
  uint32_t encEnable;
  uint32_t encKey;
  uint32_t encKeyIndex;
}S_EponIoctlEnc;

/* OAM Transmission */
typedef struct
{
  uint32_t length;
  uint8_t  data[EPON_API_MAX_OAM_FRAME_SIZE];
}S_EponOamFrame;

typedef struct
{
  uint32_t       macId;
  S_EponOamFrame oamFrame;
}S_EponIoctlOamTx;

/* DBA */
typedef struct
{
  uint8_t  state;	    /* Is threshold configuration active for the queue */
  uint16_t threshold;   /* Queue threshold: range 0 - 65535                */
}S_EponDbaThreshold;

typedef struct
{
  uint32_t           numOfQueueSets;
  uint32_t           numOfQueues;
  uint32_t           aggrThreshold;
  S_EponDbaThreshold threshold[MAX_QUEUE_NUMBER][MAX_THRESHOLD_SET_RER_QUEUE];
}S_EponIoctlDbaLlid;

typedef struct
{
  uint8_t            validLlid[EPON_API_MAX_NUM_OF_MAC];
  S_EponIoctlDbaLlid dbaLlid[EPON_API_MAX_NUM_OF_MAC];
}S_EponIoctlDba;

/* Holdover */
typedef struct
{
  uint32_t holdoverState; /* 0 - Deactivate, 1 - Activate */
  uint32_t holdoverTime;
}S_EponIoctlHoldOver;

/* Silence */
typedef struct
{
  uint32_t silenceState; /* 0 - Deactivate, 1 - Activate */
}S_EponIoctlSilence;

/* Authentication */
typedef struct
{
  uint8_t loid[24];
  uint8_t password[12];
}S_EponIoctlAuth;

/* Global functions
------------------------------------------------------------------------------*/
/*******************************************************************************
* mvEponInit()
*
* DESCRIPTION:      configure Epon module init
*
* INPUTS:			S_EponIoctlInit *init_config
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponInit(S_EponIoctlInit *init_config);

/*******************************************************************************
* mvEponOamChannelInit()
*
* DESCRIPTION:      Initialization of an OAM channel for EPON.
*
* INPUTS:			uint32_t tx_queue_bitmap (bitmap for queue per LLID:
*                               BIT3..BIT0 = Queue_number of LLID_0
*                               BIT7..BIT4 = Queue_number of LLID_1   , etc.)
*                               not used LLID => fill in 0xF for queue number
*                   uint32_t rx_queue
*                   uint32_t config_bitmap
*                               bit#0   => 0 = non-blocking socket created
*                                          1 = blocking socket
*                               bit1-31 => not used
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponOamChannelInit(uint32_t tx_queue_bitmap, uint32_t rx_queue, uint32_t config_bitmap);


/*******************************************************************************
* mvEponOamChannelUpdate()
*
* DESCRIPTION:      Update of an LLID per Queue for EPON.
*
* INPUTS:			uint32_t tx_queue_bitmap (bitmap for queue per LLID:
*                               BIT3..BIT0 = Queue_number of LLID_0
*                               BIT7..BIT4 = Queue_number of LLID_1   , etc.)
*                               not used LLID => fill in 0xF for queue number
*                   uint32_t rx_queue
*                   uint32_t config_bitmap
*                               bit#0   => 0 = non-blocking socket created
*                                          1 = blocking socket
*                               bit1-31 => not used
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponOamChannelUpdate(uint32_t tx_queue_bitmap, uint32_t rx_queue, uint32_t config_bitmap);

/*******************************************************************************
* mvEponSendOamFrame()
*
* DESCRIPTION:      Transmit OAM frame to specific LLID.
*
* INPUTS:			uint8_t   llid
*                   uint8_t  *data
*                   uint16_t  length
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSendOamFrame(uint8_t llid, uint8_t *data, uint16_t length);

/*******************************************************************************
* mvEponReceiveOamFrame()
*
* DESCRIPTION:      Receive OAM frame from specific LLID.
*
* INPUTS:			uint8_t  *llid
*                   uint8_t  *data
*                   uint16_t *length
*                   uint32_t timeout    - timeout in msec
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponReceiveOamFrame(uint8_t *llid, uint8_t *data, uint16_t *length, uint32_t timeout);

/*******************************************************************************
* mvChannelClose()
*
* DESCRIPTION:      Close the management communication socket (for OAM / OMCI)
*
* INPUTS:	        none
*
* OUTPUTS:			none
*
* RETURNS:          none
*
*******************************************************************************/
void mvChannelClose(void);

/*******************************************************************************
* mvEponSetDbaParameter()
*
* DESCRIPTION:      configure DBA parameters
*                   it scan all valid LLID according to input parameters.
*					For each LLID it configures queue threshold and queue state according
*					to the number of Queuesets and Queues from the input structure.
*					it execute Report Validation Check as follows
*					 Number of bytes for queue report = 39 bytes (64 minus mac, type, fcs, etc')
*					 Formula - Number of Queueset X
*							   Number of Queues   X
*							   Queue bytes [2]    +
*							   Number of reports ( == Number of Queueset)
*					 Valid values: 2 X 8 X 2 + 3 = 34Bytes
*								   3 X 6 X 2 + 3 = 39bytes
*								   4 X 4 X 2 + 4 = 36bytes
*
* INPUTS:			S_EponIoctlDba *dba_para
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSetDbaParameter(S_EponIoctlDba *dba_para);

/*******************************************************************************
* mvEponGetDbaParameter()
*
* DESCRIPTION:      return DBA parameters
*
* INPUTS:			S_EponIoctlDba *dba_para
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponGetDbaParameter(S_EponIoctlDba *dba_para);

/*******************************************************************************
* mvEponSetHoldover()
*
* DESCRIPTION:      configure EPON MAC holdover time and state
*
* INPUTS:			uint32_t holdoverTime
*                   uint32_t holdoverState
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSetHoldover(uint32_t holdoverTime, uint32_t holdoverState);

/*******************************************************************************
* mvEponGetHoldover()
*
* DESCRIPTION:      return EPON MAC holdover time and state
*
* INPUTS:			uint32_t *holdoverTime
*                   uint32_t *holdoverState
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponGetHoldover(uint32_t *holdoverTime, uint32_t *holdoverState);

/*******************************************************************************
* mvEponSetSilence()
*
* DESCRIPTION:      configure EPON MAC silence state
*
* INPUTS:			uint32_t silenceState
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSetSilence(uint32_t silenceState);

/*******************************************************************************
**
**  mvEponGetAlarm
**  ____________________________________________________________________________
**
**  DESCRIPTION: The function get EPON alarm(PON LOS)
**
**  PARAMETERS:  None
**
**  OUTPUTS:     alarm bit0:XVR, alarm bit1:SERDES
**
**  RETURNS:     None
**
*******************************************************************************/
mrvlErrorCode_t mvEponGetAlarm(uint32_t *alarm);

/*******************************************************************************
* mvEponGetOnuAuth()
*
* DESCRIPTION:      configure EPON MAC autentication parameters including
*                   loid, and password
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponGetOnuAuth(S_EponIoctlAuth *auth);

/*******************************************************************************
* mvEponEnableEncryption()
*
* DESCRIPTION:      enable EPON MAC encryption
*
* INPUTS:			uint32_t enable
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponEnableEncryption(uint32_t enable);

/*******************************************************************************
* mvEponSetEncryptionKey()
*
* DESCRIPTION:      change EPON MAC encryption key per llid
*
* INPUTS:			uint32_t llId
*                   uint32_t keyVal
*                   uint32_t keyIdx
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSetEncryptionKey(uint32_t llId, uint32_t keyVal, uint32_t keyIdx);

/*******************************************************************************
* mvEponFecConfig()
*
* DESCRIPTION:      config EPON MAC FEC
*
* INPUTS:			S_EponIoctlFec *ioctlFec
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponFecConfig(S_EponIoctlFec *ioctlFec);

/*******************************************************************************
* mvEponGetInfo()
*
* DESCRIPTION:      return EPON MAC information
*
* INPUTS:			S_EponIoctlInfo *ioctlInfo
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponGetInfo(S_EponIoctlInfo *ioctlInfo);

/*******************************************************************************
* mvEponGetPmCnt()
*
* DESCRIPTION:      return EPON MAC PM information
*
* INPUTS:			S_EponIoctlPm *ioctlCnts
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponGetPmCnt(S_EponIoctlPm *ioctlCnts);

/*******************************************************************************
* mvEponSetLlidAdmin()
*
* DESCRIPTION:      reset gem port monitor list counters
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSetLlidAdmin(uint8_t numberOfLlidActived);


/******************************************************************************/
/* ========================================================================== */
/* ========================================================================== */
/* ==                                                                      == */
/* ==           ==     ==   =========   =========   ===       ==           == */
/* ==           ==     ==   =========   =========   ====      ==           == */
/* ==           ==     ==   ==     ==   ==     ==   == ==     ==           == */
/* ==           ==     ==   ==     ==   ==     ==   ==  ==    ==           == */
/* ==           ==     ==   =========   ==     ==   ==   ==   ==           == */
/* ==           ==     ==   =========   ==     ==   ==    ==  ==           == */
/* ==           ==     ==   ==          ==     ==   ==     == ==           == */
/* ==           ==     ==   ==          ==     ==   ==      ====           == */
/* ==           =========   ==          =========   ==       ===           == */
/* ==           =========   ==          =========   ==        ==           == */
/* ==                                                                      == */
/* ========================================================================== */
/* ========================================================================== */
/******************************************************************************/
/******************************************************************************/

/* Definitions
------------------------------------------------------------------------------*/
#define MVPON_IOCTL_START         _IOW(MVPON_IOCTL_MAGIC, 30, unsigned int)
#define MVPON_IOCTL_MODE_GET      _IOR(MVPON_IOCTL_MAGIC, 31, unsigned int)

/* Enums
------------------------------------------------------------------------------*/
typedef enum {
	E_PON_DRIVER_UNDEF_MODE	= 0,
	E_PON_DRIVER_EPON_MODE	= 1,
	E_PON_DRIVER_GPON_MODE	= 2,
	E_PON_DRIVER_MAX_MODE
} E_PonDriverMode;

#endif /* _ONU_PON_MNG_INTERFACE_H */

