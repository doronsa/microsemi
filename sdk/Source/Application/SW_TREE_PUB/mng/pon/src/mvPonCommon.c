/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "globals.h"
#include "ponOnuMngIf.h"
#include "ponOnuInternals.h"
#include "qprintf.h"
#include "mng_trace.h"
#include "OsGlueLayer.h"

int    ponDrvFd = -1;

int                         sockfd_pon = 0;
struct sockaddr_ll          sockAddress_pon = {0, 0, 0, 0, 0, 0, 0};
static  struct ifreq        ifReq;
static  char               *ponIfName = "pon0";

mrvl_pon_statistics_type    mrvl_mngmnt_counters = {0, 0, 0, 0};

GL_SEMAPHORE_ID ponApiSemId = NULL;
GL_SEMAPHORE_ID ponCfgFileSemId = NULL;


/*******************************************************************************
* mvPonDrvFdGet()
*
* DESCRIPTION:   Get Pon driver file descriptor
*
* INPUTS:       
*
*
* OUTPUTS:      
*
*
* RETURNS:      
*           Pon driver file descriptor
*
* COMMENTS:     
*
*******************************************************************************/
int mvPonDrvFdGet(void)
{
	if (NULL == ponApiSemId)
	{
		/* Initialized as sem_full */
		if (osSemCreate(&ponApiSemId, "/PonApiSem", 1, SEM_Q_FIFO, TRUE) != IAMBA_OK)
		{
			printf("Failed to create PON API semaphore\n\r");													 
			return -1; 
		}	
	}

	if (NULL == ponCfgFileSemId)
	{
		/* Initialized as sem_full */
		if (osSemCreate(&ponCfgFileSemId, "/PonFileSem", 1, SEM_Q_FIFO, TRUE) != IAMBA_OK)
		{
			printf("Failed to create PON File semaphore\n\r");													 
			return -1; 
		}	
	}

	if (ponDrvFd >= 0)
	{
		return ponDrvFd;
	}
	
	ponDrvFd = open("/dev/pon", O_RDWR);
	if (ponDrvFd < 0)
	{
		printf("Failed to open /dev/pon\n\r");
		return(-1);
	}

    return(ponDrvFd);
}


/*******************************************************************************
* mvPonTransceiverDiagnosisGet()
*
* DESCRIPTION:      return transceiver diagnosis parameters 
*
* INPUTS:			E_PonTransceiverDiagnosis *transceiver_diagnosis
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK. 
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvPonTransceiverDiagnosisGet(S_PonTransceiverDiagnosis *transceiver_diagnosis)
{
  qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: NOT implemented yet\n\r", __FUNCTION__);
  return(MRVL_OK);
}

/*******************************************************************************
* mvPonSlaSet()
*
* DESCRIPTION:      configure pon device SLA parameters
*
* INPUTS:		    E_PonSlaBestEffortSchedulingScheme bfSched    	
*                   uint32_t                           cycleLength
*                   uint32_t                           serviceNum 
*                   E_PonSlaScheme                    *slaPara   
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK. 
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvPonSlaSet(E_PonSlaBestEffortSchedulingScheme bfSched, 
                            uint32_t                           cycleLength, 
                            uint32_t                           serviceNum, 
                            S_PonSlaScheme                    *slaPara)
{
  qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: NOT implemented yet\n\r", __FUNCTION__);
  return(MRVL_OK);
}


/*******************************************************************************
* mvPonChannelInit()
*
* DESCRIPTION:   Create socket for TX / RX - EOAM and OMCI 
*
* INPUTS:       uint32_t config_bitmap 
*               bit#0 => '1' = socket is blocking / '0' = socket is non-blocking
*
* OUTPUTS:      
*
*
* RETURNS:      
*           MRVL_EXIT_OK or error code
*
* COMMENTS:     
*
*******************************************************************************/
mrvlErrorCode_t   mvPonChannelInit (uint32_t config_bitmap)
{
    int                 onFlag = 1;     /* socket is non-blocking */            
    struct timeval      sockTimer;

    if (config_bitmap & MRVL_SOCKET_BLOCKING_MASK == 1) {
        onFlag = 0;         /* socket is blocking*/
    }

    /* Open Socket for protocol ETH_PROTO_eOAM = 0xBABA */
    sockfd_pon = socket(AF_PACKET, SOCK_RAW, htons(ETH_PROTO_eOAM_OMCI));
    if (sockfd_pon < 0) 
    {
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: socket(AF_PACKET, SOCK_RAW, htons(0x%x)) failed. errno = %d, %s", __FUNCTION__, ETH_PROTO_eOAM_OMCI, errno, strerror(errno));
        return MRVL_ERROR_HARDWARE;
    }

    if (onFlag == 1) {
        /* Non-blocking socket */
        if (ioctl(sockfd_pon, FIONBIO, &onFlag) < 0)
        {
            qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: ioctl(sockfd_pon, FIONBIO, &onFlag= %d) failed. ", __FUNCTION__,  onFlag);
            return MRVL_ERROR_HARDWARE;
        }

    }
    else {
        /* Useful for blocking socket - allows a vitality counter */
        sockTimer.tv_sec  = 30;
        sockTimer.tv_usec = 0;
        if (setsockopt(sockfd_pon, SOL_SOCKET, SO_RCVTIMEO, &sockTimer, sizeof(struct timeval)) < 0)
        {
            qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: setsockopt(sockfd_pon, SOL_SOCKET, SO_RCVTIMEO, &onFlag= %d) failed.", __FUNCTION__,  onFlag);
            return MRVL_ERROR_HARDWARE;
        }

    }

    /* Set interface for transmit */
    strncpy(ifReq.ifr_name, ponIfName, sizeof(ifReq.ifr_name));
    if (ioctl(sockfd_pon, SIOCGIFINDEX, &ifReq) < 0) 
    {
        qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: ioctl(sockfd_pon,SIOCGIFINDEX,&ifReq) failed. errno = %d, %s", __FUNCTION__,  errno, strerror(errno));
        return MRVL_ERROR_HARDWARE;
    }

    /* Set Transmit params */
    sockAddress_pon.sll_ifindex  = ifReq.ifr_ifindex;
    sockAddress_pon.sll_family   = AF_PACKET;
    sockAddress_pon.sll_halen    = 6;
    sockAddress_pon.sll_protocol = htons(ETH_PROTO_eOAM_OMCI);

    return MRVL_EXIT_OK;
}



/*******************************************************************************
* mvChannelClose()
*
* DESCRIPTION:   Create socket for TX / RX - EOAM and OMCI 
*
* INPUTS:       none
*
* OUTPUTS:      
*
*
* RETURNS:      
*           MRVL_EXIT_OK or error code
*
* COMMENTS:     
*
*******************************************************************************/
void   mvChannelClose (void) 
{
    if (sockfd_pon != 0) {      
        close(sockfd_pon);
    }
    qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: ioctl(sockfd_pon,SIOCGIFINDEX,&ifReq) failed. errno = %d, %s", __FUNCTION__,  errno, strerror(errno));

    return;
}


/*******************************************************************************
* mvDbgPrintStatistics()
*
* DESCRIPTION:   Debug function - Print packet TX / RX statistics 
*
* INPUTS:       none
*
* OUTPUTS:      
*
*
* RETURNS:      
*           MRVL_EXIT_OK or error code
*
* COMMENTS:     
*
*******************************************************************************/
void mvDbgPrintStatistics(void) {
    printf("\n");
    printf("====TX = STATISTICS==========\n ");
    printf("     TX_total_frames= %d \n", mrvl_mngmnt_counters.tx_total_frames);
    printf("     TX_failures = %d \n", mrvl_mngmnt_counters.tx_failures);
    printf("====RX = STATISTICS==========\n ");
    printf("     RX_total_frames= %d \n", mrvl_mngmnt_counters.rx_total_frames);
    printf("     RX_failures = %d \n", mrvl_mngmnt_counters.rx_failures);
    printf("=============================\n");
}

