/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "globals.h"
#include "ponOnuMngIf.h"
#include "ponOnuInternals.h"
#include "qprintf.h"
#include "mng_trace.h"
#include "OsGlueLayer.h"
#include "params_mng.h"

static  int  omci_init_done = 0;

extern int                          sockfd_pon;
extern struct sockaddr_ll           sockAddress_pon;
extern mrvl_pon_statistics_type     mrvl_mngmnt_counters;

extern GL_SEMAPHORE_ID ponApiSemId;

/*******************************************************************************
* module_gponSetup()
*
* DESCRIPTION:   GPON initialization
*
* INPUTS:
*
*
* OUTPUTS:
*
*
* RETURNS:
*           TRUE or FALSE
*
* COMMENTS:
*
*******************************************************************************/
int module_gponSetup(void)
{
	int             fd;
	S_GponIoctlInfo init;
	UINT32          *param[PON_XML_DEF_PARAM_MAX] = {0};
	UINT8           defSn[US_XML_PON_SN_LEN+1];
	UINT8           defPass[US_XML_PON_PASSWD_LEN+1];
	UINT32          defDis = 0;
	UINT32          defGemRemove = 0;
	UINT32          defTcontRemove = 0;
	UINT32          defSnSrc = 0;
    UINT32          ponXvrBurstEnPolarity = 0;
    UINT32          ponXvrPolarity = 0;
    UINT32          p2pXvrBurstEnPolarity = 0;
    UINT32          p2pXvrPolarity = 0;
	UINT32          dgPol = 0;
	UINT32          defGemRestore = 0;
    UINT32          defFecHyst = 0;
    UINT32          defCouplingMode = 0;    /* For AC Coupling Mode */

	param[PON_XML_DEF_PAR_SERIAL_NUM]		= (UINT32 *)defSn;
	param[PON_XML_DEF_PAR_PASSWORD]			= (UINT32 *)defPass;
	param[PON_XML_DEF_PAR_DIS_SERIAL_NUM]	= &defDis;
	param[PON_XML_DEF_PARAM_CLEAR_GEM]		= &defGemRemove;
	param[PON_XML_DEF_PARAM_CLEAR_TCONT]	= &defTcontRemove;
	param[PON_XML_DEF_PARAM_SERIAL_NUM_SRC]	= &defSnSrc;
	param[PON_XML_DEF_PARAM_XVR_BURST_ENABLE_POLARITY]	= &ponXvrBurstEnPolarity;
    param[PON_XML_DEF_PARAM_XVR_POLARITY]   = &ponXvrPolarity;
    param[P2P_XML_DEF_PARAM_XVR_BURST_ENABLE_POLARITY]  = &p2pXvrBurstEnPolarity;
    param[P2P_XML_DEF_PARAM_XVR_POLARITY]   = &p2pXvrPolarity;
	param[PON_XML_DEF_PARAM_DG_POLARITY]	= &dgPol;
	param[PON_XML_DEF_PARAM_RESTORE_GEM]	= &defGemRestore;
    param[PON_XML_DEF_PARAM_FEC_HYST]       = &defFecHyst;
    param[PON_XML_DEF_PARAM_COUPLING_MODE]  = &defCouplingMode;

	if (get_pon_def_params((void **)param) != 0) {
		qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "Failed to get default PON parameters\n");
		return(MRVL_ERROR_EXIT);
	}

	memcpy(init.serialNum, defSn,   US_XML_PON_SN_LEN);
	memcpy(init.password,  defPass, US_XML_PON_PASSWD_LEN);
	init.disableSn       = defDis;
	init.clearGem        = defGemRemove;
	init.clearTcont      = defTcontRemove;
	init.serialNumSource = defSnSrc;
    init.ponXvrBurstEnPolarity = ponXvrBurstEnPolarity;
    init.ponXvrPolarity  = ponXvrPolarity;
    init.p2pXvrBurstEnPolarity = p2pXvrBurstEnPolarity;
    init.p2pXvrPolarity  = p2pXvrPolarity;
	init.dgPolarity      = dgPol;
	init.restoreGem      = defGemRestore;
    init.fecHyst         = defFecHyst;
    init.couplingMode    = defCouplingMode;

	fd = mvPonDrvFdGet();
	if (fd < 0) {
		qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: Failed to open /dev/pon\n\r", __FUNCTION__);
		return(MRVL_ERROR_EXIT);
	}

	if (ioctl(fd, MVGPON_IOCTL_INIT, &init) < 0) {
		qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: Failed to init /dev/pon\n\r", __FUNCTION__);
		return(MRVL_ERROR_EXIT);
	}

	return(MRVL_OK);
}

/*******************************************************************************
* mvGponOmciChannelInit()
*
* DESCRIPTION:      Initialization of an OMCI channel for GPON.
*
* INPUTS:	        uint16_t  gemPort
*                   uint8_t   tcontNum
*                   uint8_t   tx_queue
*                   uint32_t  config_bitmap
*                               bit#0   => 0 = non-blocking socket created
*                                          1 = blocking socket
*                               bit1-31 => not used
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponOmciChannelInit(uint16_t gemPort, uint8_t tcontNum, uint8_t tx_queue, uint8_t rx_queue, uint32_t config_bitmap)
{
	mrvlErrorCode_t     rc = MRVL_ERROR_EXIT;
    tpm_error_code_t    tpm_rc;
    uint32_t            owner_id = 0;

    /* do not permit twice the call to INIT channel */
    if (omci_init_done == 1){
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: mrvl_mngmnt_channel_init FAILED - config_bitmap (%d) \n\r", __FUNCTION__, config_bitmap);
        return rc;
    }

    omci_init_done = 1;

    /* socket initialization and treatment */
    rc = mvPonChannelInit(config_bitmap);
    if (rc != MRVL_EXIT_OK) {
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: mrvl_mngmnt_channel_init FAILED - config_bitmap (%d) \n\r", __FUNCTION__, config_bitmap);
        return rc;
    }

    /* set port / queue  for OMCI mv_cust */
    /* for OMCI - no MH in packet */
    /* call to mv_cust_omci_enable is inside the set function */
    rc = mv_cust_omci_set(tcontNum, tx_queue, gemPort, 0);
    if (rc != MRVL_EXIT_OK) {
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: mv_cust_omci_set FAILED - tcontNum(%d) tx_queue(%d) gemPort(%d) \n\r", __FUNCTION__, tcontNum, tx_queue, gemPort);
        return rc;
    }

    tpm_rc = tpm_omci_add_channel(owner_id,(tpm_gem_port_key_t)gemPort, rx_queue, tcontNum, tx_queue);
    if (tpm_rc != TPM_RC_OK){
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: tpm_omci_add_channel FAILED - owner_id(%d) gemPort(%d) tcontNum(%d) tx_queue(%d) \n\r",
                __FUNCTION__, owner_id, gemPort, tcontNum, tx_queue);
        return MRVL_ERROR_HARDWARE;
    }


    return MRVL_EXIT_OK;
}

/*******************************************************************************
* mvGponOmciChannelUpdate()
*
* DESCRIPTION:      Update the OMCI channel for GPON.
*
* INPUTS:	        uint16_t gemPort
*                   uint8_t  tcontNum
*                   uint8_t  tx_queue
*                   uint8_t  rx_queue
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponOmciChannelUpdate(uint16_t gemPort, uint8_t tcontNum, uint8_t tx_queue, uint8_t rx_queue)
{
    mrvlErrorCode_t     rc;
    tpm_error_code_t    tpm_rc;
    uint32_t            owner_id = 0;

    tpm_rc = tpm_omci_del_channel(owner_id);
    if (tpm_rc != TPM_RC_OK){
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: tpm_omci_del_channel FAILED - owner_id (%d) \n\r", __FUNCTION__, owner_id);
        return MRVL_ERROR_HARDWARE;
    }

    tpm_rc = tpm_omci_add_channel(owner_id,(tpm_gem_port_key_t)gemPort, rx_queue, tcontNum, tx_queue);
    if (tpm_rc != TPM_RC_OK){
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: tpm_omci_add_channel FAILED - owner_id(%d) gemPort(%d) tcontNum(%d) tx_queue(%d) \n\r",
                __FUNCTION__, owner_id, gemPort, tcontNum, tx_queue);
        return MRVL_ERROR_HARDWARE;
    }

    /* set port / queue  for OMCI mv_cust */
    /* for OMCI - no MH in packet */
    rc = mv_cust_omci_set(tcontNum, tx_queue, gemPort, 0);
    if (rc != MRVL_EXIT_OK) {
        return rc;
    }
    return MRVL_EXIT_OK;
}

/*******************************************************************************
* mvGponSendOmciFrame()
*
* DESCRIPTION:      Transmit OMCI frame to specific T-CONT.
*
* INPUTS:			uint8_t  *data
*                   uint16_t  length
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponSendOmciFrame(uint8_t *data, uint16_t length)
{
    int                 i, sendlength;
    mrvlErrorCode_t     rc;

    rc = MRVL_EXIT_OK;

    if (length <= 0) {
        return MRVL_ERROR_HARDWARE;
    }

    /* check the socket exists - otherwise return ERROR to APP */
   if (sockfd_pon <= 0) {
       qprintf( MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: TX failed - socket not initialized. \n\r", __FUNCTION__);
       return MRVL_ERROR_NOT_INITIALIZED;
   }

    /* Transmit frame */
    sendlength = sendto(sockfd_pon, data, length, 0, (struct sockaddr *) &sockAddress_pon, sizeof(sockAddress_pon));
    if (sendlength == -1)
    {
        mrvl_mngmnt_counters.tx_failures++;
        qprintf( MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: sendto() failed, errno = %d, %s\n\r", __FUNCTION__, errno, strerror(errno));
        rc = MRVL_ERROR_TIME_OUT;
    }

    return rc;
}

/*******************************************************************************
* mvGponReceiveOmciFrame()
*
* DESCRIPTION:      Receive OMCI frame
*
* INPUTS:			uint8_t  *data
*                   uint16_t *length
*                   uint32_t timeout     - timeout in msec
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponReceiveOmciFrame(uint8_t *data, uint16_t *length, uint32_t timeout)
{
    int                 recvlength;
    mrvlErrorCode_t     ret_code;
    uint8_t             tmsg[128], i;

    ret_code = MRVL_EXIT_OK;

    /* check the socket exists - otherwise return ERROR to APP */
    if (sockfd_pon <= 0) {
        qprintf( MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: RX failed - socket not initialized. \n\r", __FUNCTION__);
        return MRVL_ERROR_NOT_INITIALIZED;
    }

    /* Blocking socket*/
    /* Read frame*/
    recvlength = recv(sockfd_pon, tmsg, sizeof(tmsg), 0);
    if (recvlength == -1) {
        ret_code = MRVL_ERROR_TIME_OUT;
    }
    else
    {
		if (recvlength <= 128)
	        *length = recvlength;
		else
			*length = 128;
  	    memcpy(data, tmsg, *length);
        mrvl_mngmnt_counters.rx_total_frames++;

    }

    return ret_code;
}

/*******************************************************************************
* mvGponInit()
*
* DESCRIPTION:      configure Gpon module init: serial number, password,
*                   and disable SN status
*
* INPUTS:			uint8_t  *sn
*                   uint8_t  *pswd
*                   uint32_t  dis
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponInit(uint8_t *sn, uint8_t *pswd, uint32_t dis, uint32_t snSrc)
{
   int             fd;
   S_GponIoctlInfo init;

   memcpy(init.serialNum, sn,   US_XML_PON_SN_LEN);
   memcpy(init.password,  pswd, US_XML_PON_PASSWD_LEN);
   init.disableSn       = dis;
   init.serialNumSource = snSrc;

   fd = mvPonDrvFdGet();
   if (fd < 0)
   {
     qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: Failed to open /dev/pon\n\r", __FUNCTION__);
     return(MRVL_ERROR_EXIT);
   }

   if (ioctl(fd, MVGPON_IOCTL_INIT, &init) < 0)
   {
     qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: Failed to init /dev/pon\n\r", __FUNCTION__);
     return(MRVL_ERROR_EXIT);
   }

   return(MRVL_OK);
}

/*******************************************************************************
* mvGponGetInfo()
*
* DESCRIPTION:      return Gpon module info, including onu Id, state,
*                   signal detect status, DS sync status, omcc port, serial number,
*                   password, disable SN status
*
* INPUTS:			S_GponIoctlInfo *ioctlInfo
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponGetInfo(S_GponIoctlInfo *ioctlInfo)
{
  int fd;

  fd = mvPonDrvFdGet();
  if (fd < 0)
      return(MRVL_ERROR_EXIT);

  osSemTake(ponApiSemId, GL_SUSPEND);
  if (ioctl(fd, MVGPON_IOCTL_INFO, ioctlInfo) < 0)
  {
      qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
	  osSemGive(ponApiSemId);
      return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvGponSetTcont()
*
* DESCRIPTION:      set connectivity between tcont and alloc Id
*
* INPUTS:			uint32_t tcontNum
*                   uint32_t allocId
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponSetTcont(uint32_t tcontNum, uint32_t allocId)
{
  int             fd;
  S_GponIoctlData tcontAllocIdSet;

  fd = mvPonDrvFdGet();
  if (fd < 0)
    return(MRVL_ERROR_EXIT);

  tcontAllocIdSet.tcont = tcontNum;
  tcontAllocIdSet.alloc = allocId;

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVGPON_IOCTL_DATA_TCONT_CONFIG, &tcontAllocIdSet) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
	osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvGponResetTconts()
*
* DESCRIPTION:      clear all tcont configuration
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponResetTconts(void)
{
  int fd;

  fd = mvPonDrvFdGet();
  if(fd < 0)
    return(MRVL_ERROR_EXIT);

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVGPON_IOCTL_DATA_TCONT_RESET) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
	osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }

  osSemGive(ponApiSemId);
  return(MRVL_OK);
}

/*******************************************************************************
* mvGponClearTcont()
*
* DESCRIPTION:      clear specified tcont configuration
*
* INPUTS:			tcontNum
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponClearTcont(uint32_t tcontNum)
{
    int fd;
    S_GponIoctlData tcontClear;

    fd = mvPonDrvFdGet();
    if(fd < 0)
    {
        return(MRVL_ERROR_EXIT);
    }

    tcontClear.tcont = tcontNum;

    osSemTake(ponApiSemId, GL_SUSPEND);
    if(ioctl(fd, MVGPON_IOCTL_DATA_TCONT_CLEAR, &tcontClear) < 0)
    {
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(ponApiSemId);
        
        return(MRVL_ERROR_EXIT);
    }
    osSemGive(ponApiSemId);

    return(MRVL_OK);
}

/*******************************************************************************
* mvGponRemoveGemPort()
*
* DESCRIPTION:      remove gem port form gem port monitor list
*
* INPUTS:			uint32_t* gemmap
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponRemoveGemPort(uint32_t* gemmap)
{
  int             fd;
  S_GponIoctlGem  gemPortSet;

  fd = mvPonDrvFdGet();
  if(fd < 0)
    return(MRVL_ERROR_EXIT);

  gemPortSet.action = E_GPON_IOCTL_GEM_REMOVE;
  memset(gemPortSet.gemmap, 0, sizeof(uint32_t)*(ONU_GPON_MAX_GEM_PORTS/32));
  memcpy(gemPortSet.gemmap, gemmap,sizeof(uint32_t)*(ONU_GPON_MAX_GEM_PORTS/32));

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVGPON_IOCTL_GEM, &gemPortSet) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
	osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvGponAddGemPort()
*
* DESCRIPTION:      monitor specific gem port counters, up to 8 gem port counters
*                   can be monitored, valid only in Rx direction
*
* INPUTS:			uint32_t* gemmap
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponAddGemPort(uint32_t* gemmap)
{
  int             fd;
  S_GponIoctlGem  gemPortSet;

  fd = mvPonDrvFdGet();
  if(fd < 0)
    return(MRVL_ERROR_EXIT);

  gemPortSet.action = E_GPON_IOCTL_GEM_ADD;
  memset(gemPortSet.gemmap, 0, sizeof(uint32_t)*(ONU_GPON_MAX_GEM_PORTS/32));
  memcpy(gemPortSet.gemmap, gemmap,sizeof(uint32_t)*(ONU_GPON_MAX_GEM_PORTS/32));

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVGPON_IOCTL_GEM, &gemPortSet) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
	osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvGponResetGemPort()
*
* DESCRIPTION:      remove all gem ports from gem port monitor list
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponResetGemPort(void)
{
  int             fd;
  S_GponIoctlGem  gemPortSet;

  fd = mvPonDrvFdGet();
  if(fd < 0)
    return(MRVL_ERROR_EXIT);

  gemPortSet.action = E_GPON_IOCTL_GEM_CLEARALL;
  memset(gemPortSet.gemmap, 0, sizeof(uint32_t)*(ONU_GPON_MAX_GEM_PORTS/32));

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVGPON_IOCTL_GEM, &gemPortSet) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
	osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvGponReadPmGemPort()
*
* DESCRIPTION:      return monitored gem port rx counters
*
* INPUTS:			uint16_t                       gemPort
*                   S_GponIoctlGemPortMibCounters *gponIoctlGemPortMibCounters
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponReadPmGemPort(uint16_t gemPort,
									S_GponIoctlGemPortMibCounters *gponIoctlGemPortMibCounters)
{
  int fd;

  fd = mvPonDrvFdGet();
  if(fd < 0)
  {
    printf("%s: mvPonDrvFdGet() failed\n\r", __FUNCTION__);
    return(MRVL_ERROR_EXIT);
  }

  gponIoctlGemPortMibCounters->gem_port = gemPort;

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVGPON_IOCTL_GEMPORT_PM_GET, gponIoctlGemPortMibCounters) < 0)
  {
    printf("%s: MVGPON_IOCTL_GEMPORT_PM_GET failed\n\r", __FUNCTION__);
	osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvGponStartPmGemPort()
*
* DESCRIPTION:      start monitoring gem port counters
*
* INPUTS:			uint16_t gemPort
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponStartPmGemPort(uint16_t gemPort)
{
  int fd;
  S_GponIoctlGemPortPmConfig gponIoctlGemPortPmConfig;

  fd = mvPonDrvFdGet();
  if(fd < 0)
  {
    printf("%s: mvPonDrvFdGet() failed\n\r", __FUNCTION__);
    return(MRVL_ERROR_EXIT);;
  }

  gponIoctlGemPortPmConfig.command  = GEMPORTPMCMD_START;
  gponIoctlGemPortPmConfig.gem_port = gemPort;

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVGPON_IOCTL_GEMPORT_PM_CONFIG, &gponIoctlGemPortPmConfig) < 0)
  {
    printf("%s: MVGPON_IOCTL_GEMPORT_PM_CONFIG (start) failed\n\r", __FUNCTION__);
	osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvGponStopPmGemPort()
*
* DESCRIPTION:      stop monitoring gem port counters
*
* INPUTS:			uint16_t gemPort
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponStopPmGemPort(uint16_t gemPort)
{
  int fd;
  S_GponIoctlGemPortPmConfig gponIoctlGemPortPmConfig;

  fd = mvPonDrvFdGet();
  if(fd < 0)
  {
    printf("%s: mvPonDrvFdGet() failed\n\r", __FUNCTION__);
    return(MRVL_ERROR_EXIT);
  }

  gponIoctlGemPortPmConfig.command  = GEMPORTPMCMD_STOP;
  gponIoctlGemPortPmConfig.gem_port = gemPort;

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVGPON_IOCTL_GEMPORT_PM_CONFIG, &gponIoctlGemPortPmConfig) < 0)
  {
    printf("%s: MVGPON_IOCTL_GEMPORT_PM_CONFIG (stop) failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
	return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvGponResetPmAllGemPorts()
*
* DESCRIPTION:      reset gem port monitor list counters
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponResetPmAllGemPorts(void)
{
  int fd;

  fd = mvPonDrvFdGet();
  if(fd < 0)
  {
    printf("%s: mvPonDrvFdGet() failed\n\r", __FUNCTION__);
    return(MRVL_ERROR_EXIT);
  }

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVGPON_IOCTL_GEMPORT_PM_RESET) < 0)
  {
    printf("%s: MVGPON_IOCTL_GEMPORT_PM_RESET failed\n\r", __FUNCTION__);
	osSemGive(ponApiSemId);
	return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvGponTxBurstEnableParamsSet()
*
* DESCRIPTION:      set burst enable parameters
*
* INPUTS:			mask
*                   polarity
*                   delay
*                   enStop
*                   enStart
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponTxBurstEnableParamsSet(uint32_t mask,
                                             uint32_t polarity,
                                             uint32_t delay,
                                             uint32_t enStop,
                                             uint32_t enStart)
{
  int               fd;
  S_GponIoctlXvr    ioctlXvr;

  fd = mvPonDrvFdGet();
  if(fd < 0)
  {
    printf("%s: mvPonDrvFdGet() failed\n\r", __FUNCTION__);
    return(MRVL_ERROR_EXIT);
  }

  ioctlXvr.mask     = mask;
  ioctlXvr.polarity = polarity;
  ioctlXvr.delay    = delay;
  ioctlXvr.enStop   = enStop;
  ioctlXvr.enStart  = enStart;

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVGPON_IOCTL_BEN_INIT, &ioctlXvr) < 0)
  {
    printf("%s: MVGPON_IOCTL_BEN_INIT failed\n\r", __FUNCTION__);
	osSemGive(ponApiSemId);
	return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvGponGetAlarms()
*
* DESCRIPTION:      get GPON alarms
*
* INPUTS:			S_GponIoctlAlarm *ioctlAlarm
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponGetAlarms(S_GponIoctlAlarm *ioctlAlarm)
{
  int fd;

  fd = mvPonDrvFdGet();
  if (fd < 0)
      return(MRVL_ERROR_EXIT);

  osSemTake(ponApiSemId, GL_SUSPEND);
  if (ioctl(fd, MVGPON_IOCTL_ALARM, ioctlAlarm) < 0)
  {
      qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
	  osSemGive(ponApiSemId);
      return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvGponGetPMs()
*
* DESCRIPTION:      get GPON PMs
*
* INPUTS:			S_GponIoctlPm *ioctlPm
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvGponGetPMs(S_GponIoctlPm *ioctlPm)
{
  int fd;

  fd = mvPonDrvFdGet();
  if (fd < 0)
      return(MRVL_ERROR_EXIT);

  osSemTake(ponApiSemId, GL_SUSPEND);
  if (ioctl(fd, MVGPON_IOCTL_PM, ioctlPm) < 0)
  {
      qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
	  osSemGive(ponApiSemId);
      return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
**
**  mvGponPortIdSet
**  ____________________________________________________________________________
**
**  DESCRIPTION: The function set valid for port Id
**
**  PARAMETERS:  uint32_t  gemPort
**               uint32_t  state
**
**  OUTPUTS:     None
**
**  RETURNS:     mrvlErrorCode_t
**
*******************************************************************************/
mrvlErrorCode_t mvGponPortIdSet(uint32_t gemPort, uint32_t state)
{
    int             fd;
    S_GponIoctlData gemPortStateSet;

    fd = mvPonDrvFdGet();
    if (fd < 0)
      return(MRVL_ERROR_EXIT);

    gemPortStateSet.gemPort   = gemPort;
    gemPortStateSet.gemState  = state;

    osSemTake(ponApiSemId, GL_SUSPEND);
    if(ioctl(fd, MVGPON_IOCTL_GEMPORT_STATE_SET, &gemPortStateSet) < 0)
    {
      qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
      osSemGive(ponApiSemId);
      return(MRVL_ERROR_EXIT);
    }
    osSemGive(ponApiSemId);

    return(MRVL_OK);
}

/*******************************************************************************
**
**  mvGponPortIdGet
**  ____________________________________________________________________________
**
**  DESCRIPTION: The function return port Id valid state
**
**  PARAMETERS:  uint32_t gemPort
**
**  OUTPUTS:     uint32_t state
**
**  RETURNS:     mrvlErrorCode_t
**
*******************************************************************************/
mrvlErrorCode_t mvGponPortIdGet(uint32_t gemPort, uint32_t *state)
{
    int             fd;
    S_GponIoctlData gemPortStateSet;

    fd = mvPonDrvFdGet();
    if (fd < 0)
      return(MRVL_ERROR_EXIT);

    gemPortStateSet.gemPort   = gemPort;

    osSemTake(ponApiSemId, GL_SUSPEND);
    if(ioctl(fd, MVGPON_IOCTL_GEMPORT_STATE_GET, &gemPortStateSet) < 0)
    {
      qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
      osSemGive(ponApiSemId);
      return(MRVL_ERROR_EXIT);
    }
    osSemGive(ponApiSemId);

    *state = gemPortStateSet.gemState;

    return(MRVL_OK);

}

