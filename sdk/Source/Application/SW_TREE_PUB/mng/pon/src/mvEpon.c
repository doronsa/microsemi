/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "globals.h"
#include "ponOnuMngIf.h"
#include "ponOnuInternals.h"
#include "qprintf.h"
#include "mng_trace.h"
#include "common.h"
#include "OsGlueLayer.h"


extern int                          sockfd_pon;
extern struct sockaddr_ll           sockAddress_pon;
extern mrvl_pon_statistics_type     mrvl_mngmnt_counters;
extern GL_SEMAPHORE_ID ponApiSemId;


/*******************************************************************************
* module_eponSetup()
*
* DESCRIPTION:      configure Epon module setup
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
int module_eponSetup()
{
    S_EponIoctlInit init;
    mrvlErrorCode_t rc;
    uint32_t        pon_xvr_burst_en_pol  = 0;
    uint32_t        pon_xvr_pol = 0;
    uint32_t        p2p_xvr_burst_en_pol  = 0;
    uint32_t        p2p_xvr_pol = 0;
    uint32_t        dg_pol   = 0;
    uint32_t        pkt_2k_supported = 0;

    get_epon_def_params(&pon_xvr_burst_en_pol, &pon_xvr_pol, &p2p_xvr_burst_en_pol, &p2p_xvr_pol, &dg_pol, &pkt_2k_supported);
    
    init.ponXvrBurstEnPolarity = pon_xvr_burst_en_pol;
    init.ponXvrPolarity = pon_xvr_pol;
    init.p2pXvrBurstEnPolarity = p2p_xvr_burst_en_pol;
    init.p2pXvrPolarity = p2p_xvr_pol;
	init.dgPolarity  = dg_pol;
    init.pkt2kSupported = pkt_2k_supported;

    rc = mvEponInit(&init);

    return(rc);
}

/*******************************************************************************
* mvEponInit()
*
* DESCRIPTION:      configure Epon module init
*
* INPUTS:			S_EponIoctlInit *init_config
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponInit(S_EponIoctlInit *init_config)
{
  int fd;

  fd = mvPonDrvFdGet();
  if(fd < 0)
    return(MRVL_ERROR_EXIT);

  if (ioctl(fd, MVEPON_IOCTL_INIT, init_config) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    return(MRVL_ERROR_EXIT);
  }

  return(MRVL_OK);
}


/*******************************************************************************
* mvEponOamDbInit()
*
* DESCRIPTION:      Initialization of an OAM DB for EPON.
*
* INPUTS:			uint32_t tx_queue_bitmap (bitmap for queue per LLID, llid0_queue= BIT3..BIT0, etc.)
*                   uint32_t config_bitmap (for future purposes)
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvl_error_code_t mvEponOamDbInit(uint32_t tx_queue_bitmap, uint32_t config_bitmap)
{
    int             i,j, queue_num;
    S_EponIoctlInfo eponIoctlInfo;

    /* validate queue - according to the following rules:
       - q=0x0-0x7 - valid - ok
       - q=0x8-0xE - invalid - return error
       - q=0xF - indication that LLID is not used */
    for (i=0; i< EPON_API_MAX_NUM_OF_MAC;i++) {
        /* extract Q from param tx_queue_bitmap*/
        queue_num = ( tx_queue_bitmap >> (4*i) ) & MRVL_EOAM_QUEUE_MASK;
        if ( (queue_num <0) || (queue_num>8) ) {
            if (queue_num != 0xF) {
                return MRVL_ERROR_EXIT;
            }
        }
    }

    /* Get MAC / LLID table from Kernel = new IOCTL to implement and initialize it in the database */
    /* fill in: mrvl_llid_mac_q_map_db[EPON_API_MAX_NUM_OF_MAC];*/
    for (i=0; i< EPON_API_MAX_NUM_OF_MAC;i++) {
        /* extract Q from param tx_queue_bitmap*/
        mrvl_llid_mac_q_map_db[i].queue_num = ( tx_queue_bitmap >> (4*i) ) & MRVL_EOAM_QUEUE_MASK;
        eponIoctlInfo.macId = i;
        mvEponGetInfo(&eponIoctlInfo);
        /* receive the MAC from IOCTL */
        for (j=0;j<EPON_API_MAC_LEN;j++) {
            mrvl_llid_mac_q_map_db[i].mac[j] = eponIoctlInfo.onuEponMacAddr[j];
        }
    }

    return MRVL_EXIT_OK;
}


/*******************************************************************************
* mvEponOamChannelInit()
*
* DESCRIPTION:      Initialization of an OAM channel for EPON.
*
* INPUTS:			uint32_t tx_queue_bitmap (bitmap for queue per LLID:
*                               BIT3..BIT0 = Queue_number of LLID_0
*                               BIT7..BIT4 = Queue_number of LLID_1   , etc.)
*                               not used LLID => fill in 0xF for queue number
*                   uint32_t rx_queue
*                   uint32_t config_bitmap
*                               bit#0   => 0 = non-blocking socket created
*                                          1 = blocking socket
*                               bit1-31 => not used
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponOamChannelInit(uint32_t tx_queue_bitmap, uint32_t rx_queue, uint32_t config_bitmap)
{
    mrvlErrorCode_t   	errno;
    int                 i, rc;
    tpm_error_code_t    tpm_rc;
    uint32_t            owner_id = 0;
    uint32_t            llid_num;

    qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s:  tx_queue_bitmap= 0x%x, rx_queue= %d, config_bitmap= 0x%x",
            __FUNCTION__, tx_queue_bitmap, rx_queue, config_bitmap);

    /* initialize the DB for mapping the LLID to Q and MAC */
    errno = mvEponOamDbInit( tx_queue_bitmap, config_bitmap);
    if (errno != MRVL_EXIT_OK ) {
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: call to DB init failed. errno = %d, %s", __FUNCTION__,  errno, strerror(errno));
        return errno;
    }

    /* set LLID / MAC / Queue table in the MV_CUST package */
    for (i=0; i< EPON_API_MAX_NUM_OF_MAC;i++) {
        mrvl_llid_mac_q_map_db[i].queue_num = ( tx_queue_bitmap >> (4*i) ) & MRVL_EOAM_QUEUE_MASK;
        rc = mv_cust_eoam_llid_mac_set(i, mrvl_llid_mac_q_map_db[i].mac, mrvl_llid_mac_q_map_db[i].queue_num);
        if (rc != 0) {
            qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: call to SET mv_cust failed. errno = %d", __FUNCTION__,  rc);
            return rc;
        }
    }

    /* socket initialization and treatment */
    errno = mvPonChannelInit(config_bitmap);
    if (errno != MRVL_EXIT_OK) {
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: call to mrvl_mngmnt_channel_init failed. errno = %d, %s", __FUNCTION__,  errno, strerror(errno));
        return errno;
    }

    /* enable EOAM */
    rc = mv_cust_eoam_enable(1);
    if (rc != 0) {
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: call to enable EOAM failed. errno = %d", __FUNCTION__,  rc);
        return rc;
    }

    /*todo - how to fill in LLID */
    llid_num = 0;

    tpm_rc = tpm_oam_epon_add_channel(owner_id, rx_queue, llid_num);
    if (tpm_rc != TPM_RC_OK){
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: tpm_oam_epon_add_channel FAILED - owner_id(%d), rx_queue(%d)\n\r",
                __FUNCTION__, owner_id, rx_queue);
        return MRVL_ERROR_HARDWARE;
    }

    return MRVL_EXIT_OK;
}

/*******************************************************************************
* mvEponOamChannelUpdate()
*
* DESCRIPTION:      Update of an LLID per Queue for EPON.
*
* INPUTS:			uint32_t tx_queue_bitmap (bitmap for queue per LLID:
*                               BIT3..BIT0 = Queue_number of LLID_0
*                               BIT7..BIT4 = Queue_number of LLID_1   , etc.)
*                               not used LLID => fill in 0xF for queue number
*                   uint32_t rx_queue (not used in this stage)
*                   uint32_t config_bitmap
*                               bit#0   => 0 = non-blocking socket created
*                                          1 = blocking socket
*                               bit1-31 => not used
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponOamChannelUpdate(uint32_t tx_queue_bitmap, uint32_t rx_queue, uint32_t config_bitmap)
{
    mrvlErrorCode_t   errno;
    int i, rc;

    qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s:  tx_queue_bitmap= 0x%x, config_bitmap= 0x%x", __FUNCTION__, tx_queue_bitmap, config_bitmap);

    /* initialize the DB for mapping the LLID to Q and MAC */
    errno = mvEponOamDbInit( tx_queue_bitmap, config_bitmap);
    if (errno != MRVL_EXIT_OK ) {
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: call to DB init failed. errno = %d, %s", __FUNCTION__,  errno, strerror(errno));
        return MRVL_ERROR_DB_FULL;
    }

    /* set LLID / MAC / Queue table in the MV_CUST package */
    for (i=0; i< EPON_API_MAX_NUM_OF_MAC;i++) {
        mrvl_llid_mac_q_map_db[i].queue_num = ( tx_queue_bitmap >> (4*i) ) & MRVL_EOAM_QUEUE_MASK;
        rc = mv_cust_eoam_llid_mac_set(i, mrvl_llid_mac_q_map_db[i].mac, mrvl_llid_mac_q_map_db[i].queue_num);
        if (rc != 0) {
            qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: call to SET mv_cust failed. errno = %d", __FUNCTION__,  rc);
            return rc;
        }
    }
    return MRVL_EXIT_OK;
}

/*******************************************************************************
* mvEponSendOamFrame()
*
* DESCRIPTION:      transmit OAM frame to specific LLID.
*
* INPUTS:			uint8_t   llid
*                   uint8_t  *data
*                   uint16_t  length
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvl_error_code_t mvEponSendOamFrameViaFifo(int oamIf, uint8_t *msg, int length)
{
  int              fd;
  S_EponIoctlOamTx oam;

  fd = mvPonDrvFdGet();
  if (fd < 0)
	return(MRVL_ERROR_EXIT);

  oam.macId           = oamIf;
  oam.oamFrame.length = length;
  memcpy(oam.oamFrame.data, msg, length);

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd,MVEPON_IOCTL_OAM_TX, &oam) != MRVL_OK)
  {
    osSemGive(ponApiSemId);
    return(MRVL_ERROR_HARDWARE);
  }

  osSemGive(ponApiSemId);

  return(MRVL_EXIT_OK);
}

mrvlErrorCode_t mvEponSendOamFrame(uint8_t llid, uint8_t *data, uint16_t length)
{
    mrvlErrorCode_t	    rc;
    uint8_t        	        retries;
    uint8_t                 i;  /*for debug only */
    uint16_t                pllid;
    uint8_t                 pmacsa[EPON_API_MAC_LEN], j;
    uint8_t                 param_validation;


    rc = MRVL_EXIT_OK;
    param_validation = 0;


    qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "\n %s: llid(%d) length(%d) data=[  \n\r", __FUNCTION__, llid);
    length = (length >=60 ? length :60); /*padding to 60*/

    /* TODO - validate LLID+SA received in the packet vs LLID+DA in DB */
    for (j=0; j < EPON_API_MAC_LEN; j++) {
        pmacsa[j] = data[j+6];
    }

	/* TODO - validate LLID vs DB */
    for(j=0; j<EPON_API_MAX_NUM_OF_MAC; j++) {
        if ( (memcmp(mrvl_llid_mac_q_map_db[j].mac, pmacsa, EPON_API_MAC_LEN) == 0) ) {
            /*found*/
            param_validation = 1;
            break;
        }
    }

    if (!param_validation) {
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "\n %s: missmatch between LLID+SA_MAC in message and DB \n\r", __FUNCTION__, llid);
        rc = MRVL_ERROR_PARAMETER;
        return rc;
    }

    if(length <= (MRVL_PON_FIFO_MAX_LEN-4)) {  /* send OAM frame by fifo */
        retries = 0;
        /* Try again to send if failed*/
        while (retries < 2)
        {
            /* Transmit frame: + 4 is place for CRC*/
            /*assume that only single LLID, modified the first paramter of sendOamFrameViaFifo in future*/
            if ((errno = mvEponSendOamFrameViaFifo(llid, data, length+4)) == MRVL_EXIT_OK){
                mrvl_mngmnt_counters.tx_total_frames++;
                break;
            }
            retries++;
            if(retries == 1)
                qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: mvEponSendOamFrameViaFifo() failed on LLID_%d for the first time\n\r", __FUNCTION__, llid);
            else
                if(retries == 2)
                    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: mvEponSendOamFrameViaFifo() failed on LLID_%d for the second time\n\r", __FUNCTION__, llid);
            mrvl_mngmnt_counters.tx_failures++;
        }

        if (errno != MRVL_EXIT_OK)
        {

            qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: try to send OAM frame by socket\n\r", __FUNCTION__);
            if (ERROR == sendto(sockfd_pon, data, length, 0, (struct sockaddr *) &sockAddress_pon, sizeof(sockAddress_pon)))
            {
                errno = MRVL_ERROR_HARDWARE;
                qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: sendto() failed, errno = %d, %s\n\r", __FUNCTION__, errno, strerror(errno));
                mrvl_mngmnt_counters.tx_failures++;
            }
            mrvl_mngmnt_counters.tx_total_frames++;

        }
    }
    else /*transmit by socket*/
    {
        /* Transmit frame*/
        if (ERROR == sendto(sockfd_pon, data, length, 0, (struct sockaddr *) &sockAddress_pon, sizeof(sockAddress_pon)))
        {
            qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: sendto() failed, errno = %d, %s\n\r", __FUNCTION__, errno, strerror(errno));
            rc = MRVL_ERROR_HARDWARE;
            mrvl_mngmnt_counters.tx_failures++;
        }
        mrvl_mngmnt_counters.tx_total_frames++;

    }
    return rc;
}

/*******************************************************************************
* mvEponReceiveOamFrame()
*
* DESCRIPTION:      Receive OAM frame from specific LLID.
*
* INPUTS:			uint8_t  *llid
*                   uint8_t  *data
*                   uint16_t *length
*                   uint32_t timeout    - timeout in msec
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponReceiveOamFrame(uint8_t *llid, uint8_t *data, uint16_t *length, uint32_t timeout)
{
    int                 rc,  recvlength, debug_j, i;
    fd_set              readfd;
    struct timeval      tv;
    mrvlErrorCode_t	    ret_code;
    uint8_t             tmsg[2000];

    if (timeout == 0) {
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: sendto() failed, errno = %d, %s\n\r", __FUNCTION__, errno, strerror(errno));
    }
    /* fill in the socket timeout - depending in the parameter received */
    tv.tv_sec  = timeout/1000;
    tv.tv_usec = timeout%1000;

    *length = 0;
    *llid   = 0;

    FD_ZERO (&readfd);
    FD_SET (sockfd_pon, &readfd);
    rc = select (sockfd_pon+1, &readfd, NULL, NULL, &tv);
    if (rc <0 ) {
        return MRVL_ERROR_HARDWARE;
    }
    if(rc == 0) {
         FD_CLR(sockfd_pon, &readfd);
         return MRVL_ERROR_TIME_OUT;
    }

	if(FD_ISSET(sockfd_pon, &readfd))  {
         // Non-blocking socket - read frame
        recvlength = recv(sockfd_pon, tmsg, sizeof(tmsg), 0);
        if (recvlength == -1)  {
            ret_code = MRVL_ERROR_EXIT;
        }
        else
        {
            if (recvlength > 2) {
                *length = recvlength-2;
                /* Subtract 1 to indicate right llid, and set the LLID of broad cast to 0*/
                if((tmsg[1] & 0x0F) == 0x0F)
                {
                    *llid = 0;
                }
                else
                {
                *llid = (tmsg[1] & 0x0F)-1;
                }

                memcpy(data, &(tmsg[2]), recvlength-2);
                mrvl_mngmnt_counters.rx_total_frames++;
                qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: RX-EPON-OK: *llid(%d) *length(%d) tmsg= \n\r", __FUNCTION__, *llid, *length);
            }
            else {
                if (recvlength > 0) {
                    mrvl_mngmnt_counters.rx_failures++;
                    ret_code = MRVL_ERROR_NOT_SUPPORT;
                    qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: RX-EPON-NOK: short packets received. \n\r", __FUNCTION__, *llid, *length);
                }
            }
        }
    }

    return MRVL_EXIT_OK;
}

/*******************************************************************************
* mvEponSetDbaParameter()
*
* DESCRIPTION:      configure DBA parameters
*                   it scan all valid LLID according to input parameters.
*					For each LLID it configures queue threshold and queue state according
*					to the number of Queuesets and Queues from the input structure.
*					it execute Report Validation Check as follows
*					 Number of bytes for queue report = 39 bytes (64 minus mac, type, fcs, etc')
*					 Formula - Number of Queueset X
*							   Number of Queues   X
*							   Queue bytes [2]    +
*							   Number of reports ( == Number of Queueset)
*					 Valid values: 2 X 8 X 2 + 3 = 34Bytes
*								   3 X 6 X 2 + 3 = 39bytes
*								   4 X 4 X 2 + 4 = 36bytes
*
* INPUTS:			S_EponIoctlDba *dba_para
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSetDbaParameter(S_EponIoctlDba *dba_para)
{
  int fd;

  fd = mvPonDrvFdGet();
  if(fd < 0)
  	return(MRVL_ERROR_EXIT);

  osSemTake(ponApiSemId, GL_SUSPEND);
  if (ioctl(fd, MVEPON_IOCTL_DBA_CFG, dba_para) < 0)
  {
  	qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
  	return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvEponGetDbaParameter()
*
* DESCRIPTION:      return DBA parameters
*
* INPUTS:			S_EponIoctlDba *dba_para
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponGetDbaParameter(S_EponIoctlDba *dba_para)
{
  int             fd;
  S_EponIoctlDba  ioctlDba;

  fd = mvPonDrvFdGet();
  if(fd < 0)
    return(MRVL_ERROR_EXIT);

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVEPON_IOCTL_DBA_RPRT, &ioctlDba) < 0)
  {
  	qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
  	return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvEponSetHoldover()
*
* DESCRIPTION:      configure EPON MAC holdover time and state
*
* INPUTS:			uint32_t holdoverTime  - 0 - Deactivate, 1 - Activate
*                   uint32_t holdoverState - time to hold before sync loss
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSetHoldover(uint32_t holdoverTime, uint32_t holdoverState)
{
  int                 fd;
  S_EponIoctlHoldOver ioctlHoldover;

  fd = mvPonDrvFdGet();
  if (fd < 0)
  	return(MRVL_ERROR_EXIT);

  ioctlHoldover.holdoverState = holdoverState;
  ioctlHoldover.holdoverTime  = holdoverTime;

  osSemTake(ponApiSemId, GL_SUSPEND);
  if (ioctl(fd, MVEPON_IOCTL_HOLDOVER_CFG, &ioctlHoldover) < 0)
  {
  	qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
  	return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvEponGetHoldover()
*
* DESCRIPTION:      return EPON MAC holdover time and state
*
* INPUTS:			uint32_t *holdoverTime - 0 - Deactivate, 1 - Activate
*                   uint32_t *holdoverState - time to hold before sync loss
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponGetHoldover(uint32_t *holdoverTime, uint32_t *holdoverState)
{
  int                 fd;
  S_EponIoctlHoldOver ioctlHoldover;

  fd = mvPonDrvFdGet();
  if(fd < 0)
  	return(MRVL_ERROR_EXIT);

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVEPON_IOCTL_HOLDOVER_RPRT, &ioctlHoldover) < 0)
  {
  	qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
     return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  *holdoverState = ioctlHoldover.holdoverState;
  *holdoverTime  = ioctlHoldover.holdoverTime;

  return(MRVL_OK);
}

/*******************************************************************************
* mvEponSetSilence()
*
* DESCRIPTION:      configure EPON MAC silence state
*
* INPUTS:			uint32_t silenceState - 0 - Deactivate, 1 - Activate
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSetSilence(uint32_t silenceState)
{
  int                fd;
  S_EponIoctlSilence ioctlSilence;

  fd = mvPonDrvFdGet();
  if(fd < 0)
    return(MRVL_ERROR_EXIT);

  ioctlSilence.silenceState = silenceState;

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVEPON_IOCTL_SILENCE, &ioctlSilence) < 0)
  {
  	qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
  	return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
**
**  mvEponGetAlarm
**  ____________________________________________________________________________
**
**  DESCRIPTION: The function get EPON alarm(PON LOS)
**
**  PARAMETERS:  None
**
**  OUTPUTS:     alarm bit0:XVR, alarm bit1:SERDES
**
**  RETURNS:     None
**
*******************************************************************************/
mrvlErrorCode_t mvEponGetAlarm(uint32_t *alarm)
{
    int                 fd;
    uint32_t            epon_alarm;

    fd = mvPonDrvFdGet();
    if(fd < 0)
      return(MRVL_ERROR_EXIT);

    osSemTake(ponApiSemId, GL_SUSPEND);
    if(ioctl(fd, MVEPON_IOCTL_ALARM_GET, &epon_alarm) < 0)
    {
       qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
       osSemGive(ponApiSemId);
       return(MRVL_ERROR_EXIT);
    }
    osSemGive(ponApiSemId);

    *alarm = epon_alarm;

    return(MRVL_OK);
}


/*******************************************************************************
* mvEponGetOnuAuth()
*
* DESCRIPTION:      configure EPON MAC autentication parameters including
*                   loid, and password
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponGetOnuAuth(S_EponIoctlAuth *auth)
{
  qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: NOT implemented yet\n\r", __FUNCTION__);
  return(MRVL_OK);
}

/*******************************************************************************
* mvEponEnableEncryption()
*
* DESCRIPTION:      enable EPON MAC encryption
*
* INPUTS:			uint32_t enable
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponEnableEncryption(uint32_t enable)
{
  int            fd;
  S_EponIoctlEnc ioctlEnc;

  fd = mvPonDrvFdGet();
  if(fd < 0)
	return(MRVL_ERROR_EXIT);

  ioctlEnc.encEnable = enable;

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVEPON_IOCTL_ENC_CONFIG, &ioctlEnc) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
	return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvEponSetEncryptionKey()
*
* DESCRIPTION:      change EPON MAC encryption key per llid
*
* INPUTS:			uint32_t llId
*                   uint32_t keyVal
*                   uint32_t keyIdx
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSetEncryptionKey(uint32_t llId, uint32_t keyVal, uint32_t keyIdx)
{
  int            fd;
  S_EponIoctlEnc ioctlEnc;

  fd = mvPonDrvFdGet();
  if (fd < 0)
    return(MRVL_ERROR_EXIT);

  ioctlEnc.macId       = llId;
  ioctlEnc.encKey      = keyVal;
  ioctlEnc.encKeyIndex = keyIdx;

  osSemTake(ponApiSemId, GL_SUSPEND);
  if (ioctl(fd, MVEPON_IOCTL_ENC_KEY, &ioctlEnc) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvEponFecConfig()
*
* DESCRIPTION:      config EPON MAC FEC
*
* INPUTS:			S_EponIoctlFec *ioctlFec
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponFecConfig(S_EponIoctlFec *ioctlFec)
{
  int fd;

  fd = mvPonDrvFdGet();
  if(fd < 0)
    return(MRVL_ERROR_EXIT);

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVEPON_IOCTL_FEC_CONFIG, ioctlFec) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvEponGetInfo()
*
* DESCRIPTION:      return EPON MAC information
*
* INPUTS:			S_EponIoctlInfo *ioctlInfo
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponGetInfo(S_EponIoctlInfo *ioctlInfo)
{
  int fd;

  fd = mvPonDrvFdGet();
  if(fd < 0)
    return(MRVL_ERROR_EXIT);

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVEPON_IOCTL_INFO, ioctlInfo) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvEponGetPmCnt()
*
* DESCRIPTION:      return EPON MAC PM information
*
* INPUTS:			S_EponIoctlPm *ioctlCnts
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponGetPmCnt(S_EponIoctlPm *ioctlCnts)
{
  int fd;

  fd = mvPonDrvFdGet();
  if(fd < 0)
	return(MRVL_ERROR_EXIT);

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVEPON_IOCTL_PM, ioctlCnts) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvEponP2pModeSet()
*
* DESCRIPTION:      configure EPON / P2P working modes
*
* INPUTS:			uint32_t state
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
int mvEponP2pModeSet(uint32_t state)
{
  int      fd;
  uint32_t onOff;

  fd = mvPonDrvFdGet();
  if(fd < 0)
    return(MRVL_ERROR_EXIT);

  onOff = (uint32_t)state;

  osSemTake(ponApiSemId, GL_SUSPEND);
  if(ioctl(fd, MVEPON_IOCTL_P2P_SET, &onOff) < 0)
  {
    qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(ponApiSemId);
    return(MRVL_ERROR_EXIT);
  }
  osSemGive(ponApiSemId);

  return(MRVL_OK);
}

/*******************************************************************************
* mvEponP2pForceModeSet()
*
* DESCRIPTION:      configure P2P force working modes
*
* INPUTS:			uint32_t state
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
int mvEponP2pForceModeSet(uint32_t state)
{
    int      fd;
    uint32_t onOff;

    fd = mvPonDrvFdGet();
    if(fd < 0)
    {
        return(MRVL_ERROR_EXIT);
    }

    onOff = (uint32_t)state;

    osSemTake(ponApiSemId, GL_SUSPEND);

    if(ioctl(fd, MVEPON_IOCTL_P2P_FORCE_MODE_SET, &onOff) < 0)
    {
        qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON, "%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(ponApiSemId);
        return(MRVL_ERROR_EXIT);
    }
    
    osSemGive(ponApiSemId);

    return(MRVL_OK);
}

/*******************************************************************************
* mvEponSetLlidAdmin()
*
* DESCRIPTION:      enable / disable pon llid admin
*
* INPUTS:			uint8_t numberOfLlidActived
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSetLlidAdmin(uint8_t numberOfLlidActived)
{
  qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: NOT implemented yet\n\r", __FUNCTION__);
  return(MRVL_OK);
}

/*******************************************************************************
* mvEponSetLlidAdmin()
*
* DESCRIPTION:      configure LLID queues
*
* INPUTS:			llid         - LLID
*					num_of_queue - number of queues of this LLID
*					wrr_weight   - Weight for each queue, 0 means SP, others are WRR weight
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns MRVL_OK.
*					On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
mrvlErrorCode_t mvEponSetLlidQueue(uint8_t llid,
                                   uint8_t num_of_queue,
                                   uint8_t wrr_weight[8])
{
  qprintf(MNG_TRACE_DEBUG, MNG_TRACE_MODULE_PON, "%s: NOT implemented yet\n\r", __FUNCTION__);
  return(MRVL_OK);
}



