/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Flash                                                     **/
/**                                                                          **/
/**  FILE        : flash_cli.h                                               **/
/**                                                                          **/
/**  DESCRIPTION : This file has the CLISH routines                          **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 * 
 ******************************************************************************/

#ifndef __flash_clih__
#define __flash_clih__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"


/********************************************************************************/
/*                            Flash debug CLI functions                         */
/********************************************************************************/
bool_t flash_cli_write_image_to_flash         (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t flash_cli_show_statistics              (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t flash_cli_show_resources               (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t flash_cli_show_sw_image_uboot_vars     (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t flash_cli_commit_image                 (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t flash_cli_activate_image               (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t flash_cli_is_dual_image_configured     (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
#endif
