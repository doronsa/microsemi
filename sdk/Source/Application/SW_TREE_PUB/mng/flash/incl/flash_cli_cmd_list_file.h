/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Flash                                                     **/
/**                                                                          **/
/**  FILE        : flash_cli_cmd_list_file.h                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file has the CLISH action/routine matchup            **/
/**                                                                          **/
/******************************************************************************
 **                                                                           *
 *   MODIFICATION HISTORY:                                                    *
 *                                                                            *
 * 
 ******************************************************************************/

/********************************************************************************/
/*                            Flash debug CLI functions                         */
/********************************************************************************/
  {"flash_cli_write_image_to_flash",         flash_cli_write_image_to_flash},
  {"flash_cli_show_statistics",              flash_cli_show_statistics},
  {"flash_cli_show_resources",               flash_cli_show_resources},
  {"flash_cli_show_sw_image_uboot_vars",     flash_cli_show_sw_image_uboot_vars},
  {"flash_cli_commit_image",                 flash_cli_commit_image},
  {"flash_cli_activate_image",               flash_cli_activate_image},
  {"flash_cli_is_dual_image_configured",     flash_cli_is_dual_image_configured},

