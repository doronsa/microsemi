/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Flash                                                     **/
/**                                                                          **/
/**  FILE        : FlashExports.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file is the Flash Exports - used by client & server  **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Feb04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/


#ifndef __INCFlashExports
#define __INCFlashExports

#include <stdint.h>
#include <stdbool.h>

//#include "flashApp.h"

#define FLASH_MSGQ            "/flashQ"

#define FILENAME_SIZE                (60)
#define CLIENTID_SIZE                (4)
#define VERSION_NAME_LENGTH          (32)

#if 0
// Message
typedef enum
{
    ENUM_FLASHMSG_START=1, ENUM_FLASHMSG_ABORT
} ENUM_FLASHMSG;

typedef struct
{
    int msgType;
    union
    {
        struct
        {
            char    filename[FILENAME_SIZE];
            uint8_t bank;
            uint8_t clientId[CLIENTID_SIZE];
        } start;
    } u;
} FlashMsg_S;
#endif

struct BurnStatus_S {
	bool     inprogress;
	uint16_t bank;
	bool     iswriteok;
	char     bankversion[VERSION_NAME_LENGTH];
    uint8_t  clientId[CLIENTID_SIZE];
};
typedef struct BurnStatus_S BurnStatus_S;


extern bool FlashApi_setCommittedBank      (unsigned char bank);
extern bool FlashApi_prepareImageActivation(unsigned char bank);
extern bool FlashApi_setImageToInvalid     (unsigned char bank);
extern bool FlashApi_getBurnStatus         (BurnStatus_S *param_BurnStatus);
extern bool FlashApi_startWriteFlash       (unsigned char bank, char filename[FILENAME_SIZE], char clientId[CLIENTID_SIZE]);
extern bool FlashApi_abortWriteFlash       ();
extern bool FlashApi_isDualImageConfigured (bool *isDualImage);

extern int flashClientInit(void);
#endif

