#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "clish/shell.h"
#include <rpc/rpc.h>
//#include "flashCli.h"
#include "errorCode.h"
#include "globals.h"
#include "FlashUtils.h"
#include "FlashImage.h"
#include "FlashExports.h"
#include "FlashMain.h"

bool Flash_cli_write_image_to_flash (char* name, int bank)
{
    bool rc = true;

    mipc_printf(name, "bank = %d ", bank);

    rc = writeImageToFlash(bank);

    return rc;
}

bool Flash_cli_show_resources (char* name)
{
    bool rc = true;

    showFlashResources(name);

    return rc;
}

bool Flash_cli_show_statistics(char* name)
{
    bool rc = true;

    showFlashStatistics(name);

    return rc;
}

bool Flash_cli_show_sw_image_uboot_vars(char* name)
{
    bool rc = true;

    printUBootFlashImageVars(name);

    return rc;
}

bool Flash_cli_commit_image(char* name, int bank)
{
	bool rc = true;

    mipc_printf(name, "Bank = %c", bank);

    doCommitImage(bank);

	return rc;
}

bool Flash_cli_activate_image(char* name, int bank, int doReboot)
{
	bool rc = true;

    mipc_printf(name, "Bank = %c, doReboot = %s", bank, doReboot ? "true" : "false");

    doActivateImage(bank, doReboot ? true: false);

	return rc;
}

bool Flash_cli_is_dual_image_configured(char* name, bool *isDualImage)
{
	bool rc = true;

    *isDualImage = isDualImageConfigured();

	return rc;
}


