#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "clish/shell.h"
#include <rpc/rpc.h>
//#include "flashCli.h"
#include "errorCode.h"
#include "globals.h"
#include "FlashUtils.h"
#include "FlashImage.h"
#include "FlashExports.h"
#include "FlashMain.h"

extern void *flash_cl;


bool flash_cli_write_image_to_flash (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
  bool       rc;
  char        bank;
  const char *bstr;

  bstr = lub_argv__get_arg(argv, 0);
  bank = bstr[0];

  /* Print parameters */
  DBG_PRINT("Bank = %c", bank);

  rc = Flash_cli_write_image_to_flash(NULL, bank);
  
  if (rc != TRUE)
  {
    printf("%s, call server failed \r\n");
    return false;
  }
  
  return (rc == TRUE) ? true : false; 

}

bool flash_cli_show_resources (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
  bool rc;

  rc = Flash_cli_show_resources(NULL);
  if (rc != TRUE)
  {
    printf("%s, call server failed \r\n");
    return false;
  }
  
  return (rc == TRUE) ? true : false; 

}

bool flash_cli_show_statistics (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
  bool rc;

  rc = Flash_cli_show_statistics(NULL);
  if (rc != TRUE)
  {
    printf("%s, call server failed \r\n");
    return false;
  }
  
  return (rc == TRUE) ? true : false; 

}

bool flash_cli_show_sw_image_uboot_vars (const clish_shell_t    *instance,
                                           const lub_argv_t       *argv)
{
  bool rc;

  rc = Flash_cli_show_sw_image_uboot_vars(NULL);
  if (rc != TRUE)
  {
    printf("%s, call server failed \r\n");
    return false;
  }
  
  return (rc == TRUE) ? true : false; 

}

bool flash_cli_commit_image (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
	bool       rc;
	char        bank;
	const char *bstr;

	bstr = lub_argv__get_arg(argv, 0);
	bank = bstr[0];

	/* Print parameters */
	DBG_PRINT("Bank = %c", bank);

	rc = Flash_cli_commit_image(NULL, bank);
	if (rc != TRUE)
	{
      printf("%s, call server failed \r\n");
      return false;
	}

    return (rc == TRUE) ? true : false; 

}


bool flash_cli_activate_image (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
	bool        rc;
	char        bank;
	const char  *bstr;
    uint32_t    doReboot;

	bstr = lub_argv__get_arg(argv, 0);
	bank = bstr[0];

    doReboot = atoi(lub_argv__get_arg(argv, 1));

	/* Print parameters */
	DBG_PRINT("Bank = %c, doReboot = %s", bank, doReboot ? "true" : "false");

	rc = Flash_cli_activate_image(NULL, bank, doReboot);
	if (rc != TRUE)
	{
      printf("%s, call server failed \r\n");
      return false;
	}

    return (rc == TRUE) ? true : false; 

}

bool flash_cli_is_dual_image_configured (const clish_shell_t    *instance,
                                         const lub_argv_t       *argv)
{
	bool  rc;
    bool  isDualImage;

	rc = Flash_cli_is_dual_image_configured(NULL, &isDualImage);
	if (rc != TRUE)
	{
      printf("%s, call server failed \r\n");
      return false;
	}
    else
    {
        printf("Dual image is %sconfigured\n", (isDualImage == true) ? "" : "not ");
    }

    return (rc == TRUE) ? true : false; 

}


