/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Flash                                                     **/
/**                                                                          **/
/**  FILE        : FlashStartup.c                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file is the Flash Image startup                      **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    11May06     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>
#include <ctype.h>

#include "globals.h"
#include "MiscUtils.h"
//#include "OmciExtern.h"
#include "FlashUtils.h"
#include "FlashImage.h"
#include "FlashMain.h"
#include "mipc.h"

extern void flash_mipc_server_handler(char* inMsg, unsigned int size);

static FlashResources_S  g_flashResources;
static FlashStatistics_S g_flashStatistics;

///////////////////////////////////////////////////////////////////////////////
////////////                           ////////////////////////////////////////
////////////      Flash Utilities      ////////////////////////////////////////
////////////                           ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/******************************************************************************
 *
 * Function   : getFlashStatistics 
 *              
 * Description: This function returns pointer to Flash stistics global
 *              
 * Returns    : FlashResources_S *
 *              
 ******************************************************************************/

FlashStatistics_S *getFlashStatistics()
{
    return &g_flashStatistics;
}

/******************************************************************************
 *
 * Function   : getFlashResources 
 *              
 * Description: This function returns pointer to global Flash resources
 *              
 * Returns    : FlashResources_S *
 *              
 ******************************************************************************/

FlashResources_S *getFlashResources()
{
    return &g_flashResources;
}

/******************************************************************************
 *
 * Function   : getFlashWorker 
 *              
 * Description: This function returns pointer to Flash worker 
 *              
 * Returns    : FlashWorker_S *
 *              
 ******************************************************************************/

FlashWorker_S *getFlashWorker()
{
    return &g_flashResources.worker;
}

/******************************************************************************
 *
 * Function   : showFlashStatistics
 *              
 * Description: This function displays the flash statistics
 *              
 * Returns    : void
 *              
 ******************************************************************************/

void showFlashStatistics (char* name)
{
    FlashStatistics_S *p_FlashStatistics = getFlashStatistics();

    mipc_printf(name, "Starts                       : %d\n\r", p_FlashStatistics->starts);
    mipc_printf(name, "Starts failed                : %d\n\r", p_FlashStatistics->startsFailed);
    mipc_printf(name, "Aborts                       : %d\n\r", p_FlashStatistics->aborts);
    mipc_printf(name, "Writes OK                    : %d\n\r", p_FlashStatistics->writesOk);
    mipc_printf(name, "Writes failed                : %d\n\r", p_FlashStatistics->writesFailed);
    mipc_printf(name, "Write thread create failures : %d\n\r", p_FlashStatistics->writeThreadCreateFailures);
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//---------------------           OS Resources            ----------------------
//------------------------------------------------------------------------------
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

static MapEntry flashWriteStateEntryAra[] =
{
    { FWS_IDLE,           "Idle"           },
    { FWS_ACTIVE,         "Active"         },
    { FWS_FAILED,         "Failed"         },
    { FWS_SUCCESS,        "Success"        },
    { FWS_CANCEL_PENDING, "Cancel-pending" },
};


static EnumMap flashWriteStateEnumMap =
{
    sizeof(flashWriteStateEntryAra)/sizeof(flashWriteStateEntryAra[0]),
    flashWriteStateEntryAra
};

static char *flashWriteStateLookup(int enumValue)
{
    return enumToStrLookup(&flashWriteStateEnumMap, enumValue);
}


/******************************************************************************
 *
 * Function   : translateBankToPartition
 *              
 * Description: This function translates bank A/B to U/B since we use 
 *              uimageU and rootfsU partitions
 *              
 * Parameters : 
 *
 * Returns    : char   A=>U B=>B
 *              
 ******************************************************************************/

char translateBankToPartition(char bank)
{
    char ucBank = toupper(bank);

    if (ucBank == ID_BANK_A)
    {
        return 'U';
    }
    return ucBank;
}


/******************************************************************************
 *
 * Function   : showFlashResources 
 *              
 * Description: This function displays the OS resources USED BY flash
 *              
 * Returns    : void
 *              
 ******************************************************************************/

void showFlashResources (char* name)
{
    FlashWorker_S      *p_FlashWorker      = getFlashWorker();
    char               buf[20] = { 0 };

    mipc_printf(name, "Flash write state      : %s\n\r",   flashWriteStateLookup(p_FlashWorker->flashWriteState));
    mipc_printf(name, "isUbiFsMounted         : %s\n\r",   (p_FlashWorker->isUbiFsMounted == true) ?"yes" : "false");
    mipc_printf(name, "isSquashFsMounted      : %s\n\r",   (p_FlashWorker->isSquashFsMounted == true) ?"yes" : "false");
    mipc_printf(name, "procMtdFile FILE       : 0x%x\n\r", (unsigned int)p_FlashWorker->procMtdFile);
    mipc_printf(name, "writeTaskId            : 0x%x\n\r", (unsigned int)p_FlashWorker->writeTaskId);

    if (p_FlashWorker->clientId[0] != 0)
    {
        sprintf(buf, "%c%c%c%c", p_FlashWorker->clientId[0], p_FlashWorker->clientId[1], p_FlashWorker->clientId[2], p_FlashWorker->clientId[3]);
    }
    mipc_printf(name, "clientId               : %s\n\r", buf);

    mipc_printf(name, "Elapsed time (secs)    : %d.%d\n\r", (unsigned int)p_FlashWorker->elapsedMilliSecs/1000, (unsigned int)p_FlashWorker->elapsedMilliSecs%1000);

    if (p_FlashWorker->flashWriteState == FWS_IDLE)
    {
        mipc_printf(name, "lastWrite.completionState : %s\n\r", flashWriteStateLookup(p_FlashWorker->lastWrite.completionState));
        mipc_printf(name, "lastWrite.bank            : %c\n\r", p_FlashWorker->lastWrite.bank);
        if (p_FlashWorker->lastWrite.completionState == FWS_SUCCESS)
        {
            mipc_printf(name, "lastWrite.version         : %s\n\r", p_FlashWorker->lastWrite.version);
        }
    }
}


///////////////////////////////////////////////////////////////////////////////
////////////                               ////////////////////////////////////
////////////      Flash Message handling   ////////////////////////////////////
////////////                               ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/******************************************************************************
 *
 * Function   : flash_abortWrite
 *              
 * Description: This routine handles a single flash message
 *              
 * Returns    : void
 *              
 ******************************************************************************/

void flash_abortWrite()
{
    FlashWorker_S     *p_FlashWorker = getFlashWorker();
    FlashStatistics_S *p_FlashStatistics = getFlashStatistics();

    p_FlashStatistics->aborts++;

    printf("%s: ======= State = %s ===========\n", __FUNCTION__, flashWriteStateLookup(p_FlashWorker->flashWriteState));

    // Algorithm: 
    //      If writeTaskId = 0 => simple cleanup
    //      else if  thread no longer exists do cleanup (NOTE pthread_kill(id, action) - if action = 0, no kill actually done)
    //      else write is in progress (flashWriteState = FWS_ACTIVE) so do pthread_cancel
    if (p_FlashWorker->writeTaskId == 0) 
    {
        p_FlashWorker->flashWriteState = FWS_IDLE;
    }
    else if (pthread_kill(p_FlashWorker->writeTaskId, 0) == ESRCH)
    {
        printf("%s: Worker thread DEAD\n\r", __FUNCTION__);
        p_FlashWorker->writeTaskId     = 0;
        p_FlashWorker->flashWriteState = FWS_IDLE;
    }
    else
    {
        // This is a thread technique that should use pthread_testcancel() in the worker thread
        pthread_cancel(p_FlashWorker->writeTaskId);
    }
}

/******************************************************************************
 *
 * Function   : flash_startWrite
 *              
 * Description: This routine handles the Start message
 *              
 * Returns    : bool
 *              
 ******************************************************************************/

bool flash_startWrite(uint8_t bank, char *filename, uint8_t *clientId)
{
    FlashWorker_S          *p_FlashWorker     = getFlashWorker();
    FlashStatistics_S      *p_FlashStatistics = getFlashStatistics();
    static WriteFlashThreadArgs_S writeFlashThreadArgs;

    p_FlashStatistics->starts++;

    if (p_FlashWorker->writeTaskId != 0) 
    {
        if (pthread_kill(p_FlashWorker->writeTaskId, 0) != ESRCH)
        {
            printf("%s: Worker thread already ALIVE\n\r", __FUNCTION__);
            p_FlashStatistics->startsFailed++;

            return false;
        }
    }

    writeFlashThreadArgs.bank     = bank;
    strcpy(writeFlashThreadArgs.filename, filename);
    memcpy(p_FlashWorker->clientId, clientId, CLIENTID_SIZE);

    printf("%s: Bank %c, filename = '%s', writeFlashThreadArgs.filename = '%s', Init state = %s\n", 
           __FUNCTION__, bank, filename, writeFlashThreadArgs.filename, flashWriteStateLookup(p_FlashWorker->flashWriteState));
    
    p_FlashWorker->flashWriteState = FWS_ACTIVE;

    // IMPORTANT: Create the Flash task directly - osTaskCreate loses the i/o stderr, stdout that were dup'ed
//    if (osTaskCreate(&p_FlashWorker->writeTaskId , 
//                     "writeFlash", 
//                     (GLFUNCPTR) writeFlashThread,
//                     1,
//                     &writeFlashThreadArgs,
//                     50,
//                     10000) != IAMBA_OK)
    if (pthread_create(&p_FlashWorker->writeTaskId, 0, writeFlashThread, &writeFlashThreadArgs) != 0)
    {
        printf("%s: pthread_create failed for Flash task!\n\r", __FUNCTION__);
        p_FlashWorker->flashWriteState = FWS_IDLE;
        p_FlashWorker->lastWrite.completionState = FWS_FAILED;
        p_FlashStatistics->writeThreadCreateFailures++;
        return false;
    }
    return true;
}


///////////////////////////////////////////////////////////////////////////////
////////////                           ////////////////////////////////////////
////////////      Flash Startup        ////////////////////////////////////////
////////////                           ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/******************************************************************************
 *
 * Function   : initFlashGlobals ()
 *
 * Description: This function inits Flash global structures
 *                                                  
 * Returns    : void
 *
 ******************************************************************************/

static void initFlashGlobals()
{
    memset(&g_flashResources,  0, sizeof(g_flashResources));
    memset(&g_flashStatistics, 0, sizeof(g_flashStatistics));

    g_flashResources.worker.flashWriteState = FWS_IDLE;
}

/******************************************************************************
 *
 * Function   : flashStartup ()
 *
 * Description: This function creates the main Flash task
 *                                                  
 * Returns    : IAMBA_OK       - switch on ended successfully, else Queue or Task
 *              create errors
 *
 ******************************************************************************/

E_ErrorCodes flashStartup ( void )
{
    initFlashGlobals();

    return IAMBA_OK;
}

/******************************************************************************
 *
 * Function   : module_flashSwitchOn ()
 *
 * Description: This function does the flash startup
 *                                                  
 * Returns    : IAMBA_OK          - switch on ended successfully,
 *              IAMBA_ERR_GENERAL - failed
 *
 ******************************************************************************/

E_ErrorCodes module_flashSwitchOn ( void )
{
    analyzeUbiMounts();
    parseSwImageUBootVars();      // Init in case we do operation before U-Boot SW image vars are read iin

    if (flashStartup() != IAMBA_OK)
    {
        printf("%s: flashStartup failed. Ignore for now!!\n\r", __FUNCTION__);
        return IAMBA_ERR_GENERAL;
    }

    return IAMBA_OK;
}

/*******************************************************************************
* main
*
* DESCRIPTION:      Rest main
*
* INPUTS:            none
*
* OUTPUTS:            none
*
* RETURNS:          none
*
*******************************************************************************/
int main(int argc, char* argv[])
{
    int32_t             rcode;
    int                 mipc_fd;

    //printf("############ Flash Server Init    ############\n");

    if (IAMBA_OK != module_flashSwitchOn()) 
    {
        fprintf(stderr, "Flash Server init failed, aborting\n");
        return(IAMBA_ERR_GENERAL); 
    }

    //printf("############ Flash Server Running ############\n");

	mipc_fd = mipc_init("flash", 0, 0);
    if (MIPC_ERROR == mipc_fd)  
    { 	  
      return; 
    }   

    mthread_register_mq(mipc_fd, flash_mipc_server_handler);
    mthread_start();

    return(IAMBA_OK);
}

