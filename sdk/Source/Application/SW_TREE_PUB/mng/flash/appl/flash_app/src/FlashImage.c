/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Flash                                                     **/
/**                                                                          **/
/**  FILE        : FlashImage.c                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file deals with writing software image to flash      **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Feb04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <time.h>

#include "FlashExports.h"
#include "FlashMain.h"
#include "FlashUtils.h"
#include "FlashImage.h"

static MtdInfo_S  mtdInfo[MAX_MTD_PARTITIONS];

static int        dbUbiVolNumA = -1;
static int        dbUbiVolNumB = -1;

char garbage;
/******************************************************************************
 *
 * Function   : analyzeMounts
 *              
 * Description: This function reads in /proc/mounts to see what ubix are in use
 *              if any and fills in mini DB
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

void analyzeUbiMounts()
{
    FILE      *filehandle;
    char      line[CMDLINEBUFF];
    char      *token;
    char      *ubixrootfsUStrings[] = {"ubi0:rootfsU", "ubi1:rootfsU"};
    char      *ubixrootfsBStrings[] = {"ubi0:rootfsB", "ubi1:rootfsB"};
    uint32_t  indx;
    char      *pstr;

    // Do /proc/mounts analysis
    if ((filehandle = fopen("/proc/mounts", "r")) == NULL)
    {
        printf("fopen /proc/mounts failed. errno = %d\n", errno);
        return;
    }

    // Read in a line, cleanup \n\r at end of line and copy string
    while (fgets(line, CMDLINEBUFF, filehandle) != 0)
    {
        token = strtok(line, "\n\r");

        //printf("%s: line %s\n", __FUNCTION__, token);

        for (indx = 0; indx < sizeof(ubixrootfsUStrings)/sizeof(ubixrootfsUStrings[0]); indx++)
        {
            pstr = ubixrootfsUStrings[indx];

            if (strstr(token, pstr) != 0)
            {
                printf("%s: Found %s\n", __FUNCTION__, pstr);

                dbUbiVolNumA = indx;
            }
        }

        for (indx = 0; indx < sizeof(ubixrootfsBStrings)/sizeof(ubixrootfsBStrings[0]); indx++)
        {
            pstr = ubixrootfsBStrings[indx];

            if (strstr(token, pstr) != 0)
            {
                printf("%s: Found %s\n", __FUNCTION__, pstr);

                dbUbiVolNumB = indx;
            }
        }
    }

    if (fclose(filehandle) != 0)
    {
        printf("%s: fclose failed. errno = %d\n", __FUNCTION__, errno);
    }
}

/******************************************************************************
 *
 * Function   : getUbiVolNum
 *              
 * Description: This function returns a UBI volume number per bank
 *              
 * Parameters : Bank 
 *
 * Returns    : int
 *              
 ******************************************************************************/

static int getUbiVolNum(char ucBank)
{
    if (ucBank == ID_BANK_A)
    {
        if (dbUbiVolNumA == -1) 
        {
            if (dbUbiVolNumB == -1)
            {
                dbUbiVolNumA = 0;
            }
            else
            {
                dbUbiVolNumA = 1;
            }
        }
        return dbUbiVolNumA;
    }
    else
    {
        if (dbUbiVolNumB == -1) 
        {
            if (dbUbiVolNumA == -1)
            {
                dbUbiVolNumB = 0;
            }
            else
            {
                dbUbiVolNumB = 1;
            }
        }
        return dbUbiVolNumB;
    }
}

/******************************************************************************
 *
 * Function   : findMtdByName
 *              
 * Description: This function finds the MTD by name
 *              
 * Parameters : Name of partition 
 *
 * Returns    : MtdInfo_S *
 *              
 ******************************************************************************/

static MtdInfo_S *findMtdByName(char *name, int numMtdsFound)
{
    int        indx;
    MtdInfo_S  *p_MtdInfo;

    for (indx = 0; indx < numMtdsFound; indx++)
    {
        p_MtdInfo = &mtdInfo[indx];

//        printf("%s: name = %s, dbname = %s, %d %d\n", __FUNCTION__, name, p_MtdInfo->name, strlen(name), strlen(p_MtdInfo->name));
        if (strcmp(name, p_MtdInfo->name) == 0)
        {
            return p_MtdInfo;
        }
    }
    return 0;
}


static void dismountUbifs(FlashWorker_S *p_FlashWorker)
{
    char             line[CMDLINEBUFF];

    sprintf(line, "umount %s", MOUNT_POINT_UBIFS);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line)) printf("%s: %s FAILED\n", __FUNCTION__, line);
    else p_FlashWorker->isUbiFsMounted = false;
}

static void dismountSquashfs(FlashWorker_S *p_FlashWorker)
{
    char             line[CMDLINEBUFF];

    sprintf(line, "umount -d %s", MOUNT_POINT_SQUASHFS);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line)) printf("%s: %s FAILED\n", __FUNCTION__, line);
    else p_FlashWorker->isSquashFsMounted = false;
}

static void closeMtdProcFile(FlashWorker_S *p_FlashWorker)
{
    if (fclose(p_FlashWorker->procMtdFile) == 0)
    {
        p_FlashWorker->procMtdFile = 0;
    }
    else
    {
        printf("%s: fclose failed. errno = %d\n", __FUNCTION__, errno);
    }
}

/******************************************************************************
 *
 * Function   : elapsedTime
 *              
 * Description: This function calculates difference between two timespec (structures)
 *              
 * Returns    : int in milliseconds
 *              
 ******************************************************************************/

static unsigned int elapsedTime(struct timespec *pLate, struct timespec *pEarly)
{
    if (pLate->tv_nsec > pEarly->tv_nsec)
    {
        return ((pLate->tv_sec - pEarly->tv_sec) * 1000) + (pLate->tv_nsec - pEarly->tv_nsec)/1000000;
    }
    else
    {
        return ((pLate->tv_sec - pEarly->tv_sec - 1) * 1000) + (1000000000 + pLate->tv_nsec - pEarly->tv_nsec)/1000000;
    }
}

/******************************************************************************
 *
 * Function   : readImageVersion
 *              
 * Description: This function reads in the version name from etc/version.txt
 *              in rootfs.squashfs
 *              
 * Parameters :  
 *
 * Returns    : bool   true - got the name; else false
 *              
 ******************************************************************************/

static bool readImageVersion(char *mountpoint, char *version, int length)
{
    char  filename[60];
    FILE  *filehandle;
    char  line[CMDLINEBUFF];
    char  *token;
    bool  rc = false;

    sprintf(filename, "%s/%s", mountpoint, VERSION_FILE);

    // Do /proc/mtd analysis
    if ((filehandle = fopen(filename, "r")) == NULL)
    {
        printf("fopen %s failed. errno = %d\n", filename, errno);
        return false;
    }

    // Read in a line, cleanup \n\r at end of line and copy string
    if (fgets(line, CMDLINEBUFF, filehandle) != 0)
    {
        token = strtok(line, "\n\r");

        if (strlen(token) > 0)
        {
            strncpy(version, token, length);
            rc = true;
        }
    }

    if (fclose(filehandle) != 0)
    {
        printf("%s: fclose failed. errno = %d\n", __FUNCTION__, errno);
    }
    return rc;
}

/******************************************************************************
 *
 * Function   : prepareMtdData
 *              
 * Description: This function scans /proc/mtd and prepares MTD database
 *              
 * Parameters :  
 *
 * Returns    : int   num of MTD entries found, -1 = error
 *              
 ******************************************************************************/

static int prepareMtdData(FlashWorker_S *p_FlashWorker)
{
    int             indx = 0;
    char            line[CMDLINEBUFF];
    MtdInfo_S       *p_MtdInfo;
    char            temp_mtd_name[20];
    char            temp_name[20];

    // Do /proc/mtd analysis
    if ((p_FlashWorker->procMtdFile = fopen("/proc/mtd", "r")) == NULL)
    {
        printf("Opening /proc/mtd failed\n");
        return -1;
    }
    
    // Skip the header line
    fgets(line, CMDLINEBUFF, p_FlashWorker->procMtdFile);

    // Scan all MTD partitions and build DB
    printf("scanning /proc/mtd\n");
    memset(mtdInfo, 0, sizeof(mtdInfo));
    indx = 0;
    while (indx < MAX_MTD_PARTITIONS)
    {
        p_MtdInfo = &mtdInfo[indx];
        temp_mtd_name[0] = 0;
        temp_name[0]     = 0;
        if (fscanf(p_FlashWorker->procMtdFile, "%s %x %x %s", temp_mtd_name, &p_MtdInfo->size, &p_MtdInfo->erasesize, temp_name) != 0)
        {
            if (strlen(temp_mtd_name) == 0)
            {
                break;
            }
            if (strncmp(temp_mtd_name, "mtd", 3) != 0)
            {
                printf("Unexpected MTD device name %s. Should start with 'mtd'. Iteration %d\n", temp_mtd_name, indx);
                break;
            }
            else
            {
                // Trim mtd_name: Remove trailing semi-colon
                strcpy(p_MtdInfo->mtd_name, temp_mtd_name);
                p_MtdInfo->mtd_name[strlen(p_MtdInfo->mtd_name) - 1] = 0;

                // Trim name: Remove leading & trailing quotes
                strcpy(p_MtdInfo->name, &temp_name[1]);
                p_MtdInfo->name[strlen(p_MtdInfo->name) - 1] = 0;
                printf("%5s 0x%010X 0x%X %s\n", p_MtdInfo->mtd_name, p_MtdInfo->size, p_MtdInfo->erasesize, p_MtdInfo->name);
                indx++;
            }
        }
        else
        {
            break;
        }
    }
    closeMtdProcFile(p_FlashWorker);
    return indx;
}

/******************************************************************************
 *
 * Function   : writeKw2SquashToFlash
 *              
 * Description: This function writes KW2 image to flash bank
 *              
 * Parameters :  
 *
 * Returns    : bool
 *              
 ******************************************************************************/

bool writeKw2SquashToFlash(char bank, char *squashfile)
{
    char              line[CMDLINEBUFF];
    char              targetMtdName[20];
    MtdInfo_S         *p_uimageMtdInfo;
    MtdInfo_S         *p_rootfsMtdInfo;
    int               numMtdsFound;
    int               mtdNumber;
    bool              rc = false;
    int               ubiVolNum;
    char              ucBank = toupper(bank);
    char              xlatedBank = translateBankToPartition(bank);
    char              version[15];
    FlashWorker_S     *p_FlashWorker = getFlashWorker();
    FlashStatistics_S *p_FlashStatistics = getFlashStatistics();
    int               oldcancelstate;
    int               oldcanceltype;
    struct timespec   startTime;
    struct timespec   endTime;
    int ret = 0;

    clock_gettime(CLOCK_MONOTONIC, &startTime);

    // NOTE: umount squashfs and ubifs before trying to mount them again
    //       This helps avoid possible failures die to inconsistent completion state

    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,  &oldcancelstate);
    pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &oldcanceltype);

    //p_FlashWorker->flashWriteState = FWS_ACTIVE;
    p_FlashWorker->bank            = ucBank;

    if ( !(ucBank == ID_BANK_A || ucBank == ID_BANK_B))
    {
        printf("Invalid bank %c\n", bank);
        goto early_fail_exit;
    }

    if ((numMtdsFound = prepareMtdData(p_FlashWorker)) == -1) goto early_fail_exit;

    // Find the target MTD partitions
    // First uimageX
    sprintf(targetMtdName, "%s%c", UIMAGE_MTD_PREFIX, xlatedBank);
    if ((p_uimageMtdInfo = findMtdByName(targetMtdName, numMtdsFound)) == 0)
    {
        printf("%s: Search for MTD %s failed\n", __FUNCTION__, targetMtdName);
        goto early_fail_exit;
    }
    // Next rootfsX
    sprintf(targetMtdName, "%s%c", ROOTFS_MTD_PREFIX, xlatedBank);
    if ((p_rootfsMtdInfo = findMtdByName(targetMtdName, numMtdsFound)) == 0)
    {
        printf("%s: Search for MTD %s failed\n", __FUNCTION__, targetMtdName);
        goto early_fail_exit;
    }

    // -----   DEAL WITH UIMAGE   -----
    // Create the mount point for the image file
    sprintf(line, "mkdir -p %s", MOUNT_POINT_SQUASHFS);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line))  goto early_fail_exit;

    // unmount the image file: to ensure that mount will be OK
    dismountSquashfs(p_FlashWorker);

    // Mount the image file as a SquashFs file system
    sprintf(line, "mount -t squashfs -o loop %s %s", squashfile, MOUNT_POINT_SQUASHFS);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line))  goto early_fail_exit;
    p_FlashWorker->isSquashFsMounted = true;

    // Erase the uimage MTD
    sprintf(line, "flash_erase /dev/%s 0 0", p_uimageMtdInfo->mtd_name);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line))  goto uimage_exit;

    pthread_testcancel();

    // Nandwrite to uimage MTD
    sprintf(line, "nandwrite -p /dev/%s %s/%s > /dev/null", p_uimageMtdInfo->mtd_name, MOUNT_POINT_SQUASHFS, UIMAGE_FILE);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line))  goto uimage_exit;

    pthread_testcancel();

    // -----   DEAL WITH ROOTFS   -----
    // Identify the MTD device number
    if (sscanf(&p_rootfsMtdInfo->mtd_name[3], "%d", &mtdNumber) != 1)
    {
        printf("%s: sscanf failed to get mtdNum from %s\n", __FUNCTION__, p_rootfsMtdInfo->mtd_name);
        goto uimage_exit;
    }

    // Create the mount point for the target rootfs
    sprintf(line, "mkdir -p %s", MOUNT_POINT_UBIFS);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line))  goto uimage_exit;

    // Detach from ubi_ctrl
    sprintf(line, "ubidetach /dev/ubi_ctrl -m %d", mtdNumber);
    printf("%s: %s\n", __FUNCTION__, line);
    system(line);  // If cannot detach just continue
    
    // Format UBI
    sprintf(line, "ubiformat /dev/%s -y > /dev/null", p_rootfsMtdInfo->mtd_name);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line))  goto uimage_exit;

    pthread_testcancel();

    // Attach to ubi_ctrl
    sprintf(line, "ubiattach /dev/ubi_ctrl -m %d > /dev/null", mtdNumber);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line))  goto uimage_exit;

    // Intermittent failure on DB board A0 at this point - error 2 = no such file or directory /dev/ubi1 
    sleep(2);

    ubiVolNum = getUbiVolNum(ucBank);

    // Make UBI volume
    sprintf(line, "ubimkvol /dev/ubi%i -N %s -m", ubiVolNum, p_rootfsMtdInfo->name);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line))  goto uimage_exit;

    pthread_testcancel();

    // Mount the UBI vloume
    sprintf(line, "mount -t ubifs ubi%i:%s %s", ubiVolNum, p_rootfsMtdInfo->name, MOUNT_POINT_UBIFS);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line))  goto uimage_exit;
    p_FlashWorker->isUbiFsMounted = true;

    // Make the kernel filesystem mount points, the tmp directory
    sprintf(line, "for d in tmp proc sys; do mkdir -p %s/${d}; done", MOUNT_POINT_UBIFS);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line)) goto rootfs_exit;

    pthread_testcancel();

    // Change to the squashfs mount (important otherwise, umount fails if we use just chdir - seems to hanging reference to squashfs)
    // Also, need to change directory, otherwise we are looking at the running system
    // Copy in the contents of all the non mount folders
    sprintf(line, "cd %s;ls -1 | grep -vE \"proc|sys|tmp|boot\" | awk '{print \"cp -adpr \"$1\" %s \"}' | sh ", MOUNT_POINT_SQUASHFS, MOUNT_POINT_UBIFS);
    printf("%s: %s\n", __FUNCTION__, line);
    if (system(line)) goto rootfs_exit;

    pthread_testcancel();

    // Read in the version name
    if (readImageVersion(MOUNT_POINT_SQUASHFS, version, sizeof(version) -1) == false)
    {
        sprintf(version, "not-found-%c", bank);
    }

    // And now write the versionX and isValidX U-Boot variables
    sprintf(line, "%s %s%c %.14s", FW_SETENVSTR, UBOOTVAR_VERSION_PREFIX, ucBank, version); 
    if (system(line)) goto rootfs_exit;

    pthread_testcancel();

    // Witten uImage + file system, written U-Boot versionX - if got this far - no more pthread_testcancel()
    sprintf(line, "%s %s%c true", FW_SETENVSTR, UBOOTVAR_ISVALID_PREFIX, ucBank); 
    if (system(line)) goto rootfs_exit;

    rc = true;

rootfs_exit:
    // Unmount the UBIFS 
    dismountUbifs(p_FlashWorker);

uimage_exit:
    // Unmount the image file
    dismountSquashfs(p_FlashWorker);

early_fail_exit:
    if (rc == false)
    {
        p_FlashWorker->flashWriteState = FWS_FAILED;
        p_FlashStatistics->writesFailed++;
    }
    else
    {
        printf("%s: FlashWork success\n", __FUNCTION__);
        p_FlashWorker->flashWriteState = FWS_SUCCESS;
        strncpy(p_FlashWorker->version, version, sizeof(p_FlashWorker->version));
        p_FlashStatistics->writesOk++;
    }
    clock_gettime(CLOCK_MONOTONIC, &endTime);
    p_FlashWorker->elapsedMilliSecs = elapsedTime(&endTime, &startTime);

    // update the upgrade status immediately, no wait for timeout message.
    if (p_FlashWorker->flashWriteState == FWS_SUCCESS || p_FlashWorker->flashWriteState == FWS_FAILED)
    {
        // Copy in results for applications that poll
        p_FlashWorker->lastWrite.completionState = p_FlashWorker->flashWriteState;
        p_FlashWorker->lastWrite.bank            = p_FlashWorker->bank;
        memcpy(p_FlashWorker->lastWrite.version, p_FlashWorker->version, sizeof(p_FlashWorker->lastWrite.version));

        p_FlashWorker->flashWriteState = FWS_IDLE;
        p_FlashWorker->writeTaskId     = 0;
    }

    return rc;
}

#define DDSPLIT_UIMG      "/tmp/uImg"
#define DDSPLIT_ROOTFS    "/tmp/rootFs"
#define DDSPLIT_VARS      "/tmp/vars"

int wrap_system_api(const char *caller, char *line)
{
    int rc = 0;

    printf("%s: %s\n", caller, line);
    if ((rc = system(line)) != 0)
    {
        printf("%s: '%s' failed. rc=%d(%s)\n", caller, line, errno, strerror(errno));
    }
    return rc;
}

/******************************************************************************
 *
 * Function   : writeMcUnifiedImgToFlash
 *              
 * Description: This function writes MC image to flash bank
 *              
 * Parameters :  
 *
 * Returns    : bool
 *              
 ******************************************************************************/

bool writeMcUnifiedImgToFlash(char bank, char *imagefile)
{
    char              line[CMDLINEBUFF];
    char              targetMtdName[20];
    MtdInfo_S         *p_uimageMtdInfo;
    MtdInfo_S         *p_rootfsMtdInfo;
    MtdInfo_S         *p_varsMtdInfo;
    int               numMtdsFound;
    int               mtdNumber;
    bool              rc                 = false;
    char              ucBank             = toupper(bank);
    char              version[15];
    FlashWorker_S     *p_FlashWorker     = getFlashWorker();
    FlashStatistics_S *p_FlashStatistics = getFlashStatistics();
    int               oldcancelstate;
    int               oldcanceltype;
    struct timespec   startTime;
    struct timespec   endTime;
    int               skipValue = 0;

    clock_gettime(CLOCK_MONOTONIC, &startTime);

    // NOTE: umount squashfs and ubifs before trying to mount them again
    //       This helps avoid possible failures die to inconsistent completion state

    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,  &oldcancelstate);
    pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &oldcanceltype);

    //p_FlashWorker->flashWriteState = FWS_ACTIVE;
    p_FlashWorker->bank            = ucBank;

    if ( !(ucBank == ID_BANK_A || ucBank == ID_BANK_B))
    {
        printf("Invalid bank %c\n", bank);
        goto early_fail_exit;
    }

    if ((numMtdsFound = prepareMtdData(p_FlashWorker)) == -1) goto early_fail_exit;

    // Find the target MTD partitions
    // First uImg
    if (ucBank == ID_BANK_A) sprintf(targetMtdName, "%s",   UIMAGE_MTD_PREFIX_MC);
    else                     sprintf(targetMtdName, "%s%c", UIMAGE_MTD_PREFIX_MC, ucBank);
    if ((p_uimageMtdInfo = findMtdByName(targetMtdName, numMtdsFound)) == 0)
    {
        printf("%s: Search for MTD %s failed\n", __FUNCTION__, targetMtdName);
        goto early_fail_exit;
    }
    // Next rootfsX
    if (ucBank == ID_BANK_A) sprintf(targetMtdName, "%s",   ROOTFS_MTD_PREFIX_MC);
    else                     sprintf(targetMtdName, "%s%c", ROOTFS_MTD_PREFIX_MC, ucBank);
    if ((p_rootfsMtdInfo = findMtdByName(targetMtdName, numMtdsFound)) == 0)
    {
        printf("%s: Search for MTD %s failed\n", __FUNCTION__, targetMtdName);
        goto early_fail_exit;
    }
    // Next varsX
    if (ucBank == ID_BANK_A) sprintf(targetMtdName, "%s",   VARS_MTD_PREFIX_MC);
    else                     sprintf(targetMtdName, "%s%c", VARS_MTD_PREFIX_MC, ucBank);
    if ((p_varsMtdInfo = findMtdByName(targetMtdName, numMtdsFound)) == 0)
    {
        printf("%s: Search for MTD %s failed\n", __FUNCTION__, targetMtdName);
        goto early_fail_exit;
    }


    // -----   SPLIT the IMAGEFILE INTO UIMAGE, ROOTFS, VARS   -----
    sprintf(line, "dd if=%s of=%s count=%d", imagefile, DDSPLIT_UIMG, p_uimageMtdInfo->size/512);
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;

    pthread_testcancel();

    skipValue += p_uimageMtdInfo->size/512;
    sprintf(line, "dd if=%s of=%s skip=%d count=%d", imagefile, DDSPLIT_ROOTFS, skipValue, p_rootfsMtdInfo->size/512);
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;

    pthread_testcancel();

    skipValue += p_rootfsMtdInfo->size/512;
    sprintf(line, "dd if=%s of=%s skip=%d count=%d", imagefile, DDSPLIT_VARS, skipValue, p_varsMtdInfo->size/512);
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;

    pthread_testcancel();

    // NOW FLASHCP THE FILES TO THEIR RESPECTIVE MTDS
    // First, uImage
    sprintf(line, "flash_unlock /dev/%s", p_uimageMtdInfo->mtd_name);
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;
    sprintf(line, "flashcp -v %s /dev/%s", DDSPLIT_UIMG, p_uimageMtdInfo->mtd_name);
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;

    pthread_testcancel();

    // Next, rootfs
    sprintf(line, "flash_unlock /dev/%s", p_rootfsMtdInfo->mtd_name);
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;
    sprintf(line, "flashcp -v %s /dev/%s", DDSPLIT_ROOTFS, p_rootfsMtdInfo->mtd_name);
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;

    pthread_testcancel();

    // Next, vars
    sprintf(line, "flash_unlock /dev/%s", p_varsMtdInfo->mtd_name);
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;
    sprintf(line, "flashcp -v %s /dev/%s", DDSPLIT_VARS, p_varsMtdInfo->mtd_name);
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;

    pthread_testcancel();


    // -----   DEAL WITH VERSION NAME   -----
    // Create the mount point for the rootfs squash file
    sprintf(line, "mkdir -p %s", MOUNT_POINT_SQUASHFS);
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;

    // unmount the image file: to ensure that mount will be OK
    dismountSquashfs(p_FlashWorker);

    // Mount the rootfs file as a SquashFs file system
    sprintf(line, "mount -t squashfs -o loop %s %s", DDSPLIT_ROOTFS, MOUNT_POINT_SQUASHFS);
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;

    p_FlashWorker->isSquashFsMounted = true;

    // Read in the version name
    if (readImageVersion(MOUNT_POINT_SQUASHFS, version, sizeof(version) -1) == false)
    {
        sprintf(version, "not-found-%c", bank);
    }

    // And now write the versionX and isValidX U-Boot variables
    sprintf(line, "%s %s%c %.14s", FW_SETENVSTR, UBOOTVAR_VERSION_PREFIX, ucBank, version); 
    if (wrap_system_api(__FUNCTION__, line) != 0) goto rootfs_exit;

    pthread_testcancel();

    // Witten uImage + file system, written U-Boot versionX - if got this far - no more pthread_testcancel()
    sprintf(line, "%s %s%c true", FW_SETENVSTR, UBOOTVAR_ISVALID_PREFIX, ucBank); 
    if (wrap_system_api(__FUNCTION__, line) != 0) goto early_fail_exit;

    rc = true;

rootfs_exit:
    // Unmount the ROOTFS 
    dismountSquashfs(p_FlashWorker);

early_fail_exit:
    if (rc == false)
    {
        p_FlashWorker->flashWriteState = FWS_FAILED;
        p_FlashStatistics->writesFailed++;
    }
    else
    {
        printf("%s: FlashWork success\n", __FUNCTION__);
        p_FlashWorker->flashWriteState = FWS_SUCCESS;
        strncpy(p_FlashWorker->version, version, sizeof(p_FlashWorker->version));
        p_FlashStatistics->writesOk++;
    }
    clock_gettime(CLOCK_MONOTONIC, &endTime);
    p_FlashWorker->elapsedMilliSecs = elapsedTime(&endTime, &startTime);

    // update the upgrade status immediately, no wait for timeout message.
    if (p_FlashWorker->flashWriteState == FWS_SUCCESS || p_FlashWorker->flashWriteState == FWS_FAILED)
    {
        // Copy in results for applications that poll
        p_FlashWorker->lastWrite.completionState = p_FlashWorker->flashWriteState;
        p_FlashWorker->lastWrite.bank            = p_FlashWorker->bank;
        memcpy(p_FlashWorker->lastWrite.version, p_FlashWorker->version, sizeof(p_FlashWorker->lastWrite.version));

        p_FlashWorker->flashWriteState = FWS_IDLE;
        p_FlashWorker->writeTaskId     = 0;
    }

    return rc;
}

/******************************************************************************
 *
 * Function   : writeFlashThread
 *              
 * Description: The thread that calls the write flash implementation
 *              
 * Parameters :  
 *
 * Returns    : bool
 *              
 ******************************************************************************/

void *writeFlashThread(void *args)
{
    WriteFlashThreadArgs_S *p_Args = (WriteFlashThreadArgs_S *)args;

    printf("%s: bank %c, file '%s'.\n", __FUNCTION__, p_Args->bank, p_Args->filename);

    if (isBoardKW2() == true)
    {
        writeKw2SquashToFlash(p_Args->bank, p_Args->filename);
    }
    else
    {
        writeMcUnifiedImgToFlash(p_Args->bank, p_Args->filename);
    }
}


#define IMAGEFILE_LOCATION     "/tmp/download.img"
/******************************************************************************
 *
 * Function   : writeImageToFlash
 *              
 * Description: This is CLI function writes image to flash bank
 *              
 * Parameters :  
 *
 * Returns    : bool
 *              
 ******************************************************************************/

bool writeImageToFlash(char bank)
{
    char *filename = IMAGEFILE_LOCATION;

    printf("%s: bank %c, Expect to use file %s\n", __FUNCTION__, bank, filename);

    if (FlashApi_setImageToInvalid((unsigned char) bank) == true)
    {
        if (isBoardKW2() == true)
        {
            return writeKw2SquashToFlash(bank, filename);
        }
        else
        {
            return writeMcUnifiedImgToFlash(bank, filename);
        }
    }
    else
    {
        printf("%s: flashApi_setImageToInvalid for bank %c failed\n", __FUNCTION__, bank);
    }
    return false;
}

