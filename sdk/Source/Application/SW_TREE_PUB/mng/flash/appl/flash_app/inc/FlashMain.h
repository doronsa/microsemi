/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Flash                                                     **/
/**                                                                          **/
/**  FILE        : FlashMain.h                                               **/
/**                                                                          **/
/**  DESCRIPTION : This file is the Flash Image main include file            **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Feb04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/


#ifndef __INCFlashMainh
#define __INCFlashMainh

#include <stdint.h>

#include "globals.h"
#include "errorCode.h"

#include "OsGlueLayer.h"
#include "FlashExports.h"


/* Defines
------------------------------------------------------------------------------*/
#define FW_PRINTENVSTR                        "fw_printenv"
#define FW_SETENVSTR                          "fw_setenv"
#define UBOOTVAR_IMAGE_ACTIVATION_STR         "act_test" 
#define UBOOTVAR_ACT_BOOT_COMPLETE_STR        "act_boot_complete" 
#define BOOTCMD_STR                           "bootcmd"
#define BOOTCMD_IMG_ACTIVATION_STR            "bootcmd_active"
#define UBOOTVAR_COMMITTED_BANK_STR           "committedBank" 
#define UBOOTVAR_ISVALID_PREFIX               "isValid" 

#define UBOOTVAR_VERSION_PREFIX               "version" 

#define ID_BANK_A                             'A'
#define ID_BANK_B                             'B'



// Queue size should be small - 
// do not expect huge flash activity for writing software images
#define MAX_FLASHMSGQ_ENTRIES          20

// The largest message size for the Flash queue 
#define FLASHMSGSIZE                  120


/* Typedefs
------------------------------------------------------------------------------*/
typedef enum
{
    FWS_IDLE, FWS_ACTIVE, FWS_FAILED, FWS_SUCCESS, FWS_CANCEL_PENDING
} FLASHWRITE_STATUS;


// Flash resources and status
typedef struct
{
    FLASHWRITE_STATUS  flashWriteState;              // Status of write
    char               bank;                         // Targer bank for write 
    bool               isUbiFsMounted;               // During flash programming: Has UBIFS been mounted
    bool               isSquashFsMounted;            // During flash programming: Has UBIFS been mounted
    FILE               *procMtdFile;                 // /proc/mtd file handle
    GL_TASK_ID         writeTaskId;                  // Worker task Id
    char               version[VERSION_NAME_LENGTH]; // Name of version
    uint8_t            clientId[CLIENTID_SIZE];      // Client Id
    uint32_t           elapsedMilliSecs;             // Elapsed time last operation
    struct
    {
        FLASHWRITE_STATUS  completionState;              // Status of last write: SUCCESS or FAIL
        char               bank;                         // Targer bank for write 
        char               version[VERSION_NAME_LENGTH]; // Name of version
    } lastWrite;
} FlashWorker_S;


// Flash resources and status
typedef struct
{
    FlashWorker_S      worker;
} FlashResources_S;


typedef struct
{
    uint32_t starts;
    uint32_t startsFailed;
    uint32_t aborts;
    uint32_t writesOk;
    uint32_t writesFailed;
    uint32_t writeThreadCreateFailures;
} FlashStatistics_S;


typedef struct
{
    uint8_t bank;
    char    filename[FILENAME_SIZE];
} WriteFlashThreadArgs_S;


/* Routine exports
-----------------------------------------------------------------------------*/
E_ErrorCodes flashStartup ();
extern bool flash_startWrite(uint8_t bank, char *filename, uint8_t *clientId);
extern void flash_abortWrite();


extern void showFlashStatistics ();
extern void showFlashResources ();

extern FlashWorker_S     *getFlashWorker();
extern FlashStatistics_S *getFlashStatistics();

extern char translateBankToPartition(char bank);

#endif
