/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Flash                                                     **/
/**                                                                          **/
/**  FILE        : FlashImage.c                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file deals with writing software image to flash      **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Feb04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <unistd.h>
#include <pthread.h>

#include "FlashMain.h"
#include "FlashExports.h"

#define CHAR_SEMI_COLON                       (0x3a)
#define CHAR_DOUBLE_QUOTE                     (0x22)
#define CHAR_SPACE                            (0x20)

// KW2
#define UIMAGE_STR                            "uimage"
#define ROOTFS_STR                            "rootfs"

#define MC_UIMAGE_STR                         "uImg"
#define MC_ROOTFS_STR                         "rootFs"

static char *KW2_templateBootCmd = "%s %s 'setenv bootargs ${console} ubi.mtd=%i root=ubi0:rootfs%c rootfstype=ubifs ${mvNetConfig} ${mvPhoneConfig}; nand read.e ${loadaddr} 0x%x 0x%x; bootm ${loadaddr};'";
static char *MC_templateBootCmd  = "%s %s 'setenv bootargs ${console} root=/dev/mtdblock%i rootfstype=squashfs ${mtdParts} ${mvNetConfig} ${mvPhoneConfig}; sf read ${loadaddr} 0x%x 0x%x; bootm ${loadaddr}'";

/******************************************************************************
 *
 * Function   : extractMtdInfoForBootCmd_KW2
 *              
 * Description: (KW2) This function extracts MTD info for boot commands
 *              
 * Parameters :  
 *
 * Returns    : bool     true = success
 *              
 ******************************************************************************/

static bool extractMtdInfoForBootCmd_KW2(unsigned char bank, int *mtdNum, uint32_t *startAddress, uint32_t *length)
{
    FILE      *procMtdFile;
    char      line[300];
    char      temp_mtd_name[20];
    char      temp_name[20];
    uint32_t  temp_size;
    uint32_t  temp_erasesize;
    char      str[20];
    uint32_t  uimageStartAddress = 0;
    uint32_t  uimageLength       = 0;
    int       rootfsMtdNum;
    bool      uimageMtdFound = false;
    bool      rootfsMtdFound = false;
    uint32_t  indx;
    char      xlatedBank = translateBankToPartition(bank);

    // Do /proc/mtd analysis
    if ((procMtdFile = fopen("/proc/mtd","r")) == NULL)
    {
        printf("Opening /proc/mtd failed\n");
        return false;
    }
    
    // Skip the header line
    fgets(line, sizeof(line), procMtdFile);

    // Scan for matching MTD partitions
    while (fgets(line, sizeof(line), procMtdFile) != 0)
    {
        // Clean up line
        for (indx = 0; indx < strlen(line); indx++)
        {
            if (line[indx] == CHAR_SEMI_COLON || line[indx] == CHAR_DOUBLE_QUOTE || line[indx] == '\n' || line[indx] == '\r')
            {
                line[indx] = CHAR_SPACE;
            }
        }

        //printf( "%s: MTD-LINE *** %s ***\n", __FUNCTION__, line);
        temp_mtd_name[0] = 0;
        temp_name[0]     = 0;
        if (sscanf(line, "%s %x %x %s", temp_mtd_name, (unsigned int *)&temp_size, (unsigned int *)&temp_erasesize, temp_name) != 0)
        {
//            printf( "%s: DETAILS MTD-LINE ** %s %x %x %s ***\n", 
//                     __FUNCTION__, temp_mtd_name, (unsigned int *)temp_size, (unsigned int *)temp_erasesize, temp_name);

            if (strncmp(temp_mtd_name, "mtd", 3) != 0 && isdigit(temp_mtd_name[3]))
            {
                printf("Unexpected MTD device name %s\n", temp_mtd_name);
                break;
            }
            // Find the target MTD partitions
            // First uimageX
            sprintf(str, "%s%c", UIMAGE_STR, xlatedBank);
            if (strcmp(temp_name, str) == 0)
            {
                uimageLength = temp_size;
                printf("%s: %s found. loadAddress = 0x%x, length = 0x%x\n", __FUNCTION__, str, uimageStartAddress, uimageLength);
                uimageMtdFound = true;
            }
            // Next rootfsX
            sprintf(str, "%s%c", ROOTFS_STR, xlatedBank);
            if (strcmp(temp_name, str) == 0)
            {
                sscanf(&temp_mtd_name[3], "%d", &rootfsMtdNum);
                printf("%s: %s found. mtd = %d\n", __FUNCTION__, str, rootfsMtdNum);
                rootfsMtdFound = true;
            }
        }

        if (uimageMtdFound == false)
            uimageStartAddress += temp_size;

        if (uimageMtdFound == true && rootfsMtdFound == true)
            break;
    }
    fclose(procMtdFile);

    if (uimageMtdFound == true && rootfsMtdFound == true)
    {
        *mtdNum       = rootfsMtdNum;
        *startAddress = uimageStartAddress;
        *length       = uimageLength;
        return true;
    }
    else
    {
        printf("%s: Failed to find MTD information for bank %c\n", __FUNCTION__, bank);
        return false;
    }
}

/******************************************************************************
 *
 * Function   : extractMtdInfoForBootCmd_MC
 *              
 * Description: (MC) This function extracts MTD info for boot commands
 *              
 * Parameters :  
 *
 * Returns    : bool     true = success
 *              
 ******************************************************************************/

static bool extractMtdInfoForBootCmd_MC(unsigned char bank, int *mtdNum, uint32_t *startAddress, uint32_t *length)
{
    FILE      *procMtdFile;
    char      line[300];
    char      temp_mtd_name[20];
    char      temp_name[20];
    uint32_t  temp_size;
    uint32_t  temp_erasesize;
    char      str[20];
    uint32_t  uimageStartAddress = 0;
    uint32_t  uimageLength       = 0;
    int       rootfsMtdNum;
    bool      uimageMtdFound = false;
    bool      rootfsMtdFound = false;
    uint32_t  indx;

    // Do /proc/mtd analysis
    if ((procMtdFile = fopen("/proc/mtd","r")) == NULL)
    {
        printf("Opening /proc/mtd failed\n");
        return false;
    }
    
    // Skip the header line
    fgets(line, sizeof(line), procMtdFile);

    // Scan for matching MTD partitions
    while (fgets(line, sizeof(line), procMtdFile) != 0)
    {
        // Clean up line
        for (indx = 0; indx < strlen(line); indx++)
        {
            if (line[indx] == CHAR_SEMI_COLON || line[indx] == CHAR_DOUBLE_QUOTE || line[indx] == '\n' || line[indx] == '\r')
            {
                line[indx] = CHAR_SPACE;
            }
        }

        //printf( "%s: MTD-LINE *** %s ***\n", __FUNCTION__, line);
        temp_mtd_name[0] = 0;
        temp_name[0]     = 0;
        if (sscanf(line, "%s %x %x %s", temp_mtd_name, (unsigned int *)&temp_size, (unsigned int *)&temp_erasesize, temp_name) != 0)
        {
//            printf( "%s: DETAILS MTD-LINE ** %s %x %x %s ***\n", 
//                     __FUNCTION__, temp_mtd_name, (unsigned int *)temp_size, (unsigned int *)temp_erasesize, temp_name);

            if (strncmp(temp_mtd_name, "mtd", 3) != 0 && isdigit(temp_mtd_name[3]))
            {
                printf("Unexpected MTD device name %s\n", temp_mtd_name);
                break;
            }
            // Find the target MTD partitions
            // First uImg
            if (bank == ID_BANK_A) sprintf(str, "%s",   MC_UIMAGE_STR);
            else                   sprintf(str, "%s%c", MC_UIMAGE_STR, bank);
            if (strcmp(temp_name, str) == 0)
            {
                uimageLength = temp_size;
                printf("%s: %s found. loadAddress = 0x%x, length = 0x%x\n", __FUNCTION__, str, uimageStartAddress, uimageLength);
                uimageMtdFound = true;
            }
            // Next rootfsX
            if (bank == ID_BANK_A) sprintf(str, "%s",   MC_ROOTFS_STR);
            else                   sprintf(str, "%s%c", MC_ROOTFS_STR, bank);
            if (strcmp(temp_name, str) == 0)
            {
                sscanf(&temp_mtd_name[3], "%d", &rootfsMtdNum);
                printf("%s: %s found. mtd = %d\n", __FUNCTION__, str, rootfsMtdNum);
                rootfsMtdFound = true;
            }
        }

        if (uimageMtdFound == false)
            uimageStartAddress += temp_size;

        if (uimageMtdFound == true && rootfsMtdFound == true)
            break;
    }
    fclose(procMtdFile);

    if (uimageMtdFound == true && rootfsMtdFound == true)
    {
        *mtdNum       = rootfsMtdNum;
        *startAddress = uimageStartAddress;
        *length       = uimageLength;
        return true;
    }
    else
    {
        printf("%s: Failed to find MTD information for bank %c\n", __FUNCTION__, bank);
        return false;
    }
}

/******************************************************************************
 *
 * Function   : FlashApi_setCommittedBank
 *              
 * Description: This function sets the bootcmd and committedbank env. variables 
 *              
 * Parameters :  
 *
 * Returns    : bool     true = success
 *              
 ******************************************************************************/

bool FlashApi_setCommittedBank(unsigned char bank)
{
    char      line[300];

    sprintf(line, "%s %s %c", FW_SETENVSTR, UBOOTVAR_COMMITTED_BANK_STR, bank); 
    printf( "%s: Execute '%s'\n", __FUNCTION__, line);
    if (system(line))
    {
        printf("%s: system(\"%s\" failed\n\r", __FUNCTION__, line);
        return false;
    }
    return true;
}

/******************************************************************************
 *
 * Function   : FlashApi_prepareImageActivation_KW2
 *              
 * Description: (KW2) This function sets the bootcmd for image activation, test activation and 
 *              activation boot complete env. variables 
 *              
 * Parameters :  
 *
 * Returns    : bool   true = success
 *              
 ******************************************************************************/

static bool FlashApi_prepareImageActivation_KW2(unsigned char bank)
{
    int       rootfsMtdNum;
    uint32_t  startAddress;
    uint32_t  uimageLength;
    char      line[300];
    char      xlatedBank = translateBankToPartition(bank);

    if (extractMtdInfoForBootCmd_KW2(bank, &rootfsMtdNum, &startAddress, &uimageLength) == false)
        return false;

    sprintf(line, KW2_templateBootCmd, FW_SETENVSTR, BOOTCMD_IMG_ACTIVATION_STR, rootfsMtdNum, xlatedBank, (unsigned int)startAddress, (unsigned int)uimageLength); 
    printf("%s: Executing '%s'\n", __FUNCTION__, line);
    if (system(line))
    {
        printf("%s: system(\"%30.30s...\" failed\n\r", __FUNCTION__, line);
        return false;
    }

    sprintf(line, "%s %s 1", FW_SETENVSTR, UBOOTVAR_IMAGE_ACTIVATION_STR); 
    printf("%s: Executing '%s'\n", __FUNCTION__, line);
    if (system(line))
    {
        printf("%s: system(\"%s\"\n\r", __FUNCTION__, line);
        return false;
    }

    sprintf(line, "%s %s 0", FW_SETENVSTR, UBOOTVAR_ACT_BOOT_COMPLETE_STR); 
    printf("%s: Executing '%s'\n", __FUNCTION__, line);
    if (system(line))
    {
        printf("%s: system(\"%s\"\n\r", __FUNCTION__, line);
        return false;
    }
    return true;
}

/******************************************************************************
 *
 * Function   : FlashApi_prepareImageActivation_MC
 *              
 * Description: (KW2) This function sets the bootcmd for image activation, test activation and 
 *              activation boot complete env. variables 
 *              
 * Parameters :  
 *
 * Returns    : bool   true = success
 *              
 ******************************************************************************/

static bool FlashApi_prepareImageActivation_MC(unsigned char bank)
{
    int       rootfsMtdNum;
    uint32_t  startAddress;
    uint32_t  uimageLength;
    char      line[300];

    if (extractMtdInfoForBootCmd_MC(bank, &rootfsMtdNum, &startAddress, &uimageLength) == false)
        return false;

    sprintf(line, MC_templateBootCmd, FW_SETENVSTR, BOOTCMD_IMG_ACTIVATION_STR, rootfsMtdNum, (unsigned int)startAddress, (unsigned int)uimageLength); 
    printf("%s: Executing '%s'\n", __FUNCTION__, line);
    if (system(line))
    {
        printf("%s: system(\"%30.30s...\" failed\n\r", __FUNCTION__, line);
        return false;
    }

    sprintf(line, "%s %s 1", FW_SETENVSTR, UBOOTVAR_IMAGE_ACTIVATION_STR); 
    printf("%s: Executing '%s'\n", __FUNCTION__, line);
    if (system(line))
    {
        printf("%s: system(\"%s\"\n\r", __FUNCTION__, line);
        return false;
    }

    sprintf(line, "%s %s 0", FW_SETENVSTR, UBOOTVAR_ACT_BOOT_COMPLETE_STR); 
    printf("%s: Executing '%s'\n", __FUNCTION__, line);
    if (system(line))
    {
        printf("%s: system(\"%s\"\n\r", __FUNCTION__, line);
        return false;
    }
    return true;
}

/******************************************************************************
 *
 * Function   : FlashApi_prepareImageActivation
 *              
 * Description: This function sets the bootcmd for image activation, test activation and 
 *              actibvation boot complete env. variables 
 *              
 * Parameters :  
 *
 * Returns    : bool   true = success
 *              
 ******************************************************************************/

bool FlashApi_prepareImageActivation(unsigned char bank)
{
    int       rootfsMtdNum;
    uint32_t  startAddress;
    char      line[300];

    if (isBoardKW2() == true)
    {
        return FlashApi_prepareImageActivation_KW2(bank);
    }
    else
    {
        return FlashApi_prepareImageActivation_MC(bank);
    }
}

/******************************************************************************
 *
 * Function   : FlashApi_setImageToInvalid
 *              
 * Description: This function sets the specified bank's validity flag to false
 *              
 * Parameters :  
 *
 * Returns    : bool   true = success
 *              
 ******************************************************************************/

bool FlashApi_setImageToInvalid(unsigned char bank)
{
    char  line[120];
    char  ucBank = toupper(bank);

    sprintf(line, "%s %s%c false", FW_SETENVSTR, UBOOTVAR_ISVALID_PREFIX, ucBank); 
    printf("%s: Executing '%s'\n", __FUNCTION__, line);
    if (system(line))
    {
        printf("%s: system(\"%s\"\n", __FUNCTION__, line);
        return false;
    }
    return true;
}

/******************************************************************************
 *
 * Function   : FlashApi_getStatus
 *              
 * Description: This function returns progress indication. When write is done
 *              returns values - completion status, version
 *              
 * Parameters :  
 *
 * Returns    : bool   always true
 *              
 ******************************************************************************/
bool FlashApi_getBurnStatus(BurnStatus_S* p_BurnStatus)
{
    FlashWorker_S *p_FlashWorker = getFlashWorker();
    int           verslen =  VERSION_NAME_LENGTH;

    memcpy(p_BurnStatus->clientId, p_FlashWorker->clientId, sizeof(p_BurnStatus->clientId));

    if (p_FlashWorker->flashWriteState != FWS_IDLE)
    {
        p_BurnStatus->inprogress = true;
        p_BurnStatus->bank       = p_FlashWorker->bank;
    }
    else
    {
        p_BurnStatus->inprogress = false;
        p_BurnStatus->bank       = p_FlashWorker->lastWrite.bank;

        if (p_FlashWorker->lastWrite.completionState == FWS_SUCCESS)
        {
            p_BurnStatus->iswriteok  = true;
            memcpy(p_BurnStatus->bankversion, p_FlashWorker->lastWrite.version, verslen);
        }
        else
        {
            p_BurnStatus->iswriteok  = false;
            memset(p_BurnStatus->bankversion, 0, verslen);
        }
    }
    return true;
}

bool FlashApi_startWriteFlash(unsigned char bank, char filename[FILENAME_SIZE], char clientId[CLIENTID_SIZE])
{
    return flash_startWrite(bank, filename, clientId);
}

bool FlashApi_abortWriteFlash(void)
{
    flash_abortWrite();

    return true;
}

bool FlashApi_isDualImageConfigured(bool *isDualImage)
{
    *isDualImage = isDualImageConfigured();

    return true;
}
