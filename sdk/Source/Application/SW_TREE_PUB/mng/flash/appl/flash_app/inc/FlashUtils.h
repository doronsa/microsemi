
/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Flash                                                     **/
/**                                                                          **/
/**  FILE        : FlashUtils.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file is the Flash utils include file                 **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    22Feb04     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/


#ifndef __INCFlashUtilsh
#define __INCFlashUtilsh

#include <stdint.h>

//#include "globals.h"
//#include "errorCode.h"

#include "FlashExports.h"

typedef struct
{
    char  *varName;
    int   varEnum;
} UBVarNameEnum_S;

typedef enum
{
    UBV_COMMITTED_ENUM, UBV_ISVALID_A_ENUM, UBV_ISVALID_B_ENUM, UBV_VERSION_A_ENUM, UBV_VERSION_B_ENUM, UBV_BOOTCMD_ENUM
} UBV_SWIM_ENUMS;

#define UBV_COMMITTED_STR       "committedBank"
#define UBV_ISVALID_A_STR       "isValidA"
#define UBV_ISVALID_B_STR       "isValidB"
#define UBV_VERSION_A_STR       "versionA"
#define UBV_VERSION_B_STR       "versionB"
#define UBV_BOOTCMD_STR         "bootcmd"

#define SOC_MC_STRING           "6601"

typedef struct 
{
    bool found;
    bool value;
} IsValid_S;

/* Defines
------------------------------------------------------------------------------*/
typedef struct
{
    IsValid_S isValidA;
    IsValid_S isValidB;
    char      committedBank;
    char      versionA[15];
    char      versionB[15];
    char      bootcmd[300];
} FlashImageDb_S;

typedef enum{
    UNUSED_SOC_TYPE    = 0,
    MV_SOC_6510,
    MV_SOC_6530,
    MV_SOC_6550,
    MV_SOC_6560,
    MV_SOC_END
} MV_SOC_TYPE_E;

typedef struct
{
    MV_SOC_TYPE_E        type;
    char                *socString;

} MV_SOC_TYPE_T;

extern void printUBootFlashImageVars();
extern void doCommitImage(char bank);
extern void parseSwImageUBootVars();
extern void doActivateImage(char bank, bool doReboot);
extern bool isDualImageConfigured();

extern bool isBoardMC();
extern bool isBoardKW2();

#endif
