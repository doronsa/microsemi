/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Flash                                                      **/
/**                                                                          **/
/**  FILE        : FlashImage.h                                              **/
/**                                                                          **/
/**  DESCRIPTION : This file deals with writing software image to flash      **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    24Feb11     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCFlashImageh
#define __INCFlashImageh

#include <stdbool.h>

typedef struct
{
    char          mtd_name[20];
    unsigned int  size;
    unsigned int  erasesize;
    char          name[20];
} MtdInfo_S;
#define MAX_MTD_PARTITIONS         10


#define CMDLINEBUFF                1024                /* Defines command line buffer size */


#define MOUNT_POINT_SQUASHFS       "/tmp/squashfs"

// KW2 system
#define MOUNT_POINT_UBIFS          "/tmp/ubifs"
#define UIMAGE_MTD_PREFIX          "uimage"
#define ROOTFS_MTD_PREFIX          "rootfs"

// MC system
#define UIMAGE_MTD_PREFIX_MC       "uImg"
#define ROOTFS_MTD_PREFIX_MC       "rootFs"
#define VARS_MTD_PREFIX_MC         "vars"


#define UIMAGE_FILE                "boot/uImage"
#define VERSION_FILE               "etc/version.txt"


extern bool writeImageToFlash(char bank);

extern void *writeFlashThread(void *args);

extern void analyzeUbiMounts();

#endif
