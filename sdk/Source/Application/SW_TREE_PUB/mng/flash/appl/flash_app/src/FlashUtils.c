/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Flash                                                     **/
/**                                                                          **/
/**  FILE        : FlashStartup.c                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file is the Flash Image startup                      **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    11May06     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>

//#include "globals.h"
#include "FlashUtils.h"
#include "FlashExports.h"
#include "FlashImage.h"
#include "FlashMain.h"


static FlashImageDb_S  g_flashImageDb;

///////////////////////////////////////////////////////////////////////////////
////////////                           ////////////////////////////////////////
////////////      Flash Utilities      ////////////////////////////////////////
////////////                           ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/******************************************************************************
 *
 * Function   : isUBCommittedValueOK
 *              
 * Description: This function checks if committed value is 'A' or 'B'
 *
 * Parameters :  
 *
 * Returns    : bool 
 *              
 ******************************************************************************/

static bool isUBCommittedValueOK(char *valueStr, char *rvalchar)
{
    char *token;

    token = strtok((char *)valueStr, "\n\r");

    if (strlen(token) == 1 && (toupper(valueStr[0]) == 'A' || toupper(valueStr[0]) == 'B'))
    {
        *rvalchar = toupper(valueStr[0]);
        return true;
    }
    printf("%s: Illegal value \"%s\"\n", __FUNCTION__, valueStr);
    return false;
}

/******************************************************************************
 *
 * Function   : isUBIsValidValueOK
 *              
 * Description: This function checks if committed value is true or false
 *
 * Parameters :  
 *
 * Returns    : bool 
 *              
 ******************************************************************************/

static bool isUBIsValidValueOK(char *valueStr, bool *rvalbool)
{
    char *token;

    token = strtok((char *)valueStr, "\n\r");

    if (strcmp(token, "true") == 0) 
    {
        *rvalbool = true;
        return true;
    }
    else if (strcmp(token, "false") == 0) 
    {
        *rvalbool = false;
        return true;
    }
    printf("%s: Illegal value \"%s\"\n", __FUNCTION__, token);
    return false;
}

/******************************************************************************
 *
 * Function   : isUBVersionValueOK
 *              
 * Description: This function checks if version has "reasonable" length, skips blanks
 *
 * Parameters :  
 *
 * Returns    : bool 
 *              
 ******************************************************************************/

static bool isUBVersionValueOK(char *valueStr, char **rvalstr)
{
    uint32_t  indx;
    char      *token;

    token = strtok((char *)valueStr, "\n\r");

    for (indx = 0; indx < strlen(token); indx++)
    {
        if (token[indx] != ' ')
        {
            *rvalstr = &token[indx];
            return true;
        }
    }
    printf("%s: Illegal value \"%s\"\n", __FUNCTION__, valueStr);
    return false;
}

static UBVarNameEnum_S uBVarNameEnumAra[] =
{
    {UBV_COMMITTED_STR,           UBV_COMMITTED_ENUM},
    {UBV_ISVALID_A_STR,           UBV_ISVALID_A_ENUM},
    {UBV_ISVALID_B_STR,           UBV_ISVALID_B_ENUM},
    {UBV_VERSION_A_STR,           UBV_VERSION_A_ENUM},
    {UBV_VERSION_B_STR,           UBV_VERSION_B_ENUM},
    {UBV_BOOTCMD_STR,             UBV_BOOTCMD_ENUM  },
};
static int uBVarNameEnumAra_size = sizeof(uBVarNameEnumAra)/sizeof(uBVarNameEnumAra[0]);

static MV_SOC_TYPE_T board_type_ara[] =
{
    {MV_SOC_6510, "6510"},
    {MV_SOC_6530, "6530"},
    {MV_SOC_6550, "6550"},
    {MV_SOC_6560, "6560"},
};

/******************************************************************************
 *
 * Function   : dealWithOneUBVar
 *              
 * Description: This function parses the U-Boot printenv output
 *
 * Parameters :  
 *
 * Returns    : bool - var found and inserted into Profile Cache DB; else false 
 *              
 ******************************************************************************/

static bool dealWithOneUBVar(FlashImageDb_S *p_FlashImageDb, char *line)
{
    int                 indx;
    UBVarNameEnum_S     *p_UBVarNameEnum;
    bool                rc = false;
    int                 namesize;
    char                rvalchar;
    bool                rvalbool;
    char                *rvalstr;

    for (indx = 0; indx < uBVarNameEnumAra_size; indx++)
    {
        p_UBVarNameEnum = &uBVarNameEnumAra[indx];
        namesize        = strlen(p_UBVarNameEnum->varName);

        // Look for matching U-Boot var followed by '=' 
        if (strncmp(line, p_UBVarNameEnum->varName, namesize) == 0 && line[namesize] == '=')
        {
            //printf("%s: Found U-Boot var %s\n", __FUNCTION__, line);

            if (p_UBVarNameEnum->varEnum == UBV_COMMITTED_ENUM)
            {
                if (isUBCommittedValueOK(&line[namesize+1], &rvalchar) == true)
                {
                    p_FlashImageDb->committedBank = rvalchar;
                    rc = true;
                    break;
                }
            }
            else if (p_UBVarNameEnum->varEnum == UBV_ISVALID_A_ENUM)
            {
                if (isUBIsValidValueOK(&line[namesize+1], &rvalbool) == true)
                {
                    p_FlashImageDb->isValidA.found = true;
                    p_FlashImageDb->isValidA.value = rvalbool;
                    rc = true;
                    break;
                }
            }
            else if (p_UBVarNameEnum->varEnum == UBV_ISVALID_B_ENUM)
            {
                if (isUBIsValidValueOK(&line[namesize+1], &rvalbool) == true)
                {
                    p_FlashImageDb->isValidB.found = true;
                    p_FlashImageDb->isValidB.value = rvalbool;
                    rc = true;
                    break;
                }
            }
            else if (p_UBVarNameEnum->varEnum == UBV_VERSION_A_ENUM)
            {
                if (isUBVersionValueOK(&line[namesize+1], &rvalstr) == true)
                {
                    char *token;

                    // So we do not copy \n at end of line
                    token = strtok(rvalstr, "\n\r");

                    strcpy(p_FlashImageDb->versionA, token);
                    rc = true;
                    break;
                }
            }
            else if (p_UBVarNameEnum->varEnum == UBV_VERSION_B_ENUM)
            {
                if (isUBVersionValueOK(&line[namesize+1], &rvalstr) == true)
                {
                    char *token;

                    // So we do not copy \n at end of line
                    token = strtok(rvalstr, "\n\r");

                    strcpy(p_FlashImageDb->versionB, token);
                    rc = true;
                    break;
                }
            }
            else if (p_UBVarNameEnum->varEnum == UBV_BOOTCMD_ENUM)
            {
                char *token;

                // So we do not copy \n at end of line
                token = strtok(&line[namesize+1], "\n\r");

                strcpy((char *)p_FlashImageDb->bootcmd, token);
                rc = true;
                break;
            }
        }
    }
    return rc;
}


/******************************************************************************
 *
 * Function   : parseSwImageUBootVars
 *              
 * Description: This function parses the U-Boot Sw Image variables
 *
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

void parseSwImageUBootVars()
{
    char            line[500];
    int             lineIndx   = 0;
    int             parsedVars = 0;
    FILE            *fptr;
    char            *outFile = "/tmp/printenv.txt";
    FlashImageDb_S  *p_FlashImageDb = &g_flashImageDb;


    memset(p_FlashImageDb, 0, sizeof(FlashImageDb_S));
    p_FlashImageDb->isValidA.found = false;
    p_FlashImageDb->isValidB.found = false;

    sprintf(line, "%s > %s", FW_PRINTENVSTR, outFile);

    if (system(line))
    {
        printf("%s: \"%s\" FAILED. \n", __FUNCTION__, line);
    }
    else
    {
        if ((fptr = fopen(outFile, "r")) != 0)
        {
            while (fgets(line, sizeof(line), fptr) != 0)
            {
                lineIndx++;
                if (dealWithOneUBVar(p_FlashImageDb, line) == true)
                {
                    parsedVars++;
                }
            }      
            //printf("%s: Read %d lines from %s output. Parsed %d variables\n", __FUNCTION__, lineIndx, FW_PRINTENVSTR, parsedVars);
        }
        else
        {
            printf("%s: fopen(%s, \"r\") FAILED, errno = %d(%s). \n", __FUNCTION__, outFile, errno, strerror(errno));
        }
        fclose(fptr);
    }
}


/******************************************************************************
 *
 * Function   : printUBootFlashImageVars
 *              
 * Description: This function displays the U-Boot Sw Image flash variables
 *
 * Parameters :  
 *
 * Returns    : void 
 *              
 ******************************************************************************/

void printUBootFlashImageVars(char* name)
{
    FlashImageDb_S  *p_FlashImageDb = &g_flashImageDb;
    char            tempc;

    parseSwImageUBootVars();

    mipc_printf(name,  "U-Boot Sw Image: isValidA      : %s\n", (p_FlashImageDb->isValidA.found == false) ? "not-found" : (p_FlashImageDb->isValidA.value == true) ? "true" : "false");
    mipc_printf(name,  "U-Boot Sw Image: isValidB      : %s\n", (p_FlashImageDb->isValidB.found == false) ? "not-found" : (p_FlashImageDb->isValidB.value == true) ? "true" : "false");
    mipc_printf(name,  "U-Boot Sw Image: versionA      : %s\n", p_FlashImageDb->versionA);
    mipc_printf(name,  "U-Boot Sw Image: versionB      : %s\n", p_FlashImageDb->versionB);
    tempc = p_FlashImageDb->committedBank;
    mipc_printf(name,  "U-Boot Sw Image: committedBank : %c\n", (tempc == 0) ? '0' : tempc);
    mipc_printf(name,  "U-Boot Sw Image: bootcmd       : %s\n", p_FlashImageDb->bootcmd);
}

/******************************************************************************
 *
 * Function   : doCommitImage
 *              
 * Description: This function commits image: committedBank= to flash
 *
 * Parameters :  
 *
 * Returns    : void 
 *              
 ******************************************************************************/

void doCommitImage(char bank)
{
    FlashImageDb_S  *p_FlashImageDb = &g_flashImageDb;
    bool            goAhead = false;
    char            ucBank = toupper(bank);

    // Make sute that we are up to date
    parseSwImageUBootVars();

    if (ucBank == ID_BANK_A && (p_FlashImageDb->isValidA.found == true && p_FlashImageDb->isValidA.value == true))
    {
        goAhead = true;
    }
    else if (ucBank == ID_BANK_B && (p_FlashImageDb->isValidB.found == true && p_FlashImageDb->isValidB.value == true))
    {
        goAhead = true;
    }

    if (goAhead == true)
    {
        if (FlashApi_setCommittedBank(ucBank) == true) printf( "%s: flashApi_setCommittedBank OK\n", __FUNCTION__);
        else                                                printf( "%s: flashApi_setCommittedBank failed\n", __FUNCTION__);
    }
    else
    {
        printf( "%s: Bank %c is NOT valid\n", __FUNCTION__, ucBank);
    }
}

/******************************************************************************
 *
 * Function   : doActivateImage
 *              
 * Description: This function activates image
 *
 * Parameters :  
 *
 * Returns    : void 
 *              
 ******************************************************************************/

void doActivateImage(char bank, bool doReboot)
{
    FlashImageDb_S  *p_FlashImageDb = &g_flashImageDb;
    bool            goAhead = false;
    char            ucBank = toupper(bank);

    // Make sute that we are up to date
    parseSwImageUBootVars();

    if (ucBank == ID_BANK_A && (p_FlashImageDb->isValidA.found == true && p_FlashImageDb->isValidA.value == true))
    {
        goAhead = true;
    }
    else if (ucBank == ID_BANK_B && (p_FlashImageDb->isValidB.found == true && p_FlashImageDb->isValidB.value == true))
    {
        goAhead = true;
    }

    if (goAhead == true)
    {
        if (FlashApi_prepareImageActivation(ucBank) == true) 
        {
            printf( "%s: FlashApi_prepareImageActivation OK\n", __FUNCTION__);
            if (doReboot == true)
            {
                printf( "%s: Rebooting system...\n", __FUNCTION__);
                system("reboot");
            }
        }
        else
        {
            printf( "%s: FlashApi_prepareImageActivation failed\n", __FUNCTION__);
        }
    }
    else
    {
        printf( "%s: Bank %c is NOT valid\n", __FUNCTION__, ucBank);
    }
}

/******************************************************************************
 *
 * Function   : matchBoardType
 *
 * Description: This function determines if board has given SoC
 *
 * Parameters :
 *
 * Returns    : bool
 *
 ******************************************************************************/

static bool matchBoardType(const char *socType)
{
    char  line[500];
    FILE  *fptr;
    bool  rc = false;

    // Read the Linux command line - to identify the active bank
    if ((fptr = fopen("/proc/board_type","r")) != NULL)
    {
        if (fgets(line, sizeof(line), fptr) != 0)
        {
            if (strstr(line, socType) != 0)
            {
                rc = true;
            }
        }
        else
        {
            printf("%s: Failed to get /proc/board_type text\n", __FUNCTION__);
        }
        fclose(fptr);
    }
    else
    {
        printf("%s: fopen(/proc/board_type, \"r\") FAILED, errno = %d(%s).\n", __FUNCTION__, errno, strerror(errno));
    }
    return rc;
}

/******************************************************************************
 *
 * Function   : isBoardMC
 *
 * Description: This function determines if board has MC SoC
 *
 * Parameters :
 *
 * Returns    : bool
 *
 ******************************************************************************/

bool isBoardMC()
{
    return matchBoardType(SOC_MC_STRING);
}

/******************************************************************************
 *
 * Function   : isBoardKW2
 *
 * Description: This function determines if board has KW2 SoC
 *
 * Parameters :
 *
 * Returns    : bool
 *
 ******************************************************************************/

bool isBoardKW2()
{
    bool   isKW2 = false;
    int    index;

    for (index=0; index<sizeof(board_type_ara)/sizeof(board_type_ara[0]); index++)
    {
        isKW2 = matchBoardType(board_type_ara[index].socString);
        if ( isKW2 == true )
            break;
    }

    return isKW2;

}


#if 0
/******************************************************************************
 *
 * Function   : isImageValid
 *              
 * Description: This function returns isValidX for the bank
 *
 * Parameters :  
 *
 * Returns    : bool
 *              
 ******************************************************************************/

static bool isImageValid(char bank)
{
    FlashImageDb_S  *p_FlashImageDb = &g_flashImageDb;
    char            ucBank = toupper(bank);

    if (ucBank == ID_BANK_A && p_FlashImageDb->isValidA.found == true)
    {
        return p_FlashImageDb->isValidA.value;
    }
    else if (ucBank == ID_BANK_B && p_FlashImageDb->isValidB.found == true)
    {
        return p_FlashImageDb->isValidB.value;
    }

    printf( "%s: Bank %c is NOT valid/isValid%c not defined in U-Boot\n", __FUNCTION__, ucBank, ucBank);
    return false;
}
#endif

#define DUALIMAGEYES_STR    "dual_image=yes"

bool isDualImageConfigured()
{
    char        line[500];
    FILE        *fptr;
    char        *outFile = "/tmp/swdldual.txt";
    bool        rc = false;
    struct stat statbuf;

    sprintf(line, "%s | grep '%s' > %s", FW_PRINTENVSTR, DUALIMAGEYES_STR, outFile);

    system(line);

    if (stat(outFile, &statbuf) == -1) 
    {
        printf("%s: stat of %s, failed - %s (%d)\n", __FUNCTION__, outFile, strerror(errno), errno);
    }
    else
    {
        //printf("%s: Size of file %s is %jd\n", __FUNCTION__, outFile, (intmax_t)statbuf.st_size);
        if (statbuf.st_size > 0)
        {
            rc = true;
        }
    }

    return rc;
}


