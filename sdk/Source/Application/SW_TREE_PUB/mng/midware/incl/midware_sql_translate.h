#ifndef __MIDWARE_SQL_TRANSLATE_H__
#define __MIDWARE_SQL_TRANSLATE_H__

#define  POS_PACKED      __attribute__ ((__packed__))
//#define  VOID void


/*
typedef unsigned int UINT32;
typedef unsigned short UINT16;
typedef unsigned char UINT8;
*/

#define MIDWARE_TRACE_LEVEL_FATAL   0x80000000 /*Currently, not used */
#define MIDWARE_TRACE_LEVEL_ERROR   0xC0000000
#define MIDWARE_TRACE_LEVEL_WARN    0xE0000000
#define MIDWARE_TRACE_LEVEL_INFO    0xF0000000
#define MIDWARE_TRACE_LEVEL_DEBUG   0xF8000000

/* Midware DB saving parameters */
#define MIDWARE_DB_DEFAULT_INTERVAL 10
#define XML_CFG_FILE_MIDWARE        "/etc/xml_params/midware_xml_cfg_file.xml"
#define ELM_MIDWARE_CONFIG          "MIDWARE_CONFIG"
#define ELM_saveDBflag              "saveDBflag"
#define ELM_saveDBInterval          "saveDBInterval"
#define ATTR_enabled                "enabled"
#define ATTR_intervalTime           "intervalTime"
#define SECOND_TO_MSECOND           (1000)
#define ELM_uniType                 "uniType"
#define ATTR_uni                    "uni"
#define ATTR_src_port               "src_port"
#define ATTR_phy_type               "phy_type"


#define MIDWARE_ALL_TRACE_LEVEL     0xFFFF0000

extern UINT32 midware_glob_trace;

#define MDW_GLOB_TRACE      midware_glob_trace

#define MDW_DEBUG(LEVEL, FORMAT, ...) if (((LEVEL) & MDW_GLOB_TRACE)==(LEVEL)) {printf("%s(%d):  "FORMAT,__FUNCTION__,__LINE__, ##__VA_ARGS__);}

#define MDW_TRACE      MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO, "\n")

#define SFU_NO_MORE              2


#define MIDWARE_MAX_COL_PER_TABLE        64
#define MIDWARE_MAX_TABLE_NUM            100
//#define FALSE        0
//#define TRUE        1

#define MIDWARE_STR_ADD_TO_TAIL(DST_STR, SRC_STR)        sprintf((DST_STR) + strlen((DST_STR)), "%s", (SRC_STR))

typedef enum
{
    DATA_TYPE_UINT8,
    DATA_TYPE_UINT16,
    DATA_TYPE_UINT32,
    DATA_TYPE_ARRAY,
    DATA_TYPE_STRING
}MIDWARE_COL_DATA_TYPE;

/*
typedef enum
{
    READONLY,
    READ_WRITE
}MIDWARE_COL_READONLY;
*/

typedef struct
{
    char   *col_name;
    UINT32 size;
    MIDWARE_COL_DATA_TYPE  datatype;
    BOOL  readonly;
    BOOL  primary_key;
    BOOL  save_to_flash;
}MIDWARE_COL_INFO;

typedef struct
{
    MIDWARE_TABLE_ID_E    table_id;
    char                *table_name;
    UINT8                col_num;
    MIDWARE_COL_INFO    col_info[MIDWARE_MAX_COL_PER_TABLE];
}MIDWARE_TABLE_INFO;

/*
typedef struct
{
    MIDWARE_TABLE_ID_E                            table_id;
    sqlite3 *db;
}MIDWARE_TABLE_DB_INFO;
*/

typedef struct
{
    MIDWARE_TABLE_ID_E                            table_id;
    sqlite3 *db;
}MIDWARE_TABLE_DB_INFO;

extern sqlite3 *db;
extern MIDWARE_TABLE_INFO  g_midware_table_info[];

/******************************************************************************
 *
 * Function   : midware_get_uni_port_type
 *
 * Description: This function returns PHY type by input src port
 *
 * Parameters :
 *
 * Returns    : UINT32
 *
 ******************************************************************************/

UINT32 midware_get_uni_port_type(UINT32 src_port);

ONU_STATUS  midware_set_db_status_flag(MIDWARE_DB_STATUS_E status);

MIDWARE_DB_STATUS_E  midware_get_db_status_flag(void);

#endif
