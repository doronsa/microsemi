/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Middleware module                                         **/
/**                                                                          **/
/**  FILE        : midware_expo.h                                            **/
/**                                                                          **/
/**  DESCRIPTION : Definition of midware tables, bitmaps and structures      **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    Victor, 18/Feb/2011   - initial version created.                              
 *                                                                      
 ******************************************************************************/
 
#ifndef __MIDWARE_EXPO_H__
#define __MIDWARE_EXPO_H__

/*----------------------------------------------------------------------------*/
/* macro definition                                                           */
/*----------------------------------------------------------------------------*/
#define MIDWARE_SFU_VENDOR_ID           "MRVL"
#define MIDWARE_SFU_MODULE_ID           "6510"
#define MIDWARE_SFU_HARDWARE_VERSION    "M88F6510"
#define MIDWARE_SFU_SOFTWARE_VERSION    "2.4.11-RC2"
#define MIDWARE_SFU_FIRMWARE_VERSION    {0, 0x2, 0, 0x19}
#define MIDWARE_SFU_CHIP_ID             "MV"
#define MIDWARE_SFU_CHIP_VERSION        {0xa, 0x8, 0x1a}
#define MIDWARE_SFU_CHIP_MODEL          0x6510
#define MIDWARE_SFU_CHIP_REV            0x2
#define MIDWARE_SFU_SN                  "MRVL_SN"
#define MIDWARE_SFU_GE_PORT_NUM         1
#define MIDWARE_SFU_GE_PORT_BITMAP      0x8
#define MIDWARE_SFU_FE_PORT_NUM         3
#define MIDWARE_SFU_FE_PORT_BITMAP      7
#define MIDWARE_SFU_POTS_PORT_NUM       2
#define MIDWARE_SFU_E1_PORT_NUM         0
#define MIDWARE_SFU_WLAN_PORT_NUM       0
#define MIDWARE_SFU_USB_PORT_NUM        0
#define MIDWARE_SFU_UPSTREAM_QUEUES_NUMBER          4 
#define MIDWARE_SFU_UPSTREAM_QUEUES_PER_PORT        8 
#define MIDWARE_SFU_DOWNSTREAM_QUEUES_NUMBER        4
#define MIDWARE_SFU_DOWNSTREAM_QUEUES_PER_PORT      8
#define MIDWARE_SFU_LLID_NUM            8
#define MIDWARE_SFU_QUEUE_NUM_PER_LLID  4
#define MIDWARE_SFU_CTC_STACK_ENABLE    1
#define MIDWARE_SFU_MTU_DEFAULT_SIZE    1518
#define MIDWARE_SFU_ENABLE              1
#define MIDWARE_SFU_DISABLE             0
#define MIDWARE_SFU_AUTONEG             1
#define MIDWARE_SFU_SPEED_10            0
#define MIDWARE_SFU_SPEED_100           1
#define MIDWARE_SFU_SPEED_1000          2
#define MIDWARE_SFU_SPEED_AUTO          3
#define MIDWARE_SFU_MODE_HALF           0
#define MIDWARE_SFU_MODE_FULL           1
#define MIDWARE_SFU_MODE_AUTO           2
#define MIDWARE_SFU_DEF_VLAN            4095
#define MIDWARE_SFU_DEF_PRI             0
#define MIDWARE_SFU_BRIDGE_PRIORITY     0x8000
#define MIDWARE_SFU_FWD_DELAY           15
#define MIDWARE_SFU_HELLO_TIME          2
#define MIDWARE_SFU_AGE_TIME            20
#define MIDWARE_SFU_PORT_PRIORITY       160
#define MIDWARE_SFU_MAX_GROUP_NUM       64 /* for CTC:8, for AT:64 */
#define MIDWARE_SFU_PROTOCOL_H248               0
#define MIDWARE_SFU_PROTOCOL_SIP                1
#define MIDWARE_SFU_VOICE_IP_MODE_STATIC        0
#define MIDWARE_SFU_VOICE_IP_MODE_DHCP          1
#define MIDWARE_SFU_VOICE_IP_MODE_PPPOE         2
#define MIDWARE_SFU_H248_MG_PORT_NUM            2944
#define MIDWARE_SFU_SIP_MG_PORT_NUM             5060
#define MIDWARE_SFU_H248_HEARTBEAT_MODE_CLOSE   0
#define MIDWARE_SFU_SIP_HEARTBEAT_SWITCH_OPEN   0
#define MIDWARE_SFU_HEARTBEAT_CYCLE             60
#define MIDWARE_SFU_HEARTBEAT_HEARTBEAT_COUNT   3
#define MIDWARE_SFU_SIP_REG_INTERVAL            3600
#define MIDWARE_SFU_FAX_CTRL_NEG                0
#define MIDWARE_SFU_OAM_STACK_DEREG             1
#define MIDWARE_SFU_OAM_STACK_COMPLETE          4 
#define MIDWARE_TAG_TCI_NO_USE                  0xffff0000

#define MIDWARE_IGMP_MAX_RATE                 (2000)
#define MIDWARE_IGMP_QUERY_DEFT_INTERVAL      (150) /* 125s                         */
#define MIDWARE_IGMP_MAX_RESP_TIME            (100) /* 10s in unit of 1/10s         */
#define MIDWARE_IGMP_LAST_QUERY_TIME          (10)  /* 1s in unit of 1/10s          */
#define MIDWARE_IGMP_LAST_QUERY_TIME          (10)  /* 1s in unit of 1/10s          */
#define MIDWARE_DEFAULT_ZERO_VAL              (0)   /* common 0 for default value   */

#define MIDWARE_V_DEFT_REG_EXP_TIME           (3600)
#define MIDWARE_V_REREG_HEAD_START_TIME       (360)
#define MIDWARE_V_SIP_AGENT_NUM               (2)
#define MIDWARE_V_DEFT_RELEASE_TIME           (10)
#define MIDWARE_V_DEFT_ROH_TIME               (15)


/*----------------------------------------------------------------------------*/
/* Data Structure definition                                                  */
/*----------------------------------------------------------------------------*/

/* Return Code */
#define ONU_OK          (0)
#define ONU_EXIST       (1)
#define ONU_NOT_EXIST   (2)
#define ONU_FAIL        (-1)

typedef signed int ONU_STATUS;
#define VOID void

#define MIDWARE_DBA_MAX_QUEUESET_NUM  4
typedef  UINT8  uint_arr_s[MIDWARE_DBA_MAX_QUEUESET_NUM];

/*Marvell middle-ware table ID*/
typedef enum 
{
    MIDWARE_TABLE_START               = 0,
    MIDWARE_TABLE_ONU_INFO            = 0,
    MIDWARE_TABLE_ONU_CFG             = 1,
    MIDWARE_TABLE_ONU_IP              = 2,    
    MIDWARE_TABLE_UNI_CFG             = 3,
    MIDWARE_TABLE_UNI_QOS             = 4,
    MIDWARE_TABLE_VLAN                = 5,    
    MIDWARE_TABLE_FLOW                = 6,
    MIDWARE_TABLE_FLOW_MOD_VLAN       = 7,
    MIDWARE_TABLE_FLOW_MOD_MAC        = 8,
    MIDWARE_TABLE_FLOW_MOD_PPPOE      = 9,
    MIDWARE_TABLE_FLOW_MOD_IPV4       = 10,
    MIDWARE_TABLE_FLOW_MOD_IPV6       = 11,
    MIDWARE_TABLE_FLOW_KEY_L2         = 12,
    MIDWARE_TABLE_FLOW_KEY_IPV4       = 13,
    MIDWARE_TABLE_FLOW_KEY_IPV6       = 14,
    MIDWARE_TABLE_MC_CFG              = 15,
    MIDWARE_TABLE_MC_PORT_CFG         = 16,
    MIDWARE_TABLE_MC_PORT_STATUS      = 17,   
    MIDWARE_TABLE_MC_ACTIVE_GROUP     = 18,     
    MIDWARE_TABLE_MC_PORT_CONTROL     = 19,   
    MIDWARE_TABLE_MC_PORT_SERV        = 20,
    MIDWARE_TABLE_MC_PORT_PREVIEW     = 21,
    MIDWARE_TABLE_MC_DS_VLAN_TRANS    = 22,
    MIDWARE_TABLE_MC_US_VLAN_TRANS    = 23,    
    MIDWARE_TABLE_MC_LEARN            = 24,
    MIDWARE_TABLE_MC_STREAM           = 25,    
    MIDWARE_TABLE_WAN_QOS             = 26,
    MIDWARE_TABLE_EPON                = 27,
    MIDWARE_TABLE_DBA                 = 28,  
    MIDWARE_TABLE_SW_IMAGE            = 29,
    MIDWARE_TABLE_OPT_TRANSCEIVER     = 30,
    MIDWARE_TABLE_ALARM               = 31,
    MIDWARE_TABLE_PM                  = 32,    
    MIDWARE_TABLE_EPON_AUTH           = 33,
    MIDWARE_TABLE_WEB_ACCOUNT         = 34,    
    MIDWARE_TABLE_IAD_PORT_ADMIN      = 35,    
    MIDWARE_TABLE_IAD_INFO            = 36,  
    MIDWARE_TABLE_IAD_CFG             = 37, 
    MIDWARE_TABLE_H248_CFG            = 38,  
    MIDWARE_TABLE_H248_USER_INFO      = 39, 
    MIDWARE_TABLE_H248_RTP_CFG        = 40,         
    MIDWARE_TABLE_H248_RTP_INFO       = 41,
    MIDWARE_TABLE_SIP_CFG             = 42,    
    MIDWARE_TABLE_SIP_USER_INFO       = 43,    
    MIDWARE_TABLE_FAX_CFG             = 44,  
    MIDWARE_TABLE_IAD_STATUS          = 45,      
    MIDWARE_TABLE_POTS_STATUS         = 46,   
    MIDWARE_TABLE_IAD_OPERATION       = 47, 
    MIDWARE_TABLE_SIP_DIGITMAP        = 48, 
    MIDWARE_TABLE_RSTP                = 49,    
    MIDWARE_TABLE_RSTP_PORT           = 50, 
    MIDWARE_TABLE_SW_MAC              = 51,
    MIDWARE_TABLE_VLAN_PORT           = 52,
    MIDWARE_TABLE_ETH_UNI_PM          = 53,  
    MIDWARE_TABLE_V_CFG               = 54,  
    MIDWARE_TABLE_V_PORT_CFG          = 55,      
    MIDWARE_TABLE_V_SIP_AGENT         = 56,        
    MIDWARE_TABLE_V_SIP_USER          = 57,   
    MIDWARE_TABLE_V_APP_PROFILE       = 58,  
    MIDWARE_TABLE_V_FEATURE_NODE      = 59,  
    MIDWARE_TABLE_V_NET_DIAL_PLAN     = 60,  
    MIDWARE_TABLE_V_MEDIA_PROFILE     = 61, 
    MIDWARE_TABLE_V_SERV_PROFILE      = 62,
    MIDWARE_TABLE_V_TONE_PATTERN      = 63,
    MIDWARE_TABLE_V_TONE_EVENT        = 64,    
    MIDWARE_TABLE_V_RING_PATTERN      = 65,
    MIDWARE_TABLE_V_RING_EVENT        = 66,
    MIDWARE_TABLE_V_RTP_PROFILE       = 67,
    MIDWARE_TABLE_V_LINE_STATUS       = 68,
    MIDWARE_TABLE_AVC                 = 69,
    MIDWARE_TABLE_PM_HISTORY          = 70,
    MIDWARE_TABLE_END                 = 71,
    MIDWARE_TABLE_ILLEGAL_ENTRY       = 0xff
} MIDWARE_TABLE_ID_E;

/*Match all entried in specific table*/
#define MIDWARE_ALL_ENTRY 255

/*Middleware ONU info table bitmap*/
typedef enum 
{
    MIDWARE_ENTRY_INDEX_ONU_INFO      = 0x1,
    MIDWARE_ENTRY_VENDOR_ID           = 0x2,
    MIDWARE_ENTRY_MODULE_ID           = 0x4,
    MIDWARE_ENTRY_HW_VER              = 0x8,
    MIDWARE_ENTRY_SW_VER              = 0x10,
    MIDWARE_ENTRY_FIRMWARE_VER        = 0x20,
    MIDWARE_ENTRY_CHI_ID              = 0x40,
    MIDWARE_ENTRY_CHIP_MODEL          = 0x80,
    MIDWARE_ENTRY_CHIP_REVISION       = 0x100,
    MIDWARE_ENTRY_CHIP_VER            = 0x200,
    MIDWARE_ENTRY_SN                  = 0x400,    
    MIDWARE_ENTRY_GE_POER_NUM         = 0x800,
    MIDWARE_ENTRY_GE_PORT_BITMAP      = 0x1000,
    MIDWARE_ENTRY_FE_PORT_NUM         = 0x2000,
    MIDWARE_ENTRY_FE_POER_BITMAP      = 0x4000,
    MIDWARE_ENTRY_POTS_PORT_NUM       = 0x8000,
    MIDWARE_ENTRY_E1_PORT_NUM         = 0x10000,
    MIDWARE_ENTRY_WLAN_PORT_NUM       = 0x20000,
    MIDWARE_ENTRY_USB_PORT_NUM        = 0x40000,
    MIDWARE_ENTRY_US_QUEUE_NUM        = 0x80000,
    MIDWARE_ENTRY_US_PER_QUEUW_NUM    = 0x100000,
    MIDWARE_ENTRY_DS_QUEUE_NUM        = 0x200000,
    MIDWARE_ENTRY_DS_PER_QUEUE_NUM    = 0x400000,
    MIDWARE_ENTRY_BATTERT_BACKUP      = 0x800000,
    MIDWARE_ENTRY_ONU_TYPE            = 0x1000000,
    MIDWARE_ENTRY_LLID_NUM            = 0x2000000,
    MIDWARE_ENTRY_QUEUE_NUM_PER_LLID  = 0x4000000,
    MIDWARE_ENTRY_IPV6_SUPPORT        = 0x8000000,
    MIDWARE_ENTRY_ONU_POWER_CONTROL   = 0x10000000,
    MIDWARE_ENTRY_ONU_UP_TIME         = 0x20000000,     
    MIDWARE_ENTRY_ONU_SERVICE_SLA     = 0x40000000,
    MIDWARE_ENTRY_END_ONU_INFO        = 0x80000000

} MIDWARE_ONU_INFO_ENTRY_E;

/*Middleware ONU configuration table bitmap*/
typedef enum 
{
    MIDWARE_ENTRY_INDEX_ONU_CFG       = 0x1,
    MIDWARE_ENTRY_RESET_ONU           = 0x2,
    MIDWARE_ENTRY_RESTORE_ONU         = 0x4,  
    MIDWARE_ENTRY_MAC_AGE_TIME        = 0x8,
    MIDWARE_ENTRY_CLEAR_DYNAMIC_MAC   = 0x10,
    MIDWARE_ENTRY_OAM_STACK_ENABLE    = 0x20,   
    MIDWARE_ENTRY_SYNCHRONIZE_TIME    = 0x40,
    MIDWARE_ENTRY_MIB_RESET           = 0x80,
    MIDWARE_ENTRY_PM_INTERVAL         = 0x100,
    MIDWARE_ENTRY_MTU_SIZE            = 0x200,     
    MIDWARE_ENTRY_END_ONU_CFG         = 0x400   

} MIDWARE_ONU_CFG_ENTRY_E;

/*Middleware ONU IP table bitmap*/
typedef enum 
{
    MIDWARE_ENTRY_ENTITY_ID     = 0X1,
    MIDWARE_ENTRY_ENTITY_TYPE   = 0X2,
    MIDWARE_ENTRY_IP_OPTION     = 0X4,  
    MIDWARE_ENTRY_ONU_ID        = 0X8,
    MIDWARE_ENTRY_MAC           = 0X10,
    MIDWARE_ENTRY_IP            = 0X20,   
    MIDWARE_ENTRY_NETMASK       = 0X40,
    MIDWARE_ENTRY_GATEWAY       = 0X80,
    MIDWARE_ENTRY_PRI_DNS       = 0X100,
    MIDWARE_ENTRY_SEC_DNS       = 0X200,  
    MIDWARE_ENTRY_CUR_IP        = 0X400,
    MIDWARE_ENTRY_CUR_NETMASK   = 0X800,
    MIDWARE_ENTRY_CUR_GATEWAY   = 0X1000,   
    MIDWARE_ENTRY_CUR_PRI_DNS   = 0X2000,
    MIDWARE_ENTRY_CUR_SEC_DNS   = 0X4000,
    MIDWARE_ENTRY_DOMAIN_NAME   = 0X8000,  
    MIDWARE_ENTRY_HOST_NAME     = 0X10000,   
    MIDWARE_ENTRY_END_ONU_IP    = 0X20000
} MIDWARE_ONU_IP_ENTRY_E;


/*Middleware UNI port administration bitmap*/
typedef enum 
{
    MIDWARE_ENTRY_PORT_ID_UNI_CFG     = 0x1,
    MIDWARE_ENTRY_PORT_TYPE           = 0x2,    
    MIDWARE_ENTRY_PORT_ADMIN          = 0x4,
    MIDWARE_ENTRY_PORT_LINK_STATE     = 0x8,
    MIDWARE_ENTRY_PORT_AUTONEG_CFG    = 0x10,
    MIDWARE_ENTRY_PORT_AUTONEG_MODE   = 0x20,
    MIDWARE_ENTRY_PORT_RESET_AUTONEG  = 0x40,
/*  MIDWARE_ENTRY_PORT_SPEED_CFG      = 0x80,  */
    MIDWARE_ENTRY_PORT_SPEED_STATE    = 0x80,
/*  MIDWARE_ENTRY_PORT_DUPLEX_CFG     = 0x200, */
    MIDWARE_ENTRY_PORT_DUPLEX_STATE   = 0x100,
    MIDWARE_ENTRY_PORT_LOOPBACK_CFG   = 0x200,
    MIDWARE_ENTRY_PORT_ISOLATE_CFG    = 0x400,
    MIDWARE_ENTRY_PORT_FLOOD_CFG      = 0x800,
    MIDWARE_ENTRY_PORT_MAC_LEARN      = 0x1000,
    MIDWARE_ENTRY_PORT_DEFAULT_VID    = 0x2000,
    MIDWARE_ENTRY_PORT_DEFAULT_PRI    = 0x4000,
    MIDWARE_ENTRY_PORT_DISCARD_TAG    = 0x8000,
    MIDWARE_ENTRY_PORT_DISCARD_UNTAG  = 0x10000,
    MIDWARE_ENTRY_PORT_VID_FILTER     = 0x20000,
    MIDWARE_ENTRY_PORT_LOOP_DETECT    = 0x40000,
    MIDWARE_ENTRY_PORT_LD_STATUS      = 0x80000,
    MIDWARE_ENTRY_PORT_IN_MIRROR_EN   = 0x100000,
    MIDWARE_ENTRY_PORT_IN_MIRROR_SRC  = 0x200000,
    MIDWARE_ENTRY_PORT_IN_MIRROR_DST  = 0x400000,
    MIDWARE_ENTRY_PORT_OUT_MIRROR_EN  = 0x800000,
    MIDWARE_ENTRY_PORT_OUT_MIRROR_SRC = 0x1000000,
    MIDWARE_ENTRY_PORT_OUT_MIRROR_DST = 0x2000000,
    MIDWARE_ENTRY_PORT_STP_STATE      = 0x4000000,
/*  MIDWARE_ENTRY_PORT_CONFIGURATION_IND  = 0x20000000,*/
    MIDWARE_ENTRY_END_UNI_CFG         = 0x8000000

} MIDWARE_UNI_CFG_ENTRY_E;

/*Middleware UNI QoS table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_PORT_ID_UNI_QOS     = 0x1,
    MIDWARE_ENTRY_QOS_FC_CFG          = 0x2,
    MIDWARE_ENTRY_QOS_FC_STATE        = 0x4,
    MIDWARE_ENTRY_QOS_PAUSE_TIMM      = 0x8,    
    MIDWARE_ENTRY_QOS_US_POLICING_EN  = 0x10,
    MIDWARE_ENTRY_QOS_US_POLICING_MODE= 0x20,    
    MIDWARE_ENTRY_QOS_US_CIR          = 0x40,
    MIDWARE_ENTRY_QOS_US_CBS          = 0x80,
    MIDWARE_ENTRY_QOS_US_EBS          = 0x100,
    MIDWARE_ENTRY_QOS_DS_RL_EN        = 0x200, 
    MIDWARE_ENTRY_QOS_DS_RL_MODE      = 0x400,  
    MIDWARE_ENTRY_QOS_DS_RL_CIR       = 0x800,
    MIDWARE_ENTRY_QOS_DS_RL_PIR       = 0x1000,
    MIDWARE_ENTRY_END_UNI_QOS         = 0x2000
    
}MIDWARE_UNI_QOS_ENTRY_E;

/*Middleware ONU VLAN table bitmap*/
typedef enum 
{
    MIDWARE_ENTRY_VLAN_TAG_ID         = 0x1,
    MIDWARE_ENTRY_VLAN_UNTAG_VEC      = 0x2,
    MIDWARE_ENTRY_VLAN_TAG_VEC        = 0x4,
    MIDWARE_ENTRY_END_VLAN            = 0x8

}MIDWARE_VLAN_ENTRY_E;

/*Middleware multicast configuration table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_INDEX_MC_CFG        = 0x1,
    MIDWARE_ENTRY_IMGP_VERSION        = 0x2,    
    MIDWARE_ENTRY_MC_MODE             = 0x4,
    MIDWARE_ENTRY_MC_FILTER_MODE      = 0x8,  
    MIDWARE_ENTRY_MC_FAST_ABILITY     = 0x10,
    MIDWARE_ENTRY_MC_FAST_STATE       = 0x20,
    MIDWARE_ENTRY_END_MC_CFG          = 0x40
    
}MIDWARE_MC_CFG_ENTRY_E;

/*Middleware multicast port configuration table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_PORT_ID_MC_PORT_CFG = 0x1,
    MIDWARE_ENTRY_MC_MAX_GROUP        = 0x2,
    MIDWARE_ENTRY_MC_US_IGMP_RATE     = 0x4,  
    MIDWARE_ENTRY_MC_US_TAG_OPER      = 0x8,  
    MIDWARE_ENTRY_MC_DS_TAG_OPER      = 0x10,
    MIDWARE_ENTRY_MC_US_TAG_DEF_TCI   = 0x20,
    MIDWARE_ENTRY_MC_DS_TAG_DEF_TCI   = 0x40,
    MIDWARE_ENTRY_MC_ROBUST           = 0x80,    
    MIDWARE_ENTRY_MC_QUERY_IP_ADDR    = 0x100,      
    MIDWARE_ENTRY_MC_QUERY_INTERVAL   = 0x200,      
    MIDWARE_ENTRY_MC_MAX_RESP_TIME    = 0x400,   
    MIDWARE_ENTRY_MC_LASR_QUERY_TIME  = 0x800,    
    MIDWARE_ENTRY_MC_UNAUTH_JOIN_MODE = 0x1000,      
    MIDWARE_ENTRY_END_MC_PORT         = 0x2000
    
}MIDWARE_MC_PORT_CFG_ENTRY_E;

/*Middleware multicast port status table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_PORT_ID_MC_PORT_STATUS = 0x1,
    MIDWARE_ENTRY_MC_CURRENT_BW          = 0x2,
    MIDWARE_ENTRY_MC_JOIN_MSG_COUNTET    = 0x4,
    MIDWARE_ENTRY_MC_EXCEED_BW_COUNT     = 0x8,      
    MIDWARE_ENTRY_END_MC_PORT_STATUS     = 0x10
    
}MIDWARE_MC_PORT_STATUS_ENTRY_E;

/*Middleware multicast port active group table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_PORT_ID_MC_ACTIVE_GROUP = 0x1,
    MIDWARE_ENTRY_INDEX_MC_ACTIVE_GROUP   = 0x2,    
    MIDWARE_ENTRY_MC_AG_VID               = 0x4,
    MIDWARE_ENTRY_MC_AG_SIP_ADDR          = 0x8,  
    MIDWARE_ENTRY_MC_AG_DIP_ADDR          = 0x10,  
    MIDWARE_ENTRY_MC_AG_ACTUAL_BW         = 0x20,
    MIDWARE_ENTRY_MC_AG_CLIENT_IP_ADDR    = 0x40,
    MIDWARE_ENTRY_MC_AG_JOIN_TIME         = 0x80,
    MIDWARE_ENTRY_MC_AG_RESERVED          = 0x100,      
    MIDWARE_ENTRY_END_MC_ACTIVE_GROUP     = 0x200

}MIDWARE_MC_ACTIVE_GROUP_ENTRY_E;

/*Middleware multicast control table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_PORT_ID_MC_PORT_CTRL       = 0x1,
    MIDWARE_ENTRY_MC_PORT_CTRL_TABLE_TYPE    = 0x2,
    MIDWARE_ENTRY_MC_PORT_CTRL_SET_TYPE      = 0x4,  
    MIDWARE_ENTRY_MC_PORT_CTRL_ROW_KEY       = 0x8,  
    MIDWARE_ENTRY_MC_PORT_CTRL_GEM_PORT      = 0x10,  
    MIDWARE_ENTRY_MC_PORT_CTRL_VID           = 0x20,  
    MIDWARE_ENTRY_MC_PORT_CTRL_SRC_IP        = 0x40,
    MIDWARE_ENTRY_MC_PORT_CTRL_DST_IP_START  = 0x80,
    MIDWARE_ENTRY_MC_PORT_CTRL_DST_IP_END    = 0x100,   
    MIDWARE_ENTRY_MC_PORT_CTRL_GROUP_BW      = 0x200,  
    MIDWARE_ENTRY_MC_PORT_CTRL_SRC_IPV6      = 0x400, 
    MIDWARE_ENTRY_MC_PORT_CTRL_DST_IPV6      = 0x800,        
    MIDWARE_ENTRY_MC_PORT_CTRL_PREVIEW_LEN   = 0x1000,
    MIDWARE_ENTRY_MC_PORT_CTRL_PREVIEW_TIME  = 0x2000,
    MIDWARE_ENTRY_MC_PORT_CTRL_PREVIEW_COUNT = 0x4000,    
    MIDWARE_ENTRY_MC_PORT_CTRL_PREVIEW_RESET = 0x8000,   
    MIDWARE_ENTRY_MC_PORT_CTRL_VENDOR_SPEC   = 0x10000,        
    MIDWARE_ENTRY_END_MC_PORT_CTRL           = 0x20000

}MIDWARE_MC_PORT_CTRL_ENTRY_E;

/*Middleware multicast control service packet table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_PORT_ID_MC_PORT_SRV       = 0x1,
    MIDWARE_ENTRY_MC_PORT_SRV_SET_TYPE      = 0x2,  
    MIDWARE_ENTRY_MC_PORT_SRV_ROW_KEY       = 0x4,  
    MIDWARE_ENTRY_MC_PORT_SRV_VID           = 0x8,  
    MIDWARE_ENTRY_MC_PORT_SRV_MAX_GROUP     = 0x10,  
    MIDWARE_ENTRY_MC_PORT_SRV_MAX_BW        = 0x20,          
    MIDWARE_ENTRY_END_MC_PORT_SRV           = 0x40
    
}MIDWARE_MC_PORT_SRV_E;

/*Middleware multicast control preview table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_PORT_ID_MC_PORT_PREV      = 0x1,
    MIDWARE_ENTRY_MC_PORT_PREV_SET_TYPE     = 0x2,  
    MIDWARE_ENTRY_MC_PORT_PREV_ROW_KEY      = 0x4,  
    MIDWARE_ENTRY_MC_PORT_PREV_SRC_IP       = 0x8,    
    MIDWARE_ENTRY_MC_PORT_PREV_DST_IP       = 0x10,
    MIDWARE_ENTRY_MC_PORT_PREV_ANI_VID      = 0x20,
    MIDWARE_ENTRY_MC_PORT_PREV_UNI_VID      = 0x40,  
    MIDWARE_ENTRY_MC_PORT_PREV_DURATION     = 0x80,
    MIDWARE_ENTRY_MC_PORT_PREV_TIME_LEFT    = 0x100,         
    MIDWARE_ENTRY_END_MC_PORT_PREV          = 0x200
    
}MIDWARE_MC_PORT_PREV_E;

/*Middleware multicast downstream VLAN table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_PORT_ID_MC_DS_VLAN     = 0x1,
    MIDWARE_ENTRY_MC_IN_MC_DS_VLAN       = 0x2,
    MIDWARE_ENTRY_MC_OUT_MC_DS_VLAN      = 0x4,  
    MIDWARE_ENTRY_END_MC_DS_VLAN         = 0x8
    
}MIDWARE_MC_DS_VLAN_ENTRY_E;

/*Middleware multicast upstream VLAN table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_PORT_ID_MC_US_VLAN     = 0x1,
    MIDWARE_ENTRY_MC_IN_MC_US_VLAN       = 0x2,
    MIDWARE_ENTRY_MC_OUT_MC_US_VLAN      = 0x4,  
    MIDWARE_ENTRY_END_MC_US_VLAN         = 0x8
    
}MIDWARE_MC_US_VLAN_ENTRY_E;
    
/*Middleware WAN QoS table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LLID                = 0x1,
    MIDWARE_ENTRY_WAN_QUEUE_ID        = 0x2,
    MIDWARE_ENTRY_WANC_SCHEDULE_MODE  = 0x4,
    MIDWARE_ENTRY_WAN_QOS_WEIGHT      = 0x8,
    MIDWARE_ENTRY_WAN_QOS_CIR         = 0x10,
    MIDWARE_ENTRY_WAN_QOS_PIR         = 0x20,    
    MIDWARE_ENTRY_END_WAN_QOS         = 0x40
    
}MIDWARE_WAN_QOS_ENTRY_E;

/*Middleware EPON table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_INDEX_EPON          = 0x1,
    MIDWARE_ENTRY_EPON_MAC            = 0x2,
    MIDWARE_ENTRY_EPON_MULTI_LLID     = 0x4,  
    MIDWARE_ENTRY_EPON_FEC            = 0x8,
    MIDWARE_ENTRY_HOLDOVER_CFG        = 0x10,
    MIDWARE_ENTRY_HOLDOVER_TIME       = 0x20,    
    MIDWARE_ENTRY_ONU_REGISTER_STATUS = 0x40,
    MIDWARE_ENTRY_ONU_LLID_NUM        = 0x80,    
    MIDWARE_ENTRY_END_EPON            = 0x100  
}MIDWARE_EPON_ENTRY_E;

/*Middleware DBA table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_DBA_LLID_NUM        = 0x1,
    MIDWARE_ENTRY_QUEUESET_NUM        = 0x2,
    MIDWARE_ENTRY_QUEUE_NUM           = 0x4,
    MIDWARE_ENTRY_AGGRE_THRESHOLD     = 0x8,
    MIDWARE_ENTRY_DBA_STATE           = 0x10,
    MIDWARE_ENTRY_DBA_THRESHOLD       = 0x20,    
    MIDWARE_ENTRY_END_DBA             = 0x40
    
}MIDWARE_DBA_ENTRY;

/*Middleware opt transceiver table bitmap*/
typedef enum
{  
    MIDWARE_ENTRY_INDEX_OPT_TRANSCEIVER = 0x1,
    MIDWARE_ENTRY_TEMPERATURE           = 0x2,
    MIDWARE_ENTRY_SUPPLY_VOLTAGE        = 0x4,
    MIDWARE_ENTRY_BIAS_CURRENT          = 0x8,
    MIDWARE_ENTRY_TX_POWER              = 0x10,
    MIDWARE_ENTRY_RX_POWER              = 0x20, 
    MIDWARE_ENTRY_END_OPT_TRANSCEIVER   = 0x40  
    
} MIDWARE_OPT_TRANSCEIVER_ENTRY_E;

/*Middleware software upgrade image table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_IMAGE_ID            = 0x1,
    MIDWARE_ENTRY_IMAGE_ACTIVE        = 0x2,
    MIDWARE_ENTRY_IMAGE_COMMIT        = 0x4,
    MIDWARE_ENTRY_IMAGE_VALID         = 0x8,
    MIDWARE_ENTRY_IMAGE_VERSION       = 0x10,
    MIDWARE_ENTRY_IMAGE_TO_FLASH      = 0x20,
    MIDWARE_ENTRY_IMAGE_ABORT         = 0x40,
    MIDWARE_ENTRY_IMAGE_FILENAME      = 0x80,
    MIDWARE_ENTRY_UPGRADE_STATUS      = 0x100,
    MIDWARE_ENTRY_END_IAMGE           = 0x200  
    
}MIDWARE_IMAGE_ENTRY_E;

/*Middleware alarm table bitmap*/
typedef enum
{  
    MIDWARE_ENTRY_ALARM_TYPE            = 0x1,
    MIDWARE_ENTRY_ALARM_PARAM1          = 0x2,
    MIDWARE_ENTRY_ALARM_PARAM2          = 0x4,
    MIDWARE_ENTRY_ALARM_CLASS           = 0x8,
    MIDWARE_ENTRY_ALARM_ADMIN           = 0x10,
    MIDWARE_ENTRY_ALARM_STATE           = 0x20,     
    MIDWARE_ENTRY_ALARM_DUMMY1          = 0x40,
    MIDWARE_ENTRY_ALARM_THRESHOLD       = 0x80,
    MIDWARE_ENTRY_ALARM_CLEAR_THRESHOLD = 0x100,
    MIDWARE_ENTRY_ALARM_INFO            = 0x200,
    MIDWARE_ENTRY_END_ALARM             = 0x400      
} MIDWARE_ALARM_ENTRY_E;

/*Middleware pm table bitmap*/
typedef enum
{  
    MIDWARE_ENTRY_PM_TYPE                       = 0x1,
    MIDWARE_ENTRY_PM_PARAM1                     = 0x2,
    MIDWARE_ENTRY_PM_PARAM2                     = 0x4,    
    MIDWARE_ENTRY_PM_ADMIN_BITS                 = 0x8,
    MIDWARE_ENTRY_PM_ACCUMULATION_MODE          = 0x10,
    MIDWARE_ENTRY_PM_GLOBAL_CLEAR               = 0x20,
    MIDWARE_ENTRY_PM_INTERVAL_PERIOD            = 0x40,    
    MIDWARE_ENTRY_PM_COUNTER0                   = 0x80,
    MIDWARE_ENTRY_PM_COUNTER1                   = 0x100,
    MIDWARE_ENTRY_PM_COUNTER2                   = 0x200,
    MIDWARE_ENTRY_PM_COUNTER3                   = 0x400,
    MIDWARE_ENTRY_PM_COUNTER4                   = 0x800,
    MIDWARE_ENTRY_PM_COUNTER5                   = 0x1000,
    MIDWARE_ENTRY_PM_COUNTER6                   = 0x2000,
    MIDWARE_ENTRY_PM_COUNTER7                   = 0x4000,
    MIDWARE_ENTRY_PM_COUNTER8                   = 0x8000,
    MIDWARE_ENTRY_PM_COUNTER9                   = 0x10000,
    MIDWARE_ENTRY_PM_COUNTER10                  = 0x20000,
    MIDWARE_ENTRY_PM_COUNTER11                  = 0x40000,
    MIDWARE_ENTRY_PM_COUNTER12                  = 0x80000,
    MIDWARE_ENTRY_PM_COUNTER13                  = 0x100000,
    MIDWARE_ENTRY_PM_COUNTER14                  = 0x200000,
    MIDWARE_ENTRY_PM_COUNTER15                  = 0x400000,
    MIDWARE_ENTRY_END_PM                        = 0x800000
} MIDWARE_PM_ENTRY_E;

#define MIDWARE_ENTRY_PM_COUNTERS (MIDWARE_ENTRY_PM_COUNTER0|MIDWARE_ENTRY_PM_COUNTER1|MIDWARE_ENTRY_PM_COUNTER2\
    |MIDWARE_ENTRY_PM_COUNTER3|MIDWARE_ENTRY_PM_COUNTER4|MIDWARE_ENTRY_PM_COUNTER5|MIDWARE_ENTRY_PM_COUNTER6\
    |MIDWARE_ENTRY_PM_COUNTER7|MIDWARE_ENTRY_PM_COUNTER8|MIDWARE_ENTRY_PM_COUNTER9|MIDWARE_ENTRY_PM_COUNTER10\
    |MIDWARE_ENTRY_PM_COUNTER11|MIDWARE_ENTRY_PM_COUNTER12|MIDWARE_ENTRY_PM_COUNTER13|MIDWARE_ENTRY_PM_COUNTER14|MIDWARE_ENTRY_PM_COUNTER15)

typedef enum
{  
    MIDWARE_ENTRY_AVC_TYPE            = 0x1,
    MIDWARE_ENTRY_AVC_PARAM1          = 0x2,
    MIDWARE_ENTRY_AVC_PARAM2          = 0x4,
    MIDWARE_ENTRY_AVC_ADMIN           = 0x8,
    MIDWARE_ENTRY_AVC_DUMMY1          = 0x10,
    MIDWARE_ENTRY_AVC_DUMMY2          = 0x20,     
    MIDWARE_ENTRY_AVC_VALUE           = 0x40,
    MIDWARE_ENTRY_END_AVC             = 0x80,
} MIDWARE_AVC_ENTRY_E;

/*Middleware IAD port administration bitmap*/
typedef enum 

{  
    MIDWARE_ENTRY_POTS_PORT_ID        = 0x1,
    MIDWARE_ENTRY_POTS_PORT_ADMIN     = 0x2,
    MIDWARE_ENTRY_END_POTS_ADMIN      = 0x4
    
}MIDWARE_POTS_ADMIN_E;

/*Middleware IAD information bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_INDEX_IAD_INFO      = 0x1,
    MIDWARE_ENTRY_IAD_MAC_ADD         = 0x2,
    MIDWARE_ENTRY_IAD_PROTOCOL        = 0x4,
    MIDWARE_ENTRY_IAD_SW_VER          = 0x8,
    MIDWARE_ENTRY_IAD_SW_TIME         = 0x10,
    MIDWARE_ENTRY_IAD_POTS_NUM        = 0x20,
    MIDWARE_ENTRY_END_IAD_INFO        = 0x40
    
}MIDWARE_IAD_INFO_E;

/*Middleware IAD configuration bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_INDEX_IAD_CFG       = 0x1,
    MIDWARE_ENTRY_IAD_IP_MODE         = 0x2,
    MIDWARE_ENTRY_IAD_IP_ADD          = 0x4,
    MIDWARE_ENTRY_IAD_IP_MASK         = 0x8,
    MIDWARE_ENTRY_IAD_DEF_GW          = 0x10,
    MIDWARE_ENTRY_IAD_PPPOE_MODE      = 0x20,
    MIDWARE_ENTRY_IAD_PPPOE_USER      = 0x40,
    MIDWARE_ENTRY_IAD_PPPOE_PWD       = 0x80,
    MIDWARE_ENTRY_IAD_TAG_FLAG        = 0x100,
    MIDWARE_ENTRY_IAD_CVLAN           = 0x200, 
    MIDWARE_ENTRY_IAD_SVLAN           = 0x400,
    MIDWARE_ENTRY_IAD_PBITS           = 0x800,    
    MIDWARE_ENTRY_END_IAD_CFG         = 0x1000
    
}MIDWARE_IAD_CFG_E;

/*Middleware H.248 parameter bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_INDEX_H248_CFG      = 0x1,
    MIDWARE_ENTRY_H248_MG_PORT        = 0x2,
    MIDWARE_ENTRY_H248_MGC_IP         = 0x4,
    MIDWARE_ENTRY_H248_MGC_PORT       = 0x8,
    MIDWARE_ENTRY_H248_MGC_IP_BACK    = 0x10,
    MIDWARE_ENTRY_H248_MGC_PORT_BACK  = 0x20,
    MIDWARE_ENTRY_H248_ACTIVE_MGC     = 0x40,
    MIDWARE_ENTRY_H248_REG_MODE       = 0x80,
    MIDWARE_ENTRY_H248_MG_ID          = 0x100,
    MIDWARE_ENTRY_H248_HB_MODE        = 0x200, 
    MIDWARE_ENTRY_H248_HB_CYCLE       = 0x400,
    MIDWARE_ENTRY_H248_HB_COUNT       = 0x800,    
    MIDWARE_ENTRY_END_H248_CFG        = 0x1000
    
}MIDWARE_H248_CFG_E;

/*Middleware H.248 user TID information bitmap*/
typedef enum 

{  
    MIDWARE_ENTRY_H248_USER_PORT_ID   = 0x1,
    MIDWARE_ENTRY_H248_USER_NAME      = 0x2,  
    MIDWARE_ENTRY_END_H248_USER_INFO  = 0x4
    
}MIDWARE_H248_USER_INFO_E;

/*Middleware H.248 RTP TID configuration bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_INDEX_H248_RTP_CFG  = 0x1,
    MIDWARE_ENTRY_H248_RTP_NUM        = 0x2,
    MIDWARE_ENTRY_H248_RTP_PREFIX     = 0x4,
    MIDWARE_ENTRY_H248_RTP_DIGIT      = 0x8,
    MIDWARE_ENTRY_H248_RTP_MODE       = 0x10,
    MIDWARE_ENTRY_H248_RTP_DIGIT_LEN  = 0x20,    
    MIDWARE_ENTRY_END_H248_RTP_CFG    = 0x40
    
}MIDWARE_H248_RTP_CFG_E;

/*Middleware H.248 RTP TID information bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_INDEX_H248_RTP_INFO = 0x1,
    MIDWARE_ENTRY_H248_RTP_TID_NUM    = 0x2,
    MIDWARE_ENTRY_H248_FIRST_RTP_NAME = 0x4, 
    MIDWARE_ENTRY_END_H248_RTP_INFO   = 0x8
    
}MIDWARE_H248_RTP_INFO_E;

/*Middleware SIP parameter bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_INDEX_SIP_CFG      = 0x1,
    MIDWARE_ENTRY_SIP_MG_PORT        = 0x2,
    MIDWARE_ENTRY_SIP_PROXY_IP       = 0x4,
    MIDWARE_ENTRY_SIP_PROXY_PORT     = 0x8,
    MIDWARE_ENTRY_SIP_PROXY_IP_BACK  = 0x10,
    MIDWARE_ENTRY_SIP_PROXY_PORT_BACK= 0x20,
    MIDWARE_ENTRY_SIP_ACTIVE_PROXY   = 0x40,
    MIDWARE_ENTRY_SIP_REG_IP         = 0x80,
    MIDWARE_ENTRY_SIP_REG_PORT       = 0x100,
    MIDWARE_ENTRY_SIP_REG_IP_BACK    = 0x200, 
    MIDWARE_ENTRY_SIP_REG_PORT_BACK  = 0x400,
    MIDWARE_ENTRY_SIP_OUTBAND_IP     = 0x800,
    MIDWARE_ENTRY_SIP_OUTBAND_PORT   = 0x1000,
    MIDWARE_ENTRY_SIP_REG_INTERVAL   = 0x2000,
    MIDWARE_ENTRY_SIP_HB_SWITCH      = 0x4000,
    MIDWARE_ENTRY_SIP_HB_CYCLE       = 0x8000,
    MIDWARE_ENTRY_SIP_HB_COUNT       = 0x10000,
    MIDWARE_ENTRY_END_SIP_CFG        = 0x20000
    
}MIDWARE_SIP_CFG_E;

/*Middleware SIP user bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_SIP_USER_PORT_ID   = 0x1,
    MIDWARE_ENTRY_SIP_USER_ACCOUNT   = 0x2,
    MIDWARE_ENTRY_SIP_USER_NAME      = 0x4,
    MIDWARE_ENTRY_SIP_USER_PWD       = 0x8, 
    MIDWARE_ENTRY_END_SIP_USER       = 0x10
    
} MIDWARE_SIP_USER_E;

/*Middleware FAX configuration bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_INDEX_FAX          = 0x1,
    MIDWARE_ENTRY_FAX_T38_EN         = 0x2,
    MIDWARE_ENTRY_FAX_CONTROL        = 0x4,
    MIDWARE_ENTRY_END_FAX            = 0x8
    
}MIDWARE_FAX_E;

/*Middleware POTS status bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_IAD_PORT_ID        = 0x1,
    MIDWARE_ENTRY_IAD_PORT_STATUS    = 0x2,
    MIDWARE_ENTRY_IAD_SERVICE_STATUS = 0x4,
    MIDWARE_ENTRY_IAD_CODEC_MODE     = 0x8, 
    MIDWARE_ENTRY_END_POTS_STATUS    = 0x10
    
}MIDWARE_POTS_STATUS_E;

/*Middleware RSTP table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_INDEX_RSTP         = 0x1,
    MIDWARE_ENTRY_RSTP_STATE         = 0x2,
    MIDWARE_ENTRY_RSTP_BRIDGE_PRI    = 0x4,
    MIDWARE_ENTRY_RSTP_FWD_DELAY     = 0x8,
    MIDWARE_ENTRY_RSTP_HELLO_TIME    = 0x10, 
    MIDWARE_ENTRY_RSTP_AGE_TIME      = 0x20,     
    MIDWARE_ENTRY_END_RSTP           = 0x40
    
}MIDWARE_RSTP_E;

/*Middleware RSTP port table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_PORT_ID_RSTP_PORT  = 0x1,
    MIDWARE_ENTRY_RSTP_PORT_STATE    = 0x2,
    MIDWARE_ENTRY_RSTP_PORT_PRI      = 0x4,  
    MIDWARE_ENTRY_END_RSTP_PORT      = 0x8
    
}MIDWARE_RSTP_PORT_E;

/*Middleware common enum for UNI configuration state*/
typedef enum 
{  
    MIDWARE_UNI_CFG_DISABLE          = 0,
    MIDWARE_UNI_CFG_ENABLE           = 1
    
}MIDWARE_UNI_CFG_E;

/*Middleware VLAN port table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_PORT_ID_VLAN_PORT  = 0x1,
    MIDWARE_ENTRY_VLAN_MODE          = 0x2,
    MIDWARE_ENTRY_VLAN_NUM           = 0x4,
    MIDWARE_ENTRY_DEFAULT_VLAN       = 0x8,
    MIDWARE_ENTRY_VLAN_LIST          = 0x10, 
    MIDWARE_ENTRY_AGG_VLAN_NUM       = 0x20, 
    MIDWARE_ENTRY_AGG_TGT_VLAN       = 0x40,
    MIDWARE_ENTRY_AGG_VLAN_LIST      = 0x80,    
    MIDWARE_ENTRY_END_VLAN_PORT      = 0x80
    
}MIDWARE_VLAN_PORT_E;

/*Middleware OMCI VoIP global config table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_INDEX_V_VOIP_CFG   = 0x1,
    MIDWARE_ENTRY_V_SUPPORT_PROTO    = 0x2,
    MIDWARE_ENTRY_V_USED_PROTO       = 0x4,
    MIDWARE_ENTRY_V_SUPPORT_METHOD   = 0x8,
    MIDWARE_ENTRY_V_USED_METHOD      = 0x10, 
    MIDWARE_ENTRY_V_CONFIG_STATE     = 0x20, 
    MIDWARE_ENTRY_V_RTRL_PROFILE     = 0x40,
    MIDWARE_ENTRY_V_PROFILE_VER      = 0x80,    
    MIDWARE_ENTRY_V_MAC_ADDR         = 0x100,
    MIDWARE_ENTRY_V_SW_VER           = 0x200,
    MIDWARE_ENTRY_V_SW_TIME          = 0x400,
    MIDWARE_ENTRY_V_POTS_NUM         = 0x800, 
    MIDWARE_ENTRY_V_IP_MODE          = 0x1000, 
    MIDWARE_ENTRY_V_IP_ADDR          = 0x2000,
    MIDWARE_ENTRY_V_NET_MASK         = 0x4000,   
    MIDWARE_ENTRY_V_DEFAULT_GW       = 0x8000,
    MIDWARE_ENTRY_V_PPPOE_MODE       = 0x10000,
    MIDWARE_ENTRY_V_PPPOE_USER       = 0x20000, 
    MIDWARE_ENTRY_V_PPPOE_PWD        = 0x40000, 
    MIDWARE_ENTRY_V_TAG_FLAG         = 0x80000,
    MIDWARE_ENTRY_V_VOICE_CVID       = 0x100000,   
    MIDWARE_ENTRY_V_VOICE_SVID       = 0x200000, 
    MIDWARE_ENTRY_V_VOICE_PBIT       = 0x400000, 
    MIDWARE_ENTRY_V_IAD_OPER         = 0x800000,     
    MIDWARE_ENTRY_END_G_VOIP_CFG     = 0x1000000
    
}MIDWARE_V_VOIP_CFG_E;

/*Middleware OMCI VoIP port config table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_PORT_CFG   = 0x1,
    MIDWARE_ENTRY_V_ADMIN_STATE      = 0x2,
    MIDWARE_ENTRY_V_IMPEDANCE        = 0x4,
    MIDWARE_ENTRY_V_TX_PATH          = 0x8,
    MIDWARE_ENTRY_V_RX_GAIN          = 0x10, 
    MIDWARE_ENTRY_V_TX_GAIN          = 0x20, 
    MIDWARE_ENTRY_V_OPER_STATE       = 0x40,
    MIDWARE_ENTRY_V_HOOK_STATE       = 0x80,    
    MIDWARE_ENTRY_V_HOLDOVER_TIME    = 0x100,   
    MIDWARE_ENTRY_V_SIGNAL_CODE      = 0x200,      
    MIDWARE_ENTRY_END_V_PORT_CFG     = 0x400
    
}MIDWARE_V_PORT_CFG_E;

/*Middleware OMCI SIP agent table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_SIP_AGENT     = 0x1,
    MIDWARE_ENTRY_V_AGENT_ID            = 0x2,
    MIDWARE_ENTRY_V_PROXY_SERV_ADDR     = 0x4,
    MIDWARE_ENTRY_V_BACKUP_SERV_ADDR    = 0x8,
    MIDWARE_ENTRY_V_OUTBOUND_PROXY_ADDR = 0x10, 
    MIDWARE_ENTRY_V_HOST_PART_URL       = 0x20, 
    MIDWARE_ENTRY_V_REG_SCHEME          = 0x40,
    MIDWARE_ENTRY_V_REG_USER            = 0x80,    
    MIDWARE_ENTRY_V_REG_PWD             = 0x100,
    MIDWARE_ENTRY_V_REG_REALM           = 0x200,
    MIDWARE_ENTRY_V_REG_ADDR            = 0x400,
    MIDWARE_ENTRY_V_BACKUP_REG_ADDR     = 0x800, 
    MIDWARE_ENTRY_V_PRIMARY_DNS         = 0x1000, 
    MIDWARE_ENTRY_V_SECOND_DNS          = 0x2000,
    MIDWARE_ENTRY_V_L4_PROTO_TYPE       = 0x4000,   
    MIDWARE_ENTRY_V_L4_PORT             = 0x8000,
    MIDWARE_ENTRY_V_DSCP_VAL            = 0x10000,
    MIDWARE_ENTRY_V_REG_EXP_TIME        = 0x20000, 
    MIDWARE_ENTRY_V_REG_HEAD_TIME       = 0x40000, 
    MIDWARE_ENTRY_V_RESPONSE_CODE       = 0x80000,
    MIDWARE_ENTRY_V_RESPONSE_TONE       = 0x100000,   
    MIDWARE_ENTRY_V_OPTION_TC           = 0x200000, 
    MIDWARE_ENTRY_V_URL_FORMAT          = 0x400000, 
    MIDWARE_ENTRY_V_SIP_STATUS          = 0x800000,   
    MIDWARE_ENTRY_V_HEARTBEAT_SWITCH    = 0x100000,   
    MIDWARE_ENTRY_V_HEARTBEAT_CYCLE     = 0x200000, 
    MIDWARE_ENTRY_V_HEARTBEAT_COUNT     = 0x400000, 
    MIDWARE_ENTRY_END_V_SIP_AGENT       = 0x800000
    
}MIDWARE_V_SIP_AGENT_E;

/*Middleware OMCI SIP user table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_SIP_USER      = 0x1,
    MIDWARE_ENTRY_V_USER_AOR            = 0x2,
    MIDWARE_ENTRY_V_SIP_DISPLAY_NAME    = 0x4,
    MIDWARE_ENTRY_V_AUTH_SCHEME         = 0x8,
    MIDWARE_ENTRY_V_AUTH_USER           = 0x10, 
    MIDWARE_ENTRY_V_AUTH_PWD            = 0x20, 
    MIDWARE_ENTRY_V_AUTH_REALM          = 0x40,
    MIDWARE_ENTRY_V_AUTH_SERV_ADDR      = 0x80,    
    MIDWARE_ENTRY_V_VOICEMAIL_SERV_ADDR = 0x100,
    MIDWARE_ENTRY_V_VOICEMAIL_EXP_TIME  = 0x200,
    MIDWARE_ENTRY_V_RELEASE_TIMER       = 0x400,
    MIDWARE_ENTRY_V_ROH_TIMER           = 0x800,  
    MIDWARE_ENTRY_END_V_SIP_USER        = 0x1000
    
}MIDWARE_V_SIP_USER_E;

/*Middleware OMCI VoIP application service profile table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_APP_SERV_PROFILE = 0x1,
    MIDWARE_ENTRY_V_CID_FEATURE            = 0x2,
    MIDWARE_ENTRY_V_CALLWAIT_FEATURE       = 0x4,
    MIDWARE_ENTRY_V_CALLPROGRESS_FEATURE   = 0x8,
    MIDWARE_ENTRY_V_CALLPRESENT_FEATURE    = 0x10, 
    MIDWARE_ENTRY_V_DIRECTCONNECT_FEATURE  = 0x20, 
    MIDWARE_ENTRY_V_DIRECTCONNECT_SCHEME   = 0x40,
    MIDWARE_ENTRY_V_DIRECTCONNECT_USER     = 0x80,    
    MIDWARE_ENTRY_V_DIRECTCONNECT_PWD      = 0x100,
    MIDWARE_ENTRY_V_DIRECTCONNECT_REALM    = 0x200,
    MIDWARE_ENTRY_V_DIRECTCONNECT_ADDR     = 0x400,
    MIDWARE_ENTRY_V_BRIDGE_LINE_SCHEME     = 0x800, 
    MIDWARE_ENTRY_V_BRIDGE_LINE_USER       = 0x1000, 
    MIDWARE_ENTRY_V_BRIDGE_LINE_PWD        = 0x2000,
    MIDWARE_ENTRY_V_BRIDGE_LINE_REALM      = 0x4000,   
    MIDWARE_ENTRY_V_BRIDGE_LINE_ADDR       = 0x8000,
    MIDWARE_ENTRY_V_CONFER_URL_SCHEME      = 0x10000,
    MIDWARE_ENTRY_V_CONFER_URL_USER        = 0x20000, 
    MIDWARE_ENTRY_V_CONFER_URL_PWD         = 0x40000, 
    MIDWARE_ENTRY_V_CONFER_URL_REALM       = 0x80000,
    MIDWARE_ENTRY_V_CONFER_URL_ADDR        = 0x100000,   
    MIDWARE_ENTRY_END_V_APP_SERV_PROFILE   = 0x200000
    
}MIDWARE_V_APP_SERV_PROFILE_E;

/*Middleware OMCI VoIP feature access node table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_FEATURE_AN     = 0x1,
    MIDWARE_ENTRY_V_CALCEL_CALLWAIT      = 0x2,
    MIDWARE_ENTRY_V_CALL_HOLD            = 0x4,
    MIDWARE_ENTRY_V_CALL_WAIT            = 0x8,
    MIDWARE_ENTRY_V_CIDS_ACTIVE          = 0x10, 
    MIDWARE_ENTRY_V_CIDS_DEACTIVE        = 0x20,
    MIDWARE_ENTRY_V_DND_ACTIVE           = 0x40,    
    MIDWARE_ENTRY_V_DND_DEACTIVE         = 0x80,
    MIDWARE_ENTRY_V_DND_PIN_CHANGE       = 0x100,
    MIDWARE_ENTRY_V_EMERG_SN             = 0x200,
    MIDWARE_ENTRY_V_INTERCOM_SERV        = 0x400, 
    MIDWARE_ENTRY_V_BLIND_CALL_TRANS     = 0x800, 
    MIDWARE_ENTRY_V_ATTEND_CALL_TRANS    = 0x1000, 
    MIDWARE_ENTRY_END_V_FEATURE_AN       = 0x2000
     
}MIDWARE_V_FEATURE_AN_E;

/*Middleware OMCI VoIP network dial plan table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_DIAN_PLAN       = 0x1,
    MIDWARE_ENTRY_V_DIAL_PLAN_NUM         = 0x2,
    MIDWARE_ENTRY_V_DIAL_PLAN_TABLE_SIZE  = 0x4,
    MIDWARE_ENTRY_V_CRITICAL_DIAL_TIMEOUT = 0x8,
    MIDWARE_ENTRY_V_PART_DIAL_TIMEOUT     = 0x10, 
    MIDWARE_ENTRY_V_DIAL_PLAN_FORMAT      = 0x20, 
    MIDWARE_ENTRY_V_DIGIT_MAP             = 0x40,
    MIDWARE_ENTRY_END_V_DIAN_PLAN         = 0x80
     
}MIDWARE_V_DIAN_PLAN_E;

/*Middleware OMCI VoIP media profile table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_MEDIA_PROFILE   = 0x1,
    MIDWARE_ENTRY_V_FAX_MODE              = 0x2,
    MIDWARE_ENTRY_V_FAX_CONTROL           = 0x4,
    MIDWARE_ENTRY_V_CODE_SELECT_1         = 0x8,
    MIDWARE_ENTRY_V_PACKET_PERIOD_1       = 0x10, 
    MIDWARE_ENTRY_V_SILENCE_SUPRESS_1     = 0x20, 
    MIDWARE_ENTRY_V_CODE_SELECT_2         = 0x40,
    MIDWARE_ENTRY_V_PACKET_PERIOD_2       = 0x80,    
    MIDWARE_ENTRY_V_SILENCE_SUPRESS_2     = 0x100,
    MIDWARE_ENTRY_V_CODE_SELECT_3         = 0x200,
    MIDWARE_ENTRY_V_PACKET_PERIOD_3       = 0x400,
    MIDWARE_ENTRY_V_SILENCE_SUPRESS_3     = 0x800, 
    MIDWARE_ENTRY_V_CODE_SELECT_4         = 0x1000,
    MIDWARE_ENTRY_V_PACKET_PERIOD_4       = 0x2000,
    MIDWARE_ENTRY_V_SILENCE_SUPRESS_4     = 0x4000,     
    MIDWARE_ENTRY_V_OOB_DTMF              = 0x8000, 
    MIDWARE_ENTRY_END_V_MEDIA_PROFILE     = 0x10000
     
}MIDWARE_V_MEDIA_PROFILE_E;

/*Middleware OMCI VoIP service profile table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_SERV_PROFILE   = 0x1,
    MIDWARE_ENTRY_V_ANNOUNCE_TYPE        = 0x2,
    MIDWARE_ENTRY_V_JITTER_TAEGET        = 0x4,
    MIDWARE_ENTRY_V_JITTER_BUF_MAX       = 0x8, 
    MIDWARE_ENTRY_V_ECHO_CANCEL_IND      = 0x10, 
    MIDWARE_ENTRY_V_PSTN_PROTO_VAR       = 0x20,
    MIDWARE_ENTRY_V_DTMF_LEVEL           = 0x40,    
    MIDWARE_ENTRY_V_DTMF_DUARATION       = 0x80,
    MIDWARE_ENTRY_V_HOOKFLASH_MIN_TIME   = 0x100,
    MIDWARE_ENTRY_V_HOOKFLASH_MAX_TIME   = 0x200,
    MIDWARE_ENTRY_END_V_SERV_PROFILE     = 0x400
     
}MIDWARE_V_SERV_PROFILE_E;

/*Middleware OMCI VoIP tone pattern table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_TONE_PATTERN   = 0x1,
    MIDWARE_ENTRY_INDEX_TONE_PATTERN     = 0x2,
    MIDWARE_ENTRY_V_TONE_PATTERN_INDEX   = 0x4,
    MIDWARE_ENTRY_V_TONE_ON              = 0x8,
    MIDWARE_ENTRY_V_TONE_FREQUENCY_1     = 0x10, 
    MIDWARE_ENTRY_V_TONE_POWER_1         = 0x20, 
    MIDWARE_ENTRY_V_TONE_FREQUENCY_2     = 0x40,
    MIDWARE_ENTRY_V_TONE_POWER_2         = 0x80,    
    MIDWARE_ENTRY_V_TONE_FREQUENCY_3     = 0x100,
    MIDWARE_ENTRY_V_TONE_POWER_3         = 0x200,
    MIDWARE_ENTRY_V_TONE_FREQUENCY_4     = 0x400,
    MIDWARE_ENTRY_V_TONE_POWER_4         = 0x800, 
    MIDWARE_ENTRY_V_MODUL_FREQUENCY      = 0x1000,
    MIDWARE_ENTRY_V_MODUL_POWER          = 0x2000,
    MIDWARE_ENTRY_V_TONE_DUAR            = 0x4000,     
    MIDWARE_ENTRY_V_TONE_NEXT_ENTRY      = 0x8000, 
    MIDWARE_ENTRY_END_V_TONE_PATTERN     = 0x10000
     
}MIDWARE_V_TONE_PATTERN_E;

/*Middleware OMCI VoIP tone event table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_TONE_EVENT     = 0x1,
    MIDWARE_ENTRY_INDEX_TONE_EVENT       = 0x2,
    MIDWARE_ENTRY_V_EVENT_TONE           = 0x4,    
    MIDWARE_ENTRY_V_EVENT_TONE_PATTERN   = 0x8,
    MIDWARE_ENTRY_V_EVENT_TONE_FILE      = 0x10,
    MIDWARE_ENTRY_V_EVENT_TONE_FILE_REP  = 0x20, 
    MIDWARE_ENTRY_END_V_TONE_EVENT       = 0x40
     
}MIDWARE_V_TONE_EVENT_E;

/*Middleware OMCI VoIP ringing pattern table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_RING_PATTERN   = 0x1,
    MIDWARE_ENTRY_INDEX_RING_PATTERN     = 0x2,
    MIDWARE_ENTRY_V_RING_PATTERN_INDEX   = 0x4,
    MIDWARE_ENTRY_V_RING_ON              = 0x8,
    MIDWARE_ENTRY_V_RING_DUAR            = 0x10,  
    MIDWARE_ENTRY_V_RING_NEXT_ENTRY      = 0x20, 
    MIDWARE_ENTRY_END_V_RING_PATTERN     = 0x40
     
}MIDWARE_V_RING_PATTERN_E;

/*Middleware OMCI VoIP ring event table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_RING_EVENT     = 0x1,
    MIDWARE_ENTRY_INDEX_RING_EVENT       = 0x2,
    MIDWARE_ENTRY_V_EVENT_RING           = 0x4,    
    MIDWARE_ENTRY_V_EVENT_RING_PATTERN   = 0x8,
    MIDWARE_ENTRY_V_EVENT_RING_FILE      = 0x10,
    MIDWARE_ENTRY_V_EVENT_RING_FILE_REP  = 0x20, 
    MIDWARE_ENTRY_V_EVENT_RING_TEXT      = 0x40,     
    MIDWARE_ENTRY_END_V_RING_EVENT       = 0x80
     
}MIDWARE_V_RING_EVENT_E;

/*Middleware OMCI VoIP RTP table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_RTP_PROFILE   = 0x1,
    MIDWARE_ENTRY_V_RTP_PORT_MIN        = 0x2,
    MIDWARE_ENTRY_V_RTP_PORT_MAX        = 0x4,
    MIDWARE_ENTRY_V_RTP_DSCP_MARK       = 0x8, 
    MIDWARE_ENTRY_V_RTP_PIGGYBACK_EVENT = 0x10, 
    MIDWARE_ENTRY_V_RTP_TONE_EVENT      = 0x20,
    MIDWARE_ENTRY_V_RTP_DTMF_EVENT      = 0x40,    
    MIDWARE_ENTRY_V_RTP_CAS_EVENT       = 0x80,
    MIDWARE_ENTRY_END_V_RTP_PROFILE     = 0x100
     
}MIDWARE_V_RTP_PROFILE_E;

/*Middleware OMCI line status table bitmap*/
typedef enum 
{  
    MIDWARE_ENTRY_LINE_ID_LINE_STATUS    = 0x1,
    MIDWARE_ENTRY_V_PORT_STATUS          = 0x2,
    MIDWARE_ENTRY_V_CODEC_USED           = 0x4,    
    MIDWARE_ENTRY_V_SERVER_STATUS        = 0x8,
    MIDWARE_ENTRY_V_PORT_SESSION_TYPE    = 0x10,
    MIDWARE_ENTRY_V_CALL1_PACKET_PERIOD  = 0x20, 
    MIDWARE_ENTRY_V_CALL2_PACKET_PERIOD  = 0x40,    
    MIDWARE_ENTRY_V_CALL1_DST_ADDR       = 0x80, 
    MIDWARE_ENTRY_V_CALL2_DST_ADDR       = 0x100,     
    MIDWARE_ENTRY_END_V_LINE_STATUS      = 0x200

}MIDWARE_V_LINE_STATUS_E;


/*Length definition*/
#define MIDWARE_DBA_MAX_QUEUESET_NUM  4
#define MIDWARE_VENDOR_ID_LEN         4
#define MIDWARE_VENDOR_ID_LEN_PL1     8
#define MIDWARE_ONU_MODEL_LEN         4
#define MIDWARE_ONU_MODEL_LEN_PL1     8
#define MIDWARE_HW_VERSION_LEN        8
#define MIDWARE_HW_VERSION_LEN_PL1    12
#define MIDWARE_SW_VERSION_LEN        16
#define MIDWARE_SW_VERSION_LEN_PL1    20
#define MIDWARE_FW_VERSION_LEN        4
#define MIDWARE_CHIP_ID_LEN           2
#define MIDWARE_CHIP_VER_LEN          3
#define MIDWARE_CHIP_VER_LEN_PL1      4
#define MIDWARE_MAC_ADD_LEN           6
#define MIDWARE_MAC_ADD_LEN_PL1       8
#define MIDWARE_SN_LEN                16
#define MIDWARE_SN_LEN_PL1            20
#define MIDWARE_IPV4_IP_LEN           4
#define MIDWARE_IPV6_IP_LEN           16
#define MIDWARE_DBA_MAX_QUEUE_PER_SET 8
#define MIDWARE_ENC_ENTRY_LEN         4
#define MIDWARE_IMAGE_NAME_LEN        64
#define MIDWARE_IMAGE_VER_LEN         16
#define MIDWARE_IMAGE_VER_LEN_PL1     20
#define MIDWARE_IMAGE_FILENAME_LEN    48
#define MIDWARE_IMAGE_FILENAME_LEN_PL1 52
#define MIDWARE_LOID_USER_LEN         24
#define MIDWARE_LOID_USER_LEN_PL1     28
#define MIDWARE_LOID_PWD_LEN          12
#define MIDWARE_LOID_PWD_LEN_PL1      16
#define MIDWARE_WEB_USER_LEN          16
#define MIDWARE_WEB_USER_LEN_PL1      20
#define MIDWARE_WEB_PWD_LEN           16
#define MIDWARE_WEB_PWD_LEN_PL1       20
#define MIDWARE_IAD_SW_VER_LEN        32
#define MIDWARE_IAD_SW_VER_LEN_PL1    36
#define MIDWARE_IAD_SW_TIME_LEN       32
#define MIDWARE_IAD_SW_TIME_LEN_PL1   36
#define MIDWARE_PPPOE_USER_LEN        32
#define MIDWARE_PPPOE_USER_LEN_PL1    36
#define MIDWARE_PPPOE_PWD_LEN         32
#define MIDWARE_PPPOE_PWD_LEN_PL1     36
#define MIDWARE_H248_MG_ID_LEN        64
#define MIDWARE_H248_MG_ID_LEN_PL1    68
#define MIDWARE_H248_USER_NAME_LEN    32
#define MIDWARE_H248_USER_NAME_LEN_PL1 36
#define MIDWARE_RTP_PREFIX_LEN        16
#define MIDWARE_RTP_PREFIX_LEN_PL1    20
#define MIDWARE_RTP_DIGIT_LEN         8
#define MIDWARE_RTP_DIGIT_LEN_PL1     12
#define MIDWARE_RTP_NAME_LEN          32
#define MIDWARE_RTP_NAME_LEN_PL1      36
#define MIDWARE_SIP_USER_ACCOUNT_LEN  16
#define MIDWARE_SIP_USER_ACCOUNT_LEN_PL1 20
#define MIDWARE_SIP_USER_NAME_LEN     32
#define MIDWARE_SIP_USER_NAME_LEN_PL1 36
#define MIDWARE_SIP_USER_PWD_LEN      16
#define MIDWARE_SIP_USER_PWD_LEN_PL1  20
#define MIDWARE_DIGIMAP_LEN           1024
#define MIDWARE_DIGIMAP_LEN_PL1       1028
#define MIDWARE_VLAN_LIST_LEN         32
#define MIDWARE_AGGRE_RULE_MAX_NUM    4
#define MIDWARE_AGGRE_VLAN_MAX_NUM    8

#define MIDWARE_MIN_IMAGE_ID          0
#define MIDWARE_MAX_IMAGE_ID          1
#define MIDWARE_ONU_ID_LEN            25
#define MIDWARE_ONU_ID_LEN_PL1        28
#define MIDWARE_DOMAIN_NAME_LEN       25
#define MIDWARE_DOMAIN_NAME_LEN_PL1   28
#define MIDWARE_HOST_NAME_LEN         25
#define MIDWARE_HOST_NAME_LEN_PL1     28

#define MIDWARE_USER_NAME_LEN         50
#define MIDWARE_USER_NAME_LEN_PL1     52

#define MIDWARE_DEFAULT_STRING_LEN        25
#define MIDWARE_DEFAULT_STRING_LEN_PL1    28
#define MIDWARE_OMCI_URL_LEN              80
#define MIDWARE_OMCI_URL_LEN_PL1          84
#define MIDWARE_ACCESS_NODE_LEN           5
#define MIDWARE_ACCESS_NODE_LEN_PL1       8


/* OMCI lock and ulock state */
typedef enum 
{
    MIDWARE_OMCI_UNLOCK_STATE         = 0,
    MIDWARE_OMCI_LOCK_STATE           = 1
    
}MIDWARE_OMCI_LOCK_STATE_E;

/*ONU type*/
typedef enum 
{
    MIDWARE_ONU_TYPE_SFU              = 0,
    MIDWARE_ONU_TYPE_HGU              = 1,
    MIDWARE_ONU_TYPE_SBU              = 2,
    MIDWARE_ONU_TYPE_F_MDU            = 3, 
    MIDWARE_ONU_TYPE_X_MDU            = 4, 
    MIDWARE_ONU_TYPE_BOX_DSL_MDU      = 5, 
    MIDWARE_ONU_TYPE_RACK_DSL_MDU     = 6, 
    MIDWARE_ONU_TYPE_HYBIDR_MDU       = 7, 
    MIDWARE_ONU_TYPE_OTHER                 
}MIDWARE_ONU_TYPE_E;

/*Structure of ONU info table*/
typedef struct 
{
    UINT32     index;  
    UINT8      vendor_id[MIDWARE_VENDOR_ID_LEN_PL1];
    UINT8      onu_model[MIDWARE_ONU_MODEL_LEN_PL1];
    UINT8      hw_ver[MIDWARE_HW_VERSION_LEN_PL1];   
    UINT8      sw_ver[MIDWARE_SW_VERSION_LEN_PL1]; 
    UINT8      fw_ver[MIDWARE_FW_VERSION_LEN]; 
    UINT8      chip_id[MIDWARE_CHIP_ID_LEN]; 
    UINT16     chip_model; 
    UINT32     chip_rev; 
    UINT8      chip_ver[MIDWARE_CHIP_VER_LEN_PL1];  
    UINT8      sn[MIDWARE_SN_LEN_PL1];      
    UINT32     ge_port_num;     
    UINT32     ge_port_bitmap;  
    UINT32     fe_port_num;     
    UINT32     fe_port_bitmap;     
    UINT8      pots_port_num;  
    UINT8      e1_port_num;  
    UINT8      wlan_port_num;  
    UINT8      usb_port_num;      
    UINT8      us_queue_num; 
    UINT8      us_queue_num_per_port;     
    UINT8      ds_queue_num; 
    UINT8      ds_queue_num_per_port;
    UINT8      battery_backup;       
    UINT8      onu_type;      
    UINT8      llid_num; 
    UINT8      queue_num_per_llid;
    UINT8      ipv6_support; 
    UINT8      power_control;
    UINT32     onu_up_time;
    UINT8      service_sla;
}MIDWARE_TABLE_ONU_INFO_T;

/*OAM stack status*/
typedef enum 
{
    MIDWARE_OAM_STACK_DISABLE         = 0,
    MIDWARE_OAM_STACK_ENABLE          = 1
}MIDWARE_OAM_STACK_STATUS_E;

/*Structure of ONU configuration table*/
typedef struct 
{
    UINT8       index; 
    UINT8       reset_onu;     
    UINT16      restore_onu;       
    UINT32      mac_age_time;  
    UINT8       clear_dynamic_mac;
    UINT8       oam_stack_status;
    UINT8       synchronize_time;
    UINT8       mib_reset;
    UINT32      pm_interval;
    UINT32      mtu_size;     
}MIDWARE_TABLE_ONU_CFG_T;

/*UNI port type*/
typedef enum 
{
    MIDWARE_UNI_TYPE_FE               = 0,
    MIDWARE_UNI_TYPE_GE               = 1
}MIDWARE_UNI_TYPE_E;

#define NETWORK_INTERFACE_NAME_LEN 30

/*IP interface*/
typedef enum 
{    
    MIDWARE_IP_PON0          = 0,
    MIDWARE_IP_ETH0          = 0x10000
}MIDWARE_IP_ID_E;

/*IP type*/
typedef enum 
{
    MIDWARE_IP_TYPE_LAN_HOST          = 0,
    MIDWARE_IP_TYPE_WAN_HOST          = 1,
    MIDWARE_IP_TYPE_VOIP_HOST         = 2,
    MIDWARE_IP_TYPE_TR069_HOST        = 3  
}MIDWARE_IP_TYPE_E;

/*IP options*/
typedef enum 
{
    MIDWARE_IP_OPTION_EN_DHCP         = 0x01,
    MIDWARE_IP_OPTION_EN_PING         = 0x02,
    MIDWARE_IP_OPTION_EN_TRACEROUTE   = 0x04,
    MIDWARE_IP_OPTION_EN_IP_STACK     = 0x08
}MIDWARE_IP_OPTION_E;

/*ONU IP*/
typedef struct 
{
    UINT32      entity_id;
    MIDWARE_IP_TYPE_E    entity_type;    
    MIDWARE_IP_OPTION_E  ip_option;
    UINT8       onu_id[MIDWARE_ONU_ID_LEN_PL1];
    UINT8       mac[MIDWARE_MAC_ADD_LEN_PL1];
    UINT8       ip[MIDWARE_IPV4_IP_LEN];      
    UINT8       netmask[MIDWARE_IPV4_IP_LEN];    
    UINT8       gateway[MIDWARE_IPV4_IP_LEN];    
    UINT8       pri_dns[MIDWARE_IPV4_IP_LEN];
    UINT8       sec_dns[MIDWARE_IPV4_IP_LEN];    
    UINT8       cur_ip[MIDWARE_IPV4_IP_LEN];      
    UINT8       cur_netmask[MIDWARE_IPV4_IP_LEN];    
    UINT8       cur_gateway[MIDWARE_IPV4_IP_LEN];    
    UINT8       cur_pri_dns[MIDWARE_IPV4_IP_LEN];
    UINT8       cur_sec_dns[MIDWARE_IPV4_IP_LEN];        
    UINT8       domain_name[MIDWARE_DOMAIN_NAME_LEN_PL1];
    UINT8       host_name[MIDWARE_HOST_NAME_LEN_PL1];
}MIDWARE_TABLE_ONU_IP_T;

typedef enum
{
    MIDWARE_LOOPBACK_DETECTION_DEACTIVE = 1,
    MIDWARE_LOOPBACK_DETECTION_ACTIVE   = 2
}MIDWARE_LOOPBACK_DETECTION_ADMIN_E;

typedef enum
{
    MIDWARE_PORT_DISABLE = 0,
    MIDWARE_PORT_BLOCKING,
    MIDWARE_PORT_LEARNING,
    MIDWARE_PORT_FORWARDING
}MIDWARE_PORT_STP_STATE_E;

typedef enum {
    MIDWARE_SPEED_AUTO_DUPLEX_AUTO,
    MIDWARE_SPEED_1000_DUPLEX_AUTO,
    MIDWARE_SPEED_100_DUPLEX_AUTO,
    MIDWARE_SPEED_10_DUPLEX_AUTO,
    MIDWARE_SPEED_AUTO_DUPLEX_FULL,
    MIDWARE_SPEED_AUTO_DUPLEX_HALF,
    MIDWARE_SPEED_1000_DUPLEX_FULL,
    MIDWARE_SPEED_1000_DUPLEX_HALF,
    MIDWARE_SPEED_100_DUPLEX_FULL,
    MIDWARE_SPEED_100_DUPLEX_HALF,
    MIDWARE_SPEED_10_DUPLEX_FULL,
    MIDWARE_SPEED_10_DUPLEX_HALF
} MIDWARE_AUTONEG_MODE_E;

typedef enum {
    MIDWARE_ETH_INTERNAL_LOOPBACK_DISABLE,
    MIDWARE_ETH_INTERNAL_LOOPBACK_EN,
    MIDWARE_ETH_EXTERNAL_LOOPBACK_DISABLE,
    MIDWARE_ETH_EXTERNAL_LOOPBACK_EN
} MIDWARE_ETH_LOOPBACK_E;

/*Structure of common ONU UNI port configuration*/
typedef struct 
{
    UINT8       port_id;
    UINT8       port_type;
    UINT8       port_admin;     
    UINT8       link_state;  
    UINT8       autoneg_cfg;     
    UINT8       autoneg_mode;
    UINT8       reset_autoneg;   
/*  UINT8       speed_cfg;  */
    UINT8       speed_state; 
/*  UINT8       duplex_cfg; */
    UINT8       duplex_state;  
    UINT8       loopback;     
    UINT16      port_isolate; 
    UINT8       port_flood;   
    UINT8       mac_learn_en; 
    UINT16      default_vid;
    UINT8       default_pri;    
    UINT8       discard_tag;
    UINT8       discard_untag;
    UINT8       vlan_filter;
    UINT8       loop_detect;
    UINT8       loop_detect_status;    
    UINT8       in_mirror_en;
    UINT8       in_mirror_src;
    UINT8       in_mirror_dst;
    UINT8       out_mirror_en;
    UINT16      out_mirror_src;
    UINT16      out_mirror_dst;
    UINT16      stp_state;
/*  MIDWARE_AUTONEG_MODE_E   configurationInd;*/
}MIDWARE_TABLE_UNI_CFG_T;

/*Middleware rate limit mode*/
typedef enum 
{  
    MIDWARE_LIMIT_MODE_FRAME         = 0,
    MIDWARE_LIMIT_MODE_LAYER1        = 1,
    MIDWARE_LIMIT_MODE_LAYER2        = 2,
    MIDWARE_LIMIT_MODE_LAYER3        = 3
}MIDWARE_LIMIT_MODE_E;

/*Structure of ONU UNI QoS configuration*/
typedef struct 
{
    UINT8       port_id;
    UINT8       fc_cfg;     
    UINT16      fc_state;  
    UINT16      pause_time;      
    UINT8       us_policing_en;     
    UINT8       us_policing_mode;      
    UINT32      us_policing_cir;  
    UINT32      us_policing_cbs; 
    UINT32      us_policing_ebs; 
    UINT16      ds_ratelimit_en;     
    UINT16      ds_ratelimit_mode;      
    UINT32      ds_ratelimit_cir;  
    UINT32      ds_ratelimit_pir;  
}MIDWARE_TABLE_UNI_QOS_T;

/*Structure of VLAN table*/
typedef struct 
{
    UINT32      vlan_id;     
    UINT32      untag_member_vec;  
    UINT32      tag_member_vec;     
}MIDWARE_TABLE_VLAN_T;

/*Structure of flow table*/
typedef struct 
{
    UINT16     flow_id;
    UINT8      hw_flow_id;
    UINT8      ingress_port; 
    UINT32     parse_bm; 
    UINT8      precedence; 
    UINT8      target_port; 
    UINT16     target_queue; 
    UINT32     target_gem_port; 
    UINT32     mod_bm; 
    UINT32     flow_en; 
}MIDWARE_TABLE_FLOW_T;

/*Structure of VLAN modification table*/
typedef struct 
{
    UINT16     flow_id;
    UINT16     vlan_oper;
    UINT16     in_tpid; 
    UINT16     in_vid; 
    UINT16     in_vid_mask; 
    UINT8      in_cfi; 
    UINT8      in_pbit;     
    UINT16     in_pbit_mask; 
    UINT16     out_tpid; 
    UINT16     out_vid; 
    UINT16     out_vid_mask; 
    UINT16     out_cfi; 
    UINT8      out_pbit;     
    UINT8      out_pbit_mask; 
}MIDWARE_TABLE_FLOW_MOD_VLAN_T;

/*Structure of MAC modification table*/
typedef struct 
{
    UINT32     flow_id;
    UINT8      mac_da[MIDWARE_MAC_ADD_LEN_PL1];
    UINT8      mac_da_mask[MIDWARE_MAC_ADD_LEN_PL1];
    UINT8      mac_sa[MIDWARE_MAC_ADD_LEN_PL1];
    UINT8      mac_sa_mask[MIDWARE_MAC_ADD_LEN_PL1];
}MIDWARE_TABLE_FLOW_MOD_MAC_T;

/*Structure of PPPoE modification table*/
typedef struct 
{
    UINT16     flow_id;
    UINT16     session_id;
    UINT32     protocol;
}MIDWARE_TABLE_FLOW_MOD_PPPOE_T;

/*Structure of IPV4 modification table*/
typedef struct 
{
    UINT16     flow_id;
    UINT8      ipv4_dscp;
    UINT8      ipv4_dscp_mask;
    UINT32     ipv4_proto; 
    UINT32     ipv4_src_ip; 
    UINT32     ipv4_src_ip_mask; 
    UINT32     ipv4_dst_ip;     
    UINT32     ipv4_dst_ip_mask; 
    UINT16     ipv4_src_port; 
    UINT16     ipv4_dst_port; 
}MIDWARE_TABLE_FLOW_MOD_IPV4_T;

/*Structure of IPV6 modification table*/
typedef struct 
{
    UINT16     flow_id;
    UINT8      ipv6_dscp;
    UINT8      ipv6_dscp_mask;
    UINT32     ipv6_proto; 
    UINT8      ipv6_src_ip[MIDWARE_IPV6_IP_LEN]; 
    UINT8      ipv6_src_ip_mask[MIDWARE_IPV6_IP_LEN]; 
    UINT8      ipv6_dst_ip[MIDWARE_IPV6_IP_LEN];     
    UINT8      ipv6_dst_ip_mask[MIDWARE_IPV6_IP_LEN]; 
    UINT16     ipv6_src_port; 
    UINT16     ipv6_dst_port; 
}MIDWARE_TABLE_FLOW_MOD_IPV6_T;

/*Structure of flow layer 2 key table*/
typedef struct 
{
    UINT32     flow_id;
    UINT8      mac_da[MIDWARE_MAC_ADD_LEN_PL1];
    UINT8      mac_da_mask[MIDWARE_MAC_ADD_LEN_PL1];
    UINT8      mac_sa[MIDWARE_MAC_ADD_LEN_PL1];
    UINT8      mac_sa_mask[MIDWARE_MAC_ADD_LEN_PL1];
    UINT16     num_of_tag;  
    UINT16     in_tpid; 
    UINT16     in_vid; 
    UINT16     in_mask;  
    UINT8      in_cfi; 
    UINT8      in_pbit;
    UINT16     in_pbit_mask; 
    UINT16     out_tpid; 
    UINT16     out_vid; 
    UINT16     out_mask;  
    UINT8      out_cfi; 
    UINT8      out_pbit;
    UINT16     out_pbit_mask;  
    UINT16     eth_type; 
    UINT16     ppp_session; 
    UINT16     ppp_proto; 
    UINT16     gem_port; 
    UINT16     llid;  

}MIDWARE_TABLE_FLOW_KEY_L2_T;

/*Structure of flow IPV4 key table*/
typedef struct 
{
    UINT16     flow_id;
    UINT8      ipv4_dscp;
    UINT8      ipv4_dscp_mask;
    UINT8      ipv4_proto;
    UINT32     ipv4_src_ip;
    UINT32     ipv4_src_ip_mask;
    UINT32     ipv4_dst_ip;
    UINT32     ipv4_dst_ip_mask; 
    UINT16     ipv4_src_port; 
    UINT16     ipv4_dst_port; 

}MIDWARE_TABLE_FLOW_KEY_IPV4_T;

/*Structure of flow IPV6 key table*/
typedef struct 
{
    UINT16     flow_id;
    UINT8      ipv6_dscp;
    UINT8      ipv6_dscp_mask;
    UINT8      ipv6_next_header;
    UINT8      ipv6_src_ip[MIDWARE_IPV6_IP_LEN];
    UINT8      ipv6_src_ip_maskMIDWARE_IPV6_IP_LEN;
    UINT8      ipv6_dst_ip[MIDWARE_IPV6_IP_LEN];
    UINT8      ipv6_dst_ip_mask[MIDWARE_IPV6_IP_LEN]; 
    UINT16     ipv6_src_port; 
    UINT16     ipv6_dst_port; 

}MIDWARE_TABLE_FLOW_KEY_IPV6_T;

/*Multicast mode*/
typedef enum 
{
    MIDWARE_MC_TYPE_IGMP_SNOOPING                 = 0x00,
    MIDWARE_MC_TYPE_CTC_CONTROL                   = 0x01,    
    MIDWARE_MC_TYPE_IGMP_PROXY                    = 0x02,
    MIDWARE_MC_TYPE_IGMP_SNOOPING_AND_PROXYREPORT = 0x03,
}MIDWARE_MC_TYPE_E;

/*MC Fast leave ability*/
typedef enum 
{
    MIDWARE_MC_IGMP_SNOOPING_NON_FAST_LEAVE_SUPPORT     = 1,
    MIDWARE_MC_IGMP_SNOOPING_FAST_LEAVE_SUPPORT         = 2,
    MIDWARE_MC_MLD_SNOOPING_NON_FAST_LEAVE_SUPPORT      = 4,
    MIDWARE_MC_MLD_SNOOPING_FAST_LEAVE_SUPPORT          = 8, 
    MIDWARE_MC_CTC_CONTROL_NON_FAST_LEAVE_SUPPORT       = 16,
    MIDWARE_MC_CTC_CONTROL_FAST_LEAVE_SUPPORT           = 32 
}MIDWARE_MC_FAST_LEAVE_ABILITY_E;

/*MC Fast leave state*/
typedef enum 
{
    MIDWARE_FAST_LEAVE_ADMIN_STATE_DISABLED         = 0x01,
    MIDWARE_FAST_LEAVE_ADMIN_STATE_ENABLED          = 0x02
}MIDWARE_FAST_LEAVE_ADMIN_STATE_E;

/*IGMP version */
typedef enum 
{
    MIDWARE_IGMP_VERSION_1    = 0x01,
    MIDWARE_IGMP_VERSION_2    = 0x02,    
    MIDWARE_IGMP_VERSION_3    = 0x03,
    MIDWARE_MLD_VERSION_1     = 0x10,
    MIDWARE_MLD_VERSION_2     = 0x11  
}MIDWARE_IGMP_VERSION_E;

/*Structure of MC configuration table*/
typedef struct 
{
    UINT32     index;
    UINT8      igmp_version;    
    UINT8      mc_mode;
    UINT8      filter_mode;
    UINT8      fast_leave_ability;    
    UINT32     fast_leave_state;
}MIDWARE_TABLE_MC_CFG_T;

/*Structure of MC port configuration table*/
typedef struct 
{
    UINT16     port_id;
    UINT16     max_group_num;
    UINT32     us_igmp_rate;   
    UINT16     us_tag_oper;
    UINT16     ds_tag_oper;
    UINT32     us_tag_default_tci;
    UINT32     ds_tag_default_tci;
    UINT32     robust;   
    UINT32     query_ip;  
    UINT32     query_interval;  
    UINT32     query_max_resp_time;  
    UINT32     last_query_time;   
    UINT32     unauth_join_behaviour;        
}MIDWARE_TABLE_MC_PORT_CFG_T;

/*Structure of MC port status table*/
typedef struct 
{
    UINT32     port_id;
    UINT32     mc_current_bw;
    UINT32     join_count;   
    UINT32     exceed_bw_count;        
}MIDWARE_TABLE_MC_PORT_STATUS_T;

/*Structure of MC port active group table*/
typedef struct 
{
    UINT32     port_id;
    UINT16     index;    
    UINT16     vid;
    UINT32     src_ip;   
    UINT32     dst_mc_ip;
    UINT32     actual_bw;
    UINT32     client_ip;
    UINT32     last_join_time;
    
}MIDWARE_TABLE_MC_ACTIVE_GROUP_T;

/* MC ALC table type enum */
typedef enum 
{
    MIDWARE_MC_ACL_DYNAMIC_TABLE   = 0x00,
    MIDWARE_MC_ACL_STATIC_TABLE    = 0x01

}MIDWARE_MC_ACL_TYPE_E;

/* MC set control type */
typedef enum 
{
    MIDWARE_MC_SET_TYPE_RESERVED   = 0x00,
    MIDWARE_MC_SET_TYPE_ADD_RULE   = 0x01,
    MIDWARE_MC_SET_TYPE_DEL_RULE   = 0x02,
    MIDWARE_MC_SET_TYPE_CLEAR_RULE = 0x03

}MIDWARE_MC_SET_TYPE_E;


/*Structure of MC port control table*/
typedef struct 
{
    UINT16     port_id;   
    UINT16     table_type;
    UINT16     set_ctrl;   
    UINT16     row_key;
    UINT16     gem_port;
    UINT16     ani_vid;    
    UINT32     src_ip;
    UINT32     dst_ip_start;   
    UINT32     dst_ip_end;
    UINT32     imputed_group_bw;  
    UINT8      src_ipv6[MIDWARE_IPV6_IP_LEN];  
    UINT8      dst_ipv6[MIDWARE_IPV6_IP_LEN];   
    UINT16     preview_len;      
    UINT16     preview_repeat_time;   
    UINT16     preview_repeat_count;  
    UINT16     preview_reset_time; 
    UINT32     vendor_spec;      
    
}MIDWARE_TABLE_MC_PORT_CTRL_T;

/*Structure of MC port service table*/
typedef struct 
{
    UINT16     port_id;   
    UINT16     set_ctrl;   
    UINT16     row_key;
    UINT16     vid;
    UINT32     max_sum_group;
    UINT32     max_mc_bw;   
    
}MIDWARE_TABLE_MC_PORT_SERV_T;

/*Structure of MC port preview table*/
typedef struct 
{
    UINT16     port_id;    
    UINT16     set_ctrl;   
    UINT32     row_key;
    UINT8      src_ip[MIDWARE_IPV6_IP_LEN];  
    UINT8      dst_ip[MIDWARE_IPV6_IP_LEN];   
    UINT16     ani_vid;
    UINT16     uni_vid;   
    UINT32     duration;
    UINT32     time_left;    
    
}MIDWARE_TABLE_MC_PORT_PREVIEW_T;

/*Structure of MC downstream VLAN tag translation table*/
typedef struct  
{
    UINT32     port_id;
    UINT16     in_vid;
    UINT16     out_vid;
}MIDWARE_TABLE_MC_DS_VLAN_TRANS_T;

/*Structure of MC upstream VLAN tag translation table*/
typedef struct  
{
    UINT32     port_id;
    UINT16     in_vid;
    UINT16     out_vid;
}MIDWARE_TABLE_MC_US_VLAN_TRANS_T;


/*WAN schedule mode*/
typedef enum 
{
    MIDWARE_PP_SCHED_STRICT             = 0x1,
    MIDWARE_PP_SCHED_WRR                = 0x2,
    MIDWARE_PP_SCHED_STRICT_WRR         = 0x3
}MIDWARE_PP_SCHED_MODE_E;

/*Structure of WAN QoS table*/
typedef struct 
{
    UINT16     llid;
    UINT16     queue_id;
    UINT16     schedule_mode;
    UINT16     weight;
    UINT32     cir;
    UINT32     pir;
}MIDWARE_TABLE_WAN_QOS_T;

/*EPON FEC mode*/
typedef enum 
{
    MIDWARE_PON_FEC_MODE_UNKNOWN        = 0x1,
    MIDWARE_PON_FEC_MODE_ENABLED        = 0x2,
    MIDWARE_PON_FEC_MODE_DISABLED       = 0x3
}MIDWARE_PON_FEC_MODE_E;

typedef enum
{
    MIDWARE_HOLDOVER_STATE_DEACTIVE      = 0x00000001,
    MIDWARE_HOLDOVER_STATE_ACTIVE        = 0x00000002
}MIDWARE_HOLDOVER_STATE_E;

/*Structure of EPON table*/
typedef struct 
{
    UINT32                 index;
    UINT8                  base_pon_mac[MIDWARE_MAC_ADD_LEN_PL1];
    UINT8                  multi_llid;    
    UINT8                  fec;
    UINT16                 holdover_cfg;  
    UINT32                 holdover_time;  
    UINT16                 register_status;
    UINT16                 onu_llid;     
}MIDWARE_TABLE_EPON_T;

/*Structure of DBA table*/
typedef struct 
{
    UINT8      llid;
    UINT8      num_of_queuesets;    
    UINT16     num_of_queues; 
    UINT32     aggre_threshold;   
    uint_arr_s state[MIDWARE_DBA_MAX_QUEUE_PER_SET];
    uint_arr_s threshold[MIDWARE_DBA_MAX_QUEUE_PER_SET];    
/*
    UINT8      state[MIDWARE_DBA_MAX_QUEUESET_NUM][MIDWARE_DBA_MAX_QUEUE_PER_SET];
    UINT8      threshold[MIDWARE_DBA_MAX_QUEUESET_NUM][MIDWARE_DBA_MAX_QUEUE_PER_SET];    
*/
}MIDWARE_TABLE_DBA_T;

typedef enum 
{
    MIDWARE_SW_UPDATE_OK              = 0,/*successful after last updating ,or idle for next request.*/
    MIDWARE_SW_UPDATE_BUSY            = 1, 
    MIDWARE_SW_UPDATE_WRITING         = 2, 
    MIDWARE_SW_UPDATE_CHECKERROR      = 3,
    MIDWARE_SW_UPDATE_FILE_OUT_RANGE  = 4,
    MIDWARE_SW_UPDATE_FILE_NOT_EXIST  = 5,
    MIDWARE_SW_UPDATE_NOT_SUPPORT     = 6,
    MIDWARE_SW_UPDATE_NOT_STARTED     = 7
}MIDWARE_SW_UPDATE_STATE_E;

typedef enum 
{
    MIDWARE_MC_CTRL_TYPE_DA_ONLY = 0x00,
    MIDWARE_MC_CTRL_TYPE_DA_VID,
    MIDWARE_MC_CTRL_TYPE_DA_SRCIP,       
    MIDWARE_MC_CTRL_TYPE_IPV4_VID,
    MIDWARE_MC_CTRL_TYPE_IPV6_VID
}MIDWARE_MC_CTRL_TYPE_E;

/*Structure of software image table*/
typedef struct 
{
    UINT8      image_id;
    UINT8      active_image;
    UINT8      commit_image;
    UINT8      is_valid;
    UINT8      verion[MIDWARE_IMAGE_VER_LEN_PL1];
    UINT32     burn_to_flash;
    UINT32     abort;
    UINT8      image_file_name[MIDWARE_IMAGE_FILENAME_LEN_PL1];
    MIDWARE_SW_UPDATE_STATE_E status;    
}MIDWARE_TABLE_IMAGE_T;

/*Structure of optical transceiver table*/
typedef struct 
{
    UINT16     index;
    UINT16     temperature;
    UINT16     supply_voltage;
    UINT16     bias_current;
    UINT16     tx_power;
    UINT16     rx_power;
}MIDWARE_TABLE_TRANSCEIVER_T;

typedef enum
{
    MIDWARE_ALARM = 0,
    MIDWARE_TCA = 1,
    /*MIDWARE_AVC = 2*/
} MIDWARE_ALARM_CLASS;

typedef enum
{
    MIDWARE_STATE_OFF = 0,
    MIDWARE_STATE_ON = 1,
} MIDWARE_ALARM_STATE;

typedef enum
{
    MIDWARE_ADMIN_DISABLE = 0,
    MIDWARE_ADMIN_ENABLE=1,
} MIDWARE_ALARM_ADMIN;


/*Structure of alarm table*/
/*
type - give a unsigned long number for each alarm type(such as lan loss, battery failure and so on)
param1 - alarm instance index1, may be slot/port, gem port or others
param2 - for omci is class+meid, for eoam is eoam alarm id, used for reporting alarm
class - alarm(0) avc(1) tca(2)
admin -enable(1) disable(0)
state - alarm_cleared(0) alarm_raised(1) (no use for avc)
threshold - used for alarm and tca
clear_threshold - only used for epon alarm
info - for epon alarm, it is alarm info; for avc, it's the value
*/
typedef struct 
{
    UINT32  type;           /*index, alarm type*/
    UINT32  param1;         /*index, for example, slot/port*/
    UINT32  param2;         /*the mapping info of omci or eoam, for omci is class+instanceid, for eoam is eoam alarm id */
    UINT8   alarm_class;    
    UINT8   admin;
    UINT8   state;
    UINT8   dummy1;     /*used for 4 bytes aligned*/
    UINT32  threshold;
    UINT32  clear_threshold;
    UINT32  info;
}MIDWARE_TABLE_ALARM_T;

#if 0
typedef struct 
{
    UINT16     alarm_id;
    UINT8      port_id;    
    UINT8      enable;
    UINT32     threshold;
    UINT32     clear_threshold;
    UINT32     state;
}MIDWARE_TABLE_ALARM_T;

/*Structure of alarm event table*/
typedef struct 
{
    UINT16     alarm_id;
    UINT16     port_id;
    UINT32     event_time;    
    UINT32     type;
    UINT32     value;
    UINT32     state;
}MIDWARE_TABLE_ALARM_EVENT_T;
#endif

/*
type    - PM type(e.g. Ethernet frame performance monitoring history data upstream)
param1 - PM instance index1, may be slot/port, gem port or others
param2 - for omci is class+meid, for eoam is eoam alarm id, used for reporting alarm
admin -enable(1) disable(0)
accumulation_mode - 15-minute accumulation mode(0) continuous accumulation(1)
global_clear - for omci requirements, if set to 1 will clear PM data and tca bitmap, always return 0 by get operation
counter[i] - the pm counter attribute i

*/
#define PM_GLOBAL_CLEAR 1
#define PM_COUNTERS_MAX_NUM 16
typedef enum
{
    MIDWARE_ACCUMULATION_15_MIN = 0,
    MIDWARE_ACCUMULATION_CONTINUOUS = 1,
} MIDWARE_PM_ACCUMULATION_MODE;

/*Structure of pm table*/
typedef struct 
{
    UINT32  type;       /*index*/
    UINT32  param1;     /*index*/
    UINT32  param2;     /*index*/
    UINT16  admin_bits;
    UINT8   accumulation_mode;
    UINT8   global_clear;
    UINT32  interval;
    UINT32  counter0;
    UINT32  counter1;
    UINT32  counter2;
    UINT32  counter3;
    UINT32  counter4;
    UINT32  counter5;
    UINT32  counter6;
    UINT32  counter7;
    UINT32  counter8;
    UINT32  counter9;
    UINT32  counter10;
    UINT32  counter11;
    UINT32  counter12;
    UINT32  counter13;
    UINT32  counter14;
    UINT32  counter15;
}MIDWARE_TABLE_PM_T;

#define MIDWARE_AVC_VALUE_LENGTH 28

/*Structure of avc table*/
typedef struct 
{
    UINT32  type;       /*index,  same to apm avc type*/
    UINT32  param1;     /*index*/
    UINT32  param2;     /*index*/
    UINT8   admin;
    UINT8   dummy1;
    UINT16  dummy2;
    UINT8   value[MIDWARE_AVC_VALUE_LENGTH]; /*length 28*/
}MIDWARE_TABLE_AVC_T;


/*Structure of LOID table*/
typedef struct 
{
    UINT16     index;
    UINT16     auth_mode;
    UINT8      loid[MIDWARE_LOID_USER_LEN_PL1];
    UINT8      pwd[MIDWARE_LOID_PWD_LEN_PL1];
}MIDWARE_TABLE_LOID_T;

/*Structure of WEB account table*/
typedef struct 
{
    UINT8      name[MIDWARE_WEB_USER_LEN_PL1];
    UINT8      pwd[MIDWARE_WEB_PWD_LEN_PL1];
    UINT32     account_type;
}MIDWARE_TABLE_WEB_ACCOUNT_T;

/*Structure of IAD POTS port administration table*/
typedef struct 
{
    UINT16     port_id;
    UINT16     port_admin;
}MIDWARE_TABLE_POTS_ADMIN_T;

/*Structure of IAD info table*/
typedef struct 
{
    UINT32     index;
    UINT8      mac_addr[MIDWARE_MAC_ADD_LEN_PL1];
    UINT32     protocol;
    UINT8      sw_ver[MIDWARE_IAD_SW_VER_LEN_PL1];
    UINT8      sw_time[MIDWARE_IAD_SW_TIME_LEN_PL1];
    UINT32     pots_num;
}MIDWARE_TABLE_IAD_INFO_T;

/*Structure of IAD configuration table*/
typedef struct  
{
    UINT16     index;
    UINT16     ip_mode;
    UINT32     ip_addr;
    UINT32     net_mask;
    UINT32     def_gw;
    UINT32     pppoe_mode;
    UINT8      pppoe_user[MIDWARE_PPPOE_USER_LEN_PL1];
    UINT8      pppoe_pwd[MIDWARE_PPPOE_PWD_LEN_PL1];
    UINT16     tag_flag;
    UINT16     cvlan;
    UINT16     svlan;
    UINT16     pbits;
}MIDWARE_TABLE_IAD_CFG_T;

/*Structure of H248 configuration table*/
typedef struct 
{
    UINT16     index;
    UINT16     mg_port;
    UINT32     mgc_ip;
    UINT32     mgc_port;
    UINT32     backup_mgc_ip;
    UINT16     backup_mgc_port;    
    UINT8      active_mgc;
    UINT8      reg_mode;
    UINT8      mg_id[MIDWARE_H248_MG_ID_LEN_PL1];
    UINT16     heartbeat_mode;
    UINT32     heartbeat_cycle;
    UINT32     heartbeat_count;
}MIDWARE_TABLE_H248_CFG_T;

/*Structure of H248 user TID information table*/
typedef struct  
{
    UINT32     port_id;
    UINT8      user_name[MIDWARE_H248_USER_NAME_LEN_PL1];
}MIDWARE_TABLE_H248_USER_INFO_T;

/*Structure of H248 RTP TID configuration table*/
typedef struct 
{
    UINT16     index;
    UINT16     num_of_rtp;
    UINT8      rtp_prefix[MIDWARE_RTP_PREFIX_LEN_PL1];
    UINT8      rtp_digit[MIDWARE_RTP_DIGIT_LEN_PL1];
    UINT16     rtp_mode;
    UINT16     rtp_digit_len;
}MIDWARE_TABLE_H248_RTP_CFG_T;

/*Structure of H248 RTP TID information table*/
typedef struct 
{
    UINT16     index;
    UINT16     num_of_rtp;
    UINT8      first_rtp_name[MIDWARE_RTP_NAME_LEN_PL1];
}MIDWARE_TABLE_H248_RTP_INFO_T;

/*Structure of SIP configuration table*/
typedef struct 
{
    UINT16     index;
    UINT16     mg_port;
    UINT32     proxy_ip;
    UINT32     proxy_port;
    UINT32     backup_proxy_ip;
    UINT32     backup_proxy_port;    
    UINT32     active_proxy;
    UINT32     reg_ip;
    UINT32     reg_port;
    UINT32     backup_reg_ip;
    UINT32     backup_reg_port;  
    UINT32     outband_ip;
    UINT32     outband_port;
    UINT32     reg_interval;    
    UINT32     heartbeat_switch;
    UINT16     heartbeat_cycle;
    UINT16     heartbeat_count;
}MIDWARE_TABLE_SIP_CFG_T;

/*Structure of SIP user table*/
typedef struct 
{
    UINT32     port_id;
    UINT8      user_account[MIDWARE_SIP_USER_ACCOUNT_LEN_PL1];
    UINT8      user_name[MIDWARE_SIP_USER_NAME_LEN_PL1];
    UINT8      user_pwd[MIDWARE_SIP_USER_PWD_LEN_PL1];
}MIDWARE_TABLE_SIP_USER_T;

/*Structure of FAX table*/
typedef struct 
{
    UINT8      index;
    UINT8      t38_en;
    UINT16     fax_ctrl;
}MIDWARE_TABLE_FAX_T;

/*Structure of IAD status table*/
typedef struct 
{
    UINT32      index;
    UINT32      status;
}MIDWARE_TABLE_IAD_STATUS_T;

/*Structure of POTS status table*/
typedef struct 
{
    UINT32      port_id;
    UINT32      port_status;
    UINT32      service_status;
    UINT32      codec;    
}MIDWARE_TABLE_POTS_STATUS_T;

/*Structure of IAD operation table*/
typedef struct 
{
    UINT32      index;
    UINT32      operation;  
}MIDWARE_TABLE_IAD_OPER_T;

/*Structure of SIP digitalmap table*/
typedef struct 
{
    UINT32      index;
    UINT8       digitmap[MIDWARE_DIGIMAP_LEN_PL1];
}MIDWARE_TABLE_DIGITMAP_T;

/*Structure of RSTP table*/
typedef struct 
{
    UINT16      index;
    UINT16      rstp_en;
    UINT32      bridge_pri;
    UINT32      fwd_delay;
    UINT32      hello_time;    
    UINT32      age_time;   
}MIDWARE_TABLE_RSTP_T;

/*Structure of RSTP port table*/
typedef struct 
{
    UINT16      port_id;
    UINT16      rstp_en;
    UINT32      port_pri; 
}MIDWARE_TABLE_RSTP_PORT_T;

/*Structure of switch MAC address*/
typedef struct 
{
    UINT8       mac[MIDWARE_MAC_ADD_LEN_PL1];
    UINT32      port_vec;
}MIDWARE_TABLE_SW_MAC_T;

/*Structure of illegal table*/
typedef struct 
{
    UINT32      ret_val;
}MIDWARE_TABLE_ILLEGAL_ENTRY_T;

/*Enum of VLAN mode defined by CTC */
typedef enum
{
    MIDWARE_VLAN_MODE_TRANSPARENT        = 0x00,
    MIDWARE_VLAN_MODE_TAG                = 0x01,
    MIDWARE_VLAN_MODE_TRANSLATION        = 0x02,
    MIDWARE_VLAN_MODE_AGGREGATION        = 0x03,
    MIDWARE_VLAN_MODE_TRUNK              = 0x04
}MIDWARE_VLAN_MODE_E;

/*Structure of VLAN port table*/
typedef struct 
{
    UINT8       port_id;
    UINT8       vlan_mode;
    UINT16      vlan_num;
    UINT32      default_vlan;
    UINT32      vlan_list[MIDWARE_VLAN_LIST_LEN];    
    UINT32      aggre_num[MIDWARE_AGGRE_RULE_MAX_NUM]; 
    UINT32      aggre_target_vlan[MIDWARE_AGGRE_RULE_MAX_NUM];   
    UINT32      aggre_vlan_list[MIDWARE_AGGRE_RULE_MAX_NUM][MIDWARE_AGGRE_VLAN_MAX_NUM];     
}MIDWARE_TABLE_VLAN_PORT_T;

/*Structure of port statistics table*/
typedef struct 
{
    UINT16      port_id;
    UINT16      enable;
    UINT32      packet_rx;
    UINT32      unicast_packet_rx;
    UINT32      multicast_packet_rx;
    UINT32      broadcast_packet_rx;
    UINT32      packet_64B_rx;
    UINT32      packet_65_127B_rx;
    UINT32      packet_128_255B_rx;
    UINT32      packet_256_511B_rx;
    UINT32      packet_512_1023B_rx;
    UINT32      packet_1024_1518B_rx;
    UINT32      packet_1519B_rx;
    UINT32      packet_tx;
    UINT32      unicast_packet_tx;
    UINT32      multicast_packet_tx;
    UINT32      broadcast_packet_tx;
    UINT32      packet_64B_tx;
    UINT32      packet_65_127B_tx;
    UINT32      packet_128_255B_tx;
    UINT32      packet_256_511B_tx;
    UINT32      packet_512_1023B_tx;
    UINT32      packet_1024_1518B_tx;
    UINT32      packet_1519B_tx;

}MIDWARE_TABLE_STATISTICS_T;

/* VoIP protocal bitmap */
typedef enum 
{
    MIDWARE_V_PROTO_BM_SIP            = 0x01,
    MIDWARE_V_PROTO_BM_H248           = 0x02,
    MIDWARE_V_PROTO_BM_MGCP           = 0x04
}MIDWARE_V_PROTO_BM_E;

/* VoIP protocal type */
typedef enum 
{
    MIDWARE_V_PROTO_TYPE_SIP          = 0x01,
    MIDWARE_V_PROTO_TYPE_H248         = 0x02,
    MIDWARE_V_PROTO_TYPE_MGCP         = 0x03,
    MIDWARE_V_PROTO_NONE_OMCI         = 0xff
}MIDWARE_V_PROTO_TYPE_E;

/* VoIP method bitmap */
typedef enum 
{
    MIDWARE_V_METHOD_BM_OMCI          = 0x01,
    MIDWARE_V_METHOD_BM_FILE          = 0x02,
    MIDWARE_V_METHOD_BM_TR69          = 0x04,
    MIDWARE_V_METHOD_BM_IETF          = 0x08    
    
}MIDWARE_V_METHOD_BM_E;

/* VoIP method type */
typedef enum 
{
    MIDWARE_V_METHOD_TYPE_NOT_CONF    = 0x00,
    MIDWARE_V_METHOD_TYPE_OMCI        = 0x01,
    MIDWARE_V_METHOD_TYPE_FILE        = 0x02,
    MIDWARE_V_METHOD_TYPE_TR69        = 0x03,
    MIDWARE_V_METHOD_NONE_IETF        = 0xff
    
}MIDWARE_V_METHOD_TYPE_E;

/* VoIP config state */
typedef enum 
{
    MIDWARE_V_CONFGI_STATE_INACTIVE   = 0x00,
    MIDWARE_V_CONFGI_STATE_ACTIVE     = 0x01,
    MIDWARE_V_CONFGI_STATE_INITIAL    = 0x02,
    MIDWARE_V_CONFGI_STATE_FAULT      = 0x03,
    
}MIDWARE_V_CONFIG_STATE_E;

/* IAD operation enum */
typedef enum 
{
    MIDWARE_V_IAD_OPERATION_RE_INIT   = 0x00,
    MIDWARE_V_IAD_OPERATION_LOGOFF    = 0x01,
    MIDWARE_V_IAD_OPERATION_RESET     = 0x02
    
}MIDWARE_V_IAD_OPERATION_E;

/*Structure of VoIP global configuration table*/
typedef struct  
{
    UINT16     index;
    UINT8      available_proto;
    UINT8      used_proto;
    UINT32     available_method;
    UINT8      used_method;
    UINT8      config_state;
    UINT16     retrieve_profile;
    UINT8      profile_version[MIDWARE_DEFAULT_STRING_LEN_PL1];
    UINT8      mac_addr[MIDWARE_MAC_ADD_LEN_PL1];
    UINT8      sw_ver[MIDWARE_IAD_SW_VER_LEN_PL1];
    UINT8      sw_time[MIDWARE_IAD_SW_TIME_LEN_PL1];
    UINT16     pots_num;
    UINT16     ip_mode;
    UINT32     ip_addr;
    UINT32     net_mask;
    UINT32     def_gw;
    UINT32     pppoe_mode;
    UINT8      pppoe_user[MIDWARE_PPPOE_USER_LEN_PL1];
    UINT8      pppoe_pwd[MIDWARE_PPPOE_PWD_LEN_PL1];
    UINT16     tag_flag;
    UINT16     cvlan;
    UINT16     svlan;
    UINT16     pbits;
    UINT32     iad_operate;    

}MIDWARE_TABLE_V_CFG_T;

/* VoIP hook state */
typedef enum 
{
    MIDWARE_V_ON_HOOK_STATE         = 0x00,
    MIDWARE_V_OFF_HOOK_STATE        = 0x01
    
}MIDWARE_V_HOOK_STATE_E;

/* VoIP signalling code */
typedef enum 
{
    MIDWARE_V_SIGNAL_LOOP_START        = 0x00,
    MIDWARE_V_SIGNAL_GROUND_START      = 0x01,
    MIDWARE_V_SIGNAL_LOOP_BATTERY      = 0x02,
    MIDWARE_V_SIGNAL_COIN_FIRST        = 0x03,
    MIDWARE_V_SIGNAL_DIAL_TONE_FIRST   = 0x04,
    MIDWARE_V_SIGNAL_MULTI_PART        = 0x05    
    
}MIDWARE_V_SIGNAL_CODE_E;

/*Structure of VoIP port configuration table*/
typedef struct  
{
    UINT8      line_id;
    UINT8      admin_state;
    UINT8      impedence;
    UINT8      tx_path;
    UINT8      rx_gain;
    UINT8      tx_gain;
    UINT8      operation_state;
    UINT8      hook_state;
    UINT16     holdover_time;    
    UINT16     signal_code;  

}MIDWARE_TABLE_V_PORT_CFG_T;

/* VoIP validation method */
typedef enum 
{
    MIDWARE_V_VALIDATE_SCHEME_NONE     = 0x00,
    MIDWARE_V_VALIDATE_SCHEME_MD5      = 0x01,
    MIDWARE_V_VALIDATE_SCHEME_RFC2617  = 0x03
    
}MIDWARE_V_VALIDATE_SCHEME_E;

/* URL format enum */
typedef enum 
{
    MIDWARE_V_URL_FORMAT_TEL     = 0x00,
    MIDWARE_V_URL_FORMAT_SIP     = 0x01
    
}MIDWARE_V_URL_FORMAT_E;

/* SIP status enum */
typedef enum 
{
    MIDWARE_V_SIP_STATUS_OK_INIT          = 0x00,
    MIDWARE_V_SIP_STATUS_OK_CONNECTED     = 0x01,
    MIDWARE_V_SIP_STATUS_FALI_ICMP        = 0x02,
    MIDWARE_V_SIP_STATUS_FALI_MALFORMED   = 0x03,
    MIDWARE_V_SIP_STATUS_FALI_INADEQUATE  = 0x04,
    MIDWARE_V_SIP_STATUS_FALI_TIMEOUT     = 0x05,
    MIDWARE_V_SIP_STATUS_FALI_OFFLINE     = 0x06
    
}MIDWARE_V_SIP_STATUS_E;

/*Structure of VoIP SIP agent table*/
typedef struct  
{
    UINT16     line_id;
    UINT16     agent_id;
    UINT8      proxy_serv_addr[MIDWARE_OMCI_URL_LEN_PL1];
    UINT8      backup_proxy_serv_addr[MIDWARE_OMCI_URL_LEN_PL1];
    UINT8      outbound_proxy_addr[MIDWARE_OMCI_URL_LEN_PL1];
    UINT8      host_part_url[MIDWARE_OMCI_URL_LEN_PL1];
    UINT32     reg_scheme;
    UINT8      reg_user[MIDWARE_USER_NAME_LEN_PL1];
    UINT8      reg_pwd[MIDWARE_DEFAULT_STRING_LEN_PL1];    
    UINT8      reg_realm[MIDWARE_DEFAULT_STRING_LEN_PL1];  
    UINT8      reg_addr[MIDWARE_OMCI_URL_LEN_PL1];     
    UINT8      backup_reg_addr[MIDWARE_OMCI_URL_LEN_PL1];   
    UINT32     primary_dns;
    UINT32     second_dns;    
    UINT16     l4_type;
    UINT16     l4_port;
    UINT32     dscp_val;
    UINT32     reg_expire_time;    
    UINT32     reg_head_start_time;
    UINT16     response_code;
    UINT16     response_tone; 
    UINT8      response_text[MIDWARE_USER_NAME_LEN_PL1];    
    UINT32     soft_switch;
    UINT8      optical_tx_ctrl;
    UINT8      url_format;
    UINT8      status;
    UINT8      heartbeat_switch;
    UINT16     heartbeat_cycle;
    UINT16     heartbeat_count;

}MIDWARE_TABLE_V_SIP_AGENT_T;

/*Structure of VoIP SIP user table*/
typedef struct  
{
    UINT32     line_id;
    UINT8      user_aor[MIDWARE_OMCI_URL_LEN_PL1];
    UINT8      display_name[MIDWARE_DEFAULT_STRING_LEN_PL1];   
    UINT32     auth_scheme;
    UINT8      auth_user[MIDWARE_USER_NAME_LEN_PL1];
    UINT8      auth_pwd[MIDWARE_DEFAULT_STRING_LEN_PL1];    
    UINT8      auth_realm[MIDWARE_DEFAULT_STRING_LEN_PL1];  
    UINT8      auth_addr[MIDWARE_OMCI_URL_LEN_PL1];   
    UINT8      voicemail_addr[MIDWARE_OMCI_URL_LEN_PL1];
    UINT32     voicemail_exp_time;
    UINT32     release_timer;
    UINT32     roh_timer;

}MIDWARE_TABLE_V_SIP_USER_T;

/*Structure of VoIP application service profile table*/
typedef struct  
{
    UINT32     line_id;
    UINT8      cid_feature[MIDWARE_OMCI_URL_LEN_PL1];
    UINT8      callwait_feature[MIDWARE_DEFAULT_STRING_LEN_PL1];
    UINT32     call_feature;    
    UINT8      callpresent_feature[MIDWARE_USER_NAME_LEN_PL1];
    UINT8      directconnet_feature[MIDWARE_DEFAULT_STRING_LEN_PL1]; 
    UINT32     dc_scheme;
    UINT8      dc_user[MIDWARE_USER_NAME_LEN_PL1];
    UINT8      dc_pwd[MIDWARE_DEFAULT_STRING_LEN_PL1];    
    UINT8      dc_realm[MIDWARE_DEFAULT_STRING_LEN_PL1];  
    UINT8      dc_addr[MIDWARE_OMCI_URL_LEN_PL1];     
    UINT32     bridge_agent_scheme;
    UINT8      bridge_agent_user[MIDWARE_USER_NAME_LEN_PL1];
    UINT8      bridge_agent_pwd[MIDWARE_DEFAULT_STRING_LEN_PL1];    
    UINT8      bridge_agent_realm[MIDWARE_DEFAULT_STRING_LEN_PL1];  
    UINT8      bridge_agent_addr[MIDWARE_OMCI_URL_LEN_PL1];         
    UINT32     conference_url_scheme;
    UINT8      conference_url_user[MIDWARE_USER_NAME_LEN_PL1];
    UINT8      conference_url_pwd[MIDWARE_DEFAULT_STRING_LEN_PL1];    
    UINT8      conference_url_realm[MIDWARE_DEFAULT_STRING_LEN_PL1];  
    UINT8      conference_url_addr[MIDWARE_OMCI_URL_LEN_PL1];  

}MIDWARE_TABLE_V_APP_SERV_PROFILE_T;

/*Structure of VoIP feature access code table*/
typedef struct  
{
    UINT32     line_id;
    UINT8      cancel_callwait[MIDWARE_ACCESS_NODE_LEN_PL1];
    UINT8      call_hold[MIDWARE_ACCESS_NODE_LEN_PL1];
    UINT8      call_park[MIDWARE_ACCESS_NODE_LEN_PL1];    
    UINT8      cids_activate[MIDWARE_ACCESS_NODE_LEN_PL1];
    UINT8      cids_deactivate[MIDWARE_ACCESS_NODE_LEN_PL1];
    UINT8      donotdisturb_activate[MIDWARE_ACCESS_NODE_LEN_PL1]; 
    UINT8      donotdisturb_deactivate[MIDWARE_ACCESS_NODE_LEN_PL1];
    UINT8      donotdisturb_pin_change[MIDWARE_ACCESS_NODE_LEN_PL1];
    UINT8      emergency_sn[MIDWARE_ACCESS_NODE_LEN_PL1]; 
    UINT8      intercom_service[MIDWARE_ACCESS_NODE_LEN_PL1];
    UINT8      blind_call_transfer[MIDWARE_ACCESS_NODE_LEN_PL1];
    UINT8      attend_call_transfer[MIDWARE_ACCESS_NODE_LEN_PL1]; 

}MIDWARE_TABLE_V_ACCESS_NODE_T;

/*Structure of VoIP network dial plan table*/
typedef struct  
{
    UINT16     line_id;
    UINT16     dial_plan_num;
    UINT16     dial_plan_table_size;
    UINT16     critical_dial_timeout;
    UINT16     partial_dial_timeout;
    UINT16     dial_plan_format; 
    UINT8      digit_map[MIDWARE_DIGIMAP_LEN_PL1];

}MIDWARE_TABLE_V_DIAL_PLAN_T;

/*Structure of VoIP media profile table*/
typedef struct  
{
    UINT8      line_id;
    UINT8      fax_mode;
    UINT8      fax_ctrl;
    UINT8      code_select_1;    
    UINT8      packet_select_1;
    UINT8      silence_sup_1;    
    UINT8      code_select_2;    
    UINT8      packet_select_2;
    UINT8      silence_sup_2;  
    UINT8      code_select_3;    
    UINT8      packet_select_3;
    UINT8      silence_sup_3;  
    UINT8      code_select_4;    
    UINT8      packet_select_4;
    UINT8      silence_sup_4;  
    UINT8      oob_dtmf;    

}MIDWARE_TABLE_V_MEDIA_PROFILE_T;

/*Structure of voice service profile table*/
typedef struct  
{
    UINT8      line_id;
    UINT8      announce_type;
    UINT16     jitter_target;
    UINT16     jitter_buf;    
    UINT8      echo_cancel_ind;
    UINT8      pstn_proto_var;    
    UINT16     dtmf_digit_level;    
    UINT16     dtmf_digit_duration;
    UINT16     hook_min_time;  
    UINT16     hook_max_time;        

}MIDWARE_TABLE_V_SERV_PROFILE_T;

/*Structure of tone pattern table*/
typedef struct  
{
    UINT16     line_id;
    UINT16     index;
    UINT16     entry_index;
    UINT16     tone_on;    
    UINT16     frequency_1;
    UINT16     power_1;    
    UINT16     frequency_2;
    UINT16     power_2; 
    UINT16     frequency_3;
    UINT16     power_3; 
    UINT16     frequency_4;
    UINT16     power_4; 
    UINT16     modul_frequency;
    UINT16     modul_power; 
    UINT16     duration;
    UINT16     next_entry;     

}MIDWARE_TABLE_V_TONE_PATTERN_T;

/*Structure of tone event table*/
typedef struct  
{
    UINT8      line_id;
    UINT8      index;
    UINT8      event;
    UINT8      tone_pattern;    
    UINT8      tone_file[MIDWARE_OMCI_URL_LEN_PL1];;
    UINT32     tone_file_repet;      

}MIDWARE_TABLE_V_TONE_EVENT_T;

/*Structure of ring pattern table*/
typedef struct  
{
    UINT8      line_id;
    UINT8      index;
    UINT8      entry_index;
    UINT8      ring_on;    
    UINT16     duration;
    UINT16     next_entry;    

}MIDWARE_TABLE_V_RING_PATTERN_T;

/*Structure of ring event table*/
typedef struct  
{
    UINT8      line_id;
    UINT8      index;
    UINT8      event;
    UINT8      ring_pattern;    
    UINT8      ring_file[MIDWARE_OMCI_URL_LEN_PL1];;
    UINT32     ring_file_repet;     
    UINT8      ring_text[MIDWARE_OMCI_URL_LEN_PL1];;       

}MIDWARE_TABLE_V_RING_EVENT_T;

/*Structure of RTP profile table*/
typedef struct  
{
    UINT16     line_id;
    UINT16     local_port_min;
    UINT16     local_port_max;    
    UINT16     dscp_mark;     
    UINT8      piggyback_event;
    UINT8      tone_event;
    UINT8      dtmf_event;
    UINT8      cas_event;    

}MIDWARE_TABLE_V_RTP_PROFILE_T;

/*Structure of line status table*/
typedef struct  
{
    UINT32     line_id;
    UINT32     port_status;
    UINT16     codec_used;    
    UINT16     server_status;     
    UINT32     port_session_type;
    UINT16     packet_period_call_1;
    UINT16     packet_period_call_2;
    UINT8      dst_addr_call_1[MIDWARE_DEFAULT_STRING_LEN_PL1];;
    UINT8      dst_addr_call_2[MIDWARE_DEFAULT_STRING_LEN_PL1];;   

}MIDWARE_TABLE_V_PORT_STATUS_T;

/* Enum to define whether middle ware DB is changed and need be saved */
typedef enum 
{
    MIDWARE_DB_STATUS_UNCHANGED      = 0x00,
    MIDWARE_DB_STATUS_CHANGED        = 0x01
    
}MIDWARE_DB_STATUS_E;

/*----------------------------------------------------------------------------*/
/* Function Declaration                                                       */
/*----------------------------------------------------------------------------*/

/*******************************************************************************
* midware_sqlite3_insert_entry()
*
* DESCRIPTION: Insert an entry to specific middle-ware table
*                   
* INPUTS:
*  table_id        - Middle-ware table ID.
*  entry_bitmap    - Used to idntify table items.
*  entry_context   - Input entry context, point to specific table structure.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns SFU_OK.
*
* COMMENTS:
*
*******************************************************************************/ 
ONU_STATUS  midware_insert_entry
(
    MIDWARE_TABLE_ID_E  table_id,
    VOID                *entry,
    UINT32              size
);

ONU_STATUS  midware_insert_group_entry
(
    MIDWARE_TABLE_ID_E table_id, 
    UINT32             entry_num, 
    VOID               *entry,
    UINT32             size
);

ONU_STATUS  midware_update_entry
(
    MIDWARE_TABLE_ID_E  table_id,
    UINT32              bitmap,     
    VOID                *entry,
    UINT32              size
);

ONU_STATUS  midware_remove_entry
(
    MIDWARE_TABLE_ID_E  table_id,
    VOID                *entry_context,
    UINT32              size
);

ONU_STATUS  midware_reset_table
(
    MIDWARE_TABLE_ID_E  table_id
);
    
ONU_STATUS midware_reset_table_data
(
    MIDWARE_TABLE_ID_E table_id
);

ONU_STATUS  midware_reset_db(VOID);

ONU_STATUS  midware_save_db(VOID);

ONU_STATUS  midware_get_entry
(
    MIDWARE_TABLE_ID_E  table_id,
    UINT32              bitmap,     
    VOID                *entry,
    UINT32              size
);

ONU_STATUS  midware_get_first_entry
(
    MIDWARE_TABLE_ID_E  table_id,
    UINT32              bitmap,
    VOID                *entry,
    UINT32              size
);

ONU_STATUS  midware_get_next_entry
(
    MIDWARE_TABLE_ID_E  table_id,
    VOID                *entry,
    UINT32              size
);

ONU_STATUS  midware_update_entry_asynchronous
(
    MIDWARE_TABLE_ID_E  table_id,
    UINT32              bitmap,     
    VOID                *entry,
    UINT32              size
);


#endif /*__MIDWARE_EXPO_H__*/
