/********************************************************************************/
/*                            middleware CLI functions                          */
/********************************************************************************/
{"midware_print_table_info",midware_cli_print_table_info},
{"midware_insert_entry",midware_cli_insert_entry},
{"midware_update_entry",midware_cli_update_entry},
{"midware_remove_entry",midware_cli_remove_entry},
{"midware_reset_table",midware_cli_reset_table},
{"midware_reset_table_data",midware_cli_reset_table_data},
{"midware_get_entry",midware_cli_get_entry},
{"midware_show_table_all_entry",midware_cli_show_table_all_entry},
{"midware_set_print_level",midware_cli_set_print_level},
{"midware_save_db",midware_cli_save_db},

