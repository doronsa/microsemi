/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.                    
*                                                                                
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED  
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD  
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS  
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".      
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS      
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY    
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),      
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL     
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).                               
******************************************************************************** 
*      tpm_cli.h                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY: ken                                                   
*                                                                                
* DATE CREATED: 
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.2 $                                                           
*******************************************************************************/


#ifndef __MIDWARE_CLI_H__
#define __MIDWARE_CLI_H__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

bool_t midware_cli_print_table_info(const clish_shell_t *this, const lub_argv_t *argv);
bool_t midware_cli_insert_entry(const clish_shell_t *this, const lub_argv_t *argv);
bool_t midware_cli_update_entry(const clish_shell_t *this, const lub_argv_t *argv);
bool_t midware_cli_remove_entry(const clish_shell_t *this, const lub_argv_t *argv);
bool_t midware_cli_reset_table(const clish_shell_t *this, const lub_argv_t *argv);
bool_t midware_cli_reset_table_data(const clish_shell_t *this, const lub_argv_t *argv);
bool_t midware_cli_get_entry(const clish_shell_t *this, const lub_argv_t *argv);
bool_t midware_cli_show_table_all_entry(const clish_shell_t *this, const lub_argv_t *argv);
bool_t midware_cli_set_print_level(const clish_shell_t *this, const lub_argv_t *argv);
bool_t midware_cli_save_db(const clish_shell_t *this, const lub_argv_t *argv);
#endif