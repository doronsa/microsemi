/*******************************************************************************
*               Copyright 2009, Marvell Technology Group Ltd.
*
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK), GALILEO TECHNOLOGY LTD. (GTL)
* GALILEO TECHNOLOGY, INC. (GTI) AND RADLAN Computer Communications, LTD.
********************************************************************************
* tpm_mod.c
*
* DESCRIPTION:
*
* DEPENDENCIES:
*               None
*
* CREATED BY:   JingHua
*
* DATE CREATED:
*
* FILE REVISION NUMBER:
*               $Revision: 1.51 $
*
*
*******************************************************************************/
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/if_ether.h>
#include <net/if.h>
#include <linux/sockios.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/route.h>

#include <sys/time.h>
#include <stdio.h>
#include <memory.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "sqlite3.h"
#include "globals.h"

#include "midware_expo.h"
#include "midware_api.h"
#include "midware_sql_translate.h"
#include "midware_table_callback.h"
#include "midware_table_def.h"

#include "ponOnuMngIf.h"

#include "tpm_types.h"
#include "tpm_api.h"
#include "rstp_api.h"
#include "voip_api.h"
#include "igmp_api.h"
//#include "oam_stack_comm.h"
//#include "oam_stack_ossl.h"
#include "oam_stack_type_expo.h"
//#include "oam_stack_alarm.h"
//#include "oam_tl_alarm.h"
#include "oam_ret_val.h"

#include "FlashExports.h"
#include "I2cMain.h"
#include "apm_types.h"
#include "apm_alarm_api.h"
#include "apm_pm_api.h"
#include "apm_avc_api.h"
#include "OmciExtern.h"





typedef enum{
    MDW_MC_VLAN_UPDATE = 0,
    MDW_MC_VLAN_INSETT,
    MDW_MC_VLAN_REMOVE
} MIDWARE_MC_VLAN_OPERATION;


//typedef UINT32 STATUS;
UINT32 midware_onu_up_time = 0;
INT32 is_support_onu_xvr_I2c = 0;
const char* network_if_name[]=
{
    "pon0",
    "eth0"
};

static char *configuration_src    = "/etc/xml_params/sipapp.xml";
static char *configuration_dst    = "/etc/xml_params/sipapp.xml";
static char *sip_application      = "sipapp";
static char *sip_application_path = "/usr/bin/";

extern tpm_init_pon_type_t wanTech;

extern MIDWARE_TABLE_INFO *midware_get_table_info(MIDWARE_TABLE_ID_E table_id);
extern UINT32 midware_sqlite3_get_entry_size(MIDWARE_TABLE_INFO *table_info);
extern UINT32 midware_sqlite3_get_entry_count(MIDWARE_TABLE_ID_E table_id);
extern ONU_STATUS midware_sqlite3_get_insert_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry);
extern ONU_STATUS midware_sqlite3_get_select_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry);

extern void  midware_sqlite3_print_entry(MIDWARE_TABLE_ID_E table_id, void *entry);
static BOOL readImageVersion(char *version, int length);

extern bool omci_report_alarm(OMCI_REPORT_ALARM_MSG_S *alarmMsg);
extern bool omci_report_avc(OMCI_REPORT_AVC_MSG_S *avcMsg);
extern bool omci_report_pm(OMCI_REPORT_PM_MSG_S *pmMsg);

extern STATUS oam_tl_alarm_generate(UINT32 alarm_type, UINT32 param1, UINT32 param2, OAM_ALARM_REPORT_STATE_E state,UINT32 alarm_val);
extern STATUS oam_tl_avc_generate(UINT32 avc_type, UINT32 param1, UINT32 param2, UINT32 avc_val);


static void  midware_Synchronize_TCA();

#if 0
STATUS oam_alarm_generate(UINT32 alarm_type, UINT32 param1, UINT32 param2, OAM_ALARM_REPORT_STATE_E state,UINT32 alarm_val)
{
    return ONU_OK;
}

STATUS oam_avc_generate(UINT32 avc_type, UINT32 param1, UINT32 param2, UINT32 avc_val)
{
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"oam_avc_generate avc type 0x%x param1 0x%x param2 0x%x value %d\r\n", avc_type, param1, param2, avc_val);
    return ONU_OK;
}
#endif


/******************************************************************************
 *
 * Function   : midware_restart_mmp_app
 *
 * Description: This routine restarts the MMP application.
 *                1. It removes old result file
 *                2. It gets the PID of the MMP process via "ps" - it is placed
 *                   in the result file
 *                3. It takes the PID from the result file and kills process
 *                4. It restarts the MMP application
 *
 * Returns    : OMCIPROCSTATUS
 *
 ******************************************************************************/

static void midware_restart_mmp_app()
{
    char buf[120];

    sprintf(buf, "killall %s; usleep 500000; %s%s -f %s -d 1", sip_application, sip_application_path, sip_application, configuration_dst);
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO, "%s: 2 ***** %s ******\n\r", __FUNCTION__, buf);
    system(buf);
}

ONU_STATUS midware_table_restore(MIDWARE_TABLE_ID_E table_id)
{
    MIDWARE_TABLE_INFO *table_info;
    UINT32    entry_size;
    UINT32    entry_num;
    UINT8    *entry_p;
    UINT32    *bitmap_p;
    ONU_STATUS      rc;
    UINT8 col_num =0;
    UINT32 index_entry_num =0;
    UINT32  bitmap=0;

    /*
    restore all the entries in table
    1. get all the entries
    2. update all the entries
  */

    table_info = midware_get_table_info(table_id);

    entry_num = midware_sqlite3_get_entry_count(table_id);
    entry_size = midware_sqlite3_get_entry_size(table_info);

    entry_p = malloc(entry_num * entry_size);
    if(0 == entry_p)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't malloc mem for size: %ld\n", entry_num * entry_size);
        return ONU_FAIL;
    }

    bitmap_p = malloc(entry_num * sizeof(UINT32));
    if(0 == bitmap_p)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't malloc mem for size: %ld\n", entry_num * sizeof(UINT32));
        free(entry_p);
        return ONU_FAIL;
    }
    //memset(bitmap_p, 0xff, entry_num * sizeof(UINT32));


    for(col_num=0;col_num<table_info->col_num;col_num++)
    {
        if(table_info->col_info[col_num].save_to_flash)
        {
            bitmap |= 1<<col_num;
        }
    }

    for(index_entry_num=0;index_entry_num<entry_num;index_entry_num++)
    {
        *(bitmap_p+index_entry_num) = bitmap;
    }

    rc = midware_sqlite3_get_all_entry(table_id, &entry_num, entry_p);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get all entries for table: %d\n", table_id);
        free(entry_p);
        free(bitmap_p);
        return ONU_FAIL;
    }

    rc = midware_sqlite3_update_group_entry(table_id, entry_num, bitmap_p, entry_p);

    free(entry_p);
    free(bitmap_p);

    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't update all entries for table: %d\n", table_id);
        return ONU_FAIL;
    }

    return ONU_OK;
}

///////////////////////////////////////////////
//MIDWARE_TABLE_ONU_INFO
///////////////////////////////////////////////

ONU_STATUS midware_table_onu_info_init(void)
{
    MIDWARE_TABLE_ONU_INFO_T OnuInfo;
    ONU_STATUS rc;
    MIDWARE_TABLE_INFO *table_info;
    char *vendor_id = MIDWARE_SFU_VENDOR_ID;
    char *module_id = MIDWARE_SFU_MODULE_ID;
    char *hardware_version = MIDWARE_SFU_HARDWARE_VERSION;
    char *software_version = MIDWARE_SFU_SOFTWARE_VERSION;
    UINT8 firmware_version[MIDWARE_FW_VERSION_LEN] = MIDWARE_SFU_FIRMWARE_VERSION;
    char *chip_id = MIDWARE_SFU_CHIP_ID;
    UINT8 chip_version[MIDWARE_CHIP_VER_LEN] = MIDWARE_SFU_CHIP_VERSION;
    char *sn = MIDWARE_SFU_SN;
    struct timeval tv1;

    //get all the attr from XML
    //for now, there is no XML
    memset(&OnuInfo,0,sizeof(OnuInfo));
    memcpy(&OnuInfo.vendor_id, vendor_id, sizeof(OnuInfo.vendor_id));
    memcpy(&OnuInfo.onu_model, module_id, sizeof(OnuInfo.onu_model));
    memcpy(&OnuInfo.hw_ver, hardware_version, sizeof(OnuInfo.hw_ver));
    readImageVersion(OnuInfo.sw_ver,sizeof(OnuInfo.sw_ver)-1);
    if(strlen(OnuInfo.sw_ver) == 0)
    {
    memcpy(&OnuInfo.sw_ver, software_version, sizeof(OnuInfo.sw_ver));
    }
    memcpy(&OnuInfo.fw_ver, firmware_version, sizeof(OnuInfo.fw_ver));
    memcpy(&OnuInfo.chip_id, chip_id, sizeof(OnuInfo.chip_id));
    OnuInfo.chip_model = MIDWARE_SFU_CHIP_MODEL;
    OnuInfo.chip_rev = MIDWARE_SFU_CHIP_REV;
    memcpy(&OnuInfo.chip_ver, chip_version, sizeof(OnuInfo.chip_ver));
    memcpy(&OnuInfo.sn, sn, sizeof(OnuInfo.sn));

    OnuInfo.ge_port_num = MIDWARE_SFU_GE_PORT_NUM;
    OnuInfo.ge_port_bitmap = MIDWARE_SFU_GE_PORT_BITMAP;
    OnuInfo.fe_port_num = MIDWARE_SFU_FE_PORT_NUM;
    OnuInfo.fe_port_bitmap = MIDWARE_SFU_FE_PORT_BITMAP;
    OnuInfo.pots_port_num = MIDWARE_SFU_POTS_PORT_NUM;
    OnuInfo.e1_port_num = MIDWARE_SFU_E1_PORT_NUM;
    OnuInfo.wlan_port_num = MIDWARE_SFU_WLAN_PORT_NUM;
    OnuInfo.usb_port_num = MIDWARE_SFU_USB_PORT_NUM;
    OnuInfo.us_queue_num = MIDWARE_SFU_UPSTREAM_QUEUES_NUMBER;
    OnuInfo.us_queue_num_per_port = MIDWARE_SFU_UPSTREAM_QUEUES_PER_PORT;
    OnuInfo.ds_queue_num = MIDWARE_SFU_DOWNSTREAM_QUEUES_NUMBER;
    OnuInfo.ds_queue_num_per_port = MIDWARE_SFU_DOWNSTREAM_QUEUES_PER_PORT;
    OnuInfo.battery_backup = 0;
    OnuInfo.onu_type = 0;
    OnuInfo.llid_num = MIDWARE_SFU_LLID_NUM;
    OnuInfo.queue_num_per_llid = MIDWARE_SFU_QUEUE_NUM_PER_LLID;
    OnuInfo.ipv6_support = 0;
    OnuInfo.power_control = 0;

    gettimeofday(&tv1,0);
    OnuInfo.onu_up_time = tv1.tv_sec;
    midware_onu_up_time = tv1.tv_sec;

    //insert the entry into SQLite
    table_info = midware_get_table_info(MIDWARE_TABLE_ONU_INFO);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &OnuInfo);
     if(ONU_FAIL == rc)
     {
         MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entry\n");
         return ONU_FAIL;
     }

     return ONU_OK;
}
MIDWARE_CALLBACK_RET midware_table_onu_info_get(UINT32 bitmap, void *entry)
{
    struct timeval tv1;
    MIDWARE_TABLE_ONU_INFO_T *OnuInfo;

    OnuInfo = (MIDWARE_TABLE_ONU_INFO_T*)entry;

    if(bitmap & MIDWARE_ENTRY_ONU_UP_TIME)
    {
        gettimeofday(&tv1,0);
        /*updated by ken*/
        OnuInfo->onu_up_time = tv1.tv_sec - midware_onu_up_time;
    }

    return MDW_CB_RET_SET_HW_NO_NEED;
}


///////////////////////////////////////////////
//MIDWARE_TABLE_ONU_CFG
///////////////////////////////////////////////
ONU_STATUS midware_table_onu_cfg_init(void)
{
    MIDWARE_TABLE_ONU_CFG_T OnuCfg;
    ONU_STATUS rc;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_ONU_CFG))
    {
        //already got entries in the table,
        return ONU_OK;
    }

    //TBD
    memset(&OnuCfg, 0, sizeof(OnuCfg));
    OnuCfg.oam_stack_status = MIDWARE_SFU_CTC_STACK_ENABLE;
    OnuCfg.mtu_size         = MIDWARE_SFU_MTU_DEFAULT_SIZE;
    OnuCfg.pm_interval      = APM_DEFAULT_PM_INTERVAL_MS;
    OnuCfg.mac_age_time     = MIDWARE_SFU_AGE_TIME;
    //insert the entry into SQLite
    table_info = midware_get_table_info(MIDWARE_TABLE_ONU_CFG);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &OnuCfg);
     if(ONU_FAIL == rc)
     {
         MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entry\n");
         return ONU_FAIL;
     }

     return ONU_OK;
}

MIDWARE_CALLBACK_RET midware_table_onu_cfg_get(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_ONU_CFG_T         *OnuCfg;
    UINT32                                                        mac_aging_time;
    tpm_error_code_t                                        tpm_rc = ERR_GENERAL;
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;

    OnuCfg = (MIDWARE_TABLE_ONU_CFG_T *)entry;

    if(bitmap & MIDWARE_ENTRY_MAC_AGE_TIME)
    {
        tpm_rc = tpm_sw_get_mac_age_time(0, (uint32_t *)&mac_aging_time);
        if (tpm_rc != TPM_RC_OK)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get MAC aging time\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        OnuCfg->mac_age_time = mac_aging_time;
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

     return callback_ret;
}


MIDWARE_CALLBACK_RET midware_table_onu_cfg_set(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_ONU_CFG_T         *OnuCfg;
    tpm_error_code_t                                        tpm_rc = ERR_GENERAL;

    OnuCfg = (MIDWARE_TABLE_ONU_CFG_T *)entry;

    if(      (bitmap & MIDWARE_ENTRY_RESET_ONU)
        &&(1 == OnuCfg->reset_onu))
    {
        system("reboot");
    }
    if(bitmap & MIDWARE_ENTRY_MAC_AGE_TIME)
    {
        tpm_rc = tpm_sw_set_mac_age_time(0, OnuCfg->mac_age_time);
        if (tpm_rc != TPM_RC_OK)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set MAC aging time\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
    }
    if(bitmap & MIDWARE_ENTRY_MTU_SIZE)
    {
        tpm_rc = tpm_set_mtu_size(0, TPM_NETA_MTU_SWITCH, OnuCfg->mtu_size);
        /* Do not check the return value in case it will fali for MC*/
        /*
        if (tpm_rc != TPM_RC_OK)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set Switch MTU size\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }*/
        tpm_rc = tpm_set_mtu_size(0, TPM_NETA_MTU_GMAC0, OnuCfg->mtu_size);
        if (tpm_rc != TPM_RC_OK)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set GMAC0 MTU size\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        tpm_rc = tpm_set_mtu_size(0, TPM_NETA_MTU_PONMAC, OnuCfg->mtu_size);
        if (tpm_rc != TPM_RC_OK)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set PON MAC MTU size\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
    }

    if(bitmap & MIDWARE_ENTRY_SYNCHRONIZE_TIME)
    {
        /*ken need to be update*/
        if(apm_pm_synchronize())
        {
            midware_Synchronize_TCA();
        }
        else
        {
            return MDW_CB_RET_SET_HW_FAIL;
        }
    }

    if(bitmap & MIDWARE_ENTRY_MIB_RESET)
    {
        /*ken need to be update*/
        if(!apm_alarm_db_obj_reset() || !apm_pm_db_obj_reset() || !apm_avc_db_obj_reset())
        {
            return MDW_CB_RET_SET_HW_FAIL;
        }

        if(ONU_OK != midware_sqlite3_reset_table_without_callback(MIDWARE_TABLE_ALARM))
        {
            return MDW_CB_RET_SET_HW_FAIL;
        }
        if(ONU_OK != midware_sqlite3_reset_table_without_callback(MIDWARE_TABLE_PM))
        {
            return MDW_CB_RET_SET_HW_FAIL;
        }
        if(ONU_OK != midware_sqlite3_reset_table_without_callback(MIDWARE_TABLE_AVC))
        {
            return MDW_CB_RET_SET_HW_FAIL;
        }

    }

    if(bitmap & MIDWARE_ENTRY_PM_INTERVAL)
    {
#if 0
        /*ken need to be update*/
        if(!apm_pm_set_interval(OnuCfg->pm_interval))
        {
            return MDW_CB_RET_SET_HW_FAIL;
        }
#endif
    }

     return MDW_CB_RET_SET_HW_OK;
}

int  midware_mv_ext_get_if_mac(const char *ifName, UINT8 *pMac)
{
    struct ifreq req;
    int err;

     if (NULL == pMac)
     {
         return ERROR;
     }

    strcpy(req.ifr_name, ifName);
    int s=socket(AF_INET, SOCK_DGRAM, 0);
    err = ioctl(s,SIOCGIFHWADDR, &req);
    close(s);

    memset(pMac, 0 , 6*sizeof(char));
    if (err!=-1)
         memcpy(pMac, req.ifr_hwaddr.sa_data, ETH_ALEN);

       return 0 ;
}

int  midware_mv_ext_get_if_addr(const char *ifName, UINT8 *ipAddr)
{
    struct ifreq req;
    int err;
    struct sockaddr_in *myaddr;

     if (NULL == ipAddr)
     {
         return ERROR;
     }

    strcpy(req.ifr_name, ifName);
    int s=socket(AF_INET, SOCK_DGRAM, 0);
    err = ioctl(s,SIOCGIFADDR, &req);
    close(s);

    myaddr = (struct sockaddr_in *)&(req.ifr_addr);

    memset(ipAddr, 0 , MIDWARE_IPV4_IP_LEN*sizeof(char));

    if (err!=-1)
    {
        ipAddr[0] = (ntohl(myaddr->sin_addr.s_addr)&0xff000000)>>24;
        ipAddr[1] = (ntohl(myaddr->sin_addr.s_addr)&0x00ff0000)>>16;
        ipAddr[2] = (ntohl(myaddr->sin_addr.s_addr)&0x0000ff00)>>8;
        ipAddr[3] = (ntohl(myaddr->sin_addr.s_addr)&0x0000000ff);
    }

    return 0 ;
}

int  midware_mv_ext_get_if_mask(const char *ifname, UINT8 *mask)
{
    struct ifreq req;
    int err;
    struct sockaddr_in *myaddr;

     if (NULL == mask)
     {
         return ERROR;
     }

    strcpy(req.ifr_name, ifname);
    int s=socket(AF_INET, SOCK_DGRAM, 0);
    err = ioctl(s,SIOCGIFNETMASK, &req);
    close(s);

    myaddr = (struct sockaddr_in *)&(req.ifr_netmask);

    memset(mask, 0 , MIDWARE_IPV4_IP_LEN*sizeof(char));
    if (err!=-1)
    {
        mask[0] = (ntohl(myaddr->sin_addr.s_addr)&0xff000000)>>24;
        mask[1] = (ntohl(myaddr->sin_addr.s_addr)&0x00ff0000)>>16;
        mask[2] = (ntohl(myaddr->sin_addr.s_addr)&0x0000ff00)>>8;
        mask[3] = (ntohl(myaddr->sin_addr.s_addr)&0x000000ff);
    }

    return 0 ;
}

int midware_mv_ext_get_if_gateway(const char *ifname,UINT8 *gateway)
{
    FILE *fp;
    char buf[256]; // 128 is enough for linux
    char iface[16];
    unsigned long dest_addr, gate_addr;

    memset(gateway,0,MIDWARE_IPV4_IP_LEN*sizeof(char));

    fp = fopen("/proc/net/route", "r");
    if (fp == NULL)
    return -1;
    /* Skip title line */
    fgets(buf, sizeof(buf), fp);
    while (fgets(buf, sizeof(buf), fp))
    {
        if (sscanf(buf, "%s\t%lX\t%lX", iface, &dest_addr, &gate_addr) != 3)
        {
            continue;
        }

        if(0!=strcmp(iface,ifname))
        {
            continue;
        }

        gateway[0] = (ntohl(gate_addr)&0xff000000)>>24;
        gateway[1] = (ntohl(gate_addr)&0x00ff0000)>>16;
        gateway[2] = (ntohl(gate_addr)&0x0000ff00)>>8;
        gateway[3] = (ntohl(gate_addr)&0x000000ff);

        break;
    }

    fclose(fp);
    return 0;
}

int midware_mv_ext_set_if_addr(const char *ifname, UINT8 *Ipaddr)
{
    int fd;
    struct ifreq ifr;
    struct sockaddr_in *sin;


    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(fd < 0)
    {
        perror("socket   error");
        return -1;
    }

    memset(&ifr,0,sizeof(ifr));
    strcpy(ifr.ifr_name,ifname);
    sin = (struct sockaddr_in*)&ifr.ifr_addr;
    sin->sin_family = AF_INET;

    //ipaddr
    sin->sin_addr.s_addr = htonl((Ipaddr[0]<<24)+(Ipaddr[1]<<16)+(Ipaddr[2]<<8)+(Ipaddr[3]));

    if(ioctl(fd,SIOCSIFADDR,&ifr) < 0)
    {
        perror("ioctl   SIOCSIFADDR   error");
        return -3;
    }

    close(fd);
    return OK;
}

int midware_mv_ext_set_if_mask(const char *ifname, UINT8 *mask)
{
    int fd;
    struct ifreq ifr;
    struct sockaddr_in *sin;


    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(fd < 0)
    {
        perror("socket   error");
        return -1;
    }

    memset(&ifr,0,sizeof(ifr));
    strcpy(ifr.ifr_name,ifname);
    sin = (struct sockaddr_in*)&ifr.ifr_addr;
    sin->sin_family = AF_INET;

    //netmask
    sin->sin_addr.s_addr = htonl((mask[0]<<24)+(mask[1]<<16)+(mask[2]<<8)+(mask[3]));

    if(ioctl(fd, SIOCSIFNETMASK, &ifr) < 0)
    {
        perror("ioctl");
        return -5;
    }

    close(fd);
    return OK;
}

int midware_mv_ext_set_if_gateway(const char *ifname, UINT8 *gateway)
{
    int fd;
    struct rtentry  rt;
    struct sockaddr_in sin;


    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(fd < 0)
    {
        perror("socket   error");
        return -1;
    }

    sin.sin_family = AF_INET;


    //gateway
    memset(&rt, 0, sizeof(struct rtentry));
    memset(&sin, 0, sizeof(struct sockaddr_in));
    sin.sin_family = AF_INET;
    sin.sin_port = 0;

    sin.sin_addr.s_addr = htonl((gateway[0]<<24)+(gateway[1]<<16)+(gateway[2]<<8)+(gateway[3]));

    memcpy ( &rt.rt_gateway, &sin, sizeof(struct sockaddr_in));
    ((struct sockaddr_in *)&rt.rt_dst)->sin_family=AF_INET;
    ((struct sockaddr_in *)&rt.rt_genmask)->sin_family=AF_INET;
    rt.rt_flags = RTF_GATEWAY;

    if (ioctl(fd, SIOCADDRT, &rt)<0)
    {
        perror( "ioctl(SIOCADDRT) error in set_default_route\n");
        close(fd);
        return -1;
    }

    close(fd);
    return OK;
}

int midware_mv_ext_del_if_gateway(const char *ifname, UINT8 *gateway)
{
    int fd;
    struct rtentry  rt;
    struct sockaddr_in sin;


    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(fd < 0)
    {
        perror("socket   error");
        return -1;
    }

    sin.sin_family = AF_INET;


    //gateway
    memset(&rt, 0, sizeof(struct rtentry));
    memset(&sin, 0, sizeof(struct sockaddr_in));
    sin.sin_family = AF_INET;
    sin.sin_port = 0;

    sin.sin_addr.s_addr = htonl((gateway[0]<<24)+(gateway[1]<<16)+(gateway[2]<<8)+(gateway[3]));

    memcpy ( &rt.rt_gateway, &sin, sizeof(struct sockaddr_in));
    ((struct sockaddr_in *)&rt.rt_dst)->sin_family=AF_INET;
    ((struct sockaddr_in *)&rt.rt_genmask)->sin_family=AF_INET;
    rt.rt_flags = RTF_GATEWAY;

    if (ioctl(fd, SIOCDELRT, &rt)<0)
    {
        perror( "ioctl(SIOCADDRT) error in set_default_route\n");
        close(fd);
        return -1;
    }

    close(fd);
    return OK;
}


ONU_STATUS midware_table_onu_ip_init(void)
{
    MIDWARE_TABLE_ONU_IP_T OnuIP;
    ONU_STATUS rc;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_ONU_IP))
    {
        //already got entries in the table,
        return ONU_OK;
    }

    #if 0
    //pon0
    memset(&OnuIP, 0, sizeof(OnuIP));
    OnuIP.entity_id = 1;
    OnuIP.entity_type = MIDWARE_IP_TYPE_WAN_HOST;
    midware_mv_ext_get_if_mac("pon0",OnuIP.mac);
    midware_mv_ext_get_if_addr("pon0",OnuIP.cur_ip);
    midware_mv_ext_get_if_mask("pon0",OnuIP.cur_netmask);
    midware_mv_ext_get_if_gateway("pon0",OnuIP.cur_gateway);

    //insert the entry into SQLite
    table_info = midware_get_table_info(MIDWARE_TABLE_ONU_IP);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &OnuIP);
     if(ONU_FAIL == rc)
     {
         MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entry\n");
         return ONU_FAIL;
     }
    #endif

    //pon0
    memset(&OnuIP, 0, sizeof(OnuIP));

    OnuIP.entity_id = MIDWARE_IP_PON0;
    OnuIP.entity_type = MIDWARE_IP_TYPE_WAN_HOST;
    midware_mv_ext_get_if_mac("pon0",OnuIP.mac);
    midware_mv_ext_get_if_addr("pon0",OnuIP.cur_ip);
    midware_mv_ext_get_if_mask("pon0",OnuIP.cur_netmask);
    midware_mv_ext_get_if_gateway("pon0",OnuIP.cur_gateway);

    //insert the entry into SQLite
    table_info = midware_get_table_info(MIDWARE_TABLE_ONU_IP);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &OnuIP);
    if(ONU_FAIL == rc)
    {
     MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entry\n");
     return ONU_FAIL;
    }


    //eth0
    memset(&OnuIP, 0, sizeof(OnuIP));

    OnuIP.entity_id = MIDWARE_IP_ETH0;
    OnuIP.entity_type = MIDWARE_IP_TYPE_LAN_HOST;
    midware_mv_ext_get_if_mac("eth0",OnuIP.mac);
    midware_mv_ext_get_if_addr("eth0",OnuIP.cur_ip);
    midware_mv_ext_get_if_mask("eth0",OnuIP.cur_netmask);
    midware_mv_ext_get_if_gateway("eth0",OnuIP.cur_gateway);

    //insert the entry into SQLite
    table_info = midware_get_table_info(MIDWARE_TABLE_ONU_IP);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &OnuIP);
    if(ONU_FAIL == rc)
    {
     MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entry\n");
     return ONU_FAIL;
    }

    return ONU_OK;
}

MIDWARE_CALLBACK_RET midware_table_onu_ip_get(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_ONU_IP_T              *OnuIP;
    INT32                               rc;
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    UINT32 entity_id = 0;


    OnuIP = (MIDWARE_TABLE_ONU_IP_T *)entry;

    entity_id = OnuIP->entity_id;
    if(entity_id == MIDWARE_IP_ETH0)
    {
        entity_id = sizeof(network_if_name)/sizeof(const char*) - 1;
    }

    if(bitmap & MIDWARE_ENTRY_MAC)
    {
        rc = midware_mv_ext_get_if_mac(network_if_name[entity_id],OnuIP->mac);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to midware_mv_ext_get_if_mac\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_CUR_IP)
    {
        rc = midware_mv_ext_get_if_addr(network_if_name[entity_id],OnuIP->cur_ip);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to midware_mv_ext_get_if_addr\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_CUR_NETMASK)
    {
        rc = midware_mv_ext_get_if_mask(network_if_name[entity_id],OnuIP->cur_netmask);
        if (OK != rc)
        {
         MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to midware_mv_ext_get_if_mask\r\n");
         return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_CUR_GATEWAY)
    {
        rc = midware_mv_ext_get_if_gateway(network_if_name[entity_id],OnuIP->cur_gateway);
        if (OK != rc)
        {
         MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to midware_mv_ext_get_if_gateway\r\n");
         return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_onu_ip_set(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_ONU_IP_T              *OnuIP;
    INT32                               rc;
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    UINT8                               gateway[MIDWARE_IPV4_IP_LEN];
    UINT32 entity_id = 0;

    OnuIP = (MIDWARE_TABLE_ONU_IP_T *)entry;
    entity_id = OnuIP->entity_id;
    if(entity_id == MIDWARE_IP_ETH0)
    {
        entity_id = sizeof(network_if_name)/sizeof(const char*) - 1;
    }

    if(bitmap & MIDWARE_ENTRY_IP)
    {
        rc = midware_mv_ext_set_if_addr(network_if_name[entity_id], OnuIP->ip);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to midware_mv_ext_set_if_addr\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_NETMASK)
    {
        rc = midware_mv_ext_set_if_mask(network_if_name[entity_id], OnuIP->netmask);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to midware_mv_ext_set_if_mask\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

     if(bitmap & MIDWARE_ENTRY_GATEWAY)
     {
        if(0==*(UINT32 *)(OnuIP->gateway))
        {
            do
            {
                *(UINT32 *)(OnuIP->gateway)=0;
                midware_mv_ext_get_if_gateway(network_if_name[entity_id],OnuIP->gateway);
                if(0==*(UINT32 *)(OnuIP->gateway))
                {
                    break;
                }
                else
                {
                    midware_mv_ext_del_if_gateway(network_if_name[entity_id],OnuIP->gateway);
                }

            }while(0!=*(UINT32 *)(OnuIP->gateway));
        }
        else
        {
            do
            {
                *(UINT32 *)(gateway)=0;
                midware_mv_ext_get_if_gateway(network_if_name[entity_id],gateway);
                if(0==*(UINT32 *)(gateway))
                {
                    break;
                }
                else
                {
                    midware_mv_ext_del_if_gateway(network_if_name[entity_id],gateway);
                }

            }while(0!=*(UINT32 *)(gateway));

            rc = midware_mv_ext_set_if_gateway(network_if_name[entity_id], OnuIP->gateway);
            if (OK != rc)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to midware_mv_ext_set_if_gateway\r\n");
                return MDW_CB_RET_SET_HW_FAIL;
            }
            }
            callback_ret = MDW_CB_RET_SET_HW_OK;

    }

     return callback_ret;
}


///////////////////////////////////////////////
//MIDWARE_TABLE_UNI_CFG
///////////////////////////////////////////////
ONU_STATUS midware_table_uni_cfg_init(void)
{
    ONU_STATUS      rc=ONU_OK;
    MIDWARE_TABLE_UNI_CFG_T     uni_cfg;
    UINT32             i;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_UNI_CFG))
    {
        //already got entries in the table, restore
        rc = midware_table_restore(MIDWARE_TABLE_UNI_CFG);
        if (ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to restore MIDWARE_TABLE_UNI_CFG\r\n");
            return ONU_FAIL;
        }

        return ONU_OK;
    }

    //init the default entries
    memset(&uni_cfg, 0, sizeof(uni_cfg));
    uni_cfg.port_id = TPM_SRC_PORT_UNI_0;
    uni_cfg.port_type = MIDWARE_UNI_TYPE_FE;
    uni_cfg.port_admin = MIDWARE_SFU_ENABLE;
    uni_cfg.autoneg_cfg = MIDWARE_SFU_AUTONEG;
    uni_cfg.autoneg_mode = TPM_SPEED_AUTO_DUPLEX_AUTO;
    uni_cfg.reset_autoneg = MIDWARE_SFU_DISABLE;
    /*uni_cfg.speed_cfg = MIDWARE_SFU_SPEED_100;*/
    uni_cfg.speed_state = MIDWARE_SFU_SPEED_100;
    /*uni_cfg.duplex_cfg = MIDWARE_SFU_MODE_FULL;*/
    uni_cfg.duplex_state = MIDWARE_SFU_MODE_FULL;
    uni_cfg.loopback = MIDWARE_SFU_DISABLE;
    //uni_cfg.port_isolate = 0xff-(1<<(uni_cfg.port_id-1));
    uni_cfg.port_flood = 0x00;
    uni_cfg.mac_learn_en = MIDWARE_SFU_ENABLE;
    uni_cfg.default_vid = MIDWARE_SFU_DEF_VLAN;
    uni_cfg.default_pri = MIDWARE_SFU_DEF_PRI;
    uni_cfg.discard_tag = MIDWARE_SFU_DISABLE;
    uni_cfg.discard_untag = MIDWARE_SFU_DISABLE;
    uni_cfg.vlan_filter = MIDWARE_SFU_DISABLE;
    uni_cfg.loop_detect = MIDWARE_SFU_DISABLE;
    uni_cfg.in_mirror_en = MIDWARE_SFU_DISABLE;
    uni_cfg.in_mirror_dst = TPM_SRC_PORT_UNI_0;
    uni_cfg.out_mirror_en = MIDWARE_SFU_DISABLE;
    uni_cfg.out_mirror_dst = TPM_SRC_PORT_UNI_0;
    uni_cfg.stp_state = MIDWARE_PORT_FORWARDING;

    table_info = midware_get_table_info(MIDWARE_TABLE_UNI_CFG);
    for(i = TPM_SRC_PORT_UNI_0; i <= TPM_SRC_PORT_UNI_3; i++)
    {
        uni_cfg.port_id = i;
        uni_cfg.in_mirror_src = i;
        uni_cfg.out_mirror_src = i;
        uni_cfg.port_isolate = 0xfff-(1<<i);

        if(MIDWARE_UNI_TYPE_GE == midware_get_uni_port_type(i))
        {
            uni_cfg.port_type = MIDWARE_UNI_TYPE_GE;
            uni_cfg.speed_state = MIDWARE_SFU_SPEED_1000;
        }
        else
        {
            uni_cfg.port_type   = MIDWARE_UNI_TYPE_FE;
            uni_cfg.speed_state = MIDWARE_SFU_SPEED_100;

        }

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &uni_cfg);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for port: %ld\n", i);
            return ONU_FAIL;
        }

    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_uni_cfg_get(UINT32 bitmap, void *entry)
{
  MIDWARE_TABLE_UNI_CFG_T     *uni_cfg;
  tpm_error_code_t                      tpm_rc = ERR_GENERAL;
  MIDWARE_CALLBACK_RET        callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
  //bool         phy_port_state;
  bool         port_link_status;
  tpm_src_port_type_t   port_id;
  //tpm_src_port_type_t   dst_port_id;
  //tpm_src_port_type_t   src_port_id;
  //tpm_autoneg_mode_t  configurationInd;
  bool         autoneg_status;
  tpm_phy_speed_t       speed;
  bool         duplex_enable;
  //tpm_phy_loopback_mode_t loopback_mode;
  //bool   loopback_enable;
  //UINT32                    eth_port_vector;
  //bool                          flood_enable;

  uni_cfg = (MIDWARE_TABLE_UNI_CFG_T *)entry;

  //TBD
  //here maybe we need to adapt the midware portid to tpm portid
  port_id = uni_cfg->port_id;


  #if 0
  if(bitmap & MIDWARE_ENTRY_PORT_ADMIN)
  {
      tpm_rc = tpm_phy_get_port_admin_state(0, port_id, &phy_port_state);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_admin_state, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      if (phy_port_state)
      {
          uni_cfg->port_admin = 1;
      }
      else
      {
          uni_cfg->port_admin = 0;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }
  #endif

  if(bitmap & MIDWARE_ENTRY_PORT_LINK_STATE)
  {
      tpm_rc = tpm_phy_get_port_link_status(0, port_id, &port_link_status);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_link_state, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      if (port_link_status)
      {
          uni_cfg->link_state = 1;
      }
      else
      {
          uni_cfg->link_state = 0;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  /*
  if(    (bitmap & MIDWARE_ENTRY_PORT_AUTONEG_CFG)
       || (bitmap & MIDWARE_ENTRY_PORT_AUTONEG_STATE))
  */
  /*
  if(bitmap & MIDWARE_ENTRY_PORT_CONFIGURATION_IND)
  {
      tpm_rc = tpm_phy_get_port_autoneg_mode(0, port_id, &autoneg_status, &configurationInd);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_admin_state, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }

      uni_cfg->configurationInd = configurationInd;
      callback_ret = MDW_CB_RET_SET_HW_OK;
      }
      */
  #if 0
  if(bitmap & MIDWARE_ENTRY_PORT_SPEED_CFG)
  {
      tpm_rc = tpm_phy_get_port_speed(0, port_id, &speed);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_admin_state, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      uni_cfg->speed_cfg = speed;
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }
  #endif

  if(bitmap & MIDWARE_ENTRY_PORT_SPEED_STATE)
  {
      tpm_rc = tpm_phy_get_port_speed_mode(0, port_id, &speed);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_admin_state, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      uni_cfg->speed_state = speed;
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  #if 0
  if(bitmap & MIDWARE_ENTRY_PORT_DUPLEX_CFG)
  {
      tpm_rc = tpm_phy_get_port_duplex_mode(0, port_id, &duplex_enable);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_admin_state, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      if (duplex_enable)
      {
          uni_cfg->duplex_cfg = 1;
      }
      else
      {
          uni_cfg->duplex_cfg = 0;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }
  #endif

  if(bitmap & MIDWARE_ENTRY_PORT_DUPLEX_STATE)
  {
      tpm_rc = tpm_phy_get_port_duplex_status(0, port_id, &duplex_enable);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_admin_state, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      if (duplex_enable)
      {
          uni_cfg->duplex_state = 1;
      }
      else
      {
          uni_cfg->duplex_state = 0;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  #if 0
  if(bitmap & MIDWARE_ENTRY_PORT_LOOPBACK_CFG)
  {
      tpm_rc = tpm_phy_get_port_loopback(0, port_id, loopback_mode, &loopback_enable);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_loopback, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      if (loopback_enable)
      {
          uni_cfg->loopback = 1;
      }
      else
      {
          uni_cfg->loopback = 0;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(bitmap & MIDWARE_ENTRY_PORT_ISOLATE_CFG)
  {
      tpm_rc = tpm_sw_get_isolate_eth_port_vector(0, port_id, &eth_port_vector);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_loopback, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      uni_cfg->port_isolate = eth_port_vector;
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(bitmap & MIDWARE_ENTRY_PORT_FLOOD_CFG)
  {
      tpm_rc = tpm_sw_get_port_flooding(0, port_id, TPM_FLOOD_UNKNOWN_UNICAST, &flood_enable);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_flooding, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      if (flood_enable)
      {
          uni_cfg->port_flood = 1;
      }
      else
      {
          uni_cfg->port_flood = 0;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(bitmap & MIDWARE_ENTRY_PORT_MAC_LEARN)
  {
      tpm_rc = tpm_sw_get_mac_learn(0, port_id, &flood_enable);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get mac_learn, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      if (flood_enable)
      {
          uni_cfg->mac_learn_en = 1;
      }
      else
      {
          uni_cfg->mac_learn_en = 0;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(    (bitmap & MIDWARE_ENTRY_PORT_IN_MIRROR_DST)
       || (bitmap & MIDWARE_ENTRY_PORT_IN_MIRROR_EN)
       || (bitmap & MIDWARE_ENTRY_PORT_IN_MIRROR_SRC))
  {
      //TBD
      //here maybe we need to adapt the midware portid to tpm portid
      dst_port_id = uni_cfg->in_mirror_dst;
      src_port_id = uni_cfg->in_mirror_src;
      tpm_rc = tpm_sw_get_port_mirror(0, src_port_id, dst_port_id, TPM_SW_MIRROR_INGRESS, &flood_enable);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_mirror, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      if (flood_enable)
      {
          uni_cfg->in_mirror_en = 1;
      }
      else
      {
          uni_cfg->in_mirror_en = 0;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(    (bitmap & MIDWARE_ENTRY_PORT_OUT_MIRROR_DST)
       || (bitmap & MIDWARE_ENTRY_PORT_OUT_MIRROR_EN)
       || (bitmap & MIDWARE_ENTRY_PORT_OUT_MIRROR_SRC))
  {
      //TBD
      //here maybe we need to adapt the midware portid to tpm portid
      dst_port_id = uni_cfg->out_mirror_dst;
      src_port_id = uni_cfg->out_mirror_dst;
      tpm_rc = tpm_sw_get_port_mirror(0, src_port_id, dst_port_id, TPM_SW_MIRROR_EGRESS, &flood_enable);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_mirror, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      if (flood_enable)
      {
          uni_cfg->out_mirror_en = 1;
      }
      else
      {
          uni_cfg->out_mirror_en = 0;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }
  #endif

   return callback_ret;
}


MIDWARE_CALLBACK_RET midware_table_uni_cfg_set(UINT32 bitmap, void *entry)
{
  MIDWARE_TABLE_UNI_CFG_T *uni_cfg;
  tpm_error_code_t         tpm_rc = ERR_GENERAL;
  MIDWARE_CALLBACK_RET     callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
  tpm_src_port_type_t      port_id;
  tpm_src_port_type_t      dst_port_id;
  tpm_src_port_type_t      src_port_id;
  tpm_phy_speed_t          speed;

  uni_cfg = (MIDWARE_TABLE_UNI_CFG_T *)entry;

  //TBD
  //here maybe we need to adapt the midware portid to tpm portid
  port_id = uni_cfg->port_id;

  if(bitmap & MIDWARE_ENTRY_PORT_ADMIN)
  {
      tpm_rc = tpm_phy_set_port_admin_state(0, port_id, MIDWARE_U8_TO_BOOL(uni_cfg->port_admin));
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set port_admin_state, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(bitmap & MIDWARE_ENTRY_PORT_AUTONEG_CFG)
  {
      tpm_rc = tpm_phy_set_port_autoneg_mode(0, port_id, uni_cfg->autoneg_cfg, TPM_SPEED_AUTO_DUPLEX_AUTO);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set MIDWARE_ENTRY_PORT_AUTONEG_CFG, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(bitmap & MIDWARE_ENTRY_PORT_AUTONEG_MODE)
  {
      tpm_rc = tpm_phy_set_port_autoneg_mode(0, port_id, MIDWARE_SFU_AUTONEG, uni_cfg->autoneg_mode);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set MIDWARE_ENTRY_PORT_AUTONEG_MODE, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(bitmap & MIDWARE_ENTRY_PORT_RESET_AUTONEG)
  {
      if(MIDWARE_SFU_ENABLE==uni_cfg->reset_autoneg)
      {
          tpm_rc = tpm_phy_restart_port_autoneg(0, port_id);
          if (tpm_rc != TPM_RC_OK)
          {
              MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set MIDWARE_ENTRY_PORT_RESET_AUTONEG, port: %d\r\n", port_id);
              return MDW_CB_RET_SET_HW_FAIL;
          }
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }
#if 0 /*victor masked temporary 2011-05-18, open and debug it in future*/
  if(bitmap & MIDWARE_ENTRY_PORT_SPEED_CFG)
  {
      tpm_rc = tpm_phy_set_port_speed(0, port_id, uni_cfg->speed_cfg);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set port speed, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      uni_cfg->speed_cfg = speed;
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(bitmap & MIDWARE_ENTRY_PORT_DUPLEX_CFG)
  {
      tpm_rc = tpm_phy_set_port_duplex_mode(0, port_id, MIDWARE_U8_TO_BOOL(uni_cfg->duplex_cfg));
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set port duplex mode, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }
#endif

#if 1
  if(bitmap & MIDWARE_ENTRY_PORT_LOOPBACK_CFG)
  {
      if (MIDWARE_ETH_INTERNAL_LOOPBACK_DISABLE == uni_cfg->loopback) {
          tpm_rc = tpm_phy_set_port_loopback(0, port_id, TPM_PHY_INTERNAL_LOOPBACK, false);
      }
      else if (MIDWARE_ETH_INTERNAL_LOOPBACK_EN == uni_cfg->loopback) {
          tpm_rc = tpm_phy_set_port_loopback(0, port_id, TPM_PHY_INTERNAL_LOOPBACK, true);
      }
      else if (MIDWARE_ETH_EXTERNAL_LOOPBACK_DISABLE == uni_cfg->loopback) {
          tpm_rc = tpm_phy_set_port_loopback(0, port_id, TPM_PHY_EXTERNAL_LOOPBACK, false);
      }
      else if (MIDWARE_ETH_EXTERNAL_LOOPBACK_EN == uni_cfg->loopback) {
          tpm_rc = tpm_phy_set_port_loopback(0, port_id, TPM_PHY_EXTERNAL_LOOPBACK, true);
      }      

      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set port_loopback, port: %d, mode: %d\r\n", port_id, uni_cfg->loopback);
          return MDW_CB_RET_SET_HW_FAIL;
      }

      callback_ret = MDW_CB_RET_SET_HW_OK;
  }
#endif

#if 1
  if(bitmap & MIDWARE_ENTRY_PORT_ISOLATE_CFG)
  {
      tpm_rc = tpm_sw_set_isolate_eth_port_vector(0, port_id, uni_cfg->port_isolate);

      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set port_isolate, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }
#endif

  if(bitmap & MIDWARE_ENTRY_PORT_FLOOD_CFG)
  {
      tpm_rc = tpm_sw_set_port_flooding(0, port_id, TPM_FLOOD_UNKNOWN_UNICAST, MIDWARE_U8_TO_BOOL(uni_cfg->port_flood));
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_flooding, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(bitmap & MIDWARE_ENTRY_PORT_MAC_LEARN)
  {
      tpm_rc = tpm_sw_set_mac_learn(0, port_id, MIDWARE_U8_TO_BOOL(uni_cfg->mac_learn_en));
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get mac_learn, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(bitmap & MIDWARE_ENTRY_PORT_DISCARD_TAG)
  {
      tpm_rc = tpm_sw_set_port_tagged(0, port_id, ((uni_cfg->discard_tag)?1:0));
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set port_tagged, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(bitmap & MIDWARE_ENTRY_PORT_DISCARD_UNTAG)
  {
      tpm_rc = tpm_sw_set_port_untagged(0, port_id, ((uni_cfg->discard_untag)?1:0));
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set port_untagged, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(bitmap & MIDWARE_ENTRY_PORT_VID_FILTER)
  {
      tpm_rc = tpm_sw_port_set_vid_filter(0, port_id, uni_cfg->vlan_filter);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set vid_filter, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  
  if(    (bitmap & MIDWARE_ENTRY_PORT_IN_MIRROR_DST)
       || (bitmap & MIDWARE_ENTRY_PORT_IN_MIRROR_EN)
       || (bitmap & MIDWARE_ENTRY_PORT_IN_MIRROR_SRC))
  {
      //TBD
      //here maybe we need to adapt the midware portid to tpm portid
      dst_port_id = uni_cfg->in_mirror_dst;
      src_port_id = uni_cfg->in_mirror_src;
      tpm_rc = tpm_sw_set_port_mirror(0, src_port_id, dst_port_id, TPM_SW_MIRROR_INGRESS, MIDWARE_U8_TO_BOOL(uni_cfg->in_mirror_en));
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set port_mirror, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

   if(    (bitmap & MIDWARE_ENTRY_PORT_OUT_MIRROR_DST)
        || (bitmap & MIDWARE_ENTRY_PORT_OUT_MIRROR_EN)
        || (bitmap & MIDWARE_ENTRY_PORT_OUT_MIRROR_SRC))
   {
       //TBD
       //here maybe we need to adapt the midware portid to tpm portid
       dst_port_id = uni_cfg->out_mirror_dst;
       src_port_id = uni_cfg->out_mirror_dst;
       tpm_rc = tpm_sw_set_port_mirror(0, src_port_id, dst_port_id, TPM_SW_MIRROR_EGRESS, MIDWARE_U8_TO_BOOL(uni_cfg->out_mirror_en));
       if (tpm_rc != TPM_RC_OK)
       {
           MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_mirror, port: %d\r\n", port_id);
           return MDW_CB_RET_SET_HW_FAIL;
       }
       callback_ret = MDW_CB_RET_SET_HW_OK;
   }
   /*
   if(bitmap & MIDWARE_ENTRY_PORT_STP_STATE)
   {
       tpm_rc = tpm_sw_set_port_stp_state(0, port_id, (tpm_port_stp_state)uni_cfg->stp_state);
       if (tpm_rc != TPM_RC_OK)
       {
           MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set port STP state, port: %d\r\n", port_id);
           return MDW_CB_RET_SET_HW_FAIL;
       }
       callback_ret = MDW_CB_RET_SET_HW_OK;
   }
    */
   return callback_ret;
}

ONU_STATUS midware_table_uni_qos_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_UNI_QOS_T    uni_qos;
    UINT32             i;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_UNI_QOS))
    {
        //already got entries in the table, restore
        rc = midware_table_restore(MIDWARE_TABLE_UNI_QOS);
        if (ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to restore MIDWARE_TABLE_UNI_QOS\r\n");
            return ONU_FAIL;
        }

        return ONU_OK;
    }

    //init the default entries
    uni_qos.fc_cfg = MIDWARE_SFU_DISABLE;
    uni_qos.fc_state = MIDWARE_SFU_DISABLE;
    uni_qos.pause_time = 0;
    uni_qos.us_policing_en = MIDWARE_SFU_DISABLE;
    uni_qos.us_policing_mode = TPM_SW_LIMIT_LAYER2;
    uni_qos.us_policing_cir = OAM_FE_BIT_RATE_IN_KBPS;
    uni_qos.us_policing_cbs = 0;
    uni_qos.us_policing_ebs = 0;
    uni_qos.ds_ratelimit_en = MIDWARE_SFU_DISABLE;
    uni_qos.ds_ratelimit_mode = TPM_SW_LIMIT_LAYER2;
    uni_qos.ds_ratelimit_cir = 0;
    uni_qos.ds_ratelimit_pir = 0;

    table_info = midware_get_table_info(MIDWARE_TABLE_UNI_QOS);
    for(i = TPM_SRC_PORT_UNI_0; i <= TPM_SRC_PORT_UNI_3; i++)
    {
        uni_qos.port_id = i;
        if(MIDWARE_UNI_TYPE_GE == midware_get_uni_port_type(i))
        {
            uni_qos.us_policing_cir = OAM_GE_BIT_RATE_IN_KBPS;
        }
        else
        {
            uni_qos.us_policing_cir = OAM_FE_BIT_RATE_IN_KBPS;
        }

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &uni_qos);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for port: %ld\n", i);
            return ONU_FAIL;
        }

    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_uni_qos_get(UINT32 bitmap, void *entry)
{
  MIDWARE_TABLE_UNI_QOS_T   *qos_cfg;
  tpm_error_code_t                      tpm_rc = ERR_GENERAL;
  MIDWARE_CALLBACK_RET        callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
  bool         bool_state;
  //tpm_limit_mode_t    count_mode;
  tpm_src_port_type_t   port_id;

  qos_cfg = (MIDWARE_TABLE_UNI_QOS_T *)entry;

  //TBD
  //here maybe we need to adapt the midware portid to tpm portid
  port_id = qos_cfg->port_id;

  #if 0
  if(bitmap & MIDWARE_ENTRY_QOS_FC_CFG)
  {
      tpm_rc = tpm_phy_get_port_flow_control_support(0, port_id, &bool_state);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_flow_control_support, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      qos_cfg->fc_cfg = MIDWARE_BOOL_TO_U8(bool_state);
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }
  #endif

  if(bitmap & MIDWARE_ENTRY_QOS_FC_STATE)
  {
      tpm_rc = tpm_phy_get_port_flow_control_state(0, port_id, &bool_state);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get port_flow_control_state, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      qos_cfg->fc_state = MIDWARE_BOOL_TO_U8(bool_state);
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  #if 0
  if(    (bitmap & MIDWARE_ENTRY_QOS_US_CBS)
       || (bitmap & MIDWARE_ENTRY_QOS_US_EBS)
       || (bitmap & MIDWARE_ENTRY_QOS_US_POLICING_EN)
       || (bitmap & MIDWARE_ENTRY_QOS_US_POLICING_MODE)
       || (bitmap & MIDWARE_ENTRY_QOS_US_CIR))
  {
      tpm_rc = tpm_sw_get_uni_ingr_police_rate(0, port_id, &count_mode, (uint32_t*)&qos_cfg->us_policing_cir,
                                                                 (uint32_t*)&qos_cfg->us_policing_cbs, (uint32_t*)&qos_cfg->us_policing_ebs);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get uni_ingr_police_rate, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      qos_cfg->us_policing_mode = count_mode;
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(    (bitmap & MIDWARE_ENTRY_QOS_DS_RL_EN)
       || (bitmap & MIDWARE_ENTRY_QOS_DS_RL_MODE)
       || (bitmap & MIDWARE_ENTRY_QOS_DS_RL_CIR)
       || (bitmap & MIDWARE_ENTRY_QOS_DS_RL_PIR))
  {
      tpm_rc = tpm_sw_get_uni_egr_rate_limit(0, port_id, &count_mode, &qos_cfg->ds_ratelimit_cir);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to get uni_egr_rate_limit, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      qos_cfg->ds_ratelimit_mode = count_mode;
      qos_cfg->ds_ratelimit_pir = qos_cfg->ds_ratelimit_cir;
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }
  #endif
   return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_uni_qos_set(UINT32 bitmap, void *entry)
{
  MIDWARE_TABLE_UNI_QOS_T   *qos_cfg;
  tpm_error_code_t                      tpm_rc = ERR_GENERAL;
  MIDWARE_CALLBACK_RET        callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
  tpm_limit_mode_t    count_mode;
  tpm_src_port_type_t   port_id;

  //return (MDW_CB_RET_SET_HW_OK);
  qos_cfg = (MIDWARE_TABLE_UNI_QOS_T *)entry;

  //TBD
  //here maybe we need to adapt the midware portid to tpm portid
  port_id = qos_cfg->port_id;

  if(bitmap & MIDWARE_ENTRY_QOS_FC_CFG)
  {
      tpm_rc = tpm_phy_set_port_flow_control_support(0, port_id, MIDWARE_U8_TO_BOOL(qos_cfg->fc_cfg));
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set port_flow_control_support, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(    (bitmap & MIDWARE_ENTRY_QOS_US_CBS)
       || (bitmap & MIDWARE_ENTRY_QOS_US_EBS)
       || (bitmap & MIDWARE_ENTRY_QOS_US_POLICING_EN)
       || (bitmap & MIDWARE_ENTRY_QOS_US_POLICING_MODE)
       || (bitmap & MIDWARE_ENTRY_QOS_US_CIR))
  {
      count_mode = qos_cfg->us_policing_mode;
      tpm_rc = tpm_sw_set_uni_ingr_police_rate(0, port_id, count_mode, qos_cfg->us_policing_cir,
                                               qos_cfg->us_policing_cbs, qos_cfg->us_policing_ebs);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set uni_ingr_police_rate, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(    (bitmap & MIDWARE_ENTRY_QOS_DS_RL_EN)
       || (bitmap & MIDWARE_ENTRY_QOS_DS_RL_MODE)
       || (bitmap & MIDWARE_ENTRY_QOS_DS_RL_CIR)
       || (bitmap & MIDWARE_ENTRY_QOS_DS_RL_PIR))
  {
      count_mode = qos_cfg->ds_ratelimit_mode;
      tpm_rc = tpm_sw_set_uni_egr_rate_limit(0, port_id, count_mode, qos_cfg->ds_ratelimit_cir);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set uni_egr_rate_limit, port: %d\r\n", port_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

   return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_vlan_cmp_set_hw
(
    MIDWARE_TABLE_VLAN_T   *vlan_current,
    MIDWARE_TABLE_VLAN_T   *vlan_new
)
{
    UINT32    i;
    UINT32    tmp_port_vec;
    tpm_error_code_t   tpm_rc = TPM_RC_OK;
    tpm_src_port_type_t port_id;

    for(i = TPM_SRC_PORT_UNI_0; i <= TPM_SRC_PORT_UNI_3; i++)
    {
        tmp_port_vec = 1 << i;

        //TBD
        //here maybe we need to adapt the midware portid to tpm portid
        port_id = i;

        /* jinghua modify 2011-06-02
        if the port is moving from untag to tag or vise,
        do not need remove or add this port into vlan.
    */
        if(       (tmp_port_vec == (vlan_current->tag_member_vec & tmp_port_vec))
            && (0 == (vlan_new->tag_member_vec & tmp_port_vec))
            && (tmp_port_vec == (vlan_new->untag_member_vec & tmp_port_vec)))
        {
            /* this port moves from tag to untag */
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"this port moves from tag to untag\r\n");
            tpm_rc = tpm_sw_set_port_vid_egress_mode(0, port_id, (UINT16)vlan_current->vlan_id, MEMBER_EGRESS_UNTAGGED);
            if (tpm_rc != TPM_RC_OK)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set tpm_sw_set_port_vid_egress_mode, vlan: %ld, port: %d\r\n", vlan_current->vlan_id, port_id);
                return MDW_CB_RET_SET_HW_FAIL;
            }
            continue;
        }

        if(       (0 == (vlan_current->tag_member_vec & tmp_port_vec))
            && (tmp_port_vec == (vlan_new->tag_member_vec & tmp_port_vec))
            && (tmp_port_vec == (vlan_current->untag_member_vec & tmp_port_vec)))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"this port moves from untag to tag\r\n");
            /* this port moves from untag to tag */
            tpm_rc = tpm_sw_set_port_vid_egress_mode(0, port_id, (UINT16)vlan_current->vlan_id, MEMBER_EGRESS_TAGGED);
            if (tpm_rc != TPM_RC_OK)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set tpm_sw_set_port_vid_egress_mode, vlan: %ld, port: %d\r\n", vlan_current->vlan_id, port_id);
                return MDW_CB_RET_SET_HW_FAIL;
            }
            continue;
        }

        //handle tag member vec
        if(       (tmp_port_vec == (vlan_current->tag_member_vec & tmp_port_vec))
            && (0 == (vlan_new->tag_member_vec & tmp_port_vec)))
        {
            //remove port from vlan
            tpm_rc = tpm_sw_port_del_vid(0, port_id, (UINT16)vlan_current->vlan_id);
            if (tpm_rc != TPM_RC_OK)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set tpm_sw_port_del_vid, vlan: %ld, port: %d\r\n", vlan_current->vlan_id, port_id);
                return MDW_CB_RET_SET_HW_FAIL;
            }
        }
        else if(       (0 == (vlan_current->tag_member_vec & tmp_port_vec))
                   && (tmp_port_vec == (vlan_new->tag_member_vec & tmp_port_vec)))
        {
            //add port into vlan
            tpm_rc = tpm_sw_port_add_vid(0, port_id, (UINT16)vlan_current->vlan_id);
            if (tpm_rc != TPM_RC_OK)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set tpm_sw_port_add_vid, vlan: %ld, port: %d\r\n", vlan_current->vlan_id, port_id);
                return MDW_CB_RET_SET_HW_FAIL;
            }

            tpm_rc = tpm_sw_set_port_vid_egress_mode(0, port_id, (UINT16)vlan_current->vlan_id, MEMBER_EGRESS_TAGGED);
            if (tpm_rc != TPM_RC_OK)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set tpm_sw_set_port_vid_egress_mode, vlan: %ld, port: %d\r\n", vlan_current->vlan_id, port_id);
                return MDW_CB_RET_SET_HW_FAIL;
            }
        }


        //handle untag member vec
        if(       (tmp_port_vec == (vlan_current->untag_member_vec & tmp_port_vec))
            && (0 == (vlan_new->untag_member_vec & tmp_port_vec)))
        {
            //remove port from vlan
            tpm_rc = tpm_sw_port_del_vid(0, port_id, (UINT16)vlan_current->vlan_id);
            if (tpm_rc != TPM_RC_OK)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set tpm_sw_port_del_vid, vlan: %ld, port: %d\r\n", vlan_current->vlan_id, port_id);
                return MDW_CB_RET_SET_HW_FAIL;
            }
        }
        else if(       (0 == (vlan_current->untag_member_vec & tmp_port_vec))
                   && (tmp_port_vec == (vlan_new->untag_member_vec & tmp_port_vec)))
        {
            //add port into vlan
            tpm_rc = tpm_sw_port_add_vid(0, port_id, (UINT16)vlan_current->vlan_id);
            if (tpm_rc != TPM_RC_OK)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set tpm_sw_port_add_vid, vlan: %ld, port: %d\r\n", vlan_current->vlan_id, port_id);
                return MDW_CB_RET_SET_HW_FAIL;
            }

            tpm_rc = tpm_sw_set_port_vid_egress_mode(0, port_id, (UINT16)vlan_current->vlan_id, MEMBER_EGRESS_UNTAGGED);
            if (tpm_rc != TPM_RC_OK)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to Set tpm_sw_set_port_vid_egress_mode, vlan: %ld, port: %d\r\n", vlan_current->vlan_id, port_id);
                return MDW_CB_RET_SET_HW_FAIL;
            }
        }
    }

    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_vlan_insert(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_VLAN_T   vlan_current;
    MIDWARE_TABLE_VLAN_T   *vlan_new;
    MIDWARE_CALLBACK_RET  midware_rc;


    vlan_new = (MIDWARE_TABLE_VLAN_T*)entry;

    memset(&vlan_current, 0, sizeof(vlan_current));
    vlan_current.vlan_id = vlan_new->vlan_id;

    midware_rc = midware_table_vlan_cmp_set_hw(&vlan_current, vlan_new);

    if (MDW_CB_RET_SET_HW_OK != midware_rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set midware_table_vlan_cmp_set_hw, vlan: %ld\r\n", vlan_new->vlan_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_vlan_set(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_VLAN_T   vlan_current;
    MIDWARE_TABLE_VLAN_T   *vlan_new;
    MIDWARE_CALLBACK_RET  midware_rc;
    ONU_STATUS    rc;
    MIDWARE_TABLE_INFO * table_info;

    vlan_new = (MIDWARE_TABLE_VLAN_T*)entry;

    //get current vlan
    table_info = midware_get_table_info(MIDWARE_TABLE_VLAN);

    vlan_current.vlan_id = vlan_new->vlan_id;
    rc = midware_sqlite3_get_select_sql_prepared(table_info, &vlan_current);
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get entry for vlan: %ld\n", vlan_new->vlan_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    midware_rc = midware_table_vlan_cmp_set_hw(&vlan_current, vlan_new);

    if (MDW_CB_RET_SET_HW_OK != midware_rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set midware_table_vlan_cmp_set_hw, vlan: %ld\r\n", vlan_new->vlan_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_vlan_remove(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_VLAN_T   vlan_current;
    MIDWARE_TABLE_VLAN_T   *vlan_new;
    MIDWARE_CALLBACK_RET  midware_rc;
    ONU_STATUS    rc;
    MIDWARE_TABLE_INFO * table_info;

    vlan_new = (MIDWARE_TABLE_VLAN_T*)entry;

    //get current vlan
    table_info = midware_get_table_info(MIDWARE_TABLE_VLAN);

    vlan_current.vlan_id = vlan_new->vlan_id;
    rc = midware_sqlite3_get_select_sql_prepared(table_info, &vlan_current);
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get entry for vlan: %ld\n", vlan_new->vlan_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    vlan_new->tag_member_vec = 0;
    vlan_new->untag_member_vec = 0;

    midware_rc = midware_table_vlan_cmp_set_hw(&vlan_current, vlan_new);

    if (MDW_CB_RET_SET_HW_OK != midware_rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set midware_table_vlan_cmp_set_hw, vlan: %ld\r\n", vlan_new->vlan_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_wan_qos_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_WAN_QOS_T    wan_qos;
    UINT32             i;
    UINT32             queue_id;
    MIDWARE_TABLE_INFO *table_info;

    //init the default entries
    wan_qos.schedule_mode = 0;
    wan_qos.weight = 0;
    wan_qos.cir = 0;
    wan_qos.pir = 0;

    table_info = midware_get_table_info(MIDWARE_TABLE_WAN_QOS);
    for(i = 0; i <= 7; i++)
    {
        wan_qos.llid = i;
        for(queue_id = 0; queue_id <= 7; queue_id++)
        {
            wan_qos.queue_id = queue_id;

            rc = midware_sqlite3_get_insert_sql_prepared(table_info, &wan_qos);
            if(ONU_OK != rc)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for llid: %ld, queue_id: %ld\n", i, queue_id);
                return ONU_FAIL;
            }
        }
    }

    return ONU_OK;
}

MIDWARE_CALLBACK_RET midware_table_wan_qos_set(UINT32 bitmap, void *entry)
{
  MIDWARE_TABLE_WAN_QOS_T   *qos_cfg;
  tpm_error_code_t                      tpm_rc = ERR_GENERAL;
  MIDWARE_CALLBACK_RET        callback_ret = MDW_CB_RET_SET_HW_NO_NEED;

  qos_cfg = (MIDWARE_TABLE_WAN_QOS_T *)entry;

  if(    (bitmap & MIDWARE_ENTRY_LLID)
       || (bitmap & MIDWARE_ENTRY_WAN_QUEUE_ID)
       || (bitmap & MIDWARE_ENTRY_WANC_SCHEDULE_MODE)
       || (bitmap & MIDWARE_ENTRY_WAN_QOS_WEIGHT))
  {
      tpm_rc = tpm_tm_set_wan_egr_queue_sched(0, TPM_TRG_PORT_WAN, qos_cfg->schedule_mode,
                                              qos_cfg->queue_id, qos_cfg->weight/100);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set tpm_tm_set_wan_egr_queue_sched, queue_id: %d\r\n", qos_cfg->queue_id);
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

  if(    (bitmap & MIDWARE_ENTRY_WAN_QOS_CIR)
       || (bitmap & MIDWARE_ENTRY_WAN_QOS_PIR))
  {
      if(qos_cfg->cir != 0)
      {
      tpm_rc = tpm_tm_set_wan_queue_egr_rate_lim(0, TPM_TRG_PORT_WAN,
                                                 qos_cfg->queue_id, qos_cfg->cir, qos_cfg->pir);
      if (tpm_rc != TPM_RC_OK)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to set tpm_tm_set_wan_queue_egr_rate_lim\r\n");
          return MDW_CB_RET_SET_HW_FAIL;
      }
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

   return callback_ret;
}

// COPY FROM OMCI ...
#if 1

// Utility for reading U-Boot environment
#define FW_PRINTENVSTR                   "fw_printenv"
#define FW_SETENVSTR                     "fw_setenv"
#define FALLBACK_ACTIVE_BANK             'A'
#define FALLBACK_COMMITTED_BANK          'A'
#define FALLBACK_ISVALID_A               true
#define FALLBACK_ISVALID_B               false

#define UBVSTR_DUALIMAGE                 "dual_image"
#define UBVSTR_COMMITTED                 "committedBank"
#define UBVSTR_ISVALID_A                 "isValidA"
#define UBVSTR_ISVALID_B                 "isValidB"
#define UBVSTR_VERSION_A                 "versionA"
#define UBVSTR_VERSION_B                 "versionB"
#define UBVSTR_ACTIVATION_TEST           "act_test"
#define UBVSTR_ACT_BOOT_COMPLETE         "act_boot_complete"

#define PROFCACHE_BANK_A                   'A'
#define PROFCACHE_BANK_B                   'B'

typedef enum
{
    ENUM_UBV_COMMITTED, ENUM_UBV_ISVALID_A, ENUM_UBV_ISVALID_B, ENUM_UBV_VERSION_A, ENUM_UBV_VERSION_B,
    ENUM_UBV_ACTIVATION_TEST, ENUM_UBV_DUALIMAGE
} ENUM_UBOOTVARS;

typedef struct
{
    char  *varName;
    int   varEnum;
} UBootVarParseInfo_S;

typedef struct
{
    bool     isValidA;
    bool     isValidB;
    UINT8    committedBank;
    UINT8    activeBank;
    UINT8    versionA[15];
    UINT8    versionB[15];
    bool     altImageActivationVarSet;
} SwImage_S;

static UBootVarParseInfo_S swimageVarsParseAra[] =
{
    {UBVSTR_COMMITTED,           ENUM_UBV_COMMITTED},
    {UBVSTR_ISVALID_A,           ENUM_UBV_ISVALID_A},
    {UBVSTR_ISVALID_B,           ENUM_UBV_ISVALID_B},
    {UBVSTR_VERSION_A,           ENUM_UBV_VERSION_A},
    {UBVSTR_VERSION_B,           ENUM_UBV_VERSION_B},
    {UBVSTR_ACTIVATION_TEST,     ENUM_UBV_ACTIVATION_TEST},
    {UBVSTR_DUALIMAGE,     ENUM_UBV_DUALIMAGE},
};
static int swimageVarsParseAra_size = sizeof(swimageVarsParseAra)/sizeof(swimageVarsParseAra[0]);

static bool isCommittedValueOK(char *valueStr, char *rvalchar)
{
    char *token;

    token = strtok((char *)valueStr, "\n\r");

    if (strlen(token) == 1 && (toupper(valueStr[0]) == 'A' || toupper(valueStr[0]) == 'B'))
    {
        *rvalchar = toupper(valueStr[0]);
        return true;
    }
    printf("%s: Illegal value \"%s\"\n", __FUNCTION__, valueStr);
    return false;
}

static bool isIsValidValueOK(char *valueStr, bool *rvalbool)
{
    char *token;

    token = strtok((char *)valueStr, "\n\r");

    if (strcmp(token, "true") == 0)
    {
        *rvalbool = true;
        return true;
    }
    else if (strcmp(token, "false") == 0)
    {
        *rvalbool = false;
        return true;
    }
    printf("%s: Illegal value \"%s\"\n", __FUNCTION__, token);
    return false;
}

static bool isDualImageValueOK(char *valueStr, bool *rvalbool)
{
    char *token;

    token = strtok((char *)valueStr, "\n\r");

    if (strcmp(token, "yes") == 0)
    {
        *rvalbool = 1;
        return true;
    }
    else if (strcmp(token, "no") == 0)
    {
        *rvalbool = 0;
        return true;
    }
    printf("%s: Illegal value \"%s\"\n", __FUNCTION__, token);
    return false;
}

static bool isVersionValueOK(char *valueStr, char **rvalstr)
{
    unsigned int  indx;
    char *token;

    token = strtok((char *)valueStr, "\n\r");

    for (indx = 0; indx < strlen(token); indx++)
    {
        if (token[indx] != ' ')
        {
            *rvalstr = &token[indx];
            return true;
        }
    }
    printf("%s: Illegal value \"%s\"\n", __FUNCTION__, valueStr);
    return false;
}

static bool isImageTestActivated_SetTo1(char *valueStr, bool *rvalbool)
{
    char *token;

    token = strtok((char *)valueStr, "\n\r");

    if (strlen(token) == 1)
    {
        if (token[0] == '1')
        {
            *rvalbool = true;
            return true;
        }
        else
        {
            *rvalbool = false;
            return true;
        }
    }
    printf("%s: Illegal value %s\n", __FUNCTION__, valueStr);
    return false;
}


static bool handleUbootSwImageVar(SwImage_S *p_RawSwImage, BOOL * dual_image, char *line)
{
    int                 indx;
    UBootVarParseInfo_S *p_UBootVarParseInfo;
    bool                rc = false;
    int                 namesize;
    char                rvalchar;
    bool                rvalbool;
    char                *rvalstr;

    for (indx = 0; indx < swimageVarsParseAra_size; indx++)
    {
        p_UBootVarParseInfo = &swimageVarsParseAra[indx];
        namesize            = strlen(p_UBootVarParseInfo->varName);

        // Look for matching U-Boot var followed by '='
        if (strncmp(line, p_UBootVarParseInfo->varName, namesize) == 0 && line[namesize] == '=')
        {
            //printf("%s: Found U-Boot var %s\n", __FUNCTION__, line);

            if (p_UBootVarParseInfo->varEnum == ENUM_UBV_COMMITTED)
            {
                if (isCommittedValueOK(&line[namesize+1], &rvalchar) == true)
                {
                    p_RawSwImage->committedBank = rvalchar;
                    rc = true;
                    break;
                }
            }
            else if (p_UBootVarParseInfo->varEnum == ENUM_UBV_DUALIMAGE)
            {
                if (isDualImageValueOK(&line[namesize+1], &rvalbool) == true)
                {
                    *dual_image = rvalbool;
                    rc = true;
                    break;
                }
            }
            else if (p_UBootVarParseInfo->varEnum == ENUM_UBV_ISVALID_A)
            {
                if (isIsValidValueOK(&line[namesize+1], &rvalbool) == true)
                {
                    p_RawSwImage->isValidA = rvalbool;
                    rc = true;
                    break;
                }
            }
            else if (p_UBootVarParseInfo->varEnum == ENUM_UBV_ISVALID_B)
            {
                if (isIsValidValueOK(&line[namesize+1], &rvalbool) == true)
                {
                    p_RawSwImage->isValidB = rvalbool;
                    rc = true;
                    break;
                }
            }
            else if (p_UBootVarParseInfo->varEnum == ENUM_UBV_VERSION_A)
            {
                if (isVersionValueOK(&line[namesize+1], &rvalstr) == true)
                {
                    char *token;

                    // So we do not copy \n at end of line
                    token = strtok((char *)rvalstr, "\n\r");

                    strcpy((char *)p_RawSwImage->versionA, token);
                    rc = true;
                    break;
                }
            }
            else if (p_UBootVarParseInfo->varEnum == ENUM_UBV_VERSION_B)
            {
                if (isVersionValueOK(&line[namesize+1], &rvalstr) == true)
                {
                    char *token;

                    // So we do not copy \n at end of line
                    token = strtok((char *)rvalstr, "\n\r");

                    strcpy((char *)p_RawSwImage->versionB, token);
                    rc = true;
                    break;
                }
            }
            else if (p_UBootVarParseInfo->varEnum == ENUM_UBV_ACTIVATION_TEST)
            {
                if (isImageTestActivated_SetTo1(&line[namesize+1], &rvalbool) == true)
                {
                    p_RawSwImage->altImageActivationVarSet = rvalbool;
                    rc = true;
                    break;
                }
            }
        }
    }
    return rc;
}

static void parsePrintEnv(SwImage_S *p_RawSwImage, BOOL *dual_image)
{
    char  line[500];
    int   lineIndx   = 0;
    int   parsedVars = 0;
    FILE  *fptr;
    char  *outFile = "/tmp/printenv.txt";

    sprintf(line, "%s > %s", FW_PRINTENVSTR, outFile);

    if (system(line))
    {
        printf("%s: \"%s\" FAILED. \n", __FUNCTION__, line);
    }
    else
    {
        if ((fptr = fopen(outFile, "r")) != 0)
        {
            while (fgets(line, sizeof(line), fptr) != 0)
            {
                lineIndx++;
                if (handleUbootSwImageVar(p_RawSwImage, dual_image, line) == true)
                {
                    parsedVars++;
                }
            }
            //printf("%s: Read %d lines from %s output. Parsed %d variables\n", __FUNCTION__, lineIndx, FW_PRINTENVSTR, parsedVars);
        }
        else
        {
            printf("%s: fopen(%s, \"r\") FAILED. \n", __FUNCTION__, outFile);
        }
        fclose(fptr);
    }
}

static void parseLinuxCmdLine(SwImage_S *p_RawSwImage)
{
    char  line[500];
    char  *token;
    FILE  *fptr;

    // Expect the line to include "ubi.mtd=2 root=ubi0:rootfsU" for execution from flash
    //    OR
    // Expect the line to include "ubi.mtd=5 root=ubi0:rootfsB" for execution from flash

    // Read the Linux command line - to identify the active bank
    if ((fptr = fopen("/proc/cmdline","r")) != NULL)
    {
        if (fgets(line, sizeof(line), fptr) != 0)
        {
            token = strtok((char *)line, " ");
            while (token != 0)
            {
                if (strstr(token, "root=ubi0:rootfsU") != 0)
                {
                    p_RawSwImage->activeBank = 'A';
                    break;
                }
                else if (strstr(token, "root=ubi0:rootfsB") != 0)
                {
                    p_RawSwImage->activeBank = 'B';
                    break;
                }
                token = strtok(NULL, " ");
            }
        }
        else
        {
            printf("%s: Failed to get Linux cmdline\n", __FUNCTION__);
        }
        fclose(fptr);
    }
    else
    {
        printf("%s: fopen(/proc/cmdline, \"r\") FAILED.\n", __FUNCTION__);
    }
}

static BOOL readImageVersion(char *version, int length)
{
    FILE  *filehandle;
    char  line[32];
    char  *token;

    if ((filehandle = fopen("/etc/version.txt", "r")) == NULL)
    {
        printf("fopen %s failed.\n", "/etc/version.txt");
        return FALSE;
    }

    if (fgets(line, 32, filehandle) != 0)
    {
        token = strtok(line, "\n\r");

        if (strlen(token) > 0)
        {
            strncpy(version, token, length);
        }
    }

    if (fclose(filehandle) != 0)
    {
        printf("%s: fclose failed.\n", __FUNCTION__);
    }

    return TRUE;
}

#endif

ONU_STATUS midware_table_sw_image_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_IMAGE_T    sw_image;
    MIDWARE_TABLE_INFO *table_info;
    SwImage_S             image_info;
    char                  line[500];
    BOOL                  dual_image ;

    memset(&image_info, 0, sizeof(SwImage_S));
    parsePrintEnv(&image_info, &dual_image);
    parseLinuxCmdLine(&image_info);

    // The code below is disconnected from U-Boot. There is no way that U-Boot bankA, bankB information can be dictated from
    // midware. E.g if (image_info.isValidA == false) CANNOT EVER MAKE image_info.isValidA = true in U-Boot
    // If absolutely necessary, OMCI will update midware.
    if (dual_image)
    {
        /*check nvram variables*/
        if (image_info.committedBank == 0)
        {
           image_info.committedBank = image_info.activeBank;
#if 0
           sprintf(line, "%s %s %c", FW_SETENVSTR, UBVSTR_COMMITTED, image_info.committedBank);
           system(line);
#endif
        }

        if (image_info.isValidA == false)
        {
#if 0
           sprintf(line, "%s %s true", FW_SETENVSTR, UBVSTR_ISVALID_A);
           system(line);
#endif
           image_info.isValidA = true;
        }

        if (image_info.isValidB == false)
        {
#if 0
           sprintf(line, "%s %s true", FW_SETENVSTR, UBVSTR_ISVALID_B);
           system(line);
#endif
           image_info.isValidB= true;
        }
    }
    else
    {
           image_info.activeBank = 'A';
           image_info.committedBank = 'A';
           image_info.isValidA = true;
           image_info.isValidB = true;
    }

    // Force version to non-null
    /*versionA may be wrong if burn new flash without resetenv*/
    if (image_info.isValidA == true)/* && (strlen((char *)image_info.versionA) == 0))*/
    {
        if (image_info.activeBank == 'A')
        {
            readImageVersion((char *)image_info.versionA, sizeof(image_info.versionA)-1);

            if(strlen((char *)image_info.versionA) == 0)
            {
                strcpy((char *)image_info.versionA, "<forced-A>");
            }
        }
#if 0
        sprintf(line, "%s %s %s", FW_SETENVSTR, UBVSTR_VERSION_A, (char *)image_info.versionA);
        if (system(line))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"%s: \"%s\" FAILED.\n", __FUNCTION__, line);
        }
#endif
    }

    if (image_info.isValidB == true) /*&& (strlen((char *)image_info.versionB) == 0))*/
    {
        if (image_info.activeBank == 'B')
        {
            readImageVersion((char *)image_info.versionB, sizeof(image_info.versionB)-1);

            if(strlen((char *)image_info.versionB) == 0)
            {
                strcpy((char *)image_info.versionB, "<forced-B>");
            }
        }

#if 0
        sprintf(line, "%s %s %s", FW_SETENVSTR, UBVSTR_VERSION_B, (char *)image_info.versionB);
        if (system(line))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"%s: \"%s\" FAILED.\n", __FUNCTION__, line);
        }
#endif
    }


    /*add the first active image*/
    memset(&sw_image,0,sizeof(sw_image));
    sw_image.image_id = MIDWARE_MIN_IMAGE_ID;
    sw_image.active_image  = (image_info.activeBank == 'A') ? 1 : 0;
    sw_image.commit_image  = (image_info.committedBank == 'A') ? 1 : 0;
    sw_image.is_valid      = image_info.isValidA;
    sw_image.burn_to_flash = 0;
    sw_image.status = MIDWARE_SW_UPDATE_NOT_STARTED;

    strncpy((char *)sw_image.verion, (char *)image_info.versionA, MIDWARE_IMAGE_VER_LEN);
    sw_image.verion[MIDWARE_IMAGE_VER_LEN] = '\0';

    //insert the entry into SQLite
    table_info = midware_get_table_info(MIDWARE_TABLE_SW_IMAGE);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &sw_image);
    if (ONU_FAIL == rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entry\n");
        return ONU_FAIL;
    }

    /*add the second inactive image*/
    memset(&sw_image, 0, sizeof(sw_image));
    sw_image.image_id     = MIDWARE_MAX_IMAGE_ID;
    sw_image.active_image = (image_info.activeBank == 'B') ? 1 : 0;
    sw_image.commit_image = (image_info.committedBank == 'B') ? 1 : 0;
    sw_image.is_valid     = image_info.isValidB;
    sw_image.status       = MIDWARE_SW_UPDATE_NOT_STARTED;

    strncpy((char *)sw_image.verion, (char *)image_info.versionB, MIDWARE_IMAGE_VER_LEN);
    sw_image.verion[MIDWARE_IMAGE_VER_LEN] = '\0';

    //insert the entry into SQLite
    table_info = midware_get_table_info(MIDWARE_TABLE_SW_IMAGE);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &sw_image);
    if (ONU_FAIL == rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entry\n");
        return ONU_FAIL;
    }

    // Clear the test activation variables
    if (image_info.altImageActivationVarSet == true)
    {
        sprintf(line, "%s %s 0", FW_SETENVSTR, UBVSTR_ACTIVATION_TEST);
        if (system(line))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"%s: \"%s\" FAILED.\n", __FUNCTION__, line);
            return ONU_FAIL;
        }

        sprintf(line, "%s %s 0", FW_SETENVSTR, UBVSTR_ACT_BOOT_COMPLETE);
        if (system(line))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"%s: \"%s\" FAILED.\n", __FUNCTION__, line);
            return ONU_FAIL;
        }
    }


    return ONU_OK;
}

#define BANK_2_IMG(x)    (((x) == 'A') ? 0 : 1)
#define IMG_2_BANK(x)    (!(x) ? 'A' : 'B')

MIDWARE_CALLBACK_RET midware_table_sw_image_get(UINT32 bitmap, void *entry)
{
    MIDWARE_CALLBACK_RET   callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    MIDWARE_TABLE_IMAGE_T *image;
    BurnStatus_S           burn_state;

    image = (MIDWARE_TABLE_IMAGE_T *)entry;

    memset(&burn_state, 0, sizeof(BurnStatus_S));

    if (bitmap & MIDWARE_ENTRY_UPGRADE_STATUS)
    {
        if (FlashApi_getBurnStatus(&burn_state))
        {
            if (image->image_id != BANK_2_IMG(burn_state.bank))
            {
                //callback_ret = MDW_CB_RET_SET_HW_FAIL;
                image->status = MIDWARE_SW_UPDATE_NOT_STARTED;
                return callback_ret;
            }

            if (burn_state.inprogress)
            {
                image->status = MIDWARE_SW_UPDATE_BUSY;
            }
            else if (burn_state.iswriteok)
            {
                image->status = MIDWARE_SW_UPDATE_OK;
                if(bitmap & MIDWARE_ENTRY_IMAGE_VERSION)
                {
                    memcpy(image->verion, burn_state.bankversion, sizeof(image->verion));
                }
            }
            else
            {
                image->status = MIDWARE_SW_UPDATE_NOT_STARTED;
            }
        }
        else
        {
            callback_ret = MDW_CB_RET_SET_HW_FAIL;
        }
    }

    return callback_ret;
}

extern void sycl_reboot(int delay);

static void midware_clear_other_image_commit(UINT8 image_id)
{
    MIDWARE_TABLE_INFO      *table_info;
    ONU_STATUS              rc;
    MIDWARE_TABLE_IMAGE_T   midware_image;
    UINT32 bitmap = MIDWARE_ENTRY_IMAGE_COMMIT;

    memset(&midware_image, 0 , sizeof(midware_image));
    midware_image.image_id = 1 - image_id;
    midware_image.commit_image = 0;

    table_info = midware_get_table_info(MIDWARE_TABLE_SW_IMAGE);

    midware_sqlite3_get_update_sql_prepared(table_info, bitmap, &midware_image);
}

MIDWARE_CALLBACK_RET midware_table_sw_image_set(UINT32 bitmap, void *entry)
{
    MIDWARE_CALLBACK_RET   callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    MIDWARE_TABLE_IMAGE_T *image;
    UINT8 bank;
    char filename[FILENAME_SIZE], clientId[CLIENTID_SIZE];

    image = (MIDWARE_TABLE_IMAGE_T *)entry;

    memset(filename, 0, sizeof(filename));
    memset(clientId, 0, sizeof(clientId));

    bank = IMG_2_BANK(image->image_id);

    if (bitmap & MIDWARE_ENTRY_IMAGE_VALID)
    {
        if (image->is_valid == 0)
        {
            if (FlashApi_setImageToInvalid(bank))
            {
            }
            else
            {
                callback_ret = MDW_CB_RET_SET_HW_FAIL;
            }
        }
    }

    if (bitmap & MIDWARE_ENTRY_IMAGE_TO_FLASH)
    {
        if (image->burn_to_flash)
        {
            strcpy(filename, image->image_file_name);
            if(TPM_EPON == wanTech)
            {
                clientId[0] = 'e'; clientId[1] = 'O'; clientId[2] = 'A'; clientId[3] = 'M'; 
            }
            else
            {
                clientId[0] = 'O'; clientId[1] = 'M'; clientId[2] = 'C'; clientId[3] = 'I'; 
            }

            if (FlashApi_setImageToInvalid(bank))
            {
                // TODO: invalidate current image.

                if (FlashApi_startWriteFlash(bank, filename, clientId))
                {
                }
                else
                {
                    callback_ret = MDW_CB_RET_SET_HW_FAIL;
                }
            }
            else
            {
                callback_ret = MDW_CB_RET_SET_HW_FAIL;
            }
        }
    }

    if (bitmap & MIDWARE_ENTRY_IMAGE_ACTIVE)
    {
        if (FlashApi_prepareImageActivation(bank))
        {
            callback_ret = MDW_CB_RET_SET_HW_OK;
            if(TPM_EPON == wanTech)
            {
            sycl_reboot(3000);
        }

        }
        else
        {
            callback_ret = MDW_CB_RET_SET_HW_FAIL;
        }
    }

    if (bitmap & MIDWARE_ENTRY_IMAGE_COMMIT)
    {
        if (FlashApi_setCommittedBank(bank))
        {
            midware_clear_other_image_commit(image->image_id);
            callback_ret = MDW_CB_RET_SET_HW_OK;
        }
        else
        {
            callback_ret = MDW_CB_RET_SET_HW_FAIL;
        }
    }

    if (bitmap & MIDWARE_ENTRY_IMAGE_ABORT)
    {
        if (FlashApi_abortWriteFlash())
        {
            callback_ret = MDW_CB_RET_SET_HW_OK;
        }
        else
        {
            callback_ret = MDW_CB_RET_SET_HW_FAIL;
        }
    }

    return callback_ret;
}

ONU_STATUS midware_table_opt_transceiver_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_TRANSCEIVER_T    opt_transceiver;
    MIDWARE_TABLE_INFO *table_info;

    //init the default entries
    memset(&opt_transceiver,0,sizeof(opt_transceiver));


    table_info = midware_get_table_info(MIDWARE_TABLE_OPT_TRANSCEIVER);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &opt_transceiver);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entriy for MIDWARE_TABLE_OPT_TRANSCEIVER\n");
        return ONU_FAIL;
    }

    return ONU_OK;
}

MIDWARE_CALLBACK_RET midware_table_opt_transceiver_get(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_TRANSCEIVER_T         *opt_transceiver;
    OnuXvrA2D_S                         onuXvrA2D;
    bool                               rc;
    bool                                enable;
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;

    opt_transceiver = (MIDWARE_TABLE_TRANSCEIVER_T *)entry;

    if(is_support_onu_xvr_I2c > 0)
    {
        rc = localI2cApi_getOnuXvrA2dValues(&onuXvrA2D);
        if (true != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to localI2cApi_getOnuXvrA2dValues\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }

        opt_transceiver->temperature = (UINT16)onuXvrA2D.temperature;
        opt_transceiver->supply_voltage = (UINT16)onuXvrA2D.supplyVoltage;
        opt_transceiver->bias_current = (UINT16)onuXvrA2D.txBiasCurrent;
        opt_transceiver->tx_power = (UINT16)onuXvrA2D.txOpticalPower;
        opt_transceiver->rx_power = (UINT16)onuXvrA2D.rxReceivedPower;

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }
    else
    {
        //callback_ret = MDW_CB_RET_SET_HW_FAIL;
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }


     return callback_ret;
}

ONU_STATUS midware_table_epon_auth_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_LOID_T    epon_auth;
    MIDWARE_TABLE_INFO *table_info;
    char *default_admin = "admin";

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_EPON_AUTH))
    {
        //already got entries in the table,
        return ONU_OK;
    }

    //init the default entries
    memset(&epon_auth,0,sizeof(epon_auth));
    epon_auth.auth_mode = 0;
    //memcpy(epon_auth.loid, default_admin, sizeof("admin"));
    strcpy(epon_auth.loid, default_admin);
    //memcpy(epon_auth.pwd, default_admin, sizeof("admin"));
    strcpy(epon_auth.pwd, default_admin);

    table_info = midware_get_table_info(MIDWARE_TABLE_EPON_AUTH);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &epon_auth);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_EPON_AUTH\n");
        return ONU_FAIL;
    }

    return ONU_OK;
}

ONU_STATUS midware_table_web_account_init(void)
{
    ONU_STATUS           rc;
    MIDWARE_TABLE_WEB_ACCOUNT_T web_account;
    MIDWARE_TABLE_INFO   *table_info;
    char *default_account = "admin";

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_WEB_ACCOUNT))
    {
        //already got entries in the table, restore
        rc = midware_table_restore(MIDWARE_TABLE_WEB_ACCOUNT);
        if (ONU_OK == rc)
        {
            return ONU_OK;

        }
        else
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to restore MIDWARE_TABLE_WEB_ACCOUNT\r\n");
            return ONU_FAIL;
        }
    }

    //init the default entries
    memset(&web_account,0,sizeof(web_account));
    web_account.account_type = 0;
    strcpy(web_account.name, default_account);
    strcpy(web_account.pwd, default_account);

    table_info = midware_get_table_info(MIDWARE_TABLE_WEB_ACCOUNT);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &web_account);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_WEB_ACCOUNT\n");
        return ONU_FAIL;
    }

    return ONU_OK;
}

ONU_STATUS midware_table_rstp_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_RSTP_T    rstp;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_RSTP))
    {
        //already got entries in the table,
        return ONU_OK;
    }

    //init the default entries
    rstp.index = 0;
    rstp.rstp_en = MIDWARE_SFU_ENABLE;
    rstp.bridge_pri = MIDWARE_SFU_BRIDGE_PRIORITY;
    rstp.fwd_delay = MIDWARE_SFU_FWD_DELAY;
    rstp.hello_time = MIDWARE_SFU_HELLO_TIME;
    rstp.age_time = MIDWARE_SFU_AGE_TIME;

    table_info = midware_get_table_info(MIDWARE_TABLE_RSTP);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &rstp);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entriy for MIDWARE_TABLE_RSTP\n");
        return ONU_FAIL;
    }

    return ONU_OK;
}

MIDWARE_CALLBACK_RET midware_table_rstp_get(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_RSTP_T               *rstp;
    INT32                                                            rc;
    bool                                                   enable;
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;

    rstp = (MIDWARE_TABLE_RSTP_T *)entry;

    if(bitmap & MIDWARE_ENTRY_RSTP_STATE)
    {
        rc = RSTP_get_enable(&enable);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to RSTP_get_enable\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        rstp->rstp_en = MIDWARE_BOOL_TO_U8(enable) ;
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    #if 0
    if(bitmap & MIDWARE_ENTRY_RSTP_BRIDGE_PRI)
    {
        rc = RSTP_get_bridge_priority(&rstp->bridge_pri);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to RSTP_get_bridge_priority\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

     if(    (bitmap & MIDWARE_ENTRY_RSTP_FWD_DELAY)
          || (bitmap & MIDWARE_ENTRY_RSTP_HELLO_TIME)
          || (bitmap & MIDWARE_ENTRY_RSTP_AGE_TIME))
     {
         rc = RSTP_get_time(&rstp->fwd_delay, &rstp->hello_time, &rstp->age_time);
         if (OK != rc)
         {
             MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to RSTP_get_time\r\n");
             return MDW_CB_RET_SET_HW_FAIL;
         }
         callback_ret = MDW_CB_RET_SET_HW_OK;
     }

    #endif

     return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_rstp_set(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_RSTP_T               *rstp;
    INT32                                                            rc;
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;

    rstp = (MIDWARE_TABLE_RSTP_T *)entry;

    if(bitmap & MIDWARE_ENTRY_RSTP_STATE)
    {
        rc = RSTP_set_enable(MIDWARE_U8_TO_BOOL(rstp->rstp_en));
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to RSTP_set_enable\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_RSTP_BRIDGE_PRI)
    {
        rc = RSTP_set_bridge_priority(rstp->bridge_pri);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to RSTP_set_time\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

     if(    (bitmap & MIDWARE_ENTRY_RSTP_FWD_DELAY)
          || (bitmap & MIDWARE_ENTRY_RSTP_HELLO_TIME)
          || (bitmap & MIDWARE_ENTRY_RSTP_AGE_TIME))
     {
         rc = RSTP_set_time(rstp->fwd_delay, rstp->hello_time, rstp->age_time);
         if (OK != rc)
         {
             MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to RSTP_set_time\r\n");
             return MDW_CB_RET_SET_HW_FAIL;
         }
         callback_ret = MDW_CB_RET_SET_HW_OK;
     }

     return callback_ret;
}

ONU_STATUS midware_table_rstp_port_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_RSTP_PORT_T    rstp_port;
    UINT32             i;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_RSTP_PORT))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    rstp_port.rstp_en = MIDWARE_SFU_ENABLE;
    rstp_port.port_pri = MIDWARE_SFU_PORT_PRIORITY;

    table_info = midware_get_table_info(MIDWARE_TABLE_RSTP_PORT);
    for(i = TPM_SRC_PORT_UNI_0; i <= TPM_SRC_PORT_UNI_3; i++)
    {
        rstp_port.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &rstp_port);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for port: %ld\n", i);
            return ONU_FAIL;
        }
    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_rstp_port_get(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_RSTP_PORT_T               *rstp;
    INT32                                                            rc;
    bool                                                   enable;
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;

    rstp = (MIDWARE_TABLE_RSTP_PORT_T *)entry;

    if(bitmap & MIDWARE_ENTRY_RSTP_PORT_STATE)
    {
        rc = RSTP_port_get_enable(rstp->port_id, &enable);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to RSTP_port_get_enable\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        rstp->rstp_en = MIDWARE_BOOL_TO_U8(enable) ;
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    #if 0
    if(bitmap & MIDWARE_ENTRY_RSTP_PORT_PRI)
    {
        rc = RSTP_get_priority(rstp->port_id, &rstp->port_pri);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to RSTP_get_bridge_priority\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }
    #endif

     return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_mc_port_set_to_igmp(MIDWARE_TABLE_MC_PORT_CFG_T *mc_port)
{
    INT32                                                            rc;
    ONU_STATUS      midware_rc;
    MIDWARE_TABLE_MC_DS_VLAN_TRANS_T  *mc_vlan;
    MIDWARE_TABLE_INFO *table_info;
    UINT32    mc_vlan_num;
    MULTICAST_PORT_TAG_OPER_T tag_strip;
    UINT32    i;

    /*
    1. get all the current mc vlan for this port
    2. get mc port cfg for this port
    3. set these info to IGMP module
    */
    memset(&tag_strip, 0, sizeof(tag_strip));

    tag_strip.us_tag_oper = mc_port->us_tag_oper;
    tag_strip.ds_tag_oper = mc_port->ds_tag_oper;
    tag_strip.us_tag_default_tci = mc_port->us_tag_default_tci;
    tag_strip.ds_tag_default_tci = mc_port->ds_tag_default_tci;
    tag_strip.multicast_ds_vlan_switching.number_of_entries = 0;

    mc_vlan_num = midware_sqlite3_get_entry_count(MIDWARE_TABLE_MC_DS_VLAN_TRANS);
    if((tag_strip.ds_tag_oper != IGMP_REPLACE_VLAN_ID_DOWN)
        ||(tag_strip.ds_tag_default_tci != MIDWARE_TAG_TCI_NO_USE) || (mc_vlan_num == 0))
    {
        //not ctc IGMP_REPLACE_VLAN_ID_DOWN or no vlan for this port
        rc = IGMP_set_multicast_tag_strip(mc_port->port_id, &tag_strip);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_set_multicast_tag_strip\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        return MDW_CB_RET_SET_HW_OK;
    }

    mc_vlan = malloc(mc_vlan_num * sizeof(MIDWARE_TABLE_MC_DS_VLAN_TRANS_T));
    if( 0 == mc_vlan )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't malloc mem for MIDWARE_TABLE_MC_DS_VLAN_TRANS_T entry for num: %ld\n", mc_vlan_num);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    midware_rc = midware_sqlite3_get_all_entry(MIDWARE_TABLE_MC_DS_VLAN_TRANS, &mc_vlan_num, mc_vlan);
    if( ONU_FAIL == midware_rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get all_entry for MIDWARE_TABLE_MC_DS_VLAN_TRANS for port: %ld\n", mc_port->port_id);
        free(mc_vlan);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    for(i = 0; i < mc_vlan_num; i++)
    {
        if( mc_vlan[i].port_id != mc_port->port_id)
        {
            continue;
        }

        tag_strip.multicast_ds_vlan_switching.entries[tag_strip.multicast_ds_vlan_switching.number_of_entries].in_vid = mc_vlan[i].in_vid;
        tag_strip.multicast_ds_vlan_switching.entries[tag_strip.multicast_ds_vlan_switching.number_of_entries].out_vid = mc_vlan[i].out_vid;
        tag_strip.multicast_ds_vlan_switching.number_of_entries++;
    }

    rc = IGMP_set_multicast_tag_strip(mc_port->port_id, &tag_strip);
    free(mc_vlan);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_set_multicast_tag_strip\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }
    return MDW_CB_RET_SET_HW_OK;
}

/*the fun must be call for ctc downstream vlan translate vid set*/
MIDWARE_CALLBACK_RET midware_table_mc_ds_vlan_set_to_igmp(MIDWARE_TABLE_MC_DS_VLAN_TRANS_T *entry,
    MIDWARE_MC_VLAN_OPERATION op)
{
    INT32                                                            rc;
    ONU_STATUS      midware_rc;
    MIDWARE_TABLE_MC_PORT_CFG_T  mc_port;
    MIDWARE_TABLE_MC_DS_VLAN_TRANS_T  *mc_vlan;
    MIDWARE_TABLE_INFO *table_info;
    UINT32    mc_vlan_num;
    MULTICAST_PORT_TAG_OPER_T tag_strip;
    UINT32    i;
    UINT32 port_id = entry->port_id;

    /*
    1. get all the current mc vlan for this port
    2. get mc port cfg for this port
    3. set these info to IGMP module
    */
    memset(&tag_strip, 0, sizeof(tag_strip));
    mc_port.port_id = port_id;
    table_info = midware_get_table_info(MIDWARE_TABLE_MC_PORT_CFG);
    midware_rc = midware_sqlite3_get_select_sql_prepared(table_info, &mc_port);
    if( ONU_FAIL == midware_rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get MIDWARE_TABLE_MC_PORT_CFG entry for port: %ld\n", port_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    /*MIDWARE_TAG_TCI_NO_USE mean now is ctc downstream vlan translate*/
    mc_port.ds_tag_default_tci = MIDWARE_TAG_TCI_NO_USE;

    tag_strip.us_tag_oper = mc_port.us_tag_oper;
    tag_strip.ds_tag_oper = mc_port.ds_tag_oper;
    tag_strip.us_tag_default_tci = mc_port.us_tag_default_tci;
    tag_strip.ds_tag_default_tci = mc_port.ds_tag_default_tci;
    tag_strip.multicast_ds_vlan_switching.number_of_entries = 0;

    mc_vlan_num = midware_sqlite3_get_entry_count(MIDWARE_TABLE_MC_DS_VLAN_TRANS);
    if(0 == mc_vlan_num && op != MDW_MC_VLAN_INSETT)
    {
        //no vlan for this port
        rc = IGMP_set_multicast_tag_strip(port_id, &tag_strip);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_set_multicast_tag_strip\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        return MDW_CB_RET_SET_HW_OK;
    }

    mc_vlan = malloc(mc_vlan_num * sizeof(MIDWARE_TABLE_MC_DS_VLAN_TRANS_T));
    if( 0 == mc_vlan )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't malloc mem for MIDWARE_TABLE_MC_DS_VLAN_TRANS_T entry for num: %ld\n", mc_vlan_num);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    midware_rc = midware_sqlite3_get_all_entry(MIDWARE_TABLE_MC_DS_VLAN_TRANS, &mc_vlan_num, mc_vlan);
    if( ONU_FAIL == midware_rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get all_entry for MIDWARE_TABLE_MC_DS_VLAN_TRANS for port: %ld\n", port_id);
        free(mc_vlan);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    for(i = 0; i < mc_vlan_num; i++)
    {
        if( mc_vlan[i].port_id != port_id)
        {
            continue;
        }

        /*is update mc vlan*/
        if(mc_vlan[i].in_vid == entry->in_vid)
        {
            if(MDW_MC_VLAN_UPDATE == op)
            {
                mc_vlan[i].out_vid = entry->out_vid;
            }
            else if(MDW_MC_VLAN_REMOVE == op)
            {
                continue;
            }
        }

        tag_strip.multicast_ds_vlan_switching.entries[tag_strip.multicast_ds_vlan_switching.number_of_entries].in_vid = mc_vlan[i].in_vid;
        tag_strip.multicast_ds_vlan_switching.entries[tag_strip.multicast_ds_vlan_switching.number_of_entries].out_vid = mc_vlan[i].out_vid;
        tag_strip.multicast_ds_vlan_switching.number_of_entries++;
    }

    if(MDW_MC_VLAN_INSETT == op)
    {
        tag_strip.multicast_ds_vlan_switching.entries[tag_strip.multicast_ds_vlan_switching.number_of_entries].in_vid = entry->in_vid;
        tag_strip.multicast_ds_vlan_switching.entries[tag_strip.multicast_ds_vlan_switching.number_of_entries].out_vid = entry->out_vid;
        tag_strip.multicast_ds_vlan_switching.number_of_entries++;
    }

    rc = IGMP_set_multicast_tag_strip(port_id, &tag_strip);
    free(mc_vlan);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_set_multicast_tag_strip\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    midware_rc = midware_sqlite3_get_update_sql_prepared(table_info, MIDWARE_ENTRY_MC_DS_TAG_DEF_TCI, mc_port);
    if( ONU_FAIL == midware_rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't update MIDWARE_ENTRY_MC_DS_TAG_DEF_TCI entry for port: %ld\n", port_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }
    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_rstp_port_set(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_RSTP_PORT_T               *rstp;
    INT32                                                            rc;
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;

    rstp = (MIDWARE_TABLE_RSTP_PORT_T *)entry;

    if(bitmap & MIDWARE_ENTRY_RSTP_PORT_STATE)
    {
        rc = RSTP_port_set_enable(rstp->port_id, MIDWARE_U8_TO_BOOL(rstp->rstp_en));
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to RSTP_port_set_enable\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_RSTP_PORT_PRI)
    {
        rc = RSTP_set_priority(rstp->port_id, rstp->port_pri);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to RSTP_set_bridge_priority\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

     return callback_ret;
}

ONU_STATUS midware_table_mc_cfg_init(void)
{
    MIDWARE_TABLE_MC_CFG_T McCfg;
    ONU_STATUS rc;
    MIDWARE_TABLE_INFO *table_info;

    //get all the attr from XML
    //for now, there is no XML
    memset(&McCfg,0,sizeof(McCfg));
    McCfg.igmp_version       = MIDWARE_IGMP_VERSION_3;
    McCfg.mc_mode            = MIDWARE_MC_TYPE_IGMP_SNOOPING;
    McCfg.filter_mode        = MIDWARE_MC_CTRL_TYPE_DA_VID;
    McCfg.fast_leave_ability = MIDWARE_MC_IGMP_SNOOPING_NON_FAST_LEAVE_SUPPORT|MIDWARE_MC_IGMP_SNOOPING_FAST_LEAVE_SUPPORT|MIDWARE_MC_CTC_CONTROL_FAST_LEAVE_SUPPORT|MIDWARE_MC_CTC_CONTROL_NON_FAST_LEAVE_SUPPORT;
    McCfg.fast_leave_state   = MIDWARE_FAST_LEAVE_ADMIN_STATE_ENABLED;


    //insert the entry into SQLite
    table_info = midware_get_table_info(MIDWARE_TABLE_MC_CFG);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &McCfg);
     if(ONU_FAIL == rc)
     {
         MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entry\n");
         return ONU_FAIL;
     }

     return ONU_OK;
}

MIDWARE_CALLBACK_RET midware_table_mc_cfg_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    MIDWARE_TABLE_MC_CFG_T              *McCfg;
    MULTICAST_CONTROL_T                 mc_control;
    MULTICAST_PROTOCOL_T                multicast_protocol;
    IGMP_FAST_LEAVE_ADMIN_STATE_T       fast_leave_admin_state;
    MULTICAST_FAST_LEAVE_ABILITY_T      fast_leave_ability;
    int i=0;

    McCfg = (MIDWARE_TABLE_MC_CFG_T *)entry;

    if(bitmap & MIDWARE_ENTRY_IMGP_VERSION)
    {
    }

    if(bitmap & MIDWARE_ENTRY_MC_MODE)
    {
        rc = IGMP_get_multicast_switch(&multicast_protocol);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_get_multicast_switch\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        McCfg->mc_mode = (UINT8)multicast_protocol;
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_MC_FILTER_MODE)
    {
        rc = IGMP_get_multicast_control(&mc_control);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_get_multicast_control\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }

        McCfg->filter_mode = (UINT8)mc_control.control_type;
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_MC_FAST_ABILITY)
    {
        rc = IGMP_get_fast_leave_ability(&fast_leave_ability);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_get_multicast_switch\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }

        McCfg->fast_leave_ability = 0;
        for(i=0;i<fast_leave_ability.num_of_modes;i++)
        {
            switch (fast_leave_ability.modes[i])
            {
                case IGMP_SNOOPING_NON_FAST_LEAVE_SUPPORT:
                    McCfg->fast_leave_ability |= 1<<0;
                    break;
                case IGMP_SNOOPING_FAST_LEAVE_SUPPORT:
                    McCfg->fast_leave_ability |= 1<<1;
                    break;
                case IGMP_MC_CONTROL_NON_FAST_LEAVE_SUPPORT:
                    McCfg->fast_leave_ability |= 1<<2;
                    break;
                case IGMP_MC_CONTROL_FAST_LEAVE_SUPPORT:
                    McCfg->fast_leave_ability |= 1<<3;
                    break;
                case IGMP_MLD_SNOOPING_NON_FAST_LEAVE_SUPPORT:
                    McCfg->fast_leave_ability |= 1<<4;
                    break;
                case IGMP_MLD_SNOOPING_FAST_LEAVE_SUPPORT:
                    McCfg->fast_leave_ability |= 1<<5;
                    break;
                default:
                    break;
            }
        }

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_MC_FAST_STATE)
    {
        rc = IGMP_get_fast_leave_admin_state(&fast_leave_admin_state);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_get_multicast_switch\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        McCfg->fast_leave_state = (UINT8)fast_leave_admin_state;
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

     return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_mc_cfg_set(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_MC_CFG_T              *McCfg;
    MULTICAST_CONTROL_T                 mc_control;
    MULTICAST_PROTOCOL_T                multicast_protocol;
    IGMP_FAST_LEAVE_ADMIN_STATE_T       fast_leave_admin_state;
    INT32                               rc;
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;

    McCfg = (MIDWARE_TABLE_MC_CFG_T *)entry;

    if(bitmap & MIDWARE_ENTRY_IMGP_VERSION)
    {
        multicast_protocol = (MULTICAST_PROTOCOL_T)McCfg->mc_mode;
        rc = IGMP_set_multicast_switch(&multicast_protocol);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_set_multicast_switch\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_MC_MODE)
    {
        multicast_protocol = (MULTICAST_PROTOCOL_T)McCfg->mc_mode;
        rc = IGMP_set_multicast_switch(&multicast_protocol);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_set_multicast_switch\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_MC_FILTER_MODE)
    {
        rc = IGMP_get_multicast_control(&mc_control);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_get_multicast_control\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }

        mc_control.control_type = (MULTICAST_CONTROL_TYPE_T)McCfg->filter_mode;
        rc = IGMP_set_multicast_control(&mc_control);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_get_multicast_control\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_MC_FAST_STATE)
    {
        fast_leave_admin_state = (IGMP_FAST_LEAVE_ADMIN_STATE_T)McCfg->fast_leave_state;
        rc = IGMP_set_fast_leave_admin_control(&fast_leave_admin_state);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_set_fast_leave_admin_control\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

     return callback_ret;
}

ONU_STATUS midware_table_mc_port_init(void)
{
    ONU_STATUS                     rc;
    MIDWARE_TABLE_MC_PORT_CFG_T    mc_port;
    UINT32                         i;
    MIDWARE_TABLE_INFO            *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_MC_PORT_CFG))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&mc_port, 0, sizeof(mc_port));
    mc_port.us_igmp_rate          = MIDWARE_IGMP_MAX_RATE;
    mc_port.max_group_num         = MIDWARE_SFU_MAX_GROUP_NUM;
    mc_port.us_tag_oper           = IGMP_NO_OP_VLAN_TAG;
    mc_port.us_tag_default_tci    = MIDWARE_TAG_TCI_NO_USE;
    mc_port.ds_tag_default_tci    = MIDWARE_TAG_TCI_NO_USE;
    mc_port.robust                = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port.query_interval        = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port.query_interval        = MIDWARE_IGMP_QUERY_DEFT_INTERVAL;
    mc_port.query_max_resp_time   = MIDWARE_IGMP_MAX_RESP_TIME;
    mc_port.last_query_time       = MIDWARE_IGMP_LAST_QUERY_TIME;
    mc_port.unauth_join_behaviour = MIDWARE_DEFAULT_ZERO_VAL;

    table_info = midware_get_table_info(MIDWARE_TABLE_MC_PORT_CFG);
    for(i = TPM_SRC_PORT_UNI_0; i <= TPM_SRC_PORT_UNI_3; i++)
    {
        mc_port.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &mc_port);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_MC_PORT_CFG port: %ld\n", i);
            return ONU_FAIL;
        }
    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_mc_port_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    MIDWARE_TABLE_MC_PORT_CFG_T    *mc_port;
    MULTICAST_PORT_TAG_OPER_T tag_oper;

    mc_port = (MIDWARE_TABLE_MC_PORT_CFG_T *)entry;

    if(bitmap & MIDWARE_ENTRY_MC_MAX_GROUP)
    {
        rc = IGMP_get_multicast_group_num(mc_port->port_id, &mc_port->max_group_num);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_get_multicast_group_num\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_MC_US_TAG_OPER)
    {
        rc = IGMP_get_multicast_tag_strip(mc_port->port_id, &tag_oper);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_get_multicast_tag_strip\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        mc_port->us_tag_oper = tag_oper.us_tag_oper;
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_MC_DS_TAG_OPER)
    {
        rc = IGMP_get_multicast_tag_strip(mc_port->port_id, &tag_oper);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_get_multicast_tag_strip\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        mc_port->ds_tag_oper = tag_oper.ds_tag_oper;
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

     return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_mc_port_set(UINT32 bitmap, void *entry)
{
    INT32                            rc;
    MIDWARE_CALLBACK_RET             callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    MIDWARE_TABLE_MC_PORT_CFG_T      mc_port;
    MIDWARE_TABLE_MC_PORT_CFG_T     *p_mc_port_cfg;
    MIDWARE_TABLE_INFO              *table_info;
    ONU_STATUS                       midware_rc;

    p_mc_port_cfg = (MIDWARE_TABLE_MC_PORT_CFG_T *)entry;

    mc_port.port_id = p_mc_port_cfg->port_id;
    table_info = midware_get_table_info(MIDWARE_TABLE_MC_PORT_CFG);
    midware_rc = midware_sqlite3_get_select_sql_prepared(table_info, &mc_port);
    if( ONU_FAIL == midware_rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get MIDWARE_TABLE_MC_PORT_CFG entry for port: %ld\n", p_mc_port_cfg->port_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }


    if(bitmap & MIDWARE_ENTRY_MC_MAX_GROUP)
    {
        rc = IGMP_set_multicast_group_num(p_mc_port_cfg->port_id, p_mc_port_cfg->max_group_num);
        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to IGMP_set_multicast_group_num\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_MC_US_TAG_OPER)
    {
        mc_port.us_tag_oper = p_mc_port_cfg->us_tag_oper;
    }

    if(bitmap & MIDWARE_ENTRY_MC_DS_TAG_OPER)
    {
        mc_port.ds_tag_oper = p_mc_port_cfg->ds_tag_oper;
    }


    if(bitmap & MIDWARE_ENTRY_MC_US_TAG_DEF_TCI)
    {
        mc_port.us_tag_default_tci = p_mc_port_cfg->us_tag_default_tci;
    }

    if(bitmap & MIDWARE_ENTRY_MC_DS_TAG_DEF_TCI)
    {
        mc_port.ds_tag_default_tci = p_mc_port_cfg->ds_tag_default_tci;
    }

    if(bitmap & (MIDWARE_ENTRY_MC_US_TAG_OPER | MIDWARE_ENTRY_MC_DS_TAG_OPER | MIDWARE_ENTRY_MC_US_TAG_DEF_TCI | MIDWARE_ENTRY_MC_DS_TAG_DEF_TCI))
    {
        callback_ret = midware_table_mc_port_set_to_igmp(&mc_port);
        if (MDW_CB_RET_SET_HW_FAIL == rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to midware_table_mc_vlan_set\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
    }

     return callback_ret;
}

ONU_STATUS midware_table_mc_port_status_init(void)
{
    ONU_STATUS                        rc;
    MIDWARE_TABLE_MC_PORT_STATUS_T    mc_port_status;
    UINT32                            i;
    MIDWARE_TABLE_INFO               *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_MC_PORT_STATUS))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&mc_port_status, 0, sizeof(mc_port_status));
    mc_port_status.mc_current_bw      = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_status.join_count         = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_status.exceed_bw_count    = MIDWARE_DEFAULT_ZERO_VAL;

    table_info = midware_get_table_info(MIDWARE_TABLE_MC_PORT_STATUS);
    for(i = TPM_SRC_PORT_UNI_0; i <= TPM_SRC_PORT_UNI_3; i++)
    {
        mc_port_status.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &mc_port_status);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_MC_PORT_STATUS port: %ld\n", i);
            return ONU_FAIL;
        }
    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_mc_port_status_get(UINT32 bitmap, void *entry)
{
    INT32                            rc;
    MIDWARE_CALLBACK_RET             callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    MIDWARE_TABLE_MC_PORT_STATUS_T  *mc_port;

    mc_port = (MIDWARE_TABLE_MC_PORT_STATUS_T *)entry;

    if(bitmap & MIDWARE_ENTRY_MC_CURRENT_BW)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_MC_JOIN_MSG_COUNTET)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_MC_EXCEED_BW_COUNT)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

     return callback_ret;
}

ONU_STATUS midware_table_mc_port_acl_init(void)
{
    ONU_STATUS                        rc;
    MIDWARE_TABLE_MC_PORT_CTRL_T      mc_port_acl;
    UINT32                            i;
    MIDWARE_TABLE_INFO               *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_MC_PORT_CONTROL))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&mc_port_acl, 0, sizeof(mc_port_acl));
    mc_port_acl.table_type           = MIDWARE_MC_ACL_DYNAMIC_TABLE;
    mc_port_acl.set_ctrl             = MIDWARE_MC_SET_TYPE_RESERVED;
    mc_port_acl.row_key              = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_acl.gem_port             = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_acl.src_ip               = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_acl.dst_ip_start         = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_acl.dst_ip_end           = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_acl.imputed_group_bw     = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_acl.preview_len          = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_acl.preview_repeat_time  = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_acl.preview_repeat_count = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_acl.preview_reset_time   = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_acl.vendor_spec          = MIDWARE_DEFAULT_ZERO_VAL;

    table_info = midware_get_table_info(MIDWARE_TABLE_MC_PORT_CONTROL);
    for(i = TPM_SRC_PORT_UNI_0; i <= TPM_SRC_PORT_UNI_3; i++)
    {
        mc_port_acl.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &mc_port_acl);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_MC_PORT_CONTROL port: %ld\n", i);
            return ONU_FAIL;
        }
    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_mc_port_acl_set(UINT32 bitmap, void *entry)
{
    INT32                         rc;
    MIDWARE_CALLBACK_RET          callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    mrvl_eth_mc_port_ctrl_t       mc_port_acl;
    MIDWARE_TABLE_MC_PORT_CTRL_T *p_mc_port_acl;
    MIDWARE_TABLE_INFO           *table_info;
    ONU_STATUS                    midware_rc;

    p_mc_port_acl = (MIDWARE_TABLE_MC_PORT_CTRL_T *)entry;

    mc_port_acl.port_id = p_mc_port_acl->port_id;

    table_info = midware_get_table_info(MIDWARE_TABLE_MC_PORT_CONTROL);
    midware_rc = midware_sqlite3_get_select_sql_prepared(table_info, &mc_port_acl);
    if( ONU_FAIL == midware_rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get MIDWARE_TABLE_MC_PORT_CONTROL entry for port: %ld\n", p_mc_port_acl->port_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_TABLE_TYPE)
    {
        mc_port_acl.table_type = p_mc_port_acl->table_type;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_SET_TYPE)
    {
        mc_port_acl.set_ctrl = p_mc_port_acl->set_ctrl;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_ROW_KEY)
    {
        mc_port_acl.row_key = p_mc_port_acl->row_key;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_GEM_PORT)
    {
        mc_port_acl.gem_port = p_mc_port_acl->gem_port;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_VID)
    {
        mc_port_acl.ani_vid = p_mc_port_acl->ani_vid;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_SRC_IP)
    {
        mc_port_acl.src_ip = p_mc_port_acl->src_ip;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_DST_IP_START)
    {
        mc_port_acl.dst_ip_start = p_mc_port_acl->dst_ip_start;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_DST_IP_END)
    {
        mc_port_acl.dst_ip_end = p_mc_port_acl->dst_ip_end;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_GROUP_BW)
    {
     mc_port_acl.imputed_group_bw = p_mc_port_acl->imputed_group_bw;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_SRC_IPV6)
    {
        memcpy(mc_port_acl.src_ipv6, p_mc_port_acl->src_ipv6, sizeof(mc_port_acl.src_ipv6));
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_DST_IPV6)
    {
        memcpy(mc_port_acl.dst_ipv6, p_mc_port_acl->dst_ipv6, sizeof(mc_port_acl.dst_ipv6));
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_PREVIEW_LEN)
    {
        mc_port_acl.preview_len = p_mc_port_acl->preview_len;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_PREVIEW_TIME)
    {
        mc_port_acl.preview_repeat_time = p_mc_port_acl->preview_repeat_time;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_PREVIEW_COUNT)
    {
     mc_port_acl.preview_repeat_count = p_mc_port_acl->preview_repeat_count;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_PREVIEW_RESET)
    {
        mc_port_acl.preview_reset_time = p_mc_port_acl->preview_reset_time;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_CTRL_VENDOR_SPEC)
    {
        mc_port_acl.vendor_spec = p_mc_port_acl->vendor_spec;
    }

    rc = IGMP_set_acl_rule(&mc_port_acl);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to call IGMP_set_acl_rule\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    return callback_ret;
}


ONU_STATUS midware_table_mc_port_serv_init(void)
{
    ONU_STATUS                        rc;
    MIDWARE_TABLE_MC_PORT_SERV_T      mc_port_serv;
    UINT32                            i;
    MIDWARE_TABLE_INFO               *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_MC_PORT_SERV))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&mc_port_serv, 0, sizeof(mc_port_serv));
    mc_port_serv.set_ctrl        = MIDWARE_MC_SET_TYPE_RESERVED;
    mc_port_serv.row_key         = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_serv.vid             = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_serv.max_sum_group   = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_serv.max_mc_bw       = MIDWARE_DEFAULT_ZERO_VAL;

    table_info = midware_get_table_info(MIDWARE_TABLE_MC_PORT_SERV);
    for(i = TPM_SRC_PORT_UNI_0; i <= TPM_SRC_PORT_UNI_3; i++)
    {
        mc_port_serv.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &mc_port_serv);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_MC_PORT_SERV port: %ld\n", i);
            return ONU_FAIL;
        }
    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_mc_port_serv_set(UINT32 bitmap, void *entry)
{
    INT32                         rc;
    MIDWARE_CALLBACK_RET          callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    mrvl_eth_mc_port_serv_t       mc_port_serv;
    MIDWARE_TABLE_MC_PORT_SERV_T *p_mc_port_serv;
    MIDWARE_TABLE_INFO           *table_info;
    ONU_STATUS                    midware_rc;

    p_mc_port_serv = (MIDWARE_TABLE_MC_PORT_SERV_T *)entry;

    mc_port_serv.port_id = p_mc_port_serv->port_id;

    table_info = midware_get_table_info(MIDWARE_TABLE_MC_PORT_SERV);
    midware_rc = midware_sqlite3_get_select_sql_prepared(table_info, &mc_port_serv);
    if( ONU_FAIL == midware_rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get MIDWARE_TABLE_MC_PORT_SERV entry for port: %ld\n", p_mc_port_serv->port_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }


    if(bitmap & MIDWARE_ENTRY_MC_PORT_SRV_SET_TYPE)
    {
        mc_port_serv.set_ctrl = p_mc_port_serv->set_ctrl;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_SRV_ROW_KEY)
    {
        mc_port_serv.row_key = p_mc_port_serv->row_key;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_SRV_VID)
    {
        mc_port_serv.vid = p_mc_port_serv->vid;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_SRV_MAX_GROUP)
    {
        mc_port_serv.max_sum_group = p_mc_port_serv->max_sum_group;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_SRV_MAX_BW)
    {
        mc_port_serv.max_mc_bw = p_mc_port_serv->max_mc_bw;
    }

    rc = IGMP_set_serv_rule(&mc_port_serv);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to call IGMP_set_serv_rule\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    return callback_ret;
}

ONU_STATUS midware_table_mc_port_preview_init(void)
{
    ONU_STATUS                        rc;
    MIDWARE_TABLE_MC_PORT_PREVIEW_T   mc_port_preview;
    UINT32                            i;
    MIDWARE_TABLE_INFO               *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_MC_PORT_PREVIEW))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&mc_port_preview, 0, sizeof(mc_port_preview));
    mc_port_preview.set_ctrl        = MIDWARE_MC_SET_TYPE_RESERVED;
    mc_port_preview.row_key         = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_preview.ani_vid         = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_preview.uni_vid         = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_preview.duration        = MIDWARE_DEFAULT_ZERO_VAL;
    mc_port_preview.time_left       = MIDWARE_DEFAULT_ZERO_VAL;

    table_info = midware_get_table_info(MIDWARE_TABLE_MC_PORT_PREVIEW);
    for(i = TPM_SRC_PORT_UNI_0; i <= TPM_SRC_PORT_UNI_3; i++)
    {
        mc_port_preview.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &mc_port_preview);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_MC_PORT_PREVIEW port: %ld\n", i);
            return ONU_FAIL;
        }
    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_mc_port_preview_set(UINT32 bitmap, void *entry)
{
    INT32                            rc;
    MIDWARE_CALLBACK_RET             callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    mrvl_eth_mc_port_preview_t       mc_port_preview;
    MIDWARE_TABLE_MC_PORT_PREVIEW_T *p_mc_port_preview;
    MIDWARE_TABLE_INFO              *table_info;
    ONU_STATUS                       midware_rc;

    p_mc_port_preview = (MIDWARE_TABLE_MC_PORT_PREVIEW_T *)entry;

    mc_port_preview.port_id = p_mc_port_preview->port_id;

    table_info = midware_get_table_info(MIDWARE_TABLE_MC_PORT_PREVIEW);
    midware_rc = midware_sqlite3_get_select_sql_prepared(table_info, &mc_port_preview);
    if( ONU_FAIL == midware_rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get MIDWARE_TABLE_MC_PORT_PREVIEW entry for port: %ld\n", p_mc_port_preview->port_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_PREV_SET_TYPE)
    {
        mc_port_preview.set_ctrl = p_mc_port_preview->set_ctrl;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_PREV_SRC_IP)
    {
        memcpy(mc_port_preview.src_ip, p_mc_port_preview->src_ip, sizeof(mc_port_preview.src_ip));
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_PREV_DST_IP)
    {
        memcpy(mc_port_preview.dst_ip, p_mc_port_preview->dst_ip, sizeof(mc_port_preview.dst_ip));
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_PREV_ANI_VID)
    {
        mc_port_preview.ani_vid = p_mc_port_preview->ani_vid;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_PREV_UNI_VID)
    {
        mc_port_preview.uni_vid = p_mc_port_preview->uni_vid;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_PREV_DURATION)
    {
        mc_port_preview.duration = p_mc_port_preview->duration;
    }

    if(bitmap & MIDWARE_ENTRY_MC_PORT_PREV_TIME_LEFT)
    {
        mc_port_preview.time_left = p_mc_port_preview->time_left;
    }

    rc = IGMP_set_preview_rule(&mc_port_preview);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to call IGMP_set_preview_rule\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    return callback_ret;

}

MIDWARE_CALLBACK_RET midware_table_mc_ds_vlan_set(UINT32 bitmap, void *entry)
{
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    MIDWARE_TABLE_MC_DS_VLAN_TRANS_T *mc_vlan;

    mc_vlan = (MIDWARE_TABLE_MC_DS_VLAN_TRANS_T *)entry;

    callback_ret = midware_table_mc_ds_vlan_set_to_igmp(mc_vlan, MDW_MC_VLAN_UPDATE);
    if (MDW_CB_RET_SET_HW_FAIL == callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to midware_table_mc_ds_vlan_set_to_igmp\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_mc_ds_vlan_insert(UINT32 bitmap, void *entry)
{
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    MIDWARE_TABLE_MC_DS_VLAN_TRANS_T *mc_vlan;

    mc_vlan = (MIDWARE_TABLE_MC_DS_VLAN_TRANS_T *)entry;

    callback_ret = midware_table_mc_ds_vlan_set_to_igmp(mc_vlan, MDW_MC_VLAN_INSETT);
    if (MDW_CB_RET_SET_HW_FAIL == callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to midware_table_mc_ds_vlan_set_to_igmp\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_mc_ds_vlan_remove(UINT32 bitmap, void *entry)
{
    MIDWARE_CALLBACK_RET                callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    MIDWARE_TABLE_MC_DS_VLAN_TRANS_T *mc_vlan;

    mc_vlan = (MIDWARE_TABLE_MC_DS_VLAN_TRANS_T *)entry;

    callback_ret = midware_table_mc_ds_vlan_set_to_igmp(mc_vlan, MDW_MC_VLAN_REMOVE);
    if (MDW_CB_RET_SET_HW_FAIL == callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to midware_table_mc_ds_vlan_set_to_igmp\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_mc_us_vlan_set(UINT32 bitmap, void *entry)
{
     return MDW_CB_RET_SET_HW_NO_NEED;
}

MIDWARE_CALLBACK_RET midware_table_mc_us_vlan_insert(UINT32 bitmap, void *entry)
{

     return MDW_CB_RET_SET_HW_NO_NEED;
}

MIDWARE_CALLBACK_RET midware_table_mc_us_vlan_remove(UINT32 bitmap, void *entry)
{
     return MDW_CB_RET_SET_HW_NO_NEED;
}

ONU_STATUS midware_table_iad_port_admin_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_POTS_ADMIN_T    iad_port_admin;
    UINT32             i;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_IAD_PORT_ADMIN))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    iad_port_admin.port_admin = MIDWARE_SFU_DISABLE;

    table_info = midware_get_table_info(MIDWARE_TABLE_IAD_PORT_ADMIN);
    for(i = 1; i <= 2; i++)
    {
        iad_port_admin.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &iad_port_admin);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_IAD_PORT_ADMIN port: %ld\n", i);
            return ONU_FAIL;
        }
    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_iad_port_admin_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_POTS_ADMIN_T    *iad_port_admin;
    VOIP_ST_PORT_ACTIVE_STATE info;

    iad_port_admin = (MIDWARE_TABLE_POTS_ADMIN_T *)entry;

    rc = VOIP_getPortActiveState_F(iad_port_admin->port_id, &info);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetPortActiveState_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }
    if(info == VOIP_ETH_PORT_ADMIN_ACTIVATE)
        iad_port_admin->port_admin = MIDWARE_UNI_CFG_ENABLE;
    else
        iad_port_admin->port_admin = MIDWARE_UNI_CFG_DISABLE;


     return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_iad_port_admin_set(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_POTS_ADMIN_T    *iad_port_admin;
    VOIP_ST_PORT_ACTIVE_STATE info;

    iad_port_admin = (MIDWARE_TABLE_POTS_ADMIN_T *)entry;

    if(iad_port_admin->port_admin  == MIDWARE_UNI_CFG_ENABLE)
        info = VOIP_ETH_PORT_ADMIN_ACTIVATE;
    else
        info = VOIP_ETH_PORT_ADMIN_DEACTIVATE;

    rc = VOIP_setPortActiveState_F(iad_port_admin->port_id, &info);

    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_SetPortActiveState_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_iad_info_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_IAD_INFO_T    iad_info;
    MIDWARE_TABLE_INFO *table_info;

    //init the default entries
    memset(&iad_info, 0, sizeof(iad_info));

    midware_mv_ext_get_if_mac("pon0",iad_info.mac_addr);
    iad_info.pots_num = MIDWARE_SFU_POTS_PORT_NUM;
    iad_info.protocol = MIDWARE_SFU_PROTOCOL_SIP;
    table_info = midware_get_table_info(MIDWARE_TABLE_IAD_INFO);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &iad_info);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_IAD_INFO \n");
        return ONU_FAIL;
    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_iad_info_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_IAD_INFO_T   *midware_iad_info;
    VOIP_ST_IADINFO voip_iad_info;

    midware_iad_info = (MIDWARE_TABLE_IAD_INFO_T *)entry;

    rc = VOIP_getIadInfo_F(&voip_iad_info);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetIadInfo_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    memcpy(midware_iad_info->mac_addr, voip_iad_info.mac_address, sizeof(midware_iad_info->mac_addr));
    midware_iad_info->protocol = voip_iad_info.voip_protocol;
    memcpy(midware_iad_info->sw_ver, voip_iad_info.sw_version, sizeof(midware_iad_info->sw_ver));
    memcpy(midware_iad_info->sw_time, voip_iad_info.sw_time, sizeof(midware_iad_info->sw_time));
    midware_iad_info->pots_num = voip_iad_info.user_count;

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_iad_cfg_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_IAD_CFG_T     iad_cfg;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_IAD_CFG))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&iad_cfg, 0, sizeof(iad_cfg));
    iad_cfg.ip_mode = MIDWARE_SFU_VOICE_IP_MODE_STATIC;

    table_info = midware_get_table_info(MIDWARE_TABLE_IAD_CFG);
    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &iad_cfg);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_IAD_CFG  \n");
        return ONU_FAIL;
    }
    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_iad_cfg_get(UINT32 bitmap, void *entry)
{
/*
    INT32                                                            rc;
    ONU_STATUS      midware_rc;
    MIDWARE_TABLE_IAD_CFG_T     *midware_iad_cfg;
    MIDWARE_TABLE_INFO *table_info;
    VOIP_ST_SIP_PARAMETER_CONFIG voip_iad_cfg;

    midware_iad_cfg = (MIDWARE_TABLE_IAD_CFG_T *)entry;

    rc = VOIP_GetSipParamConfig_F(&voip_iad_cfg);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetIadInfo_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    memcpy(midware_iad_info->mac_addr, voip_iad_info.mac_address, sizeof(midware_iad_info->mac_addr));
    midware_iad_info->protocol = voip_iad_info.voip_protocol;
    memcpy(midware_iad_info->sw_ver, voip_iad_info.sw_version, sizeof(midware_iad_info->sw_ver));
    memcpy(midware_iad_info->sw_time, voip_iad_info.sw_time, sizeof(midware_iad_info->sw_time));
    midware_iad_info->pots_num = voip_iad_info.user_count;
*/
     return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_iad_cfg_set(UINT32 bitmap, void *entry)
{

    INT32                                                            rc;
    MIDWARE_TABLE_IAD_CFG_T     *midware_iad_cfg;
    VOIP_GLOBAL_PARAM_T voip_iad_cfg;

    midware_iad_cfg = (MIDWARE_TABLE_IAD_CFG_T *)entry;

    voip_iad_cfg.voice_ip_mode =                         midware_iad_cfg->ip_mode ;
    voip_iad_cfg.iad_ip_addr =                           midware_iad_cfg->ip_addr ;
    voip_iad_cfg.iad_net_mask =                          midware_iad_cfg->net_mask ;
    voip_iad_cfg.iad_def_gw =                            midware_iad_cfg->def_gw ;
    voip_iad_cfg.pppoe_mode =                            midware_iad_cfg->pppoe_mode ;
    memcpy(voip_iad_cfg.pppoe_user,  midware_iad_cfg->pppoe_user, sizeof(voip_iad_cfg.pppoe_user));
    memcpy(voip_iad_cfg.pppoe_passwd,  midware_iad_cfg->pppoe_pwd, sizeof(voip_iad_cfg.pppoe_passwd));
    voip_iad_cfg.vlan_tag_mode =                         midware_iad_cfg->tag_flag ;
    voip_iad_cfg.cvlan_id =                              midware_iad_cfg->cvlan ;
    voip_iad_cfg.svlan_id =                              midware_iad_cfg->svlan ;
    voip_iad_cfg.priority =                              midware_iad_cfg->pbits ;

    rc = VOIP_setGlobalParam_F(&voip_iad_cfg);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_SetGlobalParam_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_h248_cfg_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_H248_CFG_T     h248_cfg;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_H248_CFG))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&h248_cfg, 0, sizeof(h248_cfg));
    h248_cfg.mg_port = MIDWARE_SFU_H248_MG_PORT_NUM;
    h248_cfg.heartbeat_mode = MIDWARE_SFU_H248_HEARTBEAT_MODE_CLOSE;
    h248_cfg.heartbeat_cycle = MIDWARE_SFU_HEARTBEAT_CYCLE;
    h248_cfg.heartbeat_count = MIDWARE_SFU_HEARTBEAT_HEARTBEAT_COUNT;

    table_info = midware_get_table_info(MIDWARE_TABLE_H248_CFG);
    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &h248_cfg);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_H248_CFG  \n");
        return ONU_FAIL;
    }
    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_h248_cfg_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_H248_CFG_T   *midware_h248_cfg;
    VOIP_ST_H248_PARAMETER_CONFIG voip_h248_cfg;

    midware_h248_cfg = (MIDWARE_TABLE_H248_CFG_T *)entry;

    rc = VOIP_getH248ParamConfig_F(&voip_h248_cfg);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetH248ParamConfig_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }


    midware_h248_cfg->mg_port = voip_h248_cfg.mg_port;
    midware_h248_cfg->mgc_ip = voip_h248_cfg.mgcip;
    midware_h248_cfg->mgc_port = voip_h248_cfg.mgccom_port_num;
    midware_h248_cfg->backup_mgc_ip = voip_h248_cfg.back_mgcip;
    midware_h248_cfg->backup_mgc_port = voip_h248_cfg.back_mgccom_port_num;
    midware_h248_cfg->active_mgc = voip_h248_cfg.active_mgc;
    midware_h248_cfg->reg_mode = voip_h248_cfg.reg_mode;
    memcpy(midware_h248_cfg->mg_id, voip_h248_cfg.mid, sizeof(midware_h248_cfg->mg_id));
    midware_h248_cfg->heartbeat_mode = voip_h248_cfg.heart_beat_mode;
    midware_h248_cfg->heartbeat_cycle = voip_h248_cfg.heart_beat_cycle;
    midware_h248_cfg->heartbeat_count = voip_h248_cfg.heart_beat_count;

     return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_h248_cfg_set(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_H248_CFG_T   *midware_h248_cfg;
    VOIP_ST_H248_PARAMETER_CONFIG voip_h248_cfg;

    midware_h248_cfg = (MIDWARE_TABLE_H248_CFG_T *)entry;

    voip_h248_cfg.mg_port = midware_h248_cfg->mg_port;
    voip_h248_cfg.mgcip = midware_h248_cfg->mgc_ip;
    voip_h248_cfg.mgccom_port_num = midware_h248_cfg->mgc_port;
    voip_h248_cfg.back_mgcip = midware_h248_cfg->backup_mgc_ip;
    voip_h248_cfg.back_mgccom_port_num = midware_h248_cfg->backup_mgc_port;
    voip_h248_cfg.active_mgc = midware_h248_cfg->active_mgc;
    voip_h248_cfg.reg_mode = midware_h248_cfg->reg_mode;
    memcpy(voip_h248_cfg.mid, midware_h248_cfg->mg_id, sizeof(voip_h248_cfg.mid));
    voip_h248_cfg.heart_beat_mode = midware_h248_cfg->heartbeat_mode;
    voip_h248_cfg.heart_beat_cycle = midware_h248_cfg->heartbeat_cycle;
    voip_h248_cfg.heart_beat_count = midware_h248_cfg->heartbeat_count;

    rc = VOIP_setH248ParamConfig_F(&voip_h248_cfg);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_SetH248ParamConfig_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_h248_user_info_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_H248_USER_INFO_T     h248_user_info;
    MIDWARE_TABLE_INFO *table_info;
    UINT32 i;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_H248_USER_INFO))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&h248_user_info, 0, sizeof(h248_user_info));

    table_info = midware_get_table_info(MIDWARE_TABLE_H248_USER_INFO);
    for(i = 1; i <= 2; i++)
    {
        h248_user_info.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &h248_user_info);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_H248_USER_INFO  \n");
            return ONU_FAIL;
        }
    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_h248_user_info_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_H248_USER_INFO_T   *midware_h248_user_info;
    VOIP_ST_H248_USER_TIDINFO voip_h248_user_info;

    midware_h248_user_info = (MIDWARE_TABLE_H248_USER_INFO_T *)entry;

    rc = VOIP_getH248UserTIDInfo_F(midware_h248_user_info->port_id, &voip_h248_user_info);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetH248ParamConfig_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    memcpy(midware_h248_user_info->user_name, voip_h248_user_info.user_tid_name, sizeof(midware_h248_user_info->user_name));

     return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_h248_user_info_set(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_H248_USER_INFO_T   *midware_h248_user_info;
    VOIP_ST_H248_USER_TIDINFO voip_h248_user_info;

    midware_h248_user_info = (MIDWARE_TABLE_H248_USER_INFO_T *)entry;

    memcpy(voip_h248_user_info.user_tid_name, midware_h248_user_info->user_name, sizeof(voip_h248_user_info.user_tid_name));

    rc = VOIP_setH248UserTIDInfo_F(midware_h248_user_info->port_id, &voip_h248_user_info);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetH248ParamConfig_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_h248_rtp_cfg_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_H248_RTP_CFG_T     h248_rtp_cfg;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_H248_RTP_CFG))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&h248_rtp_cfg, 0, sizeof(h248_rtp_cfg));

    table_info = midware_get_table_info(MIDWARE_TABLE_H248_RTP_CFG);
    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &h248_rtp_cfg);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_H248_RTP_CFG  \n");
        return ONU_FAIL;
    }
    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_h248_rtp_cfg_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_H248_RTP_CFG_T   *midware_h248_rtp_cfg;
    VOIP_ST_H248_RTP_TID_CONFIG voip_h248_rtp_cfg;

    midware_h248_rtp_cfg = (MIDWARE_TABLE_H248_RTP_CFG_T *)entry;

    rc = VOIP_getH248RtpTIDConfig_F(&voip_h248_rtp_cfg);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetH248RtpTIDConfig_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     midware_h248_rtp_cfg->num_of_rtp = voip_h248_rtp_cfg.num_of_rtp_tid;
     memcpy(midware_h248_rtp_cfg->rtp_prefix, voip_h248_rtp_cfg.rtp_tid_prefix, sizeof(midware_h248_rtp_cfg->rtp_prefix));
     memcpy(midware_h248_rtp_cfg->rtp_digit, voip_h248_rtp_cfg.rtp_tid_digit_begin, sizeof(midware_h248_rtp_cfg->rtp_digit));
     midware_h248_rtp_cfg->rtp_mode = voip_h248_rtp_cfg.rtp_tid_mode;
     midware_h248_rtp_cfg->rtp_digit_len = voip_h248_rtp_cfg.rtp_tid_digit_length;

     return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_h248_rtp_cfg_set(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_H248_RTP_CFG_T   *midware_h248_rtp_cfg;
    VOIP_ST_H248_RTP_TID_CONFIG voip_h248_rtp_cfg;

    midware_h248_rtp_cfg = (MIDWARE_TABLE_H248_RTP_CFG_T *)entry;

     voip_h248_rtp_cfg.num_of_rtp_tid = midware_h248_rtp_cfg->num_of_rtp;
     memcpy(voip_h248_rtp_cfg.rtp_tid_prefix, midware_h248_rtp_cfg->rtp_prefix, sizeof(voip_h248_rtp_cfg.rtp_tid_prefix));
     memcpy(voip_h248_rtp_cfg.rtp_tid_digit_begin, midware_h248_rtp_cfg->rtp_digit, sizeof(voip_h248_rtp_cfg.rtp_tid_digit_begin));
     voip_h248_rtp_cfg.rtp_tid_mode = midware_h248_rtp_cfg->rtp_mode;
     voip_h248_rtp_cfg.rtp_tid_digit_length = midware_h248_rtp_cfg->rtp_digit_len;

     rc = VOIP_setH248RtpTIDConfig_F(&voip_h248_rtp_cfg);
     if (OK != rc)
     {
         MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_SetH248RtpTIDConfig_F\r\n");
         return MDW_CB_RET_SET_HW_FAIL;
     }

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_h248_rtp_info_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_H248_RTP_INFO_T     h248_rtp_info;
    MIDWARE_TABLE_INFO *table_info;

    //init the default entries
    memset(&h248_rtp_info, 0, sizeof(h248_rtp_info));

    table_info = midware_get_table_info(MIDWARE_TABLE_H248_RTP_INFO);

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &h248_rtp_info);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_H248_RTP_INFO  \n");
        return ONU_FAIL;
    }

    return ONU_OK;

}
MIDWARE_CALLBACK_RET midware_table_h248_rtp_info_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_H248_RTP_INFO_T     *midware_h248_rtp_info;
    VOIP_ST_H248_RTP_TID_INFO                  voip_h248_rtp_info;

    midware_h248_rtp_info = (MIDWARE_TABLE_H248_RTP_INFO_T *)entry;

     rc = VOIP_getH248RtpTIDInfo_F(&voip_h248_rtp_info);
     if (OK != rc)
     {
         MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_SetH248RtpTIDConfig_F\r\n");
         return MDW_CB_RET_SET_HW_FAIL;
     }

     midware_h248_rtp_info->num_of_rtp = voip_h248_rtp_info.num_of_rtp_tid;
     memcpy(midware_h248_rtp_info->first_rtp_name, voip_h248_rtp_info.rtp_tid_name, sizeof(midware_h248_rtp_info->first_rtp_name));

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_sip_cfg_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_SIP_CFG_T     sip_cfg;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_SIP_CFG))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&sip_cfg, 0, sizeof(sip_cfg));
    sip_cfg.mg_port = MIDWARE_SFU_SIP_MG_PORT_NUM;
    sip_cfg.reg_interval = MIDWARE_SFU_SIP_REG_INTERVAL;
    sip_cfg.heartbeat_switch = MIDWARE_SFU_SIP_HEARTBEAT_SWITCH_OPEN;
    sip_cfg.heartbeat_cycle = MIDWARE_SFU_HEARTBEAT_CYCLE;
    sip_cfg.heartbeat_count = MIDWARE_SFU_HEARTBEAT_HEARTBEAT_COUNT;

    table_info = midware_get_table_info(MIDWARE_TABLE_SIP_CFG);
    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &sip_cfg);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_SIP_CFG  \n");
        return ONU_FAIL;
    }
    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_sip_cfg_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_SIP_CFG_T   *midware_sip_cfg;
    VOIP_ST_SIP_PARAMETER_CONFIG voip_sip_cfg;

    midware_sip_cfg = (MIDWARE_TABLE_SIP_CFG_T *)entry;

    rc = VOIP_getSipParamConfig_F(&voip_sip_cfg);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetSipParamConfig_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    midware_sip_cfg->mg_port             =  voip_sip_cfg.mg_port;
    midware_sip_cfg->proxy_ip            =  voip_sip_cfg.server_ip;
    midware_sip_cfg->proxy_port          =  voip_sip_cfg.serv_com_port;
    midware_sip_cfg->backup_proxy_ip     =  voip_sip_cfg.back_server_ip;
    midware_sip_cfg->backup_proxy_port   =  voip_sip_cfg.back_serv_com_port;
    midware_sip_cfg->active_proxy        =  voip_sip_cfg.active_proxy_server;
    midware_sip_cfg->reg_ip              =  voip_sip_cfg.reg_server_ip;
    midware_sip_cfg->reg_port            =  voip_sip_cfg.reg_serv_com_port;
    midware_sip_cfg->backup_reg_ip       =  voip_sip_cfg.back_reg_server_ip;
    midware_sip_cfg->backup_reg_port     =  voip_sip_cfg.back_reg_serv_com_port;
    midware_sip_cfg->outband_ip          =  voip_sip_cfg.outbound_server_ip;
    midware_sip_cfg->outband_port        =  voip_sip_cfg.outbound_serv_com_port;
    midware_sip_cfg->reg_interval        =  voip_sip_cfg.reg_interval;
    midware_sip_cfg->heartbeat_switch    =  voip_sip_cfg.heart_beat_switch;
    midware_sip_cfg->heartbeat_cycle     =  voip_sip_cfg.heart_beat_cycle;
    midware_sip_cfg->heartbeat_count     =  voip_sip_cfg.heart_beat_count;

     return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_sip_cfg_set(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_SIP_CFG_T   *midware_sip_cfg;
    VOIP_ST_SIP_PARAMETER_CONFIG voip_sip_cfg;

    midware_sip_cfg = (MIDWARE_TABLE_SIP_CFG_T *)entry;

    voip_sip_cfg.mg_port                  =  midware_sip_cfg->mg_port;
    voip_sip_cfg.server_ip                =  midware_sip_cfg->proxy_ip;
    voip_sip_cfg.serv_com_port            =  midware_sip_cfg->proxy_port;
    voip_sip_cfg.back_server_ip           =  midware_sip_cfg->backup_proxy_ip;
    voip_sip_cfg.back_serv_com_port       =  midware_sip_cfg->backup_proxy_port;
    voip_sip_cfg.active_proxy_server      =  midware_sip_cfg->active_proxy;
    voip_sip_cfg.reg_server_ip            =  midware_sip_cfg->reg_ip;
    voip_sip_cfg.reg_serv_com_port        =  midware_sip_cfg->reg_port;
    voip_sip_cfg.back_reg_server_ip       =  midware_sip_cfg->backup_reg_ip;
    voip_sip_cfg.back_reg_serv_com_port   =  midware_sip_cfg->backup_reg_port;
    voip_sip_cfg.outbound_server_ip       =  midware_sip_cfg->outband_ip;
    voip_sip_cfg.outbound_serv_com_port   =  midware_sip_cfg->outband_port;
    voip_sip_cfg.reg_interval             =  midware_sip_cfg->reg_interval;
    voip_sip_cfg.heart_beat_switch        =  midware_sip_cfg->heartbeat_switch;
    voip_sip_cfg.heart_beat_cycle         =  midware_sip_cfg->heartbeat_cycle;
    voip_sip_cfg.heart_beat_count         =  midware_sip_cfg->heartbeat_count;

    rc = VOIP_setSipParamConfig_F(&voip_sip_cfg);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_setSipParamConfig_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return MDW_CB_RET_SET_HW_OK;
}


ONU_STATUS midware_table_sip_user_info_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_SIP_USER_T     sip_user_info;
    MIDWARE_TABLE_INFO *table_info;
    UINT32 i;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_SIP_USER_INFO))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&sip_user_info, 0, sizeof(sip_user_info));

    table_info = midware_get_table_info(MIDWARE_TABLE_SIP_USER_INFO);
    for(i = 1; i <= 2; i++)
    {
        sip_user_info.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &sip_user_info);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_SIP_USER_INFO  \n");
            return ONU_FAIL;
        }
    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_sip_user_info_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_SIP_USER_T   *midware_sip_user;
    VOIP_ST_SIP_USER_PARAMETER_CONFIG voip_sip_user;

    midware_sip_user = (MIDWARE_TABLE_SIP_USER_T *)entry;

    rc = VOIP_getSipUserParamConfig_F(midware_sip_user->port_id, &voip_sip_user);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetSipUserParamConfig_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    memcpy(midware_sip_user->user_account, voip_sip_user.sip_port_num, sizeof(midware_sip_user->user_account));
    memcpy(midware_sip_user->user_name, voip_sip_user.user_name, sizeof(midware_sip_user->user_name));
    memcpy(midware_sip_user->user_pwd, voip_sip_user.passwd, sizeof(midware_sip_user->user_pwd));

     return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_sip_user_info_set(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_SIP_USER_T   *midware_sip_user;
    VOIP_ST_SIP_USER_PARAMETER_CONFIG voip_sip_user;

    midware_sip_user = (MIDWARE_TABLE_SIP_USER_T *)entry;

    memcpy(voip_sip_user.sip_port_num, midware_sip_user->user_account, sizeof(voip_sip_user.sip_port_num));
    memcpy(voip_sip_user.user_name, midware_sip_user->user_name, sizeof(voip_sip_user.user_name));
    memcpy(voip_sip_user.passwd, midware_sip_user->user_pwd, sizeof(voip_sip_user.passwd));

    rc = VOIP_setSipUserParamConfig_F(midware_sip_user->port_id, &voip_sip_user);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_SetSipUserParamConfig_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_sip_digitmap_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_DIGITMAP_T     midware_sip_digitmap;
    MIDWARE_TABLE_INFO *table_info;

    //init the default entries

    table_info = midware_get_table_info(MIDWARE_TABLE_SIP_DIGITMAP);
    memset(&midware_sip_digitmap, 0, sizeof(midware_sip_digitmap));

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &midware_sip_digitmap);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_SIP_USER_INFO  \n");
        return ONU_FAIL;
    }

    return ONU_OK;

}


MIDWARE_CALLBACK_RET midware_table_sip_digitmap_set(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_DIGITMAP_T *midware_sip_digitmap;
    VOIP_ST_SIP_DIGITAL_MAP  voip_sip_digitmap;

    midware_sip_digitmap = (MIDWARE_TABLE_DIGITMAP_T *)entry;

    memcpy(voip_sip_digitmap.sip_unit, midware_sip_digitmap->digitmap, sizeof(voip_sip_digitmap.sip_unit));

    rc = VOIP_setSipDigitmap_F(&voip_sip_digitmap);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_SetSipDigitmap_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_fax_cfg_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_FAX_T    fax_cfg;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_FAX_CFG))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&fax_cfg, 0, sizeof(fax_cfg));
    fax_cfg.t38_en = MIDWARE_SFU_DISABLE;
    fax_cfg.fax_ctrl = MIDWARE_SFU_FAX_CTRL_NEG;

    table_info = midware_get_table_info(MIDWARE_TABLE_FAX_CFG);
    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &fax_cfg);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_FAX_CFG  \n");
        return ONU_FAIL;
    }
    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_fax_cfg_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_FAX_T   *midware_fax_cfg;
    VOIP_ST_FAX_MODEM_CONFIG voip_fax_cfg;

    midware_fax_cfg = (MIDWARE_TABLE_FAX_T *)entry;

    rc = VOIP_getFaxModemConfig_F(&voip_fax_cfg);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetFaxModemConfig_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    midware_fax_cfg->t38_en=  voip_fax_cfg.t38_enable;
    midware_fax_cfg->fax_ctrl=  voip_fax_cfg.fax_control;

     return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_fax_cfg_set(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_FAX_T   *midware_fax_cfg;
    VOIP_ST_FAX_MODEM_CONFIG voip_fax_cfg;

    midware_fax_cfg = (MIDWARE_TABLE_FAX_T *)entry;

    voip_fax_cfg.t38_enable = midware_fax_cfg->t38_en ;
    voip_fax_cfg.fax_control =  midware_fax_cfg->fax_ctrl;

    rc = VOIP_setFaxModemConfig_F(&voip_fax_cfg);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_SetFaxModemConfig_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_iad_status_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_IAD_STATUS_T  iad_cfg;
    MIDWARE_TABLE_INFO *table_info;

    //init the default entries
    memset(&iad_cfg, 0, sizeof(iad_cfg));

    table_info = midware_get_table_info(MIDWARE_TABLE_IAD_STATUS);
    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &iad_cfg);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_IAD_STATUS  \n");
        return ONU_FAIL;
    }
    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_iad_status_get(UINT32 bitmap, void *entry)
{
    INT32                        rc;
    MIDWARE_TABLE_IAD_STATUS_T   *midware_iad_cfg;
    VOIP_ST_H248_IAD_OPERATION_STATUS voip_iad_cfg;

    midware_iad_cfg = (MIDWARE_TABLE_IAD_STATUS_T *)entry;

    rc = VOIP_getH248IadOperStatus_F(&voip_iad_cfg);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetH248IadOperStatus_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    midware_iad_cfg->status =  voip_iad_cfg;

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_pots_status_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_POTS_STATUS_T pots_cfg;
    MIDWARE_TABLE_INFO *table_info;
    UINT32 i;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_POTS_STATUS))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&pots_cfg, 0, sizeof(pots_cfg));

    table_info = midware_get_table_info(MIDWARE_TABLE_POTS_STATUS);
    for(i = 1; i <= 2; i++)
    {
        pots_cfg.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &pots_cfg);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_POTS_STATUS  \n");
            return ONU_FAIL;
        }
    }

    return ONU_OK;
}

MIDWARE_CALLBACK_RET midware_table_pots_status_get(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_POTS_STATUS_T   *midware_pots_status;
    VOIP_ST_POTS_STATUS                         voip_pots_status;

    midware_pots_status = (MIDWARE_TABLE_POTS_STATUS_T *)entry;

    rc = VOIP_getPotsStatus_F(midware_pots_status->port_id, &voip_pots_status);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_GetPotsStatus_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    midware_pots_status->port_status =  voip_pots_status.port_status;
    midware_pots_status->service_status =  voip_pots_status.service_status;
    midware_pots_status->codec =  voip_pots_status.codec_mode;

     return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_iad_operation_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_IAD_OPER_T        iad_operation;
    MIDWARE_TABLE_INFO *table_info;

    //init the default entries
    memset(&iad_operation, 0, sizeof(iad_operation));

    table_info = midware_get_table_info(MIDWARE_TABLE_IAD_OPERATION);
    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &iad_operation);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_IAD_OPERATION  \n");
        return ONU_FAIL;
    }
    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_iad_operation_set(UINT32 bitmap, void *entry)
{
    INT32                                                            rc;
    MIDWARE_TABLE_IAD_OPER_T   *midware_iad_operation;
    VOIP_ST_IAD_OPERATION voip_iad_operation;

    midware_iad_operation = (MIDWARE_TABLE_IAD_OPER_T *)entry;

    voip_iad_operation = midware_iad_operation->operation ;

    rc = VOIP_setIadOperation_F(&voip_iad_operation);
    if (OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to VOIP_SetIadOperation_F\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return MDW_CB_RET_SET_HW_OK;
}

#if 0
ONU_STATUS midware_table_alarm_init(void)
{
    ONU_STATUS      rc = ONU_OK;
    MIDWARE_TABLE_ALARM_T alarm;
    MIDWARE_TABLE_INFO *table_info;
    UINT32 i;
    MIDWARE_TABLE_ONU_INFO_T OnuInfo;
    OnuXvrThresholds_S      onuXvrThresholds;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_ALARM))
    {
        //already got entries in the table
        return ONU_OK;
    }

    memset(&OnuInfo,0,sizeof(OnuInfo));
    table_info = midware_get_table_info(MIDWARE_TABLE_ONU_INFO);
    midware_sqlite3_get_select_sql_prepared(table_info,&OnuInfo);

    //init the default entries
    memset(&alarm, 0, sizeof(alarm));

    table_info = midware_get_table_info(MIDWARE_TABLE_ALARM);

    if(is_support_onu_xvr_I2c > 0)
    {
        memset(&onuXvrThresholds,0,sizeof(onuXvrThresholds));

        rc = localI2cApi_getOnuXvrThresholds(&onuXvrThresholds);

        if (true != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to localI2cApi_getOnuXvrThresholds \r\n");
            //return MDW_CB_RET_SET_HW_FAIL;
        }
    }

    for(i = OAM_ALARM_EQUIPMENT; i <= OAM_ALARM_PON_IF_SWITCH; i++)
    {
        alarm.alarm_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &alarm);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_ALARM  \n");
            return ONU_FAIL;
        }
    }

    for(i = OAM_ALARM_PON_RX_POWER_HIGH; i <= OAM_ALARM_PON_TEMP_LOW_WARN; i++)
    {
        alarm.alarm_id = i;

        if(is_support_onu_xvr_I2c > 0)
        {
            switch(alarm.alarm_id)
            {
                case OAM_ALARM_PON_RX_POWER_HIGH:
                    alarm.threshold = onuXvrThresholds.rxReceivedPower_hi_alarm;
                    break;
                case OAM_ALARM_PON_RX_POWER_LOW:
                    alarm.threshold = onuXvrThresholds.rxReceivedPower_lo_alarm;
                    break;
                case OAM_ALARM_PON_TX_POWER_HIGH:
                    alarm.threshold = onuXvrThresholds.txOpticalPower_hi_alarm;
                    break;
                case OAM_ALARM_PON_TX_POWER_LOW:
                    alarm.threshold = onuXvrThresholds.txOpticalPower_lo_alarm;
                    break;
                case OAM_ALARM_PON_TX_BIAS_HIGH:
                    alarm.threshold = onuXvrThresholds.txBiasCurrent_hi_alarm;
                    break;
                case OAM_ALARM_PON_TX_BIAS_LOW:
                    alarm.threshold = onuXvrThresholds.txBiasCurrent_lo_alarm;
                    break;
                case OAM_ALARM_PON_VCC_HIGH:
                    alarm.threshold = onuXvrThresholds.supplyVoltage_hi_alarm;
                    break;
                case OAM_ALARM_PON_VCC_LOW:
                    alarm.threshold = onuXvrThresholds.supplyVoltage_lo_alarm;
                    break;
                case OAM_ALARM_PON_TEMP_HIGH:
                    alarm.threshold = onuXvrThresholds.temperature_hi_alarm;
                    break;
                case OAM_ALARM_PON_TEMP_LOW:
                    alarm.threshold = onuXvrThresholds.temperature_lo_alarm;
                    break;
                case OAM_ALARM_PON_RX_POWER_HIGH_WARN:
                    alarm.threshold = onuXvrThresholds.rxReceivedPower_hi_warning;
                    break;
                case OAM_ALARM_PON_RX_POWER_LOW_WARN:
                    alarm.threshold = onuXvrThresholds.rxReceivedPower_lo_warning;
                    break;
                case OAM_ALARM_PON_TX_POWER_HIGH_WARN:
                    alarm.threshold = onuXvrThresholds.txOpticalPower_hi_warning;
                    break;
                case OAM_ALARM_PON_TX_POWER_LOW_WARN:
                    alarm.threshold = onuXvrThresholds.txOpticalPower_lo_warning;
                    break;
                case OAM_ALARM_PON_TX_BIAS_HIGH_WARN:
                    alarm.threshold = onuXvrThresholds.txBiasCurrent_hi_warning;
                    break;
                case OAM_ALARM_PON_TX_BIAS_LOW_WARN:
                    alarm.threshold = onuXvrThresholds.txBiasCurrent_lo_warning;
                    break;
                case OAM_ALARM_PON_VCC_HIGH_WARN:
                    alarm.threshold = onuXvrThresholds.supplyVoltage_hi_warning;
                    break;
                case OAM_ALARM_PON_VCC_LOW_WARN:
                    alarm.threshold = onuXvrThresholds.supplyVoltage_lo_warning;
                    break;
                case OAM_ALARM_PON_TEMP_HIGH_WARN:
                    alarm.threshold = onuXvrThresholds.temperature_hi_warning;
                    break;
                case OAM_ALARM_PON_TEMP_LOW_WARN:
                    alarm.threshold = onuXvrThresholds.temperature_lo_warning;
                    break;
                default:
                    break;
            }
        }

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &alarm);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_ALARM  \n");
            return ONU_FAIL;
        }
    }

    /*There is no frame failure alarm in CTC, so no initialize it*/
    #if 0
    alarm.alarm_id = OAM_ALARM_FRAME_FAILURE;

    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &alarm);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_ALARM  \n");
        return ONU_FAIL;
    }
    #endif

    alarm.threshold = 0;
    alarm.alarm_id = OAM_ALARM_POTS_PORT_FAILURE;


    for(i=1;i<=OnuInfo.pots_port_num;i++)
    {
        alarm.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &alarm);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_ALARM  \n");
            return ONU_FAIL;
        }
    }

    #if 0
    alarm.alarm_id = OAM_ALARM_ADSL2_PORT_FAILURE;
    for(i=1;i<=OnuInfo.pots_port_num;i++)
    {
        alarm.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &alarm);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_ALARM  \n");
            return ONU_FAIL;
        }
    }
    #endif

    for(i = OAM_ALARM_CARD; i <= OAM_ALARM_CARD_SELF_TEST_FAILURE; i++)
    {
        alarm.alarm_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &alarm);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_ALARM  \n");
            return ONU_FAIL;
        }
    }

    for(alarm.alarm_id = OAM_ALARM_PORT_AUTONEG_FAILURE; alarm.alarm_id <= OAM_ALARM_PORT_CONGESTION; alarm.alarm_id++)
    {
        for(i=TPM_SRC_PORT_UNI_0;i<=TPM_SRC_PORT_UNI_3;i++)
        {
            alarm.port_id = i;

            rc = midware_sqlite3_get_insert_sql_prepared(table_info, &alarm);
            if(ONU_OK != rc)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_ALARM  \n");
                return ONU_FAIL;
            }
        }
    }

    for(alarm.alarm_id = OAM_ALARM_E1_PORT_FAILURE; alarm.alarm_id <= OAM_ALARM_E1_LOS; alarm.alarm_id++)
    {
        for(i=1;i<=OnuInfo.e1_port_num;i++)
        {
            alarm.port_id = i;

            rc = midware_sqlite3_get_insert_sql_prepared(table_info, &alarm);
            if(ONU_OK != rc)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_ALARM  \n");
                return ONU_FAIL;
            }
        }
    }

    return ONU_OK;
}

/*This func is not used yet*/
MIDWARE_CALLBACK_RET midware_table_alarm_get(UINT32 bitmap, void *entry)
{
    bool                   rc;
    MIDWARE_TABLE_ALARM_T   *midware_alarm;
    OnuXvrThresholds_S      onuXvrThresholds;

    midware_alarm = (MIDWARE_TABLE_ALARM_T *)entry;

    if(bitmap & MIDWARE_ENTRY_ALARM_THRESHOLD)// || bitmap & MIDWARE_ENTRY_ALARM_CLEAR_THRESHOLD)
    {
        if(midware_alarm->alarm_id >= OAM_ALARM_PON_RX_POWER_HIGH && midware_alarm->alarm_id <= OAM_ALARM_PON_TEMP_LOW_WARN)
        {
            if(is_support_onu_xvr_I2c > 0)
            {
                memset(&onuXvrThresholds,0,sizeof(onuXvrThresholds));

                rc = localI2cApi_getOnuXvrThresholds(&onuXvrThresholds);

                if (true != rc)
                {
                    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to localI2cApi_getOnuXvrThresholds for alarm_id %d port %d\r\n",midware_alarm->alarm_id,midware_alarm->port_id);
                    //return MDW_CB_RET_SET_HW_FAIL;
                }

                switch(midware_alarm->alarm_id)
                {
                    case OAM_ALARM_PON_RX_POWER_HIGH:
                        midware_alarm->threshold = onuXvrThresholds.rxReceivedPower_hi_alarm;
                        break;
                    case OAM_ALARM_PON_RX_POWER_LOW:
                        midware_alarm->threshold = onuXvrThresholds.rxReceivedPower_lo_alarm;
                        break;
                    case OAM_ALARM_PON_TX_POWER_HIGH:
                        midware_alarm->threshold = onuXvrThresholds.txOpticalPower_hi_alarm;
                        break;
                    case OAM_ALARM_PON_TX_POWER_LOW:
                        midware_alarm->threshold = onuXvrThresholds.txOpticalPower_lo_alarm;
                        break;
                    case OAM_ALARM_PON_TX_BIAS_HIGH:
                        midware_alarm->threshold = onuXvrThresholds.txBiasCurrent_hi_alarm;
                        break;
                    case OAM_ALARM_PON_TX_BIAS_LOW:
                        midware_alarm->threshold = onuXvrThresholds.txBiasCurrent_lo_alarm;
                        break;
                    case OAM_ALARM_PON_VCC_HIGH:
                        midware_alarm->threshold = onuXvrThresholds.supplyVoltage_hi_alarm;
                        break;
                    case OAM_ALARM_PON_VCC_LOW:
                        midware_alarm->threshold = onuXvrThresholds.supplyVoltage_lo_alarm;
                        break;
                    case OAM_ALARM_PON_TEMP_HIGH:
                        midware_alarm->threshold = onuXvrThresholds.temperature_hi_alarm;
                        break;
                    case OAM_ALARM_PON_TEMP_LOW:
                        midware_alarm->threshold = onuXvrThresholds.temperature_lo_alarm;
                        break;
                    case OAM_ALARM_PON_RX_POWER_HIGH_WARN:
                        midware_alarm->threshold = onuXvrThresholds.rxReceivedPower_hi_warning;
                        break;
                    case OAM_ALARM_PON_RX_POWER_LOW_WARN:
                        midware_alarm->threshold = onuXvrThresholds.rxReceivedPower_lo_warning;
                        break;
                    case OAM_ALARM_PON_TX_POWER_HIGH_WARN:
                        midware_alarm->threshold = onuXvrThresholds.txOpticalPower_hi_warning;
                        break;
                    case OAM_ALARM_PON_TX_POWER_LOW_WARN:
                        midware_alarm->threshold = onuXvrThresholds.txOpticalPower_lo_warning;
                        break;
                    case OAM_ALARM_PON_TX_BIAS_HIGH_WARN:
                        midware_alarm->threshold = onuXvrThresholds.txBiasCurrent_hi_warning;
                        break;
                    case OAM_ALARM_PON_TX_BIAS_LOW_WARN:
                        midware_alarm->threshold = onuXvrThresholds.txBiasCurrent_lo_warning;
                        break;
                    case OAM_ALARM_PON_VCC_HIGH_WARN:
                        midware_alarm->threshold = onuXvrThresholds.supplyVoltage_hi_warning;
                        break;
                    case OAM_ALARM_PON_VCC_LOW_WARN:
                        midware_alarm->threshold = onuXvrThresholds.supplyVoltage_lo_warning;
                        break;
                    case OAM_ALARM_PON_TEMP_HIGH_WARN:
                        midware_alarm->threshold = onuXvrThresholds.temperature_hi_warning;
                        break;
                    case OAM_ALARM_PON_TEMP_LOW_WARN:
                        midware_alarm->threshold = onuXvrThresholds.temperature_lo_warning;
                        break;
                    default:
                        break;
                }
            }
        }

    }

    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_alarm_set(UINT32 bitmap, void *entry)
{
    bool                   rc;
    MIDWARE_TABLE_ALARM_T   *midware_alarm;
    OnuXvrThresholds_S      onuXvrThresholds;

    midware_alarm = (MIDWARE_TABLE_ALARM_T *)entry;

    if(bitmap & MIDWARE_ENTRY_ALARM_ENABLE)
    {
        if (!apmSetAlarmAdminState(midware_alarm->alarm_id, midware_alarm->port_id, midware_alarm->enable))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmSetAlarmAdminState for alarm id 0x%x\r\n",midware_alarm->alarm_id);
            return MDW_CB_RET_SET_HW_FAIL;
        }
    }
    if(bitmap & MIDWARE_ENTRY_ALARM_THRESHOLD)
    {
        #if 0
        if(is_support_onu_xvr_I2c)
        {
            memset(&onuXvrThresholds,0,sizeof(onuXvrThresholds));

            rc = localI2cApi_getOnuXvrThresholds(&onuXvrThresholds);

            if (true != rc)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to localI2cApi_getOnuXvrThresholds for alarm_id %d port %d\r\n",midware_alarm->alarm_id,midware_alarm->port_id);
                //return MDW_CB_RET_SET_HW_FAIL;
            }

            switch(midware_alarm->alarm_id)
            {
                case OAM_ALARM_PON_RX_POWER_HIGH:
                    onuXvrThresholds.rxReceivedPower_hi_alarm = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_RX_POWER_LOW:
                    onuXvrThresholds.rxReceivedPower_lo_alarm = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TX_POWER_HIGH:
                    onuXvrThresholds.txOpticalPower_hi_alarm = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TX_POWER_LOW:
                    onuXvrThresholds.txOpticalPower_lo_alarm = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TX_BIAS_HIGH:
                    onuXvrThresholds.txBiasCurrent_hi_alarm = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TX_BIAS_LOW:
                    onuXvrThresholds.txBiasCurrent_lo_alarm = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_VCC_HIGH:
                    onuXvrThresholds.supplyVoltage_hi_alarm = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_VCC_LOW:
                    onuXvrThresholds.supplyVoltage_lo_alarm = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TEMP_HIGH:
                    onuXvrThresholds.temperature_hi_alarm = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TEMP_LOW:
                    onuXvrThresholds.temperature_lo_alarm = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_RX_POWER_HIGH_WARN:
                    onuXvrThresholds.rxReceivedPower_hi_warning = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_RX_POWER_LOW_WARN:
                    onuXvrThresholds.rxReceivedPower_lo_warning = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TX_POWER_HIGH_WARN:
                    onuXvrThresholds.txOpticalPower_hi_warning = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TX_POWER_LOW_WARN:
                    onuXvrThresholds.txOpticalPower_lo_warning = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TX_BIAS_HIGH_WARN:
                    onuXvrThresholds.txBiasCurrent_hi_warning = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TX_BIAS_LOW_WARN:
                    onuXvrThresholds.txBiasCurrent_lo_warning = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_VCC_HIGH_WARN:
                    onuXvrThresholds.supplyVoltage_hi_warning = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_VCC_LOW_WARN:
                    onuXvrThresholds.supplyVoltage_lo_warning = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TEMP_HIGH_WARN:
                    onuXvrThresholds.temperature_hi_warning = midware_alarm->threshold;
                    break;
                case OAM_ALARM_PON_TEMP_LOW_WARN:
                    onuXvrThresholds.temperature_lo_warning = midware_alarm->threshold;
                    break;
                default:
                    break;
            }

            rc = localI2cApi_setOnuXvrThresholds(&onuXvrThresholds);
            if (true != rc)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to localI2cApi_setOnuXvrThresholds for alarm_id %d port %d\r\n",midware_alarm->alarm_id,midware_alarm->port_id);
                //return MDW_CB_RET_SET_HW_FAIL;
            }
        }
        #endif
    }
    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_alarm_insert(UINT32 bitmap, void *entry)
{
     return midware_table_alarm_set(bitmap, entry);
}

MIDWARE_CALLBACK_RET midware_table_alarm_event_insert(UINT32 bitmap, void *entry)
{
    STATUS      eoam_rc;
    MIDWARE_TABLE_ALARM_EVENT_T   *midware_alarm_event;

    midware_alarm_event = (MIDWARE_TABLE_ALARM_EVENT_T *)entry;

    eoam_rc = oam_tl_alarm_generate(midware_alarm_event->alarm_id, 0, midware_alarm_event->port_id, OAM_ALARM_REPORT, midware_alarm_event->value);
    eoam_rc = OAM_EXIT_OK;
    printf("report alarm 0x%x port %d\n",midware_alarm_event->alarm_id,midware_alarm_event->port_id);
    if (OAM_EXIT_OK != eoam_rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to oam_tl_alarm_generate\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_alarm_event_remove(UINT32 bitmap, void *entry)
{
    STATUS      eoam_rc;
    MIDWARE_TABLE_ALARM_EVENT_T   *midware_alarm_event;

    midware_alarm_event = (MIDWARE_TABLE_ALARM_EVENT_T *)entry;

    eoam_rc = oam_tl_alarm_generate(midware_alarm_event->alarm_id, 0, midware_alarm_event->port_id, OAM_ALARM_CLEAR_REPORT, midware_alarm_event->value);
    eoam_rc = OAM_EXIT_OK;
    printf("clear alarm 0x%x port %d\n",midware_alarm_event->alarm_id,midware_alarm_event->port_id);

    if (OAM_EXIT_OK != eoam_rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to oam_tl_alarm_generate\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

     return MDW_CB_RET_SET_HW_OK;
}
#endif

ONU_STATUS midware_table_alarm_init(void)
{
    return ONU_OK;
}

/*This func is not used yet*/
MIDWARE_CALLBACK_RET midware_table_alarm_get(UINT32 bitmap, void *entry)
{
    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_alarm_set(UINT32 bitmap, void *entry)
{
    bool                    rc;
    MIDWARE_TABLE_ALARM_T   *midware_alarm;
    UINT32                  threshold;
    UINT32                  clear_threshold;
    MIDWARE_TABLE_ALARM_T   old_midware_alarm;
    OMCI_REPORT_ALARM_MSG_S omci_alarm_msg;

    midware_get_entry(MIDWARE_TABLE_ALARM, 0xffffffff, &old_midware_alarm, sizeof(old_midware_alarm));

    midware_alarm = (MIDWARE_TABLE_ALARM_T *)entry;

    if(bitmap & MIDWARE_ENTRY_ALARM_ADMIN)
    {
        if (!apm_alarm_set_admin(midware_alarm->type, midware_alarm->param1, midware_alarm->admin))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmSetAlarmAdminState for alarm type 0x%x\r\n",midware_alarm->type);
            return MDW_CB_RET_SET_HW_FAIL;
        }


        if((old_midware_alarm.alarm_class == MIDWARE_TCA) && (old_midware_alarm.admin == MIDWARE_ADMIN_DISABLE)
            && (midware_alarm->admin == MIDWARE_ADMIN_ENABLE) && (old_midware_alarm.state == MIDWARE_STATE_ON))
        {
            old_midware_alarm.state = MIDWARE_STATE_OFF;
            midware_update_entry_without_callback(MIDWARE_TABLE_ALARM, 0xffffffff, &old_midware_alarm);
        }
    }

    if(bitmap & MIDWARE_ENTRY_ALARM_THRESHOLD || bitmap & MIDWARE_ENTRY_ALARM_CLEAR_THRESHOLD)
    {
        if (!apm_alarm_set_threshold(midware_alarm->type, midware_alarm->param1, midware_alarm->threshold, midware_alarm->clear_threshold))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmSetAlarmThreshold for alarm id 0x%x\r\n",midware_alarm->type);
            return MDW_CB_RET_SET_HW_FAIL;
        }
    }

    /*MIDWARE_ENTRY_ALARM_STATE set from apm alarm report*/
    if(bitmap & MIDWARE_ENTRY_ALARM_STATE)
    {
        if(TPM_EPON == wanTech)
        {
            if(midware_alarm->state)
            {
                oam_tl_alarm_generate(midware_alarm->type, midware_alarm->param1, midware_alarm->param2,
                OAM_ALARM_REPORT, midware_alarm->info);
            }
            else
            {
                oam_tl_alarm_generate(midware_alarm->type, midware_alarm->param1, midware_alarm->param2,
                OAM_ALARM_CLEAR_REPORT, midware_alarm->info);
            }

        }
        else if(TPM_GPON == wanTech)
        {
            omci_alarm_msg.alarm_type   = midware_alarm->type;
            omci_alarm_msg.param1       = midware_alarm->param1;
            omci_alarm_msg.param2       = midware_alarm->param2;
            omci_alarm_msg.alarm_class  = midware_alarm->alarm_class;
            omci_alarm_msg.state        = midware_alarm->state;
            omci_report_alarm(&omci_alarm_msg);
        }
    }

    #if 0
    /*MIDWARE_ENTRY_ALARM_STATE set from apm alarm report*/
    if(bitmap & MIDWARE_ENTRY_ALARM_INFO)
    {
        if(TPM_GPON == wanTech)
        {
            omci_report_alarm(midware_alarm->type, midware_alarm->param1, midware_alarm->param2, \
                midware_alarm->state, old_midware_alarm->alarm_class, midware_alarm->info);
        }
    }
    #endif

    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_alarm_insert(UINT32 bitmap, void *entry)
{
    bool                    callback_ret;
    MIDWARE_TABLE_ALARM_T   *midware_alarm = (MIDWARE_TABLE_ALARM_T *)entry;

    callback_ret = apm_alarm_create_entity(midware_alarm->type, midware_alarm->param1, midware_alarm->param2,
        midware_alarm->alarm_class, midware_alarm->admin, midware_alarm->threshold, midware_alarm->clear_threshold);
    if(!callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apm_alarm_create_entity\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_alarm_remove(UINT32 bitmap, void *entry)
{
    bool                    callback_ret;
    MIDWARE_TABLE_ALARM_T   *midware_alarm = (MIDWARE_TABLE_ALARM_T *)entry;

    callback_ret = apm_alarm_delete_entity(midware_alarm->type, midware_alarm->param1);
    if (!callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmDeleteAlarmEntity\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }
     return MDW_CB_RET_SET_HW_OK;
}

static void  midware_clean_pm_data()
{
    MIDWARE_TABLE_INFO      *table_info;
    ONU_STATUS              rc;
    MIDWARE_TABLE_ALARM_T   midware_alarm;
    MIDWARE_TABLE_PM_T      midware_pm;
    UINT32                  bitmap;

    /*clean PM data*/
    table_info = midware_get_table_info(MIDWARE_TABLE_PM);
    bitmap = MIDWARE_ENTRY_PM_COUNTERS;
    rc = midware_sqlite3_get_first_sql_prepared(table_info, (void *)(&midware_pm));
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_DEBUG,"Can't get first entry for pm: %d\n", rc);
        return;
    }

    do
    {
        if(midware_pm.accumulation_mode == MIDWARE_ACCUMULATION_15_MIN)
        {
            memset(&midware_pm.counter0, 0, PM_COUNTERS_MAX_NUM*sizeof(UINT32));
            midware_sqlite3_get_update_sql_prepared(table_info, bitmap, &midware_pm);
        }
        rc = midware_sqlite3_get_next_sql_prepared(table_info, (void *)(&midware_pm));

    }while( ONU_FAIL != rc );

    /*clean PM history data*/
    table_info = midware_get_table_info(MIDWARE_TABLE_PM_HISTORY);
    bitmap = MIDWARE_ENTRY_PM_COUNTERS;
    rc = midware_sqlite3_get_first_sql_prepared(table_info, (void *)(&midware_pm));
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_DEBUG,"Can't get first entry for pm history: %d\n", rc);
        return;
    }

    do
    {
        if(midware_pm.accumulation_mode == MIDWARE_ACCUMULATION_15_MIN)
        {
            memset(&midware_pm.counter0, 0, PM_COUNTERS_MAX_NUM*sizeof(UINT32));
            midware_sqlite3_get_update_sql_prepared(table_info, bitmap, &midware_pm);
        }
        rc = midware_sqlite3_get_next_sql_prepared(table_info, (void *)(&midware_pm));

    }while( ONU_FAIL != rc );

    return;

}

static void  midware_Synchronize_TCA()
{
    MIDWARE_TABLE_INFO      *table_info;
    ONU_STATUS              rc;
    MIDWARE_TABLE_ALARM_T   midware_alarm;
    MIDWARE_TABLE_PM_T      midware_pm;
    UINT32                  bitmap;  

    /*clean PM data*/
    table_info = midware_get_table_info(MIDWARE_TABLE_PM);
    bitmap = MIDWARE_ENTRY_PM_COUNTERS;
    rc = midware_sqlite3_get_first_sql_prepared(table_info, (void *)(&midware_pm));
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_DEBUG,"Can't get first entry for pm: %d\n", rc);
        return;
    }   
    
    do    
    {
        if(midware_pm.accumulation_mode == MIDWARE_ACCUMULATION_15_MIN)
        {
            memset(&midware_pm.counter0, 0, PM_COUNTERS_MAX_NUM*sizeof(UINT32));
            midware_sqlite3_get_update_sql_prepared(table_info, bitmap, &midware_pm);
        }
        rc = midware_sqlite3_get_next_sql_prepared(table_info, (void *)(&midware_pm));
            
    }while( ONU_FAIL != rc );

    /*clean TCA*/
    table_info = midware_get_table_info(MIDWARE_TABLE_ALARM);
    bitmap = MIDWARE_ENTRY_ALARM_STATE;
    rc = midware_sqlite3_get_first_sql_prepared(table_info, (void *)(&midware_alarm));
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_DEBUG,"Can't get first entry for alarm: %d\n", rc);
        return;
    }

    do
    {
        if( midware_alarm.alarm_class == MIDWARE_TCA && midware_alarm.admin == MIDWARE_ADMIN_ENABLE
            && midware_alarm.state == MIDWARE_STATE_ON)
        {
            midware_alarm.state = MIDWARE_STATE_OFF;
            midware_sqlite3_get_update_sql_prepared(table_info, bitmap, &midware_alarm);
        }
        rc = midware_sqlite3_get_next_sql_prepared(table_info, (void *)(&midware_alarm));
             
    }while( ONU_FAIL != rc );

    /*clean PM history data*/
    table_info = midware_get_table_info(MIDWARE_TABLE_PM_HISTORY);
    bitmap = MIDWARE_ENTRY_PM_COUNTERS;
    rc = midware_sqlite3_get_first_sql_prepared(table_info, (void *)(&midware_pm));
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_DEBUG,"Can't get first entry for pm history: %d\n", rc);
        return;
    }

    do
    {
        if(midware_pm.accumulation_mode == MIDWARE_ACCUMULATION_15_MIN)
        {
            memset(&midware_pm.counter0, 0, PM_COUNTERS_MAX_NUM*sizeof(UINT32));
            midware_sqlite3_get_update_sql_prepared(table_info, bitmap, &midware_pm);
        }
        rc = midware_sqlite3_get_next_sql_prepared(table_info, (void *)(&midware_pm));

    }while( ONU_FAIL != rc );

    return;

}

void  midware_clear_Pm_Related_TCA(MIDWARE_TABLE_PM_T   *midware_pm)
{
    MIDWARE_TABLE_INFO              *table_info;
    ONU_STATUS                      rc;
    MIDWARE_TABLE_ALARM_T           midware_alarm;
    APM_PM_RELATED_ALARM_TYPES_T    pm_related_alarm_types;
    UINT32                          bitmap  = MIDWARE_ENTRY_ALARM_STATE;
    int                             i       = 0;


    if(!apm_pm_get_related_alarm_types(midware_pm->type, &pm_related_alarm_types))
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"Can't get related alarm type for pm type %d\n", midware_pm->type);
        return;
    }

    table_info = midware_get_table_info(MIDWARE_TABLE_ALARM);

    for(i=0; i<APM_EXT_PM_BLOCK_SIZE; i++)
    {
        if(pm_related_alarm_types.pm_related_alarm_types[i]!=0)
        {
            midware_alarm.type      = pm_related_alarm_types.pm_related_alarm_types[i];
            midware_alarm.param1    = midware_pm->param1;
            rc                      = midware_sqlite3_get_select_sql_prepared(table_info, (void *)(&midware_alarm));
            if( ONU_FAIL == rc )
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_DEBUG,"Can't get alarm entry type %d param1 %d\n", midware_alarm.type, midware_alarm.param1);
                return;
            }
            midware_alarm.state = MIDWARE_STATE_OFF;
            midware_sqlite3_get_update_sql_prepared(table_info, bitmap, &midware_alarm);
        }
    }

    return;
}


ONU_STATUS midware_table_pm_init(void)
{
    return ONU_OK;
}

/*This func is not used yet*/
MIDWARE_CALLBACK_RET midware_table_pm_get(UINT32 bitmap, void *entry)
{
    bool                    rc;
    MIDWARE_TABLE_PM_T      *midware_pm;


    midware_pm = (MIDWARE_TABLE_PM_T *)entry;

    if(bitmap & MIDWARE_ENTRY_PM_COUNTERS)
    {
        if (!apm_pm_get_data(midware_pm->type, midware_pm->param1, (APM_PM_DATA_T *)&midware_pm->counter0))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmGetPmData for pm type 0x%x param1 %d\r\n",
                midware_pm->type, midware_pm->param1);
            return MDW_CB_RET_SET_HW_FAIL;
        }
    }

    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_pm_set(UINT32 bitmap, void *entry)
{
    bool                    rc;
    MIDWARE_TABLE_PM_T      *midware_pm;

    midware_pm = (MIDWARE_TABLE_PM_T *)entry;
    OMCI_REPORT_PM_MSG_S pmMsg;

    if(bitmap & MIDWARE_ENTRY_PM_ADMIN_BITS)
    {
        if (!apm_pm_set_admin(midware_pm->type, midware_pm->param1, midware_pm->admin_bits))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmSetPmAdmin for pm type 0x%x\r\n",midware_pm->type);
            return MDW_CB_RET_SET_HW_FAIL;
        }
    }

    if(bitmap & MIDWARE_ENTRY_PM_COUNTERS)
    {
        if(TPM_GPON == wanTech)
        {
            midware_clear_Pm_Related_TCA(midware_pm);

            pmMsg.pm_type   = midware_pm->type;
            pmMsg.param1    = midware_pm->param1;
            pmMsg.param2    = midware_pm->param2;
            memmove(pmMsg.pmData, &midware_pm->counter0, sizeof(UINT32)*PM_COUNTERS_MAX_NUM);

            omci_report_pm(&pmMsg);
        }
    }

    if(bitmap & MIDWARE_ENTRY_PM_ACCUMULATION_MODE)
    {
        if (!apm_pm_set_accumulation_mode(midware_pm->type, midware_pm->param1, midware_pm->accumulation_mode))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmSetPmAccumulationMode for type id 0x%x\r\n",midware_pm->type);
            return MDW_CB_RET_SET_HW_FAIL;
        }
        midware_clear_Pm_Related_TCA(midware_pm);
    }

    if(bitmap & MIDWARE_ENTRY_PM_GLOBAL_CLEAR)
    {
        if(PM_GLOBAL_CLEAR == midware_pm->global_clear)
        {
            if (!apm_pm_set_global_clear(midware_pm->type, midware_pm->param1))
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmSetPmGlobalClear for pm type 0x%x\r\n",midware_pm->type);
                return MDW_CB_RET_SET_HW_FAIL;
            }
            midware_clear_Pm_Related_TCA(midware_pm);
        }

        /*clean current PM and HISTORY PM midware data */
        midware_clean_pm_data();

    }

    if(bitmap & MIDWARE_ENTRY_PM_INTERVAL_PERIOD)
    {
		if(!apm_pm_set_interval(midware_pm->type, midware_pm->param1, midware_pm->interval))
	    {
	        return MDW_CB_RET_SET_HW_FAIL;
	    }

    }
    
    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_pm_insert(UINT32 bitmap, void *entry)
{
    bool                    callback_ret;
    MIDWARE_TABLE_PM_T      *midware_pm = (MIDWARE_TABLE_PM_T *)entry;


    callback_ret = apm_pm_create_entity(midware_pm->type, midware_pm->param1, midware_pm->param2, midware_pm->admin_bits, midware_pm->accumulation_mode, midware_pm->interval);
    if(!callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apm_pm_create_entity\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_pm_remove(UINT32 bitmap, void *entry)
{
    bool                    callback_ret;
    MIDWARE_TABLE_PM_T      *midware_pm = (MIDWARE_TABLE_PM_T *)entry;

    callback_ret = apm_pm_delete_entity(midware_pm->type, midware_pm->param1);
    if (!callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmDeletePmEntity\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }
    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_pm_history_set(UINT32 bitmap, void *entry)
{
    bool                    rc;
    MIDWARE_TABLE_PM_T      *midware_pm;
    
    midware_pm = (MIDWARE_TABLE_PM_T *)entry;
    OMCI_REPORT_PM_MSG_S pmMsg;

    if(bitmap & MIDWARE_ENTRY_PM_COUNTERS)
    {
        if(TPM_GPON == wanTech)
        {
            midware_clear_Pm_Related_TCA(midware_pm);

            pmMsg.pm_type   = midware_pm->type;
            pmMsg.param1    = midware_pm->param1;
            pmMsg.param2    = midware_pm->param2;
            memmove(pmMsg.pmData, &midware_pm->counter0, sizeof(UINT32)*PM_COUNTERS_MAX_NUM);
            
            omci_report_pm(&pmMsg);
        }
    }
    
    return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_avc_init(void)
{
    return ONU_OK;
}

/*This func is not used yet*/
MIDWARE_CALLBACK_RET midware_table_avc_get(UINT32 bitmap, void *entry)
{
    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_avc_set(UINT32 bitmap, void *entry)
{
    bool                   rc;
    MIDWARE_TABLE_AVC_T   *midware_avc;
    OMCI_REPORT_AVC_MSG_S omci_avc_msg;

    midware_avc = (MIDWARE_TABLE_AVC_T *)entry;

    if(bitmap & MIDWARE_ENTRY_AVC_ADMIN)
    {
        if (!apm_avc_set_admin(midware_avc->type, midware_avc->param1, midware_avc->admin))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmSetAVCAdminState for avc type 0x%x\r\n",midware_avc->type);
            return MDW_CB_RET_SET_HW_FAIL;
        }
    }

    if(bitmap & MIDWARE_ENTRY_AVC_PARAM2)
    {
        if (!apm_avc_set_param2(midware_avc->type, midware_avc->param1, midware_avc->param2))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmSetAVCAdminState for avc type 0x%x\r\n",midware_avc->type);
            return MDW_CB_RET_SET_HW_FAIL;
        }
    }

    /*MIDWARE_ENTRY_AVC_STATE set from apm avc report*/
    if(bitmap & MIDWARE_ENTRY_AVC_VALUE)
    {
        if(TPM_EPON == wanTech)
        {
            oam_tl_avc_generate(midware_avc->type, midware_avc->param1, midware_avc->param2, *(UINT32 *)midware_avc->value);
        }
        else if(TPM_GPON == wanTech)
        {
            omci_avc_msg.avc_type   = midware_avc->type;
            omci_avc_msg.param1     = midware_avc->param1;
            omci_avc_msg.param2     = midware_avc->param2;
            memmove(omci_avc_msg.value, midware_avc->value, MIDWARE_AVC_VALUE_LENGTH*sizeof(UINT8));
            omci_report_avc(&omci_avc_msg);
        }
    }

    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_avc_insert(UINT32 bitmap, void *entry)
{
    bool                    callback_ret;
    MIDWARE_TABLE_AVC_T     *midware_avc = (MIDWARE_TABLE_AVC_T *)entry;

    callback_ret = apm_avc_create_entity(midware_avc->type, midware_avc->param1, midware_avc->param2, midware_avc->admin);
    if (!callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apm_avc_create_entity\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }

    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_avc_remove(UINT32 bitmap, void *entry)
{
    bool                    callback_ret;
    MIDWARE_TABLE_AVC_T     *midware_avc = (MIDWARE_TABLE_AVC_T *)entry;

    callback_ret = apm_avc_delete_entity(midware_avc->type, midware_avc->param1);
    if (!callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to apmDeleteAVCEntity\r\n");
        return MDW_CB_RET_SET_HW_FAIL;
    }
    return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_vlan_port_init(void)
{
    ONU_STATUS rc = ONU_OK;
    MIDWARE_TABLE_VLAN_PORT_T vlan_cfg;
    MIDWARE_TABLE_INFO *table_info;
    UINT32 i;

#if 1 /* Need not to restore*/
    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_VLAN_PORT))
    {
        //already got entries in the table, restore
        /*
        rc = midware_table_restore(MIDWARE_TABLE_VLAN_PORT);
        if (ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to restore MIDWARE_TABLE_VLAN_PORT\r\n");
            return ONU_FAIL;
        }
        */
        return ONU_OK;
    }
#endif

    //init the default entries
    memset(&vlan_cfg, 0, sizeof(vlan_cfg));
    vlan_cfg.vlan_mode = MIDWARE_VLAN_MODE_TRANSPARENT; /*VLAN transparent mode*/

    table_info = midware_get_table_info(MIDWARE_TABLE_VLAN_PORT);
    for(i = TPM_SRC_PORT_UNI_0; i <= TPM_SRC_PORT_UNI_3; i++)
    {
        vlan_cfg.port_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &vlan_cfg);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for port: %ld\n", i);
            return ONU_FAIL;
        }
    }

    return ONU_OK;
}


MIDWARE_CALLBACK_RET midware_table_vlan_port_set(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_VLAN_PORT_T *vlan_cfg;
    tpm_error_code_t          rc = ERR_GENERAL;
    MIDWARE_CALLBACK_RET      callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    tpm_src_port_type_t       port_id;
    UINT32                    default_vid;

    vlan_cfg = (MIDWARE_TABLE_VLAN_PORT_T *)entry;

    //here maybe we need to adapt the midware portid to tpm portid
    port_id = vlan_cfg->port_id;

    if(bitmap & MIDWARE_ENTRY_DEFAULT_VLAN)
    {
      default_vid = vlan_cfg->default_vlan;
    }
    else
    {
      default_vid = 0;
    }
    return callback_ret;
}

ONU_STATUS midware_table_ethernet_uni_pm_init(void)
{
    ONU_STATUS      rc = ONU_OK;
    MIDWARE_TABLE_STATISTICS_T     ethernet_uni_pm;
    UINT32             i;
    MIDWARE_TABLE_INFO *table_info;



    //init the default entries
    memset(&ethernet_uni_pm, 0, sizeof(ethernet_uni_pm));
    ethernet_uni_pm.enable = 1;

    table_info = midware_get_table_info(MIDWARE_TABLE_ETH_UNI_PM);
    for(i = TPM_SRC_PORT_UNI_0; i <= TPM_SRC_PORT_UNI_3; i++)
    {
        ethernet_uni_pm.port_id = i;
        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &ethernet_uni_pm);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for port: %ld\n", i);
            return ONU_FAIL;
        }

    }

    return ONU_OK;

}


MIDWARE_CALLBACK_RET midware_table_ethernet_uni_pm_get(UINT32 bitmap, void *entry)
{
    INT32                           rc;
    MIDWARE_TABLE_STATISTICS_T      *midware_ethernet_uni_pm;
    tpm_swport_pm_3_all_t           tpm_swport_pm_3;

    midware_ethernet_uni_pm = (MIDWARE_TABLE_STATISTICS_T *)entry;

    if(0 == midware_ethernet_uni_pm->enable)
    {
        return MDW_CB_RET_SET_HW_FAIL;
    }

    rc = tpm_sw_pm_3_read(0, midware_ethernet_uni_pm->port_id, &tpm_swport_pm_3);
    if (TPM_RC_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Failed to tpm_sw_pm_3_read for port_id %d \r\n",midware_ethernet_uni_pm->port_id);
        return MDW_CB_RET_SET_HW_FAIL;
    }

    midware_ethernet_uni_pm->packet_rx              = tpm_swport_pm_3.InUnicasts+tpm_swport_pm_3.InMulticasts+tpm_swport_pm_3.InBroadcasts;
    midware_ethernet_uni_pm->unicast_packet_rx      = tpm_swport_pm_3.InUnicasts;
    midware_ethernet_uni_pm->multicast_packet_rx    = tpm_swport_pm_3.InMulticasts;
    midware_ethernet_uni_pm->broadcast_packet_rx    = tpm_swport_pm_3.InBroadcasts;

    midware_ethernet_uni_pm->packet_tx              = 0;//tpm_swport_pm_1.OutUnicasts+tpm_swport_pm_1.OutMulticasts+tpm_swport_pm_1.OutBroadcasts;
    midware_ethernet_uni_pm->unicast_packet_tx      = 0;//tpm_swport_pm_1.OutUnicasts;
    midware_ethernet_uni_pm->multicast_packet_tx    = 0;//tpm_swport_pm_1.OutMulticasts;
    midware_ethernet_uni_pm->broadcast_packet_tx    = 0;//tpm_swport_pm_1.OutBroadcasts;
    /* Whether the following items are tx or rx depends Histogram Counters Mode.
    The Histogram mode bits control how the
    Histogram counters work as follows:
    00 = Reserved
    01 = Count received frames only
    10 = Count transmitted frames only
    11 = Count receive and transmitted frames
    */
    #if 0
    midware_ethernet_uni_pm->packet_64B_rx          = tpm_swport_pm_1.packets_64Octets;
    midware_ethernet_uni_pm->packet_65_127B_rx      = tpm_swport_pm_1.packets_65_127Octets;
    midware_ethernet_uni_pm->packet_128_255B_rx     = tpm_swport_pm_1.packets_128_255Octets;
    midware_ethernet_uni_pm->packet_256_511B_rx     = tpm_swport_pm_1.packets_256_511Octets;
    midware_ethernet_uni_pm->packet_512_1023B_rx    = tpm_swport_pm_1.packets_512_1023Octets;
    midware_ethernet_uni_pm->packet_1024_1518B_rx   = tpm_swport_pm_1.packets_1024_1518Octets;
    midware_ethernet_uni_pm->packet_1519B_rx        = tpm_swport_pm_1.jabbers;
    #endif

    return MDW_CB_RET_SET_HW_OK;
}

ONU_STATUS midware_table_epon_init(void)
{
    ONU_STATUS      rc;
    MIDWARE_TABLE_EPON_T    epon_cfg;
    MIDWARE_TABLE_INFO *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_EPON))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&epon_cfg, 0, sizeof(epon_cfg));
    epon_cfg.fec= MIDWARE_PON_FEC_MODE_DISABLED;
    epon_cfg.multi_llid = 1;
    epon_cfg.holdover_cfg = MIDWARE_HOLDOVER_STATE_DEACTIVE;
    epon_cfg.holdover_time = 200;
    epon_cfg.register_status = MIDWARE_SFU_OAM_STACK_DEREG;

    MvExtOamGetMac(epon_cfg.base_pon_mac);

    table_info = midware_get_table_info(MIDWARE_TABLE_EPON);
    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &epon_cfg);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_EPON  \n");
        return ONU_FAIL;
    }
    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_epon_get(UINT32 bitmap, void *entry)
{
    INT32    rc;

    // todo

    return MDW_CB_RET_SET_HW_OK;
}

MIDWARE_CALLBACK_RET midware_table_epon_set(UINT32 bitmap, void *entry)
{

    MIDWARE_TABLE_EPON_T              *EponCfg;
    MIDWARE_CALLBACK_RET      callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    //to do

    EponCfg = (MIDWARE_TABLE_EPON_T *)entry;

    if(bitmap & MIDWARE_ENTRY_EPON_MAC)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_EPON_MULTI_LLID)
    {

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_EPON_FEC)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }


    if(bitmap & MIDWARE_ENTRY_HOLDOVER_CFG)
    {

    }

    if(bitmap & MIDWARE_ENTRY_HOLDOVER_TIME)
    {

    }
#if 0
    if(bitmap & MIDWARE_ENTRY_END_EPON)
    {

    }
#endif
    if(bitmap & MIDWARE_ENTRY_ONU_REGISTER_STATUS)
    {

    }

    if(bitmap & MIDWARE_ENTRY_ONU_LLID_NUM)
    {

    }

     return callback_ret;
}

ONU_STATUS midware_table_v_cfg_init(void)
{
    ONU_STATUS              rc;
    MIDWARE_TABLE_V_CFG_T   v_cfg;
    MIDWARE_TABLE_INFO     *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_V_CFG))
    {
        //already got entries in the table
        return ONU_OK;
    }
    UINT8      available_proto;

    //init the default entries
    memset(&v_cfg, 0, sizeof(v_cfg));
    v_cfg.index            = 0;
    v_cfg.available_proto  = MIDWARE_V_PROTO_BM_SIP|MIDWARE_V_PROTO_BM_H248;
    v_cfg.used_proto       = MIDWARE_V_PROTO_TYPE_SIP;
    v_cfg.available_method = MIDWARE_V_METHOD_BM_OMCI|MIDWARE_V_METHOD_BM_TR69;
    v_cfg.used_method      = MIDWARE_V_METHOD_TYPE_OMCI;
    v_cfg.config_state     = MIDWARE_V_CONFGI_STATE_INITIAL;
    v_cfg.retrieve_profile = MIDWARE_DEFAULT_ZERO_VAL;
#if 0
    v_cfg.profile_version;
    v_cfg.mac_addr;
    v_cfg.sw_ver;
    v_cfg.sw_time;
#endif
    v_cfg.pots_num         = MIDWARE_SFU_POTS_PORT_NUM;
    v_cfg.ip_mode          = MIDWARE_SFU_VOICE_IP_MODE_STATIC;
    v_cfg.ip_addr          = MIDWARE_DEFAULT_ZERO_VAL;
    v_cfg.net_mask         = MIDWARE_DEFAULT_ZERO_VAL;
    v_cfg.def_gw           = MIDWARE_DEFAULT_ZERO_VAL;
    v_cfg.pppoe_mode       = MIDWARE_SFU_VOICE_IP_MODE_STATIC;
#if 0
    v_cfg.pppoe_user;
    v_cfg.pppoe_pwd;
#endif
    v_cfg.tag_flag         = MIDWARE_DEFAULT_ZERO_VAL;
    v_cfg.cvlan            = MIDWARE_DEFAULT_ZERO_VAL;
    v_cfg.svlan            = MIDWARE_DEFAULT_ZERO_VAL;
    v_cfg.pbits            = MIDWARE_DEFAULT_ZERO_VAL;
    v_cfg.iad_operate      = MIDWARE_DEFAULT_ZERO_VAL;

    table_info = midware_get_table_info(MIDWARE_TABLE_V_CFG);
    rc = midware_sqlite3_get_insert_sql_prepared(table_info, &v_cfg);
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for MIDWARE_TABLE_V_CFG  \n");
        return ONU_FAIL;
    }
    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_v_cfg_get(UINT32 bitmap, void *entry)
{

    MIDWARE_TABLE_V_CFG_T   *v_cfg;
    MIDWARE_CALLBACK_RET     callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    //to do

    v_cfg = (MIDWARE_TABLE_V_CFG_T *)entry;

    if(bitmap & MIDWARE_ENTRY_V_SUPPORT_PROTO)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_USED_PROTO)
    {

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_SUPPORT_METHOD)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_USED_METHOD)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_CONFIG_STATE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_RTRL_PROFILE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_PROFILE_VER)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_MAC_ADDR)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_SW_VER)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_SW_TIME)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_POTS_NUM)
    {

      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_IP_MODE)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_IP_ADDR)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_NET_MASK)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_DEFAULT_GW)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_PPPOE_MODE)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_PPPOE_USER)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_PPPOE_USER)
    {
    callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_PPPOE_PWD)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_TAG_FLAG)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_VOICE_CVID)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_VOICE_SVID)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_VOICE_PBIT)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_IAD_OPER)
    {
      callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_v_cfg_set(UINT32 bitmap, void *entry)
{

    MIDWARE_TABLE_V_CFG_T   *v_cfg;
    MIDWARE_CALLBACK_RET     callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    INT32                    rc;

    v_cfg = (MIDWARE_TABLE_V_CFG_T *)entry;

    if(bitmap & MIDWARE_ENTRY_V_USED_PROTO)
    {

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_USED_METHOD)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_CONFIG_STATE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_RTRL_PROFILE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_IP_MODE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if((bitmap & MIDWARE_ENTRY_V_IP_ADDR)  ||
       (bitmap & MIDWARE_ENTRY_V_NET_MASK) ||
       (bitmap & MIDWARE_ENTRY_V_DEFAULT_GW))
    {
        rc = VOIP_updateHostIpAddr_F(v_cfg->ip_addr, v_cfg->net_mask, v_cfg->def_gw);

        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR, "Failed to call VOIP_updateHostIpAddr_F\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_PPPOE_MODE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_PPPOE_USER)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_PPPOE_USER)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_PPPOE_PWD)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if((bitmap & MIDWARE_ENTRY_V_TAG_FLAG)   ||
       (bitmap & MIDWARE_ENTRY_V_VOICE_CVID) ||
       (bitmap & MIDWARE_ENTRY_V_VOICE_SVID) ||
       (bitmap & MIDWARE_ENTRY_V_VOICE_PBIT))
    {
        rc = VOIP_updateHostVidPbits_F(v_cfg->cvlan, v_cfg->pbits);

        if (OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR, "Failed to call VOIP_updateHostVidPbits_F\r\n");
            return MDW_CB_RET_SET_HW_FAIL;
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_IAD_OPER)
    {
        if (MIDWARE_V_IAD_OPERATION_RESET == v_cfg->iad_operate)
        {
            midware_restart_mmp_app();
        }
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    return callback_ret;
}

ONU_STATUS midware_table_v_port_cfg_init(void)
{
    ONU_STATUS                  rc=ONU_OK;
    MIDWARE_TABLE_V_PORT_CFG_T  v_port_cfg;
    UINT32                      i;
    MIDWARE_TABLE_INFO         *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_V_PORT_CFG))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&v_port_cfg, 0, sizeof(v_port_cfg));

    v_port_cfg.admin_state     = MIDWARE_OMCI_UNLOCK_STATE;
    v_port_cfg.impedence       = MIDWARE_DEFAULT_ZERO_VAL;
    v_port_cfg.tx_path         = MIDWARE_DEFAULT_ZERO_VAL;
    v_port_cfg.rx_gain         = MIDWARE_DEFAULT_ZERO_VAL;
    v_port_cfg.tx_gain         = MIDWARE_DEFAULT_ZERO_VAL;
    v_port_cfg.operation_state = MIDWARE_OMCI_UNLOCK_STATE;
    v_port_cfg.hook_state      = MIDWARE_V_OFF_HOOK_STATE;
    v_port_cfg.holdover_time   = MIDWARE_DEFAULT_ZERO_VAL;
    v_port_cfg.signal_code     = MIDWARE_DEFAULT_ZERO_VAL;

    table_info = midware_get_table_info(MIDWARE_TABLE_V_PORT_CFG);
    for(i = 0; i < MIDWARE_SFU_POTS_PORT_NUM; i++)
    {
        v_port_cfg.line_id = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &v_port_cfg);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for port: %ld\n", i);
            return ONU_FAIL;
        }

    }

    return ONU_OK;

}

MIDWARE_CALLBACK_RET midware_table_v_port_cfg_get(UINT32 bitmap, void *entry)
{

    MIDWARE_TABLE_V_PORT_CFG_T  *v_port_cfg;
    MIDWARE_CALLBACK_RET         callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    //to do

    v_port_cfg = (MIDWARE_TABLE_V_PORT_CFG_T *)entry;

    if(bitmap & MIDWARE_ENTRY_V_ADMIN_STATE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_IMPEDANCE)
    {

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_TX_PATH)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_RX_GAIN)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_TX_GAIN)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_OPER_STATE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_HOOK_STATE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_HOLDOVER_TIME)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_SIGNAL_CODE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_v_port_cfg_set(UINT32 bitmap, void *entry)
{

    MIDWARE_TABLE_V_PORT_CFG_T  *v_port_cfg;
    MIDWARE_CALLBACK_RET         callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    //to do

    v_port_cfg = (MIDWARE_TABLE_V_PORT_CFG_T *)entry;

    if(bitmap & MIDWARE_ENTRY_V_ADMIN_STATE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_IMPEDANCE)
    {

        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_TX_PATH)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_RX_GAIN)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_TX_GAIN)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_HOLDOVER_TIME)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    if(bitmap & MIDWARE_ENTRY_V_SIGNAL_CODE)
    {
        callback_ret = MDW_CB_RET_SET_HW_OK;
    }

    return callback_ret;
}

ONU_STATUS midware_table_v_sip_agent_init(void)
{
    ONU_STATUS                   rc = ONU_OK;
    MIDWARE_TABLE_V_SIP_AGENT_T  v_sip_agent;
    UINT32                       i;
    UINT32                       j;
    MIDWARE_TABLE_INFO          *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_V_SIP_AGENT))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&v_sip_agent, 0, sizeof(v_sip_agent));

#if 0
    v_sip_agent.proxy_serv_addr
    v_sip_agent.backup_proxy_serv_addr
    v_sip_agent.outbound_proxy_addr
    v_sip_agent.host_part_url
    v_sip_agent.reg_user
    v_sip_agent.reg_pwd
    v_sip_agent.reg_realm
    v_sip_agent.reg_addr
    v_sip_agent.backup_reg_addr
    v_sip_agent.response_code
    v_sip_agent.response_tone
    v_sip_agent.response_text
#endif

    v_sip_agent.reg_scheme          = MIDWARE_V_VALIDATE_SCHEME_NONE;
    v_sip_agent.primary_dns         = MIDWARE_DEFAULT_ZERO_VAL;
    v_sip_agent.second_dns          = MIDWARE_DEFAULT_ZERO_VAL;
    v_sip_agent.l4_type             = MIDWARE_DEFAULT_ZERO_VAL;
    v_sip_agent.l4_port             = MIDWARE_DEFAULT_ZERO_VAL;
    v_sip_agent.dscp_val            = MIDWARE_DEFAULT_ZERO_VAL;
    v_sip_agent.reg_expire_time     = MIDWARE_V_DEFT_REG_EXP_TIME;
    v_sip_agent.reg_head_start_time = MIDWARE_V_REREG_HEAD_START_TIME;
    v_sip_agent.soft_switch         = MIDWARE_DEFAULT_ZERO_VAL;
    v_sip_agent.optical_tx_ctrl     = MIDWARE_DEFAULT_ZERO_VAL;
    v_sip_agent.url_format          = MIDWARE_V_URL_FORMAT_SIP;
    v_sip_agent.status              = MIDWARE_V_SIP_STATUS_OK_INIT;
    v_sip_agent.heartbeat_switch    = MIDWARE_SFU_SIP_HEARTBEAT_SWITCH_OPEN;
    v_sip_agent.heartbeat_cycle     = MIDWARE_SFU_HEARTBEAT_CYCLE;
    v_sip_agent.heartbeat_count     = MIDWARE_SFU_HEARTBEAT_HEARTBEAT_COUNT;

    table_info = midware_get_table_info(MIDWARE_TABLE_V_SIP_AGENT);
    for(i = 0; i < MIDWARE_SFU_POTS_PORT_NUM; i++)
    {
        for(j = 0; j < MIDWARE_V_SIP_AGENT_NUM; j++)
        {
            v_sip_agent.line_id  = i;
            v_sip_agent.agent_id = j;

            rc = midware_sqlite3_get_insert_sql_prepared(table_info, &v_sip_agent);
            if(ONU_OK != rc)
            {
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for port: %ld %ld \n", i, j);
                return ONU_FAIL;
            }
        }

    }

    return ONU_OK;
}

MIDWARE_CALLBACK_RET midware_table_v_sip_agent_get(UINT32 bitmap, void *entry)
{
  MIDWARE_TABLE_V_SIP_AGENT_T *v_sip_agent;
  MIDWARE_CALLBACK_RET         callback_ret = MDW_CB_RET_SET_HW_NO_NEED;

  v_sip_agent = (MIDWARE_TABLE_V_SIP_AGENT_T *)entry;

  //TBD

   return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_v_sip_agent_set(UINT32 bitmap, void *entry)
{
  MIDWARE_TABLE_V_SIP_AGENT_T  *v_sip_agent;
  MIDWARE_CALLBACK_RET          callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
  INT32                         rc;

  v_sip_agent = (MIDWARE_TABLE_V_SIP_AGENT_T *)entry;

  if(bitmap & MIDWARE_ENTRY_V_PROXY_SERV_ADDR)
  {
      rc = VOIP_updateSipServerAddr_F(v_sip_agent->line_id, v_sip_agent->proxy_serv_addr);

      if (OK != rc)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR, "Failed to VOIP_updateSipServerAddr_F\r\n");
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;
  }

   return callback_ret;
}

ONU_STATUS midware_table_v_sip_user_init(void)
{
    ONU_STATUS                   rc = ONU_OK;
    MIDWARE_TABLE_V_SIP_USER_T   v_sip_user;
    UINT32                       i;
    MIDWARE_TABLE_INFO          *table_info;

    if(0 != midware_sqlite3_get_entry_count(MIDWARE_TABLE_V_SIP_USER))
    {
        //already got entries in the table
        return ONU_OK;
    }

    //init the default entries
    memset(&v_sip_user, 0, sizeof(v_sip_user));
#if 0
    v_sip_user.user_aor
    v_sip_user.display_name
    v_sip_user.auth_user
    v_sip_user.auth_pwd
    v_sip_user.auth_realm
    v_sip_user.auth_addr
    v_sip_user.voicemail_addr
#endif

    v_sip_user.auth_scheme          = MIDWARE_V_VALIDATE_SCHEME_NONE;
    v_sip_user.voicemail_exp_time   = MIDWARE_V_DEFT_REG_EXP_TIME;
    v_sip_user.release_timer        = MIDWARE_V_DEFT_RELEASE_TIME;
    v_sip_user.roh_timer            = MIDWARE_V_DEFT_ROH_TIME;

    table_info = midware_get_table_info(MIDWARE_TABLE_V_SIP_USER);
    for(i = 0; i < MIDWARE_SFU_POTS_PORT_NUM; i++)
    {
        v_sip_user.line_id  = i;

        rc = midware_sqlite3_get_insert_sql_prepared(table_info, &v_sip_user);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entries for port: %ld \n", i);
            return ONU_FAIL;
        }
    }

    return ONU_OK;
}

MIDWARE_CALLBACK_RET midware_table_v_sip_user_get(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_V_SIP_USER_T  *v_sip_user;
    MIDWARE_CALLBACK_RET         callback_ret = MDW_CB_RET_SET_HW_NO_NEED;

    v_sip_user = (MIDWARE_TABLE_V_SIP_USER_T *)entry;

  //TBD

   return callback_ret;
}

MIDWARE_CALLBACK_RET midware_table_v_sip_user_set(UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_V_SIP_USER_T  *v_sip_user;
    MIDWARE_CALLBACK_RET         callback_ret = MDW_CB_RET_SET_HW_NO_NEED;
    INT32                        rc;
    UINT8                        *pauth_user = 0;
    UINT8                        *pauth_pwd = 0;

    v_sip_user = (MIDWARE_TABLE_V_SIP_USER_T *)entry;

    if(bitmap & MIDWARE_ENTRY_V_AUTH_USER)
    {
        pauth_user = v_sip_user->auth_user;
    }
    if(bitmap & MIDWARE_ENTRY_V_AUTH_PWD)
    {
        pauth_pwd = v_sip_user->auth_pwd;
    }

    if(bitmap & MIDWARE_ENTRY_V_USER_AOR)
    {
      rc = VOIP_updateSipAccountData_F(v_sip_user->line_id, pauth_user, pauth_pwd, v_sip_user->user_aor);

      if (OK != rc)
      {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR, "Failed to VOIP_updateSipAccountData_F\r\n");
          return MDW_CB_RET_SET_HW_FAIL;
      }
      callback_ret = MDW_CB_RET_SET_HW_OK;

    }

    return callback_ret;
}

MIDWARE_TABLE_CALLBACK_INFO    g_midware_table_callback[] =
{
  /* table id,                      get, update, insert, remove, init  */
  {MIDWARE_TABLE_ONU_INFO        , midware_table_onu_info_get, NULL, NULL, NULL, midware_table_onu_info_init, NULL},
  {MIDWARE_TABLE_ONU_CFG         , NULL, midware_table_onu_cfg_set, NULL, NULL, midware_table_onu_cfg_init, NULL},
  {MIDWARE_TABLE_ONU_IP          , midware_table_onu_ip_get, midware_table_onu_ip_set, midware_table_onu_ip_set, NULL, midware_table_onu_ip_init, NULL},
  {MIDWARE_TABLE_UNI_CFG         , midware_table_uni_cfg_get, midware_table_uni_cfg_set, NULL, NULL, midware_table_uni_cfg_init, NULL},
  {MIDWARE_TABLE_UNI_QOS         , midware_table_uni_qos_get, midware_table_uni_qos_set, NULL, NULL, midware_table_uni_qos_init, NULL},
  {MIDWARE_TABLE_VLAN            , NULL, midware_table_vlan_set, midware_table_vlan_insert, midware_table_vlan_remove, NULL, NULL},
  {MIDWARE_TABLE_FLOW            , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_FLOW_MOD_VLAN   , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_FLOW_MOD_MAC    , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_FLOW_MOD_PPPOE  , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_FLOW_MOD_IPV4   , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_FLOW_MOD_IPV6   , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_FLOW_KEY_L2     , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_FLOW_KEY_IPV4   , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_FLOW_KEY_IPV6   , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_MC_CFG          , NULL, midware_table_mc_cfg_set, NULL, NULL, midware_table_mc_cfg_init, NULL},
  {MIDWARE_TABLE_MC_PORT_CFG     , NULL, midware_table_mc_port_set, NULL, NULL, midware_table_mc_port_init, NULL},
  {MIDWARE_TABLE_MC_PORT_STATUS  , midware_table_mc_port_status_get, NULL, NULL, NULL, midware_table_mc_port_status_init, NULL},
  {MIDWARE_TABLE_MC_ACTIVE_GROUP , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_MC_PORT_CONTROL , NULL, midware_table_mc_port_acl_set, NULL, NULL, midware_table_mc_port_acl_init, NULL},
  {MIDWARE_TABLE_MC_PORT_SERV    , NULL, midware_table_mc_port_serv_set, NULL, NULL, midware_table_mc_port_serv_init, NULL},
  {MIDWARE_TABLE_MC_PORT_PREVIEW , NULL, midware_table_mc_port_preview_set, NULL, NULL, midware_table_mc_port_preview_init, NULL},
  {MIDWARE_TABLE_MC_DS_VLAN_TRANS, NULL, midware_table_mc_ds_vlan_set, midware_table_mc_ds_vlan_insert, midware_table_mc_ds_vlan_remove, NULL, NULL},
  {MIDWARE_TABLE_MC_US_VLAN_TRANS, NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_MC_LEARN        , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_MC_STREAM       , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_WAN_QOS         , NULL, midware_table_wan_qos_set, NULL, NULL, midware_table_wan_qos_init, NULL},
  {MIDWARE_TABLE_EPON            , midware_table_epon_get, midware_table_epon_set, NULL, NULL, midware_table_epon_init, NULL},
  {MIDWARE_TABLE_DBA             , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_SW_IMAGE        , midware_table_sw_image_get, midware_table_sw_image_set, NULL, NULL, midware_table_sw_image_init, NULL},
  {MIDWARE_TABLE_OPT_TRANSCEIVER , midware_table_opt_transceiver_get, NULL, NULL, NULL, midware_table_opt_transceiver_init, NULL},
  {MIDWARE_TABLE_ALARM           , NULL, midware_table_alarm_set, midware_table_alarm_insert, midware_table_alarm_remove, NULL, NULL},
  {MIDWARE_TABLE_PM              , midware_table_pm_get, midware_table_pm_set, midware_table_pm_insert, midware_table_pm_remove, NULL, NULL},
  {MIDWARE_TABLE_PM_HISTORY      , NULL, midware_table_pm_history_set, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_EPON_AUTH       , NULL, NULL, NULL, NULL, midware_table_epon_auth_init, NULL},
  {MIDWARE_TABLE_WEB_ACCOUNT     , NULL, NULL, NULL, NULL, midware_table_web_account_init, NULL},
  {MIDWARE_TABLE_IAD_PORT_ADMIN  , NULL, midware_table_iad_port_admin_set, NULL, NULL, midware_table_iad_port_admin_init, NULL},
  {MIDWARE_TABLE_IAD_INFO        , midware_table_iad_info_get, NULL, NULL, NULL, midware_table_iad_info_init, NULL},
  {MIDWARE_TABLE_IAD_CFG         , midware_table_iad_cfg_get, midware_table_iad_cfg_set, NULL, NULL, midware_table_iad_cfg_init, NULL},
  {MIDWARE_TABLE_H248_CFG        , NULL, midware_table_h248_cfg_set, NULL, NULL, midware_table_h248_cfg_init, NULL},
  {MIDWARE_TABLE_H248_USER_INFO  , NULL, midware_table_h248_user_info_set, NULL, NULL, midware_table_h248_user_info_init, NULL},
  {MIDWARE_TABLE_H248_RTP_CFG    , NULL, midware_table_h248_rtp_cfg_set, NULL, NULL, midware_table_h248_rtp_cfg_init, NULL},
  {MIDWARE_TABLE_H248_RTP_INFO   , midware_table_h248_rtp_info_get, NULL, NULL, NULL, midware_table_h248_rtp_info_init, NULL},
  {MIDWARE_TABLE_SIP_CFG         , NULL, midware_table_sip_cfg_set, NULL, NULL, midware_table_sip_cfg_init, NULL},
  {MIDWARE_TABLE_SIP_USER_INFO   , NULL, midware_table_sip_user_info_set, NULL, NULL, midware_table_sip_user_info_init, NULL},
  {MIDWARE_TABLE_FAX_CFG         , NULL, midware_table_fax_cfg_set, NULL, NULL, midware_table_fax_cfg_init, NULL},
  {MIDWARE_TABLE_IAD_STATUS      , midware_table_iad_status_get, NULL, NULL, NULL, midware_table_iad_status_init, NULL},
  {MIDWARE_TABLE_POTS_STATUS     , midware_table_pots_status_get, NULL, NULL, NULL, midware_table_pots_status_init, NULL},
  {MIDWARE_TABLE_IAD_OPERATION   , NULL, midware_table_iad_operation_set, NULL, NULL, midware_table_iad_operation_init, NULL},
  {MIDWARE_TABLE_SIP_DIGITMAP    , NULL, midware_table_sip_digitmap_set, NULL, NULL, midware_table_sip_digitmap_init, NULL},
  {MIDWARE_TABLE_RSTP            , midware_table_rstp_get, midware_table_rstp_set, NULL, NULL, midware_table_rstp_init, NULL},
  {MIDWARE_TABLE_RSTP_PORT       , midware_table_rstp_port_get, midware_table_rstp_port_set, NULL, NULL, midware_table_rstp_port_init, NULL},
  {MIDWARE_TABLE_SW_MAC          , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_VLAN_PORT       , NULL, midware_table_vlan_port_set, NULL, NULL, midware_table_vlan_port_init, NULL},
  {MIDWARE_TABLE_ETH_UNI_PM      , midware_table_ethernet_uni_pm_get, NULL, NULL, NULL, midware_table_ethernet_uni_pm_init, NULL},
  {MIDWARE_TABLE_V_CFG           , midware_table_v_cfg_get, midware_table_v_cfg_set, NULL, NULL, midware_table_v_cfg_init, NULL},
  {MIDWARE_TABLE_V_PORT_CFG      , midware_table_v_port_cfg_get, midware_table_v_port_cfg_set, NULL, NULL, midware_table_v_port_cfg_init, NULL},
  {MIDWARE_TABLE_V_SIP_AGENT     , midware_table_v_sip_agent_get, midware_table_v_sip_agent_set, NULL, NULL, midware_table_v_sip_agent_init, NULL},
  {MIDWARE_TABLE_V_SIP_USER      , midware_table_v_sip_user_get, midware_table_v_sip_user_set, NULL, NULL, midware_table_v_sip_user_init, NULL},
  {MIDWARE_TABLE_V_APP_PROFILE   , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_V_FEATURE_NODE  , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_V_NET_DIAL_PLAN , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_V_MEDIA_PROFILE , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_V_SERV_PROFILE  , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_V_TONE_PATTERN  , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_V_TONE_EVENT    , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_V_RING_PATTERN  , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_V_RING_EVENT    , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_V_RTP_PROFILE   , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_V_LINE_STATUS   , NULL, NULL, NULL, NULL, NULL, NULL},
  {MIDWARE_TABLE_AVC             , NULL, midware_table_avc_set, midware_table_avc_insert, midware_table_avc_remove, NULL, NULL},

};

MIDWARE_TABLE_CALLBACK_INFO *midware_get_table_callback_info(MIDWARE_TABLE_ID_E table_id)
{
    int i = 0;
    int table_num = sizeof(g_midware_table_callback) / sizeof(MIDWARE_TABLE_CALLBACK_INFO);

    for(i = 0; i < table_num; i ++)
    {
        if(table_id == g_midware_table_callback[i].table_id)
        {
            return &g_midware_table_callback[i];
        }
    }

    return 0;
}



///////////////////////////////////////////////
//MIDWARE_TABLE_ONU_INFO
///////////////////////////////////////////////

MIDWARE_CALLBACK_RET midware_make_insert_callback(MIDWARE_TABLE_ID_E table_id, void *entry)
{
    MIDWARE_TABLE_CALLBACK_INFO *callback;

    callback = midware_get_table_callback_info(table_id);
    if(NULL == callback)
    {
        return MDW_CB_RET_SET_HW_FAIL;
    }

    if(NULL == callback->callback_insert)
    {
        return MDW_CB_RET_SET_HW_NO_NEED;
    }

    //MAKE CALLBACK

    return callback->callback_insert(0, entry);
}

MIDWARE_CALLBACK_RET midware_make_init_callback(MIDWARE_TABLE_ID_E table_id, void *entry)
{
    MIDWARE_TABLE_CALLBACK_INFO *callback;
    ONU_STATUS  rc;

    callback = midware_get_table_callback_info(table_id);
    if(NULL == callback)
    {
        return MDW_CB_RET_SET_HW_FAIL;
    }

    if(NULL == callback->table_init_fun)
    {
        return MDW_CB_RET_SET_HW_NO_NEED;
    }

    //MAKE CALLBACK
    rc = callback->table_init_fun();
    if(ONU_OK == rc)
    {
        return MDW_CB_RET_SET_HW_OK;
    }

    return MDW_CB_RET_SET_HW_FAIL;
}

MIDWARE_CALLBACK_RET midware_make_remove_callback(MIDWARE_TABLE_ID_E table_id, void *entry)
{
    MIDWARE_TABLE_CALLBACK_INFO *callback;

    callback = midware_get_table_callback_info(table_id);

    if(NULL == callback)
    {
        return MDW_CB_RET_SET_HW_FAIL;
    }

    //MAKE CALLBACK
    if(NULL == callback->callback_remove)
    {
        return MDW_CB_RET_SET_HW_NO_NEED;
    }

    return callback->callback_remove(0, entry);
}

MIDWARE_CALLBACK_RET midware_make_update_callback(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_CALLBACK_INFO *callback;

    callback = midware_get_table_callback_info(table_id);

    if(NULL == callback)
    {
        return MDW_CB_RET_SET_HW_NO_NEED;
    }

    //MAKE CALLBACK
    if(NULL == callback->callback_update)
    {
        return MDW_CB_RET_SET_HW_NO_NEED;
    }

    return callback->callback_update(bitmap, entry);
}


MIDWARE_CALLBACK_RET midware_make_insert_group_callback(MIDWARE_TABLE_ID_E table_id, UINT32 entry_num, void *entry)
{
    MIDWARE_TABLE_CALLBACK_INFO *callback;

    callback = midware_get_table_callback_info(table_id);

    if(NULL == callback)
    {
        return MDW_CB_RET_SET_HW_NO_NEED;
    }

    //MAKE CALLBACK
    if(NULL == callback->callback_insert_group)
    {
        return MDW_CB_RET_SET_HW_NO_NEED;
    }

    return callback->callback_insert_group(0, entry);
}

MIDWARE_CALLBACK_RET midware_make_get_callback(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_CALLBACK_INFO *callback;

    midware_sqlite3_print_entry(table_id, entry);

    callback = midware_get_table_callback_info(table_id);

    if(NULL == callback)
    {
        return MDW_CB_RET_SET_HW_FAIL;
    }
    if(NULL == callback->callback_get)
    {
        return MDW_CB_RET_SET_HW_NO_NEED;
    }

    //MAKE CALLBACK

    return callback->callback_get(bitmap, entry);
}


