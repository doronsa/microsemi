/*******************************************************************************
*               Copyright 2009, Marvell Technology Group Ltd.
*
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK), GALILEO TECHNOLOGY LTD. (GTL)
* GALILEO TECHNOLOGY, INC. (GTI) AND RADLAN Computer Communications, LTD.
********************************************************************************
* tpm_mod.c
*
* DESCRIPTION:
*
* DEPENDENCIES:
*               None
*
* CREATED BY:   JingHua
*
* DATE CREATED:
*
* FILE REVISION NUMBER:
*               $Revision: 1.9 $
*
*
*******************************************************************************/

#include <stdio.h>
#include <memory.h>
#include <malloc.h>
#include "sqlite3.h"

#include "globals.h"
#include "midware_expo.h"

#include "midware_api.h"
#include "midware_sql_translate.h"
#include "midware_table_callback.h"
#include "midware_table_def.h"


extern UINT32               midware_sqlite3_get_entry_size(MIDWARE_TABLE_INFO *table_info);
extern UINT32               midware_sqlite3_get_entry_count(MIDWARE_TABLE_ID_E table_id);
extern MIDWARE_TABLE_INFO  *midware_get_table_info(MIDWARE_TABLE_ID_E table_id);
extern void                 midware_sqlite3_print_entry(MIDWARE_TABLE_ID_E table_id, void *entry);
extern ONU_STATUS           midware_sqlite3_get_next_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry);
extern MIDWARE_CALLBACK_RET midware_make_get_callback(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry);
extern ONU_STATUS           midware_sqlite3_get_update_sql_prepared(MIDWARE_TABLE_INFO *table_info, UINT32 bitmap, void *entry);
extern ONU_STATUS           midware_sqlite3_get_first_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry);
extern ONU_STATUS           midware_sqlite3_get_select_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry);
extern MIDWARE_CALLBACK_RET midware_make_remove_callback(MIDWARE_TABLE_ID_E table_id, void *entry);
extern ONU_STATUS           midware_sqlite3_get_remove_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry);
extern ONU_STATUS           midware_sqlite3_get_insert_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry);
extern MIDWARE_CALLBACK_RET midware_make_insert_callback(MIDWARE_TABLE_ID_E table_id, void *entry);
extern MIDWARE_CALLBACK_RET midware_make_insert_group_callback(MIDWARE_TABLE_ID_E table_id, UINT32 entry_num, void *entry);
extern MIDWARE_CALLBACK_RET midware_make_update_callback(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry);


ONU_STATUS  midware_sqlite3_get_next_entry(MIDWARE_TABLE_ID_E table_id, void *entry)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    MIDWARE_CALLBACK_RET  callback_ret;
    UINT32 bitmap = 0xffffffff;

    MDW_TRACE;
    midware_sqlite3_print_entry(table_id, entry);

    table_info = midware_get_table_info(table_id);

    rc = midware_sqlite3_get_next_sql_prepared(table_info, entry);
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_WARN,"Can't get next entry: %d\n", rc);
        return ONU_FAIL;
    }

    //callback
    callback_ret = midware_make_get_callback(table_id,bitmap, entry);

    //no need to get value from HW
    if( MDW_CB_RET_SET_HW_NO_NEED == callback_ret )
    {
        return ONU_OK;
    }

    if( MDW_CB_RET_SET_HW_FAIL == callback_ret)
    {
        return ONU_FAIL;
    }
    
    //Get the value from HW to SQLite
    rc = midware_sqlite3_get_update_sql_prepared(table_info, bitmap, entry);
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get entry: %d\n", rc);
        return ONU_FAIL;
    }

    
    return ONU_OK;
}

ONU_STATUS  midware_sqlite3_get_first_entry(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    MIDWARE_CALLBACK_RET   callback_ret;    

    MDW_TRACE;
    midware_sqlite3_print_entry(table_id, entry);

    table_info = midware_get_table_info(table_id);


    rc = midware_sqlite3_get_first_sql_prepared(table_info, entry);
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_WARN,"Can't get first entry: %d\n", rc);
        return ONU_FAIL;
    }
  
    //callback
    callback_ret = midware_make_get_callback(table_id,bitmap, entry);

    //no need to get value from HW
    if( MDW_CB_RET_SET_HW_NO_NEED == callback_ret )
    {
        return ONU_OK;
    }

    if( MDW_CB_RET_SET_HW_FAIL == callback_ret)
    {
        return ONU_FAIL;
    }

    //set the value get from HW to SQLite
    rc = midware_sqlite3_get_update_sql_prepared(table_info, bitmap, entry);
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get entry: %d\n", rc);
        return ONU_FAIL;
    }

    return ONU_OK;
}


ONU_STATUS  midware_sqlite3_get_entry(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    MIDWARE_CALLBACK_RET   callback_ret;

    MDW_TRACE;
    midware_sqlite3_print_entry(table_id, entry);

    /* 
    1, get the entry from DB 
    2, call-back to get value from HW 
    3, update entry to DB if necessary
    */
    table_info = midware_get_table_info(table_id);

    rc = midware_sqlite3_get_select_sql_prepared(table_info, entry);
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_WARN,"Can't get entry: %d\n", rc);
        return ONU_FAIL;
    }

    //callback
    callback_ret = midware_make_get_callback(table_id,bitmap, entry);

    //no need to get value from HW
    if( MDW_CB_RET_SET_HW_NO_NEED == callback_ret )
    {
        return ONU_OK;
    }

    if( MDW_CB_RET_SET_HW_FAIL == callback_ret)
    {
        return ONU_FAIL;
    }

    //set the value get from HW to SQLite
    rc = midware_sqlite3_get_update_sql_prepared(table_info, bitmap, entry);
    if( ONU_FAIL == rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get entry: %d\n", rc);
        return ONU_FAIL;
    }

    
    return ONU_OK;
}


ONU_STATUS  midware_sqlite3_get_all_entry(MIDWARE_TABLE_ID_E table_id, UINT32 *entry_num, void *entry)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    UINT16   entry_size;
    UINT16   entry_cnt;
    UINT16   i;
    void         *cur_loc;

    MDW_TRACE;
    midware_sqlite3_print_entry(table_id, entry);

    table_info = midware_get_table_info(table_id);
    *entry_num = 0;
    
    //get first entry
    rc = midware_sqlite3_get_first_entry(table_id, 0xffffffff, entry);
    if( ONU_FAIL == rc )
    {
          MDW_DEBUG(MIDWARE_TRACE_LEVEL_WARN,"Can't get entry: %d, \n", rc);
          return ONU_FAIL;
    }
    
    entry_size = midware_sqlite3_get_entry_size(table_info);
    entry_cnt = midware_sqlite3_get_entry_count(table_id);

    for(i = 1; i < entry_cnt; i++)
    {
        cur_loc = (char*)entry + (i * entry_size);
        memcpy(cur_loc, cur_loc - entry_size, entry_size);
        rc = midware_sqlite3_get_next_entry(table_id, cur_loc);
        if( ONU_FAIL == rc )
        {
              MDW_DEBUG(MIDWARE_TRACE_LEVEL_WARN,"Can't get entry: %d, \n", rc);
              return ONU_FAIL;
        }
        
        //print_test(cur_loc);
    }
    (*entry_num) = entry_cnt;
    //excute this sql
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO," get %ld entry\n", *entry_num);
    
    return ONU_OK;
}

ONU_STATUS  midware_sqlite3_remove_entry(MIDWARE_TABLE_ID_E table_id, void *entry)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    MIDWARE_CALLBACK_RET   callback_ret;

    MDW_TRACE;
    midware_sqlite3_print_entry(table_id, entry);

    table_info = midware_get_table_info(table_id);

    //callback
    callback_ret = midware_make_remove_callback(table_id, entry);
    if(MDW_CB_RET_SET_HW_FAIL == callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"callback failed\n");
        return ONU_FAIL;
    }

    rc = midware_sqlite3_get_remove_sql_prepared(table_info, entry);
    if(ONU_FAIL == rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't remove entry\n");
        return ONU_FAIL;
    }

    /* Update DB status flag */
    midware_set_db_status_flag(MIDWARE_DB_STATUS_CHANGED);

    return ONU_OK;
}

ONU_STATUS midware_sqlite3_reset_table(MIDWARE_TABLE_ID_E table_id)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    UINT32 entry_size;
    void *entry;

    MDW_TRACE;
    //midware_sqlite3_print_entry(table_id, entry);

    table_info = midware_get_table_info(table_id);

    entry_size = midware_sqlite3_get_entry_size(table_info);

    entry = malloc(entry_size);

    if( NULL == entry)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't malloc mem: %ld\n", entry_size);
        return ONU_FAIL;
    }
    
    rc = midware_sqlite3_get_first_entry(table_id,  MIDWARE_ALL_ENTRY, entry);

    while(ONU_OK == rc)
    {
        rc = midware_sqlite3_remove_entry(table_id, entry);
        if(ONU_FAIL == rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't remove entry\n");
            free(entry);
            return ONU_FAIL;
        }
        rc = midware_sqlite3_get_first_entry(table_id, MIDWARE_ALL_ENTRY, entry);
    }
    
    free(entry);

    /* Update DB status flag */
    midware_set_db_status_flag(MIDWARE_DB_STATUS_CHANGED);
    
    return ONU_OK;
}

ONU_STATUS midware_sqlite3_reset_table_data(MIDWARE_TABLE_ID_E table_id)
{
    MIDWARE_TABLE_INFO          *table_info;
    ONU_STATUS                   rc;
    MIDWARE_TABLE_CALLBACK_INFO *callback;    
    UINT32                       entry_size;
    void                        *entry;

    MDW_TRACE;
    //midware_sqlite3_print_entry(table_id, entry);

    table_info = midware_get_table_info(table_id);

    entry_size = midware_sqlite3_get_entry_size(table_info);

    entry = malloc(entry_size);

    if( NULL == entry)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't malloc mem: %ld\n", entry_size);
        return ONU_FAIL;
    }
    

    rc = midware_sqlite3_get_first_entry(table_id,  MIDWARE_ALL_ENTRY, entry);

    while(ONU_OK == rc)
    {
        rc = midware_sqlite3_remove_entry(table_id, entry);
        if(ONU_FAIL == rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't remove entry\n");
            free(entry);
            return ONU_FAIL;
        }
        rc = midware_sqlite3_get_first_entry(table_id, MIDWARE_ALL_ENTRY, entry);
    }
    free(entry);

    /* Update DB status flag */
    midware_set_db_status_flag(MIDWARE_DB_STATUS_CHANGED);

    callback = midware_get_table_callback_info(table_id);
    if(NULL == callback)
    {
        return MDW_CB_RET_SET_HW_FAIL;
    }

    if(NULL == callback->table_init_fun)
    {
        return MDW_CB_RET_SET_HW_NO_NEED;
    }

    //MAKE CALLBACK
    rc = callback->table_init_fun();
    if(ONU_OK == rc)
    {
        return MDW_CB_RET_SET_HW_OK;
    }

    return MDW_CB_RET_SET_HW_FAIL;
}


ONU_STATUS midware_sqlite3_reset_table_without_callback(MIDWARE_TABLE_ID_E table_id)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    UINT32 entry_size;
    void *entry;

    MDW_TRACE;
    //midware_sqlite3_print_entry(table_id, entry);

    table_info = midware_get_table_info(table_id);

    rc = midware_sqlite3_get_reset_table_sql_prepared(table_info);
    
    if(ONU_FAIL == rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't reset table\n");
        return ONU_FAIL;
    }
    
    /* Update DB status flag */
    midware_set_db_status_flag(MIDWARE_DB_STATUS_CHANGED);

    return ONU_OK;
}


ONU_STATUS midware_sqlite3_reset_db()
{
/*
    ONU_STATUS            rc;
    UINT32                i;
    UINT32                table_num;

    table_num = sizeof(g_midware_table_info) / sizeof(MIDWARE_TABLE_INFO);

    for(i =0; i < table_num; i++)
    {
        rc = midware_sqlite3_reset_table(g_midware_table_info[i].table_id);
    }
    */
    // use other way to do this. like disconnect the db file
    return ONU_OK;
}

ONU_STATUS  midware_sqlite3_insert_entry(MIDWARE_TABLE_ID_E table_id, void *entry)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    MIDWARE_CALLBACK_RET   callback_ret;
    void * get_entry;
    UINT32 entry_size;

    MDW_TRACE;
    midware_sqlite3_print_entry(table_id, entry);

    table_info = midware_get_table_info(table_id);

    entry_size = midware_sqlite3_get_entry_size(table_info);
    get_entry = malloc(entry_size);
    if(NULL==get_entry)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"There is not enough memory\n");
        return ONU_FAIL;
    }    

    memmove(get_entry,entry,entry_size);   
    
    rc = midware_sqlite3_get_select_sql_prepared(table_info, get_entry);
    if(ONU_OK == rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"There is target entry already\n");
        free(get_entry);
        return ONU_EXIST;
    }
    free(get_entry);

    //callback
    callback_ret = midware_make_insert_callback(table_id, entry);
    if(MDW_CB_RET_SET_HW_FAIL == callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"callback failed\n");
        return ONU_FAIL;
    }

    //excute this sql
    rc = midware_sqlite3_get_insert_sql_prepared(table_info, entry);
    if(ONU_FAIL == rc)
    {
       MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't insert entry\n");
       return ONU_FAIL;
    }

    /* Update DB status flag */
    midware_set_db_status_flag(MIDWARE_DB_STATUS_CHANGED);

    return ONU_OK;
}

ONU_STATUS  midware_sqlite3_insert_group_entry(MIDWARE_TABLE_ID_E table_id, UINT32 entry_num, void *entry)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    UINT32    i;
    UINT32       entry_size;
    void         *cur_loc;
    MIDWARE_CALLBACK_RET   callback_ret;

    MDW_TRACE;
    midware_sqlite3_print_entry(table_id, entry);
    
    //callback
    callback_ret = midware_make_insert_group_callback(table_id, entry_num, entry);
    if(MDW_CB_RET_SET_HW_FAIL == callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"callback failed\n");
        return ONU_FAIL;
    }

    table_info = midware_get_table_info(table_id);

    entry_size = midware_sqlite3_get_entry_size(table_info);
    
    for(i = 0; i < entry_num; i ++)
    {
        cur_loc = (char*)entry + (i * entry_size);
         rc = midware_sqlite3_get_insert_sql_prepared(table_info, cur_loc);
        if( ONU_FAIL == rc )
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't update entry: %d\n", rc);
            return ONU_FAIL;
        }
    }

    /* Update DB status flag */
    midware_set_db_status_flag(MIDWARE_DB_STATUS_CHANGED);

    return ONU_OK;
}

ONU_STATUS midware_sqlite3_update_entry(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    MIDWARE_CALLBACK_RET   callback_ret;
    void * get_entry;
    UINT32 entry_size;
    
    MDW_TRACE;
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO, "bitmap 0x%lx\n",bitmap);
        
    midware_sqlite3_print_entry(table_id, entry);

    table_info = midware_get_table_info(table_id);

    entry_size = midware_sqlite3_get_entry_size(table_info);
    get_entry = malloc(entry_size);
    if(NULL==get_entry)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"There is not enough memory\n");
        return ONU_FAIL;
    }    

    memmove(get_entry,entry,entry_size);
        
    rc = midware_sqlite3_get_select_sql_prepared(table_info, get_entry);
    if(ONU_FAIL == rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"There is no target entry for table id %d \n", table_id);
        free(get_entry);
        return ONU_FAIL;
    }
    free(get_entry);
        
    //callback
    callback_ret = midware_make_update_callback(table_id, bitmap, entry);
    if(MDW_CB_RET_SET_HW_FAIL == callback_ret)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"callback failed\n");
        return ONU_FAIL;
    }

    //excute this sql
    rc = midware_sqlite3_get_update_sql_prepared(table_info, bitmap, entry);
    if(ONU_FAIL == rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't update entry\n");
        return ONU_FAIL;
    }

    /* Update DB status flag */
    if((table_id != MIDWARE_TABLE_ALARM) && (table_id != MIDWARE_TABLE_PM) &&
       (table_id != MIDWARE_TABLE_AVC))
    {
        midware_set_db_status_flag(MIDWARE_DB_STATUS_CHANGED);
    }

    return ONU_OK;
}


ONU_STATUS  midware_sqlite3_update_group_entry(MIDWARE_TABLE_ID_E table_id, UINT32 entry_num, UINT32 bitmap_array[], void *entry)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    UINT32    i;
    UINT32       entry_size;
    void         *cur_loc;

    MDW_TRACE;
    midware_sqlite3_print_entry(table_id, entry);

    table_info = midware_get_table_info(table_id);

    entry_size = midware_sqlite3_get_entry_size(table_info);
    
    for(i = 0; i < entry_num; i ++)
    {
        cur_loc = (char*)entry + (i * entry_size);
         rc = midware_sqlite3_update_entry(table_id, bitmap_array[i], cur_loc);
        if( ONU_FAIL == rc )
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't update entry: %d,\n", rc);
            return ONU_FAIL;
        }
    }

    /* Update DB status flag */
    midware_set_db_status_flag(MIDWARE_DB_STATUS_CHANGED);

    return ONU_OK;
}


ONU_STATUS  midware_get_next_entry(MIDWARE_TABLE_ID_E table_id, void *entry, UINT32 size)
{  
    return midware_sqlite3_get_next_entry(table_id, entry);
}

ONU_STATUS  midware_get_first_entry(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry, UINT32 size)
{
    return midware_sqlite3_get_first_entry(table_id, bitmap, entry);
}


ONU_STATUS  midware_get_entry(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry, UINT32 size)
{
    return midware_sqlite3_get_entry(table_id, bitmap, entry);
}


ONU_STATUS  midware_remove_entry(MIDWARE_TABLE_ID_E table_id, void *entry, UINT32 size)
{
    return midware_sqlite3_remove_entry(table_id, entry);
}

ONU_STATUS midware_reset_table(MIDWARE_TABLE_ID_E table_id)
{
    return midware_sqlite3_reset_table(table_id);
}

ONU_STATUS midware_reset_table_data(MIDWARE_TABLE_ID_E table_id)
{
    return midware_sqlite3_reset_table_data(table_id);
}

ONU_STATUS midware_reset_db()
{
    return midware_sqlite3_reset_db();
}

ONU_STATUS  midware_insert_entry(MIDWARE_TABLE_ID_E table_id, void *entry, UINT32 size)
{
    return midware_sqlite3_insert_entry(table_id, entry);
}

ONU_STATUS  midware_insert_group_entry(MIDWARE_TABLE_ID_E table_id, UINT32 entry_num, void *entry, UINT32 size)
{
    return midware_sqlite3_insert_group_entry(table_id, entry_num, entry);
}

ONU_STATUS midware_update_entry(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry, UINT32 size)
{
    return midware_sqlite3_update_entry(table_id, bitmap, entry) ;
}

ONU_STATUS midware_update_entry_asynchronous(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry, UINT32 size)
{
    return midware_sqlite3_update_entry(table_id, bitmap, entry);
}

ONU_STATUS midware_update_entry_without_callback(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, void *entry)
{
    MIDWARE_TABLE_INFO *table_info;
    ONU_STATUS            rc;
    void * get_entry;
    UINT32 entry_size;
    

    MDW_TRACE;
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO, "bitmap 0x%lx\n",bitmap);
        
    midware_sqlite3_print_entry(table_id, entry);

    table_info = midware_get_table_info(table_id);

    entry_size = midware_sqlite3_get_entry_size(table_info);
    get_entry = malloc(entry_size);
    if(NULL==get_entry)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"There is not enough memory\n");
        return ONU_FAIL;
    }    

    memmove(get_entry,entry,entry_size);
        
    rc = midware_sqlite3_get_select_sql_prepared(table_info, get_entry);
    if(ONU_FAIL == rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"There is no target entry\n");
        free(get_entry);
        return ONU_FAIL;
    }
    free(get_entry);
        
    //excute this sql
    rc = midware_sqlite3_get_update_sql_prepared(table_info, bitmap, entry);
    if(ONU_FAIL == rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't update entry\n");
        return ONU_FAIL;
    }

    return ONU_OK;
}

ONU_STATUS midware_save_db()
{
    system("cp /tmp/mdw_db /usr/");
    return ONU_OK;
}
