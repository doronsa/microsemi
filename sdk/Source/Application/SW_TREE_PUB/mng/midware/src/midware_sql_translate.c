/*******************************************************************************
*               Copyright 2009, Marvell Technology Group Ltd.
*
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK), GALILEO TECHNOLOGY LTD. (GTL)
* GALILEO TECHNOLOGY, INC. (GTI) AND RADLAN Computer Communications, LTD.
********************************************************************************
* tpm_mod.c
*
* DESCRIPTION:
*
* DEPENDENCIES:
*               None
*
* CREATED BY:   JingHua
*
* DATE CREATED:
*
* FILE REVISION NUMBER:
*               $Revision: 1.17 $
*
*
*******************************************************************************/
#include <stdio.h>
#include <memory.h>
#include <malloc.h>
#include <sys/stat.h>
#include "sqlite3.h"

#include "globals.h"
#include "midware_expo.h"

#include "midware_api.h"
#include "midware_sql_translate.h"
#include "midware_table_callback.h"
#include "midware_table_def.h"

#include "OsGlueLayer.h"

#include "ponOnuMngIf.h"
#include "tpm_types.h"
#include "common.h"
#include "ezxml.h"

char g_MDW_SQL_str[5000];
UINT32 midware_glob_trace = MIDWARE_TRACE_LEVEL_ERROR;//MIDWARE_ALL_TRACE_LEVEL;
#define MID_DB_SYNC_INTERVAL     10*1000 /*10*1000ms*/
#define MID_SYNC_THREAD_SIZE     0x2800
#define MID_SYNC_THREAD_PRIORITY 80
pthread_t g_mid_sync_thread;
//#define MIDWARE_GET_TABLE_DB(TABLE_ID)          g_midware_db_mem
#define MIDWARE_GET_TABLE_DB(TABLE_ID)          g_midware_table_db[(TABLE_ID)].db

MIDWARE_TABLE_DB_INFO       g_midware_table_db[MIDWARE_MAX_TABLE_NUM];

sqlite3 *g_midware_db_mem=NULL;
sqlite3 *g_midware_db_flash=NULL;
tpm_init_pon_type_t wanTech;

static bool gs_midware_save_db_flag       = false;
static UINT32 gs_midware_save_db_interval = MIDWARE_DB_DEFAULT_INTERVAL;
static uint32_t gs_uni_port_cfg[TPM_MAX_NUM_ETH_PORTS];

static MIDWARE_DB_STATUS_E gs_midware_db_status = MIDWARE_DB_STATUS_UNCHANGED ;
    
#define OPERATIONAL_XVR_BUS_ADDRESS      (0x51)

extern MIDWARE_CALLBACK_RET midware_make_init_callback(MIDWARE_TABLE_ID_E table_id, void *entry);

MIDWARE_TABLE_INFO  g_midware_table_info[] = 
{
    
    {MIDWARE_TABLE_ONU_INFO, "MIDWARE_TABLE_ONU_INFO", 31, {
    
            {"index_key",                1,                           DATA_TYPE_UINT32,   FALSE,    TRUE,     FALSE},
            {"vendor_id",                MIDWARE_VENDOR_ID_LEN+4,     DATA_TYPE_STRING,   FALSE,    FALSE,    FALSE},
            {"onu_model",                MIDWARE_ONU_MODEL_LEN+4,     DATA_TYPE_STRING,   FALSE,    FALSE,    FALSE},
            {"hw_ver",                   MIDWARE_HW_VERSION_LEN+4,    DATA_TYPE_STRING,   FALSE,    FALSE,    FALSE},
            {"sw_ver",                   MIDWARE_SW_VERSION_LEN+4,    DATA_TYPE_STRING,   FALSE,    FALSE,    FALSE},
            {"fw_ver",                   MIDWARE_FW_VERSION_LEN,      DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"chip_id",                  MIDWARE_CHIP_ID_LEN,         DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"chip_model",               1,                           DATA_TYPE_UINT16,   FALSE,    FALSE,    FALSE},
            {"chip_rev",                 1,                           DATA_TYPE_UINT32,   FALSE,    FALSE,    FALSE},
            {"chip_ver",                 MIDWARE_CHIP_VER_LEN+1,      DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"sn",                       MIDWARE_SN_LEN+4,            DATA_TYPE_STRING,   FALSE,    FALSE,    FALSE},
            {"ge_port_num",              1,                           DATA_TYPE_UINT32,   FALSE,    FALSE,    FALSE},
            {"ge_port_bitmap",           1,                           DATA_TYPE_UINT32,   FALSE,    FALSE,    FALSE},
            {"fe_port_num",              1,                           DATA_TYPE_UINT32,   FALSE,    FALSE,    FALSE},
            {"fe_port_bitmap",           1,                           DATA_TYPE_UINT32,   FALSE,    FALSE,    FALSE},
            {"pots_port_num",            1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"e1_port_num",              1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"wlan_port_num",            1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"usb_port_num",             1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"us_queue_num",             1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"us_queue_num_per_port",    1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"ds_queue_num",             1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"ds_queue_num_per_port",    1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"battery_backup",           1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"onu_type",                 1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"llid_num",                 1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"queue_num_per_llid",       1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},
            {"ipv6_support",             1,                           DATA_TYPE_UINT16,   FALSE,    FALSE,    FALSE},
            {"power_control",            1,                           DATA_TYPE_UINT16,   FALSE,    FALSE,    FALSE},
            {"onu_up_time",              1,                           DATA_TYPE_UINT32,   FALSE,    FALSE,    FALSE},
            {"service_sla",              1,                           DATA_TYPE_UINT8,    FALSE,    FALSE,    FALSE},            
            
            }
    },

    {MIDWARE_TABLE_ONU_CFG, "MIDWARE_TABLE_ONU_CFG", 10, {
    
            {"index_key",             1,                        DATA_TYPE_UINT8,  FALSE,  TRUE,   FALSE},
            {"reset_onu",             1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"restore_onu",           1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"mac_age_time",          1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"clear_dynamic_mac",     1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"oam_stack_status",      1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"synchronize_time",      1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"mib_reset",             1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"pm_interval",           1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"mtu_size",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},     
            
            }
    },

    {MIDWARE_TABLE_ONU_IP, "MIDWARE_TABLE_ONU_IP", 17, {
                            
            {"entity_id",     1,                           DATA_TYPE_UINT32, FALSE,  TRUE,   TRUE},
            {"entity_type",   1,                           DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"ip_option",     1,                           DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"onu_id",        MIDWARE_ONU_ID_LEN+3,        DATA_TYPE_STRING, FALSE,  FALSE,  TRUE},
            {"mac_addr",      MIDWARE_MAC_ADD_LEN+2,       DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"ip_addr",       MIDWARE_IPV4_IP_LEN,         DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"netmask",       MIDWARE_IPV4_IP_LEN,         DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"gateway",       MIDWARE_IPV4_IP_LEN,         DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},              
            {"pri_dns",       MIDWARE_IPV4_IP_LEN,         DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"sec_dns",       MIDWARE_IPV4_IP_LEN,         DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"cur_ip_addr",   MIDWARE_IPV4_IP_LEN,         DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"cur_netmask",   MIDWARE_IPV4_IP_LEN,         DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"cur_gateway",   MIDWARE_IPV4_IP_LEN,         DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},              
            {"cur_pri_dns",   MIDWARE_IPV4_IP_LEN,         DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"cur_sec_dns",   MIDWARE_IPV4_IP_LEN,         DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"domain_name",   MIDWARE_DOMAIN_NAME_LEN+3,   DATA_TYPE_STRING, FALSE,  FALSE,  FALSE},
            {"host_name",     MIDWARE_HOST_NAME_LEN+3,     DATA_TYPE_STRING, FALSE,  FALSE,  FALSE},       
            
            }
    },    

    {MIDWARE_TABLE_UNI_CFG, "MIDWARE_TABLE_UNI_CFG", 27, {
                            
            {"port_id",               1,                        DATA_TYPE_UINT8,  FALSE,  TRUE,   TRUE},
            {"port_type",             1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"port_admin",            1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"link_state",            1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"autoneg_cfg",           1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"autoneg_mode",          1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"reset_autoneg",         1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
/*          {"speed_cfg",             1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},*/
            {"speed_state",           1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
/*          {"duplex_cfg",            1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},*/
            {"duplex_state",          1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"loopback",              1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"port_isolate",          1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"port_flood",            1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"mac_learn_en",          1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"default_vid",           1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"default_pri",           1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"discard_tag",           1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"discard_untag",         1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"vlan_filter",           1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"loop_detect",           1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"loop_detect_status",    1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"in_mirror_en",          1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"in_mirror_src",         1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"in_mirror_dst",         1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"out_mirror_en",         1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"out_mirror_src",        1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"out_mirror_dst",        1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"stp_state",             1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
/*          {"configurationInd",      1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},*/

            }
    },

    {MIDWARE_TABLE_UNI_QOS, "MIDWARE_TABLE_UNI_QOS", 13, {
                            
            {"port_id",               1,                        DATA_TYPE_UINT8,  FALSE,  TRUE,   TRUE},
            {"fc_cfg",                1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"fc_state",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"pause_time",            1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"us_policing_en",        1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"us_policing_mode",      1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"us_policing_cir",       1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"us_policing_cbs",       1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"us_policing_ebs",       1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"ds_ratelimit_en",       1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"ds_ratelimit_mode",     1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"ds_ratelimit_cir",      1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"ds_ratelimit_pir",      1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
                            
            }
    },

    {MIDWARE_TABLE_VLAN, "MIDWARE_TABLE_VLAN", 3, {
                            
            {"vlan_id",               1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"untag_member_vec",      1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"tag_member_vec",        1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
                            
            }
    },

    {MIDWARE_TABLE_FLOW, "MIDWARE_TABLE_FLOW", 10, {
                            
            {"flow_id",               1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"hw_flow_id",            1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ingress_port",          1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"parse_bm",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"precedence",            1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"target_port",           1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"target_queue",          1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"target_gem_port",       1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"mod_bm",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"flow_en",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
                        
            }
    },

    {MIDWARE_TABLE_FLOW_MOD_VLAN, "MIDWARE_TABLE_FLOW_MOD_VLAN", 14, {
                            
            {"flow_id",               1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"vlan_oper",             1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"in_tpid",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"in_vid",                1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"in_vid_mask",           1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"in_cfi",                1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"in_pbit",               1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"in_pbit_mask",          1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"out_tpid",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"out_vid",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"out_vid_mask",          1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"out_cfi",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"out_pbit",              1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"out_pbit_mask",         1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
                        
            }
    },
    
    {MIDWARE_TABLE_FLOW_MOD_MAC, "MIDWARE_TABLE_FLOW_MOD_MAC", 5, {
                
            {"flow_id",               1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"mac_da",                MIDWARE_MAC_ADD_LEN+2,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"mac_da_mask",           MIDWARE_MAC_ADD_LEN+2,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"mac_sa",                MIDWARE_MAC_ADD_LEN+2,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"mac_sa_mask",           MIDWARE_MAC_ADD_LEN+2,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},

            }
    },
    
    {MIDWARE_TABLE_FLOW_MOD_PPPOE, "MIDWARE_TABLE_FLOW_MOD_PPPOE", 3, {
                
            {"flow_id",               1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"session_id",            1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"protocol",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            
            }
    },
    
    {MIDWARE_TABLE_FLOW_MOD_IPV4,  "MIDWARE_TABLE_FLOW_MOD_IPV4", 10, {
          
            {"flow_id",               1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"ipv4_dscp",             1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv4_dscp_mask",        1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv4_proto",            1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"ipv4_src_ip",           1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"ipv4_src_ip_mask",      1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"ipv4_dst_ip",           1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"ipv4_dst_ip_mask",      1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"ipv4_src_port",         1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"ipv4_dst_port",         1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},

            }
    },
    
    {MIDWARE_TABLE_FLOW_MOD_IPV6,  "MIDWARE_TABLE_FLOW_MOD_IPV6", 10, {
          
            {"flow_id",               1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"ipv6_dscp",             1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_dscp_mask",        1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_proto",            1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"ipv6_src_ip",           MIDWARE_IPV6_IP_LEN,      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_src_ip_mask",      MIDWARE_IPV6_IP_LEN,      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_dst_ip",           MIDWARE_IPV6_IP_LEN,      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_dst_ip_mask",      MIDWARE_IPV6_IP_LEN,      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_src_port",         1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"ipv6_dst_port",         1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},

            }
    },
    
    {MIDWARE_TABLE_FLOW_KEY_L2,  "MIDWARE_TABLE_FLOW_KEY_L2",  23, {
            
            {"flow_id",               1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"mac_da",                MIDWARE_MAC_ADD_LEN+2,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"mac_da_mask",           MIDWARE_MAC_ADD_LEN+2,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"mac_sa",                MIDWARE_MAC_ADD_LEN+2,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"mac_sa_mask",           MIDWARE_MAC_ADD_LEN+2,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"num_of_tag",            1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"in_tpid",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"in_vid",                1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"in_mask",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"in_cfi",                1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"in_pbit",               1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"in_pbit_mask",          1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"out_tpid",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"out_vid",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"out_mask",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"out_cfi",               1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"out_pbit",              1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"out_pbit_mask",         1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"eth_type",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"ppp_session",           1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"ppp_proto",             1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"gem_port",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"llid",                  1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            
            }
    },

    {MIDWARE_TABLE_FLOW_KEY_IPV4,  "MIDWARE_TABLE_FLOW_KEY_IPV4",  10, {
            
            {"flow_id",                 1,    DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"ipv4_dscp",               1,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv4_dscp_mask",          1,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv4_proto",              1,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv4_src_ip",             1,    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"ipv4_src_ip_mask",        1,    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"ipv4_dst_ip",             1,    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"ipv4_dst_ip_mask",        1,    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"ipv4_src_port",           1,    DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"ipv4_dst_port",           1,    DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            }
    },

    
    {MIDWARE_TABLE_FLOW_KEY_IPV6,  "MIDWARE_TABLE_FLOW_KEY_IPV6",  10, {
                
            {"flow_id",                 1,                      DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"ipv6_dscp",               1,                      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_dscp_mask",          1,                      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_proto",              1,                      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_src_ip",             MIDWARE_IPV6_IP_LEN,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_src_ip_mask",        MIDWARE_IPV6_IP_LEN,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_dst_ip",             MIDWARE_IPV6_IP_LEN,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_dst_ip_mask",        MIDWARE_IPV6_IP_LEN,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ipv6_src_port",           1,                      DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"ipv6_dst_port",           1,                      DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            }
    },

    
    
    {MIDWARE_TABLE_MC_CFG, "MIDWARE_TABLE_MC_CFG", 6, {
            
            {"index_key",             1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"igmp_version",          1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},            
            {"mc_mode",               1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"filter_mode",           1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"fast_leave_ability",    1,                        DATA_TYPE_UINT8,  TRUE,   FALSE,  FALSE},
            {"fast_leave_state",      1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            
            }
    },
    
    {MIDWARE_TABLE_MC_PORT_CFG,  "MIDWARE_TABLE_MC_PORT_CFG",  13, {

            {"port_id",               1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"max_group_num",         1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"us_igmp_rate",          1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},            
            {"us_tag_oper",           1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"ds_tag_oper",           1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"us_tag_default_tci",    1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"ds_tag_default_tci",    1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"robust",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"query_ip",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"query_interval",        1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"query_max_resp_time",   1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"last_query_time",       1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"unauth_join_behaviour", 1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},                
 
            }
    },

    {MIDWARE_TABLE_MC_PORT_STATUS,  "MIDWARE_TABLE_MC_PORT_STATUS",  4, {
              
            {"port_id",               1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"mc_current_bw",         1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"join_count",            1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},            
            {"exceed_bw_count",       1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},  

            }
    },

    {MIDWARE_TABLE_MC_ACTIVE_GROUP,  "MIDWARE_TABLE_MC_ACTIVE_GROUP",  8, {

            {"port_id",               1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"index_key",             1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},            
            {"vid",                   1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"src_ip",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},            
            {"dst_mc_ip",             1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"actual_bw",             1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"client_ip",             1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"last_join_time",        1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},             
 
            }
    },

    {MIDWARE_TABLE_MC_PORT_CONTROL,  "MIDWARE_TABLE_MC_PORT_CONTROL",  17, {

            {"port_id",              1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"table_type",           1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"set_ctrl",             1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},            
            {"row_key",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"gem_port",             1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"ani_vid",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"src_ip",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"dst_ip_start",         1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"dst_ip_end",           1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"imputed_group_bw",     1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"src_ipv6",             MIDWARE_IPV6_IP_LEN,      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"dst_ipv6",             MIDWARE_IPV6_IP_LEN,      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"preview_len",          1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"preview_repeat_time",  1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},      
            {"preview_repeat_count", 1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"preview_reset_time",   1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"vendor_spec",          1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},              
 
            }
    },    

    {MIDWARE_TABLE_MC_PORT_SERV,  "MIDWARE_TABLE_MC_PORT_SERV",  6, {

            {"port_id",              1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"set_ctrl",             1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},            
            {"row_key",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"vid",                  1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"max_sum_group",        1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"max_mc_bw",            1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},       
 
            }
    },     

    {MIDWARE_TABLE_MC_PORT_PREVIEW,  "MIDWARE_TABLE_MC_PORT_PREVIEW",  9, {

            {"port_id",              1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"set_ctrl",             1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},            
            {"row_key",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},      
            {"src_ip",               MIDWARE_IPV6_IP_LEN,      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"dst_ip",               MIDWARE_IPV6_IP_LEN,      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ani_vid",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"uni_vid",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"duration",             1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE}, 
            {"time_left",            1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE}, 
            
            }
    },      
    
    {MIDWARE_TABLE_MC_DS_VLAN_TRANS,  "MIDWARE_TABLE_MC_DS_VLAN_TRANS",  3, {
              
            {"port_id",               1,                        DATA_TYPE_UINT32, FALSE,  TRUE,  FALSE},
            {"in_vid",                1,                        DATA_TYPE_UINT16, FALSE,  TRUE,  FALSE},
            {"out_vid",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE, FALSE},

            }
    },

    {
    MIDWARE_TABLE_MC_US_VLAN_TRANS, "MIDWARE_TABLE_MC_US_VLAN_TRANS",   3, {
          
            {"port_id",               1,                        DATA_TYPE_UINT32, FALSE,  TRUE,  FALSE},
            {"in_vid",                1,                        DATA_TYPE_UINT16, FALSE,  TRUE,  FALSE},
            {"out_vid",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},

            }
    },

    {MIDWARE_TABLE_MC_LEARN,  "MIDWARE_TABLE_MC_LEARN",  1, {
          
            {"port_id",               1,                        DATA_TYPE_UINT32, FALSE,  TRUE,  FALSE},

            }
    },

    {MIDWARE_TABLE_MC_STREAM,  "MIDWARE_TABLE_MC_STREAM",  1, {
          
            {"port_id",               1,                        DATA_TYPE_UINT32, FALSE,  TRUE,  FALSE},

            }
    },
    
    {MIDWARE_TABLE_WAN_QOS,  "MIDWARE_TABLE_WAN_QOS",  6, {
                
            {"llid",                  1,                        DATA_TYPE_UINT16,  FALSE,  TRUE,   FALSE},
            {"queue_id",              1,                        DATA_TYPE_UINT16,  FALSE,  TRUE,   FALSE},
            {"schedule_mode",         1,                        DATA_TYPE_UINT16,  FALSE,  FALSE,  FALSE},
            {"weight",                1,                        DATA_TYPE_UINT16,  FALSE,  FALSE,  FALSE},
            {"cir",                   1,                        DATA_TYPE_UINT32,  FALSE,  FALSE,  FALSE},
            {"pir",                   1,                        DATA_TYPE_UINT32,  FALSE,  FALSE,  FALSE},
            
            }
    },
    
    {MIDWARE_TABLE_EPON, "MIDWARE_TABLE_EPON", 8, {
                
            {"index_key",             1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"base_pon_mac",          MIDWARE_MAC_ADD_LEN+2,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"multi_llid",            1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"fec",                   1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"holdover_cfg",          1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"holdover_time",         1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"register_status",       1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"onu_basic_llid",        1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},

            }
    },
    
    {MIDWARE_TABLE_DBA,  "MIDWARE_TABLE_DBA",  6, {
          
            {"llid",                  1,                            DATA_TYPE_UINT8,  FALSE,  TRUE,   FALSE},
            {"num_of_queuesets",      1,                            DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"num_of_queues",         1,                            DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"aggre_threshold",       1,                            DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"state",                 MIDWARE_DBA_MAX_QUEUESET_NUM, DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"threshold",             MIDWARE_DBA_MAX_QUEUESET_NUM, DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},

            }
    },
    
    {MIDWARE_TABLE_SW_IMAGE,  "MIDWARE_TABLE_SW_IMAGE",  9, {
          
            {"image_id",              1,                              DATA_TYPE_UINT8,  FALSE,  TRUE,   FALSE},
            {"active_image",          1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"commit_image",          1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"is_valid",              1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"verion",                MIDWARE_IMAGE_VER_LEN+4,        DATA_TYPE_STRING, FALSE,  FALSE,  FALSE},
            {"burn_to_flash",         1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"abort",                 1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"image_file_name",       MIDWARE_IMAGE_FILENAME_LEN+4,   DATA_TYPE_STRING, FALSE,  FALSE,  FALSE},
            {"status",                1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},

            }
    },
    
    {
    MIDWARE_TABLE_OPT_TRANSCEIVER,  "MIDWARE_TABLE_OPT_TRANSCEIVER",  6, {
           
            {"index_key",             1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"temperature",           1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"supply_voltage",        1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"bias_current",          1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"tx_power",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"rx_power",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            
            }
    },

    {
    MIDWARE_TABLE_ALARM,  "MIDWARE_TABLE_ALARM",  10 , {
                            
            {"type",                    1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"param1",                  1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"param2",                  1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"alarm_class",             1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"admin",                   1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"state",                   1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"dummy1",                  1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"threshold",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"clear_threshold",         1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"info",                    1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},            
            
            }
    },

    {
    MIDWARE_TABLE_PM,  "MIDWARE_TABLE_PM",    23, {

            {"type",                    1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"param1",                  1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"param2",                  1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},                            
            {"admin_bits",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"accumulation_mode",       1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"global_clear",            1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"interval",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter0",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter1",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter2",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter3",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter4",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter5",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter6",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter7",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter8",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter9",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter10",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter11",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter12",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter13",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter14",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter15",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            
            }
    },
    {
    MIDWARE_TABLE_EPON_AUTH, "MIDWARE_TABLE_EPON_AUTH", 4, {
            
            {"index_key",               1,                          DATA_TYPE_UINT16, FALSE,  TRUE,   TRUE},
            {"auth_mode",               1,                          DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"loid",                    MIDWARE_LOID_USER_LEN+4,    DATA_TYPE_STRING, FALSE,  FALSE,  TRUE},
            {"pwd",                     MIDWARE_LOID_PWD_LEN+4,     DATA_TYPE_STRING, FALSE,  FALSE,  TRUE},
            
            }
    },         
    
    {
    MIDWARE_TABLE_WEB_ACCOUNT, "MIDWARE_TABLE_WEB_ACCOUNT",  3, {
            
            {"name",                    MIDWARE_WEB_USER_LEN_PL1,   DATA_TYPE_STRING, FALSE,  TRUE,   TRUE},
            {"pwd",                     MIDWARE_WEB_PWD_LEN_PL1,    DATA_TYPE_STRING, FALSE,  FALSE,  TRUE},
            {"account_type",            1,                          DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            
            }
    },
    
    {MIDWARE_TABLE_IAD_PORT_ADMIN, "MIDWARE_TABLE_IAD_PORT_ADMIN",  2, {
              
            {"port_id",               1,                          DATA_TYPE_UINT16,  FALSE,  TRUE,   TRUE},
            {"port_admin",            1,                          DATA_TYPE_UINT16,  FALSE,  FALSE,  TRUE},

            }
    },
    
    {MIDWARE_TABLE_IAD_INFO, "MIDWARE_TABLE_IAD_INFO",  6, {
                
            {"index_key",             1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"mac_addr",              MIDWARE_MAC_ADD_LEN+2,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"protocol",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"sw_ver",                MIDWARE_IAD_SW_VER_LEN+4, DATA_TYPE_STRING, FALSE,  FALSE,  FALSE},
            {"sw_time",               MIDWARE_IAD_SW_TIME_LEN+4,DATA_TYPE_STRING, FALSE,  FALSE,  FALSE},
            {"pots_num",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            
            }
    },
    
    {MIDWARE_TABLE_IAD_CFG, "MIDWARE_TABLE_IAD_CFG", 12, {
                
            {"index_key",             1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   TRUE},
            {"ip_mode",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"ip_addr",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"net_mask",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"def_gw",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"pppoe_mode",            1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"pppoe_user",            MIDWARE_PPPOE_USER_LEN+4, DATA_TYPE_STRING, FALSE,  FALSE,  TRUE},
            {"pppoe_pwd",             MIDWARE_PPPOE_PWD_LEN+4,  DATA_TYPE_STRING, FALSE,  FALSE,  TRUE},
            {"tag_flag",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"cvlan",                 1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"svlan",                 1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"pbits",                 1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            
            }
    },
    
    {MIDWARE_TABLE_H248_CFG, "MIDWARE_TABLE_H248_CFG", 12 , {
              
            {"index_key",             1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   TRUE},
            {"mg_port",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"mgc_ip",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"mgc_port",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"backup_mgc_ip",         1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"backup_mgc_port",       1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"active_mgc",            1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"reg_mode",              1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"mg_id",                 MIDWARE_H248_MG_ID_LEN+4, DATA_TYPE_STRING, FALSE,  FALSE,  TRUE},
            {"heartbeat_mode",        1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"heartbeat_cycle",       1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"heartbeat_count",       1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},

            }
    },
    
    {MIDWARE_TABLE_H248_USER_INFO, "MIDWARE_TABLE_H248_USER_INFO", 2, {
          
            {"port_id",               1,                            DATA_TYPE_UINT32,  FALSE,  TRUE,   TRUE},
            {"user_name",             MIDWARE_H248_USER_NAME_LEN+4, DATA_TYPE_STRING,  FALSE,  FALSE,  TRUE},
            
            }
    },
    
    {MIDWARE_TABLE_H248_RTP_CFG, "MIDWARE_TABLE_H248_RTP_CFG", 6, {
            
            {"index_key",             1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   TRUE},
            {"num_of_rtp",            1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"rtp_prefix",            MIDWARE_RTP_PREFIX_LEN+4, DATA_TYPE_STRING, FALSE,  FALSE,  TRUE},
            {"rtp_digit",             MIDWARE_RTP_DIGIT_LEN+4,  DATA_TYPE_STRING, FALSE,  FALSE,  TRUE},
            {"rtp_mode",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"rtp_digit_len",         1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            
            }
    },
    
    {MIDWARE_TABLE_H248_RTP_INFO,  "MIDWARE_TABLE_H248_RTP_INFO",  3, {
            
            {"index_key",             1,                        DATA_TYPE_UINT16,  FALSE,  TRUE,   FALSE},
            {"num_of_rtp",            1,                        DATA_TYPE_UINT16,  FALSE,  FALSE,  FALSE},
            {"first_rtp_name",        MIDWARE_RTP_NAME_LEN+4,   DATA_TYPE_STRING,  FALSE,  FALSE,  FALSE},
            
            }
    },
    
    {MIDWARE_TABLE_SIP_CFG,  "MIDWARE_TABLE_SIP_CFG",  17, {
              
            {"index_key",             1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   TRUE},
            {"mg_port",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"proxy_ip",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"proxy_port",            1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"backup_proxy_ip",       1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"backup_proxy_port",     1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"active_proxy",          1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"reg_ip",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"reg_port",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"backup_reg_ip",         1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"backup_reg_port",       1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"outband_ip",            1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"outband_port",          1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"reg_interval",          1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"heartbeat_switch",      1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"heartbeat_cycle",       1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"heartbeat_count",       1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            
            }
    },
    
    {MIDWARE_TABLE_SIP_USER_INFO, "MIDWARE_TABLE_SIP_USER_INFO", 4, {
              
            {"port_id",               1,                              DATA_TYPE_UINT32,  FALSE,  TRUE,   TRUE},
            {"user_account",          MIDWARE_SIP_USER_ACCOUNT_LEN+4, DATA_TYPE_STRING,  FALSE,  FALSE,  TRUE},
            {"user_name",             MIDWARE_SIP_USER_NAME_LEN+4,    DATA_TYPE_STRING,  FALSE,  FALSE,  TRUE},
            {"user_pwd",              MIDWARE_SIP_USER_PWD_LEN+4,     DATA_TYPE_STRING,  FALSE,  FALSE,  TRUE},
            
            }
    },
    
    {MIDWARE_TABLE_FAX_CFG,  "MIDWARE_TABLE_FAX_CFG",  3, {
                
            {"index_key",             1,                        DATA_TYPE_UINT8,  FALSE,  TRUE,   TRUE},
            {"t38_en",                1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"fax_ctrl",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            
            }
    },
    
    {MIDWARE_TABLE_IAD_STATUS, "MIDWARE_TABLE_IAD_STATUS",  2, {
                
            {"index_key",             1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"status",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            }
    },
    
    {MIDWARE_TABLE_POTS_STATUS,  "MIDWARE_TABLE_POTS_STATUS",  4, {
              
            {"port_id",               1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"port_status",           1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"service_status",        1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"codec",                 1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            
            }
    },
    
    {MIDWARE_TABLE_IAD_OPERATION, "MIDWARE_TABLE_IAD_OPERATION", 2, {
              
            {"index_key",             1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"operation",             1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            
            }
    },
    
    {MIDWARE_TABLE_SIP_DIGITMAP, "MIDWARE_TABLE_SIP_DIGITMAP", 2, {
                
            {"index_key",             1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"digitmap",              MIDWARE_DIGIMAP_LEN+4,    DATA_TYPE_STRING, FALSE,  FALSE,  FALSE},
            
            }
    },
    
    {MIDWARE_TABLE_RSTP, "MIDWARE_TABLE_RSTP", 6, {
                
            {"index_key",             1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   TRUE},
            {"rstp_en",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"bridge_pri",            1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"fwd_delay",             1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"hello_time",            1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"age_time",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            }
    },
    
    {MIDWARE_TABLE_RSTP_PORT,  "MIDWARE_TABLE_RSTP_PORT",  3, {
              
            {"port_id",               1,                        DATA_TYPE_UINT16, FALSE,  TRUE,   TRUE},
            {"rstp_en",               1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"port_pri",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            
            }
    },
    
    {MIDWARE_TABLE_SW_MAC, "MIDWARE_TABLE_SW_MAC", 2, {
              
            {"mac",                   MIDWARE_MAC_ADD_LEN+2,    DATA_TYPE_UINT8,  FALSE,  TRUE,   FALSE},
            {"port_vec",              1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            
            }
    },
    
    {MIDWARE_TABLE_VLAN_PORT, "MIDWARE_TABLE_VLAN_PORT", 8, {
              
            {"port_id",               1,                                                        DATA_TYPE_UINT8,  FALSE,  TRUE,   TRUE},
            {"vlan_mode",             1,                                                        DATA_TYPE_UINT8,  FALSE,  FALSE,  TRUE},
            {"vlan_num",              1,                                                        DATA_TYPE_UINT16, FALSE,  FALSE,  TRUE},
            {"default_vlan",          1,                                                        DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"vlan_list",             MIDWARE_VLAN_LIST_LEN,                                    DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"aggre_num",             MIDWARE_AGGRE_RULE_MAX_NUM,                               DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"aggre_target_vlan",     MIDWARE_AGGRE_RULE_MAX_NUM,                               DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            {"aggre_vlan_list",       MIDWARE_AGGRE_RULE_MAX_NUM*MIDWARE_AGGRE_VLAN_MAX_NUM,    DATA_TYPE_UINT32, FALSE,  FALSE,  TRUE},
            
            }
    },
    
    {MIDWARE_TABLE_ETH_UNI_PM, "MIDWARE_TABLE_ETH_UNI_PM", 24, {

            {"port_id",               1,      DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"enable",                1,      DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"packet_rx",             1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"unicast_packet_rx",     1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"multicast_packet_rx",   1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"broadcast_packet_rx",   1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_64B_rx",         1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_65_127B_rx",     1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_128_255B_rx",    1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_256_511B_rx",    1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_512_1023B_rx",   1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_1024_1518B_rx",  1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_1519B_rx",       1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_tx",             1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"unicast_packet_tx",     1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"multicast_packet_tx",   1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"broadcast_packet_tx",   1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_64B_tx",         1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_65_127B_tx",     1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_128_255B_tx",    1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_256_511B_tx",    1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_512_1023B_tx",   1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_1024_1518B_tx",  1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_1519B_tx",       1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},         
            
            }
    },

    {MIDWARE_TABLE_V_CFG, "MIDWARE_TABLE_V_CFG", 24, {

            {"index_key",             1,                              DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"available_proto",       1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"used_proto",            1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"available_method",      1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"used_method",           1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"config_state",          1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"retrieve_profile",      1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"profile_version",       MIDWARE_DEFAULT_STRING_LEN_PL1, DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"mac_addr",              MIDWARE_MAC_ADD_LEN_PL1,        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"sw_ver",                MIDWARE_IAD_SW_VER_LEN_PL1,     DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"sw_time",               MIDWARE_IAD_SW_TIME_LEN_PL1,    DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"pots_num",              1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"ip_mode",               1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"ip_addr",               1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"net_mask",              1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"def_gw",                1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"pppoe_mode",            1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"pppoe_user",            MIDWARE_PPPOE_USER_LEN_PL1,     DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"pppoe_pwd",             MIDWARE_PPPOE_PWD_LEN_PL1,      DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"tag_flag",              1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"cvlan",                 1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"svlan",                 1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"pbits",                 1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"iad_operate",           1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            
            }
    },    

    {MIDWARE_TABLE_V_PORT_CFG,  "MIDWARE_TABLE_V_PORT_CFG",  10, {
              
            {"line_id",                  1,             DATA_TYPE_UINT8,  FALSE,  TRUE,   FALSE},
            {"admin_state",              1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"impedence",                1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"tx_path",                  1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"rx_gain",                  1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"tx_gain",                  1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"operation_state",          1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"hook_state",               1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"hookover_time",            1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},            
            {"signal_code",              1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            
            }
    },

    {MIDWARE_TABLE_V_SIP_AGENT, "MIDWARE_TABLE_V_SIP_AGENT", 29, {

            {"line_id",                  1,                              DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"agent_id",                 1,                              DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"proxy_serv_addr",          MIDWARE_OMCI_URL_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"backup_proxy_serv_addr",   MIDWARE_OMCI_URL_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"outbound_proxy_addr",      MIDWARE_OMCI_URL_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"host_part_url",            MIDWARE_OMCI_URL_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"reg_scheme",               1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"reg_user",                 MIDWARE_USER_NAME_LEN_PL1,      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"reg_pwd",                  MIDWARE_DEFAULT_STRING_LEN_PL1, DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"reg_realm",                MIDWARE_DEFAULT_STRING_LEN_PL1, DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"reg_addr",                 MIDWARE_OMCI_URL_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"backup_reg_addr",          MIDWARE_OMCI_URL_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"primary_dns",              1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"second_dns",               1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"l4_type",                  1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"l4_port",                  1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"dscp_val",                 1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"reg_expire_time",          1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"reg_head_start_time",      1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"response_code",            1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"response_tone",            1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"response_text",            MIDWARE_USER_NAME_LEN_PL1,      DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"soft_switch",              1,                              DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"optical_tx_ctrl",          1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"url_format",               1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"status",                   1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"heartbeat_switch",         1,                              DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"heartbeat_cycle",          1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"heartbeat_count",          1,                              DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
                                    
            }
    }, 

    {MIDWARE_TABLE_V_SIP_USER, "MIDWARE_TABLE_V_SIP_USER", 12, {

            {"line_id",                  1,                                    DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"user_aor",                 MIDWARE_OMCI_URL_LEN_PL1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"display_name",             MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"auth_scheme",              1,                                    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"auth_user",                MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"auth_pwd",                 MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"auth_realm",               MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"auth_addr",                MIDWARE_OMCI_URL_LEN_PL1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"voicemail_addr",           MIDWARE_OMCI_URL_LEN_PL1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"voicemail_exp_time",       1,                                    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"release_timer",            1,                                    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"roh_timer",                1,                                    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
                       
            }
    },     

    {MIDWARE_TABLE_V_APP_PROFILE, "MIDWARE_TABLE_V_APP_PROFILE", 21, {

            {"line_id",               1,                                    DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"cid_feature",           MIDWARE_OMCI_URL_LEN_PL1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"callwait_feature",      MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"call_feature",          1,                                    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"callpresent_feature",   MIDWARE_USER_NAME_LEN_PL1,            DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"directconnet_feature",  MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"dc_scheme",             1,                                    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"dc_user",               MIDWARE_USER_NAME_LEN_PL1,            DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"dc_pwd",                MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"dc_realm",              MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"dc_addr",               MIDWARE_OMCI_URL_LEN_PL1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"bridge_agent_scheme",   1,                                    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"bridge_agent_user",     MIDWARE_USER_NAME_LEN_PL1,            DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"bridge_agent_pwd",      MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"bridge_agent_realm",    MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"bridge_agent_addr",     MIDWARE_OMCI_URL_LEN_PL1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"conference_url_scheme", 1,                                    DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"conference_url_user",   MIDWARE_USER_NAME_LEN_PL1,            DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"conference_url_pwd",    MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"conference_url_realm",  MIDWARE_DEFAULT_STRING_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"conference_url_addr",   MIDWARE_OMCI_URL_LEN_PL1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
         
            }
    },       

    {MIDWARE_TABLE_V_FEATURE_NODE, "MIDWARE_TABLE_V_FEATURE_NODE", 13, {

            {"line_id",                 1,                                 DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"cancel_callwait",         MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"call_hold",               MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"call_park",               MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"cids_activate",           MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"cids_deactivate",         MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"donotdisturb_activate",   MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"donotdisturb_deactivate", MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"donotdisturb_pin_change", MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"emergency_sn",            MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"intercom_service",        MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"blind_call_transfer",     MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"attend_call_transfer",    MIDWARE_ACCESS_NODE_LEN_PL1,       DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},

            }
    },  

    {MIDWARE_TABLE_V_NET_DIAL_PLAN,  "MIDWARE_TABLE_V_NET_DIAL_PLAN",  7, {
              
            {"line_id",                  1,                       DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"dial_plan_num",            1,                       DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"dial_plan_table_size",     1,                       DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"critical_dial_timeout",    1,                       DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"partial_dial_timeout",     1,                       DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"dial_plan_format",         1,                       DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"digit_map",                MIDWARE_DIGIMAP_LEN_PL1, DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            
            }
    },    

    {MIDWARE_TABLE_V_MEDIA_PROFILE,  "MIDWARE_TABLE_V_MEDIA_PROFILE",  16, {
              
            {"line_id",                  1,             DATA_TYPE_UINT8, FALSE,  TRUE,   FALSE},
            {"fax_mode",                 1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"fax_ctrl",                 1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"code_select_1",            1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"packet_select_1",          1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"silence_sup_1",            1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"code_select_2",            1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"packet_select_2",          1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"silence_sup_2",            1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"code_select_3",            1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"packet_select_3",          1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"silence_sup_3",            1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"code_select_4",            1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"packet_select_4",          1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"silence_sup_4",            1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},            
            {"oob_dtmf",                 1,             DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            
            }
    },    

    {MIDWARE_TABLE_V_SERV_PROFILE,  "MIDWARE_TABLE_V_SERV_PROFILE",  10, {
              
            {"line_id",                  1,             DATA_TYPE_UINT8,  FALSE,  TRUE,   FALSE},
            {"announce_type",            1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"jitter_target",            1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"jitter_buf",               1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"echo_cancel_ind",          1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"pstn_proto_var",           1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"dtmf_digit_level",         1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"dtmf_digit_duration",      1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"hook_min_time",            1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"hook_max_time",            1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            
            }
    },   

    {MIDWARE_TABLE_V_TONE_PATTERN,  "MIDWARE_TABLE_V_TONE_PATTERN",  16, {
              
            {"line_id",                  1,             DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"index_key",                1,             DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"entry_index",              1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"tone_on",                  1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"frequency_1",              1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"power_1",                  1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"frequency_2",              1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"power_2",                  1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"frequency_3",              1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"power_3",                  1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"frequency_4",              1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"power_4",                  1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},            
            {"modul_frequency",          1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"modul_power",              1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"duration",                 1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"next_entry",               1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            
            }
    },       

    {MIDWARE_TABLE_V_TONE_EVENT,  "MIDWARE_TABLE_V_TONE_EVENT",  6, {
              
            {"line_id",                  1,                         DATA_TYPE_UINT8, FALSE,  TRUE,   FALSE},
            {"index_key",                1,                         DATA_TYPE_UINT8, FALSE,  TRUE,   FALSE},
            {"event",                    1,                         DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"tone_pattern",             1,                         DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"tone_file",                MIDWARE_OMCI_URL_LEN_PL1,  DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"tone_file_repet",          1,                         DATA_TYPE_UINT32,FALSE,  FALSE,  FALSE},
            
            }
    },   

    {MIDWARE_TABLE_V_RING_PATTERN,  "MIDWARE_TABLE_V_RING_PATTERN",  6, {
              
            {"line_id",                  1,             DATA_TYPE_UINT8,  FALSE,  TRUE,   FALSE},
            {"index_key",                1,             DATA_TYPE_UINT8,  FALSE,  TRUE,   FALSE},
            {"entry_index",              1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"ring_on",                  1,             DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"duration",                 1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"next_entry",               1,             DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            
            }
    },      

    {MIDWARE_TABLE_V_RING_EVENT,  "MIDWARE_TABLE_V_RING_EVENT",  7, {
              
            {"line_id",                  1,                         DATA_TYPE_UINT8, FALSE,  TRUE,   FALSE},
            {"index_key",                1,                         DATA_TYPE_UINT8, FALSE,  TRUE,   FALSE},
            {"event",                    1,                         DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"ring_pattern",             1,                         DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"ring_file",                MIDWARE_OMCI_URL_LEN_PL1,  DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},
            {"ring_file_repet",          1,                         DATA_TYPE_UINT32,FALSE,  FALSE,  FALSE},
            {"ring_text",                MIDWARE_OMCI_URL_LEN_PL1,  DATA_TYPE_UINT8, FALSE,  FALSE,  FALSE},            
            
            }
    },

    {MIDWARE_TABLE_V_RTP_PROFILE,  "MIDWARE_TABLE_V_RTP_PROFILE",  8, {
              
            {"line_id",                  1,  DATA_TYPE_UINT16, FALSE,  TRUE,   FALSE},
            {"local_port_min",           1,  DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"local_port_max",           1,  DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"dscp_mark",                1,  DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"piggyback_event",          1,  DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"tone_event",               1,  DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"dtmf_event",               1,  DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},            
            {"cas_event",                1,  DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},   
            
            }
    },       

    {MIDWARE_TABLE_V_LINE_STATUS,  "MIDWARE_TABLE_V_LINE_STATUS",  9, {
              
            {"line_id",                  1,                               DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"port_status",              1,                               DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"codec_used",               1,                               DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"server_status",            1,                               DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"port_session_type",        1,                               DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"packet_period_call_1",     1,                               DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"packet_period_call_2",     1,                               DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},            
            {"dst_addr_call_1",          MIDWARE_DEFAULT_STRING_LEN_PL1,  DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},   
            {"dst_addr_call_w",          MIDWARE_DEFAULT_STRING_LEN_PL1,  DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},              
            }
    },

    {
                
    MIDWARE_TABLE_AVC,  "MIDWARE_TABLE_AVC",    7, {
                
            {"type",                    1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"param1",                  1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"param2",                  1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},                            
            {"admin",                   1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"dummy1",                  1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"dummy2",                  1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"value",                   MIDWARE_AVC_VALUE_LENGTH, DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},                                                        
            }
    },

    {
    MIDWARE_TABLE_PM_HISTORY,  "MIDWARE_TABLE_PM_HISTORY",    23, {

            {"type",                    1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"param1",                  1,                        DATA_TYPE_UINT32, FALSE,  TRUE,   FALSE},
            {"param2",                  1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"admin_bits",              1,                        DATA_TYPE_UINT16, FALSE,  FALSE,  FALSE},
            {"accumulation_mode",       1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"global_clear",            1,                        DATA_TYPE_UINT8,  FALSE,  FALSE,  FALSE},
            {"interval",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter0",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter1",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter2",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter3",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter4",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter5",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter6",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter7",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter8",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter9",                1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter10",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter11",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter12",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter13",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter14",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},
            {"counter15",               1,                        DATA_TYPE_UINT32, FALSE,  FALSE,  FALSE},

            }
    },

};


MIDWARE_TABLE_INFO *midware_get_table_info(MIDWARE_TABLE_ID_E table_id)
{
    UINT32 i = 0;
    UINT32 table_num = sizeof(g_midware_table_info) / sizeof(MIDWARE_TABLE_INFO);

    for(i = 0; i < table_num; i ++)
    {
        if(table_id == g_midware_table_info[i].table_id)
        {
            return &g_midware_table_info[i];
        }
    }

    return NULL;
}

UINT32 midware_sqlite3_get_entry_count(MIDWARE_TABLE_ID_E table_id)
{
    // TBD
    //get entries in the table

    //SQL: select count(*) from table_name;
    
    char *tpm_sql_str = g_MDW_SQL_str;
    sqlite3_stmt *ppStmt = NULL;
    UINT32 rc;
    const char *unused;
    UINT32 value_tmp;
    MIDWARE_TABLE_INFO *table_info;

    table_info = midware_get_table_info(table_id);

    memset(&g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "select count(*) from ");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"get_entry_count: %s\n", g_MDW_SQL_str);

    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                      MIDWARE_GET_TABLE_DB(table_id),           /* Database handle */
                                      tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                      strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                      &ppStmt,          /* OUT: Statement handle */
                                      &unused         /* OUT: Pointer to unused portion of zSql */
                                        );
    if(  rc )
    {
         MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare entry_count: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_id)));
         return 0;
    }

    //excute this sql statement
    rc = sqlite3_step(ppStmt);
    if( SQLITE_ROW != rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get entry_count: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_id)));
        sqlite3_finalize(ppStmt);
        return 0;
    }
  
    value_tmp = sqlite3_column_int(ppStmt, 0);
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"value_tmp: %ld\n", value_tmp);

    //destroy the sql statement
    sqlite3_finalize(ppStmt);

    return value_tmp;
}

void midware_sqlite3_insert_col_name(
  MIDWARE_TABLE_INFO *table_info, 
  UINT32 bitmap, void *entry, char *dst_str, 
  char *operator_str, char *seperator_str)
{
    UINT32 i = 0;

    for(i = 0; i < table_info->col_num; i++)
    {
        if(0 == ((1 << i) & bitmap))
        {
            // do not contain this col
            continue;
        }
        
        MIDWARE_STR_ADD_TO_TAIL(dst_str, table_info->col_info[i].col_name);

        MIDWARE_STR_ADD_TO_TAIL(dst_str, operator_str);

        MIDWARE_STR_ADD_TO_TAIL(dst_str, "?");
        MIDWARE_STR_ADD_TO_TAIL(dst_str, seperator_str);
    }

    //remove the last 'seperator_str'
    if(0 != i)
    {
        *(dst_str + strlen(dst_str) - strlen(seperator_str)) = 0;
    }
    return;
}

UINT32 midware_sqlite3_get_table_prikey_bitmap(MIDWARE_TABLE_INFO *table_info)
{
    UINT32 i = 0;
    UINT32 bitmap = 0;

    for(i = 0; i < table_info->col_num; i++)
    {
        if(FALSE == table_info->col_info[i].primary_key)
        {
            // do not contain this col
            continue;
        }
        bitmap |= (1 <<    i);
    }

    return bitmap;
}

UINT32 midware_sqlite3_get_col_num(MIDWARE_TABLE_INFO *table_info, UINT32 bitmap)
{
    UINT32 i = 0;
    UINT32 col_name = 0;

    for(i = 0; i < table_info->col_num; i++)
    {
        if( 0 != (bitmap & (1 << i)))
        {
            col_name++;
        }
    }

    return col_name;
}

UINT32 midware_sqlite3_get_col_size(MIDWARE_COL_INFO *col_info)
{
    UINT32 col_size = 0;

    switch(col_info->datatype)
    {
        case DATA_TYPE_STRING:
        case DATA_TYPE_UINT8:
        {
            col_size = 1;
            break;
        }
        case DATA_TYPE_UINT16:
        {
            col_size = 2;
            break;
        }
        case DATA_TYPE_UINT32:
        {
            col_size = 4;
            break;
        }
        default:
        {
            col_size = 1;
        }
    }
    return col_size * col_info->size;
}

UINT32 midware_sqlite3_get_entry_size(MIDWARE_TABLE_INFO *table_info)
{
    UINT32 entry_size = 0;
    UINT32 i = 0;
  
    for(i = 0; i < table_info->col_num; i++)
    {
        entry_size = entry_size + midware_sqlite3_get_col_size(&table_info->col_info[i]);
    }
    return entry_size;
}

ONU_STATUS midware_sqlite3_bind_col_value(MIDWARE_TABLE_INFO *table_info, UINT32 bitmap, void *entry, sqlite3_stmt *ppStmt, UINT32 para_begin_index)
{
    UINT32 i = 0;
    UINT32 value_loc = 0;
    UINT32 value_tmp = 0;
    UINT32 col_size = 0;

    for(i = 0; i < table_info->col_num; i++)
    {
        col_size = midware_sqlite3_get_col_size(&(table_info->col_info[i]));

        if(0 == ((1 << i) & bitmap))
        {
            // do not contain this col
            value_loc = value_loc + col_size;
            continue;
        }
        
        switch(table_info->col_info[i].datatype)
        {
            case DATA_TYPE_UINT8:
            {
                if(1 == table_info->col_info[i].size)
                {
                    value_tmp = *(UINT8*)((char*)entry + value_loc);
                    sqlite3_bind_int(ppStmt, para_begin_index++, value_tmp);
                }
                else
                {
                    //this is a array object
                    sqlite3_bind_blob(ppStmt, para_begin_index++, 
                                        ((char*)entry + value_loc), 
                                        col_size,
                                        NULL);
                }
                break;
            }
            case DATA_TYPE_UINT16:
            {
                if(1 == table_info->col_info[i].size)
                {
                    value_tmp = *(UINT16*)((char*)entry + value_loc);
                    sqlite3_bind_int(ppStmt, para_begin_index++, value_tmp);
                }
                else
                {
                    //this is a array object
                    sqlite3_bind_blob(ppStmt, para_begin_index++, 
                                        ((char*)entry + value_loc), 
                                        col_size,
                                        NULL);
                }
                break;
            }
            case DATA_TYPE_UINT32:
            {
                if(1 == table_info->col_info[i].size)
                {
                    value_tmp = *(UINT32*)((char*)entry + value_loc);
                    sqlite3_bind_int(ppStmt, para_begin_index++, value_tmp);
                }
                else
                {
                    //this is a array object
                    sqlite3_bind_blob(ppStmt, para_begin_index++, 
                                        ((char*)entry + value_loc), 
                                        col_size,
                                        NULL);
                }
                break;
            }
            case DATA_TYPE_STRING:
            {
                sqlite3_bind_text(ppStmt, para_begin_index++, ((char*)entry + value_loc), table_info->col_info[i].size, NULL);
                break;
            }

            default:
              ;
        }
        value_loc = value_loc + midware_sqlite3_get_col_size(&(table_info->col_info[i]));
    }
    return ONU_OK;
}


ONU_STATUS midware_sqlite3_insert_col_name_type(MIDWARE_TABLE_INFO *table_info, char *tpm_sql_str)
{
    UINT32 i = 0;

    for(i = 0; i < table_info->col_num; i++)
    {
        //col name
        MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->col_info[i].col_name);

        if(DATA_TYPE_STRING == table_info->col_info[i].datatype)
        {
            //this is a TEXT
            MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " TEXT,");
        }
        else if(1 != table_info->col_info[i].size)
        {
            //this is a blob array
            MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " BLOB,");
        }
        else 
        {
            //this is a INTEGER
            switch(table_info->col_info[i].datatype)
            {
              case DATA_TYPE_UINT8:
              {
                MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " INTEGER(8),");
                break;
              }
              case DATA_TYPE_UINT16:
              {
                MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " INTEGER(16),");
                break;
              }
              case DATA_TYPE_UINT32:
              {
                MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " INTEGER(32),");
                break;
              }
              default:
                ;
            }
        }
    }
  
    //remove the last ','
    if(0 != i)
    {
        *(tpm_sql_str + strlen(tpm_sql_str) - 1) = 0;
    }

    //insert the ')'
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " )");

    return ONU_OK;
}


ONU_STATUS midware_sqlite3_get_col_value(MIDWARE_TABLE_INFO *table_info, UINT32 bitmap, void *entry, sqlite3_stmt *ppStmt)
{
    UINT32 i = 0;
    UINT32 value_loc = 0;
    UINT32 value_tmp = 0;
    UINT32 para_begin_index = 0;
    UINT32 col_size = 0;

    for(i = 0; i < table_info->col_num; i++)
    {
        col_size = midware_sqlite3_get_col_size(&(table_info->col_info[i]));
        if(0 == ((1 << i) & bitmap))
        {
            // do not contain this col
            value_loc = value_loc + col_size;
            continue;
        }
        
        switch(table_info->col_info[i].datatype)
        {
            case DATA_TYPE_UINT8:
            {
                if(1 == table_info->col_info[i].size)
                {
                    value_tmp = sqlite3_column_int(ppStmt, para_begin_index++);
                    *(UINT8*)((char*)entry + value_loc) = value_tmp;
                }
                else
                {
                    //this is a array object
                    memcpy(((char*)entry + value_loc), 
                        sqlite3_column_blob(ppStmt, para_begin_index), 
                        sqlite3_column_bytes(ppStmt, para_begin_index));
                        para_begin_index++;
                }
                break;
            }
            case DATA_TYPE_UINT16:
            {
                if(1 == table_info->col_info[i].size)
                {
                    value_tmp = sqlite3_column_int(ppStmt, para_begin_index++);
                    *(UINT16*)((char*)entry + value_loc) = value_tmp;
                }
                else
                {
                    //this is a array object
                    memcpy(((char*)entry + value_loc), 
                        sqlite3_column_blob(ppStmt, para_begin_index), 
                        sqlite3_column_bytes(ppStmt, para_begin_index));
                        para_begin_index++;
                }
                break;
            }
            case DATA_TYPE_UINT32:
            {
                if(1 == table_info->col_info[i].size)
                {
                    value_tmp = sqlite3_column_int(ppStmt, para_begin_index++);
                    *(UINT32*)((char*)entry + value_loc) = value_tmp;
                }
                else
                {
                    //this is a array object
                    memcpy(((char*)entry + value_loc), 
                        sqlite3_column_blob(ppStmt, para_begin_index), 
                        sqlite3_column_bytes(ppStmt, para_begin_index));
                    para_begin_index++;
                }
                break;
            }
            case DATA_TYPE_STRING:
            {
                memcpy(((char*)entry + value_loc), 
                    sqlite3_column_text(ppStmt, para_begin_index), 
                    sqlite3_column_bytes(ppStmt, para_begin_index));
                para_begin_index++;
                break;
            }

            default:
              ;
        }
        value_loc = value_loc + midware_sqlite3_get_col_size(&(table_info->col_info[i]));
    }
    return ONU_OK;
}

ONU_STATUS midware_sqlite3_get_rowid(MIDWARE_TABLE_INFO *table_info, void *entry, UINT32 *rowid)
{
    char *tpm_sql_str = g_MDW_SQL_str;
    sqlite3_stmt *ppStmt = NULL;
    UINT32 rc;
    UINT32 pri_key_bitmap;
    const char *unused;

    memset(&g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "SELECT ROWID FROM ");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    //where statement
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " WHERE ");
    pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
    midware_sqlite3_insert_col_name(table_info, pri_key_bitmap, entry, tpm_sql_str, " = ", " AND ");
  
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"GET ROWID: %s\n", g_MDW_SQL_str);

    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                        MIDWARE_GET_TABLE_DB(table_info->table_id),           /* Database handle */
                                        tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                        strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                        &ppStmt,          /* OUT: Statement handle */
                                        &unused         /* OUT: Pointer to unused portion of zSql */
                                          );
    if(  rc )
    {
       MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare get entry: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
       return ONU_FAIL;
    }
    
    //bind the where parameters
    midware_sqlite3_bind_col_value(table_info, pri_key_bitmap, entry, ppStmt, 1);
    
    //excute this sql statement
    rc = sqlite3_step(ppStmt);
    if( SQLITE_ROW != rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't get rowid: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
        sqlite3_finalize(ppStmt);
        return ONU_FAIL;
    }

    *rowid = sqlite3_column_int(ppStmt, 0);
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"rowid: %ld\n",  *rowid);
    
    //destroy the sql statement
    sqlite3_finalize(ppStmt);

    return ONU_OK;
}

ONU_STATUS midware_sqlite3_create_table_sql_prepared(MIDWARE_TABLE_ID_E table_id)
{
    char *tpm_sql_str = g_MDW_SQL_str;
    sqlite3_stmt *ppStmt = NULL;
    //UINT32 pri_key_bitmap;
    MIDWARE_TABLE_INFO *table_info;
    UINT32 rc;
    const char *unused;
    
    table_info = midware_get_table_info(table_id);

    memset(g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "CREATE TABLE IF NOT EXISTS ");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " (");

    //col statement
    midware_sqlite3_insert_col_name_type(table_info, tpm_sql_str);
    
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"CREATE TABLE: %s\n", g_MDW_SQL_str);

    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                      MIDWARE_GET_TABLE_DB(table_id),           /* Database handle */
                                      tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                      strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                      &ppStmt,          /* OUT: Statement handle */
                                      &unused         /* OUT: Pointer to unused portion of zSql */
                                        );
    if(rc)
    {
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare CREATE TABLE: %ld, %s\n", table_id, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_id)));
    return ONU_FAIL;
    }

    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"CREATE TABLE: %s\n", g_MDW_SQL_str);

    //excute this sql statement
    rc = sqlite3_step(ppStmt);
    if( SQLITE_DONE != rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't CREATE TABLE: %ld, %s\n", table_id, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_id)));
        sqlite3_finalize(ppStmt);
        return ONU_FAIL;
    }

    //destroy the sql statement
    sqlite3_finalize(ppStmt);

    return ONU_OK;
}

ONU_STATUS midware_sqlite3_get_update_sql_prepared(MIDWARE_TABLE_INFO *table_info, UINT32 bitmap, void *entry)
{
    char *tpm_sql_str = g_MDW_SQL_str;
    UINT32 pri_key_bitmap;
    sqlite3_stmt *ppStmt = NULL;
    UINT32 para_begin_index;
    UINT32 rc;
    const char *unused;

    memset(&g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "UPDATE ");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    //set statement
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " SET ");
    midware_sqlite3_insert_col_name(table_info, bitmap, entry, tpm_sql_str, " = ", ",");

    //where statement
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " WHERE ");
    pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
    midware_sqlite3_insert_col_name(table_info, pri_key_bitmap, entry, tpm_sql_str, " = ", " AND ");

    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"UPDATE: %s\n", g_MDW_SQL_str);

    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                      MIDWARE_GET_TABLE_DB(table_info->table_id),           /* Database handle */
                                      tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                      strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                      &ppStmt,          /* OUT: Statement handle */
                                      &unused         /* OUT: Pointer to unused portion of zSql */
                                        );
  if(  rc )
  {
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare UPDATE: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
    return ONU_FAIL;
  }

    //bind the set parameters
    midware_sqlite3_bind_col_value(table_info, bitmap, entry, ppStmt, 1);

    //bind the where parameters
    para_begin_index = midware_sqlite3_get_col_num(table_info, bitmap);
    midware_sqlite3_bind_col_value(table_info, pri_key_bitmap, entry, ppStmt, para_begin_index + 1);

    //excute this sql statement
    rc = sqlite3_step(ppStmt);
  if( SQLITE_DONE != rc )
  {
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't update entry: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
    sqlite3_finalize(ppStmt);
    return ONU_FAIL;
  }

    //destroy the sql statement
    sqlite3_finalize(ppStmt);

    return ONU_OK;
}

ONU_STATUS midware_sqlite3_get_insert_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry)
{
    char *tpm_sql_str = g_MDW_SQL_str;
    sqlite3_stmt *ppStmt = NULL;
    UINT32    i;
  UINT32 rc;
  const char *unused;

    memset(&g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "INSERT INTO ");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    //VALUE statement
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " VALUES ( ");
    
    for(i = 0; i < table_info->col_num; i++)
    {
        MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "?,");
    }

    //remove the last ','
    if(0 != i)
    {
        *(tpm_sql_str + strlen(tpm_sql_str) - 1) = 0;
    }

    //add ')'
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, ")");

    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"INSERT: %s\n", g_MDW_SQL_str);

    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                      MIDWARE_GET_TABLE_DB(table_info->table_id),           /* Database handle */
                                      tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                      strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                      &ppStmt,          /* OUT: Statement handle */
                                      &unused         /* OUT: Pointer to unused portion of zSql */
                                        );
    if(  rc )
    {
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare INSERT: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
        return ONU_FAIL;
    }

    //bind the set parameters
    midware_sqlite3_bind_col_value(table_info, 0xffffffff, entry, ppStmt, 1);

    //excute this sql statement
    rc = sqlite3_step(ppStmt);
  if( SQLITE_DONE != rc )
  {
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't INSERT entry: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
    sqlite3_finalize(ppStmt);
    return ONU_FAIL;
  }

    //destroy the sql statement
    sqlite3_finalize(ppStmt);

    midware_sqlite3_get_rowid(table_info, entry, &i);

    return ONU_OK;
}

ONU_STATUS midware_sqlite3_get_remove_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry)
{
    char *tpm_sql_str = g_MDW_SQL_str;
    sqlite3_stmt *ppStmt = NULL;
    UINT32 pri_key_bitmap;
  UINT32 rc;
  const char *unused;

    memset(&g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "DELETE FROM ");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    //where statement
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " WHERE ");
    pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
    midware_sqlite3_insert_col_name(table_info, pri_key_bitmap, entry, tpm_sql_str, " = ", " AND ");

    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"REMOVE: %s\n", g_MDW_SQL_str);

    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                      MIDWARE_GET_TABLE_DB(table_info->table_id),           /* Database handle */
                                      tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                      strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                      &ppStmt,          /* OUT: Statement handle */
                                      &unused         /* OUT: Pointer to unused portion of zSql */
                                        );
  if(  rc )
  {
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare REMOVE: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
    return ONU_FAIL;
  }

    //bind the where parameters
    midware_sqlite3_bind_col_value(table_info, pri_key_bitmap, entry, ppStmt, 1);

    //excute this sql statement
    rc = sqlite3_step(ppStmt);
  if( SQLITE_DONE != rc )
  {
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't REMOVE entry: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
    sqlite3_finalize(ppStmt);
    return ONU_FAIL;
  }

    //destroy the sql statement
    sqlite3_finalize(ppStmt);

    return ONU_OK;
}

ONU_STATUS midware_sqlite3_get_reset_table_sql_prepared(MIDWARE_TABLE_INFO *table_info)
{
    char *tpm_sql_str = g_MDW_SQL_str;
    sqlite3_stmt *ppStmt = NULL;
  UINT32 rc;
  const char *unused;

    memset(&g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "DELETE FROM ");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"RESET TABLE: %s\n", g_MDW_SQL_str);

    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                      MIDWARE_GET_TABLE_DB(table_info->table_id),           /* Database handle */
                                      tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                      strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                      &ppStmt,          /* OUT: Statement handle */
                                      &unused         /* OUT: Pointer to unused portion of zSql */
                                        );
  if(  rc )
  {
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare RESET: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
    return ONU_FAIL;
  }

    //excute this sql statement
    rc = sqlite3_step(ppStmt);
  if( SQLITE_DONE != rc )
  {
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't RESET TABLE: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
    sqlite3_finalize(ppStmt);
    return ONU_FAIL;
  }

    //destroy the sql statement
    sqlite3_finalize(ppStmt);

    return ONU_OK;
}


ONU_STATUS midware_sqlite3_get_select_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry)
{
    char *tpm_sql_str = g_MDW_SQL_str;
    sqlite3_stmt *ppStmt = NULL;
    UINT32 pri_key_bitmap;
    UINT32 rc;
    const char *unused;

    memset(&g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "SELECT * FROM ");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    //where statement
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " WHERE ");
    pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
    midware_sqlite3_insert_col_name(table_info, pri_key_bitmap, entry, tpm_sql_str, " = ", " AND ");

    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"SELECT: %s\n", g_MDW_SQL_str);

    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                      MIDWARE_GET_TABLE_DB(table_info->table_id),           /* Database handle */
                                      tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                      strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                      &ppStmt,          /* OUT: Statement handle */
                                      &unused         /* OUT: Pointer to unused portion of zSql */
                                        );
    if(  rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare SELECT: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
        return ONU_FAIL;
    }

    //bind the where parameters
    midware_sqlite3_bind_col_value(table_info, pri_key_bitmap, entry, ppStmt, 1);

    //excute this sql statement
    rc = sqlite3_step(ppStmt);
    if( SQLITE_ROW != rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_WARN,"Can't get entry: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
        sqlite3_finalize(ppStmt);
        return ONU_FAIL;
    }

    midware_sqlite3_get_col_value(table_info, 0xffffffff, entry, ppStmt);

    //destroy the sql statement
    sqlite3_finalize(ppStmt);

    return ONU_OK;
}

ONU_STATUS midware_sqlite3_get_first_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry)
{
    char *tpm_sql_str = g_MDW_SQL_str;
    sqlite3_stmt *ppStmt = NULL;
  UINT32 rc;
  const char *unused;

    memset(&g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "SELECT * FROM ");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    //where statement
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " LIMIT 1 ");

    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"FIRST: %s\n", g_MDW_SQL_str);

    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                      MIDWARE_GET_TABLE_DB(table_info->table_id),           /* Database handle */
                                      tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                      strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                      &ppStmt,          /* OUT: Statement handle */
                                      &unused         /* OUT: Pointer to unused portion of zSql */
                                        );
  if(  rc )
  {
       MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare get FIRST: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
       return ONU_FAIL;
  }

    //excute this sql statement
    rc = sqlite3_step(ppStmt);
  if( SQLITE_ROW != rc )
  {
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_WARN,"Can't get entry: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
    sqlite3_finalize(ppStmt);
    return ONU_FAIL;
  }

    midware_sqlite3_get_col_value(table_info, 0xffffffff, entry, ppStmt);

    //destroy the sql statement
    sqlite3_finalize(ppStmt);

    return ONU_OK;
}


ONU_STATUS midware_sqlite3_get_next_sql_prepared(MIDWARE_TABLE_INFO *table_info, void *entry)
{
    char *tpm_sql_str = g_MDW_SQL_str;
    sqlite3_stmt *ppStmt = NULL;
    UINT32 rc;
    const char *unused;
    UINT32 rowid;
    ONU_STATUS   onu_ret;

    //get row id
    onu_ret = midware_sqlite3_get_rowid(table_info, entry, &rowid);
    if( ONU_OK != onu_ret )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_WARN,"Can't get rowid: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
        return ONU_FAIL;
    }

    memset(&g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "SELECT * FROM ");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    //where statement
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " WHERE ROWID > ? ");
  
    //LIMIT statement
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " LIMIT 1 ");

    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"GET NEXT: %s\n", g_MDW_SQL_str);

    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                      MIDWARE_GET_TABLE_DB(table_info->table_id),           /* Database handle */
                                      tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                      strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                      &ppStmt,          /* OUT: Statement handle */
                                      &unused         /* OUT: Pointer to unused portion of zSql */
                                        );
  if(  rc )
  {
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare get entry: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
    return ONU_FAIL;
  }

    //bind the where parameters
    sqlite3_bind_int(ppStmt, 1, rowid);

    //excute this sql statement
    rc = sqlite3_step(ppStmt);
    if( SQLITE_ROW != rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_WARN,"Can't get entry: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
        sqlite3_finalize(ppStmt);
        return ONU_FAIL;
    }

    midware_sqlite3_get_col_value(table_info, 0xffffffff, entry, ppStmt);

    //destroy the sql statement
    sqlite3_finalize(ppStmt);


    return ONU_OK;
}

ONU_STATUS midware_mng_create_all_tables(void)
{
    UINT32 i = 0;
   ONU_STATUS rc;
    UINT32 table_num = sizeof(g_midware_table_info) / sizeof(MIDWARE_TABLE_INFO);

    for(i = 0; i < table_num; i ++)
    {
        //create table 
        rc = midware_sqlite3_create_table_sql_prepared(g_midware_table_info[i].table_id);
        if(ONU_FAIL == rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't update entry for table id %d\n",g_midware_table_info[i].table_id);
            return ONU_FAIL;
        }
    }

    return ONU_OK;
}

ONU_STATUS midware_mng_create_all_tables_without_mdw_db(void)
{
    UINT32 i = 0;
   ONU_STATUS rc;
    UINT32 table_num = sizeof(g_midware_table_info) / sizeof(MIDWARE_TABLE_INFO);

    for(i = 0; i < table_num; i ++)
    {
        //create table 
        rc = midware_sqlite3_create_table_sql_prepared(g_midware_table_info[i].table_id);
        if(ONU_FAIL == rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't update entry for table id %d without mdw_db\n",g_midware_table_info[i].table_id);
            continue;
        }
    }

    return ONU_OK;
}


ONU_STATUS midware_mng_fill_table_db_info(void)
{
    UINT32 i = 0;
    UINT32 i_in = 0;
    UINT32 table_num = sizeof(g_midware_table_info) / sizeof(MIDWARE_TABLE_INFO);

    for(i = 0; i < table_num; i ++)
    {
        g_midware_table_db[g_midware_table_info[i].table_id].db = g_midware_db_mem;
        for(i_in = 0; i_in < g_midware_table_info[i].col_num; i_in++)
        {
            if( TRUE == g_midware_table_info[i].col_info[i_in].save_to_flash)
            {
                g_midware_table_db[g_midware_table_info[i].table_id].db = g_midware_db_flash;
                break;
            }
        }
    }

    return ONU_OK;
}

ONU_STATUS midware_mng_init_all_tables(void)
{
    UINT32 i = 0;
   ONU_STATUS rc;
    UINT32 table_num = sizeof(g_midware_table_info) / sizeof(MIDWARE_TABLE_INFO);
    /*printf("midware table_num %d\n", table_num);*/

    for(i = 0; i < table_num; i ++)
    {
        /*printf("%d ", g_midware_table_info[i].table_id);*/
        //create table 
        rc = midware_make_init_callback(g_midware_table_info[i].table_id, NULL);
        if((MDW_CB_RET_SET_HW_OK != rc) && (MDW_CB_RET_SET_HW_NO_NEED != rc))
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't init entry for table %d\n", g_midware_table_info[i].table_id);
            //return ONU_FAIL;
            continue;
        }
    }

    /*printf("\n");*/
    return ONU_OK;
}

ONU_STATUS midware_mng_attach_db(void)
{
   ONU_STATUS rc;
   struct stat sb;

    //attach db from memory
    rc = sqlite3_open_v2(":memory:", &g_midware_db_mem, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, 0);
    if( rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't open database db_mem: %s\n", sqlite3_errmsg(g_midware_db_mem));
        sqlite3_close(g_midware_db_mem);
        g_midware_db_mem = NULL;
        return ONU_FAIL;
    }

    //copy db file from /usr/midware_db
    
    if (0 == stat("/usr/mdw_db", &sb)) 
    {
        system("cp /usr/mdw_db /tmp");
    }

    rc = sqlite3_open_v2("/tmp/mdw_db", &g_midware_db_flash, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, 0);
    if( rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't open database db_mem: %s\n", sqlite3_errmsg(g_midware_db_flash));
        sqlite3_close(g_midware_db_flash);
        g_midware_db_flash = NULL;
        return ONU_FAIL;
    }

    return ONU_OK;
}

void* midware_mng_save_db(void *arg)
{
    while(1)
    {
        osTaskDelay(gs_midware_save_db_interval*SECOND_TO_MSECOND);
        
        /* copy db file to /usr/midware_db */
        if (gs_midware_save_db_flag)
        {
            if(MIDWARE_DB_STATUS_CHANGED == midware_get_db_status_flag())
            {
                system("cp /tmp/mdw_db /usr/");
                midware_set_db_status_flag(MIDWARE_DB_STATUS_UNCHANGED);
            }
        }
    }
    return (void*)(true);
}

INT32 mid_db_sync_init(void)
{
    // Create the OMCI task - expects that message queue is ready
    if (osTaskCreate(&g_mid_sync_thread, 
                     "midSyncThread", 
                     (GLFUNCPTR) midware_mng_save_db,
                     0,
                     0,
                     MID_SYNC_THREAD_PRIORITY,
                     MID_SYNC_THREAD_SIZE) != IAMBA_OK)
    {
        printf("%s: osTaskCreate failed for OMCI task!\n\r", __FUNCTION__);
        return ERR_TASK_CREATE;
    }    

    return IAMBA_OK;
}


#if 1

UINT32 midware_sqlite3_get_table_col_count(MIDWARE_TABLE_INFO *table_info)
{
    //get column count in the table

    //SQL: select count(*) from table_name;
    
    char *tpm_sql_str = g_MDW_SQL_str;
    sqlite3_stmt *ppStmt = NULL;
    UINT32 rc;
    const char *unused;
    UINT32 value_tmp = 0;

    memset(&g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "PRAGMA table_info(");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, ")");
    
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"get column count: %s\n", g_MDW_SQL_str);

    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                      MIDWARE_GET_TABLE_DB(table_info->table_id),           /* Database handle */
                                      tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                      strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                      &ppStmt,          /* OUT: Statement handle */
                                      &unused         /* OUT: Pointer to unused portion of zSql */
                                        );
    if(  rc )
    {
         MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare get column count: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
         return 0;
    }

    //excute this sql statement
    while( SQLITE_ROW == sqlite3_step(ppStmt) )
    {
        value_tmp++;
    }

    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO ,"col count: %ld\n", value_tmp);

    //destroy the sql statement
    sqlite3_finalize(ppStmt);

    return value_tmp;
    
}

ONU_STATUS midware_sqlite3_handle_table_compatible(MIDWARE_TABLE_INFO *table_info)
{
    char *tpm_sql_str = g_MDW_SQL_str;
    sqlite3_stmt *ppStmt = NULL;
    UINT32 rc;
    UINT32 i;
    const char *unused;
    UINT32 value_tmp = 0;

    value_tmp = midware_sqlite3_get_table_col_count(table_info);
    if( value_tmp == table_info->col_num)
    {
         MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO, "no need to alter table: %s\n", table_info->table_name);
         return ONU_OK;
    }

    //ALTER TABLE TEST ADD COLUMN U17 INTEGER(16) 
    memset(&g_MDW_SQL_str, 0, sizeof(g_MDW_SQL_str));
    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, "ALTER TABLE ");

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->table_name);

    MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " ADD COLUMN ");
    
    for(i = value_tmp; i < table_info->col_num; i++)
    {
        //col name
        MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, table_info->col_info[i].col_name);

        if(DATA_TYPE_STRING == table_info->col_info[i].datatype)
        {
            //this is a TEXT
            MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " TEXT,");
        }
        else if(1 != table_info->col_info[i].size)
        {
            //this is a blob array
            MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " BLOB,");
        }
        else 
        {
            //this is a INTEGER
            switch(table_info->col_info[i].datatype)
            {
              case DATA_TYPE_UINT8:
              {
                MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " INTEGER(8),");
                break;
              }
              case DATA_TYPE_UINT16:
              {
                MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " INTEGER(16),");
                break;
              }
              case DATA_TYPE_UINT32:
              {
                MIDWARE_STR_ADD_TO_TAIL(tpm_sql_str, " INTEGER(32),");
                break;
              }
              default:
                ;
            }
        }
    }
  
    //remove the last ','
    if(0 != i)
    {
        *(tpm_sql_str + strlen(tpm_sql_str) - 1) = 0;
    }    
    
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"alter table: %s\n", g_MDW_SQL_str);
    
    //prepare this sql statement
    rc = sqlite3_prepare_v2(
                                    MIDWARE_GET_TABLE_DB(table_info->table_id),           /* Database handle */
                                    tpm_sql_str,           /* SQL statement, UTF-8 encoded */
                                    strlen(tpm_sql_str), /* Maximum length of zSql in bytes. */
                                    &ppStmt,          /* OUT: Statement handle */
                                    &unused         /* OUT: Pointer to unused portion of zSql */
                                      );
    if(  rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"alter table: %s\n", g_MDW_SQL_str);
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't prepare ALTER TABLE: rc %ld, %s, table name %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)), table_info->table_name);
        return ONU_FAIL;
    }
    
    //excute this sql statement
    rc = sqlite3_step(ppStmt);
    if( SQLITE_DONE != rc )
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't ALTER TABLE: %ld, %s\n", rc, sqlite3_errmsg(MIDWARE_GET_TABLE_DB(table_info->table_id)));
        return ONU_FAIL;
    }

    return ONU_OK;
}

ONU_STATUS midware_mng_check_table_compatible(void)
{
    UINT32 i = 0;
    ONU_STATUS rc;
    UINT32 table_num = sizeof(g_midware_table_info) / sizeof(MIDWARE_TABLE_INFO);
    struct stat sb;
    
    //check the existence of db file from /usr/midware_db
    if (-1 == stat("/usr/mdw_db", &sb)) 
    {
        //db file does not exist, no need to check compatible
        return ONU_OK;
    }
    
    for(i = 0; i < table_num; i ++)
    {
        //create table 
        rc = midware_sqlite3_handle_table_compatible(&g_midware_table_info[i]);
        if(ONU_OK != rc)
        {
            MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"Can't midware_sqlite3_handle_table_compatible\n");
            return ONU_FAIL;
        }
    }

    return ONU_OK;
}

#endif

/******************************************************************************
 *
 * Function   : midware_scan_for_integer
 *
 * Description: This function scans a hex/decimal integer (TBD- error in scan)
 *
 * Parameters :
 *
 * Returns    : uint32_t
 *
 ******************************************************************************/

static uint32_t midware_scan_for_integer(const char  *arg)
{
    uint32_t  val = 0;

    if ((arg[1] == 'x') || (arg[1] == 'X'))
        sscanf(&arg[2], "%x", &val);
    else
        sscanf(arg, "%d", &val);

    return val;
}

/******************************************************************************
 *
 * Function   : midware_get_numberic_value
 *
 * Description: This function finds child element and returns value of attrName as integer
 *
 * Parameters :
 *
 * Returns    : ONU_STATUS
 *
 ******************************************************************************/

static ONU_STATUS midware_get_numberic_value(ezxml_t parentElement, char *childElmName, char *attrName, int *intvalue)
{
    ezxml_t      xmlElement;
    const char  *attr_str;

    if ((xmlElement = ezxml_get(parentElement, childElmName, -1)) == 0)
    {
        printf("ezxml_get() of %s failed \n", childElmName);
        return ONU_FAIL;
    }
    else
    {
        if ((attr_str = ezxml_attr(xmlElement, attrName)) != 0)
        {
            // Need to strengthen: check that this really an integer or hex
            *intvalue = midware_scan_for_integer(attr_str);
        }
        else
        {
            printf("ezxml_get() of %s failed \n", attrName);
            return ONU_FAIL;
        }
    }

    return ONU_OK;
}


/******************************************************************************
 *
 * Function   : getBooleanValue
 *
 * Description: This function reads in child and returns value of attrName as bool
 *
 * Parameters :
 *
 * Returns    : ONU_STATUS
 *
 ******************************************************************************/

static ONU_STATUS midware_get_bool_value(ezxml_t parentElement, char *childElmName, char *attrName, bool *boolvalue)
{
    ezxml_t     xmlElement;
    const char  *attr_str;

    if ((xmlElement = ezxml_get(parentElement, childElmName, -1)) == 0)
    {
        printf("ezxml_get() of %s failed", childElmName);
        return ONU_FAIL;
    }
    else
    {
        if ((attr_str = ezxml_attr(xmlElement, attrName)) != 0)
        {
            if (strcmp(attr_str, "true") == 0)
            {
                *boolvalue = true;
            }
            else if (strcmp(attr_str, "false") == 0)
            {
                *boolvalue = false;
            }
            else
            {
                printf("attribute %s has invalid value '%s'", attrName, attr_str);
                return ONU_FAIL;
            }
        }
        else
        {
            printf("ezxml_get() of %s failed", attrName);
            return ONU_FAIL;
        }
    }

    return ONU_OK;
}

/******************************************************************************
 *
 * Function   : midware_get_switch_port
 *
 * Description: This function returns switch port by input src port
 *
 * Parameters :
 *
 * Returns    : UINT32
 *
 ******************************************************************************/

UINT32 midware_get_uni_port_type(UINT32 src_port)
{
    if (src_port >= TPM_MAX_NUM_ETH_PORTS)
        return 0;
    else
        return gs_uni_port_cfg[src_port];
}

/******************************************************************************
 *
 * Function   : midware_get_xml_param
 *
 * Description: This function reads midware XML files to initialze parameters
 *
 * Parameters :
 *
 * Returns    : ONU_STATUS
 *
 ******************************************************************************/

ONU_STATUS midware_get_xml_param(void)
{
    ezxml_t         xmlHead;
    ezxml_t         xmlElement; 
    bool            boolValue; 
    UINT32          intValue;
    ONU_STATUS      rc = ONU_OK;
    UINT32          port_nr; 
    UINT32          uni_port;    
    UINT32          phy_type;    
    const char     *xmlStr;    

    if ((xmlHead = ezxml_parse_file(XML_CFG_FILE_MIDWARE)) == 0)
    {
        printf("ezxml_parse_file() of %s failed", XML_CFG_FILE_MIDWARE);
        return ONU_FAIL;
    }

    /*
     * Get ELM ELM_MIDWARE_CONFIG
     */
    if ((xmlElement = ezxml_get(xmlHead, ELM_MIDWARE_CONFIG, -1)) == 0)
    {
        printf("ezxml_get() of %s failed\n", ELM_MIDWARE_CONFIG);
    }

    /* Get the boolean saveDBflag */
    if (midware_get_bool_value(xmlElement, ELM_saveDBflag, ATTR_enabled, &boolValue) == ONU_OK)
    {
        gs_midware_save_db_flag = boolValue;
    }
    else
    {
        /* Default value */
        printf("%s: %s - default is false \n", __FUNCTION__, ELM_saveDBflag);
        gs_midware_save_db_flag = false;
    }

    /* Get number saving DB interval */
    if (midware_get_numberic_value(xmlElement, ELM_saveDBInterval, ATTR_intervalTime, &intValue) == ONU_OK)
    {
        gs_midware_save_db_interval = intValue;
    }
    else
    {
        /* Default value */
        printf("%s: %s - default saving DB interval = %d\n", __FUNCTION__, MIDWARE_DB_DEFAULT_INTERVAL);
        gs_midware_save_db_interval = MIDWARE_DB_DEFAULT_INTERVAL;
    }

    if (gs_midware_save_db_interval < MIDWARE_DB_DEFAULT_INTERVAL)
    {
        gs_midware_save_db_interval = MIDWARE_DB_DEFAULT_INTERVAL;
    }

    /* Get Uni port type, whether connect to FE or GE PHY */
    memset(gs_uni_port_cfg, 0, sizeof(gs_uni_port_cfg));

    xmlElement = ezxml_get(xmlHead, ELM_MIDWARE_CONFIG, 0, ELM_uniType, -1);
    if (xmlElement == NULL)
        return ONU_FAIL;

    xmlElement = ezxml_child(xmlElement, ATTR_uni);
    if (xmlElement == NULL)
        return ONU_FAIL;

    for (port_nr = 0; xmlElement && (port_nr < TPM_MAX_NUM_ETH_PORTS); xmlElement = xmlElement->next, port_nr++) {
        /* mandatory field */
        xmlStr = ezxml_attr(xmlElement, ATTR_src_port);
        if (xmlStr == NULL) {
            printf("%s: Failed to get %s\n", __FUNCTION__, ATTR_src_port);
            rc = ONU_FAIL;
            break;
        }
        uni_port = midware_scan_for_integer(xmlStr);       
        if (uni_port >= TPM_SRC_PORT_WAN) {
            printf("%s: Invalid PORT id number - %s\n", __FUNCTION__, xmlStr);
            rc = ONU_FAIL;
            break;
        }
        
        /* mandatory field */
        xmlStr = ezxml_attr(xmlElement, ATTR_phy_type);
        if (xmlStr == NULL) {
            printf("%s: Failed to get %s\n", __FUNCTION__, ATTR_phy_type);
            rc = ONU_FAIL;
            break;
        }
        phy_type = midware_scan_for_integer(xmlStr);  

        gs_uni_port_cfg[uni_port] = phy_type;
       }

    ezxml_free(xmlHead);
    return rc;
}

ONU_STATUS midware_mng_init(void)
{
   ONU_STATUS rc;
   us_enabled_t force;

    rc = midware_get_xml_param();
    if(ONU_OK != rc)
    {
       MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"midware_get_xml_param failed\n");               
       return midware_mng_init_without_mdw_db();
    }

    get_wan_tech_param(&wanTech, &force);
    rc = midware_mng_attach_db();
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"midware_mng_attach_db failed\n");              
        return midware_mng_init_without_mdw_db();
    }

    rc = midware_mng_fill_table_db_info();
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"midware_mng_fill_table_db_info failed\n");
        return ONU_FAIL;
    }

    rc = midware_mng_create_all_tables();
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"midware_mng_create_all_tables failed\n");             
        return midware_mng_init_without_mdw_db();
    }

    rc = midware_mng_check_table_compatible();
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"midware_mng_check_table_compatible failed\n");              
        return midware_mng_init_without_mdw_db();
    }

    rc = midware_mng_init_all_tables();
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"midware_mng_create_all_tables failed\n");
        return ONU_FAIL;
    }

    rc = mid_db_sync_init();
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"mid_db_sync_init failed\n");
        return ONU_FAIL;
    }  

    return ONU_OK;
}

ONU_STATUS midware_mng_init_without_mdw_db(void)
{
    ONU_STATUS rc;
    struct stat sb;

    MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"midware_mng_init failed Now switch to start without old database!!!\n");
    
    if (0 == stat("/usr/mdw_db", &sb)) 
    {
        //db file does exists, rm it
        system("rm /usr/mdw_db -f");
    }

    if (0 == stat("/tmp/mdw_db", &sb)) 
    {
        system("rm /tmp/mdw_db -f");
    }

    sqlite3_close(g_midware_db_mem);
    sqlite3_close(g_midware_db_flash);  
        
    rc = midware_mng_attach_db();
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"without_mdw_db midware_mng_attach_db failed\n");
        return ONU_FAIL;
    }

    rc = midware_mng_fill_table_db_info();
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"without_mdw_db midware_mng_fill_table_db_info failed\n");
        return ONU_FAIL;
    }

    rc = midware_mng_create_all_tables_without_mdw_db();
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"without_mdw_db midware_mng_create_all_tables failed\n");
        return ONU_FAIL;
    }

    rc = midware_mng_init_all_tables();
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"without_mdw_db midware_mng_create_all_tables failed\n");
        return ONU_FAIL;
    }

#if 1 /*mask it for debug*/
    rc = mid_db_sync_init();
    if(ONU_OK != rc)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_ERROR,"without_mdw_db mid_db_sync_init failed\n");
        return ONU_FAIL;
    }  
#endif

    return ONU_OK;
    
}

void  midware_sqlite3_print_entry(MIDWARE_TABLE_ID_E table_id, void *entry)
{
    UINT32 i = 0;
    UINT32 i_in = 0;
    UINT32 value_loc = 0;
    UINT32 col_size = 0;
    UINT32 U8_tmp = 0;
    UINT32 U16_tmp = 0;
    UINT32 U32_tmp = 0;
    MIDWARE_TABLE_INFO *table_info;

    if(MDW_GLOB_TRACE == (MIDWARE_TRACE_LEVEL_INFO & MDW_GLOB_TRACE))
    {
        //under log level control, do not print
        return ;
    }

    table_info = midware_get_table_info(table_id);
    
    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"print entry for table:%s: \n", table_info->table_name);
    
    for(i = 0; i < table_info->col_num; i++)
    {
        MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"%s: ", table_info->col_info[i].col_name);
        switch(table_info->col_info[i].datatype)
        {
            case DATA_TYPE_UINT8:
            {
                for(i_in = 0; i_in < table_info->col_info[i].size; i_in++)
                {
                    U8_tmp = *(UINT8*)((char*)entry + value_loc++);
                    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"%ld, ", U8_tmp);
                }
                break;
            }
            case DATA_TYPE_UINT16:
            {
                for(i_in = 0; i_in < table_info->col_info[i].size; i_in++)
                {
                    U16_tmp = *(UINT16*)((char*)entry + value_loc);
                    value_loc += sizeof(UINT16);
                    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"%ld, ", U16_tmp);
                }
                break;
            }
            case DATA_TYPE_UINT32:
            {
                for(i_in = 0; i_in < table_info->col_info[i].size; i_in++)
                {
                    U32_tmp = *(UINT32*)((char*)entry + value_loc);
                    value_loc += sizeof(UINT32);
                    MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"%ld, ", U32_tmp);
                }
                break;
            }
            case DATA_TYPE_STRING:
            {
                col_size = midware_sqlite3_get_col_size(&(table_info->col_info[i]));
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"%s", (char*)entry + value_loc);
                value_loc += col_size;
                break;
            }

            default:
                MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"unknown col type: %d\n", table_info->col_info[i].datatype);
                return;
        }
     MDW_DEBUG(MIDWARE_TRACE_LEVEL_INFO,"\n");
    }
    return ;
}

ONU_STATUS  midware_set_db_status_flag(MIDWARE_DB_STATUS_E status)
{
    gs_midware_db_status = status;

    return ONU_OK;
}

MIDWARE_DB_STATUS_E  midware_get_db_status_flag(void)
{
    return gs_midware_db_status;
}
