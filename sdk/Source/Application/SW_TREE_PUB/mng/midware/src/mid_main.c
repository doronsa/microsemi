/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.                    
*                                                                                
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED  
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD  
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS  
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".      
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS      
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY    
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),      
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL     
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).                               
******************************************************************************** 
*      main.c                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:                                                      
*                                                                                
* DATE CREATED: July 12, 2010     
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.6 $                                                           
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "tpm_api.h"
#include "common.h"
#include "params.h"
#include "cli.h"
#include "params_mng.h"
#include "globals.h"
#include "errorCode.h"
#include "OmciGlobalData.h"
#include "apm.h"
#include "mng_trace.h"
#include "ponOnuMngIf.h"
#include "mipc.h"

extern INT32 is_support_onu_xvr_I2c;
extern void midware_mipc_server_handler(char* inMsg, unsigned int size);

/*******************************************************************************
* appl_init
*
* DESCRIPTION: Init Rest Process
*
* INPUTS:      none
*
* OUTPUTS:     none
*
* RETURNS:     On success, returns US_RC_OK. 
*              On error, returns US_RC_FAIL.
*
*******************************************************************************/
int appl_init(void)
{
  int rcode ;

  if (midware_mng_init() != US_RC_OK)
  { 
    printf("Middleware init failed\n");  
    return (US_RC_FAIL); 
  }
    
  return(US_RC_OK);
}

/*******************************************************************************
* appl_clear
*
* DESCRIPTION: TO BE Implemented
*
* INPUTS:      none
*
* OUTPUTS:     none
*
* RETURNS:     none
*
*******************************************************************************/
void appl_clear (void)
{

}

/*******************************************************************************
* main
*
* DESCRIPTION:  Middleware main
*
* INPUTS:       none
*
* OUTPUTS:      none
*
* RETURNS:      none
*
*******************************************************************************/
int main(int argc, char* argv[])
{
	int mipc_fd;

    if (appl_init() != US_RC_OK) 
    {
        fprintf(stderr, "Middleware init failed, aborting\n");
        return(US_RC_FAIL); 
    }
    
    mipc_fd = mipc_init("midware", 0, 0);
    if (MIPC_ERROR == mipc_fd)  
    {       
        return(US_RC_FAIL); 
    }
    
    localI2cApi_whetherOnuXvrSupportI2c(&is_support_onu_xvr_I2c);
    
    mthread_register_mq(mipc_fd, midware_mipc_server_handler);
    mthread_start();

    appl_clear();

    return(US_RC_OK);
}
