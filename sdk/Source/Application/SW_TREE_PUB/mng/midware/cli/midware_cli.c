/*******************************************************************************
*               Copyright 2009, Marvell Technology Group Ltd.
*
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK), GALILEO TECHNOLOGY LTD. (GTL)
* GALILEO TECHNOLOGY, INC. (GTI) AND RADLAN Computer Communications, LTD.
********************************************************************************
* tpm_mod.c
*
* DESCRIPTION:
*
* DEPENDENCIES:
*               None
*
* CREATED BY:   ken
*
* DATE CREATED:
*
* FILE REVISION NUMBER:
*               $Revision: 1.1.1.1 $
*
*
*******************************************************************************/
//#define  POS_PACKED      __attribute__ ((__packed__))
//#define  VOID void

#include <stdio.h>
#include <memory.h>
#include <malloc.h>
#include "globals.h"
#include "sqlite3.h"
//#include "Midapi.h"
#include "midware_expo.h"
#include "midware_sql_translate.h"
#include "clish/shell.h"
#include "clish/shell/private.h"
#include "tinyrl/tinyrl.h"
#include "midware_cli.h"

extern UINT32 midware_glob_trace;

extern MIDWARE_TABLE_INFO *midware_get_table_info(MIDWARE_TABLE_ID_E table_id);
extern lub_string_nocasecmp(const char *cs, const char *ct);

static const char *midware_cli_dataTypeStr[] =
{
    "UINT8",
	"UINT16",
	"UINT32",
	"ARRAY",
	"STRING"
};

static const char *midware_cli_boolStr[] =
{
    "NO",
	"YES"
};

static UINT32 midware_cli_findNextComma(const char *p)
{
    UINT32 i = 0;
    while(','!=*p && '\0'!=*p)
    {
        p++;
        i++;
    }
    return i;
}

static UINT32 midware_cli_findNextEqualSymbol(const char *p)
{
    UINT32 i = 0;
    while('='!=*p && ','!=*p && '\0'!=*p)
    {
        p++;
        i++;
    }
    return i;
}

static bool_t midware_cli_is_hex_num(const char *x, UINT32 len)
{
    while(len)
    {
        if(!((*x>='0'&&*x<='9') ||(*x>='a'&&*x<='f') || (*x>='A'&&*x<='F')))
        {
            return BOOL_FALSE;
        }
        x++;
        len--;
    }
    
    return BOOL_TRUE;
}

static UINT8 midware_cli_value(const char x)
{
    if(x>='0'&&x<='9')
    {
        return (x-'0');
    }
    else if(x>='a'&&x<='f')
    {
        return (x-'a'+10);
    }
    else if(x>='A'&&x<='F')
    {
        return (x-'A'+10);
    }
    else
    {
        return 0;
    }
}

static bool_t midware_cli_get_value(const char *p, UINT32 len, UINT32 *value)
{
    UINT32 rc=0;
    
    if(len!=2&&len!=4&&len!=8)
        return BOOL_FALSE;
    while(len)
    {
        rc *=16;
        rc += midware_cli_value(*p);
        p++;
        len--;
    }
    *value = rc;
    return BOOL_TRUE;
}

static bool_t midware_cli_getFieldValueArray(const clish_shell_t *this, char *entry, const char *p,MIDWARE_TABLE_INFO *table_info,UINT32 *bitmap)
{
    UINT8 tmpStr[100];
    UINT32 offset = 0;
    MIDWARE_TABLE_ID_E table_id = 0;
    UINT32 index = 0;
    UINT32 offset_in_col = 0;

    UINT8 tmp_value_uint8=0;
    UINT16 tmp_value_uint16=0;
    UINT32 tmp_value_uint32=0;
    
    UINT32 value_loc = 0;
    UINT32 col_size = 0;
    
    while(1)
    {
        offset=midware_cli_findNextEqualSymbol(p);
        
        if(0==offset)
        {
            tinyrl_printf(this->tinyrl,"There is a 0 length field name in arg!\n");
            return BOOL_FALSE;
        }
    
        memset(tmpStr,0,sizeof(tmpStr));
        memcpy(tmpStr,p,offset);

        if(','==*(p+offset)||'\0'==*(p+offset))
        {
            tinyrl_printf(this->tinyrl,"Please input = in str %s!\n", tmpStr);
            
            return BOOL_FALSE;
        }
        else
        {
            value_loc = 0;
            for(index=0;index<table_info->col_num;index++)
            {
                col_size = midware_sqlite3_get_col_size(&(table_info->col_info[index]));
                if(0==lub_string_nocasecmp(tmpStr,table_info->col_info[index].col_name))
                {
                    *bitmap |= (1<<index);
                    break;
                }
                else
                {                        
                    value_loc = value_loc+col_size;
                }
            }
            if(table_info->col_num==index)
            {
                tinyrl_printf(this->tinyrl,"There is no field \"%s\" in table %d!\n", tmpStr, table_info->table_id);
                
                return BOOL_FALSE;
            }

            if(table_info->col_info[index].readonly)
            {
                tinyrl_printf(this->tinyrl,"field %s in table %d is readonly!\n", tmpStr, table_info->table_id);
                
                return BOOL_FALSE;
            }
        }

        p+=offset+1;
        offset=midware_cli_findNextComma(p);            
        if(0==offset)
        {
            tinyrl_printf(this->tinyrl,"There is a 0 length field value in arg!\n");            
            return BOOL_FALSE;
        }
        
        memset(tmpStr,0,sizeof(tmpStr));
        memcpy(tmpStr,p,offset);
        if(DATA_TYPE_STRING == table_info->col_info[index].datatype)
        {
            strncpy(entry+value_loc,tmpStr,col_size-1);
        }
        else if(DATA_TYPE_UINT8 == table_info->col_info[index].datatype)
        {            
            if(table_info->col_info[index].size>1)
            {
                if(offset!=(2*table_info->col_info[index].size))
                {
                    tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, should input %d hex UINT8 number(0x need not be input,if m<0x0f, should input 0m)!\n", 
                    table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                    return BOOL_FALSE;
                }
                
                offset_in_col = 0;
                while(offset_in_col<(2*table_info->col_info[index].size))
                {                    
                    if(!midware_cli_is_hex_num(p+offset_in_col,2))
                    {
                        tinyrl_printf(this->tinyrl,"table id %d, col_name %s value should be hex number!\n",
                        table_id, table_info->col_info[index].col_name);
                        return BOOL_FALSE;
                    }                    
                    midware_cli_get_value(p+offset_in_col,2,&tmp_value_uint32);                    
                    tmp_value_uint8 = (UINT8)(tmp_value_uint32);
                    memcpy(entry+value_loc+(offset_in_col/2),&tmp_value_uint8,1);
                    offset_in_col+=2;                     
                }
            }
            else
            {
                if(atoi(tmpStr)>0xff)
                {
                    tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, input value %d is beyond 0xff!\n",
                    table_id, table_info->col_info[index].col_name,atoi(tmpStr));
                    return BOOL_FALSE;
                }
                tmp_value_uint8 = atoi(tmpStr);                
                memcpy(entry+value_loc,&tmp_value_uint8,1);
            }                                
        }
        else if(DATA_TYPE_UINT16 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                if(offset!=(4*table_info->col_info[index].size))
                {
                    tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, should input %d hex UINT16 number(0x need not be input,if m<0x0f, should input 0m)!\n", 
                    table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                    return BOOL_FALSE;
                }
                
                offset_in_col = 0;
                while(offset_in_col<(4*table_info->col_info[index].size))
                {                    
                    if(!midware_cli_is_hex_num(p+offset_in_col,4))
                    {
                        tinyrl_printf(this->tinyrl,"table id %d, col_name %s value should be hex number!\n",
                        table_id, table_info->col_info[index].col_name);
                        return BOOL_FALSE;
                    }
                    midware_cli_get_value(p+offset_in_col,4,&tmp_value_uint32);
                    tmp_value_uint16 = (UINT16)(tmp_value_uint32);
                    memcpy(entry+value_loc+(offset_in_col/2),&tmp_value_uint16,2);
                    offset_in_col+=4;
                }
            }
            else
            {
                if(atoi(tmpStr)>0xffff)
                {
                    tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, input value %d is beyond 0xffff!\n",
                    table_id, table_info->col_info[index].col_name,atoi(tmpStr));
                    return BOOL_FALSE;
                }
                tmp_value_uint16 = atoi(tmpStr);                
                memcpy(entry+value_loc,&tmp_value_uint16,2);
            }                                
        }        
        else if(DATA_TYPE_UINT32 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                if(offset!=(8*table_info->col_info[index].size))
                {
                    tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, should input %d hex UINT32 number(0x need not be input,if m < 0x0f, should input 0m)!\n", 
                    table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                    return BOOL_FALSE;
                }
                
                offset_in_col = 0;
                while(offset_in_col<(8*table_info->col_info[index].size))
                {                    
                    if(!midware_cli_is_hex_num(p+offset_in_col,8))
                    {
                        tinyrl_printf(this->tinyrl,"table id %d, col_name %s value should be hex number!\n",
                        table_id, table_info->col_info[index].col_name);
                        return BOOL_FALSE;
                    }
                    midware_cli_get_value(p+offset_in_col,8,&tmp_value_uint32);
                    memcpy(entry+value_loc+(offset_in_col/2),&tmp_value_uint32,4);
                    offset_in_col+=8;
                }
            }
            else
            {
                tmp_value_uint32 = atoi(tmpStr);
                memcpy(entry+value_loc,&tmp_value_uint32,4);
            }                                
        }        
        else if(DATA_TYPE_ARRAY == table_info->col_info[index].datatype)
        {
            memcpy(entry+value_loc,&tmpStr,col_size);
        }
        
        if('\0'==*(p+offset))
        {
            break;
        }
        p+=offset+1;
    }
    return BOOL_TRUE;
}
#if 0
static bool_t midware_cli_getFieldValueArray(const clish_shell_t *this, char *entry, const char *p,MIDWARE_TABLE_INFO *table_info,UINT32 *bitmap)
{
    UINT8 tmpStr[100];
    UINT32 offset = 0;
    MIDWARE_TABLE_ID_E table_id = 0;
    UINT32 index = 0;
    UINT32 col_index = 0;

    UINT8 tmp_value_uint8=0;
    UINT16 tmp_value_uint16=0;
    UINT32 tmp_value_uint32=0;
    
    UINT32 value_loc = 0;
    UINT32 col_size = 0;

    while(1)
    {
        offset=midware_cli_findNextComma(p);
        if(0==offset)
        {
            tinyrl_printf(this->tinyrl,"There is a 0 length field name in arg!\n");
            return BOOL_FALSE;
        }
    
        memset(tmpStr,0,sizeof(tmpStr));
        memcpy(tmpStr,p,offset);

        if('\0'==*(p+offset))
        {
            tinyrl_printf(this->tinyrl,"Please input field %s's value!\n", tmpStr);
            
            return BOOL_FALSE;
        }
        else
        {
            value_loc = 0;
            for(index=0;index<table_info->col_num;index++)
            {
                col_size = midware_sqlite3_get_col_size(&(table_info->col_info[index]));
                if(0==lub_string_nocasecmp(tmpStr,table_info->col_info[index].col_name))
                {
                    *bitmap |= (1<<index);
                    break;
                }
                else
                {                        
                    value_loc = value_loc+col_size;
                }
            }
            
            if(table_info->col_num==index)
            {
                tinyrl_printf(this->tinyrl,"There is no field \"%s\" in table %d!\n", tmpStr, table_info->table_id);
                
                return BOOL_FALSE;
            }

            if(table_info->col_info[index].readonly)
            {
                tinyrl_printf(this->tinyrl,"field %s in table %d is readonly!\n", tmpStr, table_info->table_id);
                
                return BOOL_FALSE;
            }
        }

        p+=offset+1;
        offset=midware_cli_findNextComma(p);            
        if(0==offset)
        {
            tinyrl_printf(this->tinyrl,"There is a 0 length field value in arg!\n");            
            return BOOL_FALSE;
        }
        
        memset(tmpStr,0,sizeof(tmpStr));
        memcpy(tmpStr,p,offset);

        if(DATA_TYPE_STRING == table_info->col_info[index].datatype)
        {
            strncpy(entry+value_loc,tmpStr,col_size-1);
        }
        else if(DATA_TYPE_UINT8 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                col_index = 0;
                while(1)
                {
                    offset = midware_cli_findNextDot(p);
                    col_index++;

                    if(col_index>table_info->col_info[index].size)
                    {
                        tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d too many array!\n", 
                        table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                        return BOOL_FALSE;
                    }
                    if(','==*(p+offset) || '\0'==*(p+offset))
                    {
                        if(col_index<table_info->col_info[index].size)
                        {
                            tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d now only have %d!\n",
                            table_id, table_info->col_info[index].col_name,table_info->col_info[index].size,col_index);
                            return BOOL_FALSE;
                        }
                        else
                        {
                            memset(tmpStr,0,sizeof(tmpStr));
                            memcpy(tmpStr,p,offset);
                            if(atoi(tmpStr)>0xff)
                            {
                                tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, input value %d is beyond 0xff!\n",
                                table_id, table_info->col_info[index].col_name,atoi(tmpStr));
                                return BOOL_FALSE;
                            }
                            tmp_value_uint8 = atoi(tmpStr);                            
                            memcpy(entry+value_loc+col_index-1,&tmp_value_uint8,1);
                            break;
                        }
                    }
                    memset(tmpStr,0,sizeof(tmpStr));
                    memcpy(tmpStr,p,offset);
                    if(atoi(tmpStr)>0xff)
                            {
                                tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, input value %d is beyond 0xff!\n",
                                table_id, table_info->col_info[index].col_name,atoi(tmpStr));
                                return BOOL_FALSE;
                            }
                    tmp_value_uint8 = atoi(tmpStr);                    
                    memcpy(entry+value_loc+col_index-1,&tmp_value_uint8,1);
                    p+=offset+1;
                }
            }
            else
            {
                if(atoi(tmpStr)>0xff)
                {
                    tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, input value %d is beyond 0xff!\n",
                    table_id, table_info->col_info[index].col_name,atoi(tmpStr));
                    return BOOL_FALSE;
                }
                tmp_value_uint8 = atoi(tmpStr);                
                memcpy(entry+value_loc,&tmp_value_uint8,1);
            }                                
        }
        else if(DATA_TYPE_UINT16 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                col_index = 0;
                while(1)
                {
                    offset = midware_cli_findNextDot(p);
                    col_index++;

                    if(col_index>table_info->col_info[index].size)
                    {
                        tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d too many array!\n", 
                        table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                        return BOOL_FALSE;
                    }
                    if(','==*(p+offset) || '\0'==*(p+offset))
                    {
                        if(col_index<table_info->col_info[index].size)
                        {
                            tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d now only have %d!\n",
                            table_id, table_info->col_info[index].col_name,table_info->col_info[index].size,col_index);
                            return BOOL_FALSE;
                        }
                        else
                        {
                            memset(tmpStr,0,sizeof(tmpStr));
                            memcpy(tmpStr,p,offset);
                            if(atoi(tmpStr)>0xffff)
                            {
                                tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, input value %d is beyond 0xffff!\n",
                                table_id, table_info->col_info[index].col_name,atoi(tmpStr));
                                return BOOL_FALSE;
                            }
                            tmp_value_uint16 = atoi(tmpStr);                            
                            memcpy(entry+value_loc+(col_index-1)*2,&tmp_value_uint16,2);
                            break;
                        }
                    }
                    memset(tmpStr,0,sizeof(tmpStr));
                    memcpy(tmpStr,p,offset);                    
                    if(atoi(tmpStr)>0xffff)
                    {
                        tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, input value %d is beyond 0xffff!\n",
                        table_id, table_info->col_info[index].col_name,atoi(tmpStr));
                        return BOOL_FALSE;
                    }
                    tmp_value_uint16 = atoi(tmpStr);
                    memcpy(entry+value_loc+(col_index-1)*2,&tmp_value_uint16,2);
                    p+=offset+1;
                }
            }
            else
            {
                if(atoi(tmpStr)>0xffff)
                {
                    tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, input value %d is beyond 0xffff!\n",
                    table_id, table_info->col_info[index].col_name,atoi(tmpStr));
                    return BOOL_FALSE;
                }
                tmp_value_uint16 = atoi(tmpStr);                
                memcpy(entry+value_loc,&tmp_value_uint16,2);
            }                                
        }
        else if(DATA_TYPE_UINT32 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                col_index = 0;
                while(1)
                {
                    offset = midware_cli_findNextDot(p);
                    col_index++;

                    if(col_index>table_info->col_info[index].size)
                    {
                        tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d too many array!\n", 
                        table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                        return BOOL_FALSE;
                    }
                    if(','==*(p+offset) || '\0'==*(p+offset))
                    {
                        if(col_index<table_info->col_info[index].size)
                        {
                            tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d now only have %d!\n",
                            table_id, table_info->col_info[index].col_name,table_info->col_info[index].size,col_index);
                            return BOOL_FALSE;
                        }
                        else
                        {
                            memset(tmpStr,0,sizeof(tmpStr));
                            memcpy(tmpStr,p,offset);
                            tmp_value_uint32 = atoi(tmpStr);
                            printf("kenest %s %d %d",tmpStr,offset,tmp_value_uint32);
                            memcpy(entry+value_loc+(col_index-1)*4,&tmp_value_uint32,4);
                            break;
                        }
                    }
                    memset(tmpStr,0,sizeof(tmpStr));
                    memcpy(tmpStr,p,offset);
                    tmp_value_uint32 = atoi(tmpStr);
                    memcpy(entry+value_loc+(col_index-1)*4,&tmp_value_uint32,4);
                    p+=offset+1;
                }
            }
            else
            {
                tmp_value_uint32 = atoi(tmpStr);
                memcpy(entry+value_loc,&tmp_value_uint32,4);
            }                                
        }
        else if(DATA_TYPE_ARRAY == table_info->col_info[index].datatype)
        {
            memcpy(entry+value_loc,&tmpStr,col_size);
        }
        
        if('\0'==*(p+offset))
        {
            break;
        }
        p+=offset+1;
    }
    return BOOL_TRUE;
}
#endif
#if 0
bool_t midware_cli_insert_entry(const clish_shell_t *this,
              const lub_argv_t    *argv)

{        
    MIDWARE_TABLE_INFO *table_info = NULL;
    MIDWARE_TABLE_ID_E table_id = lub_argv__get_arg(argv,0);
    const UINT8 *arg = lub_argv__get_arg(argv,1);
    UINT8 * p = arg;
    UINT8 tmpStr[100];
    UINT32 offset = 0;
    UINT32 index = 0;
    UINT32 col_index = 0;

    UINT8 tmp_value_uint8=0;
    UINT16 tmp_value_uint16=0;
    UINT32 tmp_value_uint32=0;
    
    UINT32 value_loc = 0;
    UINT32 col_size = 0;
    UINT8 * entry;    

    UINT32 pri_key_bitmap = 0;
    UINT32 bitmap = 0;


    table_info = midware_get_table_info(table_id);

    if(NULL==table_info)
    {
        tinyrl_printf(this->tinyrl,"table id %d is invalid!\n", table_id);
        return BOOL_FALSE;
    }
    
    col_size = midware_sqlite3_get_entry_size(table_info);

	entry = (void *)malloc(entry_size);

	if(NULL==entry)
	{
	    tinyrl_printf(this->tinyrl,"There is no enough memory!\n");
        return BOOL_FALSE;
	}
	memset(entry,0,col_size);
		
    
    if((NULL != arg) && ('\0' != *arg))
    {        
        
        offset=midware_cli_findNextComma(p);
        if(0==offset)
        {
            tinyrl_printf(this->tinyrl,"There is no table id in arg %s!\n", arg);
            return BOOL_FALSE;
        }
        memset(tmpStr,0,sizeof(tmpStr));
        memcpy(tmpStr,p,offset);

        table_id = atoi(tmpStr);
        table_info = midware_get_table_info(table_id);

        if(NULL==table_info)
        {
            tinyrl_printf(this->tinyrl,"table id %d is invalid!\n", table_id);
            return BOOL_FALSE;
        }
        if('\0'==*(p+offset))
        {
            tinyrl_printf(this->tinyrl,"Please input fields!\n");
            return BOOL_FALSE;
        }

        
        col_size = midware_sqlite3_get_entry_size(table_info);

		entry = (void *)malloc(entry_size);

		if(NULL==entry)
		{
		    tinyrl_printf(this->tinyrl,"There is no enough memory!\n");
            return BOOL_FALSE;
		}
		memset(entry,0,col_size);
		
        p+=offset+1;        
        while(1)
        {
            offset=midware_cli_findNextComma(p);
            if(0==offset)
            {
                tinyrl_printf(this->tinyrl,"There is a 0 length field name in arg %s!\n", arg);
                free(entry);
                return BOOL_FALSE;
            }
        
            memset(tmpStr,0,sizeof(tmpStr));
            memcpy(tmpStr,p,offset);

            if('\0'==*(p+offset))
            {
                tinyrl_printf(this->tinyrl,"Please input field %s's value!\n", tmpStr);
                free(entry);
                return BOOL_FALSE;
            }
            else
            {
                value_loc = 0;
                for(index=0;index<table_info->col_num;index++)
                {
                    col_size = midware_sqlite3_get_col_size(&(table_info->col_info[index]));
                    if(0==strcmp(tmpStr,table_info->col_info[index].col_name))
                    {
                        bitmap |= (1 <<	index);
                        break;
                    }
                    else
                    {                        
                        value_loc = value_loc + col_size;
                    }
                }
                
                if(table_info->col_num==index)
                {
                    tinyrl_printf(this->tinyrl,"There is no  field %s in table %d!\n", tmpStr, table_info->table_id);
                    free(entry);
                    return BOOL_FALSE;
                }

                if(table_info->col_info[index].readonly)
                {
                    tinyrl_printf(this->tinyrl,"field %s in table %d is readonly!\n", tmpStr, table_info->table_id);
                    free(entry);
                    return BOOL_FALSE;
                }
            }

            p+=offset+1;
            offset=midware_cli_findNextComma(p);            
            if(0==offset)
            {
                tinyrl_printf(this->tinyrl,"There is a 0 length field value in arg %s!\n", arg);
                free(entry);
                return BOOL_FALSE;
            }
            
            memset(tmpStr,0,sizeof(tmpStr));
            memcpy(tmpStr,p,offset);

            if(DATA_TYPE_STRING == table_info->col_info[index].datatype)
            {
                strncpy(entry+value_loc,tmpStr,col_size-1);
            }
            else if(DATA_TYPE_UINT8 == table_info->col_info[index].datatype)
            {
                if(table_info->col_info[index].size>1)
                {
                    col_index = 0;
                    while(1)
                    {
                        offset = midware_cli_findNextDot(p);
                        col_index++;                                              

                        if(col_index>table_info->col_info[index].size)
                        {
                            tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d too many array!\n", 
                            table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                            return BOOL_FALSE;
                        }
                        if(','==*(p+offset) || '\0'==*(p+offset))
                        {
                            if(col_index<table_info->col_info[index].size)
                            {
                                tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d now only have %d!\n",
                                table_id, table_info->col_info[index].col_name,table_info->col_info[index].size,col_index);
                                return BOOL_FALSE;
                            }
                            else
                            {
                                memset(tmpStr,0,sizeof(tmpStr));
                                memcpy(tmpStr,p,offset);
                                tmp_value_uint8 = atoi(tmpStr);
                                memcpy(entry+value_loc+col_index-1,&tmp_value_uint8,1);
                                break;
                            }
                        }
                        memset(tmpStr,0,sizeof(tmpStr));
                        memcpy(tmpStr,p,offset);
                        tmp_value_uint8 = atoi(tmpStr);
                        memcpy(entry+value_loc+col_index-1,&tmp_value_uint8,1);
                        p+=offset+1;
                    }
                }
                else
                {
                    tmp_value_uint8 = atoi(tmpStr);
                    memcpy(entry+value_loc,&tmp_value_uint8,1);
                }                                
            }
            else if(DATA_TYPE_UINT16 == table_info->col_info[index].datatype)
            {
                if(table_info->col_info[index].size>1)
                {
                    col_index = 0;
                    while(1)
                    {
                        offset = midware_cli_findNextDot(p);
                        col_index++;                                              

                        if(col_index>table_info->col_info[index].size)
                        {
                            tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d too many array!\n", 
                            table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                            return BOOL_FALSE;
                        }
                        if(','==*(p+offset) || '\0'==*(p+offset))
                        {
                            if(col_index<table_info->col_info[index].size)
                            {
                                tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d now only have %d!\n",
                                table_id, table_info->col_info[index].col_name,table_info->col_info[index].size,col_index);
                                return BOOL_FALSE;
                            }
                            else
                            {
                                memset(tmpStr,0,sizeof(tmpStr));
                                memcpy(tmpStr,p,offset);
                                tmp_value_uint16 = atoi(tmpStr);
                                memcpy(entry+value_loc+(col_index-1)*2,&tmp_value_uint16,2);
                                break;
                            }
                        }
                        memset(tmpStr,0,sizeof(tmpStr));
                        memcpy(tmpStr,p,offset);
                        tmp_value_uint16 = atoi(tmpStr);
                        memcpy(entry+value_loc+(col_index-1)*2,&tmp_value_uint16,2);
                        p+=offset+1;
                    }
                }
                else
                {
                    tmp_value_uint16 = atoi(tmpStr);
                    memcpy(entry+value_loc,&tmp_value_uint16,2);
                }                                
            }
            else if(DATA_TYPE_UINT32 == table_info->col_info[index].datatype)
            {
                if(table_info->col_info[index].size>1)
                {
                    col_index = 0;
                    while(1)
                    {
                        offset = midware_cli_findNextDot(p);
                        col_index++;                                              

                        if(col_index>table_info->col_info[index].size)
                        {
                            tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d too many array!\n", 
                            table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                            return BOOL_FALSE;
                        }
                        if(','==*(p+offset) || '\0'==*(p+offset))
                        {
                            if(col_index<table_info->col_info[index].size)
                            {
                                tinyrl_printf(this->tinyrl,"table id %d, col_name %s input wrong, its size is %d now only have %d!\n",
                                table_id, table_info->col_info[index].col_name,table_info->col_info[index].size,col_index);
                                return BOOL_FALSE;
                            }
                            else
                            {
                                memset(tmpStr,0,sizeof(tmpStr));
                                memcpy(tmpStr,p,offset);
                                tmp_value_uint32 = atoi(tmpStr);
                                memcpy(entry+value_loc+(col_index-1)*4,&tmp_value_uint32,4);
                                break;
                            }
                        }
                        memset(tmpStr,0,sizeof(tmpStr));
                        memcpy(tmpStr,p,offset);
                        tmp_value_uint32 = atoi(tmpStr);
                        memcpy(entry+value_loc+(col_index-1)*4,&tmp_value_uint32,4);
                        p+=offset+1;
                    }
                }
                else
                {
                    tmp_value_uint32 = atoi(tmpStr);
                    memcpy(entry+value_loc,&tmp_value_uint32,4);
                }                                
            }
            else if(DATA_TYPE_ARRAY == table_info->col_info[index].datatype)
            {
                memcpy(entry+value_loc,&tmpStr,col_size);
            }
            
            if('\0'==*(p+offset))
            {
                break;
            }
            p+=offset+1;
        }
    }
    else
    {
        tinyrl_printf(this->tinyrl,"No input arg!\n");
        return BOOL_FALSE;               
    }
    
    pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
    if((bitmap&pri_key_bitmap)!=pri_key_bitmap)
    {
        tinyrl_printf(this->tinyrl,"You must input all the primary key field!\n");
        free(entry);
        return BOOL_FALSE;
    }
    
        
    if(ONU_OK!=midware_insert_entry(table_id,entry))
    {
        tinyrl_printf(this->tinyrl,"midware_insert_entry() failed!\n");
        free(entry);
        return FALSE;  
    }
    free(entry);
    return BOOL_TRUE;    
}
#endif

bool_t midware_cli_print_table_info(const clish_shell_t *this, const lub_argv_t *argv)
{
    MIDWARE_TABLE_INFO *table_info = NULL;
    MIDWARE_TABLE_ID_E table_id = 0;
    UINT32  index = 0;

    table_id = atoi(lub_argv__get_arg(argv,0));
    table_info = midware_get_table_info(table_id);

    if(NULL!=table_info)
    {
        tinyrl_printf(this->tinyrl,"table id %d, name %s, col_num %d, table entry size %d:\n", table_info->table_id, table_info->table_name, table_info->col_num, midware_sqlite3_get_entry_size(table_info));
        tinyrl_printf(this->tinyrl,"                col_name    type size readonly primary_key save_to_flash\n");
        for(index=0;index<table_info->col_num;index++)
        {
            /*tinyrl_printf(this->tinyrl,"col name %s type %s size %d readonly %s primary_key %s save_to_flash %s\n", table_info->col_info[index].col_name,
            midware_cli_dataTypeStr[table_info->col_info[index].datatype], table_info->col_info[index].size, 
            midware_cli_boolStr[table_info->col_info[index].readonly], midware_cli_boolStr[table_info->col_info[index].primary_key],
            midware_cli_boolStr[table_info->col_info[index].save_to_flash]);*/
            tinyrl_printf(this->tinyrl,"%24s  %6s %4d %8s %11s %13s\n", table_info->col_info[index].col_name,
            midware_cli_dataTypeStr[table_info->col_info[index].datatype], table_info->col_info[index].size, 
            midware_cli_boolStr[table_info->col_info[index].readonly], midware_cli_boolStr[table_info->col_info[index].primary_key],
            midware_cli_boolStr[table_info->col_info[index].save_to_flash]);
        }
    }
    else
    {
        tinyrl_printf(this->tinyrl,"table id %d is invalid!\n", table_id);
        return BOOL_FALSE;
    }
    
    return BOOL_TRUE;
}

bool_t midware_cli_insert_entry(const clish_shell_t *this, const lub_argv_t *argv)

{        
    MIDWARE_TABLE_INFO *table_info = NULL;
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const UINT8 *arg = lub_argv__get_arg(argv,1);
    UINT32 entry_size = 0;
    UINT8 * entry = NULL;    
    UINT32 pri_key_bitmap = 0;
    UINT32 bitmap = 0;
    bool_t rc = BOOL_FALSE;
    
    table_info = midware_get_table_info(table_id);    
    /*
    tinyrl_printf(this->tinyrl,"test\n");
    tinyrl_printf(this->tinyrl,"arg1 %d arg2 %s\n",table_id,arg);
    */
    if(NULL==table_info)
    {
        tinyrl_printf(this->tinyrl,"table id %d is invalid!\n", table_id);
        return BOOL_FALSE;
    }
    
    entry_size = midware_sqlite3_get_entry_size(table_info);

	entry = (void *)malloc(entry_size);

	if(NULL==entry)
	{
	    tinyrl_printf(this->tinyrl,"There is no enough memory!\n");
        return BOOL_FALSE;
	}
	memset(entry,0,entry_size);
	
	pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
		    
    if((NULL != arg) && ('\0' != *arg))
    {        

        rc = midware_cli_getFieldValueArray(this,entry,arg,table_info,&bitmap);
        if(BOOL_TRUE!=rc)
        {
            return BOOL_FALSE;
        }                      
    }
    else
    {
        if(0!=pri_key_bitmap)
        {
            tinyrl_printf(this->tinyrl,"This table has index, must input primary key fields!\n");
            return BOOL_FALSE;
        }
    }
        
    
    if((bitmap&pri_key_bitmap)!=pri_key_bitmap)
    {
        tinyrl_printf(this->tinyrl,"You must input all the primary key field!\n");
        free(entry);
        return BOOL_FALSE;
    }
            
    if(ONU_OK!=midware_sqlite3_insert_entry(table_id,entry))
    {
        tinyrl_printf(this->tinyrl,"midware_insert_entry() failed!\n");
        free(entry);
        return FALSE;  
    }
    free(entry);
    return BOOL_TRUE;    
}

bool_t midware_cli_update_entry(const clish_shell_t *this, const lub_argv_t *argv)
{        
    MIDWARE_TABLE_INFO *table_info = NULL;
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const UINT8 *arg = lub_argv__get_arg(argv,1);
    UINT32 entry_size = 0;
    UINT8 * entry = NULL;    
    UINT32 pri_key_bitmap = 0;
    UINT32 bitmap = 0;
    bool_t rc = BOOL_FALSE;

    table_info = midware_get_table_info(table_id);

    if(NULL==table_info)
    {
        tinyrl_printf(this->tinyrl,"table id %d is invalid!\n", table_id);
        return BOOL_FALSE;
    }
    
    entry_size = midware_sqlite3_get_entry_size(table_info);

	entry = (void *)malloc(entry_size);

	if(NULL==entry)
	{
	    tinyrl_printf(this->tinyrl,"There is no enough memory!\n");
        return BOOL_FALSE;
	}
	memset(entry,0,entry_size);

	pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
		    
    if((NULL != arg) && ('\0' != *arg))
    {        

        rc = midware_cli_getFieldValueArray(this,entry,arg,table_info,&bitmap);
        if(BOOL_TRUE!=rc)
        {
            return BOOL_FALSE;
        }                
    }
    else
    {
        if(0!=pri_key_bitmap)
        {
            tinyrl_printf(this->tinyrl,"This table has index, must input primary key fields!\n");
            return BOOL_FALSE;
        }
    }        
    
    if((bitmap&pri_key_bitmap)!=pri_key_bitmap)
    {
        tinyrl_printf(this->tinyrl,"You must input all the primary key field!\n");
        free(entry);
        return BOOL_FALSE;
    }
            
    if(ONU_OK!=midware_sqlite3_update_entry(table_id,bitmap,entry))
    {
        tinyrl_printf(this->tinyrl,"midware_update_entry() failed!\n");
        free(entry);
        return FALSE;  
    }
    free(entry);
    return BOOL_TRUE;    
}

bool_t midware_cli_remove_entry(const clish_shell_t *this, const lub_argv_t *argv)
{        
    MIDWARE_TABLE_INFO *table_info = NULL;
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const UINT8 *arg = lub_argv__get_arg(argv,1);
    UINT32 entry_size = 0;
    UINT8 * entry = NULL;    
    UINT32 pri_key_bitmap = 0;
    UINT32 bitmap = 0;
    bool_t rc = BOOL_FALSE;

    table_info = midware_get_table_info(table_id);

    if(NULL==table_info)
    {
        tinyrl_printf(this->tinyrl,"table id %d is invalid!\n", table_id);
        return BOOL_FALSE;
    }
    
    entry_size = midware_sqlite3_get_entry_size(table_info);

	entry = (void *)malloc(entry_size);

	if(NULL==entry)
	{
	    tinyrl_printf(this->tinyrl,"There is no enough memory!\n");
        return BOOL_FALSE;
	}
	memset(entry,0,entry_size);

	pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
		    
    if((NULL != arg) && ('\0' != *arg))
    {        

        rc = midware_cli_getFieldValueArray(this,entry,arg,table_info,&bitmap);
        if(BOOL_TRUE!=rc)
        {
            return BOOL_FALSE;
        }                        
    }
    else
    {
        if(0!=pri_key_bitmap)
        {
            tinyrl_printf(this->tinyrl,"This table has index, must input primary key fields!\n");
            return BOOL_FALSE;
        }
    }    
    
    if(bitmap!=pri_key_bitmap)
    {
        tinyrl_printf(this->tinyrl,"You must input all the primary key fields as index but not other fields!\n");
        free(entry);
        return BOOL_FALSE;
    }
            
    if(ONU_OK!=midware_sqlite3_remove_entry(table_id,entry))
    {
        tinyrl_printf(this->tinyrl,"midware_remove_entry() failed!\n");
        free(entry);
        return FALSE;  
    }
    free(entry);
    return BOOL_TRUE;    
}


bool_t midware_cli_reset_table(const clish_shell_t *this, const lub_argv_t *argv)
{
    const UINT8 *arg = lub_argv__get_arg(argv,0);

    MIDWARE_TABLE_ID_E table_id = 0;    
                
    if((NULL != arg) && ('\0' != *arg))
    {
        table_id = atoi(arg);
    }
    else
    {
        tinyrl_printf(this->tinyrl,"You must input the table which you want to reset!\n");
        return BOOL_FALSE;               
    }

    if(ONU_OK!=midware_sqlite3_reset_table(table_id))
    {
        tinyrl_printf(this->tinyrl,"midware_reset_table() failed!\n");
        return BOOL_FALSE;  
    }
    return BOOL_TRUE;    
}

bool_t midware_cli_reset_table_data(const clish_shell_t *this, const lub_argv_t *argv)
{
    const UINT8 *arg = lub_argv__get_arg(argv,0);

    MIDWARE_TABLE_ID_E table_id = 0;    
                
    if((NULL != arg) && ('\0' != *arg))
    {
        table_id = atoi(arg);
    }
    else
    {
        tinyrl_printf(this->tinyrl,"You must input the table data which you want to reset!\n");
        return BOOL_FALSE;               
    }

    if(ONU_OK!=midware_sqlite3_reset_table_data(table_id))
    {
        tinyrl_printf(this->tinyrl,"midware_reset_table() failed!\n");
        return BOOL_FALSE;  
    }
    return BOOL_TRUE;    
}


#define SCREEN_LINE_LENGTH 100

static UINT32 midware_get_col_print_length(MIDWARE_COL_INFO *col_info)
{
    UINT32 col_size = 0;
    UINT32 colname_size = strlen(col_info->col_name);

    switch(col_info->datatype)
    {
        case DATA_TYPE_STRING:
        {
       
            col_size = 1*col_info->size;
       
            break;
        }
        
        case DATA_TYPE_UINT8:
        {
            if(col_info->size>1)
            {
                col_size = 2*col_info->size;
            }
            else
            {
                col_size = 3; /*if there is negative number, col_size shoud be 4, e.g., -255*/
            }
            break;
        }
        case DATA_TYPE_UINT16:
        {
            if(col_info->size>1)
            {
                col_size = 4*col_info->size;
            }
            else
            {
                col_size = 5; /*if there is negative number, col_size shoud be 4, e.g., -255*/
            }
            break;
        }
        case DATA_TYPE_UINT32:
        {
            if(col_info->size>1)
            {
                col_size = 8*col_info->size;
            }
            else
            {
                col_size = 10; /*if there is negative number, col_size shoud be 4, e.g., -255*/
            }
            break;
        }
        default:
        {
            col_size = 1*col_info->size;
        }
    }

    if(col_size>colname_size)
    {
        return col_size;
    }
    else
    {
        return colname_size;
    }
}

static void midware_cli_print_col(const clish_shell_t *this, MIDWARE_TABLE_INFO *table_info,UINT32 colIndex,UINT8 *entry)
{
    UINT32 index = 0;
    UINT32 value_loc = 0;
    UINT32 col_size = 0;
    UINT32 col_print_len = 0;
    UINT32 index_in_col;
    
    if(NULL==table_info || NULL==entry)
    {
        tinyrl_printf(this->tinyrl,"Input pointer NULL!\n");
        return;
    }
    
    if(colIndex>=table_info->col_num)
    {
        tinyrl_printf(this->tinyrl,"colIndex %d should be smaller than col_num %d!\n",colIndex,table_info->col_num);
        return;
    }

    col_print_len = midware_get_col_print_length(&(table_info->col_info[colIndex]));
    
    
    for(index=0;index<colIndex;index++)
    {
        col_size = midware_sqlite3_get_col_size(&(table_info->col_info[index]));
        value_loc += col_size;
    }
    

    if(DATA_TYPE_STRING == table_info->col_info[colIndex].datatype)
    {
        if(col_print_len>=SCREEN_LINE_LENGTH)
        {
            tinyrl_printf(this->tinyrl,"%s\n",(char *)entry+value_loc);
        }
        else
        {
            tinyrl_printf(this->tinyrl,"%-*s ",col_print_len,(char *)entry+value_loc);
        }
    }
    
    else if(DATA_TYPE_UINT8 == table_info->col_info[colIndex].datatype)
    {
        if(table_info->col_info[colIndex].size>1)
        {
            for(index_in_col = 0;index_in_col<table_info->col_info[colIndex].size;index_in_col++)
            {
                tinyrl_printf(this->tinyrl,"%.2x", *(UINT8 *)(entry+value_loc+index_in_col));
            }
            if(col_print_len>=SCREEN_LINE_LENGTH)
            {
                tinyrl_printf(this->tinyrl,"\n",(char *)entry+value_loc);
            }
            else
            {
                tinyrl_printf(this->tinyrl,"%*c",col_print_len+1-2*table_info->col_info[colIndex].size,' ');
            }
        }
        else
        {
            tinyrl_printf(this->tinyrl,"%-*d ",col_print_len,*(UINT8 *)(entry+value_loc));
        }
    }
    else if(DATA_TYPE_UINT16 == table_info->col_info[colIndex].datatype)
    {
        if(table_info->col_info[colIndex].size>1)
        {
            for(index_in_col = 0;index_in_col<table_info->col_info[colIndex].size;index_in_col++)
            {
                tinyrl_printf(this->tinyrl,"%.4x", *(UINT16 *)(entry+value_loc+index_in_col*2));
            }
            if(col_print_len>=SCREEN_LINE_LENGTH)
            {
                tinyrl_printf(this->tinyrl,"\n",(char *)entry+value_loc);
            }
            else
            {
                tinyrl_printf(this->tinyrl,"%*c",col_print_len+1-4*table_info->col_info[colIndex].size,' ');
            }
        }
        else
        {
            tinyrl_printf(this->tinyrl,"%-*d ",col_print_len, *(UINT16 *)(entry+value_loc));
        }
    }
    else if(DATA_TYPE_UINT32 == table_info->col_info[colIndex].datatype)
    {
        if(table_info->col_info[colIndex].size>1)
        {
            for(index_in_col = 0;index_in_col<table_info->col_info[colIndex].size;index_in_col++)
            {
                tinyrl_printf(this->tinyrl,"%.8x", *(UINT32 *)(entry+value_loc+index_in_col*4));
            }
            if(col_print_len>=SCREEN_LINE_LENGTH)
            {
                tinyrl_printf(this->tinyrl,"\n",(char *)entry+value_loc);
            }
            else
            {
                tinyrl_printf(this->tinyrl,"%*c",col_print_len+1-8*table_info->col_info[colIndex].size,' ');
            }
        }
        else
        {
            tinyrl_printf(this->tinyrl,"%-*d ",col_print_len, *(UINT32 *)(entry+value_loc));
        }
    }
    else
    {
        tinyrl_printf(this->tinyrl,"unknown col name %s type: %d\n", table_info->col_info[colIndex].col_name,table_info->col_info[colIndex].datatype);
    }
    return;
}

static void midware_cli_print_one_entry(const clish_shell_t *this, MIDWARE_TABLE_INFO *table_info, UINT8 *entry)
{
    UINT32 index = 0;
    UINT32 col_size = 0;
    UINT32 colIndex = 0;
    UINT32 col_print_len = 0;
    UINT32 line_print_len = 0;
    UINT32 start_col = 0;
    UINT32 end_col = 0;  
    UINT32 symbol_no = 0;
    bool_t flag_too_long = BOOL_FALSE;

    if(NULL==table_info || NULL==entry)
    {
        tinyrl_printf(this->tinyrl,"Input pointer NULL!\n");
        return;
    }
    while(index<table_info->col_num)
    {
        col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
        line_print_len += col_print_len+1;
        if(line_print_len>SCREEN_LINE_LENGTH || (table_info->col_num-1)==index)
        {
            if(line_print_len>SCREEN_LINE_LENGTH)
            {
                line_print_len = SCREEN_LINE_LENGTH;
                flag_too_long = BOOL_TRUE;
            }
            else /*if((table_info->col_num-1)==index)*/
            {
                index++;
            }
            
            end_col = index;
            for(index=start_col;index<end_col;index++)
            {
                col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
                tinyrl_printf(this->tinyrl,"%-*s ",col_print_len,table_info->col_info[index].col_name);
            }
            tinyrl_printf(this->tinyrl,"\n");
            for(symbol_no=0;symbol_no<line_print_len;symbol_no++)
            {
                tinyrl_printf(this->tinyrl,"-");
            }
            tinyrl_printf(this->tinyrl,"\n");
            
            for(index=start_col;index<end_col;index++)
            {
                midware_cli_print_col(this,table_info,index,entry);                
            }            
            tinyrl_printf(this->tinyrl,"\n");
            if(flag_too_long)
            {
                tinyrl_printf(this->tinyrl,"\n");
            }

            if(table_info->col_num==index)
            {
                return;
            }
            /*judge whether current index col len is larger that SCREEN_LINE_LENGTH*/
            col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
            if(col_print_len>=SCREEN_LINE_LENGTH)
            {                
                tinyrl_printf(this->tinyrl,"%s\n",col_print_len,table_info->col_info[index].col_name);
                for(symbol_no=0;symbol_no<SCREEN_LINE_LENGTH;symbol_no++)
                {
                    tinyrl_printf(this->tinyrl,"-");
                }
                tinyrl_printf(this->tinyrl,"\n");
                
                midware_cli_print_col(this,table_info,index,entry);
                tinyrl_printf(this->tinyrl,"\n");
                index++;
            }
            line_print_len = 0;
            start_col = index;
            continue;
        }
        index++;
    }
    return;
}

static bool_t midware_cli_print_all_entry(const clish_shell_t *this, MIDWARE_TABLE_INFO *table_info, UINT8 *entry)
{
    UINT32 index = 0;
    UINT32 col_size = 0;
    UINT32 colIndex = 0;
    UINT32 col_print_len = 0;
    UINT32 line_print_len = 0;
    UINT32 start_col = 0;
    UINT32 end_col = 0;
    bool_t rc = BOOL_FALSE;
    UINT32 entry_num = 0;
    UINT32 symbol_no = 0;
    bool_t flag_too_long = BOOL_FALSE;

    if(NULL==table_info || NULL==entry)
    {
        tinyrl_printf(this->tinyrl,"Input pointer NULL!\n");
        return BOOL_FALSE;
    }

    while(index<table_info->col_num)
    {
        col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
        line_print_len += col_print_len+1;
        
        if(line_print_len>SCREEN_LINE_LENGTH || (table_info->col_num-1)==index)
        {
            if(line_print_len>SCREEN_LINE_LENGTH)
            {
                line_print_len = SCREEN_LINE_LENGTH;
                flag_too_long = BOOL_TRUE;
            }
            else /*if((table_info->col_num-1)==index)*/
            {
                index++;
            }            
            
            end_col = index;

            /*check whether there is any entries*/
            rc = midware_sqlite3_get_first_entry(table_info->table_id, 0xffffffff,entry);	
            if( ONU_FAIL == rc )
            {
                tinyrl_printf(this->tinyrl,"Table %s has no entry!\n",table_info->table_name);
                return BOOL_TRUE;
            }

            /*print previous cols whose total print length is smaller than SCREEN_LINE_LENGTH*/
            for(index=start_col;index<end_col;index++)
            {
                col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
                tinyrl_printf(this->tinyrl,"%-*s ",col_print_len,table_info->col_info[index].col_name);
            }            
            tinyrl_printf(this->tinyrl,"\n");
            for(symbol_no=0;symbol_no<line_print_len;symbol_no++)
            {
                tinyrl_printf(this->tinyrl,"-");
            }
            tinyrl_printf(this->tinyrl,"\n");
            do    
            {
                entry_num++;
                for(index=start_col;index<end_col;index++)
                {
                    midware_cli_print_col(this,table_info,index,entry);                    
                }            
                tinyrl_printf(this->tinyrl,"\n");
                rc = midware_sqlite3_get_next_entry(table_info->table_id, entry);        
                //print_test(cur_loc);
            }while( ONU_FAIL != rc );

            if(flag_too_long)
            {
                tinyrl_printf(this->tinyrl,"\n");
                flag_too_long = BOOL_FALSE;
            }
            
            if(table_info->col_num==index)
            {
                return BOOL_TRUE;
            }
            
            /*judge whether current index col len is larger that SCREEN_LINE_LENGTH*/
            col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
            if(col_print_len>=SCREEN_LINE_LENGTH)
            {
                tinyrl_printf(this->tinyrl,"%s\n",col_print_len,table_info->col_info[index].col_name);
                
                for(symbol_no=0;symbol_no<SCREEN_LINE_LENGTH;symbol_no++)
                {
                    tinyrl_printf(this->tinyrl,"-");
                }
                tinyrl_printf(this->tinyrl,"\n");
                rc = midware_sqlite3_get_first_entry(table_info->table_id, 0xffffffff,entry);	
                if( ONU_FAIL == rc )
                {
                    /*this branch will not be entered*/
                    tinyrl_printf(this->tinyrl,"Table %s has no entry!\n",table_info->table_name);
                    return BOOL_TRUE;
                }            
                do    
                {
                    entry_num++;
                    for(index=start_col;index<end_col;index++)
                    {
                        midware_cli_print_col(this,table_info,index,entry);                    
                    }            
                    tinyrl_printf(this->tinyrl,"\n");
                    rc = midware_sqlite3_get_next_entry(table_info->table_id, entry);        
                    //print_test(cur_loc);
                }while( ONU_FAIL != rc );
                tinyrl_printf(this->tinyrl,"\n");
                index++;
            }
            line_print_len = 0;
            start_col = index;
            continue;
        }
        index++;
    }
    return BOOL_TRUE;
}

static void midware_cli_print_entry(const clish_shell_t *this, MIDWARE_TABLE_INFO *table_info, UINT8 *entry)
{
    UINT32 index = 0;
    UINT32 value_loc = 0;
    UINT32 col_size = 0;
    UINT32 colIndex = 0;

    if(NULL==table_info || NULL==entry)
    {
        tinyrl_printf(this->tinyrl,"Input pointer NULL!\n");
        return;
    }
  
    for(index = 0; index < table_info->col_num; index++)
    {
        tinyrl_printf(this->tinyrl,"%s: ", table_info->col_info[index].col_name);
        col_size = midware_sqlite3_get_col_size(&(table_info->col_info[index]));
        
        if(DATA_TYPE_STRING == table_info->col_info[index].datatype)
        {
            tinyrl_printf(this->tinyrl,"%s,\n", (char *)entry+value_loc);
        }
        else if(DATA_TYPE_UINT8 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                for(colIndex = 0;colIndex<table_info->col_info[index].size;colIndex++)
                {
                    tinyrl_printf(this->tinyrl,"%d,", *(UINT8 *)(entry+value_loc+colIndex));
                }
                tinyrl_printf(this->tinyrl,"\n");
            }
            else
            {
                tinyrl_printf(this->tinyrl,"%d,\n", *(UINT8 *)(entry+value_loc));
            }
        }
        else if(DATA_TYPE_UINT16 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                for(colIndex = 0;colIndex<table_info->col_info[index].size;colIndex++)
                {
                    tinyrl_printf(this->tinyrl,"%d,", *(UINT16 *)(entry+value_loc+colIndex*2));
                }
                tinyrl_printf(this->tinyrl,"\n");
            }
            else
            {
                tinyrl_printf(this->tinyrl,"%d,\n", *(UINT16 *)(entry+value_loc));
            }
        }
        else if(DATA_TYPE_UINT32 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                for(colIndex = 0;colIndex<table_info->col_info[index].size;colIndex++)
                {
                    tinyrl_printf(this->tinyrl,"%d,", *(UINT32 *)(entry+value_loc+colIndex*4));
                }
                tinyrl_printf(this->tinyrl,"\n");
            }
            else
            {
                tinyrl_printf(this->tinyrl,"%d,\n", *(UINT32 *)(entry+value_loc));
            }
        }
        else
        {
            tinyrl_printf(this->tinyrl,"unknown col name %s type: %d\n", table_info->col_info[index].col_name,table_info->col_info[index].datatype);
        }
        
        value_loc+=col_size;
    }
}

bool_t midware_cli_get_entry(const clish_shell_t *this, const lub_argv_t *argv)
{        
    MIDWARE_TABLE_INFO *table_info = NULL;
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const UINT8 *arg = lub_argv__get_arg(argv,1);
    UINT32 entry_size = 0;
    UINT8 * entry = NULL;    
    UINT32 pri_key_bitmap = 0;
    UINT32 bitmap = 0;
    bool_t rc = BOOL_FALSE;    

    
    table_info = midware_get_table_info(table_id);

    if(NULL==table_info)
    {
        tinyrl_printf(this->tinyrl,"table id %d is invalid!\n", table_id);
        return BOOL_FALSE;
    }
    
    entry_size = midware_sqlite3_get_entry_size(table_info);

	entry = (void *)malloc(entry_size);

	if(NULL==entry)
	{
	    tinyrl_printf(this->tinyrl,"There is no enough memory!\n");
        return BOOL_FALSE;
	}
	memset(entry,0,entry_size);

	pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
		    
    if((NULL != arg) && ('\0' != *arg))
    {        

        rc = midware_cli_getFieldValueArray(this,entry,arg,table_info,&bitmap);
        if(BOOL_TRUE!=rc)
        {
            return BOOL_FALSE;
        }
    }
    else
    {
        if(0!=pri_key_bitmap)
        {
            tinyrl_printf(this->tinyrl,"This table has index, must input primary key fields!\n");
            return BOOL_FALSE;
        }
    }        
    if(bitmap!=pri_key_bitmap)
    {
        tinyrl_printf(this->tinyrl,"You must input all the primary key fields as index but not other fields!\n");
        free(entry);
        return BOOL_FALSE;
    }
    
    if(ONU_OK!=midware_sqlite3_get_entry(table_id,0xffffffff,entry))
    {
        tinyrl_printf(this->tinyrl,"midware_get_entry() failed!\n");
        free(entry);
        return FALSE;  
    }

    midware_cli_print_one_entry(this,table_info,entry);
    
    free(entry);
    return BOOL_TRUE;     
}

bool_t midware_cli_show_table_all_entry(const clish_shell_t *this, const lub_argv_t *argv)
{
    MIDWARE_TABLE_INFO *table_info = NULL;
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    UINT32 entry_size = 0;
    UINT32 value_loc = 0;
    UINT32 col_size = 0;
    UINT32 colIndex = 0;
    UINT8 * entry = NULL;    
    UINT32 pri_key_bitmap = 0;
    UINT32 bitmap = 0;
    bool_t rc = BOOL_FALSE;
    UINT32 index = 0;
    UINT32 entry_num = 0;

    table_info = midware_get_table_info(table_id);

    if(NULL==table_info)
    {
        tinyrl_printf(this->tinyrl,"table id %d is invalid!\n", table_id);
        return BOOL_FALSE;
    }
    
    entry_size = midware_sqlite3_get_entry_size(table_info);

	entry = (void *)malloc(entry_size);

	if(NULL==entry)
	{
	    tinyrl_printf(this->tinyrl,"There is no enough memory!\n");
        return BOOL_FALSE;
	}
	
	memset(entry,0,entry_size);

    #if 0
	rc = midware_sqlite3_get_first_entry(table_id, 0xffffffff,entry);
	
    if( ONU_FAIL == rc )
    {
        tinyrl_printf(this->tinyrl,"Table %s has no entry!\n",table_info->table_name);
        return BOOL_FALSE;
    }
    
    do    
    {
        entry_num++;
        tinyrl_printf(this->tinyrl,"Entry No %d:\n",entry_num);
        midware_cli_print_entry(this,table_info,entry);

        rc = midware_sqlite3_get_next_entry(table_id, entry);        
        //print_test(cur_loc);
    }while( ONU_FAIL != rc );
    #endif

    rc = midware_cli_print_all_entry(this,table_info,entry);
    free(entry);
    return rc;     
}

bool_t midware_cli_set_print_level(const clish_shell_t *this, const lub_argv_t *argv)
{
    UINT32 print_level = atoi(lub_argv__get_arg(argv,0));
    
    if(0==print_level)
    {
        midware_glob_trace = 0;
    }
    else if(1==print_level)
    {
        midware_glob_trace = MIDWARE_TRACE_LEVEL_FATAL;
    }
    else if(2==print_level)
    {
        midware_glob_trace = MIDWARE_TRACE_LEVEL_ERROR;
    }
    else if(3==print_level)
    {
        midware_glob_trace = MIDWARE_TRACE_LEVEL_WARN;
    }
    else if(4==print_level)
    {
        midware_glob_trace = MIDWARE_TRACE_LEVEL_INFO;
    }
    else if(5==print_level)
    {
        midware_glob_trace = MIDWARE_TRACE_LEVEL_DEBUG;
    }
    else if(6==print_level)
    {
        midware_glob_trace = MIDWARE_ALL_TRACE_LEVEL;
    }
    else
    {
        tinyrl_printf(this->tinyrl,"Print level %d is outof range[0..6]!\n",print_level);
        return BOOL_FALSE;
    }    
    return BOOL_TRUE;
}
