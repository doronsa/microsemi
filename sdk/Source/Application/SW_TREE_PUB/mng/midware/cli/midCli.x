/*Marvell middle-ware table ID*/
enum MIDWARE_TABLE_CLI_ID_E
{
    MIDWARE_TABLE_CLI_START           = 0,
    MIDWARE_TABLE_CLI_ONU_INFO        = 0,
    MIDWARE_TABLE_CLI_ONU_CFG         = 1,
    MIDWARE_TABLE_CLI_ONU_IP          = 2,    
    MIDWARE_TABLE_CLI_UNI_CFG         = 3,
    MIDWARE_TABLE_CLI_UNI_QOS         = 4,
    MIDWARE_TABLE_CLI_VLAN            = 5,    
    MIDWARE_TABLE_CLI_FLOW            = 6,
    MIDWARE_TABLE_CLI_FLOW_MOD_VLAN   = 7,
    MIDWARE_TABLE_CLI_FLOW_MOD_MAC    = 8,
    MIDWARE_TABLE_CLI_FLOW_MOD_PPPOE  = 9,
    MIDWARE_TABLE_CLI_FLOW_MOD_IPV4   = 10,
    MIDWARE_TABLE_CLI_FLOW_MOD_IPV6   = 11,
    MIDWARE_TABLE_CLI_FLOW_KEY_L2     = 12,
    MIDWARE_TABLE_CLI_FLOW_KEY_IPV4   = 13,
    MIDWARE_TABLE_CLI_FLOW_KEY_IPV6   = 14,
    MIDWARE_TABLE_CLI_MC_CFG          = 15,
    MIDWARE_TABLE_CLI_MC_PORT         = 16,
    MIDWARE_TABLE_CLI_MC_VLAN_TRANS   = 17,
    MIDWARE_TABLE_CLI_MC_LEARN        = 18,
    MIDWARE_TABLE_CLI_MC_STREAM       = 19,    
    MIDWARE_TABLE_CLI_WAN_QOS         = 20,
    MIDWARE_TABLE_CLI_EPON            = 21,
    MIDWARE_TABLE_CLI_DBA             = 22,  
    MIDWARE_TABLE_CLI_SW_IMAGE        = 23,
    MIDWARE_TABLE_CLI_OPT_TRANSCEIVER = 24,
    MIDWARE_TABLE_CLI_ALARM           = 25,
    MIDWARE_TABLE_CLI_ALARM_EVENT     = 26,    
    MIDWARE_TABLE_CLI_EPON_AUTH       = 27,
    MIDWARE_TABLE_CLI_IAD_PORT_ADMIN  = 28,    
    MIDWARE_TABLE_CLI_IAD_INFO        = 29,  
    MIDWARE_TABLE_CLI_IAD_CFG         = 30, 
    MIDWARE_TABLE_CLI_H248_CFG        = 31,  
    MIDWARE_TABLE_CLI_H248_USER_INFO  = 32, 
    MIDWARE_TABLE_CLI_H248_RTP_CFG    = 33,         
    MIDWARE_TABLE_CLI_H248_RTP_INFO   = 34,
    MIDWARE_TABLE_CLI_SIP_CFG         = 35,    
    MIDWARE_TABLE_CLI_SIP_USER_INFO   = 36,    
    MIDWARE_TABLE_CLI_FAX_CFG         = 37,  
    MIDWARE_TABLE_CLI_IAD_STATUS      = 38,      
    MIDWARE_TABLE_CLI_POTS_STATUS     = 39,   
    MIDWARE_TABLE_CLI_IAD_OPERATION   = 40, 
    MIDWARE_TABLE_CLI_SIP_DIGITMAP    = 41, 
    MIDWARE_TABLE_CLI_RSTP            = 42,    
    MIDWARE_TABLE_CLI_RSTP_PORT       = 43, 
    MIDWARE_TABLE_CLI_SW_MAC          = 44,
    MIDWARE_TABLE_CLI_END             = 44
};

program MidCli {
    version MIDVERS {
        bool  Midware_cli_print_table_info(MIDWARE_TABLE_CLI_ID_E)    =  1;
        bool  Midware_cli_insert_entry(MIDWARE_TABLE_CLI_ID_E, string)=  2;
        bool  Midware_cli_update_entry(MIDWARE_TABLE_CLI_ID_E, string)=  3;
        bool  Midware_cli_remove_entry(MIDWARE_TABLE_CLI_ID_E, string)=  4;
        bool  Midware_cli_reset_table(MIDWARE_TABLE_CLI_ID_E)         =  5;
        bool  Midware_cli_get_entry(MIDWARE_TABLE_CLI_ID_E, string)   =  6;
        bool  Midware_cli_show_table_all_entry(MIDWARE_TABLE_CLI_ID_E)=  7;
        bool  Midware_cli_set_print_level(u_int)                      =  8;     
    }=1;
}=88;
