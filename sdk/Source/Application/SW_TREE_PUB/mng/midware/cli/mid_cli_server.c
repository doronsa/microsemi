/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Middleware module                                         **/
/**                                                                          **/
/**  FILE        : midCliServer.c                                            **/
/**                                                                          **/
/**  DESCRIPTION : Definition of middleware server module                    **/
/**                                                                          **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *      Victor initial version created.   12/Jan/2011          
 *                                                                              
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <memory.h>
#include <malloc.h>

#include "globals.h"
#include "sqlite3.h"

//#include "midCli.h"
//#include "midApi.h"
#include "midware_expo.h"
#include "midware_sql_translate.h"

/*----------------------------------------------------------------------------*/
/* External data declaration                                                  */
/*----------------------------------------------------------------------------*/
#define  POS_PACKED      __attribute__ ((__packed__))
#define  VOID void

#define BOOL_TRUE  1
#define BOOL_FALSE 0

extern UINT32 midware_glob_trace;

extern MIDWARE_TABLE_INFO *midware_get_table_info(MIDWARE_TABLE_ID_E table_id);
extern lub_string_nocasecmp(const char *cs, const char *ct);

#define MID_CLI_PRINT(...) mipc_printf(name, ##__VA_ARGS__)
//#define MID_CLI_PRINT printf

#define SCREEN_LINE_LENGTH 100

/*----------------------------------------------------------------------------*/
/* Global static data definition                                              */
/*----------------------------------------------------------------------------*/
static const char *midware_cli_dataTypeStr[] =
{
    "UINT8",
	"UINT16",
	"UINT32",
	"ARRAY",
	"STRING"
};

static const char *midware_cli_boolStr[] =
{
    "NO",
	"YES"
};

/*----------------------------------------------------------------------------*/
/* Function implementation                                                    */
/*----------------------------------------------------------------------------*/
static UINT32 midware_cli_findNextComma(const char *p)
{
    UINT32 i = 0;
    while(','!=*p && '\0'!=*p)
    {
        p++;
        i++;
    }
    return i;
}

static UINT32 midware_cli_findNextEqualSymbol(const char *p)
{
    UINT32 i = 0;
    while('='!=*p && ','!=*p && '\0'!=*p)
    {
        p++;
        i++;
    }
    return i;
}

static bool midware_cli_is_hex_num(const char *x, UINT32 len)
{
    while(len)
    {
        if(!((*x>='0'&&*x<='9') ||(*x>='a'&&*x<='f') || (*x>='A'&&*x<='F')))
        {
            return BOOL_FALSE;
        }
        x++;
        len--;
    }
    
    return BOOL_TRUE;
}

static UINT8 midware_cli_value(const char x)
{
    if(x>='0'&&x<='9')
    {
        return (x-'0');
    }
    else if(x>='a'&&x<='f')
    {
        return (x-'a'+10);
    }
    else if(x>='A'&&x<='F')
    {
        return (x-'A'+10);
    }
    else
    {
        return 0;
    }
}

static bool midware_cli_get_value(const char *p, UINT32 len, UINT32 *value)
{
    UINT32 rc=0;
    
    if(len!=2&&len!=4&&len!=8)
        return BOOL_FALSE;
    while(len)
    {
        rc *=16;
        rc += midware_cli_value(*p);
        p++;
        len--;
    }
    *value = rc;
    return BOOL_TRUE;
}

char lub_ctype_tolower(char c)
{
    unsigned char tmp = (unsigned char)c;
    return tolower(tmp); 
}

int lub_string_nocasecmp(const char *cs, const char *ct)
{
    int result = 0;
    while( (0==result) && *cs && *ct)
    {
        /*lint -e155 Ignoring { }'ed sequence within an expression, 0 assumed 
         * MACRO implementation uses braces to prevent multiple increments
         * when called.
         */
        int s = lub_ctype_tolower(*cs++);
        int t = lub_ctype_tolower(*ct++);

        result = s - t;
    }
    /*lint -e774 Boolean within 'if' always evealuates to True 
     * not the case because of tolower() evaluating to 0 under lint
     * (see above)
     */
    if(0 == result)
    {
        /* account for different string lengths */
        result = *cs - *ct;
    }
    return result;
}


bool  db_hexadec_arg (const char    *arg)
{
    bool  rc = false;

    if (arg != NULL)
    {
        if ((arg[1] == 'x') || (arg[1] == 'X'))
            rc = true;
    }

    return rc;
}


static UINT32 mid_cli_get_number (const char *arg)
{
    UINT32    val = 0;

    if (db_hexadec_arg(arg) == 1)
        sscanf(&arg[2], "%x", &val);
    else
        val = atoi(arg);

    return val;
}


static bool midware_cli_getFieldValueArray(char* name, char *entry, const char *p,MIDWARE_TABLE_INFO *table_info,UINT32 *bitmap)
{
    UINT8 tmpStr[100];
    UINT32 offset = 0;
    MIDWARE_TABLE_ID_E table_id = 0;
    UINT32 index = 0;
    UINT32 offset_in_col = 0;

    UINT8 tmp_value_uint8=0;
    UINT16 tmp_value_uint16=0;
    UINT32 tmp_value_uint32=0;
    
    UINT32 value_loc = 0;
    UINT32 col_size = 0;
    
    while(1)
    {
        offset=midware_cli_findNextEqualSymbol(p);
        
        if(0==offset)
        {
            MID_CLI_PRINT("There is a 0 length field name in arg!\n");
            return BOOL_FALSE;
        }
    
        memset(tmpStr,0,sizeof(tmpStr));
        memcpy(tmpStr,p,offset);

        if(','==*(p+offset)||'\0'==*(p+offset))
        {
            MID_CLI_PRINT("Please input = in str %s!\n", tmpStr);
            
            return BOOL_FALSE;
        }
        else
        {
            value_loc = 0;
            for(index=0;index<table_info->col_num;index++)
            {
                col_size = midware_sqlite3_get_col_size(&(table_info->col_info[index]));
                if(0==lub_string_nocasecmp(tmpStr,table_info->col_info[index].col_name))
                {
                    *bitmap |= (1<<index);
                    break;
                }
                else
                {                        
                    value_loc = value_loc+col_size;
                }
            }
            if(table_info->col_num==index)
            {
                MID_CLI_PRINT("There is no field \"%s\" in table %d!\n", tmpStr, table_info->table_id);
                
                return BOOL_FALSE;
            }

            if(table_info->col_info[index].readonly)
            {
                MID_CLI_PRINT("field %s in table %d is readonly!\n", tmpStr, table_info->table_id);
                
                return BOOL_FALSE;
            }
        }

        p+=offset+1;
        offset=midware_cli_findNextComma(p);
        /*comment the following judgement so that null string can be input*/
        /*
        if(0==offset)
        {
            MID_CLI_PRINT("There is a 0 length field value in arg!\n");            
            return BOOL_FALSE;
        }*/
        
        memset(tmpStr,0,sizeof(tmpStr));
        memcpy(tmpStr,p,offset);
        if(DATA_TYPE_STRING == table_info->col_info[index].datatype)
        {
            strncpy(entry+value_loc,tmpStr,col_size-1);
        }
        else if(DATA_TYPE_UINT8 == table_info->col_info[index].datatype)
        {            
            if(table_info->col_info[index].size>1)
            {
                if(offset!=(2*table_info->col_info[index].size))
                {
                    MID_CLI_PRINT("table id %d, col_name %s input wrong, should input %d hex UINT8 number(0x need not be input,if m<0x0f, should input 0m)!\n", 
                    table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                    return BOOL_FALSE;
                }
                
                offset_in_col = 0;
                while(offset_in_col<(2*table_info->col_info[index].size))
                {                    
                    if(!midware_cli_is_hex_num(p+offset_in_col,2))
                    {
                        MID_CLI_PRINT("table id %d, col_name %s value should be hex number!\n",
                        table_id, table_info->col_info[index].col_name);
                        return BOOL_FALSE;
                    }                    
                    midware_cli_get_value(p+offset_in_col,2,&tmp_value_uint32);                    
                    tmp_value_uint8 = (UINT8)(tmp_value_uint32);
                    memcpy(entry+value_loc+(offset_in_col/2),&tmp_value_uint8,1);
                    offset_in_col+=2;                     
                }
            }
            else
            {
                if(mid_cli_get_number(tmpStr)>0xff)
                {
                    MID_CLI_PRINT("table id %d, col_name %s input wrong, input value %d is beyond 0xff!\n",
                    table_id, table_info->col_info[index].col_name,mid_cli_get_number(tmpStr));
                    return BOOL_FALSE;
                }
                tmp_value_uint8 = mid_cli_get_number(tmpStr);                
                memcpy(entry+value_loc,&tmp_value_uint8,1);
            }                                
        }
        else if(DATA_TYPE_UINT16 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                if(offset!=(4*table_info->col_info[index].size))
                {
                    MID_CLI_PRINT("table id %d, col_name %s input wrong, should input %d hex UINT16 number(0x need not be input,if m<0x0f, should input 0m)!\n", 
                    table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                    return BOOL_FALSE;
                }
                
                offset_in_col = 0;
                while(offset_in_col<(4*table_info->col_info[index].size))
                {                    
                    if(!midware_cli_is_hex_num(p+offset_in_col,4))
                    {
                        MID_CLI_PRINT("table id %d, col_name %s value should be hex number!\n",
                        table_id, table_info->col_info[index].col_name);
                        return BOOL_FALSE;
                    }
                    midware_cli_get_value(p+offset_in_col,4,&tmp_value_uint32);
                    tmp_value_uint16 = (UINT16)(tmp_value_uint32);
                    memcpy(entry+value_loc+(offset_in_col/2),&tmp_value_uint16,2);
                    offset_in_col+=4;
                }
            }
            else
            {
                if(mid_cli_get_number(tmpStr)>0xffff)
                {
                    MID_CLI_PRINT("table id %d, col_name %s input wrong, input value %d is beyond 0xffff!\n",
                    table_id, table_info->col_info[index].col_name,mid_cli_get_number(tmpStr));
                    return BOOL_FALSE;
                }
                tmp_value_uint16 = mid_cli_get_number(tmpStr);                
                memcpy(entry+value_loc,&tmp_value_uint16,2);
            }                                
        }        
        else if(DATA_TYPE_UINT32 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                if(offset!=(8*table_info->col_info[index].size))
                {
                    MID_CLI_PRINT("table id %d, col_name %s input wrong, should input %d hex UINT32 number(0x need not be input,if m < 0x0f, should input 0m)!\n", 
                    table_id, table_info->col_info[index].col_name,table_info->col_info[index].size);
                    return BOOL_FALSE;
                }
                
                offset_in_col = 0;
                while(offset_in_col<(8*table_info->col_info[index].size))
                {                    
                    if(!midware_cli_is_hex_num(p+offset_in_col,8))
                    {
                        MID_CLI_PRINT("table id %d, col_name %s value should be hex number!\n",
                        table_id, table_info->col_info[index].col_name);
                        return BOOL_FALSE;
                    }
                    midware_cli_get_value(p+offset_in_col,8,&tmp_value_uint32);
                    memcpy(entry+value_loc+(offset_in_col/2),&tmp_value_uint32,4);
                    offset_in_col+=8;
                }
            }
            else
            {
                tmp_value_uint32 = mid_cli_get_number(tmpStr);
                memcpy(entry+value_loc,&tmp_value_uint32,4);
            }                                
        }        
        else if(DATA_TYPE_ARRAY == table_info->col_info[index].datatype)
        {
            memcpy(entry+value_loc,&tmpStr,col_size);
        }
        
        if('\0'==*(p+offset))
        {
            break;
        }
        p+=offset+1;
    }
    return BOOL_TRUE;
}

bool Midware_cli_print_table_info(char* name, MIDWARE_TABLE_ID_E table_id)
{
    MIDWARE_TABLE_INFO *table_info = NULL;
    UINT32  index = 0;
    //static bool rc = BOOL_FALSE;
    bool rc = BOOL_FALSE;

    table_info = midware_get_table_info(table_id);

    if(NULL!=table_info)
    {
        MID_CLI_PRINT("table id %d, name %s, col_num %d, table entry size %d:\n", table_info->table_id, table_info->table_name, table_info->col_num, midware_sqlite3_get_entry_size(table_info));
        MID_CLI_PRINT("                col_name    type size readonly primary_key save_to_flash\n");
        for(index=0;index<table_info->col_num;index++)
        {
            MID_CLI_PRINT("%24s  %6s %4d %8s %11s %13s\n", table_info->col_info[index].col_name,
            midware_cli_dataTypeStr[table_info->col_info[index].datatype], table_info->col_info[index].size, 
            midware_cli_boolStr[table_info->col_info[index].readonly], midware_cli_boolStr[table_info->col_info[index].primary_key],
            midware_cli_boolStr[table_info->col_info[index].save_to_flash]);
        }
    }
    else
    {
        MID_CLI_PRINT("table id %d is invalid!\n", table_id);
        rc = BOOL_FALSE;
        return rc;
    }

    rc = BOOL_TRUE;
    return rc;
}

bool Midware_cli_insert_entry(char* name, MIDWARE_TABLE_ID_E table_id, char *arg)
{        
    MIDWARE_TABLE_INFO *table_info = NULL;
    UINT32 entry_size = 0;
    UINT8 * entry = NULL;    
    UINT32 pri_key_bitmap = 0;
    UINT32 bitmap = 0;
    //static bool rc = BOOL_FALSE;
    bool rc = BOOL_FALSE;
    
    table_info = midware_get_table_info(table_id);    
    /*
    MID_CLI_PRINT("test\n");
    MID_CLI_PRINT("arg1 %d arg2 %s\n",table_id,arg);
    */
    if(NULL==table_info)
    {
        MID_CLI_PRINT("table id %d is invalid!\n", table_id);
        rc = BOOL_FALSE;
        return rc;
    }
    
    entry_size = midware_sqlite3_get_entry_size(table_info);

	entry = (void *)malloc(entry_size);

	if(NULL==entry)
	{
	    MID_CLI_PRINT("There is no enough memory!\n");
        rc = BOOL_FALSE;
        return rc;
	}
	memset(entry,0,entry_size);
	
	pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
		    
    if((NULL != arg) && ('\0' != *arg))
    {        

        rc = midware_cli_getFieldValueArray(name,entry,arg,table_info,&bitmap);
        if(BOOL_TRUE!=rc)
        {
            return rc;
        }                      
    }
    else
    {
        if(0!=pri_key_bitmap)
        {
            MID_CLI_PRINT("This table has index, must input primary key fields!\n");
            rc = BOOL_FALSE;
            return rc;
        }
    }
        
    
    if((bitmap&pri_key_bitmap)!=pri_key_bitmap)
    {
        MID_CLI_PRINT("You must input all the primary key field!\n");
        rc = BOOL_FALSE;
        return rc;
    }
            
    if(ONU_OK!=midware_sqlite3_insert_entry(table_id,entry))
    {
        MID_CLI_PRINT("midware_insert_entry() failed!\n");
        free(entry);
        rc = BOOL_FALSE;
        return rc;
    }
    free(entry);
    rc = BOOL_TRUE;
    return rc;   
}

bool Midware_cli_update_entry(char* name, MIDWARE_TABLE_ID_E table_id, char *arg)
{        
    MIDWARE_TABLE_INFO *table_info = NULL;
    UINT32 entry_size = 0;
    UINT8 * entry = NULL;    
    UINT32 pri_key_bitmap = 0;
    UINT32 bitmap = 0;
    //static bool rc = BOOL_FALSE;
    bool rc = BOOL_FALSE;

    table_info = midware_get_table_info(table_id);

    if(NULL==table_info)
    {
        MID_CLI_PRINT("table id %d is invalid!\n", table_id);
        rc = BOOL_FALSE;
        return rc;
    }
    
    entry_size = midware_sqlite3_get_entry_size(table_info);

	entry = (void *)malloc(entry_size);

	if(NULL==entry)
	{
	    MID_CLI_PRINT("There is no enough memory!\n");
        rc = BOOL_FALSE;
        return rc;
	}
	memset(entry,0,entry_size);

	pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
		    
    if((NULL != arg) && ('\0' != *arg))
    {        

        rc = midware_cli_getFieldValueArray(name,entry,arg,table_info,&bitmap);
        if(BOOL_TRUE!=rc)
        {
            return rc;
        }                
    }
    else
    {
        if(0!=pri_key_bitmap)
        {
            MID_CLI_PRINT("This table has index, must input primary key fields!\n");
            rc = BOOL_FALSE;
            return rc;
        }
    }        
    
    if((bitmap&pri_key_bitmap)!=pri_key_bitmap)
    {
        MID_CLI_PRINT("You must input all the primary key field!\n");
        free(entry);
        rc = BOOL_FALSE;
        return rc;
    }
            
    if(ONU_OK!=midware_sqlite3_update_entry(table_id,bitmap,entry))
    {
        MID_CLI_PRINT("midware_update_entry() failed!\n");
        free(entry);
        rc = BOOL_FALSE;
        return rc;
    }
    free(entry);
    rc = BOOL_TRUE;
    return rc;   
}

bool Midware_cli_remove_entry(char* name, MIDWARE_TABLE_ID_E table_id, char *arg)
{        
    MIDWARE_TABLE_INFO *table_info = NULL;
    UINT32 entry_size = 0;
    UINT8 * entry = NULL;    
    UINT32 pri_key_bitmap = 0;
    UINT32 bitmap = 0;
    //static bool rc = BOOL_FALSE;
    bool rc = BOOL_FALSE;

    table_info = midware_get_table_info(table_id);

    if(NULL==table_info)
    {
        MID_CLI_PRINT("table id %d is invalid!\n", table_id);
        rc = BOOL_FALSE;
        return rc;
    }
    
    entry_size = midware_sqlite3_get_entry_size(table_info);

	entry = (void *)malloc(entry_size);

	if(NULL==entry)
	{
	    MID_CLI_PRINT("There is no enough memory!\n");
        rc = BOOL_FALSE;
        return rc;
	}
	memset(entry,0,entry_size);

	pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
		    
    if((NULL != arg) && ('\0' != *arg))
    {        

        rc = midware_cli_getFieldValueArray(name,entry,arg,table_info,&bitmap);
        if(BOOL_TRUE!=rc)
        {
            rc = BOOL_FALSE;
            return rc;
        }                        
    }
    else
    {
        if(0!=pri_key_bitmap)
        {
            MID_CLI_PRINT("This table has index, must input primary key fields!\n");
            rc = BOOL_FALSE;
            return rc;
        }
    }    
    
    if(bitmap!=pri_key_bitmap)
    {
        MID_CLI_PRINT("You must input all the primary key fields as index but not other fields!\n");
        free(entry);
        rc = BOOL_FALSE;
        return rc;
    }
            
    if(ONU_OK!=midware_sqlite3_remove_entry(table_id,entry))
    {
        MID_CLI_PRINT("midware_remove_entry() failed!\n");
        free(entry);
        rc = BOOL_FALSE;
        return rc;
    }
    free(entry);
    rc = BOOL_TRUE;
    return rc;  
}

bool Midware_cli_reset_table(char* name, MIDWARE_TABLE_ID_E table_id)
{     
    //static bool rc = BOOL_FALSE;
    bool rc = BOOL_FALSE;

    if(ONU_OK!=midware_reset_table(table_id))
    {
        MID_CLI_PRINT("midware_reset_table() failed!\n");
        rc = BOOL_FALSE;
        return rc; 
    }
    rc = BOOL_TRUE;
    return rc;    
}

bool Midware_cli_reset_table_data(char* name, MIDWARE_TABLE_ID_E table_id)
{     
    //static bool rc = BOOL_FALSE;
    bool rc = BOOL_FALSE;

    if(ONU_OK!=midware_reset_table_data(table_id))
    {
        MID_CLI_PRINT("midware_reset_table_data() failed!\n");
        rc = BOOL_FALSE;
        return rc; 
    }
    rc = BOOL_TRUE;
    return rc;    
}

bool Midware_cli_save_db(char* name)
{     
    //static bool rc = BOOL_FALSE;
    bool rc = BOOL_FALSE;

    if(ONU_OK!=midware_save_db())
    {
        MID_CLI_PRINT("midware_save_db() failed!\n");
        rc = BOOL_FALSE;
        return rc; 
    }
    MID_CLI_PRINT("midware database is saved!\n");
    rc = BOOL_TRUE;
    return rc;    
}

static UINT32 midware_get_col_print_length(MIDWARE_COL_INFO *col_info)
{
    UINT32 col_size = 0;
    UINT32 colname_size = strlen(col_info->col_name);

    switch(col_info->datatype)
    {
        case DATA_TYPE_STRING:
        {
       
            col_size = 1*col_info->size;
       
            break;
        }
        
        case DATA_TYPE_UINT8:
        {
            if(col_info->size>1)
            {
                col_size = 2*col_info->size;
            }
            else
            {
                col_size = 3; /*if there is negative number, col_size shoud be 4, e.g., -255*/
            }
            break;
        }
        case DATA_TYPE_UINT16:
        {
            if(col_info->size>1)
            {
                col_size = 4*col_info->size;
            }
            else
            {
                col_size = 5; /*if there is negative number, col_size shoud be 4, e.g., -255*/
            }
            break;
        }
        case DATA_TYPE_UINT32:
        {
            if(col_info->size>1)
            {
                col_size = 8*col_info->size;
            }
            else
            {
                col_size = 10; /*if there is negative number, col_size shoud be 4, e.g., -255*/
            }
            break;
        }
        default:
        {
            col_size = 1*col_info->size;
        }
    }

    if(col_size>colname_size)
    {
        return col_size;
    }
    else
    {
        return colname_size;
    }
}

static void midware_cli_print_col(char *name, MIDWARE_TABLE_INFO *table_info,UINT32 colIndex,UINT8 *entry)
{
    UINT32 index = 0;
    UINT32 value_loc = 0;
    UINT32 col_size = 0;
    UINT32 col_print_len = 0;
    UINT32 index_in_col;
    
    if(NULL==table_info || NULL==entry)
    {
        MID_CLI_PRINT("Input pointer NULL!\n");
        return;
    }
    
    if(colIndex>=table_info->col_num)
    {
        MID_CLI_PRINT("colIndex %d should be smaller than col_num %d!\n",colIndex,table_info->col_num);
        return;
    }

    col_print_len = midware_get_col_print_length(&(table_info->col_info[colIndex]));
    
    
    for(index=0;index<colIndex;index++)
    {
        col_size = midware_sqlite3_get_col_size(&(table_info->col_info[index]));
        value_loc += col_size;
    }
    

    if(DATA_TYPE_STRING == table_info->col_info[colIndex].datatype)
    {
        if(col_print_len>=SCREEN_LINE_LENGTH)
        {
            MID_CLI_PRINT("%s\n",(char *)entry+value_loc);
        }
        else
        {
            MID_CLI_PRINT("%-*s ",col_print_len,(char *)entry+value_loc);
        }
    }
    
    else if(DATA_TYPE_UINT8 == table_info->col_info[colIndex].datatype)
    {
        if(table_info->col_info[colIndex].size>1)
        {
            for(index_in_col = 0;index_in_col<table_info->col_info[colIndex].size;index_in_col++)
            {
                MID_CLI_PRINT("%.2x", *(UINT8 *)(entry+value_loc+index_in_col));
            }
            if(col_print_len>=SCREEN_LINE_LENGTH)
            {
                MID_CLI_PRINT("\n",(char *)entry+value_loc);
            }
            else
            {
                MID_CLI_PRINT("%*c",col_print_len+1-2*table_info->col_info[colIndex].size,' ');
            }
        }
        else
        {
            MID_CLI_PRINT("%-*d ",col_print_len,*(UINT8 *)(entry+value_loc));
        }
    }
    else if(DATA_TYPE_UINT16 == table_info->col_info[colIndex].datatype)
    {
        if(table_info->col_info[colIndex].size>1)
        {
            for(index_in_col = 0;index_in_col<table_info->col_info[colIndex].size;index_in_col++)
            {
                MID_CLI_PRINT("%.4x", *(UINT16 *)(entry+value_loc+index_in_col*2));
            }
            if(col_print_len>=SCREEN_LINE_LENGTH)
            {
                MID_CLI_PRINT("\n",(char *)entry+value_loc);
            }
            else
            {
                MID_CLI_PRINT("%*c",col_print_len+1-4*table_info->col_info[colIndex].size,' ');
            }
        }
        else
        {
            MID_CLI_PRINT("%-*d ",col_print_len, *(UINT16 *)(entry+value_loc));
        }
    }
    else if(DATA_TYPE_UINT32 == table_info->col_info[colIndex].datatype)
    {
        if(table_info->col_info[colIndex].size>1)
        {
            for(index_in_col = 0;index_in_col<table_info->col_info[colIndex].size;index_in_col++)
            {
                MID_CLI_PRINT("%.8x", *(UINT32 *)(entry+value_loc+index_in_col*4));
            }
            if(col_print_len>=SCREEN_LINE_LENGTH)
            {
                MID_CLI_PRINT("\n",(char *)entry+value_loc);
            }
            else
            {
                MID_CLI_PRINT("%*c",col_print_len+1-8*table_info->col_info[colIndex].size,' ');
            }
        }
        else
        {
            MID_CLI_PRINT("%-*d ",col_print_len, *(UINT32 *)(entry+value_loc));
        }
    }
    else
    {
        MID_CLI_PRINT("unknown col name %s type: %d\n", table_info->col_info[colIndex].col_name,table_info->col_info[colIndex].datatype);
    }
    return;
}

static void midware_cli_print_one_entry(char *name, MIDWARE_TABLE_INFO *table_info, UINT8 *entry)
{
    UINT32 index = 0;
    UINT32 col_size = 0;
    UINT32 colIndex = 0;
    UINT32 col_print_len = 0;
    UINT32 line_print_len = 0;
    UINT32 start_col = 0;
    UINT32 end_col = 0;  
    UINT32 symbol_no = 0;
    bool flag_too_long = BOOL_FALSE;
    char print_format_str[SCREEN_LINE_LENGTH+1];

    if(NULL==table_info || NULL==entry)
    {
        MID_CLI_PRINT("Input pointer NULL!\n");
        return;
    }
    while(index<table_info->col_num)
    {
        col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
        line_print_len += col_print_len+1;
        if(line_print_len>SCREEN_LINE_LENGTH || ((UINT32)(table_info->col_num-1))==index)
        {
            if(line_print_len>SCREEN_LINE_LENGTH)
            {
                line_print_len = SCREEN_LINE_LENGTH;
                flag_too_long = BOOL_TRUE;
            }
            else /*if((table_info->col_num-1)==index)*/
            {
                index++;
            }
            
            end_col = index;
            for(index=start_col;index<end_col;index++)
            {
                col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
                MID_CLI_PRINT("%-*s ",col_print_len,table_info->col_info[index].col_name);
            }
            MID_CLI_PRINT("\n");
            memset(print_format_str,0,sizeof(print_format_str));
            for(symbol_no=0;symbol_no<line_print_len;symbol_no++)
            {
                print_format_str[symbol_no]='-';
                //MID_CLI_PRINT("-");
            }
            MID_CLI_PRINT("%s",print_format_str);
            MID_CLI_PRINT("\n");
            
            for(index=start_col;index<end_col;index++)
            {
                midware_cli_print_col(name,table_info,index,entry);                
            }            
            MID_CLI_PRINT("\n");
            if(flag_too_long)
            {
                MID_CLI_PRINT("\n");
            }

            if(table_info->col_num==index)
            {
                return;
            }
            /*judge whether current index col len is larger that SCREEN_LINE_LENGTH*/
            col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
            if(col_print_len>=SCREEN_LINE_LENGTH)
            {                
                MID_CLI_PRINT("%s\n",table_info->col_info[index].col_name);
                memset(print_format_str,0,sizeof(print_format_str));
                for(symbol_no=0;symbol_no<SCREEN_LINE_LENGTH;symbol_no++)
                {
                    print_format_str[symbol_no]='-';
                    //MID_CLI_PRINT("-");
                }
                MID_CLI_PRINT("%s",print_format_str);
                MID_CLI_PRINT("\n");
                
                midware_cli_print_col(name,table_info,index,entry);
                MID_CLI_PRINT("\n");
                index++;
            }
            line_print_len = 0;
            start_col = index;
            continue;
        }
        index++;
    }
    return;
}

static bool midware_cli_print_all_entry(char *name, MIDWARE_TABLE_INFO *table_info, UINT8 *entry)
{
    UINT32 index = 0;
    UINT32 col_size = 0;
    UINT32 colIndex = 0;
    UINT32 col_print_len = 0;
    UINT32 line_print_len = 0;
    UINT32 start_col = 0;
    UINT32 end_col = 0;
    ONU_STATUS rc = ONU_OK;
    UINT32 entry_num = 0;
    UINT32 symbol_no = 0;
    bool flag_too_long = BOOL_FALSE;
    char print_format_str[SCREEN_LINE_LENGTH+1];

    if(NULL==table_info || NULL==entry)
    {
        MID_CLI_PRINT("Input pointer NULL!\n");
        return BOOL_FALSE;
    }

    while(index<table_info->col_num)
    {
        col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
        line_print_len += col_print_len+1;
        
        if(line_print_len>SCREEN_LINE_LENGTH || ((UINT32)(table_info->col_num-1))==index)
        {
            if(line_print_len>SCREEN_LINE_LENGTH)
            {
                line_print_len = SCREEN_LINE_LENGTH;
                flag_too_long = BOOL_TRUE;
            }
            else /*if((table_info->col_num-1)==index)*/
            {
                index++;
            }            
            
            end_col = index;

            /*check whether there is any entries*/
            rc = midware_sqlite3_get_first_entry(table_info->table_id, 0xffffffff,entry);	
            if( ONU_FAIL == rc )
            {
                MID_CLI_PRINT("Table %s has no entry!\n",table_info->table_name);
                return BOOL_TRUE;
            }

            /*print previous cols whose total print length is smaller than SCREEN_LINE_LENGTH*/
            for(index=start_col;index<end_col;index++)
            {
                col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
                MID_CLI_PRINT("%-*s ",col_print_len,table_info->col_info[index].col_name);
            }            
            MID_CLI_PRINT("\n");
            
            memset(print_format_str,0,sizeof(print_format_str));            
            for(symbol_no=0;symbol_no<line_print_len;symbol_no++)
            {
                print_format_str[symbol_no]='-';
                //MID_CLI_PRINT("-");
            }
            MID_CLI_PRINT("%s",print_format_str);                
            MID_CLI_PRINT("\n");
            do    
            {
                entry_num++;
                for(index=start_col;index<end_col;index++)
                {
                    midware_cli_print_col(name,table_info,index,entry);                    
                }            
                MID_CLI_PRINT("\n");
                rc = midware_sqlite3_get_next_entry(table_info->table_id, entry);        
                //print_test(cur_loc);
            }while( ONU_FAIL != rc );

            if(flag_too_long)
            {
                MID_CLI_PRINT("\n");
                flag_too_long = BOOL_FALSE;
            }
            
            if(table_info->col_num==index)
            {
                return BOOL_TRUE;
            }
            
            /*judge whether current index col len is larger that SCREEN_LINE_LENGTH*/
            col_print_len = midware_get_col_print_length(&(table_info->col_info[index]));
            if(col_print_len>=SCREEN_LINE_LENGTH)
            {
                MID_CLI_PRINT("%s\n",table_info->col_info[index].col_name);
                
                memset(print_format_str,0,sizeof(print_format_str));
                for(symbol_no=0;symbol_no<SCREEN_LINE_LENGTH;symbol_no++)
                {
                    print_format_str[symbol_no]='-';
                    //MID_CLI_PRINT("-");
                }
                MID_CLI_PRINT("%s",print_format_str);
                MID_CLI_PRINT("\n");
                rc = midware_sqlite3_get_first_entry(table_info->table_id, 0xffffffff,entry);	
                if( ONU_FAIL == rc )
                {
                    /*this branch will not be entered*/
                    MID_CLI_PRINT("Table %s has no entry!\n",table_info->table_name);
                    return BOOL_TRUE;
                }            
                do    
                {
                    entry_num++;
                    //for(index=start_col;index<end_col;index++)
                    {
                        midware_cli_print_col(name,table_info,index,entry);                    
                    }            
                    //MID_CLI_PRINT("\n");
                    rc = midware_sqlite3_get_next_entry(table_info->table_id, entry);        
                    //print_test(cur_loc);
                }while( ONU_FAIL != rc );
                MID_CLI_PRINT("\n");
                index++;
            }
            line_print_len = 0;
            start_col = index;
            continue;
        }
        index++;
    }
    return BOOL_TRUE;
}

static void midware_cli_print_entry(char *name, MIDWARE_TABLE_INFO *table_info, UINT8 *entry)
{
    UINT32 index = 0;
    UINT32 value_loc = 0;
    UINT32 col_size = 0;
    UINT32 colIndex = 0;

    if(NULL==table_info || NULL==entry)
    {
        MID_CLI_PRINT("Input pointer NULL!\n");
        return;
    }
  
    for(index = 0; index < table_info->col_num; index++)
    {
        MID_CLI_PRINT("%s: ", table_info->col_info[index].col_name);
        col_size = midware_sqlite3_get_col_size(&(table_info->col_info[index]));
        
        if(DATA_TYPE_STRING == table_info->col_info[index].datatype)
        {
            MID_CLI_PRINT("%s,\n", (char *)entry+value_loc);
        }
        else if(DATA_TYPE_UINT8 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                for(colIndex = 0;colIndex<table_info->col_info[index].size;colIndex++)
                {
                    MID_CLI_PRINT("%d,", *(UINT8 *)(entry+value_loc+colIndex));
                }
                MID_CLI_PRINT("\n");
            }
            else
            {
                MID_CLI_PRINT("%d,\n", *(UINT8 *)(entry+value_loc));
            }
        }
        else if(DATA_TYPE_UINT16 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                for(colIndex = 0;colIndex<table_info->col_info[index].size;colIndex++)
                {
                    MID_CLI_PRINT("%d,", *(UINT16 *)(entry+value_loc+colIndex*2));
                }
                MID_CLI_PRINT("\n");
            }
            else
            {
                MID_CLI_PRINT("%d,\n", *(UINT16 *)(entry+value_loc));
            }
        }
        else if(DATA_TYPE_UINT32 == table_info->col_info[index].datatype)
        {
            if(table_info->col_info[index].size>1)
            {
                for(colIndex = 0;colIndex<table_info->col_info[index].size;colIndex++)
                {
                    MID_CLI_PRINT("%d,", *(UINT32 *)(entry+value_loc+colIndex*4));
                }
                MID_CLI_PRINT("\n");
            }
            else
            {
                MID_CLI_PRINT("%d,\n", *(UINT32 *)(entry+value_loc));
            }
        }
        else
        {
            MID_CLI_PRINT("unknown col name %s type: %d\n", table_info->col_info[index].col_name,table_info->col_info[index].datatype);
        }
        
        value_loc+=col_size;
    }
}

bool Midware_cli_get_entry(char* name, MIDWARE_TABLE_ID_E table_id, char *arg)
{        
    MIDWARE_TABLE_INFO *table_info = NULL;
    UINT32 entry_size = 0;
    UINT8 * entry = NULL;    
    UINT32 pri_key_bitmap = 0;
    UINT32 bitmap = 0;
    //static bool rc = BOOL_FALSE; 
    bool rc = BOOL_FALSE; 

    table_info = midware_get_table_info(table_id);

    if(NULL==table_info)
    {
        MID_CLI_PRINT("table id %d is invalid!\n", table_id);
        rc = BOOL_FALSE;
        return rc;
    }
    
    entry_size = midware_sqlite3_get_entry_size(table_info);

	entry = (void *)malloc(entry_size);

	if(NULL==entry)
	{
	    MID_CLI_PRINT("There is no enough memory!\n");
        rc = BOOL_FALSE;
        return rc;
	}
	memset(entry,0,entry_size);

	pri_key_bitmap = midware_sqlite3_get_table_prikey_bitmap(table_info);
		    
    if((NULL != arg) && ('\0' != *arg))
    {        

        rc = midware_cli_getFieldValueArray(name,entry,arg,table_info,&bitmap);
        if(BOOL_TRUE!=rc)
        {
            rc = BOOL_FALSE;
            return rc;
        }
    }
    else
    {
        if(0!=pri_key_bitmap)
        {
            MID_CLI_PRINT("This table has index, must input primary key fields!\n");
            rc = BOOL_FALSE;
            return rc;
        }
    }        
    if(bitmap!=pri_key_bitmap)
    {
        MID_CLI_PRINT("You must input all the primary key fields as index but not other fields!\n");
        free(entry);
        rc = BOOL_FALSE;
        return rc;
    }
    
    if(ONU_OK!=midware_sqlite3_get_entry(table_id,0xffffffff,entry))
    {
        MID_CLI_PRINT("midware_get_entry() failed!\n");
        free(entry);
        rc = BOOL_FALSE;
        return rc; 
    }

    midware_cli_print_one_entry(name,table_info,entry);
    
    free(entry);
    rc = BOOL_TRUE;
    return rc;   
}

bool Midware_cli_show_table_all_entry(char* name, MIDWARE_TABLE_ID_E table_id)
{
    MIDWARE_TABLE_INFO *table_info = NULL;
    UINT32 entry_size = 0;
    UINT32 value_loc = 0;
    UINT32 col_size = 0;
    UINT32 colIndex = 0;
    UINT8 * entry = NULL;    
    UINT32 pri_key_bitmap = 0;
    UINT32 bitmap = 0;
    //static bool rc = BOOL_FALSE;
    bool rc = BOOL_FALSE;
    UINT32 index = 0;
    UINT32 entry_num = 0;

    table_info = midware_get_table_info(table_id);

    if(NULL==table_info)
    {
        MID_CLI_PRINT("table id %d is invalid!\n", table_id);
        rc = BOOL_FALSE;
        return rc;
    }
    
    entry_size = midware_sqlite3_get_entry_size(table_info);

	entry = (void *)malloc(entry_size);

	if(NULL==entry)
	{
	    MID_CLI_PRINT("There is no enough memory!\n");
        rc = BOOL_FALSE;
        return rc;
	}
	
	memset(entry,0,entry_size);

    rc = midware_cli_print_all_entry(name, table_info,entry);
    free(entry);
    return rc;     
}

bool Midware_cli_set_print_level(char* name, UINT32 print_level)
{ 
    //static bool rc = BOOL_FALSE; 
    bool rc = BOOL_FALSE;

    if(0==print_level)
    {
        midware_glob_trace = 0;
    }
    else if(1==print_level)
    {
        midware_glob_trace = MIDWARE_TRACE_LEVEL_FATAL;
    }
    else if(2==print_level)
    {
        midware_glob_trace = MIDWARE_TRACE_LEVEL_ERROR;
    }
    else if(3==print_level)
    {
        midware_glob_trace = MIDWARE_TRACE_LEVEL_WARN;
    }
    else if(4==print_level)
    {
        midware_glob_trace = MIDWARE_TRACE_LEVEL_INFO;
    }
    else if(5==print_level)
    {
        midware_glob_trace = MIDWARE_TRACE_LEVEL_DEBUG;
    }
    else if(6==print_level)
    {
        midware_glob_trace = MIDWARE_ALL_TRACE_LEVEL;
    }
    else
    {
        MID_CLI_PRINT("Print level %d is outof range[0..6]!\n",print_level);
        rc = BOOL_FALSE;
        return rc;

    }
    rc = BOOL_TRUE;
    return rc;
}

