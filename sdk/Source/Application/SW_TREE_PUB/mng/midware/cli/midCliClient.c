/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Middleware module                                         **/
/**                                                                          **/
/**  FILE        : midClicClient.c                                           **/
/**                                                                          **/
/**  DESCRIPTION : Definition of middleware CLI IPC client                   **/
/**                                                                          **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *      Victor Initial version created.   12/Jan/2011          
 *                                                                              
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "clish/shell.h"

#include "midCli.h"
#include "midApi.h"
#include "midware_expo.h"
extern CLIENT *midCli_cl;
//extern CLIENT *rpc_init_client();

/*------------------------------------------------------------------------------*/

bool_t midware_cli_print_table_info(const clish_shell_t *this, const lub_argv_t *argv)
{
    MIDWARE_TABLE_CLI_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
   
    if (NULL == midware_cli_print_table_info_1(table_id, midCli_cl))
    {
      clnt_perror(midCli_cl, "Midware CLI server");
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE; 
}

bool_t midware_cli_insert_entry(const clish_shell_t *this, const lub_argv_t *argv)

{        
    MIDWARE_TABLE_CLI_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const u_char *arg = lub_argv__get_arg(argv,1);

    if (NULL == midware_cli_insert_entry_1(table_id, arg, midCli_cl))
    {
      clnt_perror(midCli_cl, "Midware CLI server");
      return BOOL_FALSE;
    }

    return BOOL_TRUE; 
}

bool_t midware_cli_update_entry(const clish_shell_t *this, const lub_argv_t *argv)
{        
    MIDWARE_TABLE_CLI_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const u_char *arg = lub_argv__get_arg(argv,1);

    if (NULL == midware_cli_update_entry_1(table_id, arg, midCli_cl))
    {
      clnt_perror(midCli_cl, "Midware CLI server");
      return BOOL_FALSE;
    }

    return BOOL_TRUE;    
}

bool_t midware_cli_remove_entry(const clish_shell_t *this, const lub_argv_t *argv)
{        
    MIDWARE_TABLE_CLI_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const u_char *arg = lub_argv__get_arg(argv,1);

    if (NULL == midware_cli_remove_entry_1(table_id, arg, midCli_cl))
    {
      clnt_perror(midCli_cl, "Midware CLI server");
      return BOOL_FALSE;
    }

    return BOOL_TRUE;    
}


bool_t midware_cli_reset_table(const clish_shell_t *this, const lub_argv_t *argv)
{
    MIDWARE_TABLE_CLI_ID_E table_id = atoi(lub_argv__get_arg(argv,0));

    if (NULL == midware_cli_reset_table_1(table_id, midCli_cl))
    {
      clnt_perror(midCli_cl, "Midware CLI server");
      return BOOL_FALSE;
    }

    return BOOL_TRUE;   
}

bool_t midware_cli_get_entry(const clish_shell_t *this, const lub_argv_t *argv)
{        
    MIDWARE_TABLE_CLI_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const u_char *arg = lub_argv__get_arg(argv,1);

    if (NULL == midware_cli_get_entry_1(table_id, arg, midCli_cl))
    {
      clnt_perror(midCli_cl, "Midware CLI server");
      return BOOL_FALSE;
    }

    return BOOL_TRUE;    
}

bool_t midware_cli_show_table_all_entry(const clish_shell_t *this, const lub_argv_t *argv)
{
    MIDWARE_TABLE_CLI_ID_E table_id = atoi(lub_argv__get_arg(argv,0));

    if (NULL == midware_cli_show_table_all_entry_1(table_id, midCli_cl))
    {
      clnt_perror(midCli_cl, "Midware CLI server");
      return BOOL_FALSE;
    }

    return BOOL_TRUE;    
}

bool_t midware_cli_set_print_level(const clish_shell_t *this, const lub_argv_t *argv)
{
    u_int level = atoi(lub_argv__get_arg(argv,0));

    if (NULL == midware_cli_set_print_level_1(level, midCli_cl))
    {
      clnt_perror(midCli_cl, "Midware CLI server");
      return BOOL_FALSE;
    }

    return BOOL_TRUE; 

}
/*
void midCLiClientInit(void)
{
  midCli_cl = rpc_init_client(MID_SOCK_ENV_NAME, MidCli, MIDVERS);

  if ((!midCli_cl))
  {
    exit(1);
  }
}
*/
