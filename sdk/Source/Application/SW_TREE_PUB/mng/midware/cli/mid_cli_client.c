/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Middleware module                                         **/
/**                                                                          **/
/**  FILE        : midClicClient.c                                           **/
/**                                                                          **/
/**  DESCRIPTION : Definition of middleware CLI IPC client                   **/
/**                                                                          **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 * 
 *      Victor Initial version created.   12/Jan/2011          
 *                                                                              
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "clish/shell.h"

//#include "midCli.h"
//#include "midApi.h"
#include "globals.h"
#include "midware_expo.h"
//extern CLIENT *midCli_cl;
//extern CLIENT *rpc_init_client();

//#include "mid_cli.h"



/*------------------------------------------------------------------------------*/

bool midware_cli_print_table_info(const clish_shell_t *this, const lub_argv_t *argv)
{
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
   
    if (BOOL_TRUE != Midware_cli_print_table_info(NULL,table_id))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE; 
}

bool midware_cli_insert_entry(const clish_shell_t *this, const lub_argv_t *argv)

{        
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const u_char *arg = lub_argv__get_arg(argv,1);
    if (BOOL_TRUE != Midware_cli_insert_entry(NULL,table_id, arg))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;     
}

bool midware_cli_update_entry(const clish_shell_t *this, const lub_argv_t *argv)
{            
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const u_char *arg = lub_argv__get_arg(argv,1);
    if (BOOL_TRUE != Midware_cli_update_entry(NULL,table_id, arg))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;     
}

bool midware_cli_remove_entry(const clish_shell_t *this, const lub_argv_t *argv)
{            
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const u_char *arg = lub_argv__get_arg(argv,1);
    if (BOOL_TRUE != Midware_cli_remove_entry(NULL,table_id, arg))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;   
}

bool midware_cli_reset_table(const clish_shell_t *this, const lub_argv_t *argv)
{
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));

    if (BOOL_TRUE != Midware_cli_reset_table(NULL,table_id))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;   
}

bool midware_cli_reset_table_data(const clish_shell_t *this, const lub_argv_t *argv)
{
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));

    if (BOOL_TRUE != Midware_cli_reset_table_data(NULL,table_id))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;   
}

bool midware_cli_save_db(const clish_shell_t *this, const lub_argv_t *argv)
{

    if (BOOL_TRUE != Midware_cli_save_db(NULL))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;   
}

bool midware_cli_get_entry(const clish_shell_t *this, const lub_argv_t *argv)
{        
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));
    const u_char *arg = lub_argv__get_arg(argv,1);
    if (BOOL_TRUE != Midware_cli_get_entry(NULL,table_id, arg))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;     
}

bool midware_cli_show_table_all_entry(const clish_shell_t *this, const lub_argv_t *argv)
{
    MIDWARE_TABLE_ID_E table_id = atoi(lub_argv__get_arg(argv,0));

    if (BOOL_TRUE != Midware_cli_show_table_all_entry(NULL,table_id))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;    
}

bool midware_cli_set_print_level(const clish_shell_t *this, const lub_argv_t *argv)
{
    u_int level = atoi(lub_argv__get_arg(argv,0));

    if (BOOL_TRUE != Midware_cli_set_print_level(NULL,level))
    {
      printf("%s, IPC call failed. \r\n", __FUNCTION__);
      return BOOL_FALSE;
    }
    
    return BOOL_TRUE;    
}
