/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************
**  FILE        : mv_cust_mng_if.h                                           **
**                                                                           **
**  DESCRIPTION : This file contains ONU MV CUST Management Interface        **
*******************************************************************************
*                                                                             *
*  MODIFICATION HISTORY:                                                      *
*                                                                             *
*   01Feb11  Yuval Caduri   created                                           *
* =========================================================================== *
******************************************************************************/
#ifndef _MV_CUST_MNG_IF_H_
#define _MV_CUST_MNG_IF_H_

/* Include Files
------------------------------------------------------------------------------*/
//#include <linux/cdev.h>

/* Definitions
------------------------------------------------------------------------------*/
#define CUST_NUM_DEVICES    (1)
#define CUST_DEV_NAME       ("cust")
#define MV_CUST_IOCTL_MAGIC  ('C')


#define MV_CUST_IOCTL_OMCI_SET                _IOW(MV_CUST_IOCTL_MAGIC,  1, unsigned int)
#define MV_CUST_IOCTL_EOAM_LLID_SET           _IOW(MV_CUST_IOCTL_MAGIC,  2, unsigned int)
#define MV_CUST_IOCTL_EOAM_ENABLE             _IOW(MV_CUST_IOCTL_MAGIC,  3, unsigned int)
#define MV_CUST_IOCTL_OMCI_ENABLE             _IOW(MV_CUST_IOCTL_MAGIC,  4, unsigned int)
#define MV_CUST_IOCTL_MAP_RULE_SET            _IOW(MV_CUST_IOCTL_MAGIC,  5, unsigned int)
#define MV_CUST_IOCTL_DSCP_MAP_SET            _IOW(MV_CUST_IOCTL_MAGIC,  6, unsigned int)
#define MV_CUST_IOCTL_MAP_RULE_DEL            _IOW(MV_CUST_IOCTL_MAGIC,  7, unsigned int)
#define MV_CUST_IOCTL_DSCP_MAP_DEL            _IOW(MV_CUST_IOCTL_MAGIC,  8, unsigned int)
#define MV_CUST_IOCTL_MAP_RULE_CLEAR          _IOW(MV_CUST_IOCTL_MAGIC,  9, unsigned int)
#define MV_CUST_IOCTL_TAG_MAP_RULE_GET        _IOR(MV_CUST_IOCTL_MAGIC,  10,unsigned int)
#define MV_CUST_IOCTL_UNTAG_MAP_RULE_GET      _IOR(MV_CUST_IOCTL_MAGIC,  11,unsigned int)
#define MV_CUST_IOCTL_APP_ETH_TYPE_SET        _IOW(MV_CUST_IOCTL_MAGIC,  12,unsigned int)



/* Enums
------------------------------------------------------------------------------*/

/* Typedefs
------------------------------------------------------------------------------*/
typedef struct
{
    int                tcont;
    int                txq;
    int                gemport;
    int                keep_rx_mh;
} mv_cust_ioctl_omci_set_t;

typedef struct
{
    int                llid;
    int                txq;
    uint8_t            llid_mac[6];
} mv_cust_ioctl_llid_set_t;

typedef struct
{
    mv_cust_app_type_e app_type;
    uint16_t           eth_type;
} mv_cust_ioctl_app_etype_t;


/* Global variables
------------------------------------------------------------------------------*/

/* Global functions
------------------------------------------------------------------------------*/

/* Global variables
------------------------------------------------------------------------------*/

/* Global functions
------------------------------------------------------------------------------*/

/* Macros
------------------------------------------------------------------------------*/

#endif /* _MV_CUST_MNG_IF_H_ */
