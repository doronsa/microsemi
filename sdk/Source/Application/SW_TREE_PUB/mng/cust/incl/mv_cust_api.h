/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************
* mv_cust_api.h
*
* DESCRIPTION:
*               MV Customized = MV CUST
*
* DEPENDENCIES:
*               None
*
* CREATED BY:  cYUval
*
* DATE CREATED:
*
* FILE REVISION NUMBER:
*               $Revision: 1.24 $
*
*
*******************************************************************************/

#ifndef _MV_CUST_API_H_
#define _MV_CUST_API_H_

#ifdef __cplusplus
extern "C" {
#endif

#define MV_CUST_MH_LEN                    (2)     /* To use global MH length definition in future */ 
#define MV_CUST_PONMAC_INDEX              (2)     /* To use global MH length definition in future */ 
#define MV_CUST_UNI_PORT_NUM              (4)
#define MV_CUST_MAX_FRAME_SIZE            (1600)

#define MV_CUST_VID_NOT_CARE_VALUE        (4096)  /* Does not care for VID for untagged frames    */
#define MV_CUST_PBITS_NOT_CARE_VALUE      (8)     /* Does not care for P-bits                     */
#define MV_CUST_DSCP_NOT_CARE_VALUE       (64)    /* Does not care for DSCP                       */

#define MV_CUST_DEFAULT_UNTAG_RULE        (4096+1)/* Default untagged  rule        */
#define MV_CUST_DEFAULT_SINGLE_TAG_RULE   (4096+2)/* Default sinlge tagged  rule   */
#define MV_CUST_DEFAULT_DOUBLE_UNTAG_RULE (4096+3)/* Default double tagged  rule   */

#define CUST_OK    (0)
#define CUST_ERROR (1)

/* Default Application Ethernet type used for socket and skb */
#define MV_CUST_ETH_TYPE_IGMP      (0xA000)
#define MV_CUST_ETH_TYPE_MLD       (0xAB00)
#define MV_CUST_ETH_TYPE_LPBK      (0xFFFA)
#define MV_CUST_ETH_TYPE_OAM       (0xBABA)
/* The Ethernet tpye of OAM/OMCI could be same since they will not use at the same time*/
#define MV_CUST_ETH_TYPE_OMCI      (0xBABA)

/* P-bits flow mapping table definition */
typedef uint32_t mv_cust_trg_port_type_t;
typedef uint32_t mv_cust_gem_port_key_t;
typedef struct mv_cust_pkt_frwd {
    uint32_t                in_use;
    mv_cust_trg_port_type_t trg_port;                                         
    uint32_t                trg_queue;                      
    uint32_t                trg_hwf_queue;      
    mv_cust_gem_port_key_t  gem_port;
} mv_cust_pkt_frwd_t;

/* DSCP to P-bits mapping table definition */
#define MV_CUST_DSCP_PBITS_TABLE_MAX_SIZE  (64)
typedef struct {
    uint32_t in_use;
    uint8_t  pbits[MV_CUST_DSCP_PBITS_TABLE_MAX_SIZE];
} mv_cust_dscp_pbits_t;

/* DIR enum */
typedef enum
{
    MV_CUST_FLOW_DIR_US   = 0,
    MV_CUST_FLOW_DIR_DS   = 1,
    MV_CUST_FLOW_DIR_NUM  = 2
} mv_cust_flow_dir_e;

typedef enum
{
    MV_CUST_APP_TYPE_IGMP = 0, /* For IGMP application               */
    MV_CUST_APP_TYPE_MLD,      /* For MLD application                */
    MV_CUST_APP_TYPE_LPBK,     /* For loopback detection application */
    MV_CUST_APP_TYPE_OAM,      /* For eOAM application               */
    MV_CUST_APP_TYPE_OMCI,     /* For OMCI application               */
    MV_CUST_APP_TYPE_MAX       /* Max number of application          */ 
} mv_cust_app_type_e;

/*  Enum: enable or disable application*/
typedef enum
{
    MV_CUST_APP_DISABLE  = 0,
    MV_CUST_APP_ENABLE   = 1,
} mv_cust_app_flag_e;

typedef struct
{
    mv_cust_app_type_e app_type; /* Application type, such as IGMP, MLD                */
    uint16_t           enable;   /* Flag indicates whether to enable application Rx/Tx */         
    uint16_t           eth_type; /* Application Ethernet type used for socket/skb      */
    char              *name;     /* The readable name of the application               */
} mv_cust_app_config_t;

typedef struct
{
    mv_cust_flow_dir_e dir;
    int                vid;
    int                pbits;
    int                dscp;  
    int                mod_vid;
    int                mod_pbits;
    mv_cust_pkt_frwd_t pkt_frwd;

} mv_cust_ioctl_flow_map_t;

typedef struct
{
    mv_cust_dscp_pbits_t dscp_map;
} mv_cust_ioctl_dscp_map_t;

int         mv_cust_omci_enable(int enable);
int         mv_cust_eoam_enable(int enable);
int         mv_cust_omci_set(int tcont, int txq, int gemport, int keep_rx_mh);
int         mv_cust_eoam_llid_mac_set(int llid, uint8_t *llid_mac, int txq);

/*******************************************************************************
**
**    mv_cust_map_rule_set
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function sets GPON flow mapping rules
**
**    INPUTS:
**      cust_flow   - flow mapping rule.
**
**    OUTPUTS:
**      None. 
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_map_rule_set(mv_cust_ioctl_flow_map_t  *cust_flow);

/*******************************************************************************
**
**    mv_cust_dscp_map_set
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function sets GPON DSCP to P-bits mapping rules
**
**    INPUTS:
**      dscp_map     - DSCP to P-bits mapping rules.
**
**    OUTPUTS:
**      None. 
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_dscp_map_set(mv_cust_dscp_pbits_t *dscp_map);

/*******************************************************************************
**
**    mv_cust_map_rule_del
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function deletes GPON flow mapping rules
**
**    INPUTS:
**      vid     - VLAN ID.
**      pbits   - 802.1p value.
**      dir     - Direction of flow mapping rule, 0:U/S, 1:D/S
**
**    OUTPUTS:
**      None 
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_map_rule_del(uint16_t vid, uint8_t pbits, mv_cust_flow_dir_e dir);

/*******************************************************************************
**
**    mv_cust_dscp_map_del
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function deletes DSCP to P-bits mapping rules
**
**    INPUTS:
**      None.
**
**    OUTPUTS:
**      None. 
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_dscp_map_del(void);

/*******************************************************************************
**
**    mv_cust_map_rule_clear
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function clears all GPON flow mapping rules
**
**    INPUTS:
**      None.
**
**    OUTPUTS:
**      None. 
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_map_rule_clear(void);

/*******************************************************************************
**
**    mv_cust_tag_map_rule_get
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function gets GPON flow mapping rule for tagged frames.
**
**  INPUTS:
**    cust_flow  - parsing vid, pbits, dir
**
**  OUTPUTS:
**    cust_flow  - out packet forwarding information, including GEM port, T-CONT, queue.
**                 and packet modification for VID, P-bits
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_tag_map_rule_get(mv_cust_ioctl_flow_map_t *cust_flow);

/*******************************************************************************
**
**    mv_cust_untag_map_rule_get
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function gets GPON flow mapping rule for untagged frames.
**
**    INPUTS:
**      dscp         - DSCP value.
**
**    OUTPUTS:
**      pkt_fwd     - packet forwarding information, including GEM port, T-CONT, queue.
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_untag_map_rule_get(mv_cust_ioctl_flow_map_t *cust_flow);

/*******************************************************************************
* mv_cust_app_etype_get()
*
* DESCRIPTION: The API gets the application Ethernet type defined in mv_cust
*
* INPUTS:   
*           app_type  - Application type, such as IGMP, lpbk detect
*
* OUTPUTS:
*           eth_type  - Ethernet type value, such as 0xfffa
*
* RETURNS:          On success, returns CUST_OK. 
*                   On error, returns CUST_ERROR.
*
*******************************************************************************/
int32_t mv_cust_app_etype_get(mv_cust_app_type_e app_type,
        uint16_t            *eth_type);

/*******************************************************************************
* mv_cust_app_etype_set()
*
* DESCRIPTION: The API sets the application Ethernet type defined in mv_cust
*
* INPUTS:   
*           app_type  - Application type, such as IGMP, lpbk detect
*           eth_type  - Ethernet type value, such as 0xfffa

*
* OUTPUTS:
*           None 
*
* RETURNS:          On success, returns CUST_OK. 
*                   On error, returns CUST_ERROR.
*
*******************************************************************************/
int32_t mv_cust_app_etype_set(mv_cust_app_type_e app_type,
        uint16_t            eth_type);

/*******************************************************************************
* mv_cust_app_rx_parse()
*
* DESCRIPTION: The API receives application frames from either LAN or WAN side,
*              it will be blocked until a packet arrived.
*
* INPUTS:   
*           buf         - Application frame buffer allocated by user
*           len         - The length of the frame
*
* OUTPUTS:
*           dir         - Direction of the frame, either upstream or downstream
*           uni_port    - source UNI port in case this frame comes from LAN side
*
* RETURNS:          On success, returns CUST_OK. 
*                   On error, returns CUST_ERROR.
*
*******************************************************************************/
int32_t mv_cust_app_rx_parse(uint8_t  *buf,
        uint32_t           *len,
        mv_cust_flow_dir_e *dir,
        uint32_t           *uni_port
);

/*******************************************************************************
* mv_cust_app_tx()
*
* DESCRIPTION: The API modifies the marvell header of the frame, so that it could be
*               sent to required target UNI ports
*
* INPUTS:   
*           type            - Application type, such as IGMP, lpbk detect
*           dir             - Direction of the frame, either upstream or downstream
*           uni_port_bitmap - Target UNI port bitmap in case this frame is to be sent to LAN side
*           buf             - Application frame buffer allocated by user
*           len             - The length of the frame
*
* OUTPUTS:
*           NONE
*
* RETURNS:          On success, returns CUST_OK. 
*                   On error, returns CUST_ERROR.
*
*******************************************************************************/
int32_t mv_cust_app_tx(mv_cust_app_type_e type,
        mv_cust_flow_dir_e  dir,
        uint32_t            uni_port_bitmap,
        uint8_t            *buf, 
        uint32_t            len);

#ifdef __cplusplus
}
#endif


#endif /* _MV_CUST_API_H_ */
