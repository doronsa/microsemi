/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/if.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <fcntl.h>
#include "globals.h"
#include "errorCode.h"
#include "OsGlueLayer.h"
#include "mv_cust_api.h"
#include "mv_cust_mng_if.h"

#include "tpm_types.h"
#include "tpm_api.h"

int             custDrvFd    = -1;
GL_SEMAPHORE_ID custApiSemId = NULL;

/* If add new app type, should add default value for it */
static int gCustSock[MV_CUST_APP_TYPE_MAX][MV_CUST_FLOW_DIR_NUM] = {{-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}};

/* Global cust configuration*/
/* Pay attention that the order could be be changed and the entry could not be removed */
static mv_cust_app_config_t gCustConfig[] =
{
    /* Application Type */   /* Enable Flag */   /*Application Eth type*/   /* Application Description*/
    {MV_CUST_APP_TYPE_IGMP,  MV_CUST_APP_ENABLE, MV_CUST_ETH_TYPE_IGMP,     "IGMP application"},
    {MV_CUST_APP_TYPE_MLD,   MV_CUST_APP_ENABLE, MV_CUST_ETH_TYPE_MLD,      "MLD application"},
    {MV_CUST_APP_TYPE_LPBK,  MV_CUST_APP_ENABLE, MV_CUST_ETH_TYPE_LPBK,     "Loopback detection application"},
    {MV_CUST_APP_TYPE_OAM,   MV_CUST_APP_ENABLE, MV_CUST_ETH_TYPE_OAM,      "eOAM application"},
    {MV_CUST_APP_TYPE_OMCI,  MV_CUST_APP_ENABLE, MV_CUST_ETH_TYPE_OMCI,     "OMCI application"},    
};

/*******************************************************************************
* getCustDrvFd
*
* DESCRIPTION:      
*
* INPUTS:            none
*
* OUTPUTS:            none
*
* RETURNS:          On success, returns 1. 
*                    On error, returns 0.
*
*******************************************************************************/
int getCustDrvFd(void)
{
    if (NULL == custApiSemId)
    {
        /* Initialized as sem_full */
        if (osSemCreate(&custApiSemId, "/CustApiSem", 1, SEM_Q_FIFO, TRUE) != IAMBA_OK)
        {
            printf("Failed to create CUST API semaphore\n\r");                                                     
            return -1; 
        }    
    }

    if (custDrvFd >= 0)
    {
        return custDrvFd;
    }
    
    custDrvFd = open("/dev/cust", O_RDWR);
    if (custDrvFd < 0)
    {
        printf("Failed to open /dev/cust\n\r");
        return(-1);
    }

    return(custDrvFd);
}



/*******************************************************************************
* mv_cust_eoam_enable
*
* DESCRIPTION:      
*
* INPUTS:            int enable
*
* OUTPUTS:            none
*
* RETURNS:          On success, returns CUST_OK. 
*                    On error, returns CUST_ERROR.
*
*******************************************************************************/
int mv_cust_eoam_enable(int enable)
{
  int                fd;

  fd = getCustDrvFd();
  if (fd < 0)
    return(CUST_ERROR);

  osSemTake(custApiSemId, GL_SUSPEND);

  if (ioctl(fd, MV_CUST_IOCTL_EOAM_ENABLE, &enable))
  {
    printf("%s: IOCTL failed\n\r", __FUNCTION__);
      osSemGive(custApiSemId);
    return(CUST_ERROR);
  }

  osSemGive(custApiSemId);

  return(CUST_OK);
}

/*******************************************************************************
* mv_cust_eoam_llid_mac_set
*
* DESCRIPTION:      
*
* INPUTS:            int      llid
*                   uint8_t *llid_mac
*                   int      txq
*
* OUTPUTS:            none
*
* RETURNS:          On success, returns CUST_OK. 
*                    On error, returns CUST_ERROR.
*
*******************************************************************************/
int mv_cust_eoam_llid_mac_set(int llid, uint8_t *llid_mac, int txq)
{
  int                       fd;
  mv_cust_ioctl_llid_set_t cust_llid;

  fd = getCustDrvFd();
  if (fd < 0)
    return(CUST_ERROR);

  cust_llid.llid = llid;
  cust_llid.txq = txq;
  memcpy(&(cust_llid.llid_mac[0]), llid_mac, 6);

  osSemTake(custApiSemId, GL_SUSPEND);
  if (ioctl(fd, MV_CUST_IOCTL_EOAM_LLID_SET, &cust_llid))
  {
    printf("%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(custApiSemId);
    return(CUST_ERROR);
  }

  osSemGive(custApiSemId);

  return(CUST_OK);
}

/*******************************************************************************
* mv_cust_omci_enable
*
* DESCRIPTION:      
*
* INPUTS:            int enable
*
* OUTPUTS:            none
*
* RETURNS:          On success, returns CUST_OK. 
*                    On error, returns CUST_ERROR.
*
*******************************************************************************/
int  mv_cust_omci_enable(int enable)
{
  int fd;

  fd = getCustDrvFd();
  if (fd < 0)
    return(CUST_ERROR);

  osSemTake(custApiSemId, GL_SUSPEND);

  if (ioctl(fd, MV_CUST_IOCTL_OMCI_ENABLE, &enable))
  {
    printf("%s: IOCTL failed\n\r", __FUNCTION__);
    osSemGive(custApiSemId);
    return(CUST_ERROR);
  }

  osSemGive(custApiSemId);

  return(CUST_OK);
}

/*******************************************************************************
* mv_cust_omci_set
*
* DESCRIPTION:      
*
* INPUTS:            int tcont
*                   int txq
*                   int gemport
*                   int keep_rx_mh
*
* OUTPUTS:            none
*
* RETURNS:          On success, returns CUST_OK. 
*                    On error, returns CUST_ERROR.
*
*******************************************************************************/
int mv_cust_omci_set(int tcont, int txq, int gemport, int keep_rx_mh)
{
  int                fd;
  mv_cust_ioctl_omci_set_t cust_omci_set;

  fd = getCustDrvFd();
  if (fd < 0)
    return(CUST_ERROR);

  cust_omci_set.tcont     = tcont;
  cust_omci_set.txq       = txq;
  cust_omci_set.gemport   = gemport;
  cust_omci_set.keep_rx_mh = keep_rx_mh;

  osSemTake(custApiSemId, GL_SUSPEND);

  if (ioctl(fd, MV_CUST_IOCTL_OMCI_SET, &cust_omci_set))
  {
    printf("%s: IOCTL failed\n\r", __FUNCTION__);
      osSemGive(custApiSemId);
    return(CUST_ERROR);
  }

  osSemGive(custApiSemId);

  return(CUST_OK);
}

/*******************************************************************************
**
**    mv_cust_map_rule_set
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function sets GPON flow mapping rules
**
**    INPUTS:
**      cust_flow   - flow mapping rule.
**
**    OUTPUTS:
**      None. 
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_map_rule_set(mv_cust_ioctl_flow_map_t  *cust_flow)
{
    int   fd;

    fd = getCustDrvFd();
    if (fd < 0)
        return CUST_ERROR;

    if( NULL == cust_flow)
        return CUST_ERROR;
    
    osSemTake(custApiSemId, GL_SUSPEND);

    if (ioctl(fd, MV_CUST_IOCTL_MAP_RULE_SET, cust_flow))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(custApiSemId);
        return CUST_ERROR;
    }
    osSemGive(custApiSemId);

    return CUST_OK;
}

/*******************************************************************************
**
**    mv_cust_dscp_map_set
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function sets GPON DSCP to P-bits mapping rules
**
**    INPUTS:
**      dscp_map     - DSCP to P-bits mapping rules.
**
**    OUTPUTS:
**      None. 
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_dscp_map_set(mv_cust_dscp_pbits_t *dscp_map)
{
    int                      fd;
    mv_cust_ioctl_dscp_map_t cust_dscp_map;

    fd = getCustDrvFd();
    if (fd < 0)
        return CUST_ERROR;

    memcpy(&cust_dscp_map.dscp_map, dscp_map, sizeof(mv_cust_dscp_pbits_t));

    osSemTake(custApiSemId, GL_SUSPEND);

    if (ioctl(fd, MV_CUST_IOCTL_MAP_RULE_SET, &cust_dscp_map))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(custApiSemId);
        return CUST_ERROR;
    }
    osSemGive(custApiSemId);

    return CUST_OK;
}

/*******************************************************************************
**
**    mv_cust_map_rule_del
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function deletes GPON flow mapping rules
**
**    INPUTS:
**      vid     - VLAN ID.
**      pbits   - 802.1p value.
**      dir     - Direction of flow mapping rule, 0:U/S, 1:D/S
**
**    OUTPUTS:
**      None 
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_map_rule_del(uint16_t vid, uint8_t pbits, mv_cust_flow_dir_e dir)
{
    int                      fd;
    mv_cust_ioctl_flow_map_t cust_flow_map;

    fd = getCustDrvFd();
    if (fd < 0)
        return CUST_ERROR;

    cust_flow_map.vid    = (int)vid;
    cust_flow_map.pbits  = (int)pbits;
    cust_flow_map.dir    = dir;

    osSemTake(custApiSemId, GL_SUSPEND);

    if (ioctl(fd, MV_CUST_IOCTL_MAP_RULE_DEL, &cust_flow_map))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(custApiSemId);
        return CUST_ERROR;
    }
    osSemGive(custApiSemId);

    return CUST_OK;    
}

/*******************************************************************************
**
**    mv_cust_dscp_map_del
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function deletes DSCP to P-bits mapping rules
**
**    INPUTS:
**      None.
**
**    OUTPUTS:
**      None. 
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_dscp_map_del(void)
{
    int                      fd;
    mv_cust_ioctl_dscp_map_t cust_dscp_map;

    fd = getCustDrvFd();
    if (fd < 0)
        return CUST_ERROR;


    osSemTake(custApiSemId, GL_SUSPEND);

    if (ioctl(fd, MV_CUST_IOCTL_DSCP_MAP_DEL, &cust_dscp_map))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(custApiSemId);
        return CUST_ERROR;
    }
    osSemGive(custApiSemId);

    return CUST_OK; 
}

/*******************************************************************************
**
**    mv_cust_map_rule_clear
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function clears all GPON flow mapping rules
**
**    INPUTS:
**      None.
**
**    OUTPUTS:
**      None. 
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_map_rule_clear(void)
{
    int                      fd;
    mv_cust_ioctl_flow_map_t cust_flow_map;

    fd = getCustDrvFd();
    if (fd < 0)
        return CUST_ERROR;

    osSemTake(custApiSemId, GL_SUSPEND);

    if (ioctl(fd, MV_CUST_IOCTL_MAP_RULE_CLEAR, &cust_flow_map))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(custApiSemId);
        return CUST_ERROR;
    }
    osSemGive(custApiSemId);

    return CUST_OK; 
}

/*******************************************************************************
**
**    mv_cust_tag_map_rule_get
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function gets GPON flow mapping rule for tagged frames.
**
**  INPUTS:
**    cust_flow  - parsing vid, pbits, dir
**
**  OUTPUTS:
**    cust_flow  - out packet forwarding information, including GEM port, T-CONT, queue.
**                 and packet modification for VID, P-bits
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_tag_map_rule_get(mv_cust_ioctl_flow_map_t *cust_flow)
{
    int  fd;

    fd = getCustDrvFd();
    if (fd < 0)
        return CUST_ERROR;

    osSemTake(custApiSemId, GL_SUSPEND);

    if (ioctl(fd, MV_CUST_IOCTL_TAG_MAP_RULE_GET, cust_flow))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(custApiSemId);
        return CUST_ERROR;
    }
    osSemGive(custApiSemId);

    return CUST_OK;    
}

/*******************************************************************************
**
**    mv_cust_untag_map_rule_get
**    ___________________________________________________________________________
**
**    DESCRIPTION: The function gets GPON flow mapping rule for untagged frames.
**
**    INPUTS:
**      dscp         - DSCP value.
**
**    OUTPUTS:
**      pkt_fwd     - packet forwarding information, including GEM port, T-CONT, queue.
**
**    RETURNS:     
**      On success, the function returns (MV_CUST_OK). On error different types are 
**    returned according to the case.  
**
*******************************************************************************/
int mv_cust_untag_map_rule_get(mv_cust_ioctl_flow_map_t *cust_flow)
{
    int                      fd;

    fd = getCustDrvFd();
    if (fd < 0)
        return CUST_ERROR;

    osSemTake(custApiSemId, GL_SUSPEND);

    if (ioctl(fd, MV_CUST_IOCTL_UNTAG_MAP_RULE_GET, cust_flow))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(custApiSemId);
        return CUST_ERROR;
    }
    osSemGive(custApiSemId);

    return CUST_OK; 
}

/*******************************************************************************
* mv_cust_app_etype_ctrl()
*
* DESCRIPTION: The API sets the application Ethernet type defined in mv_cust
*
* INPUTS:   
*           app_type  - Application type, such as IGMP, lpbk detect
*           eth_type  - Ethernet type value, such as 0xfffa

*
* OUTPUTS:
*           None 
*
* RETURNS:          On success, returns CUST_OK. 
*                   On error, returns CUST_ERROR.
*
*******************************************************************************/
int mv_cust_app_etype_ctrl(mv_cust_app_type_e app_type, uint16_t eth_type)
{
    int                       fd;
    mv_cust_ioctl_app_etype_t app_etype;

    fd = getCustDrvFd();
    if (fd < 0)
        return CUST_ERROR;

    app_etype.app_type = app_type;
    app_etype.eth_type = eth_type;
    
    osSemTake(custApiSemId, GL_SUSPEND);
    
    if (ioctl(fd, MV_CUST_IOCTL_APP_ETH_TYPE_SET, &app_etype))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(custApiSemId);
        return CUST_ERROR;
    }
    osSemGive(custApiSemId);

    return CUST_OK; 
}

/*******************************************************************************
* mv_cust_app_config_lookup()
*
* DESCRIPTION: The API lookup proper application config entry by application type.
*
* INPUTS:   
*           type        - Application type, such as IGMP, loopback detect.
*
* OUTPUTS:
*           None
*
* RETURNS:          On success, returns dedicated cust configuration entry. 
*                   On error, returns NULL.
*
*******************************************************************************/
mv_cust_app_config_t* mv_cust_app_config_lookup(mv_cust_app_type_e type)
{
    uint32_t index;

    for (index = 0; index < sizeof(gCustConfig)/sizeof(mv_cust_app_config_t); index++)
    {
        if (gCustConfig[index].app_type == type)
            return &gCustConfig[index];
    }
    
    return NULL;   
}

/*******************************************************************************
* mv_cust_app_etype_get()
*
* DESCRIPTION: The API gets the application Ethernet type defined in mv_cust
*
* INPUTS:   
*           app_type  - Application type, such as IGMP, lpbk detect
*
* OUTPUTS:
*           eth_type  - Ethernet type value, such as 0xfffa
*
* RETURNS:          On success, returns CUST_OK. 
*                   On error, returns CUST_ERROR.
*
*******************************************************************************/
int32_t mv_cust_app_etype_get(mv_cust_app_type_e app_type, uint16_t *eth_type)
{
    mv_cust_app_config_t *config;
    int32_t               cust_rc = CUST_OK;
    
    /* Find application config */
    config = mv_cust_app_config_lookup(app_type);
    if (config == NULL) {
        printf("%s: Can not find type[%d] in application config\n",
               __FUNCTION__, app_type);
        return CUST_ERROR;
    }

    *eth_type = config->eth_type;

    return cust_rc;
}

/*******************************************************************************
* mv_cust_app_etype_set()
*
* DESCRIPTION: The API sets the application Ethernet type defined in mv_cust
*
* INPUTS:   
*           app_type  - Application type, such as IGMP, lpbk detect
*           eth_type  - Ethernet type value, such as 0xfffa

*
* OUTPUTS:
*           None 
*
* RETURNS:          On success, returns CUST_OK. 
*                   On error, returns CUST_ERROR.
*
*******************************************************************************/
int32_t mv_cust_app_etype_set(mv_cust_app_type_e app_type, uint16_t eth_type)
{
    mv_cust_app_config_t *config;
    int32_t               cust_rc = CUST_OK;
    
    /* Set local application ethernet type */
    config = mv_cust_app_config_lookup(app_type);
    if (config == NULL) {
        printf("%s: Can not find type[%d] in application config\n",
               __FUNCTION__, app_type);
        return CUST_ERROR;
    }

    if (config->eth_type == eth_type)
        return cust_rc;
    
    if (gCustSock[config->app_type][MV_CUST_FLOW_DIR_US] >= 0)
    {
        close(gCustSock[config->app_type][MV_CUST_FLOW_DIR_US]);
        gCustSock[config->app_type][MV_CUST_FLOW_DIR_US] = -1;
    }
    
    if (gCustSock[config->app_type][MV_CUST_FLOW_DIR_DS] >= 0)
    {
        close(gCustSock[config->app_type][MV_CUST_FLOW_DIR_DS]);
        gCustSock[config->app_type][MV_CUST_FLOW_DIR_DS] = -1;
    }

    config->eth_type = eth_type;

    /* Set kernel application ethernet type */
    if ( CUST_OK != mv_cust_app_etype_ctrl(app_type, eth_type))
    {
        printf("%s: fail to set app_type[%d] eth_type[%d]\n",
               __FUNCTION__, app_type, eth_type);
        return CUST_ERROR;
    }       

    return cust_rc;
}

/*******************************************************************************
* mv_cust_app_rx_parse()
*
* DESCRIPTION: The API receives application frames from either LAN or WAN side,
*              it will be blocked until a packet arrived.
*
* INPUTS:   
*           buf         - Application frame buffer allocated by user
*           len         - The length of the frame
*
* OUTPUTS:
*           dir         - Direction of the frame, either upstream or downstream
*           uni_port    - source UNI port in case this frame comes from LAN side
*
* RETURNS:          On success, returns CUST_OK. 
*                   On error, returns CUST_ERROR.
*
*******************************************************************************/
int32_t mv_cust_app_rx_parse(uint8_t  *buf,
        uint32_t           *len,
        mv_cust_flow_dir_e *dir,
        uint32_t           *uni_port
)
{
    tpm_src_port_type_t tpm_src_port;
    uint32_t            switch_port;
    tpm_error_code_t    tpm_rc  = TPM_RC_OK;
    int32_t             cust_rc = CUST_OK;

    if (*len <= MV_CUST_MH_LEN)
    {
        printf("%s: the length of input frame[%d] is too short \n\r", __FUNCTION__, len);
        return CUST_ERROR;
    }
    else {

        /* Set by mv_cust Rx of IGMP, loopback detect, etc. */
        if (MV_CUST_PONMAC_INDEX  == buf[0]) {
            *dir = MV_CUST_FLOW_DIR_DS;
            *uni_port = 0; /* The UNI port has no meaning in D/S, just fill it with 0 */
        } 
        else
        {
            *dir = MV_CUST_FLOW_DIR_US;

            /* Get switch port */
            switch_port = buf[1];

            /* Convert switch port to TPM source port */
            tpm_rc = tpm_phy_convert_port_index(0, switch_port, &tpm_src_port);
            if (tpm_rc != TPM_RC_OK)
            {
                printf("%s: failed to convert switch port[%d] to TPM port, rc[%d]\n\r", __FUNCTION__, switch_port, tpm_rc);
                cust_rc = CUST_ERROR;
            }

            /* Convert TPM port to application UNI port,
               Currently calculate it directly, will replace it with unified index API in future 
            */
            *uni_port = tpm_src_port + 1;
        }
    }

    /* Adjust the length and remove the MH */
    *len -= MV_CUST_MH_LEN;
    memmove(buf, buf + MV_CUST_MH_LEN, *len);
    
    return cust_rc;
}


/*******************************************************************************
* mv_cust_app_tx()
*
* DESCRIPTION: The API modifies the marvell header of the frame, so that it could be
*               sent to required target UNI ports
*
* INPUTS:   
*           type            - Application type, such as IGMP, lpbk detect
*           dir             - Direction of the frame, either upstream or downstream
*           uni_port_bitmap - Target UNI port bitmap in case this frame is to be sent to LAN side
*           buf             - Application frame buffer allocated by user
*           len             - The length of the frame
*
* OUTPUTS:
*           NONE
*
* RETURNS:          On success, returns CUST_OK. 
*                   On error, returns CUST_ERROR.
*
*******************************************************************************/
int32_t mv_cust_app_tx(mv_cust_app_type_e type,
        mv_cust_flow_dir_e  dir,
        uint32_t            uni_port_bitmap,
        uint8_t            *buf, 
        uint32_t            len)
{
    int32_t               length;
    mv_cust_app_config_t *config;
    static uint8_t        temp_buf[MV_CUST_MAX_FRAME_SIZE];
    struct ifreq          ifReq_ld;
    struct sockaddr_ll    sockAddr_ll;
    uint32_t              uni_port;
    tpm_src_port_type_t   tpm_src_port;
    uint32_t              switch_port;
    uint32_t              port_vlan = 0; 
    int32_t              *p_socket= NULL;
    tpm_error_code_t      tpm_rc = TPM_RC_OK;

    if (type >= MV_CUST_APP_TYPE_MAX) {
        printf("%s: type[%d] is illegal \n",
               __FUNCTION__, type);
        return CUST_ERROR;
    }

    /* Find application config */
    config = mv_cust_app_config_lookup(type);
    if (config == NULL) {
        printf("%s: Can not find type[%d] in application config\n",
               __FUNCTION__, type);
        return CUST_ERROR;
    }

    if (dir == MV_CUST_FLOW_DIR_US) {

        p_socket = &gCustSock[type][MV_CUST_FLOW_DIR_US];
        /* Create a socket for the first time */    
        if (*p_socket < 0) {
           
            *p_socket = socket(AF_PACKET, SOCK_RAW, htons(config->eth_type));
            if (*p_socket < 0) {
                printf("%s: sock_create(AF_PACKET, SOCK_RAW, htons(0x%x)) failed.\n",
                       __FUNCTION__, config->eth_type);
                return CUST_ERROR;
            }
        }        

        /* Set interface for transmitting */
        /* The binding to pon0 need to improved */
        strncpy(ifReq_ld.ifr_name, "pon0", sizeof(ifReq_ld.ifr_name));
        if (ioctl(*p_socket, SIOCGIFINDEX, &ifReq_ld) < 0) 
        {
            printf("%s: ioctl(sockfd,SIOCGIFINDEX,&ifReq) failed \n", 
                   __FUNCTION__);
            return CUST_ERROR;
        }
        
        /* Set transmitting parameters */
        memset(&sockAddr_ll, 0, sizeof(sockAddr_ll));
        sockAddr_ll.sll_ifindex  = ifReq_ld.ifr_ifindex;
        sockAddr_ll.sll_family   = AF_PACKET;
        sockAddr_ll.sll_halen    = 6;
        sockAddr_ll.sll_protocol = htons(config->eth_type);

        if (bind(*p_socket, (struct sockaddr *)&sockAddr_ll, sizeof(sockAddr_ll)) < 0)
        {
            printf("%s: bind(sockfd,&sockAddress_ld,sizeof(sockAddress_ld)) failed.\n", 
                   __FUNCTION__);
            return CUST_ERROR;
        }        

        length  = sendto(*p_socket, 
                         buf, 
                         len, 
                         0, 
                         (struct sockaddr *) &sockAddr_ll, 
                         sizeof(struct sockaddr_ll));
    } 
    else {
        
        p_socket = &gCustSock[type][MV_CUST_FLOW_DIR_DS];

        /* Create a socket for the first time */    
        if (*p_socket < 0) {
           
            *p_socket = socket(AF_PACKET, SOCK_RAW, htons(config->eth_type));
            if (*p_socket < 0) {
                printf("%s: sock_create(AF_PACKET, SOCK_RAW, htons(0x%x)) failed.\n",
                       __FUNCTION__, config->eth_type);
                return CUST_ERROR;
            }
        }        

        /* Set interface for transmitting */
        /* The binding to eth0 need to improved */
        strncpy(ifReq_ld.ifr_name, "eth0", sizeof(ifReq_ld.ifr_name));
        if (ioctl(*p_socket, SIOCGIFINDEX, &ifReq_ld) < 0) 
        {
            printf("%s: ioctl(sockfd,SIOCGIFINDEX,&ifReq) failed \n", 
                   __FUNCTION__);
            return CUST_ERROR;
        }

        /* Set transmitting parameters */
        memset(&sockAddr_ll, 0, sizeof(sockAddr_ll));
        sockAddr_ll.sll_ifindex  = ifReq_ld.ifr_ifindex;
        sockAddr_ll.sll_family   = AF_PACKET;
        sockAddr_ll.sll_halen    = 6;
        sockAddr_ll.sll_protocol = htons(config->eth_type);
        if (bind(*p_socket, (struct sockaddr *)&sockAddr_ll, sizeof(sockAddr_ll)) < 0)
        {
            printf("%s: bind(sockfd,&sockAddress_ld,sizeof(sockAddress_ld)) failed.\n", 
                   __FUNCTION__);
            return CUST_ERROR;
        }    

        for (uni_port = 1; uni_port <= MV_CUST_UNI_PORT_NUM; uni_port++)
        {
            if (uni_port_bitmap & (1 << (uni_port -1) ))
            {
                /* Convert application UNI port to TPM port,
                   Currently calculate it directly, will replace it with unified index API in future 
                */
                tpm_src_port = uni_port - 1;

                /* Convert TPM port to switch port */
                tpm_rc = tpm_xlate_uni_2_switch_port(0, tpm_src_port, &switch_port);
                if (tpm_rc != TPM_RC_OK)
                {
                    printf("%s: failed to convert TPM port[%d] to switch port, rc[%d]\n\r", __FUNCTION__, tpm_src_port, tpm_rc);
                    continue;
                }

                port_vlan |= (1 << (switch_port & 0x07)); /* Currently only has upto 7 switch port*/
            } 
        }

        /* Set the port VLAN of mavell header */
        memset(temp_buf, 0, sizeof(temp_buf));
        temp_buf[0] = dir;
        temp_buf[1] = port_vlan;
        
        memcpy(&temp_buf[2], buf, len);
        
        length  = sendto(*p_socket, 
                         temp_buf, 
                         len + 2, 
                         0, 
                         (struct sockaddr *) &sockAddr_ll, 
                         sizeof(struct sockaddr_ll));
    }

    return CUST_OK;
}
