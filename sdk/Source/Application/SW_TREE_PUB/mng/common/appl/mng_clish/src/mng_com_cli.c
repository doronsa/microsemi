/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/



#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"
#include "globals.h"
#include "ponOnuMngIf.h"
#include "errorCode.h"
#include "mng_trace.h"
#include "params_mng.h"
#include "mv_os_timer_ext.h"

/********************************************************************************/
/*                          System Trace CLI functions                          */
/********************************************************************************/
bool_t mng_com_cli_trace_print_status (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    mngTracePrintModuleStatus();
	return BOOL_TRUE;
}

static UINT32   mng_com_get_mng_level (UINT32   level)
{
    UINT32  mng_level;

    switch (level) 
    {
    case 0:
    case 2:
    case 3:
        mng_level = MNG_TRACE_INFO;
        break;

    case 1:
        mng_level = MNG_TRACE_DEBUG;
        break;

    case 4:
    case 5:
        mng_level = MNG_TRACE_ERROR;
        break;
    }

    return mng_level;
}

static  UINT32  all_submodules = 99;
static  UINT32  inv_submodule  = 100;

static UINT32   mng_com_get_mng_sub_module (UINT32   module,
                                            UINT32   sub_module)
{
    UINT32  mng_sub_module;

    switch (sub_module) 
    {
    case 0:
        mng_sub_module = 0;
        break;

    case 1:
    case 2:
    case 3:    
        if (module != MNG_TRACE_MODULE_OMCI)
        {
            PRINT("Invalid submodule\n");
            return inv_submodule;
        }

        if (sub_module == 1)
            mng_sub_module = MNG_TRACE_MODULE_OMCI_BASE;
        else if (sub_module == 2)
            mng_sub_module = MNG_TRACE_MODULE_OMCI_DPA;
        else if (sub_module == 3)
            mng_sub_module = MNG_TRACE_MODULE_OMCI_TL;            
        break;

    case 4:
    case 5:
        if (module != MNG_TRACE_MODULE_OAM)
        {
            PRINT("Invalid submodule\n");
            return inv_submodule;
        }

        if (sub_module == 4)
            mng_sub_module = MNG_TRACE_MODULE_OAM_BASE;
        else
            mng_sub_module = MNG_TRACE_MODULE_OAM_DISC;
        break;

    case 6:
        /* All submodules for the specified module */
        mng_sub_module = all_submodules;
    }

    return mng_sub_module;
}

static E_ErrorCodes   mng_com_set_mng_level_all (UINT32   level)
{
    E_ErrorCodes    mng_rc;

    /* Specified trace level set for all modules */
    mng_rc = mngTraceChangeModuleStatus(MNG_TRACE_MODULE_COMMON, level, MNG_TRACE_COMMON_ATTRS);
    mng_rc |= mngTraceChangeModuleStatus(MNG_TRACE_MODULE_APM,   level, MNG_TRACE_APM_ATTRS);
    mng_rc |= mngTraceChangeModuleStatus(MNG_TRACE_MODULE_OMCI,  level, MNG_TRACE_OMCI_ATTRS);
    mng_rc |= mngTraceChangeModuleStatus(MNG_TRACE_MODULE_TPMA,  level, MNG_TRACE_TPMA_ATTRS);
    mng_rc |= mngTraceChangeModuleStatus(MNG_TRACE_MODULE_OAM,   level, MNG_TRACE_OAM_ATTRS);
    mng_rc |= mngTraceChangeModuleStatus(MNG_TRACE_MODULE_CTC,   level, MNG_TRACE_CTC_ATTRS);
    mng_rc |= mngTraceChangeModuleStatus(MNG_TRACE_MODULE_PON,   level, MNG_TRACE_PON_ATTRS);

    return mng_rc;
}

static UINT32   mng_com_get_all_sub_modules (UINT32   module)
{
    UINT32  sub_modules;

    switch (module) 
    {
    case MNG_TRACE_MODULE_COMMON:
    case MNG_TRACE_MODULE_APM:
    case MNG_TRACE_MODULE_TPMA:
    case MNG_TRACE_MODULE_CTC:
    case MNG_TRACE_MODULE_PON:
        sub_modules = 0;
        break;

    case MNG_TRACE_MODULE_OMCI:
        sub_modules = MNG_TRACE_OMCI_ATTRS;
        break;

    case MNG_TRACE_MODULE_OAM:
        sub_modules = MNG_TRACE_OAM_ATTRS;
        break;
    }

    return sub_modules;
}

static void   mng_com_remove_sub_module   (UINT32   module,
                                           UINT32   sub_module,
                                           UINT32   *mng_level,
                                           UINT32   *mng_sub_module)
{
    UINT32  tmp_level, tmp_sub_module;
    UINT32  curr_option, remove_option;

    mngTraceGetModuleStatus (module, &tmp_level, &tmp_sub_module);

    curr_option = (tmp_sub_module >> MNG_TRACE_MODULE_SHIFT) & MNG_TRACE_OPTIONS_MASK;
    remove_option = (sub_module >> MNG_TRACE_MODULE_SHIFT) & MNG_TRACE_OPTIONS_MASK;

    curr_option &= ~remove_option;
    *mng_sub_module = tmp_sub_module & (module | (curr_option << MNG_TRACE_MODULE_SHIFT));

    /* Current trace level should not be changed */
    *mng_level = tmp_level;
}

bool_t mng_com_cli_trace_change_status (const clish_shell_t    *instance,
                                        const lub_argv_t       *argv)
{
    bool_t          rc = BOOL_TRUE;
    E_ErrorCodes    mng_rc;
    UINT32          level, mng_level;
    UINT32          module;
    UINT32          sub_module, mng_sub_module;
    UINT32          tmp_level, tmp_sub_module;

    /* Get parameters */
    level   = atoi(lub_argv__get_arg(argv, 0));
    module  = atoi(lub_argv__get_arg(argv, 1));

    if (lub_argv__get_arg(argv, 2) == NULL) 
        sub_module = 0;
    else
        sub_module = atoi(lub_argv__get_arg(argv, 2));
 
    mng_level       = mng_com_get_mng_level(level);
    mng_sub_module  = mng_com_get_mng_sub_module(module, sub_module);

    if (mng_sub_module == inv_submodule) 
    {
        mng_rc = IAMBA_ERR_GENERAL;
        goto mngout;
    }

    if ((level == MNG_TRACE_NONE) && (module == MNG_TRACE_MODULE_NONE)) 
    {
        /* Disable MNG trace for all applications */
        mng_rc = mngTraceResetModuleStatus();
        goto mngout;
    }

    if ((level == MNG_TRACE_NONE) && (module != MNG_TRACE_MODULE_NONE)) 
    {
        /* Change trace status for a module/submodule */
        if (mng_sub_module != 0)
        {
           /* Remove submodule from the current trace */
            mng_com_remove_sub_module (module, mng_sub_module, &tmp_level, &tmp_sub_module);
            mng_sub_module = tmp_sub_module;

            /* Current trace level should not be changed */
            mng_level = tmp_level;
        }

        mng_rc = mngTraceChangeModuleStatus(module, mng_level, mng_sub_module);
        goto mngout;
    }

    if ((level != MNG_TRACE_NONE) && (module == MNG_TRACE_MODULE_NONE)) 
    {
        PRINT("Trace status cannot be changed for unknown module");
        mng_rc = IAMBA_ERR_GENERAL;
        goto mngout;
    }

    if ((level != MNG_TRACE_NONE) && (module != MNG_TRACE_MODULE_NONE)) 
    {
        if (module == MNG_TRACE_MODULE_LAST)
        {
            if (mng_sub_module != all_submodules) 
            {
                PRINT("Specified trace level is set for all modules and its submodules");
            }

            mng_rc = mng_com_set_mng_level_all(mng_level);
        }
        else
        {
            if (mng_sub_module == all_submodules) 
            {
               /* Change trace status for a module and all its submodules */
                mng_sub_module = mng_com_get_all_sub_modules(module);
            }
            else
            {
                /* Add submodule */
                mngTraceGetModuleStatus (module, &tmp_level, &tmp_sub_module);
                mng_sub_module |= tmp_sub_module;
            }
            mng_rc = mngTraceChangeModuleStatus(module, mng_level, mng_sub_module);
        }
    }

mngout:
    if (mng_rc != IAMBA_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to change trace status (rc=%d)\n", mng_rc);
    }
    else
    {
        PRINT("Trace status changed");
    }
   
	return rc;
}


bool_t mng_com_cli_trace_print_module_list (const clish_shell_t    *instance,
                                            const lub_argv_t       *argv)
{
    mngTracePrintModuleList();
	return BOOL_TRUE;
}

/********************************************************************************/
/*                                PON CLI functions                             */
/********************************************************************************/
bool_t mng_com_cli_set_pon_params (const clish_shell_t    *instance,
                                   const lub_argv_t       *argv)
{
    bool_t          rc = BOOL_TRUE;
    E_ErrorCodes    mng_rc;
    char            ser_num[US_XML_PON_SN_LEN+1];
    char            pssw[US_XML_PON_PASSWD_LEN+1];
    UINT32          dis;

    /* Get parameters */
    memcpy(&ser_num, lub_argv__get_arg(argv, 0), US_XML_PON_SN_LEN);
    memcpy(&pssw, lub_argv__get_arg(argv, 1), US_XML_PON_PASSWD_LEN);
    dis = atoi(lub_argv__get_arg(argv, 2));

    ser_num[US_XML_PON_SN_LEN]  = '\0';
    pssw[US_XML_PON_PASSWD_LEN] = '\0';

    DBG_PRINT("ser_num %s pssw %s dis %d", ser_num, pssw, dis);

    if (mvGponInit((UINT8*)ser_num, (UINT8*)pssw, dis, 0) == MRVL_OK)
        PRINT("PON parameters changed");
    else
        PRINT("Failed to change PON parameters");

    return BOOL_TRUE;
}


/********************************************************************************/
/*                      Timer functions                                         */
/********************************************************************************/
bool_t  mv_os_cli_timer_init (const clish_shell_t    *instance,
                           const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    mv_os_glue_error_code_t    tpm_rc = MV_OS_GLUE_GEN_ERR;


    printf("Yuval mv_os_cli_timer_init \n");

    tpm_rc = mv_os_glue_timer_init();
    if (tpm_rc != MV_OS_GLUE_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to Init Timer \n");
    }

    return rc;
}


void  mv_os_cli_timer_cb_demo (uint32_t timerCbParamA, uint32_t timerCbParamB)
{
    printf("mv_os_cli_timer_cb_demo callback paramA(%d), paramB(%d) \n", timerCbParamA, timerCbParamB);
}

bool_t  mv_os_cli_timer_create (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    mv_os_glue_error_code_t    tpm_rc = MV_OS_GLUE_GEN_ERR;
    const char          name[16+1];

    TIMER_ID       timerId;
    uint32_t	   timerCbParamA;
    uint32_t	   timerCbParamB;

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), 16);
    timerCbParamA  = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    timerCbParamB  = tpm_cli_get_number(lub_argv__get_arg(argv, 2));


    tpm_rc = mv_os_glue_timer_create(&timerId, &name, mv_os_cli_timer_cb_demo, timerCbParamA, timerCbParamB);
    if (tpm_rc != MV_OS_GLUE_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to Create timer \n");
    }
    printf("Returned TimerId(%d) \n", timerId);

    return rc;
}

bool_t  mv_os_cli_timer_start (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    mv_os_glue_error_code_t    tpm_rc = MV_OS_GLUE_GEN_ERR;

    TIMER_ID       timerId;
    uint32_t	   initialTime;
    uint32_t	   rescheduleTime;


    /* Get parameters */
    timerId        = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    initialTime    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    rescheduleTime = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    tpm_rc = mv_os_glue_timer_start(timerId, initialTime, rescheduleTime);
    if (tpm_rc != MV_OS_GLUE_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to Start timer \n");
    }

    return rc;
}


bool_t  mv_os_cli_timer_stop (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    mv_os_glue_error_code_t    tpm_rc = MV_OS_GLUE_GEN_ERR;

    TIMER_ID       timerId;


    /* Get parameters */
    timerId        = tpm_cli_get_number(lub_argv__get_arg(argv, 0));

    tpm_rc = mv_os_glue_timer_stop(timerId);
    if (tpm_rc != MV_OS_GLUE_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to Stop timer\n");
    }

    return rc;
}


bool_t  mv_os_cli_timer_del (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    mv_os_glue_error_code_t    tpm_rc = MV_OS_GLUE_GEN_ERR;

    TIMER_ID       timerId;


    /* Get parameters */
    timerId        = tpm_cli_get_number(lub_argv__get_arg(argv, 0));

    tpm_rc = mv_os_glue_timer_del(timerId);
    if (tpm_rc != MV_OS_GLUE_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to Delete timer \n");
    }

    return rc;
}


bool_t  mv_os_cli_timer_deb_act (const clish_shell_t    *instance,
                           const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    mv_os_glue_error_code_t    tpm_rc = MV_OS_GLUE_GEN_ERR;


    tpm_rc = mv_os_glue_timer_debug_act();
    if (tpm_rc != MV_OS_GLUE_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to Act Timer \n");
    }

    return rc;
}


bool_t  mv_os_cli_timer_deb_deact (const clish_shell_t    *instance,
                           const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    mv_os_glue_error_code_t    tpm_rc = MV_OS_GLUE_GEN_ERR;


    tpm_rc = mv_os_glue_timer_debug_deact();
    if (tpm_rc != MV_OS_GLUE_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to Act Timer \n");
    }

    return rc;
}


bool_t  mv_os_cli_timer_print_full (const clish_shell_t    *instance,
                           const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    mv_os_glue_error_code_t    tpm_rc = MV_OS_GLUE_GEN_ERR;


    printf("yuval something  Act Timer \n");
    tpm_rc = mv_os_glue_timer_print_full_tbl();
    if (tpm_rc != MV_OS_GLUE_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to Act Timer \n");
    }

    return rc;
}


bool_t  mv_os_cli_timer_print_valid (const clish_shell_t    *instance,
                           const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    mv_os_glue_error_code_t    tpm_rc = MV_OS_GLUE_GEN_ERR;


    tpm_rc = mv_os_glue_timer_print_valid_tbl();
    if (tpm_rc != MV_OS_GLUE_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to Act Timer \n");
    }

    return rc;
}


