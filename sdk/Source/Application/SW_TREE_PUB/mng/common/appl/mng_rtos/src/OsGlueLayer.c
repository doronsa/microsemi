/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************/
/**                                                                           */
/**  MODIFICATION HISTORY:                                                    */
/**                                                                           */
/**                                                                           */
/******************************************************************************/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/msg.h>





#include "globals.h"
#include "errorCode.h"
#include "OsGlueLayer.h"
#include "CommonQprintDefines.h"

static struct timeval omciStartTime;



/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*                          Task Section                                       */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/

/******************************************************************************
 *
 * Function   : osTaskCreate
 *              
 * Description: This function creates a thread.
 *              Linux: name, argc, priority and stackSize are unused
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osTaskCreate     (GL_TASK_ID     *task, 
                               char           *name, 
                               GLFUNCPTR      taskEntry,
                               unsigned long  argc,
                               void           *argv,
                               unsigned char  priority,
                               unsigned long  stackSize)
{
    pthread_attr_t     attr;
    pthread_attr_t     *pattr = &attr;
    struct sched_param schedParam;
    struct sched_param *pSchedParam = &schedParam;
    int                rc;

    pthread_attr_init(pattr);
    pthread_attr_setdetachstate(pattr, PTHREAD_CREATE_JOINABLE);
    pthread_attr_setinheritsched(pattr, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(pattr, SCHED_RR); 
    pSchedParam->sched_priority = priority;
    pthread_attr_setschedparam(pattr, pSchedParam);
    pthread_attr_setstacksize(pattr, stackSize);

    if ((rc = pthread_create(task, pattr, taskEntry, argv)) == 0)
    {
        return IAMBA_OK;
    }
    else
    {
        perror("osTaskCreate, pthread_create failed\n\r");
        return ERR_TASK_CREATE;
    }
}



/******************************************************************************
 *
 * Function   : osTaskDelay
 *              
 * Description: This function delays the task - uses nanosleep()
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osTaskDelay(unsigned int milliseconds)
{
    struct timespec tmReq;

    tmReq.tv_sec = (time_t)(milliseconds / 1000);
    tmReq.tv_nsec = (milliseconds % 1000) * 1000 * 1000;

    // we're not interested in remaining time nor in return value
    (void)nanosleep(&tmReq, NULL);

    return IAMBA_OK;
}

/******************************************************************************
 *
 * Function   : osTaskDelay
 *              
 * Description: This function delays the task - uses nanosleep()
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osTaskNanoDelay(unsigned int nanoseconds)
{
    struct timespec tmReq;

    tmReq.tv_sec  = 0;
    tmReq.tv_nsec = nanoseconds;

    // we're not interested in remaining time nor in return value
    (void)nanosleep(&tmReq, NULL);

    return IAMBA_OK;
}


/******************************************************************************
 *
 * Function   : osTaskSelfIdGet
 *              
 * Description: This function returns the current task's Id
 *
 * Parameters : 
 *              
 * Returns    : Task Id
 *              
 ******************************************************************************/

GL_TASK_ID osTaskSelfIdGet  ()
{
    return(GL_TASK_ID)pthread_self();
}




/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*                          Message Queue Section                              */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/


/******************************************************************************
 *
 * Function   : osMsgQCreate
 *              
 * Description: This function creates/opens a message queue
 *
 * Parameters : name must start with /. 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/
#define PMODE 0666

E_ErrorCodes osMsgQCreate   (GL_QUEUE_ID   *msgQ_p, 
                             char          *name, 
                             unsigned long numEntries,
                             unsigned long messageSize)
{
    mqd_t  mqd;
    int    oflag = O_RDWR | O_CREAT | O_EXCL;
    struct mq_attr attr;
    struct mq_attr *pmqattr = &attr;

    // Fill in attributes for message queue
    pmqattr->mq_maxmsg  = numEntries;
    pmqattr->mq_msgsize = messageSize;
    pmqattr->mq_flags   = 0;

    if ((mqd = mq_open(name, oflag, PMODE, pmqattr)) == (mqd_t)-1)
    {
        if (errno == EEXIST) 
        {
            mq_unlink(name);
            mqd = mq_open(name, oflag, PMODE, pmqattr);
        }

        if (mqd == ((mqd_t)-1))
        {
            perror("osMsgQCreate, mq_open failed\n\r");
            return ERR_QUEUE_CREATE;
        }
    }

    *msgQ_p = (GL_QUEUE_ID)mqd;
    return IAMBA_OK;
}



/******************************************************************************
 *
 * Function   : osMsgQGetReference
 *              
 * Description: This function gets reference to a message queue
 *
 * Parameters : name must start with /. 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osMsgQGetReference(GL_QUEUE_ID   *msgQ_p, 
                                char          *name) 
{
    mqd_t  mqd;
    int    oflag = O_RDWR;

    if ((mqd = mq_open(name, oflag)) != (mqd_t)-1)
    {
        *msgQ_p = (GL_QUEUE_ID)mqd;
        return IAMBA_OK;
    }
    else
    {
        printf("osMsgQGetReference, mq_open %s failed\n\r",name);
        return ERR_QUEUE_CREATE;
    }
}



/******************************************************************************
 *
 * Function   : osMsgQDelete
 *              
 * Description: This function deletes a message queue
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osMsgQDelete      (GL_QUEUE_ID   msgQ)
{
    if (!mq_close((mqd_t)msgQ))
    {
        return IAMBA_OK;
    }
    else
    {
        return IAMBA_ERR_GENERAL;
    }
}



/******************************************************************************
 *
 * Function   : osMsgQSend
 *              
 * Description: This function sends a message to a queue
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osMsgQSend        (GL_QUEUE_ID   msgQ, 
                                void          *message, 
                                unsigned int  size, 
                                unsigned int  priority)
{
    if (mq_send((mqd_t)msgQ, message, size, priority) == 0)
    {
        return IAMBA_OK;
    }
    else
    {
        return ERR_MSG_Q_CONTROL;
    }
}



/******************************************************************************
 *
 * Function   : osMsgQReceive
 *              
 * Description: This function receives a message from a queue
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osMsgQReceive     (GL_QUEUE_ID   msgQ, 
                                void          *message, 
                                unsigned int  size, 
                                unsigned int  suspend,
                                unsigned int  *msgPriority)
{
    int rcvCount; 

    if ((rcvCount = mq_receive((mqd_t)msgQ, message, size, msgPriority)) != -1)
    {
        return IAMBA_OK;
    }
    else
    {
        if (errno == EAGAIN)
        {
            perror("osMsgQReceive failed, empty queue\n\r");
            return ERR_MSG_Q_EMPTY;
        }
        else
        {
            perror("osMsgQReceive failed\n\r");
            return ERR_MSG_Q_CONTROL;
        }
    }
}





/******************************************************************************
 *
 * Function   : osMsgQTimedReceive
 *              
 * Description: This function receives a message from a queue
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osMsgQTimedReceive(GL_QUEUE_ID     msgQ, 
                                void            *message, 
                                unsigned int    size, 
                                struct timespec *pTimeoutTime,
                                unsigned int    *msgPriority)
{
    int myErrno;

    if (mq_timedreceive((mqd_t)msgQ, message, size, msgPriority, pTimeoutTime) != -1)
    {
        return IAMBA_OK;
    }
    else 
    {
        myErrno = errno;

        if (myErrno == ETIMEDOUT)
        {
            return ERR_MSG_Q_TIMEOUT;
        }
        else
        {
            char buf[120];
            char tvbuf[100] = {0};

            if (myErrno == EINVAL)
            {
                printf("tv_sec:tv_nsec %d:%d\n\r", pTimeoutTime->tv_sec, pTimeoutTime->tv_nsec);
            }
            strerror_r(myErrno, buf, sizeof(buf));
            printf("%s: myErrno %d - %s. %s (RTC battery?)\n\r", __FUNCTION__, myErrno, buf, tvbuf);
            return IAMBA_ERR_GENERAL;
        }
    }
}





/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*                          Semaphore Section                                  */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/

static INT32 osSrvLinuxRand()
{
    usleep(1000*1000);
    
    srand((UINT32)time(NULL));
    
    return rand();
}


/******************************************************************************
 *
 * Function   : osSemAllCreate
 *              
 * Description: This function creates a named semaphore - sem_open
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osSemCreate    (GL_SEMAPHORE_ID *sem, 
                             char            *name, 
                             unsigned int    initialCount, 
                             unsigned long   suspendType,
                             bool            binarySem)
{
#ifdef US_GLIBC
    int oflag = O_CREAT | O_EXCL;

    if ((*sem = (GL_SEMAPHORE_ID)sem_open(name, oflag, S_IRWXU | S_IRWXG, 1)) == SEM_FAILED)
    {
        if (errno == EEXIST) 
        {
            sem_unlink(name);

           *sem = (GL_SEMAPHORE_ID)sem_open(name, oflag, S_IRWXU | S_IRWXG, 1);
        }

        if (*sem == SEM_FAILED) 
        {
            qprintf(QPRCOM_ERROR, QPRCOMMOD_SUB1, "SEM_OPEN error: %s\n", strerror(errno));                                                   
            return ERR_SEM_CREATE;
        }
    }
    return IAMBA_OK;
#else
    /* To be implemented by customer */
    STATUS      l_ret_val       = IAMBA_OK;
    UINT16      a_init_value    = 1;
    SEM_ID_T    *sem_id         = (SEM_ID_T *)sem;

    *sem_id = semget(IPC_PRIVATE, 1, 0666);
    if(*sem_id == -1)
    {
        if(errno == EEXIST)
        {
            /* Also, get the semaphore's handle: */
            qprintf(QPRCOM_ERROR, QPRCOMMOD_SUB1, "Failed to create existing semaphore handle. Error: %d - '%s'\n", errno, strerror(errno));

            l_ret_val = ERR_SEM_CREATE;//OAM_ERROR_EXIT;
        }
        else
        {
            /* Some other error occured: */
            qprintf(QPRCOM_ERROR, QPRCOMMOD_SUB1, "Failed to create semaphore. Error: %d - '%s'\n", errno, strerror(errno));

            l_ret_val = ERR_SEM_CREATE;
        }
    }
    else
    {
        if(semctl(*sem_id, 0, SETVAL, a_init_value) == -1)
        {
            qprintf(QPRCOM_ERROR, QPRCOMMOD_SUB1, "Failed to set semaphore value. Error: %d - '%s'\n", errno, strerror(errno));

            l_ret_val = ERR_SEM_CREATE;
        }
        else
            l_ret_val = IAMBA_OK;

    }

    (*sem_id) = (l_ret_val == IAMBA_OK)?(*sem_id): ERROR;
    return l_ret_val;


#endif    
}


/******************************************************************************
 *
 * Function   : osSemDelete
 *              
 * Description: This function delelets a named semaphore
 *
 * Parameters : 
 * 		name_sem: for glibc, the input parameter name_sem is "name" of sem, while it is sem id for uclibc.
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osSemDelete    (void *name_sem)
{
#ifdef US_GLIBC
    char *name = (char *)name_sem;
    if (!sem_unlink(name))
    {
        return IAMBA_OK;
    }
    else
    {
        // analyse errno
        // int myErr = errno;
        return ERR_SEM_NOT_VALID;
    }
#else
    /* To be implemented by customer */
    STATUS      l_ret_val   = IAMBA_OK;
    SEM_ID_T    sem_id      = (SEM_ID_T)name_sem;
        
    if(semctl(sem_id, 0, IPC_RMID) == -1)
    {
        qprintf(QPRCOM_ERROR, QPRCOMMOD_SUB1, "Failed to delete semaphore. Error: %d - '%s'\n", errno, strerror(errno));

        l_ret_val = ERR_SEM_FREE;
    }
    else
    {
        l_ret_val = IAMBA_OK;
    }

    return l_ret_val;
#endif
}


/******************************************************************************
 *
 * Function   : osSemTake
 *              
 * Description: This function takes a semaphore - dos a sem_wait()
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osSemTake      (GL_SEMAPHORE_ID sem, unsigned int suspendType)
{
#ifdef US_GLIBC
    if (!sem_wait(sem))
    {
        return IAMBA_OK;
    }
    else
    {
        return ERR_SEM_NOT_VALID;
    }
#else
    /* To be implemented by customer */
    STATUS          l_ret_val   = IAMBA_OK;
    struct sembuf   l_sem_oper;
    SEM_ID_T        sem_id      = (SEM_ID_T)sem;

    l_sem_oper.sem_num = 0;
    l_sem_oper.sem_op  = -1;
    l_sem_oper.sem_flg = SEM_UNDO;

    if (semop(sem_id, &l_sem_oper, 1) == -1)
    {
        if(errno == EAGAIN)
        {
            l_ret_val = ERR_SEM_TIMEOUT;
        }
        else
        {
            qprintf(QPRCOM_ERROR, QPRCOMMOD_SUB1, "Failed to lock semaphore. Error: %d - '%s'\n", errno, strerror(errno));

            l_ret_val = ERR_SEM_OCCUPY;
        }
    }
    else
    {
        l_ret_val = IAMBA_OK;
    }
    
    return l_ret_val;
#endif
}


/******************************************************************************
 *
 * Function   : osSemTimedTake
 *              
 * Description: This function takes a semaphore - dos a sem_wait()
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osSemTimedTake      (GL_SEMAPHORE_ID sem, int mswait)
{
#ifdef US_GLIBC
    if(mswait < 0)
    {
        if (!sem_wait(sem))
        {
            return IAMBA_OK;
        }
        else
        {
            return ERR_SEM_NOT_VALID;
        }
    }
    else
    {
        struct timespec ts;

        clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_sec += (mswait / 1000);
        ts.tv_nsec += (mswait % 1000) * 1000;
        
        if (!sem_timedwait(sem, &ts))
        {
            return IAMBA_OK;
        }
        else
        {
            return ERR_SEM_NOT_VALID;
        }
    }
            
#elif defined(US_GNUEABI_UCLIBC)
    /* To be implemented by customer */
    STATUS          l_ret_val   = IAMBA_OK;
    struct sembuf   l_sem_oper;
    SEM_ID_T        sem_id      = (SEM_ID_T)sem;
    struct timespec ts;

    l_sem_oper.sem_num = 0;
    l_sem_oper.sem_op  = -1;
    l_sem_oper.sem_flg = SEM_UNDO;

	clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += (mswait / 1000);
    ts.tv_nsec += (mswait % 1000) * 1000;

    if (semtimedop(sem_id, &l_sem_oper, 1, &ts) == -1)
    {
        if(errno == EAGAIN)
        {
            l_ret_val = ERR_SEM_TIMEOUT;
        }
        else
        {
            qprintf(QPRCOM_ERROR, QPRCOMMOD_SUB1, "Failed to lock semaphore. Error: %d - '%s'\n", errno, strerror(errno));

            l_ret_val = ERR_SEM_OCCUPY;
        }
    }
    else
    {
        l_ret_val = IAMBA_OK;
    }
    
    return l_ret_val;
#else
    /* To be implemented by customer */
    STATUS          l_ret_val   = IAMBA_OK;
    struct sembuf   l_sem_oper;
    SEM_ID_T        sem_id      = (SEM_ID_T)sem;

    l_sem_oper.sem_num = 0;
    l_sem_oper.sem_op  = -1;
    l_sem_oper.sem_flg = SEM_UNDO;

    if (semop(sem_id, &l_sem_oper, 1) == -1)
    {
        if(errno == EAGAIN)
        {
            l_ret_val = ERR_SEM_TIMEOUT;
        }
        else
        {
            qprintf(QPRCOM_ERROR, QPRCOMMOD_SUB1, "Failed to lock semaphore. Error: %d - '%s'\n", errno, strerror(errno));

            l_ret_val = ERR_SEM_OCCUPY;
        }
    }
    else
    {
        l_ret_val = IAMBA_OK;
    }
    
    return l_ret_val;
#endif
}


/******************************************************************************
 *
 * Function   : osSemGive
 *              
 * Description: This function posts to the semaphore - sem_give
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osSemGive      (GL_SEMAPHORE_ID sem)
{
#ifdef US_GLIBC
    if (!sem_post(sem))
    {
        return IAMBA_OK;
    }
    else
    {
        return ERR_SEM_NOT_VALID;
    }
#else
    /* To be implemented by customer */
    STATUS          l_ret_val   = IAMBA_OK;
    struct sembuf   l_sem_oper;
    SEM_ID_T        sem_id      = (SEM_ID_T)sem;

    l_sem_oper.sem_num = 0;
    l_sem_oper.sem_op  = 1;
    l_sem_oper.sem_flg = SEM_UNDO;

    if(semop(sem_id, &l_sem_oper, 1) == -1)
    {
        qprintf(QPRCOM_ERROR, QPRCOMMOD_SUB1, "Failed to unlock semaphore. Error: %d - '%s'\n", errno, strerror(errno));

        l_ret_val = ERR_SEM_FREE;
    }
    else
    {
        l_ret_val = IAMBA_OK;
    }

    return l_ret_val;
#endif
}



/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*                         POSIX Mutex Section                                 */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/

/******************************************************************************
 *
 * Function   : osMutexInit
 *              
 * Description: This function initializes a mutex
 *
 * Parameters : 
 *              
 * Returns    : void
 *              
 ******************************************************************************/

E_ErrorCodes osMutexInit   (GL_MUTEX_ID mutex, GL_MUTEX_ATTR *p_attr)
{
    if (!pthread_mutex_init(mutex, p_attr))
    {
        return IAMBA_OK;
    }
    else
    {
        // Need to examine errno
        // int myErr = errno;
        return ERR_MUTEX_EAGAIN;
    }
}


/******************************************************************************
 *
 * Function   : osMutexDestroy
 *              
 * Description: This function destroys a mutex
 *
 * Parameters : 
 *              
 * Returns    : void
 *              
 ******************************************************************************/

E_ErrorCodes osMutexDestroy(GL_MUTEX_ID mutex)
{
    if (!pthread_mutex_destroy(mutex))
    {
        return IAMBA_OK;
    }
    else
    {
        // Need to examine errno
        // int myErr = errno;
        return ERR_MUTEX_EAGAIN;
    }
}


/******************************************************************************
 *
 * Function   : osMutexLock
 *              
 * Description: This function locks a mutex
 *
 * Parameters : 
 *              
 * Returns    : void
 *              
 ******************************************************************************/

E_ErrorCodes osMutexLock   (GL_MUTEX_ID mutex)
{
    if (!pthread_mutex_lock(mutex))
    {
        return IAMBA_OK;
    }
    else
    {
        // Need to examine errno
        return ERR_MUTEX_EAGAIN;
    }
}


/******************************************************************************
 *
 * Function   : osMutexTrylock
 *              
 * Description: This function tries to lock a mutex
 *
 * Parameters : 
 *              
 * Returns    : void
 *              
 ******************************************************************************/

E_ErrorCodes osMutexTrylock(GL_MUTEX_ID mutex)
{
    if (!pthread_mutex_trylock(mutex))
    {
        return IAMBA_OK;
    }
    else
    {
        // Need to examine errno
        // int myErr = errno;
        return ERR_MUTEX_EAGAIN;
    }
}


/******************************************************************************
 *
 * Function   : osMutexUnlock
 *              
 * Description: This function unlocks a mutex
 *
 * Parameters : 
 *              
 * Returns    : void
 *              
 ******************************************************************************/

E_ErrorCodes osMutexUnlock (GL_MUTEX_ID mutex)
{
    if (!pthread_mutex_unlock(mutex))
    {
        return IAMBA_OK;
    }
    else
    {
        // Need to examine errno
        // int myErr = errno;
        return ERR_MUTEX_EAGAIN;
    }
}



/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*                          Timer Section                                      */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
typedef struct 
{
    int    signo;
    UINT32 userData;
} SignoItem_S;

SignoItem_S signoItemPoolAra[8];
int signoItemPool_size = sizeof(signoItemPoolAra)/sizeof(signoItemPoolAra[0]);

#define BASE_OMCISIGNO   (SIGRTMIN+4)
/******************************************************************************
 *
 * Function   : initOmciTimerSignoPool
 *              
 * Description: This function initialize the OMCI signo pool
 *
 * Returns    : void
 *              
 ******************************************************************************/

void initOmciTimerSignoPool()
{
    int indx;

    for (indx = 0; indx < signoItemPool_size; indx++) 
    {
        signoItemPoolAra[indx].userData = 0;
        signoItemPoolAra[indx].signo    = BASE_OMCISIGNO - indx;
    }
}


/******************************************************************************
 *
 * Function   : allocateOmciTimerSigno
 *              
 * Description: This function allocates a signal number for timer
 *
 * Returns    : signal number or 0 (no signal number is available)
 *              
 ******************************************************************************/

int allocateOmciTimerSigno(UINT32 userData)
{
    int indx;

    for (indx = 0; indx < signoItemPool_size; indx++) 
    {
        if (signoItemPoolAra[indx].userData == 0)
        {
            signoItemPoolAra[indx].userData = userData;
            return signoItemPoolAra[indx].signo;
        }
    }
    return 0;
}


/******************************************************************************
 *
 * Function   : freeOmciTimerSigno
 *              
 * Description: This function frees a signal number (used for timer)
 *
 * Returns    : void
 *              
 ******************************************************************************/

void freeOmciTimerSigno(UINT32 userData)
{
    int indx;

    for (indx = 0; indx < signoItemPool_size; indx++) 
    {
        if (signoItemPoolAra[indx].userData == userData)
        {
            signoItemPoolAra[indx].userData = 0;
            return;
        }
    }

    perror("freeOmciTimerSigno failed\n\r");
}


/******************************************************************************
 *
 * Function   : osTimerCreate
 *              
 * Description: This function creates a timer
 *
 * Parameters : initialTime and rescheduleTime are specified in milliseconds
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osTimerCreate (GL_TIMER_ID     *timerId, 
                            char            *name, 
                            fGlTimerRoutine timerRoutine, 
                            UINT32          userData, 
                            unsigned long   initialTime, 
                            unsigned long   rescheduleTime,
                            int             timerParam)
{
    struct sigaction  sigact;
    struct sigevent   sigev;
    struct itimerspec itval;
    struct itimerspec oitval;

    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags     = SA_SIGINFO;
    sigact.sa_sigaction = timerRoutine;

    /* Set up sigaction to catch signal */
    if (sigaction(timerParam, &sigact, NULL) != 0)
    {
        perror("osTimerCreate, sigaction failed\n\r");
        return ERR_TIMER_CREATE;
    }

    /* Create the POSIX tmer to generate signo */
    sigev.sigev_notify          = SIGEV_SIGNAL;
    sigev.sigev_signo           = timerParam;
    sigev.sigev_value.sival_int = userData;

    if (timer_create(CLOCK_REALTIME, &sigev, timerId) == 0)
    {
        itval.it_value.tv_sec  = initialTime / 1000;
        itval.it_value.tv_nsec = (long)(initialTime % 1000) * (1000000L);

        if (rescheduleTime != 0)
        {
            itval.it_interval.tv_sec  = rescheduleTime / 1000;
            itval.it_interval.tv_nsec = (long)(rescheduleTime % 1000) * (1000000L);
        }
        else
        {
            itval.it_interval.tv_sec  = 0;
            itval.it_interval.tv_nsec = 0;
        }

        if (timer_settime(*timerId, 0, &itval, &oitval) != 0)
        {
            perror("osTimerCreate, timer_settime failed\n\r");
            return ERR_TIMER_CREATE;
        }
    }
    else
    {
        perror("osTimerCreate, timer_create failed\n\r");
        return ERR_TIMER_CREATE;
    }

    return IAMBA_OK;
}



/******************************************************************************
 *
 * Function   : omciOsTimerCreate
 *              
 * Description: This function allocates a sigo and creates a timer
 *
 * Parameters : initialTime and rescheduleTime are specified in milliseconds
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes omciTimerCreate (GL_TIMER_ID     *timerId, 
                              char            *name, 
                              fGlTimerRoutine timerRoutine, 
                              UINT32          userData, 
                              unsigned long   initialTime, 
                              unsigned long   rescheduleTime,
                              unsigned long   enable)
{
    int timerParam; 

    if ((timerParam = allocateOmciTimerSigno(userData)) == 0)
    {
        perror("omciTimerCreate, failed to allocate omci timer signal number\n\r");
        return ERR_TIMER_CREATE;
    }

    return osTimerCreate (timerId, 
                          name, 
                          timerRoutine, 
                          userData, 
                          initialTime, 
                          rescheduleTime,
                          timerParam);
}



/******************************************************************************
 *
 * Function   : osTimerDelete
 *              
 * Description: This function deletes the OS timer
 *
 * Returns    : void
 *              
 ******************************************************************************/

E_ErrorCodes osTimerDelete (GL_TIMER_ID      timerId)
{
    if (timer_delete(timerId) == 0)
    {
        return IAMBA_OK;
    }
    else
    {
        return ERR_TIMER_CONTROL;
    }
}


/******************************************************************************
 *
 * Function   : omciTimerDelete
 *              
 * Description: This function releases timer resources and deletes timer
 *
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes omciTimerDelete (GL_TIMER_ID timerId, UINT32 userData)
{
    freeOmciTimerSigno(userData);

    return timer_delete(timerId);
}


/******************************************************************************
 *
 * Function   : osTimerReset
 *              
 * Description: This function sets the PM sync time to current time
 *
 * Parameters : UINT32 gongTime - The time to set
 *              
 * Returns    : void
 *              
 ******************************************************************************/

E_ErrorCodes osTimerReset  (GL_TIMER_ID      *timer, 
                            fGlTimerRoutine  timerRoutine, 
                            unsigned int     initialTime, 
                            unsigned int     rescheduleTime,
                            unsigned long    enable )
{
    return IAMBA_OK;
}


/******************************************************************************
 *
 * Function   : osTimerStart
 *              
 * Description: This function starts the timer : always returns OK
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osTimerStart  (GL_TIMER_ID      timerId)
{
    return IAMBA_OK;
}


/******************************************************************************
 *
 * Function   : osTimerStop
 *              
 * Description: This function stops the timer : always returns OK
 *
 * Parameters : 
 *              
 * Returns    : E_ErrorCodes
 *              
 ******************************************************************************/

E_ErrorCodes osTimerStop   (GL_TIMER_ID      timerId)
{
    return IAMBA_OK;
}


/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*                       RTOS ticks Handling Section                           */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/       

/******************************************************************************
 *
 * Function   : osSecondsGet
 *              
 * Description: This function returns the current seconds value
 *
 * Parameters : 
 *              
 * Returns    : void
 *              
 ******************************************************************************/

unsigned int osSecondsGet()
{
    struct timeval tv;
    struct timeval *ptv = &tv;

    if (gettimeofday (ptv, NULL) == 0)
    {
        return ptv->tv_sec;
    }
    else
    {    
        return 0;
    }
}


/******************************************************************************
 *
 * Function   : osUptimeGet
 *              
 * Description: This function returns the system uptime in units of 10ms
 *              as measured by OMCI
 *
 * Parameters : 
 *              
 * Returns    : unsigned long
 *              
 ******************************************************************************/

unsigned long osUptimeGet()
{

    struct timeval tv;
    struct timeval *pCurrent = &tv;
    struct timeval *pStart   = &omciStartTime;

    unsigned int temp;

    if (gettimeofday (pCurrent, NULL) == 0)
    {
        if (pCurrent->tv_usec > pStart->tv_usec)
        {
            temp = pCurrent->tv_usec - pStart->tv_usec;
            return(((pCurrent->tv_sec - pStart->tv_sec) * 100) + (pCurrent->tv_usec - pStart->tv_usec) / 10000);
        }
        else
        {
            /*return(((pCurrent->tv_sec - pStart->tv_sec - 1) * 1000000) + (1000000 + pCurrent->tv_usec - pStart->tv_usec)) / 10000;*/
            /*ken updated, * 1000000 may cause overflow*/
            return(((pCurrent->tv_sec - pStart->tv_sec - 1) * 100) + (1000000 + pCurrent->tv_usec - pStart->tv_usec)/ 10000) ;
        }
    }
    return 0;
}


/******************************************************************************
 *
 * Function   : osTickPerSecGet
 *              
 * Description: This function returns the number of ticks per sec
 *
 * Parameters : 
 *              
 * Returns    : unsigned long
 *              
 ******************************************************************************/

unsigned long osTickPerSecGet()
{
    return sysconf(_SC_CLK_TCK);
}


/******************************************************************************
 *
 * Function   : setOmciStartTime
 *              
 * Description: This function sets the OMCI start time
 *
 * Parameters : 
 *              
 * Returns    : void
 *              
 ******************************************************************************/

void setOmciStartTime()
{
    if (gettimeofday (&omciStartTime, NULL) != 0)
    {
        // TBD-ZG Add in some error printout
    }
}



