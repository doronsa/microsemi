/*******************************************************************************
*               Copyright 2009, Marvell Technology Group Ltd.
*
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK), GALILEO TECHNOLOGY LTD. (GTL)
* GALILEO TECHNOLOGY, INC. (GTI) AND RADLAN Computer Communications, LTD.
********************************************************************************
* tpm_mod.c
*
* DESCRIPTION:
*
* DEPENDENCIES:
*               None
*
* CREATED BY:   Orenbh
*
* DATE CREATED:
*
* FILE REVISION NUMBER:
*               $Revision: 1.1 $
*
*
*******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "globals.h"
#include "errorCode.h"
#include "OsGlueLayer.h"
#include "ontTasks.h"
#include "mv_os_timer_ext.h"
#include "mv_os_timer.h"



#define TM_NUM_CYCLES_DEBUG      1

#define TM_ERR_PRINT(format, ...)    printf("(error) pid(%d) %s(%d)" format "\n", getpid(), __FUNCTION__, __LINE__, ##__VA_ARGS__);
#define TM_DBG_PRINT(format, ...)    if (timerDebug) { printf("(debug) pid(%d) %s(%d)" format "\n", getpid(), __FUNCTION__, __LINE__, ##__VA_ARGS__); }
#define TM_DBG_N_PRINT(format, ...)  if ((timerDebug) && (cycleDebug%TM_NUM_CYCLES_DEBUG == 0)) { printf("(debug) pid(%d) %s(%d)" format "\n", getpid(), __FUNCTION__, __LINE__, ##__VA_ARGS__); }



/********************/
/* Global Variables */
/********************/

/* Database */
mv_os_timer_db_t    timerDb;

/* Debug on/off */
uint32_t            timerDebug = 1;
uint32_t            cycleDebug = 0;

/* Mutexes */
pthread_mutex_t     timerDbMutex;
GL_MUTEX_ID         timerDbMutex_p = &timerDbMutex;
GL_MUTEX_ATTR       timerDbMutexAttr;

/* Pthread */
pthread_t           timerTaskId;

/* Msg Queue */
GL_QUEUE_ID         timer_msg_q;

/* Timer Tick */
uint32_t            mvOsTimerTick = 10;


/********************/
/*    DB Section    */
/********************/


void timer_db_init(void)
{
    memset(&timerDb, 0, sizeof(timerDb));
    timerDb.timerTblMaxEntry = -1;
}


static inline int timer_db_entry_check(TIMER_ID timerId)
{
    /* entry = timerid */
    if (timerId < 0 || timerId >= MV_TIMER_NUM_TIMERS)
    {
        return(-1);
    }
    return(0);
}


int timer_db_valid_get(TIMER_ID timerId, uint32_t *curValid)
{
    if (timer_db_entry_check(timerId) != 0)
        return(-1);

    *curValid = timerDb.timerTbl[timerId].valid;

    return(0);
}


int timer_db_active_get(TIMER_ID timerId, uint32_t *curActive)
{
    if (timer_db_entry_check(timerId) != 0)
        return(-1);

    if (timerDb.timerTbl[timerId].valid == 1)
        *curActive = timerDb.timerTbl[timerId].activeTimer;
    else
        *curActive = 0;

    return(0);
}


/******************************************************************************
 *
 * Function   : timer_db_active_set
 *
 * Description: This routine activates a timer in DB.
 *              Active timers are timers that have a running timer associates with them.
 *
 * Parameters :
 *              timerId - The timerId to activate
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_db_active_set(TIMER_ID timerId)
{
    /* Check valid Entry */
    if (timer_db_entry_check(timerId) != 0)
        return(-1);

    /* Cannot set active on Invalid Entry */
    if (timerDb.timerTbl[timerId].valid == 0)
        return(-1);

    timerDb.timerTbl[timerId].activeTimer = 1;

    return(0);
}


int timer_db_active_reset(TIMER_ID timerId)
{

    /* Check valid Entry */
    if (timer_db_entry_check(timerId) != 0)
        return(-1);

    /* Cannot reset active on Invalid Entry */
    if (timerDb.timerTbl[timerId].valid == 0)
        return(-1);

    timerDb.timerTbl[timerId].activeTimer = 0;

    return(0);
}


/******************************************************************************
 *
 * Function   : timer_db_active_timers_update
 *
 * Description: This routine update the number of active timers,
 *              according to a status change of a certain timer.
 *
 * Parameters :
 *              timerId - The timerId to activate
 *
 * Returns    : void
 *
 ******************************************************************************/
void timer_db_active_timers_update(uint32_t prevActive,
                                   uint32_t curActive)
{
    if ((prevActive == 0) && (curActive == 1))
    {
        timerDb.timerTblNumActiveEntries++;
    }
    else if ((prevActive == 1) && (curActive == 0))
    {
        timerDb.timerTblNumActiveEntries--;
    }
    /* else do nothing, probably the timer was restarted w/o an intermittent stop call */

    return;
}


static inline int timer_db_active_timers_get()
{
    return timerDb.timerTblNumActiveEntries;
}


/******************************************************************************
 *
 * Function   : timer_db_entry_cfg_timers_set
 *
 * Description: This routine sets the configured Init and rescheduled timer values of a timer.
 *
 * Parameters :
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_db_entry_cfg_timers_set(TIMER_ID timerId,
                                  uint32_t cfgInitialTime,
                                  uint32_t cfgRescheduleTime)
{
    /* Check valid Entry */
    if (timer_db_entry_check(timerId) != 0)
        return(-1);

    /* Cannot set Timers on Invalid Entry */
    if (timerDb.timerTbl[timerId].valid == 0)
        return(-1);

    timerDb.timerTbl[timerId].initCfgTime = cfgInitialTime;
    timerDb.timerTbl[timerId].reschedCfgTime = cfgRescheduleTime;

    /* Start */
    if ((cfgInitialTime != 0) || (cfgRescheduleTime!= 0))
    {
        timerDb.timerTbl[timerId].numStarts++;
        timerDb.timerTbl[timerId].numRescheds = 0; /* Resched counter is reset after timer is started */
        timerDb.timerTbl[timerId].activeTimer = 1;
    }
    /* Stop */
    else
        timerDb.timerTbl[timerId].activeTimer = 0;

    return(0);
}


/******************************************************************************
 *
 * Function   : timer_db_max_entry_get
 *
 * Description: This routine gets the highest table entry that is occupied.
 *
 * Parameters :
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_db_max_entry_get(void)
{
    return(timerDb.timerTblMaxEntry);
}


/******************************************************************************
 *
 * Function   : timer_db_entry_delete
 *
 * Description: This routine deletes a timer entry from the FB,
 *              and updates the max. occupied table entry if necessary
 *
 * Parameters :
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_db_entry_delete(TIMER_ID timerId)
{
    int i;

    /* Inactivate timer and invalidate timer */
    memset(&(timerDb.timerTbl[timerId]), 0, sizeof(timer_db_entry_t));

    /* If this was MaxEntry, find the new MaxEntry. If no valid entries left, set MaxEntry=-1 */
    if (timerId == timerDb.timerTblMaxEntry)
    {
        i = timerId-1;
        while (i >=0)
        {
            if (timerDb.timerTbl[i].valid == 1)
                break;
            i--;
        }
        /* Set to new max valid entry, or (-1) if no valid entry exists */
        timerDb.timerTblMaxEntry = i;
        TM_DBG_PRINT("newMaxEntry(%d)", timerDb.timerTblMaxEntry);
    }

    return(0);
}


/******************************************************************************
 *
 * Function   : timer_db_entry_create
 *
 * Description: This routine creates a timer entry in the DB,
 *              and updates the max. occupied table entry if necessary
 *
 * Parameters :
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_db_entry_create(TIMER_ID *timerId,
                          char *timerName,
                          funcTimer timerCb,
                          uint32_t  timerCbParamA,
                          uint32_t timerCbParamB)
{
    uint32_t i;

    for (i=0; i< MV_TIMER_NUM_TIMERS; i++)
    {
        if (timerDb.timerTbl[i].valid == 0)
        {
            /* Inactivate timer */
            timerDb.timerTbl[i].initCfgTime      = 0;
            timerDb.timerTbl[i].initOperTime     = 0;
            timerDb.timerTbl[i].reschedOperTime  = 0;
            timerDb.timerTbl[i].reschedCfgTime   = 0;
            timerDb.timerTbl[i].activeTimer      = 0;

            /* Reset counters */
            timerDb.timerTbl[i].numRescheds = 0;
            timerDb.timerTbl[i].numStarts = 0;

            /* Set params */
            timerDb.timerTbl[i].cbFunc = timerCb;
            timerDb.timerTbl[i].cbParamA = timerCbParamA;
            timerDb.timerTbl[i].cbParamB = timerCbParamB;
            strncpy (&(timerDb.timerTbl[i].timerName[0]), timerName, (MV_OS_TIMER_NAME_LEN-1));

            /* Set valid */
            timerDb.timerTbl[i].valid = 1;

            /* Update TblMaxEntry */
            if ((timerDb.timerTblMaxEntry == -1) ||( i > (uint32_t) timerDb.timerTblMaxEntry))
                timerDb.timerTblMaxEntry = i;

            *timerId = (TIMER_ID) i;
            return(0);
        }
    }

    return(-1);
}


int timer_db_full_entry_get(TIMER_ID timerId,
                            timer_db_entry_t *tblEntry)
{


    tblEntry->activeTimer       = timerDb.timerTbl[timerId].activeTimer;
    tblEntry->initCfgTime       = timerDb.timerTbl[timerId].initCfgTime;
    tblEntry->initOperTime      = timerDb.timerTbl[timerId].initOperTime;
    tblEntry->reschedCfgTime    = timerDb.timerTbl[timerId].reschedCfgTime;
    tblEntry->reschedOperTime   = timerDb.timerTbl[timerId].reschedOperTime;
    tblEntry->flagFunc          = timerDb.timerTbl[timerId].flagFunc;
    tblEntry->valid             = timerDb.timerTbl[timerId].valid;
    tblEntry->cbFunc            = timerDb.timerTbl[timerId].cbFunc;
    tblEntry->cbParamA          = timerDb.timerTbl[timerId].cbParamA;
    tblEntry->cbParamB          = timerDb.timerTbl[timerId].cbParamB;
    tblEntry->activeTimer       = timerDb.timerTbl[timerId].activeTimer;
    tblEntry->numStarts         = timerDb.timerTbl[timerId].numStarts;
    tblEntry->numRescheds       = timerDb.timerTbl[timerId].numRescheds;
    strncpy(&(tblEntry->timerName[0]), &(timerDb.timerTbl[timerId].timerName[0]), MV_OS_TIMER_NAME_LEN);

    return 0;
}

int timer_db_entry_timer_info_get(TIMER_ID timerId,
                                  uint32_t *activeTimer,
                                  uint32_t *initOperTime,
                                  uint32_t *reschedCfgTime,
                                  uint32_t *reschedOperTime)
{

    if (timer_db_entry_check(timerId) != 0)
    {
        TM_ERR_PRINT();
        return(-1);
    }

    *activeTimer     = timerDb.timerTbl[timerId].activeTimer;
    *initOperTime    = timerDb.timerTbl[timerId].initOperTime;
    *reschedCfgTime  = timerDb.timerTbl[timerId].reschedCfgTime;
    *reschedOperTime = timerDb.timerTbl[timerId].reschedOperTime;

    return(0);
}


int timer_db_init_oper_set(TIMER_ID timerId,
                           uint32_t timerValue)
{
    timerDb.timerTbl[timerId].initOperTime = timerValue;
    return(0);
}

int timer_db_resched_oper_set(TIMER_ID timerId,
                              uint32_t timerValue)
{
    timerDb.timerTbl[timerId].reschedOperTime = timerValue;
    return(0);
}


/******************************************************************************
 *
 * Function   : timer_db_flag_cb_set
 *
 * Description: This routine sets the Callback routine flag, which indicates to the CB stage, that this timer has expired.
 *
 * Parameters :
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_db_flag_cb_set(TIMER_ID timerId)
{
    timerDb.timerTbl[timerId].flagFunc = 1;
    return(0);
}


int timer_db_flag_cb_reset(TIMER_ID timerId)
{
    timerDb.timerTbl[timerId].flagFunc = 0;
    return(0);
}


/******************************************************************************
 *
 * Function   : timer_db_sched_timer_reload
 *
 * Description: This routine reloads the configured reschedule timer into the operational timer.
 *
 * Parameters :
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_db_sched_timer_reload(TIMER_ID timerId)
{
    timerDb.timerTbl[timerId].reschedOperTime = timerDb.timerTbl[timerId].reschedCfgTime;
    return(0);
}

int timer_db_init_timer_load(TIMER_ID timerId)
{
    timerDb.timerTbl[timerId].initOperTime = timerDb.timerTbl[timerId].initCfgTime;
    return(0);
}

int timer_db_sched_timer_cnt_inc(TIMER_ID timerId)
{
    timerDb.timerTbl[timerId].numRescheds++;
    return(0);
}


int timer_db_func_info_get(TIMER_ID timerId,
                           uint32_t *flagSet,
                           funcTimer * funcP,
                           uint32_t *cbParamA,
                           uint32_t *cbParamB)
{
    *flagSet    = timerDb.timerTbl[timerId].flagFunc;
    *funcP      = timerDb.timerTbl[timerId].cbFunc;
    *cbParamA   = timerDb.timerTbl[timerId].cbParamA;
    *cbParamB   = timerDb.timerTbl[timerId].cbParamB;
}

/******************************************************************************
 *
 * Function   : timer_db_entry_get_next
 *
 * Description: This routine gets the next valid timer entry
 *
 * Parameters :
 *              timerId - The current timerId
 *              curValid - returns the first valid timerId after the current one
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_db_entry_get_next(TIMER_ID curId,
                            TIMER_ID *nextId)
{
    int i;

    /* Check valid Entry */
    if (timer_db_entry_check(curId+1) != 0)
        return(-1);

    for (i=curId+1; i<=timerDb.timerTblMaxEntry;i++)
    {
        if (timerDb.timerTbl[i].valid == 1)
        {
            *nextId = i;
            return(0);
        }
    }

    return(-1);
}

/******************************************************************************
 *
 * Function   : timer_db_entry_get_first
 *
 * Description: This routine gets the first valid timer entry
 *
 * Parameters :
 *              timerId - The current timerId
 *              curValid - returns the first valid timerId after the current one
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_db_entry_get_first(TIMER_ID *curId)
{
    int i;

    if (timerDb.timerTblMaxEntry == -1)
        return(-1);

    for (i=0; i<=timerDb.timerTblMaxEntry;i++)
    {
        if (timerDb.timerTbl[i].valid == 1)
        {
            *curId = i;
            return(0);
        }
    }

    TM_DBG_PRINT("%s", "No first DB Entry found");
    return(-1);
}



/********************/
/*  Print Section   */
/********************/


void tpm_print_tbl_hl(void)
{
    printf("VAL ");
    printf("ACT ");
    printf("%-20s  ", "TIMER NAME");
    printf("FLG ");
    printf("FNC   ");
    printf("PARAM_A   ");
    printf("PARAM_B  ");
    printf("INIT_CFG ");
    printf("INIT_OPR ");
    printf("SCHD_CFG ");
    printf("SCHD_OPR ");
    printf("NUM_RESC ");
    printf("NUM_ST");
    printf("\n");
}

void timer_print_entry(TIMER_ID curId, timer_db_entry_t *tblEntry)
{
    printf(" %c  ", (tblEntry->valid) ? '+' : '-');
    printf(" %c  ", (tblEntry->activeTimer) ? '+' : '-');
    printf("%-20s  ", tblEntry->timerName);
    printf(" %c  ", (tblEntry->flagFunc) ? '+' : '-');
    printf(" %c  ", (tblEntry->cbFunc) ? '+' : '-');
    printf(" %08x  ", tblEntry->cbParamA);
    printf(" %08x  ", tblEntry->cbParamB);
    printf("%06d   ", tblEntry->initCfgTime);
    printf("%06d   ", tblEntry->initOperTime);
    printf("%06d  ", tblEntry->reschedCfgTime);
    printf("%06d   ", tblEntry->reschedOperTime);
    printf("%06d    ", tblEntry->numRescheds);
    printf("%02d  ", tblEntry->numStarts);
    printf("\n");

}

int  timer_print_tbl(uint32_t validOnly)
{
    int         ret_val, i;
    timer_db_entry_t  tblEntry;

    /* Print Headlines */
    tpm_print_tbl_hl();

    /* Loop over table and print entries */
    for (i=0; i<MV_TIMER_NUM_TIMERS ;i++)
    {
        ret_val = timer_db_full_entry_get(i, &tblEntry);
        if ((validOnly==0) || (tblEntry.valid == 1))
            timer_print_entry(i, &tblEntry);
    }
    return(0);
}


/********************/
/*  Main Section    */
/********************/


/******************************************************************************
 *
 * Function   : timer_table_entry_update
 *
 * Description: This routine updates the remaining time for a timer.
 *              Routine performs logic needed for timer expiration : setting callback_flag, timer reloading.
 *
 * Parameters :
 *              timerId - The current timerId
 *              timerTick - time in ms that should be deducted from the timer
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_table_entry_update(TIMER_ID timerId,
                             uint32_t timerTick)
{
    uint32_t activeTimer, initOperTime, reschedCfgTime, reschedOperTime;
    int32_t newTime;

    TM_DBG_N_PRINT("timerid(%d)", timerId);

    /* Get Timer Info  */
    if (timer_db_entry_timer_info_get(timerId, &activeTimer, &initOperTime, &reschedCfgTime, &reschedOperTime) == 0)
    {
        TM_DBG_N_PRINT("timerid(%d) act(%d), initOp(%d) reschCfg(%d) reschOpr(%d)\n",
                       timerId, activeTimer, initOperTime, reschedCfgTime, reschedOperTime);
        if (activeTimer)
        {
            /* Working on Initial Timer */
            if (initOperTime > 0)
            {
                newTime = initOperTime - timerTick;
                TM_DBG_N_PRINT("newTime(%d)\n", newTime);
                if (newTime <= 0)
                {
                    /* Set InitTime to zero */
                    timer_db_init_oper_set(timerId, 0);

                    /* Set flag to perform CallBack function in later phase */
                    timer_db_flag_cb_set(timerId);

                    /* Reload ReschedTimer (if was configured) else  Inactivate timerId */
                    if (reschedCfgTime == 0)
                    {
                        /* Inactivate the timerId */
                        timer_db_active_reset(timerId);

                        /* Decrease nmuber of active entries */
                        timer_db_active_timers_update(1 /*prevActive*/, 0 /*curActive*/);
                    }
                    else
                    {
                        /* Reload Entry */
                        timer_db_sched_timer_reload(timerId);

                        /* Update Reload Counter*/
                        timer_db_sched_timer_cnt_inc(timerId);
                    }
                }
                else
                {
                    /* Update  InitTime */
                    timer_db_init_oper_set(timerId, newTime);
                }
            }
            else if (reschedOperTime > 0)  /* Working on Reschedule Timer */
            {
                newTime = reschedOperTime - timerTick;
                TM_DBG_N_PRINT("newTime(%d)\n", newTime);
                if (newTime <= 0)
                {
                    /* Set flag to perform CallBack function in later phase */
                    timer_db_flag_cb_set(timerId);

                    /* Reload Entry */
                    timer_db_sched_timer_reload(timerId);

                    /* Update Reload Counter */
                    timer_db_sched_timer_cnt_inc(timerId);
                }
                else
                {
                    /* Update RescheduleTime */
                    timer_db_resched_oper_set(timerId, newTime);
                }
            }
            else /* DB Inconsistent */
            {
                TM_ERR_PRINT("%s", "DB Error, activeTimer>0, but no active timers \n");
                return(-1);
            }
        }
    }
    else
    {
        TM_ERR_PRINT("timerid(%d)\n", timerId);
        return(-1);
    }

    return 0;
}


/******************************************************************************
 *
 * Function   : timer_table_flag_entry_cb
 *
 *              Routine performs logic needed for timer expiration : setting callback_flag, timer reloading.
 *
 * Parameters :
 *              timerId - The current timerId
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_table_flag_entry_cb(TIMER_ID timerId)
{
    uint32_t    flagSet, cbParamA, cbParamB;
    funcTimer   cbFunc;

    /* Checking flagset is good enough, it means the entry is valid etc. */
    timer_db_func_info_get(timerId, &flagSet, &cbFunc, &cbParamA, &cbParamB);

    /* If 'timerId' is in the process of deletion during this callback call, then cbFunc could be NULL. */
    if (flagSet && cbFunc)
    {
        TM_DBG_PRINT("timerId(%d), flagSet(%d), cbFunc(%8x), cbParamA(%d), cbParamB(%d)\n",
                     timerId, flagSet, cbFunc, cbParamA, cbParamB);

        /* Perform Callback */
        (*cbFunc)(cbParamA, cbParamB);

        /* Reset Flag in DB */
        timer_db_flag_cb_reset(timerId);
    }

    return(0);
}

/******************************************************************************
 *
 * Function   : timer_table_update
 *
 *              Routine updates the tier table times and takes action for relevant timers.
 *
 * Parameters :
 *              timerId - The current timerId
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_table_update(uint32_t timerTick)
{
    int         ret_val;
    TIMER_ID    curId, nextId;

    TM_DBG_N_PRINT();

    /* I - Lock DB */
    if (osMutexLock(timerDbMutex_p)!= MV_OS_GLUE_OK)
    {
        TM_ERR_PRINT();
        return(MV_OS_GLUE_GEN_ERR);
    }

    /* II - Loop over table and update entries */
    if (timer_db_entry_get_first(&nextId) != 0)
    {
        TM_DBG_N_PRINT("%s", "No active Table entries\n");
        osMutexUnlock(timerDbMutex_p);
        return(0);
    }
    do
    {
        curId = nextId;
        ret_val = timer_table_entry_update(curId, timerTick);
        if (ret_val != 0)
        {
            TM_ERR_PRINT();
            osMutexUnlock(timerDbMutex_p);
            return(-1);
        }
    } while (timer_db_entry_get_next(curId, &nextId) == 0);

    if (timerDebug)
    {
        timer_print_tbl(0);
    }

    /* III - Unlock DB */
    osMutexUnlock(timerDbMutex_p);
}


/******************************************************************************
 *
 * Function   : timer_table_call_back
 *
 *              Routine calls callback functions for all timers that had expired
 *
 * Parameters :
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_table_call_back(void)
{
    int         ret_val;
    TIMER_ID    curId, nextId;

    if (timer_db_entry_get_first(&nextId) != 0)
    {
        TM_DBG_N_PRINT();
        return(0);
    }
    do
    {
        curId = nextId;
        ret_val = timer_table_flag_entry_cb(curId);
        if (ret_val != 0)
        {
            TM_ERR_PRINT();
            return(-1);
        }

    } while (timer_db_entry_get_next(curId, &nextId) == 0);

    return(0);
}

/******************************************************************************
 *
 * Function   : timer_check_valid
 *
 *              Routine checks that timer is legit and is in valid state
 *
 * Parameters :
 *
 * Returns    : int
 *
 ******************************************************************************/
int timer_check_valid(TIMER_ID timerId)
{
    uint32_t timerValid;

    /* Check legit entry */
    if (timer_db_valid_get(timerId, &timerValid)!= 0)
    {
        TM_ERR_PRINT();
        return(-1);
    }
    /* Cannot delete invalid Entry */
    if (timerValid != 1)
    {
        TM_ERR_PRINT();
        return(-1);
    }

    return(0);
}

/******************************************************************************
 *
 * Function   : timer_set
 *
 *              Routine sets a timer.
 *              - If the init_time and resched_time are both zero, it stops a timer.
 *              - Otherwisse it starts a new timer or restarts an existing timer.
 *
 * Parameters :
 *
 * Returns    : int
 *
 ******************************************************************************/
mv_os_glue_error_code_t timer_set(TIMER_ID timerId,
                                  uint32_t initialTime,
                                  uint32_t rescheduleTime)
{
    uint32_t prevActive, curActive, timerValid;

    TM_DBG_PRINT("timerId(%d), initialTime(%d),rescheduleTime(%d)",
                 timerId, initialTime, rescheduleTime);

    /* Take lock */
    if (osMutexLock(timerDbMutex_p)!= MV_OS_GLUE_OK)
    {
        TM_ERR_PRINT();
        return(MV_OS_GLUE_GEN_ERR);
    }

    /* Check valid Entry */
    if (timer_check_valid(timerId) != 0)
    {
        osMutexUnlock(timerDbMutex_p);
        TM_ERR_PRINT();
        return(MV_OS_GLUE_EINVAL);
    }

    /* Get previous Timer state */
    timer_db_active_get(timerId, &prevActive);

    /* Update Timer Configuration */
    if (timer_db_entry_cfg_timers_set(timerId, initialTime, rescheduleTime) != 0)
    {
        osMutexUnlock(timerDbMutex_p);
        TM_ERR_PRINT();
        return(MV_OS_GLUE_EINVAL);
    }

    /* Update Operational Timers */
    timer_db_init_timer_load(timerId);
    timer_db_sched_timer_reload(timerId);

    /* Get current Timer state */
    timer_db_active_get(timerId, &curActive);

    /* Update number_active_timers */
    timer_db_active_timers_update(prevActive, curActive);

    /* Unlock */
    osMutexUnlock(timerDbMutex_p);

    return(MV_OS_GLUE_OK);
}


/******************************************************************************
 *
 * Function   : timer_main_loop
 *
 *              This routine loops over the timers, while there is any active timer (inner loop).
 *              For each loop :
 *                  Sleep 10ms
 *                  Update the timers table (under lock)
 *                  Call Callback funcs for relevant timers (not under lock)
 *              When no active timers exist, the loop waits on a msg_queue for a new started (active) timer.
 *
 *
 * Parameters :
 *
 * Returns    : void
 *
 ******************************************************************************/
void timer_main_loop (void *argv)
{
    TIMER_ID timerId;
    unsigned int msg_pri;
    struct timespec pTimeoutTime;


    TM_DBG_PRINT("%s", "Starting Main Loop . . .\n");

    /* Enter Update Timer Loop */
    while (1)
    {
        /* Block on start API call */
        osMsgQReceive(timer_msg_q, (void *)&timerId, MV_TIMER_MSG_SIZE, 0 /* N/A*/, &msg_pri);
        TM_DBG_PRINT("Queue recvd timerid(%d)\n", timerId);

        while (timer_db_active_timers_get() > 0)
        {
            /* Sleep 10 ms */
            osTaskDelay(mvOsTimerTick);

            /* Under Lock, Check table entries, reduce counters and flag the expired timers */
            timer_table_update(mvOsTimerTick);

            /* Without Lock, Callback expired Timer functions */
            timer_table_call_back();

            /* Increase cycle count */
            cycleDebug++;
        }
    }

    return;
}

/***********************/
/* Exteral API Section */
/***********************/

/******************************************************************************
 *
 * Function   : mv_os_glue_timer_init
 *
 *              This routine inits the timer thread resources.
 *
 * Parameters :
 *
 * Returns    : error_code
 *
 ******************************************************************************/
mv_os_glue_error_code_t mv_os_glue_timer_init(void)
{
    char timer_msg_q_name[25];

    /* Init DB */
    timer_db_init();

    /* Init Default Attributes for DB mutex */
    if (pthread_mutexattr_init(&timerDbMutexAttr)  != MV_OS_GLUE_OK)
    {
        TM_ERR_PRINT();
        return(MV_OS_GLUE_GEN_ERR);
    }
    /* Init DB Locking Mutex */
    if (osMutexInit(timerDbMutex_p, &timerDbMutexAttr) != MV_OS_GLUE_OK)
    {
        TM_ERR_PRINT();
        return(MV_OS_GLUE_GEN_ERR);
    }
    /* Create Message Queue for Newly Started timers to "wake up" the main loop */
    sprintf(&timer_msg_q_name[0], "/proc%d_timerq", getpid());
    if (osMsgQCreate(&timer_msg_q, timer_msg_q_name, MV_TIMER_MSGQ_ENTRIES, MV_TIMER_MSG_SIZE))
    {
        TM_ERR_PRINT();
        return(MV_OS_GLUE_GEN_ERR);
    }

    /* Create Task for Main Sleeping Loop */
    if (osTaskCreate(&timerTaskId, "timerTaskId", (GLFUNCPTR) timer_main_loop,
                     0, 0, ONU_TIMER_THREAD_PRIORITY, MV_OS_TIMER_STACK_SIZE) != IAMBA_OK)
    {
        TM_ERR_PRINT();
        return(MV_OS_GLUE_GEN_ERR);
    }

    return(MV_OS_GLUE_OK);
}


/******************************************************************************
 *
 * Function   : mv_os_glue_timer_create
 *
 *              This routine cretaes a new timer. After this call, the timer is valid but not active.
 *
 * Parameters :
 *
 * Returns    : error_code
 *
 ******************************************************************************/
mv_os_glue_error_code_t mv_os_glue_timer_create(TIMER_ID *timerId,
                                                char *timerName,
                                                funcTimer timerCb,
                                                uint32_t  timerCbParamA,
                                                uint32_t timerCbParamB)
{
    TIMER_ID intId;

    TM_DBG_PRINT("name(%s) paramA(%d) paramB(%d)", timerName, timerCbParamA, timerCbParamB);

    /* Check params & availability */
    if (timerId == NULL || timerCb == NULL)
    {
        TM_ERR_PRINT("%s", "-received null pointer-");
        return(MV_OS_GLUE_GEN_ERR);
    }

    /* Take lock */
    if (osMutexLock(timerDbMutex_p)!= MV_OS_GLUE_OK)
    {
        TM_ERR_PRINT();
        return(MV_OS_GLUE_GEN_ERR);
    }

    /* Init Entry and create it (no timers are set) */
    if (timer_db_entry_create(&intId, timerName, timerCb, timerCbParamA, timerCbParamB) != 0)
    {
        TM_ERR_PRINT("%s", "-no resource-");
        osMutexUnlock(timerDbMutex_p);
        return(MV_OS_GLUE_GEN_ERR);
    }

    /* Unlock */
    osMutexUnlock(timerDbMutex_p);

    /* Return timerId */
    *timerId = (TIMER_ID) intId;

    TM_DBG_PRINT("timerID(%d)", *timerId);

    return(MV_OS_GLUE_OK);
}


/******************************************************************************
 *
 * Function   : mv_os_glue_timer_del
 *
 *              This routine deletes a timer. The timer must be stopped before deletion.
 *
 * Parameters :
 *
 * Returns    : error_code
 *
 ******************************************************************************/
mv_os_glue_error_code_t mv_os_glue_timer_del(TIMER_ID timerId)
{
    uint32_t curActive;

    TM_DBG_PRINT("timerID(%d)", timerId);

    /* Take lock */
    if (osMutexLock(timerDbMutex_p)!= MV_OS_GLUE_OK)
    {
        TM_ERR_PRINT();
        return(MV_OS_GLUE_GEN_ERR);
    }

    /* Check valid Entry */
    if (timer_check_valid(timerId) != 0)
    {
        osMutexUnlock(timerDbMutex_p);
        TM_ERR_PRINT();
        return(MV_OS_GLUE_EINVAL);
    }
    /* Check Inactive entry (Timer must be inactive - stopped) */
    timer_db_active_get(timerId, &curActive);
    if (curActive)
    {
        osMutexUnlock(timerDbMutex_p);
        TM_ERR_PRINT("%s%d%s", "Timer(",timerId, ") must be stopped before deleting\n");
        return(MV_OS_GLUE_EINVAL);
    }

    /* Delete Entry and Initialize it */
    if (timer_db_entry_delete(timerId) != 0)
    {
        osMutexUnlock(timerDbMutex_p);
        TM_ERR_PRINT();
        return(MV_OS_GLUE_EINVAL);
    }

    /* Unlock */
    osMutexUnlock(timerDbMutex_p);

    return(MV_OS_GLUE_OK);
}



/******************************************************************************
 *
 * Function   : mv_os_glue_timer_start
 *
 *              This routine starts a timer. The timer can have an initial timer and/or rescheduled timer.
 *              The routine sends a msg to the timer_main_loop msg queue, to wake it up if necessary.
 *
 * Parameters :
 *              timerId        - The timerId to start
 *              initialTime    - in ms
 *              rescheduleTime - in ms
 * Returns    : error_code
 *
 ******************************************************************************/
mv_os_glue_error_code_t mv_os_glue_timer_start(TIMER_ID timerId,
                                               uint32_t initialTime,
                                               uint32_t rescheduleTime)
{
    int err;

    err = timer_set(timerId, initialTime, rescheduleTime);

    if (!err)
    {
        if (osMsgQSend(timer_msg_q, (void *)&timerId, MV_TIMER_MSG_SIZE, MV_TIMER_MSG_PRIO))
        {
            TM_DBG_PRINT();
        }
    }

    return(err);
}

/******************************************************************************
 *
 * Function   : mv_os_glue_timer_stop
 *
 *              This routine stops a timer, it does not delete it.
 *
 *
 * Parameters :
 *              timerId        - The timerId to start
 * Returns    : error_code
 *
 ******************************************************************************/
mv_os_glue_error_code_t mv_os_glue_timer_stop(TIMER_ID timerId)
{
    int err;

    err = timer_set(timerId, 0, 0);

    return(err);
}

mv_os_glue_error_code_t mv_os_glue_timer_print_full_tbl(void)
{
    int ret_val;

    ret_val = timer_print_tbl(0/*validOnly*/);
    return(ret_val);
}


mv_os_glue_error_code_t mv_os_glue_timer_print_valid_tbl(void)
{
    int ret_val;

    ret_val = timer_print_tbl(1/*validOnly*/);

    return(ret_val);
}

mv_os_glue_error_code_t mv_os_glue_timer_debug_act(void)
{
    timerDebug = 1;

    return(MV_OS_GLUE_OK);
}

mv_os_glue_error_code_t mv_os_glue_timer_debug_deact(void)
{
    timerDebug = 0;
    return(MV_OS_GLUE_OK);
}

