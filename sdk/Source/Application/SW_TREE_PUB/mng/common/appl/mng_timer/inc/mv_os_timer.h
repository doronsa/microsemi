/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
alternative licensing terms.  Once you have made an election to distribute the
File under one of the following license alternatives, please (i) delete this
introductory statement regarding license alternatives, (ii) delete the two
license alternatives that you have not elected to use and (iii) preserve the
Marvell copyright notice above.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

********************************************************************************
Marvell GPL License Option

If you received this File from Marvell, you may opt to use, redistribute and/or
modify this File in accordance with the terms and conditions of the General
Public License Version 2, June 1991 (the "GPL License"), a copy of which is
available along with the File in the license.txt file or by writing to the Free
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 or
on the worldwide web at http://www.gnu.org/licenses/gpl.txt.

THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE IMPLIED
WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY
DISCLAIMED.  The GPL License provides additional details about this warranty
disclaimer.
********************************************************************************
Marvell BSD License Option

If you received this File from Marvell, you may opt to use, redistribute and/or
modify this File under the following licensing terms.
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    *   Redistributions of source code must retain the above copyright notice,
	    this list of conditions and the following disclaimer.

    *   Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    *   Neither the name of Marvell nor the names of its contributors may be
        used to endorse or promote products derived from this software without
        specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/

/******************************************************************************
**  FILE        : mv_os_timer.h                                               **
**                                                                           **
**  DESCRIPTION : This file contains ONU TPM Management Interface            **
*******************************************************************************
*                                                                             *
*  MODIFICATION HISTORY:                                                      *
*                                                                             *
*   09Mar11  Yuval Caduri  created                                            *
* =========================================================================== *
******************************************************************************/
#ifndef _MV_OS_TIMER_H_
#define _MV_OS_TIMER_H_

/* Include Files
------------------------------------------------------------------------------*/

/* Definitions
------------------------------------------------------------------------------*/

#define  MV_TIMER_NUM_TIMERS     (5) /*YUVAL - 20*/
#define  MV_OS_TIMER_NAME_LEN    (20)
#define  MV_OS_TIMER_TICK        (10)  //10 millisecond

#define  MV_OS_TIMER_STACK_SIZE  (0x2800) /* 10Kbytes */


#define  MV_TIMER_MSGQ_ENTRIES     (4)
#define  MV_TIMER_MSG_SIZE         (sizeof(uint32_t))
#define  MV_TIMER_MSG_PRIO         (0)



typedef char                    timer_db_name_t[MV_OS_TIMER_NAME_LEN];


typedef struct
{
    uint32_t                    valid;
    timer_db_name_t             timerName;
    uint32_t                    flagFunc;
    funcTimer                   cbFunc;
    uint32_t                    cbParamA;
    uint32_t                    cbParamB;
    uint32_t                    initCfgTime;
    uint32_t                    initOperTime;
    uint32_t                    reschedCfgTime;
    uint32_t                    reschedOperTime;
    uint32_t                    activeTimer;
    uint32_t                    numStarts;          /* Number of Start actions performed on this Entry */
    uint32_t                    numRescheds;        /* Number of reschedules performed on Entry since last Start action */
} timer_db_entry_t;


typedef struct
{
    timer_db_entry_t            timerTbl[MV_TIMER_NUM_TIMERS];
    int32_t                     timerTblMaxEntry;
    uint32_t                    timerTblNumActiveEntries;
} mv_os_timer_db_t;

/* Global variables
------------------------------------------------------------------------------*/

/* Global functions
------------------------------------------------------------------------------*/



/* Macros
------------------------------------------------------------------------------*/

#endif /* _MV_OS_TIMER_H_ */
