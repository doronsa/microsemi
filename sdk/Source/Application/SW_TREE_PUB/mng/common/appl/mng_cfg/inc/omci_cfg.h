/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _OMCI_CFG_H_
#define _OMCI_CFG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "cfg_defs.h"


// XML tag name and attribute defines
#define TAG_OMCI_MIB                 "OMCI_MIB"
#define TAG_voiceSupport             "voiceSupport"
#define TAG_OMCI_VOIP                "voip"
#define TAG_voipSupported            "supported"
#define TAG_voipProtocolSip          "protocol_SIP"
#define TAG_voipConfigByOmci         "config_by_OMCI"
#define TAG_ipHostDataSupport        "ipHostDataSupport"
#define TAG_ontTypeSfu               "ontTypeSfu"
#define TAG_ethernetInfo             "ethernetInfo"
#define TAG_potsInfo                 "potsInfo"
#define TAG_aniInfo                  "aniInfo"
#define TAG_firstNonOmciTcontInfo    "firstNonOmciTcontInfo"
#define TAG_gemPortPmByApmActivation "gemPortPmByApmActivation"
#define TAG_OMCI_ETY                 "OMCI_ETY"
#define TAG_OMCI_HW_CALLS            "hwCalls"

#define ATTR_enabled                 "enabled"
#define ATTR_numPorts                "numPorts"
#define ATTR_slotNumber              "slotNumber"



//Internal database
bool            isVoiceSupported;
bool            isVoipSupported;
bool            isIpHostDataServiceSupported;
bool            isVoipConfigByOmci;
bool            isVoipProtocolSip;
bool            isOntSFU;
bool            isGponInKernel;
bool            gemReset;
bool            tcontReset;
bool            isGemPortPmByApmActivation;
bool            isSkipHwPollPM;
bool            isSkipHwPollAlarm;
bool            isOmciHwCalls; 

UINT32          numEthernetPorts;
UINT32          numPotsPorts;
UINT32          firstNonOmciTcontId;
          

UINT8           numSlotEthernet;
UINT8           numSlotPots;
UINT8           numSlotAni;

UINT16          etherTypeOmciSocket;

//access functions
xml_error_codes_t omci_cfg_init (void);
void              omci_cfg_print(void);

xml_error_codes_t  omci_cfg_get_isVoiceSupported            (bool*   value);
xml_error_codes_t  omci_cfg_get_isIpHostDataServiceSupported(bool*   value);
xml_error_codes_t  omci_cfg_get_isOntSFU                    (bool*   value);
xml_error_codes_t  omci_cfg_get_numEthernetPorts            (UINT32* value);
xml_error_codes_t  omci_cfg_get_numSlotEthernet             (UINT32* value);
xml_error_codes_t  omci_cfg_get_numPotsPorts                (UINT32* value);
xml_error_codes_t  omci_cfg_get_numSlotPots                 (UINT32* value);
xml_error_codes_t  omci_cfg_get_numSlotAni                  (UINT32* value);
xml_error_codes_t  omci_cfg_get_firstNonOmciTcontId         (UINT32* value);
xml_error_codes_t  omci_cfg_get_etherType                   (UINT32* value);
xml_error_codes_t  omci_cfg_get_isGemPortPmByApmActivation  (bool*   value);
xml_error_codes_t  omci_cfg_get_isOmciCallsHW               (bool*   value);
xml_error_codes_t  omci_cfg_get_isVoipSupported             (bool*   value);
xml_error_codes_t  omci_cfg_get_isVoipProtocolSip           (bool*   value);
xml_error_codes_t  omci_cfg_get_isVoipConfigByOmci          (bool*   value);
#ifdef __cplusplus
}
#endif


#endif

