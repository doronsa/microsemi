/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : ProfileCache.c                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the Profile Cache                    **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    25Dec07     zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <errno.h>



#include "omci_cfg.h"

xml_error_codes_t omci_cfg_init(void){

    ezxml_t     xmlHead;
    ezxml_t     xmlOmciMibElement;
    ezxml_t     xmlOmciElement;
    bool        boolvalue;
    UINT32      intvalue;

    xml_error_codes_t rc = CFG_XML_OK; 

    char        buf[100] = {0};
    char        xml_file[50] = {0};

   
    if (get_cfg_xml_file(CFG_MODULE_OMCI,FALSE,xml_file) != CFG_XML_OK)
        return CFG_XML_FAIL;

    if ((xmlHead = ezxml_parse_file(xml_file)) == 0) 
    {
        sprintf(buf, "Fail ezxml_parse_file() of %s", xml_file);
        printf("%s \n", buf);

        memset(&(xml_file[0]),0,50);
        if (get_cfg_xml_file(CFG_MODULE_OMCI,TRUE,xml_file) != CFG_XML_OK)
            return CFG_XML_FAIL;

        if ((xmlHead = ezxml_parse_file(xml_file)) == 0) {
            sprintf(buf, "Fail ezxml_parse_file() of %s", xml_file);
            printf("%s \n", buf);
            return CFG_XML_FAIL;
        }
    }

    if ((xmlOmciMibElement = ezxml_get(xmlHead, TAG_OMCI_MIB, -1)) == 0)
    {
        printf("ezxml_get() of %s failed\n", TAG_OMCI_MIB);
        goto end;
    }


    // Get the boolean voiceSupport
    rc = get_attribute_boolean_value(xmlOmciMibElement, TAG_voiceSupport, ATTR_enabled, &boolvalue, buf);
    if (rc == CFG_XML_OK) {
        isVoiceSupported = boolvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

    // Get the boolean ipHostDataSupport
    rc = get_attribute_boolean_value(xmlOmciMibElement, TAG_ipHostDataSupport, ATTR_enabled, &boolvalue, buf);
    if (rc == CFG_XML_OK){
        isIpHostDataServiceSupported = boolvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

    // Get the boolean ontTypeSfu
    rc = get_attribute_boolean_value(xmlOmciMibElement, TAG_ontTypeSfu, ATTR_enabled, &boolvalue, buf);
    if (rc == CFG_XML_OK){
        isOntSFU = boolvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }


    // Get the numeric ethernetInfo number of ports
    rc = get_attribute_numeric_value(xmlOmciMibElement, TAG_ethernetInfo, ATTR_numPorts, &intvalue, buf);
    if (rc == CFG_XML_OK){
        numEthernetPorts = intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

    // Get the boolean numeric slot number
    rc = get_attribute_numeric_value(xmlOmciMibElement, TAG_ethernetInfo, ATTR_slotNumber, &intvalue, buf);
    if (rc == CFG_XML_OK){
        numSlotEthernet = intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

    // Get the numeric potsInfo number of ports
    rc = get_attribute_numeric_value(xmlOmciMibElement, TAG_potsInfo, ATTR_numPorts, &intvalue, buf);
    if (rc == CFG_XML_OK){
        numPotsPorts = intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

    // Get the numeric potsInfo slot number
    rc = get_attribute_numeric_value(xmlOmciMibElement, TAG_potsInfo, ATTR_slotNumber, &intvalue, buf);
    if (rc == CFG_XML_OK){
        numSlotPots = intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

    // Get the numeric aniInfo slot number
    rc = get_attribute_numeric_value(xmlOmciMibElement, TAG_aniInfo, ATTR_slotNumber, &intvalue, buf);
    if (rc == CFG_XML_OK){
        numSlotAni = intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

     // Get the numeric firstNonOmciTcontInfo ID
    rc = get_tag_numeric_value(xmlOmciMibElement, TAG_firstNonOmciTcontInfo, &intvalue, buf);
    if (rc == CFG_XML_OK){
        firstNonOmciTcontId = intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

    // Get the boolean gemPortPmByApmActivation 
    rc = get_attribute_numeric_value(xmlOmciMibElement, TAG_gemPortPmByApmActivation, ATTR_enabled, &intvalue, buf);
    if (rc == CFG_XML_OK){
        isGemPortPmByApmActivation = (bool)intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }
    
    // Get the boolean HW_Calls
    rc = get_tag_numeric_value(xmlOmciMibElement, TAG_OMCI_HW_CALLS, &intvalue, buf);
    if (rc == CFG_XML_OK){
        isOmciHwCalls = intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

    // Get the int OMCI ether type
    rc = get_tag_numeric_value(xmlOmciMibElement, TAG_OMCI_ETY, &intvalue, buf);
    if (rc == CFG_XML_OK){
        etherTypeOmciSocket = intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

    if ((xmlOmciMibElement = ezxml_get(xmlHead, TAG_OMCI_MIB, 0, TAG_OMCI_VOIP, -1)) == 0)
    {
        printf("ezxml_get() of %s failed\n", TAG_OMCI_MIB);
        goto end;
    }

    // Get the boolean voipSupport
    rc = get_tag_numeric_value(xmlOmciMibElement, TAG_voipSupported, &intvalue, buf);
    if (rc == CFG_XML_OK){
        isVoipSupported = intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

    // Get the boolean protocol_SIP
    rc = get_tag_numeric_value(xmlOmciMibElement, TAG_voipProtocolSip, &intvalue, buf);
    if (rc == CFG_XML_OK){
        isVoipProtocolSip = intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

     // Get the boolean config_by_OMCI
    rc = get_tag_numeric_value(xmlOmciMibElement, TAG_voipConfigByOmci, &intvalue, buf);
    if (rc == CFG_XML_OK){
        isVoipConfigByOmci = intvalue;
    }
    else{
        printf("%s \n", buf);
        goto end;
    }

end:  ezxml_free(xmlHead);
      return rc;
    
}
void              omci_cfg_print(void){

    bool bValue; 
    UINT32 iValue;

    if (omci_cfg_get_isVoiceSupported(&bValue) == CFG_XML_OK){
        printf("Tag %s value %d \n", TAG_voiceSupport, bValue);
    }
    if (omci_cfg_get_isIpHostDataServiceSupported(&bValue) == CFG_XML_OK) {
        printf("Tag %s value %d \n", TAG_ipHostDataSupport, bValue);
    }
    if(omci_cfg_get_isOntSFU(&bValue) == CFG_XML_OK){
        printf("Tag %s value %d \n", TAG_ontTypeSfu, bValue);
    }
    if(omci_cfg_get_numEthernetPorts(&iValue) == CFG_XML_OK){
        printf("Tag %s %s value %d \n", TAG_ethernetInfo, ATTR_numPorts , iValue);
    }
    if (omci_cfg_get_numSlotEthernet(&iValue) == CFG_XML_OK) {
        printf("Tag %s %s value %d \n", TAG_ethernetInfo, ATTR_slotNumber,iValue);
    }
    if (omci_cfg_get_numPotsPorts(&iValue) == CFG_XML_OK ) {
        printf("Tag %s %s value %d \n", TAG_potsInfo, ATTR_numPorts , iValue);
    }
    if (omci_cfg_get_numSlotPots(&iValue) == CFG_XML_OK) {
        printf("Tag %s %s value %d \n", TAG_potsInfo, ATTR_slotNumber,iValue);
    }
    if (omci_cfg_get_numSlotAni(&iValue) == CFG_XML_OK) {
        printf("Tag %s %s value %d \n", TAG_aniInfo, ATTR_slotNumber,iValue);
    }
    if (omci_cfg_get_firstNonOmciTcontId(&iValue) == CFG_XML_OK) {
        printf("Tag %s value %d \n", TAG_firstNonOmciTcontInfo, iValue);
    }
    if(omci_cfg_get_isGemPortPmByApmActivation(&bValue) == CFG_XML_OK) {
         printf("Tag %s value %d \n", TAG_gemPortPmByApmActivation, bValue);
    }
    if(omci_cfg_get_isSkipHwPollPM(&bValue) == CFG_XML_OK) {
         printf("Tag %s value %d \n", TAG_apmPMActivation, bValue);
    }
    if(omci_cfg_get_isSkipHwPollAlarm(&bValue) == CFG_XML_OK) {
         printf("Tag %s value %d \n", TAG_apmAlarmActivation, bValue);
    }
    if(omci_cfg_get_etherType(&iValue) == CFG_XML_OK) {
         printf("Tag %s value %d \n", TAG_OMCI_ETY, iValue);
    }
    if (omci_cfg_get_isOmciCallsHW(&bValue) == CFG_XML_OK) {
        printf("Tag %s value %d \n", TAG_OMCI_HW_CALLS, bValue);
    }
    if(omci_cfg_get_isVoipSupported(&bValue) == CFG_XML_OK) {
         printf("Tag %s value %d \n", TAG_voipSupported, bValue);
    }
    if(omci_cfg_get_isVoipProtocolSip(&bValue) == CFG_XML_OK) {
         printf("Tag %s value %d \n", TAG_voipProtocolSip, bValue);
    }
    if(omci_cfg_get_isVoipConfigByOmci(&bValue) == CFG_XML_OK) {
         printf("Tag %s value %d \n", TAG_voipConfigByOmci, bValue);
    }
    
    
    

    
}
xml_error_codes_t general_get_bool(bool* ret_value, bool get_value){

    if (ret_value == NULL) {
        return CFG_XML_GEN_ERROR;
    }
    *ret_value = get_value;
    return CFG_XML_OK;
}

xml_error_codes_t general_get_int(UINT32* ret_value, UINT32 get_value){

    if (ret_value == NULL) {
        return CFG_XML_GEN_ERROR;
    }
    *ret_value = get_value;
    return CFG_XML_OK;
}

/******************************************************************************
 *
 * Function   : omci_cfg_get_isVoiceSupported
 *              
 * Description: This function returns indication whether voice is supported (1) or not (0)
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_isVoiceSupported(bool* value)
{
    return general_get_bool(value, isVoiceSupported);
}

/******************************************************************************
 *
 * Function   : omci_cfg_get_isIpHostDataServiceSupported
 *              
 * Description: This function returns indication whether ip host data service is supported (1) or not (0)
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_isIpHostDataServiceSupported(bool* value)
{
    return general_get_bool(value, isIpHostDataServiceSupported);
}

/******************************************************************************
 *
 * Function   : omci_cfg_get_isOntSFU
 *              
 * Description: This function returns indication whether device is SFU (1) or not (0)
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_isOntSFU(bool* value)
{
    return general_get_bool(value, isOntSFU);
}

/******************************************************************************
 *
 * Function   : omci_cfg_get_numEthernetPorts
 *              
 * Description: This function returns the number of ethernet ports
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_numEthernetPorts(UINT32* value)
{
    return general_get_int(value,numEthernetPorts);     
}

/******************************************************************************
 *
 * Function   : omci_cfg_get_numSlotEthernet
 *              
 * Description: This function returns the slot number of ethernet ports
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_numSlotEthernet(UINT32* value)
{
     return general_get_int(value,numSlotEthernet);     

}
/******************************************************************************
 *
 * Function   : omci_cfg_get_numPotsPorts
 *              
 * Description: This function returns the number of pots ports
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_numPotsPorts(UINT32* value)
{
    return general_get_int(value,numPotsPorts);     
}
/******************************************************************************
 *
 * Function   : omci_cfg_get_numSlotPots
 *              
 * Description: This function returns the slot number of pots ports
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_numSlotPots(UINT32* value)
{
    return general_get_int(value,numSlotPots);
}
/******************************************************************************
 *
 * Function   : omci_cfg_get_numSlotAni
 *              
 * Description: This function returns the slot number of Ani port
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_numSlotAni(UINT32* value)
{
    return general_get_int(value,numSlotAni);
}
/******************************************************************************
 *
 * Function   : omci_cfg_get_firstNonOmciTcontId
 *              
 * Description: This function returns the id of the first TCONT
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_firstNonOmciTcontId(UINT32* value)
{
    return general_get_int(value,firstNonOmciTcontId);
}
/******************************************************************************
 *
 * Function   : omci_cfg_get_etherType
 *              
 * Description: This function returns OMCI ether type 
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/
xml_error_codes_t  omci_cfg_get_etherType  (UINT32* value)
{
    return general_get_int(value,etherTypeOmciSocket);
}
/******************************************************************************
 *
 * Function   : omci_cfg_get_isGemPortPmByApmActivation
 *              
 * Description: This function returns indication whether gem port PM is activated by APM (1) or not (0)
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_isGemPortPmByApmActivation(bool* value)
{
     return general_get_bool(value, isGemPortPmByApmActivation); 
}


/******************************************************************************
 *
 * Function   : omci_cfg_get_isOmciCallsHW
 *              
 * Description: This function returns indication OMCI calls HW (1) or not (0)
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/
xml_error_codes_t  omci_cfg_get_isOmciCallsHW (bool*   value){
    {
        return general_get_bool(value, isOmciHwCalls); 
    }

}
/******************************************************************************
 *
 * Function   : omci_cfg_get_isVoipSupported
 *              
 * Description: This function returns indication whether voip supported (1) or not (0)
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_isVoipSupported(bool* value)
{
    return general_get_bool(value, isVoipSupported); 
}
/******************************************************************************
 *
 * Function   : omci_cfg_get_isVoipProtocolSip
 *              
 * Description: This function returns indication whether voip protocol is SIP (1) or not (0)
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_isVoipProtocolSip(bool* value)
{
    return general_get_bool(value, isVoipProtocolSip); 
}


/******************************************************************************
 *
 * Function   : omci_cfg_get_isVoipConfigByOmci
 *              
 * Description: This function returns indication whether voip is configed by OMCI (1) or not (0)
 *
 * Parameters :  
 *
 * Returns    : xml_error_codes_t 
 *              
 ******************************************************************************/

xml_error_codes_t  omci_cfg_get_isVoipConfigByOmci(bool* value)
{
    return general_get_bool(value, isVoipConfigByOmci); 

}



