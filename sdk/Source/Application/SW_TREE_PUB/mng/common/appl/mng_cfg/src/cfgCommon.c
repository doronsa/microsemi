/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : OMCI                                                      **/
/**                                                                          **/
/**  FILE        : ProfileCache.c                                            **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the Profile Cache                    **/
/**                                                                          **/
/******************************************************************************
 **
 *   MODIFICATION HISTORY:
 *
 *    25Dec07     zeev  - initial version created.
 *
 ******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include "globals.h"
#include "cfg_defs.h"
#include "omci_cfg.h"



/******************************************************************************
 *
 * Function   : get_cfg_xml_file
 *
 * Description: This function initiates the xml DB population with rspect to included modules
 *
 * Parameters :
 *
 * Returns    : xml_error_codes_t
 *
 ******************************************************************************/
xml_error_codes_t get_cfg_xml_file(sfu_modules_t module, bool isDefault, char* xml_file_name){

   xml_error_codes_t rc = CFG_XML_FAIL;


   if (xml_file_name == NULL) {
       return rc;
   }

    switch(module){
    case CFG_MODULE_TPM:
        #ifdef TPM_XML_FILE
        strcpy(xml_file_name, (isDefault == TRUE? TPM_XML_DEFAULT_FILE : TPM_XML_FILE));
        rc = CFG_XML_OK;
        #endif
        break;
    case CFG_MODULE_MNG_COM:
        #ifdef MNG_COM_XML_FILE
        strcpy(xml_file_name, (isDefault == TRUE? MNG_COM_XML_DEFAULT_FILE : MNG_COM_XML_FILE));
        rc = CFG_XML_OK;
        #endif
        break;
    case CFG_MODULE_APM:
        #ifdef APM_XML_FILE
        strcpy(xml_file_name, (isDefault == TRUE? APM_XML_DEFAULT_FILE : APM_XML_FILE));
        rc = CFG_XML_OK;
        #endif
        break;
    case CFG_MODULE_OMCI:
        #ifdef OMCI_XML_FILE
        strcpy(xml_file_name, (isDefault == TRUE? OMCI_XML_DEFAULT_FILE : OMCI_XML_FILE));
        rc = CFG_XML_OK;
        #endif
        break;
    case CFG_MODULE_OAM:
        #ifdef OAM_XML_FILE
        strcpy(xml_file_name, (isDefault == TRUE? OAM_XML_DEFAULT_FILE : OAM_XML_FILE));
        rc = CFG_XML_OK;
        #endif
        break;
    case CFG_MODULE_MMP:
        #ifdef MMP_XML_FILE
        strcpy(xml_file_name, (isDefault == TRUE? MMP_XML_DEFAULT_FILE : MMP_XML_FILE));
        rc = CFG_XML_OK;
        #endif
        break;
    case CFG_MODULE_FLASH:
        #ifdef FLASH_XML_FILE
        strcpy(xml_file_name, (isDefault == TRUE? FLASH_XML_DEFAULT_FILE : FLASH_XML_FILE));
        rc = CFG_XML_OK;
        #endif
        break;
    case CFG_MODULE_MIDWARE:
        #ifdef MIDWARE_XML_FILE
        strcpy(xml_file_name, (isDefault == TRUE? MIDWARE_XML_DEFAULT_FILE : MIDWARE_XML_FILE));
        rc = CFG_XML_OK;
        #endif
        break;
    }

    return rc;
}

/******************************************************************************
 *
 * Function   : startup_cfg_init
 *
 * Description: This function initiates the xml DB population with rspect to included modules
 *
 * Parameters :
 *
 * Returns    : int
 *
 ******************************************************************************/

xml_error_codes_t startup_cfg_init (void)
{
    xml_error_codes_t rc = CFG_XML_OK;

    rc = omci_cfg_init();
    omci_cfg_print();

    return rc;

    #ifdef TPM_CLI
    /*rc = tpm_cfg_init();*/
    #endif

    #ifdef MNG_COM_CLI
    /*rc = mng_com_cfg_init();*/
    #endif

    #ifdef APM_CLI
    /*rc = apm_cfg_init();*/
    #endif

    #ifdef OMCI_CLI
    rc = omci_cfg_init();
    omci_cfg_print();
    #endif

    #ifdef OAM_CLI
    /*rc = oam_cfg_init();*/
    #endif

    #ifdef MMP_CLI
    /*rc = mmp_cfg_init();*/
    #endif

    #ifdef FLASH_CLI
    /*rc = flash_cfg_init();*/
    #endif

    #ifdef MIDWARE_CLI
    /*rc = midware_cfg_init();*/
    #endif

    return rc;

}

/******************************************************************************
 *
 * Function   : scan_for_integer
 *
 * Description: This function scans a hex/decimal integer (TBD- error in scan)
 *
 * Parameters :
 *
 * Returns    : OMCIPROCSTATUS
 *
 ******************************************************************************/

UINT32 scan_for_integer(const char  *arg)
{
    UINT32  val = 0;

    if ((arg[1] == 'x') || (arg[1] == 'X'))
        sscanf(&arg[2], "%x", &val);
    else
        sscanf(arg, "%d", &val);

    return val;
}


/******************************************************************************
 *
 * Function   : get_attribute_numeric_value
 *
 * Description: This function returns the numeric value of a given attribute name in an XML element
 *
 * Parameters :
 *
 * Returns    : xml_error_codes_t
 *
 ******************************************************************************/

xml_error_codes_t get_attribute_numeric_value(ezxml_t parentElement, char *childElmName, char *attrName, UINT32 *value, char *buf)
{
    const char *attrValueString;
    ezxml_t     xmlElement;

    if ((xmlElement = ezxml_get(parentElement, childElmName, -1)) == 0)
    {
        sprintf(buf, "ezxml_get() of %s failed", childElmName);
        return CFG_XML_FAIL;
    }
    if ((attrValueString = ezxml_attr(xmlElement, attrName)) != 0)
    {
        *value = scan_for_integer(attrValueString);
        return CFG_XML_OK;
    }
    else
    {
        return CFG_XML_GEN_ERROR;
    }
}


/******************************************************************************
 *
 * Function   : get_tag_numeric_value
 *
 * Description: This function finds child element and returns value - [non attribute]
 *
 * Parameters :
 *
 * Returns    : OMCIPROCSTATUS
 *
 ******************************************************************************/

xml_error_codes_t get_tag_numeric_value(ezxml_t parentElement, char *childElmName, UINT32 *value, char *buf)
{
    ezxml_t     xmlElement;

    if ((xmlElement = ezxml_get(parentElement, childElmName, -1)) == 0)
    {
        sprintf(buf, "ezxml_get() of %s failed", childElmName);
        return CFG_XML_FAIL;
    }
    else
    {
        if (xmlElement->txt != 0)
        {
            *value = scan_for_integer(xmlElement->txt);
        }
        else
        {
            sprintf(buf, "xmlElement->txt of %s is NULL", childElmName);
            return CFG_XML_GEN_ERROR;
        }
    }

    return CFG_XML_OK;
}



/******************************************************************************
 *
 * Function   : get_attribute_boolean_value
 *
 * Description: This function reads in child and returns value of attrName as bool
 *
 * Parameters :
 *
 * Returns    : xml_error_codes_t
 *
 ******************************************************************************/

xml_error_codes_t get_attribute_boolean_value(ezxml_t parentElement, char *childElmName, char *attrName, bool *value, char *buf)
{
    ezxml_t     xmlElement;
    const char  *attr_str;

    if ((xmlElement = ezxml_get(parentElement, childElmName, -1)) == 0)
    {
        sprintf(buf, "ezxml_get() of %s failed", childElmName);
        return CFG_XML_FAIL;
    }
    else
    {
        if ((attr_str = ezxml_attr(xmlElement, attrName)) != 0)
        {
            if (strcmp(attr_str, "true") == 0)
            {
                *value = true;
            }
            else if (strcmp(attr_str, "false") == 0)
            {
                *value = false;
            }
            else
            {
                sprintf(buf, "attribute %s has invalid value '%s'", attrName, attr_str);
                return CFG_XML_GEN_ERROR;
            }
        }
        else
        {
            sprintf(buf, "ezxml_get() of %s failed", attrName);
            return CFG_XML_FAIL;
        }
    }

    return CFG_XML_OK;
}


