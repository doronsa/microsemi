/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************** 
*      mtrace.c                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:                                                      
*                                                                                
* DATE CREATED: July 4, 2010     
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.1.1.1 $                                                           
*******************************************************************************/
#include <stdio.h>
#include <stdarg.h>
#include "globals.h"
#include "mng_trace.h"

mng_module_trace_t  mngModuleTracesTable [MNG_TRACE_MODULE_LAST] =      
{                                                      
                                {MNG_TRACE_NONE, 0},                                 
    /*MNG_TRACE_MODULE_COMMON*/ {MNG_TRACE_INFO, 0}, /* No Options */
    /*MNG_TRACE_MODULE_APM*/    {MNG_TRACE_INFO, 0}, /* No Options */
    /*MNG_TRACE_MODULE_OMCI*/   {MNG_TRACE_INFO, 0}, /* No Options */
    /*MNG_TRACE_MODULE_TPMA*/   {MNG_TRACE_INFO, 0}, /* No Options */
    /*MNG_TRACE_MODULE_OAM*/    {MNG_TRACE_INFO, 0}, /* No Options */
    /*MNG_TRACE_MODULE_CTC*/    {MNG_TRACE_INFO, 0}, /* No Options */
    /*MNG_TRACE_MODULE_PON*/    {MNG_TRACE_INFO, 0}, /* No Options */
};

char* moduleDesc[MNG_TRACE_MODULE_LAST] = 
{
    "", 
    "Common", 
    "APM", 
    "OMCI", 
    "TPMA", 
    "OAM", 
    "CTC", 
    "PON", 
};

char* traceLevelDesc[4] = 
{
    "", 
    "ERROR", 
    "INFO", 
    "DEBUG" 
};

/********************************************************************************/
/*                          mngTracePrintModuleStatus                           */
/********************************************************************************/
E_ErrorCodes    mngTracePrintModuleStatus (void)
{
    UINT32 index;
    UINT32 printLevel;   
    UINT32 moduleOptions;
    
    printf("====================================\n");
    printf("Module        Level       Options   \n");
    printf("====================================\n");

    for (index = 1; index < MNG_TRACE_MODULE_LAST; index++) 
    {
        mngTraceGetModuleStatus (index, &printLevel, &moduleOptions);
        printf("%10s    %6s    0x%08x\n", moduleDesc[index], traceLevelDesc[printLevel], moduleOptions);
    }
    
    return(IAMBA_OK);
}

/********************************************************************************/
/*                          mngTracePrintModuleList                             */
/********************************************************************************/
E_ErrorCodes    mngTracePrintModuleList (void)
{
    UINT32 index;
    
    printf("================================\n");
    printf("     Module          Submodule    \n");
    printf("================================\n");

    for (index = 1; index < MNG_TRACE_MODULE_LAST; index++) 
    {
        printf("%d  %10s ", index, moduleDesc[index]);

        switch (index) 
        {
        case MNG_TRACE_MODULE_OMCI:
            printf("  %10s (0x%4.4x)\n", "Base", MNG_TRACE_OMCI_BASE_ATTR);
            printf("  %24s (0x%4.4x)\n", "DP Adapter", MNG_TRACE_DP_ADAPTER_ATTR);
            printf("  %24s (0x%4.4x)\n", "Translation layer", MNG_TRACE_OMCI_TL_ATTR);
            break;

        case MNG_TRACE_MODULE_OAM:
            printf("  %10s (0x%4.4x)\n", "Base", MNG_TRACE_OAM_BASE_ATTR);
            printf("  %24s (0x%4.4x)\n", "Discovery", MNG_TRACE_OAM_DISCOVERY_ATTR);
            break;

        default:
            printf("\n");
            break;
        }
    }
    
    return(IAMBA_OK);
}


/********************************************************************************/
/*                          mngTraceResetModuleStatus                           */
/********************************************************************************/
E_ErrorCodes    mngTraceResetModuleStatus (void)
{
    UINT32 index;
    
    for (index = 1; index < MNG_TRACE_MODULE_LAST; index++) 
    {
        mngModuleTracesTable[index].moduleTraceLevel = MNG_TRACE_INFO;
        mngModuleTracesTable[index].moduleOptions    = 0;        
    }
    
    return(IAMBA_OK);
}



/********************************************************************************/
/*                          mngTraceCheckModuleStatus                           */
/********************************************************************************/
static E_ErrorCodes    mngTraceCheckModuleStatus  (UINT32 printLevel, 
                                                   UINT32 moduleOptions)
{
    UINT32    module;
    UINT32    options;
    UINT32    modulePrintLevel;
    UINT32    modulePrintOptions;
    BOOL      printLevelInd = FALSE;
    BOOL      printOptionsInd = FALSE;

    /* input params */
    module  =  moduleOptions & MNG_TRACE_MODULE_MASK;
    options =  (moduleOptions >> MNG_TRACE_MODULE_SHIFT) & MNG_TRACE_OPTIONS_MASK;
    
    /* current module params */
    modulePrintLevel   = mngModuleTracesTable[module].moduleTraceLevel;
    modulePrintOptions = mngModuleTracesTable[module].moduleOptions;
 
    /* check print level */
    if (printLevel <= modulePrintLevel) 
        printLevelInd = TRUE;
    
    /* check module options */
    if (options == 0) 
        printOptionsInd = TRUE; /* no options */
    else
    {
        if (options & ((modulePrintOptions >> MNG_TRACE_MODULE_SHIFT) & MNG_TRACE_OPTIONS_MASK)) 
            printOptionsInd = TRUE;
    }
    
    if ((printLevelInd == TRUE) && (printOptionsInd == TRUE)) 
        return(IAMBA_OK);
    else                                                             
        return(IAMBA_ERR_GENERAL);
}

/********************************************************************************/
/*                                   mvqprintf                                  */
/********************************************************************************/
E_ErrorCodes    mvqprintf (UINT32 level, UINT32 bitMask, const char *format, ...)
{
    E_ErrorCodes  rcode;
    char          buf[256];
    va_list       argptr;
    
    /* check module printing status */
    rcode = mngTraceCheckModuleStatus(level, bitMask);
    
    if (rcode == IAMBA_OK) 
    {
        /* build message */
        va_start(argptr, format);
        vsnprintf(buf, sizeof(buf), format, argptr);
        va_end(argptr);     

        /* print message */
        switch (level) 
        {
        case MNG_TRACE_ERROR:
            printf("%s\n", buf);
            break;
        case MNG_TRACE_INFO:
            printf("%s", buf);
            break;
        case MNG_TRACE_DEBUG:
        default:
            printf("%s", buf);
            break;
        }
    }
    
    return(IAMBA_OK); 
}

E_ErrorCodes mngTraceChangeModuleStatus (UINT32 module, 
                                         UINT32 printLevel, 
                                         UINT32 moduleOptions)
{
    if (module >= MNG_TRACE_MODULE_LAST)
        return IAMBA_ERR_GENERAL;

    mngModuleTracesTable[module].moduleTraceLevel = printLevel;
    mngModuleTracesTable[module].moduleOptions    = moduleOptions;
   
    return(IAMBA_OK);
}

E_ErrorCodes mngTraceGetModuleStatus (UINT32 module, 
                                      UINT32 *printLevel, 
                                      UINT32 *moduleOptions)
{
    if (module >= MNG_TRACE_MODULE_LAST)
        return IAMBA_ERR_GENERAL;

    *printLevel    = mngModuleTracesTable[module].moduleTraceLevel;
    *moduleOptions = mngModuleTracesTable[module].moduleOptions;

    return(IAMBA_OK);
}

