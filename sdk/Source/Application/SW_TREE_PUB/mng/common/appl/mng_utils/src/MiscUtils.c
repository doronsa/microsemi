/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON                                                      **/
/**                                                                          **/
/**  FILE        : MiscUtils.c                                               **/
/**                                                                          **/
/**  DESCRIPTION : This file contains utility functions for mapped           **/
/**                enumerations, hex dump                                    **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    30-Jul-09   zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "globals.h"
#include "MiscUtils.h"


static char unknownStr[] = "<unknown>";


/******************************************************************************
 *
 * Function   : enumToStrLookup
 *              
 * Description: This function looks the the string
 *              
 * Parameters :  
 *
 * Returns    : char *
 *              
 ******************************************************************************/

char *enumToStrLookup(EnumMap *penummap, int enumValue)
{
    int indx;
    
    for (indx=0; indx<penummap->numEntries; indx++)
    {
        if (enumValue == penummap->mapEntryAra[indx].enumValue)
            return penummap->mapEntryAra[indx].enumStr;
    }
    return unknownStr;
}



/******************************************************************************
 *
 * Function   : dumpArrayHex 
 *              
 * Description: This function prints out a hex dump 
 *              
 * Parameters : araSize <= 80
 *
 * Returns    : void
 *              
 ******************************************************************************/

void dumpArrayHex(UINT8 *ara, int araSize)
{
    int  indx;
    char buf[250];
    
    // The *3 is because the number of print chars per byte is 3 - space + 2 hex print chars    
    for (indx=0; indx < araSize; indx++)
    {
        sprintf(&buf[indx*3], " %02X", ara[indx]);
    }
    // Make sure the string is terminated
    buf[araSize*3] = 0;
    printf( "%s", buf);
}




/******************************************************************************
 *
 * Function   : getN2HShort
 *              
 * Description: This function does ntohs/htons in place 
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

void getN2HShort(void *srcValPtr, void *destValPtr)
{
    UINT16 src;
    UINT16 dest;
    
    memcpy(&src, srcValPtr, sizeof(src));
    dest = ntohs(src);
    memcpy(destValPtr, &dest, sizeof(dest));
}

/******************************************************************************
 *
 * Function   : setShortH2N
 *              
 * Description: This function does ntohs/htons in place 
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

void setShortH2N(void *destValPtr, void *srcValPtr)
{
    UINT16 src;
    UINT16 dest;
    
    memcpy(&src, srcValPtr, sizeof(src));
    dest = htons(src);
    memcpy(destValPtr, &dest, sizeof(dest));
}


/******************************************************************************
 *
 * Function   : getN2HLong 
 *              
 * Description: This function does ntohl/htonl in place 
 *              
 * Parameters :  
 *
 * Returns    : void
 *              
 ******************************************************************************/

void getN2HLong(void *srcValPtr, void *destValPtr)
{
    UINT32 src;
    UINT32 dest;
    
    memcpy(&src, srcValPtr, sizeof(src));
    dest = ntohl(src);
    memcpy(destValPtr, &dest, sizeof(dest));
}




