/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
#include "errorCode.h"
#include "globals.h"
#include "ListUtils.h"
#include "qprintf.h"


/******************************************************************************
 *
 * Function   : listInit - 
 *              
 * Description: This function initializes the list
 *              
 * Parameters:  OMLIST *pList
 *
 * Returns    : void
 *              
 ******************************************************************************/
  
void 	listInit (OMLIST *pList, int max)
{
    pList->node.next     = 0;
    pList->node.previous = 0;
    pList->count         = 0;
    pList->max           = max;
}



/******************************************************************************
 *
 * Function   : listFirst - 
 *              
 * Description: This function retturns the first node in the list
 *              
 * Parameters:  OMLIST *pList
 *
 * Returns    : OMNODE *
 *              
 ******************************************************************************/
  
OMNODE* listFirst (OMLIST *pList)
{
    return pList->node.next;
}



/******************************************************************************
 *
 * Function   : listNext - 
 *              
 * Description: This function returns the adjacent node (forward)
 *              
 * Parameters:  OMNODE *pNode
 *
 * Returns    : OMNODE *
 *              
 ******************************************************************************/
  
OMNODE* listNext (OMNODE *pNode)
{
    return pNode->next;
}



/******************************************************************************
 *
 * Function   : listPrevious - 
 *              
 * Description: This function returns the adjacent node (backward)
 *              
 * Parameters:  OMNODE *pNode
 *
 * Returns    : OMNODE *
 *              
 ******************************************************************************/
  
OMNODE* listPrevious (OMNODE *pNode)
{
    return pNode->previous;
}



/******************************************************************************
 *
 * Function   : listAdd - 
 *              
 * Description: This function adds a node to a list
 *              
 * Parameters:  OMLIST *pList
 *              OMNODE *pNode
 *
 * Returns    : int
 *              new list count if succeed else 0.
 ******************************************************************************/
  
int   listAdd (OMLIST *pList, OMNODE *pNode)
{
    OMNODE *pLastNode = &pList->node;
    
    if (pList->count >= pList->max)
    {
        return 0;
    }

    while (pLastNode->next != 0)
    {
	pLastNode = pLastNode->next;
    }

    pLastNode->next = pNode;
    pNode->previous = pLastNode;
    pNode->next     = 0;

    return (++pList->count);
}


/******************************************************************************
 *
 * Function   : listAddFirst - 
 *              
 * Description: This function adds a node to a list
 *              
 * Parameters:  OMLIST *pList
 *              OMNODE *pNode
 *
 * Returns    : void
 *              
 ******************************************************************************/
void 	listAddFirst (OMLIST *pList, OMNODE *pNode)
{
    OMNODE *pFirstNode = listFirst (pList);
    
    pList->node.next     = pNode;
    pNode->previous      = &(pList->node);
    pNode->next          = pFirstNode;
    
    if (pFirstNode != 0)
        pFirstNode->previous = pNode;
}

/******************************************************************************
 *
 * Function   : listDelete - 
 *              
 * Description: This function deletes a node from a list
 *              
 * Parameters:  OMLIST *pList
 *              OMNODE *pNode
 *
 * Returns    : void
 *              
 ******************************************************************************/
  
void 	listDelete (OMLIST *pList, OMNODE *pNode)
{
    OMNODE *pPrevNode = pNode->previous;
    OMNODE *pNextNode = pNode->next;

    pPrevNode->next = pNextNode;

    if (pNextNode != 0)
	pNextNode->previous = pPrevNode;

    pList->count--;
}


/******************************************************************************
 *
 * Function   : listDeleteFirst - 
 *              
 * Description: This function deletes the first node from a list
 *              
 * Parameters:  OMLIST *pList
 *
 * Returns    : OMNODE *
 *              
 ******************************************************************************/
  
OMNODE* listDeleteFirst (OMLIST *pList)
{   
    OMNODE *pFirstNode = listFirst (pList);
    
    listDelete (pList, pFirstNode);
    
    return pFirstNode;
    
 /* OMNODE *pFirstNode  = pList->node.next;
    OMNODE *pSecondNode = pFirstNode->next;
    
    pList->node.next = pSecondNode;
    
    if (pFirstNode->next != 0)
        pSecondNode->previous = pList->node;

    pList->count--;
    
    return pFirstNode; */
}


/******************************************************************************
 *
 * Function   : listCount - 
 *              
 * Description: This function returns the number of nodes i a list
 *              
 * Parameters:  OMLIST *pList
 *
 * Returns    : int
 *              
 ******************************************************************************/
  
int 	listCount (OMLIST *pList)
{
    return pList->count;
}


#if 0

static void printList(OMLIST *pList)
{
    OMNODE *pNode = &pList->node;

    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, 
	     "count = %d\n", pList->count);

    while (pNode->next != 0)
    {
	qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, 
	     "node = 0x%x\n", pNode->next);

	pNode = pNode->next;
    }
}


static void testListUtil()
{
    OMNODE n1, n2, n3, n4, n5, n6;
    OMLIST list;
    OMLIST *pList = &list;

    listInit (pList, 20);

    printList(pList);
    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Add n1 0x%x\n", &n1);
    listAdd(pList, &n1);
    printList(pList);

    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Add n2 0x%x\n", &n2);
    listAdd(pList, &n2);
    printList(pList);

    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Add n3 0x%x\n", &n3);
    listAdd(pList, &n3);
    printList(pList);

    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Add n4 0x%x\n", &n4);
    listAdd(pList, &n4);
    printList(pList);

    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Add n5 0x%x\n", &n5);
    listAdd(pList, &n5);
    printList(pList);

    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Add n6 0x%x\n", &n6);
    listAdd(pList, &n6);
    printList(pList);


    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Delete n4 0x%x\n", &n4);
    listDelete(pList, &n4);
    printList(pList);

    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Delete n1 0x%x\n", &n1);
    listDelete(pList, &n1);
    printList(pList);

    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Delete n3 0x%x\n", &n3);
    listDelete(pList, &n3);
    printList(pList);

    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Delete n2 0x%x\n", &n2);
    listDelete(pList, &n2);
    printList(pList);

    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Delete n6 0x%x\n", &n6);
    listDelete(pList, &n6);
    printList(pList);

    qprintf( (QPRINT_MASK_INFO  | QPRINT_MASK_PRINT | MNG_MODULE) , MNG_OMCI_MODULE, "** Delete n5 0x%x\n", &n5);
    listDelete(pList, &n5);
    printList(pList);
}

#endif

