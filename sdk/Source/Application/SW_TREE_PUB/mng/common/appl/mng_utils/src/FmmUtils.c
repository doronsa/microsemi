/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      :                                                      **/
/**                                                                          **/
/**  FILE        : FmmUtils.c                            **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the MIB store and restore operation        **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    25-Nov.-2012     Thomas.wang  - initial version created.                              
 *                                                                      
 ******************************************************************************/
#include "string.h"
#include "stdlib.h"
#include "globals.h"

#include "FmmUtils.h"
#include "assert.h"
#include "OsGlueLayer.h"

/******************************************************************************
 *  Function:    mfmm_create
 * 
 *  Description: The function create fmm for application 
 *     
 *
 * Parameters :   mfmmInfoCreate: fmm struct data
 *                      mfmmId: fmm No.
 *                      defaultState: default state 
 *                      maxState: max state
 *                      maxEvent: max event
 *
 *  Returns:     FMM_OK
 *
 ******************************************************************************/
UINT8 mfmm_create(FMM_INFO_S *mfmmInfoCreate, FMM_ID mfmmId, UINT8 defaultState, 
                                  UINT8 maxState, UINT8 maxEvent)
{
    memset(&(mfmmInfoCreate[mfmmId]), 0, sizeof(FMM_INFO_S));
    mfmmInfoCreate[mfmmId].currentState = defaultState;    
    
    mfmmInfoCreate[mfmmId].pHandlers = (FMM_EVTHANDLER*)malloc(sizeof(FMM_EVTHANDLER) * maxState * maxEvent);
    if(NULL == mfmmInfoCreate[mfmmId].pHandlers)
    {
        printf("\n%s:line=%d, apply space unsuccessfully", __FUNCTION__, __LINE__);
    }
    
    mfmmInfoCreate[mfmmId].maxState = maxState;
    mfmmInfoCreate[mfmmId].maxEvent = maxEvent;

    mfmmInfoCreate[mfmmId].valid = TRUE;
        
    return FMM_OK;
}

/*******************************************************************************
 *  Function:    mfmm_set_handler
 * 
 *  Description: The function set fmm hanlder.
 *     
 *
 * Parameters :   mfmmData: fmm struct data
 *                      mfmmId:fmm ID
 *                      state: fmm state
 *                      event:fmm event
 *                      handler:fmm handler
 *
 *  Returns:     FMM_OK
 *
 ******************************************************************************/
UINT8 mfmm_set_handler(FMM_INFO_S *mfmmData,FMM_ID mfmmId, UINT8 state, UINT8 event, FMM_EVTHANDLER handler)
{    
    if((NULL == mfmmData) || (NULL == mfmmData[mfmmId].pHandlers))
    {
        printf("\n%s:line=%d, pointer is NULL", __FUNCTION__, __LINE__);
    }

    if(event > mfmmData[mfmmId].maxEvent)
    {
        printf("\n%s:line=%d, event is not right", __FUNCTION__, __LINE__);
    }
    
    if(state > mfmmData[mfmmId].maxState)
    {
        printf("\n%s:line=%d, state is not right", __FUNCTION__, __LINE__);
    }

    *(mfmmData[mfmmId].pHandlers + state * mfmmData[mfmmId].maxEvent + event) = handler;

    return FMM_OK;
}

/*******************************************************************************
 *  Function:    mfmm_get_handler
 * 
 *  Description: The function set fmm hanlder.
 *     
 *
 * Parameters :   mfmmInfo: fmm struct data
 *                      mfmmId:fmm ID
 *                      state: fmm state
 *                      event:fmm event
 *
 *  Returns:     FMM_EVTHANDLER*
 *
 ******************************************************************************/
FMM_EVTHANDLER mfmm_get_handler(FMM_INFO_S* mfmmInfo,FMM_ID mfmmId, UINT8 state, UINT8 event)
{
    if((NULL == mfmmInfo) || (NULL == mfmmInfo[mfmmId].pHandlers))
    {
        printf("\n%s:line=%d, pointer is NULL", __FUNCTION__, __LINE__);
    }

    if(event > mfmmInfo[mfmmId].maxEvent)
    {
        printf("\n%s:line=%d, event is not right", __FUNCTION__, __LINE__);
    }
    
    if(state > mfmmInfo[mfmmId].maxState)
    {
        printf("\n%s:line=%d, state is not right", __FUNCTION__, __LINE__);
    }

    return *(mfmmInfo[mfmmId].pHandlers + state * mfmmInfo[mfmmId].maxEvent + event);
}

/*******************************************************************************
 *  Function:    mfmm_get_info
 * 
 *  Description: The function get information.
 *     
 *
 * Parameters :   mfmmId: fmm ID
 *                      FsmInfomation:fmm struct data.
 *
 *  Returns:     FMM_INFO_S*
 *
 ******************************************************************************/
FMM_INFO_S* mfmm_get_info(FMM_ID mfmmId, FMM_INFO_S *FsmInfomation)
{
    if(FALSE == FsmInfomation[mfmmId].valid)
    {
        printf("\n%s:line=%d, the state machine is not exist.", __FUNCTION__, __LINE__);
    }

    return &(FsmInfomation[mfmmId]);
}

/*******************************************************************************
 *  Function:    mfmm_delete_member_info
 * 
 *  Description: The function delete information.
 *     
 *
 * Parameters :   mfmmId: fmm ID
 *                      FsmKeyData:fmm struct data.
 *
 *  Returns:     FMM_OK
 *
 ******************************************************************************/
UINT8 mfmm_delete_member_info(FMM_ID mfmmId, FMM_INFO_S *FsmKeyData)
{
    
    free(FsmKeyData[mfmmId].pHandlers);
    memset(&FsmKeyData[mfmmId], 0, sizeof(FMM_INFO_S));

    return FMM_OK;
}


/*******************************************************************************
 *  Function:    mfmm_delete
 * 
 *  Description: The function delete fmm member.
 *     
 *
 * Parameters :   mfmmInfo: fmm struct data
 *                      mfmmId:   fmm ID.
 *
 *  Returns:     UINT8
 *
 ******************************************************************************/
UINT8 mfmm_delete(FMM_INFO_S *mfmmInfo, FMM_ID mfmmId)
{
    return mfmm_delete_member_info(mfmmId, mfmmInfo);
}

/*******************************************************************************
 *  Function:    mfmm_add_event_handler
 * 
 *  Description: The function add fmm handler.
 *     
 *
 * Parameters :   mfmmMainData: fmm struct data
 *                      mfmmId:fmm ID
 *                      state: fmm state
 *                      event:fmm event
 *                      handler:handler function
 *
 *  Returns:     FMM_OK
 *
 ******************************************************************************/
UINT8 mfmm_add_event_handler(FMM_INFO_S *mfmmMainData,FMM_ID mfmmId, UINT8 state, UINT8 event, FMM_EVTHANDLER handler)
{
    mfmm_set_handler(mfmmMainData, mfmmId, state, event, handler);
    
    return FMM_OK;
}

/*******************************************************************************
 *  Function:    mfmm_run
 * 
 *  Description: The function run fmm.
 *     
 *
 * Parameters :   pFmmInfo: fmm struct data
 *                      mfmmId:fmm ID.
 *                      event:fmm event
 *                      pEvtArg:struct data
 *
 *  Returns:     UINT8
 *
 ******************************************************************************/
UINT8 mfmm_run(FMM_INFO_S*pFmmInfo, FMM_ID mfmmId, UINT8 event, void* pEvtArg)
{
    UINT8         i = 0;
    UINT8         newState = 0;
    FMM_EVTHANDLER handler;

    if((NULL == pFmmInfo) || (NULL == pFmmInfo[mfmmId].pHandlers))
    {
        printf("\n%s:line=%d, pointer is NULL", __FUNCTION__, __LINE__);
    }

    if(event > pFmmInfo[mfmmId].maxEvent)
    {
        printf("\n%s:line=%d, event=%d,event is not right", __FUNCTION__, __LINE__);
    }
    
    if(pFmmInfo[mfmmId].currentState > pFmmInfo[mfmmId].maxState)
    {
        printf("\n%s:line=%d, currentstate is not right", __FUNCTION__, __LINE__);
    }

    newState = pFmmInfo[mfmmId].currentState;
    handler = mfmm_get_handler(pFmmInfo, mfmmId, pFmmInfo[mfmmId].currentState, event);

    if (handler)
    {
        newState = (*handler)(pEvtArg);
    }
    
    if (newState != pFmmInfo[mfmmId].currentState)
    {
        pFmmInfo[mfmmId].currentState = newState;
    }
   
    return newState;
}

/*******************************************************************************
 *  Function:    mfmm_get_current_state
 * 
 *  Description: The function get fmm current state
 *     
 *
 * Parameters :   pFmmInfo: fmm struct data
 *                      mfmmId:fmm ID.
 *
 *  Returns:     FMM_INFO_S*
 *
 ******************************************************************************/
UINT8 mfmm_get_current_state(FMM_INFO_S* pFmmInfo, FMM_ID mfmmId)
{
    if(NULL == pFmmInfo)
    {
        printf("\n%s:line=%d, pointer is NULL", __FUNCTION__, __LINE__);
    }

    return pFmmInfo[mfmmId].currentState;
}


/*******************************************************************************
 *  Function:    mfmm_show
 * 
 *  Description: The function get information.
 *     
 *
 * Parameters :  mfmmInfo: fmm struct data
 *                     mfmmId:fmm ID.
 *
 *  Returns:     FMM_INFO_S*
 *
 ******************************************************************************/
void mfmm_show(FMM_INFO_S *mfmmInfo, FMM_ID mfmmId)
{
    UINT32 index = 0;
    
    printf("ID MaxState MaxEvent CurState\r\n");
    for (index = 0; index < mfmmId; index++)
    {
        if (FALSE == mfmmInfo[index].valid)
        {
            continue;
        }
        printf("%-2d %-8d %-8d %-8d\r\n",index, mfmmInfo[index].maxState, 
                                                  mfmmInfo[index].maxEvent, mfmmInfo[index].currentState);
    }
}

/*******************************************************************************
 *  Function:    mfmm_get_info
 * 
 *  Description: The function get information.
 *     
 *
 * Parameters :   mfmmData: fmm struct data
 *                      FsmInfomation:get fmm information.
 *
 *  Returns:     FMM_INFO_S*
 *
 ******************************************************************************/
UINT8 mfmm_show_handlers(FMM_INFO_S* pFsmValue, FMM_ID mfmmId)
{
    UINT32 index = 0;
    UINT8 count =0;
    
    if(NULL == pFsmValue)
    {
        printf("\n%s:line=%d, pointer is NULL", __FUNCTION__, __LINE__);
    }

    printf("\r\n");
    for (index = 0; index < pFsmValue[mfmmId].maxState; index++)
    {
        for (count = 0; count < pFsmValue[mfmmId].maxEvent; count++)
        {
            printf("%p ", (void*)mfmm_get_handler(pFsmValue, mfmmId, index, count));
        }
        printf("\r\n");
    }

    return FMM_OK;
}


