/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : EPON                                                      **/
/**                                                                          **/
/**  FILE        : MiscUtils.c                                               **/
/**                                                                          **/
/**  DESCRIPTION : This file conatains declarations for utiliy functions etc **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    30-Jul-09   zeev  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __INCMiscUtilsh
#define __INCMiscUtilsh

#include <netinet/in.h>

typedef struct
{
    int  enumValue;
    char *enumStr;
} MapEntry;


typedef struct
{
    int      numEntries;
    MapEntry *mapEntryAra;
} EnumMap;

char *enumToStrLookup(EnumMap *penummap, int enumValue);


void dumpArrayHex(UINT8 *ara, int araSize);

void getN2HShort(void *srcValPtr, void *destValPtr);
void getN2HLong (void *srcValPtr, void *destValPtr);
void setShortH2N(void *destValPtr, void *srcValPtr);


#endif
