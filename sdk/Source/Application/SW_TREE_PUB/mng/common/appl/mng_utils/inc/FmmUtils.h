/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      :                                                      **/
/**                                                                          **/
/**  FILE        : FmmUtils.h                            **/
/**                                                                          **/
/**  DESCRIPTION : This file implements the MIB store and restore operation        **/
/**                                                                          **/
/******************************************************************************
 **                                                                          
 *   MODIFICATION HISTORY:                                                   
 *                
 *    25-Nov.-2012     Thomas.wang  - initial version created.                              
 *                                                                      
 ******************************************************************************/

#ifndef __FMM_UTILS_H__
#define __FMMUTILS_H__

#ifdef  __cplusplus
extern "C" {
#endif



#ifndef NULL
#define NULL (0)
#endif

typedef INT32 FMM_ID;

typedef INT32 (*FMM_EVTHANDLER)(void* pEvtArg);

typedef enum
{
    FMM_ERROR = 0,
    FMM_OK = 1,    
}FMM_RESULT_VALUE;

typedef struct 
{
    UINT32 currentState;
    UINT32 maxState;
    UINT32 maxEvent;
    FMM_EVTHANDLER* pHandlers;
    BOOL valid;
} FMM_INFO_S;

/******************************************************************************
 *  Function:    mfmm_create
 * 
 *  Description: The function create fmm for application 
 *     
 *
 * Parameters :   mfmmInfoCreate: fmm struct data
 *                      mfmmId: fmm No.
 *                      defaultState: default state 
 *                      maxState: max state
 *                      maxEvent: max event
 *
 *  Returns:     FMM_OK
 *
 ******************************************************************************/
UINT8 mfmm_create(FMM_INFO_S *mfmmInfoCreate, FMM_ID mfmmId, UINT8 defaultState, UINT8 maxState, UINT8 maxEvent);

/*******************************************************************************
 *  Function:    mfmm_delete
 * 
 *  Description: The function delete fmm member.
 *     
 *
 * Parameters :   mfmmInfo: fmm struct data
 *                      mfmmId:   fmm ID.
 *
 *  Returns:     UINT8
 *
 ******************************************************************************/
UINT8 mfmm_delete(FMM_INFO_S *mfmmInfo, FMM_ID mfmmId);

/*******************************************************************************
 *  Function:    mfmm_get_current_state
 * 
 *  Description: The function get fmm current state
 *     
 *
 * Parameters :   pFmmInfo: fmm struct data
 *                      mfmmId:fmm ID.
 *
 *  Returns:     FMM_INFO_S*
 *
 ******************************************************************************/
UINT8 mfmm_get_current_state(FMM_INFO_S* pFmmInfo, FMM_ID mfmmId);

/*******************************************************************************
 *  Function:    mfmm_add_event_handler
 * 
 *  Description: The function add fmm handler.
 *     
 *
 * Parameters :   mfmmMainData: fmm struct data
 *                      mfmmId:fmm ID
 *                      state: fmm state
 *                      event:fmm event
 *                      handler:handler function
 *
 *  Returns:     FMM_OK
 *
 ******************************************************************************/
UINT8 mfmm_add_event_handler(FMM_INFO_S *mfmmMainData,FMM_ID mfmmId, UINT8 state, UINT8 event, FMM_EVTHANDLER handler);


/*******************************************************************************
 *  Function:    mfmm_run
 * 
 *  Description: The function run fmm.
 *     
 *
 * Parameters :   pFmmInfo: fmm struct data
 *                      mfmmId:fmm ID.
 *                      event:fmm event
 *                      pEvtArg:struct data
 *
 *  Returns:     UINT8
 *
 ******************************************************************************/
UINT8 mfmm_run(FMM_INFO_S*pFmmInfo, FMM_ID mfmmId, UINT8 event, void* pEvtArg);

#ifdef  __cplusplus
}
#endif

#endif /* __COM_FSM_H__ */

 
