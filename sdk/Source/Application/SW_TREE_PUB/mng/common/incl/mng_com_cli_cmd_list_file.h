/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
  {"mng_com_cli_trace_print_status",        mng_com_cli_trace_print_status},
  {"mng_com_cli_trace_change_status",       mng_com_cli_trace_change_status},
  {"mng_com_cli_trace_print_module_list",   mng_com_cli_trace_print_module_list},
  {"mng_com_cli_set_pon_params",            mng_com_cli_set_pon_params},
 
 /********************************************************************************/
  /*                      Timer  functions                                        */
  /********************************************************************************/

  {"mv_os_cli_timer_create",                mv_os_cli_timer_create},
  {"mv_os_cli_timer_start",                 mv_os_cli_timer_start},
  {"mv_os_cli_timer_stop",                  mv_os_cli_timer_stop},
  {"mv_os_cli_timer_del",                   mv_os_cli_timer_del},
  {"mv_os_cli_timer_deb_act",               mv_os_cli_timer_deb_act},
  {"mv_os_cli_timer_deb_deact",             mv_os_cli_timer_deb_deact},
  {"mv_os_cli_timer_print_full",            mv_os_cli_timer_print_full},
  {"mv_os_cli_timer_print_valid",           mv_os_cli_timer_print_valid},
  {"mv_os_cli_timer_init",                  mv_os_cli_timer_init},
