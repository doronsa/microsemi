/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _CFG_DEFS_H_
#define _CFG_DEFS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "ezxml.h"
#include "globals.h"

/* xml cfg error codes */
typedef enum xml_error_codes
{
    CFG_XML_OK,
    CFG_XML_FAIL,
    CFG_XML_NOT_FOUND,
    CFG_XML_SET_DEFAULT,
    CFG_XML_GEN_ERROR
}xml_error_codes_t;

typedef enum sfu_modules
{
    CFG_MODULE_TPM,
    CFG_MODULE_MNG_COM,
    CFG_MODULE_APM,
    CFG_MODULE_OMCI,
    CFG_MODULE_OAM,
    CFG_MODULE_MMP,
    CFG_MODULE_FLASH,
    CFG_MODULE_MIDWARE,
    CFG_MODULE_LAST
} sfu_modules_t;


#ifdef TPM_CLI
    #define TPM_XML_FILE "/etc/xml_params/tpm_xml_cfg_file.xml"
    #define TPM_XML_DEFAULT_FILE "/etc/xml_default_params/tpm_xml_cfg_file.xml"
#endif

#ifdef MNG_COM_CLI
    #define MNG_COM_XML_FILE "/etc/xml_params/mng_com_xml_cfg_file.xml"
    #define MNG_COM_XML_DEFAULT_FILE "/etc/xml_default_params/mng_com_xml_cfg_file.xml"
#endif

#ifdef APM_CLI
    #define APM_XML_FILE "/etc/xml_params/apm_xml_cfg_file.xml"
    #define APM_XML_DEFAULT_FILE "/etc/xml_default_params/apm_xml_cfg_file.xml"
#endif

#ifdef OMCI_CLI
    #define OMCI_XML_FILE "/etc/xml_params/omci_xml_cfg_file.xml"
    #define OMCI_XML_DEFAULT_FILE "/etc/xml_default_params/omci_xml_cfg_file.xml"
#endif

#ifdef OAM_CLI
    #define OAM_XML_FILE "/etc/xml_params/oam_xml_cfg_file.xml"
    #define OAM_XML_DEFAULT_FILE "/etc/xml_default_params/oam_xml_cfg_file.xml"
#endif

#ifdef MMP_CLI
    #define MMP_XML_FILE "/etc/xml_params/mmp_xml_cfg_file.xml"
    #define MMP_XML_DEFAULT_FILE "/etc/xml_default_params/mmp_xml_cfg_file.xml"
#endif

#ifdef FLASH_CLI
    #define FLASH_XML_FILE "/etc/xml_params/flash_xml_cfg_file.xml"
    #define FLASH_XML_DEFAULT_FILE "/etc/xml_default_params/flash_xml_cfg_file.xml"
#endif

#ifdef MIDWARE_CLI
    #define MIDWARE_XML_FILE "/etc/xml_params/midware_xml_cfg_file.xml"
    #define MIDWARE_XML_DEFAULT_FILE "/etc/xml_default_params/midware_xml_cfg_file.xml"
#endif



xml_error_codes_t startup_cfg_init              (void);
xml_error_codes_t get_tag_numeric_value         (ezxml_t parentElement  , char *childElmName, UINT32 *value, char *buf);
xml_error_codes_t get_attribute_numeric_value   (ezxml_t parentElement, char *childElmName, char *attrName, UINT32 *value, char *buf);
xml_error_codes_t get_attribute_boolean_value   (ezxml_t parentElement  , char *childElmName, char *attrName, bool *value, char *buf);
xml_error_codes_t get_cfg_xml_file              (sfu_modules_t module, bool isDefault, char* xml_file_name);

#ifdef __cplusplus
}
#endif


#endif

