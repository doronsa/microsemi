/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Common                                                    **/
/**                                                                          **/
/**  FILE        : errorCode.h                                               **/
/**                                                                          **/
/**  DESCRIPTION : This file contains declaration of the application return  **/
/**                codes                                                     **/
/**                                                                          **/
/******************************************************************************/
/**                                                                           */
/**  MODIFICATION HISTORY:                                                    */
/**
 *      $Log: errorCode.h,v $
 *      Revision 1.1.1.1  2011-04-25 03:44:39  guxi
 *      Victor added Joe's version 
 *
 *      Revision 1.1.1.1  2010-09-25 03:08:24  joe
 *      add sw_tree Z2
 *
 *      Revision 1.1.1.1  2010-09-08 01:41:15  daniel
 *      add sw tree 1.3.26
 *
 *      Revision 1.1.1.1  2010-09-03 07:53:54  joe
 *      add_Z1_linux_kernel
 *
 * 
 *    Rev 1.0   May 19 2008 15:25:36   oren
 * Initial revision.
 * 
 *    Rev 1.62   Apr 29 2008 18:31:18   yuval
 * IGMP Support MRVL 6123
 * 
 *    Rev 1.61   Apr 07 2008 11:49:54   yaron
 * add not supported error for marvell switch
 * 
 *    Rev 1.60   Apr 01 2008 18:41:04   yoram
 * OCP protocol first version
 * 
 *    Rev 1.59   Mar 24 2008 09:26:10   yaron
 * add iPGL errorCode
 * 
 *    Rev 1.58   Mar 19 2008 10:55:22   yuval
 * pbit translation
 * 
 *    Rev 1.57   Jan 31 2008 11:11:00   yuval
 * Vlan Translation
 * 
 *    Rev 1.56   Jan 29 2008 18:39:56   yoram
 * Add iAPI, linux upload, linux profile parameters
 * 
 *    Rev 1.55   Dec 04 2007 11:14:46   oren
 * add switch error codes
 * 
 *    Rev 1.54   Nov 27 2007 10:07:52   yuval
 * Add IP Host service
 * 
 *    Rev 1.53   Nov 13 2007 11:27:40   oren
 * add FPGA driver return codes, and switch error codes
 * 
 *    Rev 1.52   Oct 15 2007 09:59:54   yuval
 * Enhanced Vlan support - without Vlan translation
 * 
 *    Rev 1.51   Jun 12 2007 09:52:58   yoram
 * IGMP V3 parsing
 * 
 *    Rev 1.49   May 01 2007 12:02:00   lab
 * add IPTC error codes
 * 
 *    Rev 1.48   Apr 29 2007 18:30:24   oren
 * add iptv errorcodes
 * 
 *    Rev 1.47   Apr 29 2007 14:59:16   yoramb
 * Add DMA ErrorCodes
 * 
 *    Rev 1.46   Apr 29 2007 14:07:34   yuval
 * Added dpm sm errors
 * 
 *    Rev 1.45   Apr 11 2007 12:41:04   yuval
 * dpm additions
 * 
 *    Rev 1.44   Apr 11 2007 09:54:40   yuval
 * additions
 * 
 *    Rev 1.43   Mar 29 2007 16:21:02   yuval
 * update
 * 
 *    Rev 1.42   Mar 15 2007 17:42:46   yuval
 * Data Path Manager + FPGA Module extention
 * 
 *    Rev 1.41   Mar 11 2007 18:23:08   yoram
 * 3CPU
 * 
 *    Rev 1.40   Jan 08 2007 16:13:52   yoram
 * change memp project and make osip use memp pool
 * 
 *    Rev 1.39   Dec 07 2006 10:45:28   yuval
 * IPTV module addition
 * 
 *    Rev 1.38   Nov 15 2006 16:15:06   oren
 * add data mng and line mng error codes
 * 
 *    Rev 1.37   Nov 02 2006 14:29:10   avnerf
 * Support ONT traffic Manager codes
 * 
 *    Rev 1.36   Sep 04 2006 10:19:40   avnerf
 * Add Illegal SN error code
 * 
 *    Rev 1.35   May 31 2006 09:38:52   avnerf
 * Add error code to bw check
 * 
 *    Rev 1.34   May 24 2006 16:05:28   oren-new
 * add   ERR_EVENT_NOT_PRESENT, Event not present while get event no suspend                            
 * 
 *    Rev 1.33   May 18 2006 15:23:12   yoram
 * add   ERR_TASK_PRIO error code
 * 
 *    Rev 1.32   May 04 2006 14:20:26   yoram
 * when creatting a pool check that memory for the new pool is available.
 * 
 *    Rev 1.31   Apr 20 2006 15:49:04   oren-new
 * add line mng MLT error codes
 * 
 *    Rev 1.30   Apr 03 2006 15:14:36   itzik_c
 * Add Error's support for MsgQ Delete.
 * 
 *    Rev 1.29   Mar 15 2006 16:03:14   avnerf
 * add onu is valid er code
 * 
 *    Rev 1.28   Mar 07 2006 17:54:22   avnerf
 * Add ERR_OLT_GPON_ONU_IS_DELETED 
 * 
 *    Rev 1.27   Mar 07 2006 13:52:18   oren-new
 * add  ERR_LINE_MNG_CHANNEL_CRAM_INIT error code
 * 
 *    Rev 1.26   Feb 21 2006 18:56:54   avnerf
 * Add APM codes
 * 
 *    Rev 1.25   Jan 30 2006 13:31:32   oren-new
 * add error return codes:  ERR_LINE_MNG_INVALID_PIGGYBACK_TONE_FLAGS,
 * ERR_RTP_MNG_INVALID_SESSION_VAD,
 *   ERR_RTP_MNG_INVALID_SESSION_PT.
 * 
 *    Rev 1.24   Jan 26 2006 08:29:26   avnerf
 * Add error code to GPON OLT BW allocation
 * 
 *    Rev 1.23   Jan 22 2006 18:11:20   oren-new
 * add   ERR_LINE_MNG_CONFIG_CID_SENDER
 * 
 *    Rev 1.22   Jan 19 2006 11:40:50   avnerf
 * Add error codes to OLT GPON
 * 
 *    Rev 1.21   Jan 08 2006 17:18:12   yoram
 * Add rtos task delete error code
 * 
 *    Rev 1.20   Dec 27 2005 19:07:32   yoram
 * Add DHCP on startup
 * 
 *    Rev 1.19   Dec 27 2005 14:43:18   oren-new
 * modify line manager error codes due to new vinetic driver 1 0 21
 * 
 *    Rev 1.18   Dec 15 2005 15:34:54   avnerf
 * Add OLT GPON Error codes for SN Table search
 * 
 *    Rev 1.17   Dec 14 2005 15:33:34   yoram
 * Add POSIX mutex error code
 * 
 *    Rev 1.16   Dec 13 2005 11:09:24   oren-new
 * add error code for RTCP support, modify RTP error codes
 * 
 *    Rev 1.15   Dec 04 2005 15:09:48   oren-new
 * new error code
 * add error codes for binary semaphore
 * 
 *    Rev 1.14   Nov 28 2005 14:27:58   yoram
 * revert to 1.13 plus ERR_SELECT_NO_SOCKET
 * 
 *    Rev 1.12   Sep 29 2005 16:47:48   oren-new
 * remove unused error codes
 * 
 *    Rev 1.11   Sep 28 2005 11:50:58   oren-new
 * added line manager error codes
 * 
 *    Rev 1.10   Sep 25 2005 09:55:14   oren-new
 * added line mng and rtp mng error codes
 * 
 *    Rev 1.9   Sep 15 2005 08:17:34   oren-new
 * added layer manager error codes
 * 
 *    Rev 1.8   Sep 11 2005 12:16:44   avnerf
 * Add Error Codes for ONU Context Manager
 * 
 *    Rev 1.7   Aug 21 2005 09:08:28   oren-new
 * update and modify line manager error codes
 * 
 *    Rev 1.6   Aug 14 2005 16:44:34   oren-new
 * added onu voice line mng error codes
 * 
 *    Rev 1.5   Aug 08 2005 15:01:30   yoram
 * 1. nucleus trace msg.
 * 2. nucleus glue layer logic fix.
 * 3. timer ms clock and minimum check.
 * 
 *    Rev 1.4   Jul 31 2005 11:30:42   yoram
 * 1. Module startup automation.
 * 2. task waiting function.
 * 3. semaphore give binary function.
 * 
 *    Rev 1.3   Jul 27 2005 18:27:28   oren-new
 * update gpon base error code
 * 
 *    Rev 1.2   Jul 27 2005 18:01:16   oren-new
 * added ONU Fpga & Gpon error codes
 * 
 *    Rev 1.1   Jul 26 2005 10:25:34   arie
 * Changed base for each module.
 * Add Errors for MODULES and Trace module
 * 
 *    Rev 1.0   Jul 11 2005 12:47:54   arie
 * Initial revision.
 * 
 *    Rev 1.3   Jul 03 2005 19:09:28   yoram
 * Nucleus GL file with minimal basic func
 * 
 *    Rev 1.2   Jun 30 2005 14:10:52   yoram
 * 1. Project build with nucleus
 * 2. fix _start worning
 * 
 *    Rev 1.1   Jun 29 2005 08:53:22   yoram
 * PVCS auto log fix
 * 
 *    Rev 1.0   Jun 28 2005 19:00:38   yoram
 * Initial revision.
 *                                                                            */
/**       18Apr05  oren_ben_hayune created.                                   */
/**                                                                           */
/******************************************************************************/

#ifndef _ERROR_CODE_H
#define _ERROR_CODE_H

/* Include Files
------------------------------------------------------------------------------*/

/* Definitions
------------------------------------------------------------------------------*/ 

/* Enums                                    
------------------------------------------------------------------------------*/ 
typedef enum
{

  IAMBA_OK                       = 0, /* OK */
  IAMBA_QUIT,                         /* Needed to stop an operation with status ok */
                                      
  IAMBA_ERR_GENERAL,                  /* General (unspecified error */
  ERR_FATAL,                          /* Fatal Error */
                           
  ERR_TASK_CREATE           = 0x1000, /* Task Create fail */
  ERR_TASK_STACK_ALLOC,               /* Task stack alloc fail */                                       
  ERR_TASK_STACK_DEALLOC,             /* Task stack dealloc fail */                                       
  ERR_TASK_CONTROL,                   /* Task control block pointer is NULL */
  ERR_TASK_FUNC,                      /* Task entry point is NULL */
  ERR_TASK_STACK,                     /* Task stack size is not large enough */                                       
  ERR_TASK_OPTIONS,                   /* Task options are invalid */                                                  
  ERR_TASK_STATE,                     /* Task is not in suspend, terminated or finished states */
  ERR_TASK_DELETE,                    /* Task is delete fail */
  ERR_TASK_PRIO,                      /* Task priority change fail */
  ERR_TASK_SPAWN,                     /* spawn a task failed */
  
  ERR_QUEUE_CREATE          = 0x1200, /* Queue Create fail */
  ERR_MSG_Q_STACK_ALLOC,              /* Message queue stack alloc fail */      
  ERR_MSG_Q_STACK_DEALLOC,            /* Message queue stack dealloc fail */ 
  ERR_MSG_Q_CONTROL,                  /* Message queue control block pointer or message pointer is NULL */
  ERR_MSG_Q_MEM,                      /* Message queue start address is invalid */         
  ERR_MSG_Q_SIZE,                     /* Message queue size & message size does not fit together */
  ERR_MSG_Q_OPTIONS,                  /* Message queue message type or suspend type is invalid */                
  ERR_MSG_Q_FULL,                     /* Message queue is full */                  
  ERR_MSG_Q_TIMEOUT,                  /* Message queue full/empty/unavailable after suspend timeout */
  ERR_MSG_Q_STATE,                    /* Message queue was deleted or reset while task suspended */             
  ERR_MSG_Q_EMPTY,                    /* Message queue is empty */ 
                                      
  ERR_SEM_CONTROL           = 0x1300, /* Semaphore control block pointer is NULL */      
  ERR_SEM_OPTIONS,                    /* Semaphore suspend type is invalid */             
  ERR_SEM_STATE,                      /* Semaphore was deleted or reset while task suspended */
  ERR_SEM_TIMEOUT,                    /* Semaphore unavailable after suspend timeout */   
  ERR_SEM_OCCUPY,                     /* Semaphore unavailable */ 
  ERR_SEM_CREATE,                     /* create semaphore failed */
  ERR_SEM_FREE,                       /* Error on semaphore free */
  ERR_SEM_NOT_VALID,                  /* semaphore pointer not valid */
  ERR_SEM_BIN_GIVE_2,                 /* Binary semaphore try to give second time but */
  ERR_SEM_NOT_BINARY,                 /* Not Binary semaphore Use Binary Function */
  ERR_SEM_BINARY,                     /* Binary semaphore Use Not Binary Function */
                                                                                                            
  ERR_TIMER_CONTROL         = 0x1400, /* Timer control block pointer is NULL */                                     
  ERR_TIMER_FUNC,                     /* Timer expiration function pointer is NULL */
  ERR_TIMER_OPTION,                   /* Timer enable parameter is NULL or initial timer is zero */
  ERR_TIMER_STATE,                    /* Timer is not disabled or is currently enabled */   
  ERR_TIMER_CREATE,                   /* Timer is not disabled or is currently enabled */                    
                                                                                                             
  ERR_EVENT_CONTROL         = 0x1500, /* Event group control block pointer or Retrieved event pointeris NULL */                                            
  ERR_EVENT_OPTION,                   /* Operation parameters is invalid or Suspend type is invalid or */                                                 
                                      /* Requested event flag not currently present */
  ERR_EVENT_TIMEOUT,                  /* Requested event flag not currently present even after timeout */              
  ERR_EVENT_STATE,                    /* Event group was deleted when task was suspended */                           
  ERR_EVENT_NOT_PRESENT,              /* Event not present while get event no suspend */
                                
  ERR_SIGNAL_SEND           = 0x1580, 
                                      
  ERR_SELECT                = 0x1600, /* driver's rtosSelect() routine was invokedvia ioctl() */
  ERR_SELECT_ADD,                     /* Requested add a wake-up node but memory is insufficient */              
  ERR_SELECT_DELETE,                  /* Requested delete a wake-up node but node can't ce found*/
  ERR_SELECT_NO_SOCKET,               /* No sockets were specified in the readfs parameter of the NU_Select system Call */


  ERR_POOL_CREATE_NOMEM     = 0x1680, /* No memory left to allocate this poll */
                                                                                                             

  ERR_MEMP_NOMEM            = 0x1700, /* No memory left to allocate this poll */
  ERR_MEMP_NO_FREE_POOL,              /* All Pools have already been used */
  ERR_MEMP_TO_MENY_TYPES,             /* try to alloc too many buffer type for one pool */
  ERR_MEMP_OUT_OF_LIMITS,             /* Input param's are out of limit */
  ERR_MEMP_NAME,                      /* Pool name already been used */
  ERR_MEMP_NOPOOL,                    /* Pool doesn't present */
  ERR_MEMP_NOTASK,                    /* Not a task doesn't present */
  ERR_MEMP_NOBUF,                     /* Can't allocate new buffer */
  ERR_MEMP_BUF_TOO_LARGE,             /* Buffer length is too large for the assigned buffers */
  ERR_MEMP_BUFADDR,                   /* Incorrect buffer address */
  ERR_MEMP_BUFFREE,                   /* Buffer is already released */
  ERR_MEMP_NOSEM,                     /* System can not allocate semaphore */
  ERR_MEMP_POOL_CREATE,               /* Couldn't create memory pool */ 
  ERR_MEMP_CLI_REGISTER,              /* Failed to register memp cli commands */
  ERR_MEMP_BUF_GET,                   /* Failed to get buffer from memory pool */ 
  ERR_MEMP_BUF_FREE,                  /* Failed to free buffer from memory pool */ 
  ERR_MEMP_REALLOC_NULL,              /* Try to realloc with null old buffer (no pool) */
                                      
  ERR_CLII                  = 0x1800, /* CLII Error */ 

  ERR_MODULES_NOT_FOUND     = 0x1900, /* Modules - element not found */
  ERR_MODULES_EOL,                    /* End of List */
  ERR_MODULES_OUT_OF_RANGE,           /* Out of range */

  ERR_TRACE_SEVERITY_NOT_FOUND = 0x1A00, /* Trace - Severity not found */

  ERR_ASIC                     = 0x1B00, /* ASIC Error */ 
  ERR_ASIC_ONT_DRV_REG_OUT_OF_RANGE,     /* ASIC register out of range */
  ERR_ASIC_ONT_DRV_REG_WRONG_ENTRY,      /* ASIC register wrong entry */ 
  ERR_ASIC_ONT_DRV_REG_WRONG_ACCESS_TYPE,/* ASIC register wrong access type */

  ERR_REG_OUT_OF_RANGE         = 0x1B20, /* register out of range */
  ERR_REG_WRONG_ENTRY,                   /* register wrong entry */ 
  ERR_REG_WRONG_ACCESS_TYPE,             /* register wrong access type */

  ERR_ONU_GPON_BASE            = 0x1C00, /* PON Error */
  ERR_ONU_GPON_MSG_SEND_FIFO_FULL,       /* Mac */ 
  ERR_ONU_GPON_DB_ONU_STATE,             /* Database */
  ERR_ONU_GPON_DB_PREAMBLE,          
  ERR_ONU_GPON_DB_DELIMITER,         
  ERR_ONU_GPON_DB_TABLE_FULL,         
  ERR_ONU_GPON_DB_NOT_EXIST,         
  ERR_ONU_GPON_SETUP,                 
  ERR_ONU_GPON_SWITCHON,             
  ERR_ONU_GPON_OPERATE,              
  ERR_ONU_GPON_APM_ALARM_STATE,       
  ERR_ONU_GPON_APM_ALARM_TYPE,        

  ERR_MUTEX_DEFAULT_ATTR		= 0x2000, /* Posix Mutex */
  ERR_MUTEX_EINVAL,
  ERR_MUTEX_EDEADLK,
  /*ERR_MUTEX_EINVAL,*/
  ERR_MUTEX_EBUSY,
  ERR_MUTEX_EPERM,
  ERR_MUTEX_EAGAIN,

  /* APM error codes */
  ERR_APM_GENERAL_ERROR         = 0x3100, 
  ERR_APM_DB_NO_DATA , 
  ERR_APM_DCAR_TABLE_FULL , 

  /* IPTV Error codes */
  ERR_IPTV_MG_INDEX_OUT_OF_RANGE = 0x3200,
  ERR_IPTV_COUNTER_ENUM_MISMATCH,
  ERR_IPTV_DELETE_VALID_HOST,
  ERR_IPTV_HOST_AND_MG_NOT_EXIST,
  ERR_IPTV_HOST_COUNT_NEGATIVE,
  ERR_IPTV_HOST_COUNT_TOO_HIGH,
  ERR_IPTV_HOST_ENTRY_COUNT_TOO_HIGH,
  ERR_IPTV_HOST_ENTRY_GROUP_COUNT_NEGATIVE,
  ERR_IPTV_HOST_LIMIT_EXCEEDED,
  ERR_IPTV_HOST_NO_EMPTY_ENTRY,
  ERR_IPTV_HOST_NOT_EXIST,
  ERR_IPTV_HOST_TABLE_CREATE_FAILED,
  ERR_IPTV_IGMP_ENTRY_NOT_EXIST,
  ERR_IPTV_IGMP_TABLE_CREATE_FAILED,
  ERR_IPTV_IGMP_TBL_LIMIT_EXCEEDED,
  ERR_IPTV_IGMP_INVALID_INDEX,
  ERR_IPTV_INCONSISTENT_HG_TABLE,
  ERR_IPTV_INVALID_HOST_GROUP_HANDLE,
  ERR_IPTV_INVALID_HOST_HANDLE,
  ERR_IPTV_IP_RANGE_COUNT_TOO_HIGH,
  ERR_IPTV_MG_COUNT_NEGATIVE,
  ERR_IPTV_MG_DELETE_FAILED,
  ERR_IPTV_MG_ENTRY_COUNT_TOO_HIGH,
  ERR_IPTV_MG_ENTRY_HOST_COUNT_NEGATIVE,
  ERR_IPTV_MG_ENTRY_PORT_HOST_COUNT_NEGATIVE,
  ERR_IPTV_MG_ENTRY_NOT_FOUND,
  /*ERR_IPTV_MG_INDEX_OUT_OF_RANGE,*/
  ERR_IPTV_MG_LIMIT_EXCEEDED,
  ERR_IPTV_MG_NO_EMPTY_ENTRY,
  ERR_IPTV_MG_NOT_EXIST,
  ERR_IPTV_MG_TABLE_CREATE_FAILED,
  ERR_IPTV_SA_MAC_LIMIT_EXCEEDED,
  ERR_IPTV_SHDW_VIDEO_OUT_OF_RANGE,
  ERR_IPTV_SHADOW_VIDEO_NO_EMPTY_ENTRY,
  ERR_IPTV_SHDW_VLAN_ETYPE_INDEX_OUT_OF_RANGE,
  ERR_IPTV_INVALID_VLAN_ETYPE,
  ERR_IPTV_QTAG_LEVEL_EXCEEDED,
  ERR_IPTV_ILLEGAL_US_FW_MODE,
  ERR_IPTV_HOST_GROUP_NOT_EXIST,
  ERR_IPTV_SHDW_VIDEO_ENTRY_NOT_EXIST,
  ERR_IPTV_MCAST_NUM_OF_STREAMS_NEGATIVE,
  ERR_IPTV_ILLEGAL_PROFILE,
  ERR_IPTV_SETUP,
  ERR_IPTV_SWITCHON,
  ERR_IPTV_PROFILE,
  ERR_IPTV_IGMP_FORWARDING_TABLE_FULL,
  ERR_IPVERSION_NOT_SUPPORTED,
  ERR_IPTV_INVALID_IGMP_FRAME_PARAMS,
  ERR_IPTV_IGMP_DS_NOT_VALID,
  ERR_IPTV_UNRECOGNIZED_IGMP_TYPE,
  ERR_IPTV_IGMPV3_RECORD,
  ERR_IPTV_OMCI_INVALID_COMMAND,
  ERR_IPTV_INVALID_IP_RANGE,
  ERR_IPTV_OMCI_MSG_SEND_FAIL,
  ERR_IPTV_MULTI_CAST_TABLE_FULL,
  ERR_IPTV_INVALID_DELETE,
  ERR_IPTV_ILLEGAL_IP_VERSION,
  ERR_IPTV_NON_SNOOP_IGMP_FRAME,
  ERR_IPTV_MG_COUNT_LIMIT_EXCEEDED,
  ERR_IPTV_HOST_COUNT_LIMIT_EXCEEDED,
  ERR_IPTV_CLI_INPUT,
  ERR_IPTV_MAC_INPUT_INVALID,
  ERR_IPTV_IP_INVALID,
  ERR_IPTV_DP_NO_FREE_DATA_BUFFER,
  ERR_IPTV_DP_INVALID_DATA_BUFFER,
  ERR_IPTV_DP_NO_MORE_DATA_TO_READ,
  ERR_IPTV_IGMPV3_GROUP_TYPE_ILLEGAL,

  ERR_SD_PARAM_OUT_OF_RANGE    = 0x5000,
  ERR_SF_PARAM_OUT_OF_RANGE,
  ERR_DISABLE_PARAM_OUT_OF_RANGE,
  ERR_TCONT_PARAM_OUT_OF_RANGE,
  ERR_ALLOC_PARAM_OUT_OF_RANGE,

  ERR_OCP_OCPCHANNEL_INIT_OUT_OF_RANGE  = 0x5300,
  ERR_OCP_OCPCHANNEL_INIT_NOT_EMPTY,

  ERR_iOCP_TASK_CLIENT_ID_REALLOC       = 0x5400,
  ERR_iOCP_TASK_CLIENT_ID_NOT_ALLOC,

  ERR_IPP_INVALID_HEADER                = 0x6000,
  ERR_IPP_INVALID_SYMBOL_HEADER,
  ERR_IPP_INVALID_PROTOCOL_VERSION,
  ERR_IPP_INVALID_MSG_TYPE,
  ERR_IPP_INVALID_MSG_OP,
  ERR_IPP_INVALID_MSG_LENGTH,
  ERR_IPP_INVALID_MSG_HEADER_CRC,
  ERR_IPP_INVALID_MSG_PAYLOAD_CRC,
  ERR_IPP_INVALID_REG_RX_ENUM_ADDR,

  /* 
  ** ###################################################
  ** ##                                               ##
  ** ## LAST ERROR CODE !!!                           ##
  ** ## DO NOT DEFINE ERROR CODES BEYOND THIS ELEMENT ##
  ** ##                                               ##
  ** ###################################################
  */
  ERR_LAST_ERRORCODE

}E_ErrorCodes;

/* Typedefs
------------------------------------------------------------------------------*/

/* Global variables
------------------------------------------------------------------------------*/

/* Global functions
------------------------------------------------------------------------------*/

/* Macros
------------------------------------------------------------------------------*/

#endif /* _ERROR_CODE_H */



