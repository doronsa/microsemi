/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/* ListUtils.h -  */


/*
  modification history
  --------------------
  8Feb04  Zeev created
*/

/*
  DESCRIPTION


  TIPS:
  None.
  
  module prefix - omci
 */


#ifndef __INCListUtilsh
#define __INCListUtilsh


// Big number for maximum list size - instead of local definitions
// for those that don't care about list size
#define OMLISTMAX               100000


/* type definitions */

typedef struct omnode		/* Node of a linked list. */
{
    struct omnode *next;	/* Points at the next node in the list */
    struct omnode *previous;	/* Points at the previous node in the list */
} OMNODE;


typedef struct			/* Header for a linked list. */
{
    OMNODE node;		/* Header list node */
    int    count;		/* Number of nodes in list */
    int    max;         /* Max number of nodes in list */
} OMLIST;


extern OMNODE *	listFirst       (OMLIST *pList);
extern OMNODE *	listNext        (OMNODE *pNode);
extern OMNODE * listPrevious    (OMNODE *pNode);
extern int   	listCount       (OMLIST *pList);
extern int  	listAdd         (OMLIST *pList, OMNODE *pNode);
extern void 	listAddFirst    (OMLIST *pList, OMNODE *pNode);
extern void 	listDelete      (OMLIST *pList, OMNODE *pNode);
extern OMNODE*  listDeleteFirst (OMLIST *pList);
extern void 	listInit        (OMLIST *pList, int max);

#endif /*__INCListUtilsh*/
