/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : ONT GPON                                                  **/
/**                                                                          **/
/**  FILE        : ontTasks.h                                                **/
/**                                                                          **/
/**  DESCRIPTION : This file contains ONT Tasks configurations               **/
/**                                                                          **/
/******************************************************************************
 *                                                                            *
 *  MODIFICATION HISTORY:                                                     *
 *                                                                            *
 *   27Jul05  Yoram Shamir    created                                         *
 * ========================================================================== *
 *
 *   $Log: ontTasks.h,v $
 *   Revision 1.1.1.1  2011-04-25 03:44:39  guxi
 *   Victor added Joe's version
 *
 *   Revision 1.1.1.1  2010-09-25 03:08:24  joe
 *   add sw_tree Z2
 *
 *   Revision 1.1.1.1  2010-09-08 01:41:15  daniel
 *   add sw tree 1.3.26
 *
 *   Revision 1.1.1.1  2010-09-03 07:53:54  joe
 *   add_Z1_linux_kernel
 *
 *
 *    Rev 1.1   Jun 01 2008 14:47:28   yoram
 * qprintf task priority lower to 240
 *
 *    Rev 1.0   May 19 2008 15:25:38   oren
 * Initial revision.
 *
 *    Rev 1.31   May 11 2008 17:46:32   oren
 * update ONT tasks
 *
 *    Rev 1.30   Jul 01 2007 13:33:36   oren
 * add single sdram memory check task
 *
 *    Rev 1.29   May 03 2007 09:39:56   zeev
 * Added in OMCI task priorities and stack sizes
 *
 *    Rev 1.28   Apr 29 2007 18:31:02   oren
 * add iptv data task
 *
 *    Rev 1.27   Apr 26 2007 21:33:34   oren
 * change pon tast pri, and interrupt definitions
 *
 *    Rev 1.26   Mar 12 2007 09:13:14   yoram
 * add GOM tasks and queues
 *
 *    Rev 1.24   Nov 15 2006 16:20:54   oren
 * replace rtp mng resources with data mng
 *
 *    Rev 1.23   Sep 07 2006 09:54:32   yoramb
 * add task scheduler
 *
 *    Rev 1.22   Aug 31 2006 16:01:22   Yoram
 * add net task that send DHCP, ping and so on after reciving queue msg
 *
 *    Rev 1.21   Jul 12 2006 11:13:36   erezi
 * changed NET_ONU_TX_TASK_PRIORITY  to  (4)
 *
 *    Rev 1.20   Jul 11 2006 18:21:30   yoram
 * Add ont net TX task
 * removenet tracer task
 *
 *    Rev 1.19   Jun 22 2006 14:17:30   erezi
 * add vinetic data task handles all ingress data traffic, modify priority of ingress data tasks from 92 to 91
 *
 *    Rev 1.18   Jun 13 2006 17:31:56   oren-new
 * add ES SIP CAS Ingress Data tasks definitions
 *
 *    Rev 1.17   Jun 08 2006 11:13:42   arie
 * Add Globals Task info
 *
 *    Rev 1.16   Jun 04 2006 16:00:20   yoram
 * add ONT NET print task
 *
 *    Rev 1.15   May 25 2006 20:19:28   oren-new
 * add service logic task for sip cas
 *
 *    Rev 1.14   Mar 23 2006 18:42:02   yoram
 * changed VSM task priority to 100
 *
 *    Rev 1.13   Mar 19 2006 18:15:10   yoram
 * Add VSM task
 *
 *    Rev 1.12   Dec 21 2005 11:11:46   oren-new
 * modify handling of vinetic device driver timers, instead of executing timer functionality from timer conext, it will be executed from the vinetic timer task
 *
 *    Rev 1.11   Dec 18 2005 14:00:26   oren-new
 * add line manager maintenance task
 *
 *    Rev 1.10   Dec 13 2005 11:09:58   oren-new
 * changed ONT tasks priority scheme
 *
 *    Rev 1.9   Nov 29 2005 09:47:30   yoram
 * increase MAIN_TASK_STACKSIZE from 10Kbytes  to 40Kbytes
 *
 *    Rev 1.8   Nov 20 2005 16:39:16   oren-new
 * changed priority of the line managee tasks
 *
 *    Rev 1.7   Oct 02 2005 10:55:20   yoram
 * FPGA driver functionality as in version 99
 *
 *    Rev 1.6   Sep 27 2005 11:16:28   yoram
 * 1. add net ONU task.
 * 2.  All packet internally flag in FPGA.
 *
 *    Rev 1.5   Sep 25 2005 12:49:46   yoram
 * 1. suport 2 vinetic in the glos.
 * 2. reset acording to FPGA version 89 (not final)
 * 3. read ready bit directly from vintic (not from FPGA)
 * 4. remove wornings
 *
 *    Rev 1.4   Sep 25 2005 09:57:28   oren-new
 * adde rtp manager ingress and egress tasks
 *
 *    Rev 1.3   Sep 04 2005 15:34:54   avnerf
 * Update PON tasks to be lower priority than CLI
 *
 *    Rev 1.2   Aug 14 2005 16:45:10   oren-new
 * added onu line mng tasks definitions
 *
 *    Rev 1.1   Aug 04 2005 17:02:56   oren-new
 * added pon apm task definitions
 *
 *    Rev 1.0   Jul 31 2005 10:50:56   yoram
 * Initial revision.
 *
 ******************************************************************************/
#ifndef _ONT_TASKS_H
#define _ONT_TASKS_H

/* Include Files
------------------------------------------------------------------------------*/

/* Definitions
------------------------------------------------------------------------------*/

/******************************************************************************/
/* ========================================================================== */
/*                              Tasks Priority                                */
/* ========================================================================== */
/******************************************************************************/
#define ONU_TIMER_THREAD_PRIORITY           (4)
#define ONU_GPON_ISR_THREAD_PRIORITY        (5)
#define ONU_GPON_MNG_THREAD_PRIORITY        (15)
#define IPTV_DATA_TASK_PRIORITY             (25)
#define IPTV_CTRL_TASK_PRIORITY             (26)
#define ONU_GPON_APM_TASK_PRIORITY          (30)
#define CLII_TASK_PRIORITY                  (35)
#define ONT_OMCI_RXFRAME_TASK_PRIORITY      (45)
#define ONT_OMCI_HIGH_TASK_PRIORITY         (50)
#define SHELL_TASK_PRIORITY                 (55)
#define IPP_ONU_MAIN_LOOP_TASK_PRIORITY     (60)
#define QPRINTF_TASK_PRIORITY               (95) /* Should be the lowest thread in the system */
#define ONT_EPON_TASK_PRIORITY              (50)
#define ONT_OAM_TASK_PRIORITY               (60)
#define ONT_OAM_RXFRAME_TASK_PRIORITY       (55)


/******************************************************************************/
/* ========================================================================== */
/*                              Tasks Stack Size                              */
/* ========================================================================== */
/******************************************************************************/
#define PTHREAD_STACK_DEF (16384) //16Kbytes

/* Enums
------------------------------------------------------------------------------*/
typedef enum
{
    T_ONU_GPON_ISR_THREAD  = 0,
    T_ONU_GPON_MNG_THREAD,
    T_ONU_GPON_APM_THREAD,
    T_CLII_THREAD,
    T_ONT_OMCI_RXFRAME_THREAD,
    T_ONT_OMCI_HIGH_THREAD,
    T_QPRINTF_THREAD,

    T_IPP_RX_LOOP_THREAD,
    T_IPP_ISR_POLLING_THREAD,

    MAX_NUM_OF_ONU_THREADS
}E_GPON_PTHREADS;

/* Typedefs
------------------------------------------------------------------------------*/

/* Global variables
------------------------------------------------------------------------------*/

/* Global functions
------------------------------------------------------------------------------*/

/* Macros
------------------------------------------------------------------------------*/

#endif /* _ONT_TASKS_H */

