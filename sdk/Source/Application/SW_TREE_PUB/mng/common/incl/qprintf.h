/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : ONT GPON                                                  **/
/**                                                                          **/
/**  FILE        : qprtinf.h                                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file contains ONT queued printf configurations       **/
/**                                                                          **/
/******************************************************************************
 *                                                                            *                              
 *  MODIFICATION HISTORY:                                                     *
 *                                                                            *
 *   24Mar09  Oren BH    created                                              *  
 * ========================================================================== *      
 ******************************************************************************/
#ifndef _ONT_QPRTINF_H
#define _ONT_QPRTINF_H

/* Include Files
------------------------------------------------------------------------------*/

/* Definitions
------------------------------------------------------------------------------*/ 
#if 0 /*Anna*/

#define QPRINT_MAX_MSGS_LEN         (128) /* 512 bytes */
                                   
  
#define QPRINT_MASK_CRITICAL	    (0x80000000)
#define QPRINT_MASK_ERROR           (0x40000000)
#define QPRINT_MASK_WARNING		    (0x20000000)
#define QPRINT_MASK_INFO		    (0x10000000)
#define QPRINT_MASK_TEST		    (0x08000000)  /* for special testing purposes */
/* optional mask - bits 5-7 */                 
#define QPRINT_MASK_LOG             (0x04000000)
#define QPRINT_MASK_TIME_DISPLAY    (0x02000000)
#define QPRINT_MASK_FORCE_PRINT     (0x01000000)


#define QPRINT_MASK_GLOBAL_SEVERITY (0xF8000000)
#define QPRINTF_MASK_GLOBAL_ATTRIB  (0xFF000000)

#define QPRINT_OPTIONS_ENABLE       (0x07FF0000)  /* to be able to print options */
#define QPRINF_MODULE_MASK          (0x0000001F)  /* lower 5 bits are dedicated to module id */

#define QPRINT_MASK_PRINT			(0) //0x01000000
#define QPRINT_MASK_PRINT_N_LOG     (QPRINT_MASK_PRINT | QPRINT_MASK_LOG)

#define l_sysClkRate                (100)


#endif
/* Enums                              
------------------------------------------------------------------------------*/ 

/* Typedefs
------------------------------------------------------------------------------*/

/* Global variables
------------------------------------------------------------------------------*/

/* Global functions
------------------------------------------------------------------------------*/
extern INT32 qprintf (UINT32 attribute , UINT32 submodule, UINT8* msg, ...); 
/*
extern E_ErrorCodes traceAndLog_GetAttrib(UINT32 a_module,
										  UINT32 a_subModule,
										  UINT32 *a_globalAttrib_p,
										  UINT32 *a_specificAttrib_p);*/
/* Macros
------------------------------------------------------------------------------*/    
#define qprintf mvqprintf

#endif /* _ONT_QPRTINF_H */
