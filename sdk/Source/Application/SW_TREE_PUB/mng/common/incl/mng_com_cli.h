/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************** 
*      mng_com_cli.h                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:                                                      
*                                                                                
* DATE CREATED: June 29, 2010     
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.2 $                                                           
*******************************************************************************/

#ifndef __MNG_COM_CLI_H__
#define __MNG_COM_CLI_H__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"


/********************************************************************************/
/*                      Mng. common CLI functions                               */
/********************************************************************************/
bool_t mng_com_cli_trace_change_status      (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t mng_com_cli_trace_print_status       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t mng_com_cli_trace_print_module_list  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t mng_com_cli_set_pon_params           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  mv_os_cli_timer_create              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  mv_os_cli_timer_init                (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  mv_os_cli_timer_start               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  mv_os_cli_timer_stop                (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  mv_os_cli_timer_del                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  mv_os_cli_timer_deb_act             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  mv_os_cli_timer_deb_deact           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  mv_os_cli_timer_print_full          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  mv_os_cli_timer_print_valid         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

#endif
