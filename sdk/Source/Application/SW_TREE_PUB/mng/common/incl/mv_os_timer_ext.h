/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
alternative licensing terms.  Once you have made an election to distribute the
File under one of the following license alternatives, please (i) delete this
introductory statement regarding license alternatives, (ii) delete the two
license alternatives that you have not elected to use and (iii) preserve the
Marvell copyright notice above.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

********************************************************************************
Marvell GPL License Option

If you received this File from Marvell, you may opt to use, redistribute and/or
modify this File in accordance with the terms and conditions of the General
Public License Version 2, June 1991 (the "GPL License"), a copy of which is
available along with the File in the license.txt file or by writing to the Free
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 or
on the worldwide web at http://www.gnu.org/licenses/gpl.txt.

THE FILE IS DISTRIBUTED AS-IS, WITHOUT WARRANTY OF ANY KIND, AND THE IMPLIED
WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY
DISCLAIMED.  The GPL License provides additional details about this warranty
disclaimer.
********************************************************************************
Marvell BSD License Option

If you received this File from Marvell, you may opt to use, redistribute and/or
modify this File under the following licensing terms.
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    *   Redistributions of source code must retain the above copyright notice,
	    this list of conditions and the following disclaimer.

    *   Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

    *   Neither the name of Marvell nor the names of its contributors may be
        used to endorse or promote products derived from this software without
        specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

******************************************************************************/

/******************************************************************************
**  FILE        : mv_os_timer_ext.h                                          **
**                                                                           **
**  DESCRIPTION : This file contains ONU TPM Management Interface            **
*******************************************************************************
*                                                                             *
*  MODIFICATION HISTORY:                                                      *
*                                                                             *
*   09Mar11  Yuval Caduri  created                                            *
* =========================================================================== *
******************************************************************************/
#ifndef _MV_OS_TIMER_EXT_H_
#define _MV_OS_TIMER_EXT_H_

/* Include Files
------------------------------------------------------------------------------*/

/* Definitions
------------------------------------------------------------------------------*/


typedef int     TIMER_ID;
typedef void (*funcTimer) (uint32_t  timerCbParamA, uint32_t  timerCbParamB);

/* Temporary - add to general ENUM */
typedef enum mv_os_glue_error_code
{
    MV_OS_GLUE_OK,                  /* Return ok (=0) */
    MV_OS_GLUE_EINVAL,              /* TimerId does not exist */
    MV_OS_GLUE_ENOTIMERES,          /* No Timer Table resource */
    MV_OS_GLUE_GEN_ERR = 0x1234,    /* General Error */
} mv_os_glue_error_code_t;



/* Global functions
------------------------------------------------------------------------------*/
mv_os_glue_error_code_t mv_os_glue_timer_init(void);
mv_os_glue_error_code_t mv_os_glue_timer_create(TIMER_ID 	*timerId,
				                                char		*timerName,             /* Timer name */
				                                funcTimer	 timerCb,               /* Callback function */
                                                uint32_t	 timerCbParamA,         /* Callback Param A */
				                                uint32_t	 timerCbParamB);        /* Callback Param B */

mv_os_glue_error_code_t mv_os_glue_timer_start(TIMER_ID       timerId,
                                               uint32_t	    initialTime,    /* In milliseconds */
                                               uint32_t	    rescheduleTime);/* In milliseconds, setting to zero does not reschedule */

mv_os_glue_error_code_t mv_os_glue_timer_stop(TIMER_ID timerId);
mv_os_glue_error_code_t mv_os_glue_timer_del(TIMER_ID timerId);
mv_os_glue_error_code_t mv_os_glue_timer_print_full_tbl(void);
mv_os_glue_error_code_t mv_os_glue_timer_print_valid_tbl(void);

mv_os_glue_error_code_t mv_os_glue_timer_debug_act(void);
mv_os_glue_error_code_t mv_os_glue_timer_debug_deact(void);


/* Macros
------------------------------------------------------------------------------*/

#endif /* _MV_OS_TIMER_EXT_H_ */




















