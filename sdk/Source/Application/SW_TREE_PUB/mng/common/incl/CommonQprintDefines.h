/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/* CommonQprintDefines.h                        */


 
/*
  DESCRIPTION
  
  Description:  For ONT ISN 100
                
  
*/
 
#ifndef __INCQprintDefineshCommon
#define __INCQprintDefineshCommon

#include "errorCode.h"
#include "qprintf.h"
#include "mng_trace.h"

/* qprint module definitions */ 
/*
#define QPROMCI_CRITICAL    (ONU_OMCI_MODULE | QPRINT_MASK_CRITICAL | QPRINT_MASK_TIME_DISPLAY)
#define QPROMCI_ERROR       (ONU_OMCI_MODULE | QPRINT_MASK_ERROR    | QPRINT_MASK_TIME_DISPLAY)                                                                                   
#define QPROMCI_WARNING     (ONU_OMCI_MODULE | QPRINT_MASK_WARNING  | QPRINT_MASK_TIME_DISPLAY)
#define QPROMCI_INFO        (ONU_OMCI_MODULE | QPRINT_MASK_INFO)
#define QPROMCI_TEST        (ONU_OMCI_MODULE | QPRINT_MASK_TEST)
*/
#define QPRCOM_ERROR       MNG_TRACE_ERROR
#define QPRCOM_WARNING     MNG_TRACE_INFO
#define QPRCOM_INFO        MNG_TRACE_INFO
#define QPRCOM_TEST        MNG_TRACE_DEBUG

/* qprint sub module definitions */
/*
#define QPROMCIMOD_SUB1         (ONU_OMCI_SUB1_SUB_MODULE)
#define QPROMCIMOD_DPA          (ONU_OMCI_DPADAPTER_SUB_MODULE)
*/
#define QPRCOMMOD_SUB1         (MNG_TRACE_MODULE_COMMON)

#endif
