/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/******************************************************************************/
/**                                                                          **/
/**  MODULE      : Common                                                    **/
/**                                                                          **/
/**  FILE        : globals.h                                                 **/
/**                                                                          **/
/**  DESCRIPTION : This file contains global defitinions which is used by    **/
/**                the entire project code                                   **/
/**                It should be included in all '.c' files in the code,      **/
/**                because it contain definitions used by all files.         **/
/**                                                                          **/ 
/******************************************************************************/
/**                                                                           */
/**  MODIFICATION HISTORY:                                                    */
/**
 *        $Log: globals.h,v $
 *        Revision 1.1.1.1  2011-04-25 03:44:39  guxi
 *        Victor added Joe's version 
 *
 *        Revision 1.3  2011-01-05 05:22:14  joe
 *        merge from 2.1.02
 *
 *        Revision 1.2  2010-09-25 06:58:36  joe
 *        remove warning
 *
 *        Revision 1.1.1.1  2010-09-25 03:08:24  joe
 *        add sw_tree Z2
 *
 *        Revision 1.1.1.1  2010-09-08 01:41:15  daniel
 *        add sw tree 1.3.26
 *
 *        Revision 1.1.1.1  2010-09-03 07:53:54  joe
 *        add_Z1_linux_kernel
 *
 * 
 *    Rev 1.3   Jun 10 2008 17:45:54   yoram
 * remove n:\ont.h file
 * 
 *    Rev 1.2   May 26 2008 14:19:14   yaron
 * change cpu numeration from (0,1,2) to (1,2,3)
 * 
 *    Rev 1.1   May 21 2008 17:56:38   yoram
 * remove POSIX from core
 * 
 *    Rev 1.0   May 19 2008 15:25:36   oren
 * Initial revision.
 * 
 *    Rev 1.16   Apr 26 2007 16:52:30   oren
 * fixed IN_RANGE_LIMIT macro
 * 
 *    Rev 1.15   Mar 11 2007 18:23:08   yoram
 * 3CPU
 * 
 *    Rev 1.14   Jan 08 2007 16:12:38   yoram
 * add "Press any key to continue (ESC to exit)..." to CLI
 * 
 *    Rev 1.13   Dec 26 2006 09:09:02   Yoram
 * BSL glue layer
 * 
 *    Rev 1.12   Nov 19 2006 18:08:38   yoram
 * Vinetic driver version 1.1.22
 * 
 *    Rev 1.11   May 25 2006 20:18:28   oren-new
 * change definition of macro IN to IN_RANGE
 * 
 *    Rev 1.10   Apr 30 2006 18:02:22   yoram
 * add unit name to CLI
 * 
 *    Rev 1.9   Dec 25 2005 10:13:12   yoram
 * fix NET, NPP and posix compilation on OLT
 * 
 *    Rev 1.8   Dec 13 2005 16:42:58   yoram
 * fix NIM and MAX macro
 * 
 *    Rev 1.7   Oct 02 2005 16:03:16   oren-new
 * remove enum E_argumentType, moved to CliiExt.h
 * 
 *    Rev 1.6   Sep 08 2005 10:48:00   yoram
 * add HEX types to CLI
 * 
 *    Rev 1.5   Aug 30 2005 16:37:42   yoram
 * ONT nucluse NET 
 * 
 *    Rev 1.4   Aug 08 2005 15:01:24   yoram
 * 1. nucleus trace msg.
 * 2. nucleus glue layer logic fix.
 * 3. timer ms clock and minimum check.
 * 
 *    Rev 1.3   Aug 02 2005 15:18:56   avnerf
 * add FOREVER
 * 
 *    Rev 1.2   Jul 26 2005 19:10:34   arie
 * Add #undef  for TRUE, FALSE and BOOL before the new definitions
 * 
 *    Rev 1.1   Jul 11 2005 17:32:30   yoram
 * project build with vinetic
 * 
 *    Rev 1.0   Jul 11 2005 12:47:54   arie
 * Initial revision.
 * 
 *    Rev 1.4   Jul 07 2005 10:16:26   yoram
 * project with CLI and small stack (2048), vsprintf function over run stack
 * 
 *    Rev 1.3   Jul 03 2005 19:09:30   yoram
 * Nucleus GL file with minimal basic func
 * 
 *    Rev 1.2   Jun 30 2005 14:10:52   yoram
 * 1. Project build with nucleus
 * 2. fix _start worning
 * 
 *    Rev 1.1   Jun 29 2005 08:53:22   yoram
 * PVCS auto log fix
 * 
 *    Rev 1.0   Jun 28 2005 19:00:38   yoram
 * Initial revision.
 *                                                                            */
/**       18Apr05  oren_ben_hayune created.                                   */
/**                                                                           */
/******************************************************************************/

#ifndef _GLOBALS_H
#define _GLOBALS_H

/* Include Files
------------------------------------------------------------------------------*/

/* Definitions
------------------------------------------------------------------------------*/ 

#define   IAMBA_NIOS_HAL
#if defined (CPU_0)
#define   IAMBA_CARD_NAME      "imbCpu1"
#elif defined (CPU_1)
#define   IAMBA_CARD_NAME      "imbCpu2"
#elif defined (CPU_2)
#define   IAMBA_CARD_NAME      "imbCpu3"
#else
#define   IAMBA_CARD_NAME      "GponKw"
#endif

#define	FOREVER				for(;;)

#define   CONTINUED    2 
#ifndef OK
#define   OK           0
#endif
#ifndef ERROR
#define   ERROR        -1
#endif
#define VALID         -1
#define INVALID        0

#define EMPTY         -2

#ifndef   MSG_Q_FIFO_TYPE 
#define   MSG_Q_FIFO_TYPE    0
#endif 

#ifndef   SEM_Q_FIFO
#define   SEM_Q_FIFO         0 
#endif 



#define PRINTF printf
#define FPUTS  fputs
#define FPUTC  fputc

#define FGETC  fgetc
#define GETC   fgetc 
#define GETS   fgets
 
#define FFLUSH fflush

/* Enums 
------------------------------------------------------------------------------*/ 

/* Typedefs
------------------------------------------------------------------------------*/
typedef signed char        INT8;
typedef unsigned char      UINT8;
typedef signed short       INT16;
typedef unsigned short     UINT16;
typedef signed long        INT32;
typedef unsigned long      UINT32;
typedef unsigned long long UINT64;

/*
typedef signed char int8_t 
typedef unsigned char uint8_t 
typedef signed int int16_t 
typedef unsigned int uint16_t 
typedef signed long int int32_t 
typedef unsigned long int uint32_t 
typedef signed long long int int64_t 
typedef unsigned long long int uint64_t 
*/


typedef int            STATUS;

#undef FALSE
#undef TRUE
#undef BOOL

typedef enum {FALSE = 0, TRUE = 1}  BOOL;
typedef int                         FLAG; 

typedef void* (*PTHREAD_FUNCPTR)(void*);
typedef int 		(*FUNCPTR)     ();	   /* ptr to function returning int */
typedef void 		(*VOIDFUNCPTR) (); /* ptr to function returning void */
typedef double 	(*DBLFUNCPTR)  ();  /* ptr to function returning double*/
typedef float 	(*FLTFUNCPTR)  ();  /* ptr to function returning float */

/* Global variables
------------------------------------------------------------------------------*/
extern BOOL taskInitEnd;

/* Global functions
------------------------------------------------------------------------------*/

/* Macros
------------------------------------------------------------------------------*/
#ifndef offsetof
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#endif

#define GET_MIN(a,b) ((a)<(b) ? (a) : (b))
#define GET_MAX(a,b) ((a)>(b) ? (a) : (b))
#define MIN_VAL_LIMIT(val,min)    (((val)<(min)) ? (min) : (val))
#define MAX_VAL_LIMIT(val,max)    (((val)>(max)) ? (max) : (val))
#define IN_RANGE_LIMIT(val,min,max) (GET_MAX(GET_MIN(val,min),max))

#define BYTE_TO_LONG_SIZE(size)  (((size)+3)>>2)
#define BYTE_TO_SHORT_SIZE(size) (((size)+1)>>2)

#define PRINT(format, ...)  printf(format "\n", ##__VA_ARGS__)

#ifdef MNG_DEBUG
    #define DBG_PRINT(format, ...)  printf("%s(%d): " format "\n",__FUNCTION__,__LINE__, ##__VA_ARGS__)
#else
    #define DBG_PRINT(format, ...)
#endif

#endif /* _GLOBALS_H */

