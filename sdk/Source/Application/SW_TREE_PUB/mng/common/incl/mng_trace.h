/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************** 
*      mng_trace.h                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:                                                      
*                                                                                
* DATE CREATED: July 4, 2010     
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.1.1.1 $                                                           
*******************************************************************************/

#ifndef __MNG_TRACE_H__
#define __MNG_TRACE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "globals.h"
#include "errorCode.h"


/********************************************************************************/
/*                              MNG trace levels                                */
/********************************************************************************/
#define MNG_TRACE_NONE              (0)                                                                                   
#define MNG_TRACE_ERROR             (1)                                                                                   
#define MNG_TRACE_INFO              (2)
#define MNG_TRACE_DEBUG             (3)

#define MNG_TRACE_ERROR_STR         "ERROR: "                                                                                   
#define MNG_TRACE_INFO_STR          "INFO: "
#define MNG_TRACE_DEBUG_STR         "DEBUG: "

#define MNG_TRACE_MODULE_SHIFT      (16)
#define MNG_TRACE_MODULE_MASK       (0x001F)
#define MNG_TRACE_OPTIONS_MASK      (0xFFFF)


#define MNG_TRACE_MODULE_NONE       (0)


/********************************************************************************/
/*                                Common module                                 */
/********************************************************************************/
#define MNG_TRACE_MODULE_COMMON     (1)

#define MNG_TRACE_COMMON_ATTRS      (0)

/********************************************************************************/
/*                                APM module                                    */
/********************************************************************************/
#define MNG_TRACE_MODULE_APM        (2)

#define MNG_TRACE_APM_ATTRS         (0)

/********************************************************************************/
/*                                OMCI module                                   */
/********************************************************************************/
#define MNG_TRACE_MODULE_OMCI       (3)

#define MNG_TRACE_OMCI_BASE_ATTR    (0x0001)
#define MNG_TRACE_DP_ADAPTER_ATTR   (0x0002)
#define MNG_TRACE_OMCI_TL_ATTR      (0x0004)

#define MNG_TRACE_MODULE_OMCI_BASE  (MNG_TRACE_MODULE_OMCI | (MNG_TRACE_OMCI_BASE_ATTR << MNG_TRACE_MODULE_SHIFT))  
#define MNG_TRACE_MODULE_OMCI_DPA   (MNG_TRACE_MODULE_OMCI | (MNG_TRACE_DP_ADAPTER_ATTR << MNG_TRACE_MODULE_SHIFT))  
#define MNG_TRACE_MODULE_OMCI_TL    (MNG_TRACE_MODULE_OMCI | (MNG_TRACE_OMCI_TL_ATTR << MNG_TRACE_MODULE_SHIFT))  

#define MNG_TRACE_OMCI_ATTRS        (MNG_TRACE_MODULE_OMCI_BASE | MNG_TRACE_MODULE_OMCI_DPA | MNG_TRACE_MODULE_OMCI_TL)

/********************************************************************************/
/*                                TPMA module                                   */
/********************************************************************************/
#define MNG_TRACE_MODULE_TPMA       (4)

#define MNG_TRACE_TPMA_ATTRS        (0)

/********************************************************************************/
/*                                OAM module                                   */
/********************************************************************************/
#define MNG_TRACE_MODULE_OAM        (5)   

#define MNG_TRACE_OAM_BASE_ATTR         (0x0001)
#define MNG_TRACE_OAM_DISCOVERY_ATTR    (0x0002)

#define MNG_TRACE_MODULE_OAM_BASE   (MNG_TRACE_MODULE_OAM | (MNG_TRACE_OAM_BASE_ATTR << MNG_TRACE_MODULE_SHIFT))  
#define MNG_TRACE_MODULE_OAM_DISC   (MNG_TRACE_MODULE_OAM | (MNG_TRACE_OAM_DISCOVERY_ATTR << MNG_TRACE_MODULE_SHIFT))  

#define MNG_TRACE_OAM_ATTRS         (MNG_TRACE_MODULE_OAM_BASE | MNG_TRACE_MODULE_OAM_DISC)

    /********************************************************************************/
/*                                CTC module                                   */
/********************************************************************************/
#define MNG_TRACE_MODULE_CTC        (6)   

#define MNG_TRACE_CTC_ATTRS         (0)

/********************************************************************************/
/*                                PON module                                   */
/********************************************************************************/
#define MNG_TRACE_MODULE_PON        (7)   

#define MNG_TRACE_PON_ATTRS         (0)

/********************************************************************************/
/*                                LAST module                                   */
/********************************************************************************/
#define MNG_TRACE_MODULE_LAST       (8)   



typedef struct
{
    UINT32    moduleTraceLevel; /* Severity printing level per module */
    UINT32    moduleOptions;    /* Optional module printing options */

} mng_module_trace_t;


/********************************************************************************/
/*                          MNG trace functions                                 */
/********************************************************************************/
E_ErrorCodes    mvqprintf (UINT32 level, 
                           UINT32 bitMask, 
                           const char *format, ...);

E_ErrorCodes    mngTracePrintModuleStatus (void);

E_ErrorCodes    mngTraceResetModuleStatus (void);

E_ErrorCodes    mngTracePrintModuleList (void);

E_ErrorCodes    mngTraceChangeModuleStatus (UINT32 module, 
                                            UINT32 printLevel, 
                                            UINT32 moduleOptions);

E_ErrorCodes    mngTraceGetModuleStatus (UINT32 module, 
                                         UINT32 *printLevel, 
                                         UINT32 *moduleOptions);



#ifdef __cplusplus
}
#endif


#endif

