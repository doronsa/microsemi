/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : RTOS                                                      **/
/**                                                                          **/
/**  FILE        : OsGlueLayer.h                                             **/
/**                                                                          **/
/**  DESCRIPTION : This file contains declaration of Real-Time Operating     **/
/**                System Glue Layer functions prototype                     **/
/**                                                                          **/
/******************************************************************************/
/**                                                                           */
/**  MODIFICATION HISTORY:                                                    */
/**                                                                           */
/**                                                                           */
/******************************************************************************/

#ifndef __INCOsGlueLayerh
#define __INCOsGlueLayerh

#include <stdio.h>
#include <stdbool.h>
#include <semaphore.h>
#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <mqueue.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <errno.h>
#include "errorCode.h"

#define GL_SEMAPHORE_ID    sem_t*       
#define GL_TIMER_ID        timer_t 
#define GL_QUEUE_ID        mqd_t
#define GL_TASK_ID         pthread_t
#define GL_MUTEX_ID        pthread_mutex_t*
#define GL_MUTEX_ATTR      pthread_mutexattr_t
#define SEM_ID_T           unsigned int


#define APITIMERUNITS_FROM_MINUTES(mins)     ((mins)*60*1000)
#define APITIMERUNITS_FROM_SECONDS(secs)     ((secs)*1000)
#define APITIMERUNITS_FROM_MSECONDS(msecs)   (msecs)


/* Definitions
------------------------------------------------------------------------------*/ 


#ifndef   SEM_Q_FIFO
    #define   SEM_Q_FIFO         0 
#endif 


#define  GL_NO_SUSPEND       0
#define  GL_SUSPEND          1

#define GL_DISABLE_TIMER     0
#define GL_ENABLE_TIMER      1

/*
#define QPRINT_OS_CRITICAL (QPRINT_MASK_CRITICAL | QPRINT_MASK_PRINT_N_LOG | COMMON_MODULE | QPRINT_MASK_TIME_DISPLAY)
#define QPRINT_OS_ERROR    (QPRINT_MASK_ERROR    | QPRINT_MASK_PRINT       | COMMON_MODULE | QPRINT_MASK_TIME_DISPLAY)
#define QPRINT_OS_WARNING  (QPRINT_MASK_WARNING  | QPRINT_MASK_PRINT       | COMMON_MODULE | QPRINT_MASK_TIME_DISPLAY)
#define QPRINT_OS_INFO     (QPRINT_MASK_INFO     | QPRINT_MASK_PRINT       | COMMON_MODULE)
#define QPRINT_OS_TEST     (QPRINT_MASK_TEST     | QPRINT_MASK_PRINT       | COMMON_MODULE)
*/

/* Typedefs
------------------------------------------------------------------------------*/

/* Function pointer to the callback function */
// typedef void (*fGlTimerRoutine)(unsigned long);
// The following is Linux specific
typedef void (*fGlTimerRoutine)(int sigNum, siginfo_t *info, void *context);


// oldtypedef void (*GLFUNCPTR) (void *);
typedef void *(*GLFUNCPTR)(void*);
/*******************************************************************************/
/*                                                                             */
/*                          TASK Handling Section                              */
/*                                                                             */
/*******************************************************************************/
extern E_ErrorCodes osTaskCreate     (GL_TASK_ID    *task, 
                                      char          *name, 
                                      GLFUNCPTR     taskEntry,
                                      unsigned long argc,
                                      void          *argv,
                                      unsigned char priority,
                                      unsigned long stackSize);

extern E_ErrorCodes osTaskDelay      (unsigned int milliseconds);
extern GL_TASK_ID   osTaskSelfIdGet  ();


/*******************************************************************************/
/*                                                                             */
/*                          Message Queue Handling Section                     */
/*                                                                             */
/*******************************************************************************/
extern E_ErrorCodes osMsgQCreate      (GL_QUEUE_ID   *msgQ_p, 
                                       char          *name, 
                                       unsigned long numEntries,
                                       unsigned long messageSize);


extern E_ErrorCodes osMsgQGetReference(GL_QUEUE_ID   *msgQ_p, 
                                       char          *name); 

extern E_ErrorCodes osMsgQDelete      (GL_QUEUE_ID   msgQ);

extern E_ErrorCodes osMsgQSend        (GL_QUEUE_ID   msgQ, 
                                       void          *message, 
                                       unsigned int  size, 
                                       unsigned int  priority);

extern E_ErrorCodes osMsgQReceive     (GL_QUEUE_ID   msgQ, 
                                       void          *message, 
                                       unsigned int  size, 
                                       unsigned int  suspend,
                                       unsigned int  *msgPriority);

extern E_ErrorCodes osMsgQTimedReceive(GL_QUEUE_ID     msgQ, 
                                       void            *message, 
                                       unsigned int    size, 
                                       struct timespec *pTimeoutTime,
                                       unsigned int    *msgPriority);



/*******************************************************************************/
/*                                                                             */
/*                          Semaphore Handling Section                         */
/*                                                                             */
/*******************************************************************************/
extern E_ErrorCodes osSemCreate    (GL_SEMAPHORE_ID *sem, 
                                    char            *name, 
                                    unsigned int    initialCount, 
                                    unsigned long   suspendType,
                                    bool            binarySem);
extern E_ErrorCodes osSemDelete    (void *name_sem);/*for glibc, the input parameter name_sem is "name" of sem, while it is sem id for uclibc.*/
extern E_ErrorCodes osSemTake      (GL_SEMAPHORE_ID sem, unsigned int suspendType);
extern E_ErrorCodes osSemGive      (GL_SEMAPHORE_ID sem);



/*******************************************************************************/
/*                                                                             */
/*                         POSIX Mutex Handling Section                        */
/*                                                                             */
/*******************************************************************************/
E_ErrorCodes osMutexInit   (GL_MUTEX_ID mutex, GL_MUTEX_ATTR *p_attr);
E_ErrorCodes osMutexDestroy(GL_MUTEX_ID mutex);
E_ErrorCodes osMutexLock   (GL_MUTEX_ID mutex);
E_ErrorCodes osMutexTrylock(GL_MUTEX_ID mutex);
E_ErrorCodes osMutexUnlock (GL_MUTEX_ID mutex);   


/*******************************************************************************/
/*                                                                             */
/*                          Timer Handling Section                             */
/*                                                                             */
/*******************************************************************************/

extern E_ErrorCodes osTimerCreate (GL_TIMER_ID     *timerId, 
                                   char            *name, 
                                   fGlTimerRoutine timerRoutine, 
                                   unsigned long   funcParam, 
                                   unsigned long   initialTime, 
                                   unsigned long   rescheduleTime,
                                   int             timerParam);

extern E_ErrorCodes osTimerDelete (GL_TIMER_ID     timerId);
extern E_ErrorCodes osTimerStart  (GL_TIMER_ID     timerId);
//extern E_ErrorCodes osTimerStop   (GL_TIMER_ID     timerId);

// The following routines deal with signo allocation
E_ErrorCodes omciTimerCreate (GL_TIMER_ID     *timerId, 
                              char            *name, 
                              fGlTimerRoutine timerRoutine, 
                              unsigned long   userData, 
                              unsigned long   initialTime, 
                              unsigned long   rescheduleTime,
                              unsigned long   enable);

E_ErrorCodes omciTimerDelete (GL_TIMER_ID timerId, unsigned long userData);

void initOmciTimerSignoPool();


/*******************************************************************************/
/*                                                                             */
/*                       PON sync signal section                               */
/*                                                                             */
/*******************************************************************************/       

E_ErrorCodes initPonSynchronizedSignalCatcher (fGlTimerRoutine actionRoutine, char *buf);


/*******************************************************************************/
/*                                                                             */
/*                       RTOS ticks Handling Section                           */
/*                                                                             */
/*******************************************************************************/       

extern unsigned int  osSecondsGet    ();
extern unsigned long osUptimeGet       ();
//extern unsigned long osTickPerSecGet ();
extern void          setOmciStartTime();

#endif

