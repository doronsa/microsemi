/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.
*
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).
********************************************************************************
*      main.c
*
* DESCRIPTION:
*
*
* CREATED BY:
*
* DATE CREATED: July 12, 2010
*
* DEPENDENCIES:
*
*
* FILE REVISION NUMBER:
*       $Revision: 1.5 $
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "tpm_types.h"
#include "common.h"
#include "params.h"
#include "cli.h"
#include "params_mng.h"
#include "globals.h"
#include "errorCode.h"
#include "mng_trace.h"
#include "ponOnuMngIf.h"
#include "mipc.h"
/*******************************************************************************
* appl_init
*
* DESCRIPTION:      Init Rest Process
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          On success, returns US_RC_OK.
*					On error, returns US_RC_FAIL.
*
*******************************************************************************/
int appl_init(us_sw_app_t sysCfg, us_enabled_t force)
{
  int rcode;

  if (sysCfg != GBE_SW_APP)
  {
    if (force == US_DISABLED)
    {
      if (module_uponOperate() != 0)
	  {
		printf("uponOperate failed\n");
		return (US_RC_FAIL);
	  }
	}
    else
    {
      if (sysCfg == P2P_SW_APP)
      {
        printf("=======================================\n");
        printf("========== P2P FORCE MODE SET =========\n");
        printf("=======================================\n");
        mvEponP2pModeSet(US_ON);
      }
      if (sysCfg == EPON_SW_APP)
      {
        printf("=======================================\n");
        printf("========= EPON FORCE MODE SET =========\n");
        printf("=======================================\n");
      }
      if (sysCfg == GPON_SW_APP)
      {
        printf("=======================================\n");
        printf("========= GPON FORCE MODE SET =========\n");
        printf("=======================================\n");
      }
    }
  }
  else
  {
    printf("=======================================\n");
    printf("============= GbE MODE SET ============\n");
    printf("=======================================\n");
  }

  return(US_RC_OK);
}

/*******************************************************************************
* main
*
* DESCRIPTION:      Rest main
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          none
*
*******************************************************************************/
int main(int argc, char* argv[])
{
  us_sw_app_t         sysCfg = NONE_SW_APP;
  us_enabled_t        force;
  tpm_init_pon_type_t wanTech;;
  int32_t             rcode;
  char                ponName[10] = " ";
  int                 mipc_fd;

  printf("############ UPON Server Init    ############\n");

  mipc_fd = mipc_init("upon", 0, 0);
  if (MIPC_ERROR == mipc_fd)
  {
      return;
  }

  rcode = get_wan_tech_param(&wanTech, &force);
  if (rcode != US_RC_OK)
  {
    fprintf(stderr, "restsvc init failed, wan tech param\n");
    return(US_RC_FAIL);
  }

  switch (wanTech)
  {
    case TPM_EPON: fprintf(stderr, "epon\n"); strcpy(ponName, "epon"); sysCfg = EPON_SW_APP; break;
    case TPM_GPON: fprintf(stderr, "gpon\n"); strcpy(ponName, "gpon"); sysCfg = GPON_SW_APP; break;
    case TPM_P2P:                             strcpy(ponName, "p2p");  sysCfg = P2P_SW_APP;  break;
    default:                                                           sysCfg = GBE_SW_APP;  break;
  }

  if (sysCfg != GBE_SW_APP)
  {
    if (force == US_ENABLED) staticUponSetup(ponName);    /* PON static config */
    else                     sysCfg = dynamicUponSetup(); /* PON dynamic detection */

    system("udevsettle --timeout=3");
  }

  if (appl_init(sysCfg, force) != US_RC_OK)
  {
    fprintf(stderr, "restsvc init failed, aborting\n");
    return(US_RC_FAIL);
  }

  printf("############ UPON Server Running ############\n");
  mthread_register_mq(mipc_fd, mipc_msg_default_handler);
  mthread_start();

  return(US_RC_OK);
}

