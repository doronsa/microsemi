/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include "globals.h"
#include "ponOnuMngIf.h"
#include "qprintf.h"
#include "mng_trace.h"
#include "OsGlueLayer.h"
#include "params_mng.h"
#include "common.h"
#include "tpm_types.h"
#include "params.h"

pthread_t       uponTaskId;
us_sw_app_t     ponType = GPON_SW_APP;

char* get_UPON_sw_version (void);
#define UPON_SW_VERSION  "2.5.25"

#define UPON_SWITCH_ITER    6
UINT8  uponInvStateCnt = 0;

void uponSwitchOver (void);
extern int mvEponP2pModeSet(uint32_t state);

/*******************************************************************************
* dynamicUponSetup()
*
* DESCRIPTION:   Set PON configuration
*
* INPUTS:
*
*
* OUTPUTS:
*
*
* RETURNS:
*
*
* COMMENTS:
*
*******************************************************************************/
us_sw_app_t dynamicUponSetup()
{
	tpm_init_pon_type_t    wanTech;
	us_enabled_t           force;

	if (get_wan_tech_param (&wanTech, &force) != US_RC_OK) {
		qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"%s: Error getting PON type, using default\n", __FUNCTION__);
		wanTech = TPM_NONE;
	}

	switch (wanTech) {
	case TPM_EPON:
	case TPM_P2P:
		ponType = EPON_SW_APP;
		break;
	case TPM_GPON:
	default:
		ponType = GPON_SW_APP;
		break;
	}

	return(ponType);

}

/*******************************************************************************
* staticUponSetup()
*
* DESCRIPTION:   Set PON configuration
*
* INPUTS:
*
*
* OUTPUTS:
*
*
* RETURNS:
*
*
* COMMENTS:
*
*******************************************************************************/
void staticUponSetup(us_sw_app_t swPonType)
{
	tpm_init_pon_type_t    wanTech;
	us_enabled_t           force = US_ENABLED; /* statuc setups is called only in that case */

	switch (swPonType) {
	case EPON_SW_APP:
		wanTech = TPM_EPON;
		break;
	case GPON_SW_APP:
		wanTech = TPM_GPON;
		break;
	case GBE_SW_APP:
		wanTech = TPM_NONE;
		break;
	case P2P_SW_APP:
		wanTech = TPM_P2P;
		break;
	default:
		return;
	}

	return;
}

/*******************************************************************************
* uponOperate()
*
* DESCRIPTION:   Verify whether the PON driver exists
*
* INPUTS:
*
*
* OUTPUTS:
*
*
* RETURNS:
*
*
* COMMENTS:
*
*******************************************************************************/
UINT32 module_uponOperate(void)
{
    if (osTaskCreate(&uponTaskId,
                     "uponThread",
                     (GLFUNCPTR) uponSwitchOver,
                     0,
                     0,
                     50,
                     0x2800) != IAMBA_OK)
		return 1;
	else
		return 0;
}

/*******************************************************************************
* uponEponTypeGet()
*
* DESCRIPTION:   Get UPON type according HW EPON mode
*
* INPUTS:
*
*
* OUTPUTS:
*
*
* RETURNS:
*
*
* COMMENTS:
*
*******************************************************************************/
us_sw_app_t uponEponTypeGet(uint32_t    mode)
{
    us_sw_app_t pt;

	switch(mode) {
    case E_EPON_IOCTL_STD_MODE:
        pt = EPON_SW_APP;
        break;

    case E_EPON_IOCTL_P2P_MODE:
        pt = P2P_SW_APP;
        break;

    default:
        pt = EPON_SW_APP;
        break;
    }

    return pt;
}


/*******************************************************************************
* uponContStateGet()
*
* DESCRIPTION:   Get UPON state according to the three continuous cycles
*
* INPUTS:
*
*
* OUTPUTS:
*
*
* RETURNS:
*
*
* COMMENTS:
*
*******************************************************************************/
us_state_t uponContStateGet (void)
{
	if (uponInvStateCnt < UPON_SWITCH_ITER)
		return US_ON;
	else
		return US_OFF;
}


/*******************************************************************************
* uponStateSet()
*
* DESCRIPTION:   Set invalid UPON state or clear uponInvalidState array
*
* INPUTS:
*
*
* OUTPUTS:
*
*
* RETURNS:
*
*
* COMMENTS:
*
*******************************************************************************/
void uponStateSet (us_state_t   state)
{
	if (state == US_ON)
		uponInvStateCnt = 0;
	else
		uponInvStateCnt++;
}



/*******************************************************************************
* uponSwitchOver()
*
* DESCRIPTION:   Verify whether the PON driver exists
*
* INPUTS:
*
*
* OUTPUTS:
*
*
* RETURNS:
*
*
* COMMENTS:
*
*******************************************************************************/
void uponSwitchOver(void)
{
	S_GponIoctlInfo gponIoctlInfo;
	S_EponIoctlInfo eponIoctlInfo;
    S_EponIoctlPm   eponIoctlCnts;

    eponIoctlInfo.macId = 0;

	while (1) {

		osTaskDelay(500);

		if (ponType == GPON_SW_APP) {

			mvGponGetInfo(&gponIoctlInfo);

			if ((gponIoctlInfo.onuDsSyncOn == US_OFF) && (gponIoctlInfo.onuSignalDetect == US_ON)) {

				uponStateSet(US_OFF);

				if (uponContStateGet() == US_OFF) {
					/* No PON switch in the debug mode */
					if (gponIoctlInfo.onuState != ONU_GPON_DEBUG_STATE) {

						qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"=======================================\n");
						qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"===== SWITCH to EPON or P2P MODE  =====\n");
						qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"=======================================\n");

						if (set_wan_tech_param (TPM_EPON, US_DISABLED) != US_RC_OK)
							qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"FAILED to save PON configuration\n");

						break;

					} else {

						qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"=======================================\n");
						qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"SWITCH detected - No switch to EPON MODE\n");
						qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"=======================================\n");
						uponStateSet(US_ON);
					}
				}

			} else
				uponStateSet(US_ON);

		} else { /* (ponType == EPON_SW_APP) || (ponType == P2P_SW_APP) */

			mvEponGetInfo(&eponIoctlInfo);

			if ((eponIoctlInfo.onuEponDsSyncOkPcs == US_OFF) && (eponIoctlInfo.onuEponSignalDetect == US_ON)) {

				uponStateSet(US_OFF);

				if (uponContStateGet() == US_OFF) {

					qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"=======================================\n");
					qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"========= SWITCH to GPON MODE =========\n");
					qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"=======================================\n");

					if (set_wan_tech_param (TPM_GPON, US_DISABLED) != US_RC_OK)
						qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"FAILED to set PON configuration\n");

					break;
				}

			} else {

				if ((eponIoctlInfo.onuEponDsSyncOkPcs == US_ON) && (eponIoctlInfo.onuEponSignalDetect == US_ON)) {

					if (ponType == EPON_SW_APP) {

						eponIoctlCnts.macId   = 0;
						eponIoctlCnts.section = E_EPON_IOCTL_PM_GPM;
						mvEponGetPmCnt(&eponIoctlCnts);

						if (eponIoctlCnts.gpmCnt.grantValidCnt == 0) {

							uponStateSet(US_OFF);

							if (uponContStateGet() == US_OFF) {
								ponType = P2P_SW_APP;

								qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"=======================================\n");
								qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"============ P2P MODE SET =============\n");
								qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"=======================================\n");

								if (set_wan_tech_param (TPM_P2P, US_DISABLED) != US_RC_OK)
									qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"FAILED to set PON configuration\n");

								/* Set P2P HW registers */
								mvEponP2pModeSet(US_ON);
							}

						} else
							uponStateSet(US_ON);

					} else { /* (ponType == P2P_SW_APP) */

						/* Verify whether PON mode type has been changed by HW upon signal detection interrupt */
						if (ponType != uponEponTypeGet(eponIoctlInfo.onuEponMode)) {
							ponType = EPON_SW_APP;

							qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"=======================================\n");
							qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"============ EPON MODE SET ============\n");
							qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"=======================================\n");

							if (set_wan_tech_param (TPM_EPON, US_DISABLED) != US_RC_OK)
								qprintf(MNG_TRACE_ERROR, MNG_TRACE_MODULE_PON,"FAILED to save PON configuration\n");

							/* Set EPON HW registers */
							mvEponP2pModeSet(US_OFF);
							uponStateSet(US_ON);
						}
					}
				}
			} /*epon or p2p*/
		} /*epon or gpon*/
	} /*while*/

	system ("reboot");
}


char* get_UPON_sw_version (void)
{
    return (UPON_SW_VERSION);
}
