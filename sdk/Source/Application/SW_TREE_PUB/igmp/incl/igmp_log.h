/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : IGMP                                                      **/
/**                                                                          **/
/**  FILE        : IGMP_log.h                                               **/
/**                                                                          **/
/**  DESCRIPTION : Definition of IGMP module                                 **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *                 															  
 *         Ken  - initial version created.   14/March/2011          
 *                                                                              
 ******************************************************************************/

#ifndef _IGMP_LOG_H_
#define _IGMP_LOG_H_


#include <stdarg.h>
#include "stdio.h"
#include "globals.h"

#define IGMP_LOG_MODULE  "IGMP"

typedef enum
{
	IGMP_LOG_NONE = 0x00,
	IGMP_LOG_ERROR,		
	IGMP_LOG_ALARM,		/*such alarm not importance*/
	IGMP_LOG_INFO , /*informat ,such as pkt received*/
	IGMP_LOG_DEBUG,
	IGMP_LOG_DEBUG_EXTPKT,	
	IGMP_LOG_DEBUG_PKT,
}IGMP_LOG_SEVERITY_T;

typedef enum
{
	IGMP_LOG_TO_FILE = 0x00 ,
	IGMP_LOG_TO_STDOUT ,
}IGMP_LOG_TERMINAL_T;


void  IGMP_log_printf(const char * func_name, IGMP_LOG_SEVERITY_T level,const char * format, ...);
INT32 IGMP_log_init();

#endif
