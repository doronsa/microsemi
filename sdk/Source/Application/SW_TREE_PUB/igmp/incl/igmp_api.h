/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : IGMP                                                      **/
/**                                                                          **/
/**  FILE        : igmp_interface.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : Definition of IGMP interfaces                             **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *                                                                               
 *         Ken  - initial version created.   10/March/2010           
 *                                                                              
 ******************************************************************************/

#ifndef _IGMP_API_H_
#define _IGMP_API_H_

#include "globals.h"

#define IGMP_MAX_MULTICAST_VLAN_ENTRIES 32

enum MULTICAST_PROTOCOL_T {
    MULTICAST_PROTOCOL_IGMP_SNOOPING = 0x00,
    MULTICAST_PROTOCOL_CTC = 0x01,
    MULTICAST_PROTOCOL_PROXY = 0x02,
    MULTICAST_PROTOCOL_IGMP_SNOOPING_AND_PROXYREPORT = 0x03 
};
typedef enum MULTICAST_PROTOCOL_T MULTICAST_PROTOCOL_T;

enum MULTICAST_VLAN_OPERS_T {
    MULTICAST_VLAN_OPER_DELETE = 0x00,
    MULTICAST_VLAN_OPER_ADD    = 0x01,
    MULTICAST_VLAN_OPER_CLEAR  = 0x02,
    MULTICAST_VLAN_OPER_LIST   = 0x03,
};
typedef enum MULTICAST_VLAN_OPERS_T MULTICAST_VLAN_OPERS_T;

struct MULTICAST_VLAN_T {
    MULTICAST_VLAN_OPERS_T vlan_operation;
    UINT16 vlan_id[IGMP_MAX_MULTICAST_VLAN_ENTRIES];
    UINT8 num_of_vlan_id;
};
typedef struct MULTICAST_VLAN_T MULTICAST_VLAN_T;
#define IGMP_MAX_MULTICAST_VLAN_SWITCHING_ENTRIES 32

enum MULTICAST_TAG_STRIP_T {
    IGMP_NO_OP_VLAN_TAG = 0x00,
    IGMP_STRIP_OUTER_VLAN_TAG_DOWN = 0x01,
    IGMP_REPLACE_VLAN_ID_DOWN = 0x02,       
    IGMP_REPLACE_VLAN_TAG_DOWN = 0x03,
    IGMP_ADD_VLAN_TAG_DOWN = 0x04,
    IGMP_ADD_VLAN_TAG_UP = 0x05,
    IGMP_REPLACE_VLAN_TAG_UP = 0x06,
    IGMP_REPLACE_VLAN_ID_UP = 0x07,
};
typedef enum MULTICAST_TAG_STRIP_T MULTICAST_TAG_STRIP_T;

struct MULTICAST_TAG_SWITCHING_ENTRY_T {
    UINT16 in_vid;
    UINT16 out_vid;
};
typedef struct MULTICAST_TAG_SWITCHING_ENTRY_T MULTICAST_TAG_SWITCHING_ENTRY_T;

struct MULTICAST_VLAN_SWITCHING_T {
    MULTICAST_TAG_SWITCHING_ENTRY_T entries[IGMP_MAX_MULTICAST_VLAN_SWITCHING_ENTRIES];
    UINT8 number_of_entries;
};
typedef struct MULTICAST_VLAN_SWITCHING_T MULTICAST_VLAN_SWITCHING_T;

struct MULTICAST_PORT_TAG_OPER_T {
    MULTICAST_TAG_STRIP_T us_tag_oper;
    MULTICAST_TAG_STRIP_T ds_tag_oper;
    UINT32 us_tag_default_tci;
    UINT32 ds_tag_default_tci;
    MULTICAST_VLAN_SWITCHING_T multicast_ds_vlan_switching;
    MULTICAST_VLAN_SWITCHING_T multicast_us_vlan_switching;
};
typedef struct MULTICAST_PORT_TAG_OPER_T MULTICAST_PORT_TAG_OPER_T;
#define IGMP_MAX_MULTICAST_MAC_ENTRIES 64
#define IGMP_MAX_MULTICAST_MAC_ENTRIES_PER_TLV 12
#define MULTICAST_CONTROL_ENTRY_LENGTH 10
#define MULTICAST_CONTROL_TLV_HEADER_LENGTH 3
#define IGMP_IPV6_IP_LEN 16

enum MULTICAST_CONTROL_TYPE_T {
    MULTICAST_CONTROL_TYPE_DA_ONLY  = 0x00,
    MULTICAST_CONTROL_TYPE_DA_VID   = 0x01,
    MULTICAST_CONTROL_TYPE_DA_SRCIP = 0x02,
    MULTICAST_CONTROL_TYPE_IPV4_VID = 0x03,
    MULTICAST_CONTROL_TYPE_IPV6_VID = 0x04,
};
typedef enum MULTICAST_CONTROL_TYPE_T MULTICAST_CONTROL_TYPE_T;

enum MULTICAST_CONTROL_ACTION_T {
    MULTICAST_CONTROL_ACTION_DELETE = 0x00,
    MULTICAST_CONTROL_ACTION_ADD = 0x01,
    MULTICAST_CONTROL_ACTION_CLEAR = 0x02,
    MULTICAST_CONTROL_ACTION_LIST = 0x03,
};
typedef enum MULTICAST_CONTROL_ACTION_T MULTICAST_CONTROL_ACTION_T;
#define IGMP_BYTES_IN_MAC_ADDRESS 6

struct MULTICAST_CONTROL_ENTRY_T {
    UINT8 da[IGMP_BYTES_IN_MAC_ADDRESS];
    UINT16 vid;
    UINT32 src_ipv4;
    UINT8 src_ipv6[IGMP_IPV6_IP_LEN];
    UINT16 user_id;
};
typedef struct MULTICAST_CONTROL_ENTRY_T MULTICAST_CONTROL_ENTRY_T;

struct MULTICAST_CONTROL_T {
    MULTICAST_CONTROL_ACTION_T action;
    MULTICAST_CONTROL_TYPE_T control_type;
    MULTICAST_CONTROL_ENTRY_T entries[IGMP_MAX_MULTICAST_MAC_ENTRIES];
    UINT16 num_of_entries;
};
typedef struct MULTICAST_CONTROL_T MULTICAST_CONTROL_T;

struct MULTICAST_GROUP_T {
    UINT8 group_num;
};
typedef struct MULTICAST_GROUP_T MULTICAST_GROUP_T;

enum IGMP_FAST_LEAVE_ADMIN_STATE_T {
    IGMP_FAST_LEAVE_ADMIN_STATE_DISABLED = 0x00000001,
    IGMP_FAST_LEAVE_ADMIN_STATE_ENABLED = 0x00000002,
};
typedef enum IGMP_FAST_LEAVE_ADMIN_STATE_T IGMP_FAST_LEAVE_ADMIN_STATE_T;
#define IGMP_MAX_FAST_LEAVE_ABILITY_MODES 6

enum MULTICAST_FAST_LEAVE_MODE_T {
    IGMP_SNOOPING_NON_FAST_LEAVE_SUPPORT = 0x00000000,
    IGMP_SNOOPING_FAST_LEAVE_SUPPORT = 0x00000001,
    IGMP_MC_CONTROL_NON_FAST_LEAVE_SUPPORT = 0x00000010,
    IGMP_MC_CONTROL_FAST_LEAVE_SUPPORT = 0x00000011,
    IGMP_MLD_SNOOPING_NON_FAST_LEAVE_SUPPORT = 0x00000002,
    IGMP_MLD_SNOOPING_FAST_LEAVE_SUPPORT = 0x00000003,
};
typedef enum MULTICAST_FAST_LEAVE_MODE_T MULTICAST_FAST_LEAVE_MODE_T;

struct MULTICAST_FAST_LEAVE_ABILITY_T {
    UINT16 num_of_modes;
    MULTICAST_FAST_LEAVE_MODE_T modes[IGMP_MAX_FAST_LEAVE_ABILITY_MODES];
};
typedef struct MULTICAST_FAST_LEAVE_ABILITY_T MULTICAST_FAST_LEAVE_ABILITY_T;

struct IPC_MULTICAST_VLAN_T {
    int rcode;
    MULTICAST_VLAN_T mcast_vlan;
};
typedef struct IPC_MULTICAST_VLAN_T IPC_MULTICAST_VLAN_T;

struct IPC_MULTICAST_PORT_TAG_OPER_T {
    int rcode;
    MULTICAST_PORT_TAG_OPER_T mcast_tag;
};
typedef struct IPC_MULTICAST_PORT_TAG_OPER_T IPC_MULTICAST_PORT_TAG_OPER_T;

struct IPC_MULTICAST_PROTOCOL_T {
    int rcode;
    MULTICAST_PROTOCOL_T mcast_protocol;
};
typedef struct IPC_MULTICAST_PROTOCOL_T IPC_MULTICAST_PROTOCOL_T;

struct IPC_MULTICAST_GROUP_T {
    int rcode;
    MULTICAST_GROUP_T group_num;
};
typedef struct IPC_MULTICAST_GROUP_T IPC_MULTICAST_GROUP_T;

struct IPC_MULTICAST_CONTROL_T {
    int rcode;
    MULTICAST_CONTROL_T mcast_control;
};
typedef struct IPC_MULTICAST_CONTROL_T IPC_MULTICAST_CONTROL_T;

struct IPC_MULTICAST_FAST_LEAVE_ABILITY_T {
    int rcode;
    MULTICAST_FAST_LEAVE_ABILITY_T fast_leave;
};
typedef struct IPC_MULTICAST_FAST_LEAVE_ABILITY_T IPC_MULTICAST_FAST_LEAVE_ABILITY_T;

struct IPC_IGMP_FAST_LEAVE_ADMIN_STATE_T {
    int rcode;
    IGMP_FAST_LEAVE_ADMIN_STATE_T admin;
};
typedef struct IPC_IGMP_FAST_LEAVE_ADMIN_STATE_T IPC_IGMP_FAST_LEAVE_ADMIN_STATE_T;

struct igmp_set_multicast_vlan_1_argument {
    UINT32 arg1;
    IPC_MULTICAST_VLAN_T arg2;
};
typedef struct igmp_set_multicast_vlan_1_argument igmp_set_multicast_vlan_1_argument;

struct igmp_set_multicast_tag_strip_1_argument {
    UINT32 arg1;
    IPC_MULTICAST_PORT_TAG_OPER_T arg2;
};
typedef struct igmp_set_multicast_tag_strip_1_argument igmp_set_multicast_tag_strip_1_argument;

struct igmp_set_multicast_group_num_1_argument {
    UINT32 arg1;
    IPC_MULTICAST_GROUP_T arg2;
};
typedef struct igmp_set_multicast_group_num_1_argument igmp_set_multicast_group_num_1_argument;

/*Structure of MC port control table*/
typedef struct 
{
    UINT16     port_id;   
    UINT16     table_type;
    UINT16     set_ctrl;   
    UINT16     row_key;
    UINT16     gem_port;
    UINT16     ani_vid;    
    UINT32     src_ip;
    UINT32     dst_ip_start;   
    UINT32     dst_ip_end;
    UINT32     imputed_group_bw;  
    UINT8      src_ipv6[16];  
    UINT8      dst_ipv6[16];   
    UINT16     preview_len;      
    UINT16     preview_repeat_time;   
    UINT16     preview_repeat_count;  
    UINT16     preview_reset_time; 
    UINT32     vendor_spec;      
    
}mrvl_eth_mc_port_ctrl_t;

/*Structure of MC port service table*/
typedef struct 
{
    UINT16     port_id;   
    UINT16     set_ctrl;   
    UINT16     row_key;
    UINT16     vid;
    UINT32     max_sum_group;
    UINT32     max_mc_bw;   
    
}mrvl_eth_mc_port_serv_t;

/*Structure of MC port preview table*/
typedef struct 
{
    UINT16     port_id;    
    UINT16     set_ctrl;   
    UINT32     row_key;
    UINT8      src_ip[16];  
    UINT8      dst_ip[16];   
    UINT16     ani_vid;
    UINT16     uni_vid;   
    UINT32     duration;
    UINT32     time_left;    
    
}mrvl_eth_mc_port_preview_t;

INT32 IGMP_set_acl_rule(mrvl_eth_mc_port_ctrl_t *mc_acl);
INT32 IGMP_set_serv_rule(mrvl_eth_mc_port_serv_t *mc_serv);
INT32 IGMP_set_preview_rule(mrvl_eth_mc_port_preview_t *mc_preview);

INT32 IGMP_set_multicast_vlan(UINT32 port_id, MULTICAST_VLAN_T *multicast_vlan);
INT32 IGMP_eth_get_multicast_vlan(UINT32 port_id, MULTICAST_VLAN_T *multicast_vlan);
INT32 IGMP_set_multicast_tag_strip(UINT32 port_id, MULTICAST_PORT_TAG_OPER_T *tag_strip);
INT32 IGMP_get_multicast_tag_strip( UINT32 port_id, MULTICAST_PORT_TAG_OPER_T *tag_strip);
INT32 IGMP_set_multicast_switch(MULTICAST_PROTOCOL_T *multicast_protocol);
INT32 IGMP_get_multicast_switch( MULTICAST_PROTOCOL_T *multicast_protocol);
INT32 IGMP_set_multicast_group_num(UINT32 port_id,UINT16 group_num);
INT32 IGMP_get_multicast_group_num(UINT32 port_id,UINT16 *group_num);
INT32 IGMP_set_multicast_control(MULTICAST_CONTROL_T *multicast_control);
INT32 IGMP_get_multicast_control(MULTICAST_CONTROL_T *multicast_control);
INT32 IGMP_get_fast_leave_ability(MULTICAST_FAST_LEAVE_ABILITY_T *fast_leave_ability);
INT32 IGMP_set_fast_leave_admin_control(IGMP_FAST_LEAVE_ADMIN_STATE_T *fast_leave_admin_state);
INT32 IGMP_get_fast_leave_admin_state(IGMP_FAST_LEAVE_ADMIN_STATE_T    *fast_leave_admin_state);

#endif
