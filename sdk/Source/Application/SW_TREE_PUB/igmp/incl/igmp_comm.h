#ifndef _IGMP_COM_H_
#define _IGMP_COM_H_

#define IGMP_EXIT_OK				            0

#define IGMP_ERROR_BASE							(-28000)
#define IGMP_ERROR_NOT_INITIALIZED			    (IGMP_ERROR_BASE-13)
#endif