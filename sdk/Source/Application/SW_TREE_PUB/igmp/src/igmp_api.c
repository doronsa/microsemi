/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.                    
*                                                                                
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED  
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD  
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS  
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".      
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS      
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY    
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),      
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL     
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).                               
******************************************************************************** 
*      igmp.c                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:   Ken                                                   
*                                                                                
* DATE CREATED: March 10, 2011
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.7 $                                                           
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <mqueue.h>
#include <time.h>
#include <signal.h>
#include <stdint.h>
#include <unistd.h>
#include "globals.h"
#include "errorCode.h"
#include "qprintf.h"

#include "igmp_api.h"
#include "igmp_comm.h"
#include "igmp_log.h"

static MULTICAST_PROTOCOL_T g_multicast_protocol = MULTICAST_PROTOCOL_IGMP_SNOOPING;
static UINT16  g_mc_group_num = 0;



#if 0

mrvl_error_code_t mrvl_eth_add_mc_control(UINT32 slot, UINT32 port, mrvl_eth_mc_control_t* mc_group_ctrl)
{
    return MRVL_OK;
}

mrvl_error_code_t mrvl_eth_del_mc_control(UINT32 slot, UINT32 port, mrvl_eth_mc_control_t* mc_group_ctrl)
{
    return MRVL_OK;
}

mrvl_error_code_t mrvl_eth_clear_mc_control(UINT32 slot, UINT32 port)
{
    return MRVL_OK;
}

mrvl_error_code_t mrvl_eth_get_mc_fast_leave_ability(mrvl_eth_mc_fast_leave_ability_t*  fast_leave_ability)
{
    return MRVL_OK;
}

mrvl_error_code_t mrvl_eth_set_mc_fast_leave_mode(BOOL enable)
{
    return MRVL_OK;
}

#endif

/* Local Constant
------------------------------------------------------------------------------*/                                               

/* Global Variables
------------------------------------------------------------------------------*/

/* Local Variables
------------------------------------------------------------------------------*/

/* Local Prototypes
------------------------------------------------------------------------------*/

/* Local Functions
------------------------------------------------------------------------------*/
INT32 IGMP_set_multicast_vlan(UINT32 port_id, MULTICAST_VLAN_T *multicast_vlan)
{
    int i =0;
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"Input parameter: port_id %d\n",port_id);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"Input parameter: multicast_vlan:\n");
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"    vlan_operation: %d\n",multicast_vlan->vlan_operation);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"    num_of_vlan_id: %d\n",multicast_vlan->num_of_vlan_id);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"    vlan_id: ");
    for(i=0;i<multicast_vlan->num_of_vlan_id;i++)
    {
        IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"%d ",multicast_vlan->vlan_id[i]);
        if((7==(i%8))&&((multicast_vlan->num_of_vlan_id-1)!=i))
        {
            IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"\n             ");
        }
    }
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"\n");
    return OK;
}

INT32 IGMP_eth_get_multicast_vlan(UINT32 port_id, MULTICAST_VLAN_T *multicast_vlan)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"Input parameter: port_id %d\n",port_id);
    return OK;
}

INT32 IGMP_set_multicast_tag_strip(UINT32 port_id, MULTICAST_PORT_TAG_OPER_T *tag_strip)
{
    int i =0;
    
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"Input parameter: port_id %d\n",port_id);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"Input parameter: tag_strip:\n");
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"    us_tag_oper: %d\n",tag_strip->us_tag_oper);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"    us_tag_default_tci: %d\n",tag_strip->us_tag_default_tci);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"    ds_tag_oper: %d\n",tag_strip->ds_tag_oper);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"    ds_tag_default_tci: %d\n",tag_strip->ds_tag_default_tci);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"    number_of_entries: %d\n",tag_strip->multicast_ds_vlan_switching.number_of_entries);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"    ds in_vid: ");
    for(i=0;i<tag_strip->multicast_ds_vlan_switching.number_of_entries;i++)
    {
        IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"%d ",tag_strip->multicast_ds_vlan_switching.entries[i].in_vid);
        if((7==(i%8))&&((tag_strip->multicast_ds_vlan_switching.number_of_entries-1)!=i))
        {
            IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"\n                    ");
        }
    }
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"\n");
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"    ds out_vid: ");
    for(i=0;i<tag_strip->multicast_ds_vlan_switching.number_of_entries;i++)
    {
        IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"%d ",tag_strip->multicast_ds_vlan_switching.entries[i].out_vid);
        if((7==(i%8))&&((tag_strip->multicast_ds_vlan_switching.number_of_entries-1)!=i))
        {
            IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"\n                    ");
        }
    }
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"\n");
    return OK;
}

INT32 IGMP_get_multicast_tag_strip( UINT32 port_id, MULTICAST_PORT_TAG_OPER_T *tag_strip)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    return OK;
}

INT32 IGMP_set_multicast_switch(MULTICAST_PROTOCOL_T *multicast_protocol)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"Input parameter: multicast_protocol %d\n",*multicast_protocol);
    if(MULTICAST_PROTOCOL_IGMP_SNOOPING == multicast_protocol)
    {
        g_multicast_protocol = MULTICAST_PROTOCOL_IGMP_SNOOPING;
    }
    else
    {
        g_multicast_protocol = MULTICAST_PROTOCOL_CTC;
    }
    return OK;
}
INT32 IGMP_get_multicast_switch( MULTICAST_PROTOCOL_T *multicast_protocol)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    *multicast_protocol = g_multicast_protocol;
    return OK;
}

INT32 IGMP_set_multicast_group_num(UINT32 port_id,UINT16 group_num)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"Input parameter: port_id %d group_num %d\n",port_id,group_num);
    g_mc_group_num = group_num;
    return OK;
}

INT32 IGMP_get_multicast_group_num(UINT32 port_id,UINT16 *group_num)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"Input parameter: port_id %d\n",port_id);
    *group_num = g_mc_group_num;
    return OK;
}
INT32 IGMP_set_multicast_control(MULTICAST_CONTROL_T *multicast_control)
{
    int i =0, j=0;
    
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"Input parameter: multicast_control\n");
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"action %d\n",multicast_control->action);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"control_type %d\n",multicast_control->control_type);
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"num_of_entries %d\n",multicast_control->num_of_entries);

    for(i=0;i<multicast_control->num_of_entries;i++)
    {
        IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"Entry index %d\r\n",i+1);
        IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"dest address %d:%d:%d:%d:%d:%d \n",\
        multicast_control->entries[i].da[0], multicast_control->entries[i].da[1],\
        multicast_control->entries[i].da[2],multicast_control->entries[i].da[3],\
        multicast_control->entries[i].da[4],multicast_control->entries[i].da[5]);
        IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"user port %d ,vid %d ,ipv4 src addr 0x%x\n",\
        multicast_control->entries[i].user_id, multicast_control->entries[i].vid, \
        multicast_control->entries[i].src_ipv4);
        IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"ipv6 src addr ");
        for(j=0;j<IGMP_IPV6_IP_LEN;j++)
        {
            IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"%d ",multicast_control->entries[i].src_ipv6[j]);
        }
        IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"\n");
    }
    return OK;
}

INT32 IGMP_get_multicast_control(MULTICAST_CONTROL_T *multicast_control)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    return OK;
}

INT32 IGMP_get_fast_leave_ability(MULTICAST_FAST_LEAVE_ABILITY_T *fast_leave_ability)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    fast_leave_ability->num_of_modes = 6;
    fast_leave_ability->modes[0]=IGMP_SNOOPING_NON_FAST_LEAVE_SUPPORT;
    fast_leave_ability->modes[1]=IGMP_SNOOPING_FAST_LEAVE_SUPPORT;
    fast_leave_ability->modes[2]=IGMP_MC_CONTROL_NON_FAST_LEAVE_SUPPORT;
    fast_leave_ability->modes[3]=IGMP_MC_CONTROL_FAST_LEAVE_SUPPORT;
    fast_leave_ability->modes[4]=IGMP_MLD_SNOOPING_NON_FAST_LEAVE_SUPPORT;
    fast_leave_ability->modes[5]=IGMP_MLD_SNOOPING_FAST_LEAVE_SUPPORT;
    return OK;
}

INT32 IGMP_set_fast_leave_admin_control(IGMP_FAST_LEAVE_ADMIN_STATE_T *fast_leave_admin_state)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"Input parameter: fast_leave_admin_state %d\n",*fast_leave_admin_state);
    if(IGMP_FAST_LEAVE_ADMIN_STATE_DISABLED == *fast_leave_admin_state)
    {
    }
    else
    {
    }
    return OK;
}

INT32 IGMP_get_fast_leave_admin_state(IGMP_FAST_LEAVE_ADMIN_STATE_T    *fast_leave_admin_state)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"%s in -----> \n", __FUNCTION__);

    *fast_leave_admin_state = IGMP_FAST_LEAVE_ADMIN_STATE_ENABLED;

    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"%s out <----- \n", __FUNCTION__);    
    return OK;
}

INT32 IGMP_set_acl_rule(mrvl_eth_mc_port_ctrl_t *mc_acl)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"%s in -----> \n", __FUNCTION__);

    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"%s out <----- \n", __FUNCTION__);
    return OK;
    
}

INT32 IGMP_set_serv_rule(mrvl_eth_mc_port_serv_t *mc_serv)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG ,"%s in -----> \n", __FUNCTION__);

    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG, "%s out <----- \n", __FUNCTION__);

    return OK;
}

INT32 IGMP_set_preview_rule(mrvl_eth_mc_port_preview_t *mc_preview)
{
    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"%s in -----> \n", __FUNCTION__);    

    IGMP_log_printf(IGMP_LOG_MODULE,IGMP_LOG_DEBUG,"%s out <----- \n", __FUNCTION__);   

    return OK;
}
