#include <stdio.h>
#include "igmp_log.h"
#include "igmp_comm.h"

/*---------------------------------------------------*/
static IGMP_LOG_SEVERITY_T      g_igmp_log_level        = IGMP_LOG_DEBUG;
static IGMP_LOG_TERMINAL_T      g_igmp_log_terminal     = IGMP_LOG_TO_STDOUT;/*out log terminal mode*/
static INT8                     g_igmp_log_trace_normal = 0;

/*---------------------------------------------------*/
static INT32 IGMP_log_trace_check_if_valid();
/*---------------------------------------------------*/
/*log level*/
void IGMP_log_severity_level_set(IGMP_LOG_SEVERITY_T level)
{
    if(level <= IGMP_LOG_DEBUG_PKT )
    {
        g_igmp_log_level = level ;
    }
}

IGMP_LOG_SEVERITY_T IGMP_log_severity_level_get()
{

    return g_igmp_log_level ;
}

/*log terminal mode ,to tty or file*/
void IGMP_log_terminal_mode_set(IGMP_LOG_TERMINAL_T mode)
{
    if(mode <= IGMP_LOG_TO_STDOUT )
    {
        g_igmp_log_terminal = mode ;
    }
}

IGMP_LOG_TERMINAL_T IGMP_log_terminal_mode_get()
{
    return g_igmp_log_terminal ;
}

/*log terminal implementation*/
static void IGMP_LOG_TERMINAL_Tty_out(const char *format, va_list param)
{
    vprintf(format, param); 
}

static void IGMP_LOG_TERMINAL_Traceout(const char *format, va_list param)
{
    FILE *fd = 0;
    
    if(IGMP_log_trace_check_if_valid() != IGMP_EXIT_OK)
    {
        printf("igmp log terminal tracerout not valid\r\n");        

        return ;
    }

    fd =fopen("/tmp/igmp_config.log", "a");
    if(fd != NULL)
    {
        vfprintf(fd, format, param);

        fclose(fd);        
    }
}


void IGMP_log_printf(const char *func_name, IGMP_LOG_SEVERITY_T level, const char *format, ...)
{ 
    va_list arg_ptr; 
    
    if(IGMP_log_severity_level_get() < level)
    {
        return;
    }

    if(IGMP_log_terminal_mode_get() == IGMP_LOG_TO_FILE)
    {
        va_start(arg_ptr, format); 
        IGMP_LOG_TERMINAL_Traceout(format, arg_ptr); 
        va_end(arg_ptr); 
    }
    else if(IGMP_log_terminal_mode_get() == IGMP_LOG_TO_STDOUT)
    {
        va_start(arg_ptr, format); 
        IGMP_LOG_TERMINAL_Tty_out  (format, arg_ptr); 
        va_end(arg_ptr); 
    }
}

/*-------------------------------------------------------------------------------
* LOG AND TRACEOUT INNER HELP API 
*-------------------------------------------------------------------------------*/
static INT32 IGMP_log_trace_check_if_valid()
{
    if(g_igmp_log_trace_normal != TRUE)
    {
        printf("Error,igmp log traceout is not initialzed!\r\n"); 

        return IGMP_ERROR_NOT_INITIALIZED ;
    }

    return IGMP_EXIT_OK;
}

/*-------------------------------------------------------------------------------
* LOG AND TRACEOUT INIT VIEW
*-------------------------------------------------------------------------------*/
INT32 IGMP_log_init()
{
    static FILE       *fd = NULL; 
    
    fd =fopen("/tmp/igmp_config.log", "r"); 
    if(fd==NULL)  
    { 
        printf("Error note :\n");
        printf("    log file '/tmp/igmp_config.log'failed\n");
        printf("    the log dump function will be affected.\n");
        printf("    pls check the entir-variable or check log file\n");

        g_igmp_log_trace_normal = 0;
    } 
    else
    {
        g_igmp_log_trace_normal = TRUE;

        fclose(fd);
    }
    return 0;
}


INT32 IGMP_STACK_log_terminate()
{
    g_igmp_log_trace_normal = 0;    
    return 0;
}
