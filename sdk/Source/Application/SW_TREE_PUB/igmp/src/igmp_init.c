/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.                    
*                                                                                
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED  
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD  
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS  
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".      
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS      
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY    
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),      
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL     
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).                               
******************************************************************************** 
*      main.c                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:   Ken                                                   
*                                                                                
* DATE CREATED: March 10, 2011
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.6 $                                                           
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "igmp_log.h"
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>        /* For mode constants */
#include <bits/local_lim.h>
#include <pthread.h>
#include <mqueue.h>
#include <time.h>
#include "errorCode.h"
#include "globals.h"
#include "qprintf.h"

/* Local Constant
------------------------------------------------------------------------------*/                                               

/* Global Variables
------------------------------------------------------------------------------*/

/* Local Variables
------------------------------------------------------------------------------*/

/* Local Prototypes
------------------------------------------------------------------------------*/

/* Local Functions
------------------------------------------------------------------------------*/

static INT32 os_chmod(INT8 *pathname,INT32 mode)
{
    return chmod(pathname,(mode_t)mode);
}

static bool IGMP_packet_check_if_initialized()
{
    bool  l_init_flag = false;
    char buff[255] ={0};
     /*start cli pthread deamon*/
    FILE *fd =fopen("/tmp/igmp_config.log", "r");
    if(fd == NULL)
    {
        fd =fopen("/tmp/igmp_config.log", "w+");
        if(fd == NULL)
        {
            printf("/tmp/igmp_config.log' ,please configure the envir-Variable first");
            return false ;
        }
    }

    os_chmod("/tmp/igmp_config.log",438);
        
     while(fgets(buff,255,fd)!= NULL)
    {
        if(strstr(buff,"<igmp statak initialized") != NULL)
        {
            if(strstr(buff,"true") != NULL)
            {
                l_init_flag = true;
                break;
            }
            else
            {
                break;
            }
        }
        memset(buff, 0, sizeof(buff));
    }
    
    fclose(fd);
    
    return l_init_flag ;
}

int IGMP_init(void)
{
    IGMP_packet_check_if_initialized();
    IGMP_log_init();
    return 0;
}
