/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/********************************************************************************
* vendor.h
*
* DESCRIPTION:
*               Get vendor info
*
* DEPENDENCIES:
*               None
*
* CREATED BY:   
*
* DATE CREATED:
*
* FILE REVISION NUMBER:
*               $Revision: 1.1.1.1 $
*
*
*******************************************************************************/
#ifndef _VENDOR_H_
#define _VENDOR_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _LITTLE_ENDIAN
#undef _BIG_ENDIAN
#endif

#ifdef _BIG_ENDIAN
#undef _LITTLE_ENDIAN
#endif

#define POS_PACKED  __attribute__ ((__packed__))
#define ONU_VENDOR_ID "MRVL"
#define ONU_MODEL     "6560"
#define ONU_SW_VER    "V2.0.04"
#define ONU_HW_VER    "M88F6065"

#define ONU_UPSTREAM_QUEUES_NUMBER         4 
#define ONU_MAX_UPSTREAM_QUEUES_PER_PORT   8 
#define ONU_DOWNSTREAM_QUEUES_NUMBER       4
#define ONU_MAX_DOWNSTREAM_QUEUES_PER_PORT 8

#define ONU_88F6560_PON_MULT_LLID_SUPPORT  1
#define ONU_88F6560_PON_IF_NUMBER          1

#define ONU_88F6560_BATTERYBAK     0
#define ONU_88F6560_IPV6_SUPPORT   1

#define ONU_LOID_USER_NAME         "admin"
#define ONU_LOID_PASSWD            "admin"
#define ONU_LOID_USER_NAME_LENGTH  24
#define ONU_LOID_PASSWD_LENGTH     12
#define ONU_LOID_FILE              "/etc/loid.txt"

#define VENDOR_SN_LEN           4
#define VENDOR_MODEL_LEN        4
#define HW_VERSION_NUM_OF_BYTES 8
#define SW_VERSION_NUM_OF_BYTES 16

#define MDU_MAX_SLOT_SPEC       20

#ifndef ERROR
#define ERROR -1
#endif

typedef struct
{
    unsigned char   vendor_id[VENDOR_SN_LEN];
    unsigned char   onu_model[VENDOR_MODEL_LEN];
    unsigned char  *onu_id;
    unsigned char   hardware_version[HW_VERSION_NUM_OF_BYTES];
    unsigned char   software_version[SW_VERSION_NUM_OF_BYTES];
}POS_PACKED  onu_sn;

typedef struct 
{
    unsigned int    msb;
    unsigned int    lsb;
} POS_PACKED mrvl_unsigned_int64;

typedef struct
{
    unsigned char           service_supported;
    unsigned char           ge_ethernet_ports_number;
    mrvl_unsigned_int64     ge_ports_bitmap;
    unsigned char           fe_ethernet_ports_number;
    mrvl_unsigned_int64     fe_ports_bitmap;
    unsigned char           pots_ports_number;
    unsigned char           e1_ports_number;
    unsigned char           upstream_queues_number;
    unsigned char           max_upstream_queues_per_port;
    unsigned char           downstream_queues_number;
    unsigned char           max_downstream_queues_per_port;
    unsigned char           battery_backup;
}POS_PACKED  onu_cap;

typedef enum
{
    MRVL_ONU_CARD_GE =0,
    MRVL_ONU_CARD_FE,
    MRVL_ONU_CARD_POTS,
    MRVL_ONU_CARD_TDM,
    MRVL_ONU_CARD_ADSL2,
    MRVL_ONU_CARD_VDSL2,
    MRVL_ONU_CARD_WIFI,
    MRVL_ONU_CARD_USB,
}MRVL_ONU_Traffic_type_t;

typedef enum
{
    MRVL_ONU_OPTICAL_PROTECTION_NOT_SUPPORT = 0,
    MRVL_ONU_OPTICAL_PROTECTION_TYPE_C,
    MRVL_ONU_OPTICAL_PROTECTION_TYPE_D
}MRVL_OPTICAL_Protection_type;

typedef struct 
{
    MRVL_ONU_Traffic_type_t    interface_type;
    unsigned short             pots_number;
}POS_PACKED MRVL_Interface_capabilities_t;

typedef enum
{
    MRVL_ONU_TYPE_SFU              = 0,
    MRVL_ONU_TYPE_HGU              = 1 ,
    MRVL_ONU_TYPE_SBU              = 2,
    MRVL_ONU_TYPE_F_MDU            = 3, 
    MRVL_ONU_TYPE_X_MDU            = 4, 
    MRVL_ONU_TYPE_BOX_DSL_MDU      = 5 , 
    MRVL_ONU_TYPE_RACK_DSL_MDU     = 6, 
    MRVL_ONU_TYPE_HYBIDR_MDU       = 7, 
    MRVL_ONU_TYPE_OTHER                 
}MRVL_onu_type_t;

typedef struct
{
    MRVL_onu_type_t                 onu_type;
    unsigned char                   multi_llid_support_capabilities;
    MRVL_OPTICAL_Protection_type    onu_optical_protection_type;
    unsigned char                   pon_ports_number;
    unsigned char                   slot_number;
    unsigned char                   interface_type_number;
    MRVL_Interface_capabilities_t   interface_capabilities[MDU_MAX_SLOT_SPEC];
    unsigned char                   battery_backup ;
}POS_PACKED  onu_cap_plus;

typedef struct  
{
    unsigned char     loid[ONU_LOID_USER_NAME_LENGTH];    
    unsigned char     password[ONU_LOID_USER_NAME_LENGTH];
} onu_auth;

typedef enum
{
    MRVL_ONU_POWER_CTL_NOT_SUPPORT = 0,
    MRVL_ONU_POWER_CTL_TX_SUPPORT,        
    MRVL_ONU_POWER_CTL_TX_RX_SUPPORT,        
}MRVL_ONU_POWER_contrl_type;

typedef struct
{
    char                         ipv6_supported;
    MRVL_ONU_POWER_contrl_type   power_ctl_supported;
}POS_PACKED  onu_cap_plus_3;

/*mux community name len*/
typedef struct
{
    unsigned int      mng_ip;            /**< The ip for manage   */
    unsigned int      mng_mask;          /**< The mask for manage */
    unsigned int      mng_gw;            /**< The gateway         */
    unsigned short    data_cvlan;        /**< The data cvlan      */
    unsigned short    data_svlan;        /**< The data svlan      */
    unsigned char     data_priority;     /**< The data priority   */
}POS_PACKED  onu_wan_config;

typedef struct in6_ifreq
{
    struct in6_addr ifr6_addr;
    unsigned int ifr6_prefixlen;
    unsigned int ifr6_ifindex;
}OAM_IPv6_REQ;

extern int MvExtOamResetOnu(void);
extern int MvExtOamUpgradeFirmware(unsigned char image_id, unsigned char file[64]);
extern int MvExtOamGetMac(unsigned char *pMac);
extern int MvExtOamGetEth0Mac(unsigned char *pMac);
extern int MvExtOamGetSn(onu_sn *pOnusn);
extern int MvExtOamGetCap(onu_cap  *pCap);
extern int MvExtOamGetCapplus(onu_cap_plus *pCapplus);
extern int MvExtOamGetCapplus3(onu_cap_plus_3 *pCapplus3);
extern int MvExtOamGetAuthData(onu_auth *pAuthData);
extern int MvExtOamGetWanInfo(onu_wan_config  *pWaninfo);
extern int MvExtOamSetIfAddr(const char *ifname, const char *ipaddr, const char *netmask, const char *gateway);
extern int MvExtOamSetIfIpv6Addr(const char *ifname, const char *ipaddr, const char prefix, const char *gateway);
#ifdef __cplusplus
}
#endif

#endif  /* _SFU_VENDOR_H_*/
