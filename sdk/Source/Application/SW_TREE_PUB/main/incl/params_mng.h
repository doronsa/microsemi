/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
 
 
 #ifndef _PARAMS_MNG_H_
#define _PARAMS_MNG_H_

#ifdef __cplusplus
extern "C" {
#endif

#define US_XML_PON_SN_VENDOR_ID_CHAR_LEN (4)
#define US_XML_PON_SN_SPECIFIC_CHAR_LEN  (8)
#define US_XML_PON_SN_CHAR_LEN (US_XML_PON_SN_VENDOR_ID_CHAR_LEN + US_XML_PON_SN_SPECIFIC_CHAR_LEN)
#define US_XML_PON_SN_LEN          (8)
#define US_XML_PON_PASSWD_LEN      (10)

typedef enum {
	PON_XML_DEF_PAR_SERIAL_NUM         = 0,
	PON_XML_DEF_PAR_PASSWORD           = 1,
	PON_XML_DEF_PAR_DIS_SERIAL_NUM     = 2,
	PON_XML_DEF_PARAM_CLEAR_GEM        = 3,
	PON_XML_DEF_PARAM_CLEAR_TCONT      = 4,
	PON_XML_DEF_PARAM_SERIAL_NUM_SRC   = 5,
    PON_XML_DEF_PARAM_XVR_BURST_ENABLE_POLARITY = 6, 
    PON_XML_DEF_PARAM_XVR_POLARITY     = 7, 
    P2P_XML_DEF_PARAM_XVR_BURST_ENABLE_POLARITY = 8, 
    P2P_XML_DEF_PARAM_XVR_POLARITY     = 9, 
	PON_XML_DEF_PARAM_DG_POLARITY      = 10,
	PON_XML_DEF_PARAM_RESTORE_GEM      = 11,
	PON_XML_DEF_PARAM_FEC_HYST         = 12, 
	PON_XML_DEF_PARAM_COUPLING_MODE    = 13, 
	PON_XML_DEF_PARAM_MAX
} PON_XML_DEF_PARAMS;
/*******************************************************************************
*                             Configuration functions
*******************************************************************************/
int     get_pon_def_params      (void  **param);

int     get_epon_def_params     (unsigned long  *pon_xvr_burst_en_pol, 
                                 unsigned long  *pon_xvr_pol, 
                                 unsigned long  *p2p_xvr_burst_en_pol, 
                                 unsigned long  *p2p_xvr_pol, 
								 unsigned long  *dg_pol, 
								 unsigned long  *pkt_2k_supported
								 );


#ifdef __cplusplus
}
#endif

#endif  /* _PARAMS_MNG_H_*/
