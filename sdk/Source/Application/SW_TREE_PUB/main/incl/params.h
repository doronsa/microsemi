/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/


 #ifndef _PARAMS_H_
#define _PARAMS_H_

#ifdef __cplusplus
extern "C" {
#endif


#define US_RC_OK           0
#define US_RC_FAIL         1
#define US_RC_NOT_FOUND    2

#define US_XML_CFG_FILE_TPM_EPON   "/etc/xml_params/tpm_xml_cfg_file_epon.xml"
#define US_XML_CFG_FILE_TPM_GPON   "/etc/xml_params/tpm_xml_cfg_file_gpon.xml"
#define US_XML_CFG_FILE_EPON       "/etc/xml_params/epon_xml_cfg_file.xml"
#define US_XML_CFG_FILE_GPON       "/etc/xml_params/gpon_xml_cfg_file.xml"
#define US_XML_CFG_FILE_TPM        "/etc/xml_params/tpm_xml_cfg_file.xml"
#define US_XML_CFG_FILE_OMCI       "/etc/xml_params/omci_xml_cfg_file.xml"
#define US_XML_CFG_FILE_PON        "/etc/xml_params/pon_xml_cfg_file.xml"
#define US_XML_CFG_FILE_WAN        "/etc/xml_params/pon_type_xml_cfg_file.xml"
#define US_XML_CFG_FILE_APM        "/etc/xml_params/apm_xml_cfg_file.xml"


#define US_XML_WAN_E               "WAN"
#define US_XML_WAN_TYPE_E          "WAN_type"
#define US_XML_WAN_TYPE_FORCE_E    "WAN_type_force"

#define US_XML_PON_E               "PON"
#define US_XML_PON_SN_E            "PON_serial_num"
#define US_XML_PON_PASSWD_E        "PON_passwd"
#define US_XML_PON_DIS_SN_E        "PON_dis_sn"
#define US_XML_PON_GEM_RST_E       "PON_gem_reset"
#define US_XML_PON_TCONT_RST_E     "PON_tcont_reset"
#define US_XML_PON_SN_SRC_E        "PON_serial_src"
#define US_XML_PON_DG_POL_E        "PON_DG_polarity"
#define US_XML_PON_XVR_POL_E       "PON_XVR_polarity"
#define US_XML_PON_XVR_BURST_EN_POL_E "PON_XVR_burst_enable_polarity"
#define US_XML_PON_XVR_POL_E       "PON_XVR_polarity"
#define US_XML_PON_GEM_RESTOR_E    "PON_gem_restore"
#define US_XML_PON_FEC_HYST_E      "PON_fec_hyst"
#define US_XML_PON_COUPLING_MODE_E "PON_coupling_mode"
#define US_XML_P2P_XVR_BURST_EN_POL_E "P2P_XVR_burst_enable_polarity"
#define US_XML_P2P_XVR_POL_E       "P2P_XVR_polarity"

#define US_XML_EPON_E              "EPON"
#define US_XML_EPON_CONNECT_E      "EPON_connect_tpm"
#define US_XML_EPON_US_QUEUE_E      "EPON_us_queue"
#define US_XML_EPON_DS_QUEUE_E      "EPON_ds_queue"

#define US_XML_EPON_SILENCE_E      "EPON_silence_mode"
#define US_XML_EPON_DG_POL_E       "EPON_DG_polarity"
#define US_XML_EPON_XVR_POL_E      "EPON_XVR_polarity"
#define US_XML_EPON_OAM_RXQ_E      "EPON_OAM_RX_queue"
#define US_XML_EPON_OAM_TXQ_E      "EPON_OAM_TX_queue"
#define US_XML_EPON_LLID_E         "LLID"
#define US_XML_EPON_IGMP_SUPP_E    "EPON_IGMP_supported"
#define US_XML_EPON_2KB_SUPP_E     "EPON_2KB_supported"

#define US_XML_OMCI_E              "OMCI"
#define US_XML_OMCI_ETY_E          "OMCI_ETY"

#define US_XML_IGMP_SNOOP_E        "IGMP_snoop"
#define US_XML_IGMP_SNOOP_ALL_E    "snoop_all"
#define MAX_HOST_NUM_UNI_0           "Max_host_num_UNI_0"
#define MAX_HOST_NUM_UNI_1           "Max_host_num_UNI_1"
#define MAX_HOST_NUM_UNI_2           "Max_host_num_UNI_2"
#define MAX_HOST_NUM_UNI_3           "Max_host_num_UNI_3"
#define IGMP_PKT_FRWD_MODE_UNI_0           "Igmp_pkt_frwd_mode_UNI_0"
#define IGMP_PKT_FRWD_MODE_UNI_1           "Igmp_pkt_frwd_mode_UNI_1"
#define IGMP_PKT_FRWD_MODE_UNI_2           "Igmp_pkt_frwd_mode_UNI_2"
#define IGMP_PKT_FRWD_MODE_UNI_3           "Igmp_pkt_frwd_mode_UNI_3"


#define US_XML_PORT_INIT_E         "port_init"
#define US_XML_DEBUG_PORT_E        "debug_port"
#define US_XML_INT_PORT_E          "int_port"

#define US_XML_ETH_PORT_PARAMS_E   "Ethernet_parameters"
#define US_XML_ETH_PORT_E          "Ethernet"
#define US_XML_ETH_CHIP_CONN_ATTR  "chip_connect"
#define US_XML_ETH_INT_CONN_ATTR   "int_connect"
#define US_XML_ETH_SW_PORT_ATTR    "switch_port"
#define US_XML_ETH_ADMIN_ATTR      "admin_state"
#define US_XML_ETH_SPEED_ATTR      "speed"
#define US_XML_ETH_DUPLEX_ATTR     "duplex"

#define US_XML_GMAC_CONFIG_E       "GMAC_config"
#define US_XML_GMAC_CONN_E         "GMAC_connection"
#define US_XML_GMAC_0_CON_E        "GMAC0_con"
#define US_XML_GMAC_1_CON_E        "GMAC1_con"
#define US_XML_TCONT_LLID_E        "num_tcont_llid"
#define US_XML_GMAC_MH_ENA_E       "GMAC_MH_enable"
#define US_XML_GMAC_0_MH_ENA_E     "GMAC0_mh_en"
#define US_XML_GMAC_1_MH_ENA_E     "GMAC1_mh_en"

#define US_XML_GMAC_BM_BUF_E       "GMAC_bm_buffers"
#define US_XML_LARGE_POOL_BUF_ATTR "large_pkt_pool_bufs"
#define US_XML_SHORT_POOL_BUF_ATTR "small_pkt_pool_bufs"

#define US_XML_GMAC_RXQUEUES_E     "GMAC_rx_queues"


#define US_XML_TX_MOD_PARAMS_E     "tx_module_parameters"
#define US_XML_TX_MOD_E            "tx_mod"
#define US_XML_QUEUE_MAP_E         "queue_map"
#define US_XML_QUEUE_E             "queue"
#define US_XML_QUEUE_SHED_ATTR     "sched_method"
#define US_XML_QUEUE_OWNER_ATTR    "owner"
#define US_XML_QUEUE_OW_Q_NUM_ATTR "owner_q_num"
#define US_XML_QUEUE_WEIGHT_ATTR   "weight"

#define US_XML_TPM_E               "TPM"
#define US_XML_GET_CFG_MODE_E      "GET_config_mode"
#define US_XML_DS_MH_SET_E         "DS_MH_set"
#define US_XML_VALID_EN_E          "VALIDATION_enable" 
#define US_XML_DEFAULT_PING_EN_E   "default_PING_enable"
#define US_XML_SW_GWY_INIT_E       "sw_gwy_init"
#define US_XML_SYSTEM_TYPE         "SYS_type"
#define US_XML_CFG_PNC_PARSER      "PNC_config"
#define US_XML_TRACE_DEBUG_info    "TRACE_DEBUG_info"

#define US_XML_WIFI_ENABLED        "Vitual_UNI_GMAC1_en"
/*#define US_XML_WIFI_PORT           "Vitual_UNI_GMAC1_port"*/

#define US_XML_DBL_TAG_SUPP        "Double_Tag_en"

#if 0
#define US_XML_DEF_V1_TPID         "default_Vlan1_tpid"
#define US_XML_DEF_V2_TPID         "default_Vlan2_tpid"
#endif
#define US_XML_TPM_VLAN_FILTER_TPID_E     "vlan_filter_tpid"
#define US_XML_TPM_FILTER_TPID_E          "filter_tpid"

#define US_XML_CPU_Q_E                "CPU_queue"
#define US_XML_TPM_PNC_E              "PnC"
#define US_XML_TPM_PNC_RANGE_PARAMS_E "range_parameters"
#define US_XML_TPM_PNC_RANGE_E        "range"
#define US_XML_TPM_BASE_LU_ATTR       "base_lu"
#define US_XML_TPM_LAST_LU_ATTR       "last_lu_range"
#define US_XML_TPM_DIR_BM_ATTR        "dir_bm"
#define US_XML_TPM_HARDCODED_ATTR     "hardcoded"
#define US_XML_TPM_MIN_RES_LVL_ATTR   "min_reset_level"
#define US_XML_TPM_RES_WIN_ATTR       "start_reserved_window_size"
#define US_XML_TPM_VLAN_TYPE_E        "VLAN_type"
#define US_XML_TPM_ETY_E              "ETY"
#define US_XML_TPM_MOD_E              "modification"
#define US_XML_TPM_MOD_RES_NUM_E      "RES_num"

#define US_XML_ID_ATTR             "id"
#define US_XML_GMAC_E              "gmac"
#define US_XML_SIZE_ATTR           "size"
#define US_XML_NUM_ATTR            "num"
#define US_XML_TYPE_ATTR           "type"
#define US_XML_VALID_ATTR          "valid"
#define US_XML_ENABLED_E           "enabled"
#define US_XML_TRAFFIC_SETTING_E      "traffic_setting"
#define US_XML_ETY_DSA_ENABLE_E       "ety_dsa_enable"

#define US_XML_APM_E                            "APM"
#define US_XML_APM_ALARM_POLL_INTERVAL_E        "alarm_poll_interval"
#define US_XML_APM_PM_POLL_INTERVAL_E           "pm_poll_interval"
#define US_XML_APM_AVC_POLL_INTERVAL_E          "avc_poll_interval"
#define US_XML_APM_ALARM_MAX_ENTITY_NUM_E       "alarm_max_entity_num"
#define US_XML_APM_ALARM_ACTIVATION_E           "apm_alarm_activation"
#define US_XML_APM_PM_ACTIVATION_E              "apm_pm_activation"
#define US_XML_APM_AVC_ACTIVATION_E             "apm_avc_activation"



#define US_XML_APM_PM_MAX_ENTITY_NUM_E          "pm_max_entity_num"
#define US_XML_APM_AVC_MAX_ENTITY_NUM_E         "avc_max_entity_num"








/*******************************************************************************
*                             Configuration functions
*******************************************************************************/
int     get_wan_tech_param      (tpm_init_pon_type_t    *wanTech,
                                 us_enabled_t           *force);
int     set_wan_tech_param      (tpm_init_pon_type_t    wanTech,
								 us_enabled_t           force);
int     get_omci_etype_param    (uint32_t    *ety);
int     get_omci_txq_param      (uint32_t    *cpuTxq);
int     get_debug_port_params   (uint32_t    *valid, 
                                 uint32_t    *num);
int     get_igmp_snoop_params   (uint32_t    *enable, 
                                 uint32_t    *que);
int     get_gmac_conn_params    (uint32_t               *num_tcont_llid);
int     get_gmac_mh_en_params   (uint32_t               *gmac0_mh_en,
                                 uint32_t               *gmac1_mh_en);
int     get_gmac_pool_bufs_params(tpm_init_gmac_bufs_t *gmac_bufs,
                                 int                max_gmacs_num);
int     get_gmac_rxq_params    (tpm_init_gmac_rx_t *gmac_rx,
                                int                 max_gmacs_num,
                                int                 max_rx_queues_num);
int     get_gmac_tx_params      (tpm_init_gmac_tx_t     *gmac_tx, 
                                 int                    max_tx_ports_num, 
                                 int                    max_tx_queues_num);
int     get_pnc_range_params    (tpm_init_pnc_range_t   *pnc_range,
                                 int                    max_pnc_ranges_num);

int     get_epon_igmp_config_params(uint32_t *igmpSupp);
int     get_epon_oam_txq_params    (uint32_t  *oam_tx_queues);

int     get_epon_silence_mode_params(uint32_t *silence_enabled);

int convertXmlSn2MsgSn(unsigned char *xml_sn, unsigned char *msg_sn);


#ifdef __cplusplus
}
#endif

#endif  /* _PARAMS_H_*/
