  //epon---------------------------------------------------------------------
  {"set_epon_connect_tpm_params",            set_epon_connect_tpm_params}, 
  {"set_epon_def_params",                    set_epon_def_params}, 
  {"set_epon_oam_rx_q_params",               set_epon_oam_rx_q_params}, 
  {"set_epon_igmp_config_params",            set_epon_igmp_config_params},
  {"set_epon_oam_txq_params",                set_epon_oam_txq_params},
  {"set_epon_silence_mode",                  set_epon_silence_mode},
   //epon tpm----------------------------------------------------------------
  {"set_epon_pnc_range_params",                   set_epon_pnc_range_params},
  {"set_epon_ety_dsa_enable",                     set_epon_ety_dsa_enable},
  {"set_epon_default_vlan_tpid_params",           set_epon_default_vlan_tpid_params},
  {"set_epon_ds_mh_config_params",                set_epon_ds_mh_config_params},
  {"set_epon_cfg_pnc_parse_param",                set_epon_cfg_pnc_parse_param},
  {"set_epon_validation_enabled_config_params",   set_epon_validation_enabled_config_params},
  {"set_epon_config_mode_param",                  set_epon_config_mode_param},
  {"set_epon_gmac_tx_params",                     set_epon_gmac_tx_params},
  {"set_epon_gmac_rxq_params",                    set_epon_gmac_rxq_params},
  {"set_epon_gmac_pool_bufs_params",              set_epon_gmac_pool_bufs_params},
  {"set_epon_gmac_mh_en_params",                  set_epon_gmac_mh_en_params},
  {"set_epon_gmac_conn_params",                   set_epon_gmac_conn_params},
  {"set_epon_pppoe_add_enable",                   set_epon_pppoe_add_enable},
  {"set_epon_num_vlan_tags",                      set_epon_num_vlan_tags},
  {"set_epon_ttl_illegal_action",                 set_epon_ttl_illegal_action},
  {"set_epon_tcp_flag_check",                     set_epon_tcp_flag_check},
  {"set_epon_cpu_trap_rx_queue",                  set_epon_cpu_trap_rx_queue},
  {"set_epon_mtu_setting_enabled",                set_epon_mtu_setting_enabled},
  {"set_epon_ipv4_mtu_ds",                        set_epon_ipv4_mtu_ds},
  {"set_epon_ipv4_mtu_us",                        set_epon_ipv4_mtu_us},
  {"set_epon_ipv4_pppoe_mtu_us",                  set_epon_ipv4_pppoe_mtu_us},
  {"set_epon_igmap_snoop_enabled",                set_epon_igmap_snoop_enabled},
  {"set_epon_igmp_cpu_rx_queue",                  set_epon_igmp_cpu_rx_queue},
  {"set_epon_igmp_frwrd_mode_wan",                set_epon_igmp_frwrd_mode_wan},
  {"set_epon_igmp_frwrd_mode_uni0",               set_epon_igmp_frwrd_mode_uni0},
  {"set_epon_igmp_frwrd_mode_uni1",               set_epon_igmp_frwrd_mode_uni1},
  {"set_epon_igmp_frwrd_mode_uni2",               set_epon_igmp_frwrd_mode_uni2},
  {"set_epon_igmp_frwrd_mode_uni3",               set_epon_igmp_frwrd_mode_uni3},
  {"set_epon_igmp_frwrd_mode_uni4",               set_epon_igmp_frwrd_mode_uni4},
  {"set_epon_mc_pppoe_enable",                    set_epon_mc_pppoe_enable},
  {"set_epon_mc_per_uni_vlan_xlat",               set_epon_mc_per_uni_vlan_xlat},
  {"set_epon_mc_filter_mode",                     set_epon_mc_filter_mode},
  {"set_epon_mc_hwf_queue",                       set_epon_mc_hwf_queue},
  {"set_epon_mc_cpu_queue",                       set_epon_mc_cpu_queue},
  {"set_epon_cpu_wan_egr_loopback",               set_epon_cpu_wan_egr_loopback},
  {"set_epon_gmac1_virt_uni",                     set_epon_gmac1_virt_uni},
  {"set_epon_trace_debug_info",                   set_epon_trace_debug_info},
  {"set_epon_catch_all_pkt_action",               set_epon_catch_all_pkt_action},
  {"set_epon_udp_checksum_calc",                  set_epon_udp_checksum_calc},
  {"set_epon_udp_checksum_use_init",              set_epon_udp_checksum_use_init},
  {"set_epon_chain_params",                       set_epon_chain_params},
  {"set_epon_mod_tpid_params",                    set_epon_mod_tpid_params},