/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/******************************************************************************** 
*      tpm_cli.h                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:                                                     
*                                                                                
* DATE CREATED: June 29, 2010     
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.2 $                                                           
*******************************************************************************/


#ifndef __MAIN_CLI_H__
#define __MAIN_CLI_H__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"

/********************************************************************************/
/*                      TPM CLI show DB data functions                          */
/********************************************************************************/
bool_t  main_cli_print_version              (const clish_shell_t    *instance,  
                                             const lub_argv_t       *argv);

bool_t  main_cli_print_module_versions      (const clish_shell_t    *instance,  
                                             const lub_argv_t       *argv);
#endif /*__MAIN_CLI_H__*/
