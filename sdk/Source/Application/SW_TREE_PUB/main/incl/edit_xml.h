/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "clish/shell.h"
#include "tpm_types.h"
#include "tpm_internal_types.h"
#include "ezxml.h"
#include "common.h"
#include "oam_expo.h"
#include "OmciDefs.h"
#include "errorCode.h"
#include "ProfileCache.h"
#include "params.h"
#include "params_mng.h"
#include "ponOnuMngIf.h"
#include "ponOnuInternals.h"



// ALL
void Update_help (const clish_shell_t    *instance,const lub_argv_t       *argv);
void printUpdateHelpALL();
void printUpdateHelpEpon();
void printUpdateHelpTpmEpon();
void printUpdateHelpGpon();
void printUpdateHelpTpmGpon();
void printUpdateHelpOmci();
bool_t     set_WAN_type             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_WAN_type_force       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
//EPON update functions

bool_t     set_epon_connect_tpm_params      (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_def_params              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_oam_rx_q_params         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_igmp_config_params      (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_oam_txq_params          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
//GPON
bool_t     set_gpon_serial_num              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_def_params              (int choise, char* new_value);
bool_t     set_gpon_dg_polarity             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_def_password            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_def_dis                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_def_gem                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_def_tcont               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_def_sn_src              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_def_xvr_pol             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

//OMCI
bool_t     update_omci_etype_param          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     update_omci_HW_cells             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     update_omci_txq_param            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupVoiceSupport       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupVoipSupport        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupIpHostDataSupport  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupVoipConfigByOmci   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupVoipProtocolSip    (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupOntTypeSfu         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciCellcap                   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupEthernetports      (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupNumPorts           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupEthernetNumPorts   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupPOTSslotnumber     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupVEIPslotnumber     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupANIslotnumber      (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciStartupfirstNonOmciTcontInfo  (const clish_shell_t    *instance,
                                                 const lub_argv_t       *argv);
bool_t     setOmciStartupgemPortPmByApmActivation  (const clish_shell_t    *instance,
                                                    const lub_argv_t       *argv);
bool_t     set_epon_silence_mode            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     setOmciDebugconsoleOutput        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
//EPON TPM
bool_t     set_epon_pnc_range_params             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_ety_dsa_enable               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_default_vlan_tpid_params     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_ds_mh_config_params          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_cfg_pnc_parse_param          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_validation_enabled_config_params   (const clish_shell_t    *instance,
                                                   const lub_argv_t       *argv);
bool_t     set_epon_config_mode_param            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_gmac_tx_params               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_gmac_rxq_params              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_gmac_pool_bufs_params        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_gmac_mh_en_params            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_gmac_conn_params             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_pppoe_add_enable             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_num_vlan_tags                (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_ttl_illegal_action           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_tcp_flag_check               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_cpu_trap_rx_queue            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_mtu_setting_enabled          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_ipv4_mtu_ds                  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_ipv4_mtu_us                  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_ipv4_pppoe_mtu_us            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_igmap_snoop_enabled          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_igmp_cpu_rx_queue            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_igmp_frwrd_mode_wan          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_igmp_frwrd_mode_uni0         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_igmp_frwrd_mode_uni1         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_igmp_frwrd_mode_uni2         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_igmp_frwrd_mode_uni3         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_igmp_frwrd_mode_uni4         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_mc_pppoe_enable              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_mc_per_uni_vlan_xlat         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_mc_filter_mode               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_mc_hwf_queue                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_mc_cpu_queue                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_cpu_wan_egr_loopback         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_gmac1_virt_uni               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_trace_debug_info             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_catch_all_pkt_action         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_udp_checksum_calc            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_udp_checksum_use_init        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_chain_params                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_epon_mod_tpid_params              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
//GPON TPM
bool_t     set_gpon_pnc_range_params             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_ety_dsa_enable               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_default_vlan_tpid_params     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_ds_mh_config_params          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_cfg_pnc_parse_param          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_validation_enabled_config_params   (const clish_shell_t    *instance,
                                                   const lub_argv_t       *argv);
bool_t     set_gpon_config_mode_param            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_gmac_tx_params               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_gmac_rxq_params              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_gmac_pool_bufs_params        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_gmac_mh_en_params            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_gmac_conn_params             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_pppoe_add_enable             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_num_vlan_tags                (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_ttl_illegal_action           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_tcp_flag_check               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_cpu_trap_rx_queue            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_mtu_setting_enabled          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_ipv4_mtu_ds                  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_ipv4_mtu_us                  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_ipv4_pppoe_mtu_us            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_igmap_snoop_enabled          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_igmp_cpu_rx_queue            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_igmp_frwrd_mode_wan          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_igmp_frwrd_mode_uni0         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_igmp_frwrd_mode_uni1         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_igmp_frwrd_mode_uni2         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_igmp_frwrd_mode_uni3         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_igmp_frwrd_mode_uni4         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_mc_pppoe_enable              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_mc_per_uni_vlan_xlat         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_mc_filter_mode               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_mc_hwf_queue                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_mc_cpu_queue                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_cpu_wan_egr_loopback         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_gmac1_virt_uni               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_trace_debug_info             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_catch_all_pkt_action         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_udp_checksum_calc            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_udp_checksum_use_init        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_chain_params                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t     set_gpon_mod_tpid_params              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
