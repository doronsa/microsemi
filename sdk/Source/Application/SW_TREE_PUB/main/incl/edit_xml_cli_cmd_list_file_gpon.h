/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/********************************************************************************/
/*                      Update CLI show functions                               */
/********************************************************************************/
  //OMCI-------------------------------------------------------------------
  {"update_omci_etype_param",                update_omci_etype_param}, 
  {"update_omci_HW_cells",                   update_omci_HW_cells}, 
  {"update_omci_txq_param",                  update_omci_txq_param},  
  {"setOmciStartupVoiceSupport",             setOmciStartupVoiceSupport},
  {"setOmciStartupVoipSupport",              setOmciStartupVoipSupport},
  {"setOmciStartupIpHostDataSupport",        setOmciStartupIpHostDataSupport},
  {"setOmciStartupVoipConfigByOmci",         setOmciStartupVoipConfigByOmci},
  {"setOmciStartupVoipProtocolSip",          setOmciStartupVoipProtocolSip},
  {"setOmciStartupOntTypeSfu",               setOmciStartupOntTypeSfu},
  {"setOmciCellcap",                         setOmciCellcap},
  {"setOmciStartupEthernetports",            setOmciStartupEthernetports},
  {"setOmciStartupNumPorts",                 setOmciStartupNumPorts},
  {"setOmciStartupEthernetNumPorts",         setOmciStartupEthernetNumPorts},
  {"setOmciStartupPOTSslotnumber",           setOmciStartupPOTSslotnumber},
  {"setOmciStartupVEIPslotnumber",           setOmciStartupVEIPslotnumber},
  {"setOmciStartupANIslotnumber",            setOmciStartupANIslotnumber},
  {"setOmciStartupfirstNonOmciTcontInfo",    setOmciStartupfirstNonOmciTcontInfo},
  {"setOmciStartupgemPortPmByApmActivation", setOmciStartupgemPortPmByApmActivation},
  {"setOmciDebugconsoleOutput",              setOmciDebugconsoleOutput},

  //gpon---------------------------------------------------------------
  {"set_gpon_serial_num",                    set_gpon_serial_num},  
  {"set_gpon_dg_polarity",                   set_gpon_dg_polarity},  
  {"set_gpon_serial_num",                    set_gpon_serial_num},  
  {"set_gpon_def_password",                  set_gpon_def_password},  
  {"set_gpon_def_dis",                       set_gpon_def_dis},  
  {"set_gpon_def_gem",                       set_gpon_def_gem},  
  {"set_gpon_def_tcont",                     set_gpon_def_tcont},  
  {"set_gpon_def_sn_src",                    set_gpon_def_sn_src},  
  {"set_gpon_def_xvr_pol",                   set_gpon_def_xvr_pol},  

  //gpon tpm----------------------------------------------------------------
  {"set_gpon_pnc_range_params",                   set_gpon_pnc_range_params},
  {"set_gpon_ety_dsa_enable",                     set_gpon_ety_dsa_enable},
  {"set_gpon_default_vlan_tpid_params",           set_gpon_default_vlan_tpid_params},
  {"set_gpon_ds_mh_config_params",                set_gpon_ds_mh_config_params},
  {"set_gpon_cfg_pnc_parse_param",                set_gpon_cfg_pnc_parse_param},
  {"set_gpon_validation_enabled_config_params",   set_gpon_validation_enabled_config_params},
  {"set_gpon_config_mode_param",                  set_gpon_config_mode_param},
  {"set_gpon_gmac_tx_params",                     set_gpon_gmac_tx_params},
  {"set_gpon_gmac_rxq_params",                    set_gpon_gmac_rxq_params},
  {"set_gpon_gmac_pool_bufs_params",              set_gpon_gmac_pool_bufs_params},
  {"set_gpon_gmac_mh_en_params",                  set_gpon_gmac_mh_en_params},
  {"set_gpon_gmac_conn_params",                   set_gpon_gmac_conn_params},
  {"set_gpon_pppoe_add_enable",                   set_gpon_pppoe_add_enable},
  {"set_gpon_num_vlan_tags",                      set_gpon_num_vlan_tags},
  {"set_gpon_ttl_illegal_action",                 set_gpon_ttl_illegal_action},
  {"set_gpon_tcp_flag_check",                     set_gpon_tcp_flag_check},
  {"set_gpon_cpu_trap_rx_queue",                  set_gpon_cpu_trap_rx_queue},
  {"set_gpon_mtu_setting_enabled",                set_gpon_mtu_setting_enabled},
  {"set_gpon_ipv4_mtu_ds",                        set_gpon_ipv4_mtu_ds},
  {"set_gpon_ipv4_mtu_us",                        set_gpon_ipv4_mtu_us},
  {"set_gpon_ipv4_pppoe_mtu_us",                  set_gpon_ipv4_pppoe_mtu_us},
  {"set_gpon_igmap_snoop_enabled",                set_gpon_igmap_snoop_enabled},
  {"set_gpon_igmp_cpu_rx_queue",                  set_gpon_igmp_cpu_rx_queue},
  {"set_gpon_igmp_frwrd_mode_wan",                set_gpon_igmp_frwrd_mode_wan},
  {"set_gpon_igmp_frwrd_mode_uni0",               set_gpon_igmp_frwrd_mode_uni0},
  {"set_gpon_igmp_frwrd_mode_uni1",               set_gpon_igmp_frwrd_mode_uni1},
  {"set_gpon_igmp_frwrd_mode_uni2",               set_gpon_igmp_frwrd_mode_uni2},
  {"set_gpon_igmp_frwrd_mode_uni3",               set_gpon_igmp_frwrd_mode_uni3},
  {"set_gpon_igmp_frwrd_mode_uni4",               set_gpon_igmp_frwrd_mode_uni4},
  {"set_gpon_mc_pppoe_enable",                    set_gpon_mc_pppoe_enable},
  {"set_gpon_mc_per_uni_vlan_xlat",               set_gpon_mc_per_uni_vlan_xlat},
  {"set_gpon_mc_filter_mode",                     set_gpon_mc_filter_mode},
  {"set_gpon_mc_hwf_queue",                       set_gpon_mc_hwf_queue},
  {"set_gpon_mc_cpu_queue",                       set_gpon_mc_cpu_queue},
  {"set_gpon_cpu_wan_egr_loopback",               set_gpon_cpu_wan_egr_loopback},
  {"set_gpon_gmac1_virt_uni",                     set_gpon_gmac1_virt_uni},
  {"set_gpon_trace_debug_info",                   set_gpon_trace_debug_info},
  {"set_gpon_catch_all_pkt_action",               set_gpon_catch_all_pkt_action},
  {"set_gpon_udp_checksum_calc",                  set_gpon_udp_checksum_calc},
  {"set_gpon_udp_checksum_use_init",              set_gpon_udp_checksum_use_init},
  {"set_gpon_chain_params",                       set_gpon_chain_params},
  {"set_gpon_mod_tpid_params",                    set_gpon_mod_tpid_params},
