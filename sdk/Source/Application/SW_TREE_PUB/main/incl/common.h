/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _US_COMMON_H_
#define _US_COMMON_H_

#ifdef __cplusplus
extern "C" {
#endif


#define US_RC_OK           0
#define US_RC_FAIL         1
#define US_RC_NOT_FOUND    2

typedef enum
{
    NONE_SW_APP = 0,
    EPON_SW_APP,
    GPON_SW_APP,
    GBE_SW_APP,
    P2P_SW_APP,

    MAX_SW_APP
} us_sw_app_t;

typedef enum
{
    US_DISABLED = 0,
    US_ENABLED
} us_enabled_t;

typedef enum
{
    US_OFF = 0,
    US_ON
} us_state_t;



#ifdef __cplusplus
}
#endif

#endif  /* _US_COMMON_H_*/

