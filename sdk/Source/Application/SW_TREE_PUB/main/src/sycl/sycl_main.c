/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.
*
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).
********************************************************************************
*      main.c
*
* DESCRIPTION:
*
*
* CREATED BY:
*
* DATE CREATED: July 12, 2010
*
* DEPENDENCIES:
*
*
* FILE REVISION NUMBER:
*       $Revision: 1.26 $
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/resource.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "common.h"
#include "tpm_types.h"
#include "params.h"
#include "cli.h"
#include "params_mng.h"
#include "globals.h"
#include "errorCode.h"
#include "OmciGlobalData.h"
#include "apm.h"
#include "mng_trace.h"
#include "ponOnuMngIf.h"
#include "mipc.h"

#define RLIMIT_MSGQUEUE 12

typedef enum {
    SYCL_EPON = 1 << EPON_SW_APP,
    SYCL_GPON = 1 << GPON_SW_APP,
    SYCL_GBE  = 1 << GBE_SW_APP,
    SYCL_P2P  = 1 << P2P_SW_APP,
} SYCL_WAN_TYPE_T;

typedef struct {
    const char*     name;
    SYCL_WAN_TYPE_T wanType;
    const char*     binPath;
    int             pid;
} SYCL_APP_T;

SYCL_WAN_TYPE_T gSyclWanType;

void sycl_mipc_server_handler(char* inMsg, unsigned int size);


SYCL_APP_T gSyclApplication[] = {
    {"rstp",       SYCL_EPON|SYCL_GPON|SYCL_GBE|SYCL_P2P, "/usr/bin/rstp",       0},
    {"misc",       SYCL_EPON|SYCL_GPON|SYCL_GBE|SYCL_P2P, "/usr/bin/misc",       0},
    {"igmp",       SYCL_EPON|SYCL_GPON|SYCL_GBE|SYCL_P2P, "/usr/bin/igmp",       0},
    {"voip",       SYCL_EPON|SYCL_GPON|SYCL_GBE|SYCL_P2P, "/usr/bin/voip",       0},
    {"apm",    SYCL_EPON|SYCL_GPON|SYCL_GBE|SYCL_P2P,     "/usr/bin/apm",    0},
    {"midware",    SYCL_EPON|SYCL_GPON|SYCL_GBE|SYCL_P2P, "/usr/bin/midware",    0},
    {"oam",        SYCL_EPON,                             "/usr/bin/oam_stack",  0},
    {"omci",       SYCL_GPON,                             "/usr/bin/omci",       0},
    {"flash",      SYCL_EPON|SYCL_GPON|SYCL_GBE|SYCL_P2P, "/usr/bin/flash",      0},
//  {"clish",      SYCL_EPON|SYCL_GPON|SYCL_GBE|SYCL_P2P, "/usr/bin/clish",      0}
    };

/*******************************************************************************
* sycl_start_application()
*******************************************************************************/
int sycl_start_application(SYCL_APP_T* pApp)
{
    int         ret;
    struct stat file_stat;
    int         pid;
    char*       argv[4];

    ret = stat(pApp->binPath, &file_stat);
    if (0 != ret)
    {
        printf("sycl_start_application: cannot find %s\r\n", pApp->binPath);
        return -1;
    }

    pid = fork();

    if (pid < 0)
    {
        printf("sycl_start_application: cannot create process for %s\r\n", pApp->binPath);
        return -1;
    }

    if (0 == pid)
    {
        argv[0] = (char*)pApp->binPath;
        argv[1] = (char*)NULL;
        if (0 > execv(pApp->binPath, argv))
        {
            printf("sycl_start_application: cannot create process for %s\r\n", pApp->binPath);
            exit(-1);
        }
    }

    pApp->pid = pid;

    while (0 != mipc_ping(pApp->name, -1))
    {
        osTaskDelay(100);
    }

    return 0;
}

/*******************************************************************************
* sycl_restart_application()
*******************************************************************************/
int sycl_restart_application(SYCL_APP_T* pApp)
{
    char str[32];

    sprintf(str, "kill -9 %d", pApp->pid);
    system(str);
    osTaskDelay(500);
    return sycl_start_application(pApp);
}

/*******************************************************************************
* syscl_get_wan_params_hook()
*******************************************************************************/
int syscl_get_wan_params_hook(tpm_init_pon_type_t *wanTech, us_enabled_t *force)
{
	/* Replace this hook with custome code for other parameters storage media */
	return(get_wan_tech_param(wanTech, force));
}

/*******************************************************************************
* syscl_pon_driver_init()
*******************************************************************************/
int syscl_pon_driver_init(us_sw_app_t ponCfg)
{
	int					tmpFd = -1;
	E_PonDriverMode		drvMode, tmpDrvMode;

	tmpFd = open("/dev/pon", O_RDWR);
	if (tmpFd < 0) {
		printf("Failed to open /dev/pon\n");
		return US_RC_FAIL;
	}

	switch (ponCfg) {
	case GPON_SW_APP:
		drvMode = E_PON_DRIVER_GPON_MODE;
		break;
	case EPON_SW_APP:
	case P2P_SW_APP:
		drvMode = E_PON_DRIVER_EPON_MODE;
		break;
	default:
		fprintf(stderr, "Wrong PON driver init mode %d!\n", ponCfg);
		goto pon_driver_init_fail;
	}

	if (ioctl(tmpFd, MVPON_IOCTL_START, &drvMode) < 0) {
		printf("Failed to set PON driver mode\n");
		goto pon_driver_init_fail;
	}
	if (ioctl(tmpFd, MVPON_IOCTL_MODE_GET, &tmpDrvMode) < 0) {
		printf("Failed to get PON driver mode\n");
		goto pon_driver_init_fail;
	}

	close(tmpFd);

	if (drvMode != tmpDrvMode) {
		printf("Wrong PON mode - set to %d, but returns %d\n", drvMode, tmpDrvMode);
		return US_RC_FAIL;
	}

	return US_RC_OK;

pon_driver_init_fail:
	close(tmpFd);
	return US_RC_FAIL;

}

/*******************************************************************************
* syscl_pon_module_setup()
*******************************************************************************/
int syscl_pon_module_setup(us_sw_app_t ponCfg)
{
    int status;
    
	if (GPON_SW_APP == ponCfg)
	{
        return (module_gponSetup());
	}
	else if (EPON_SW_APP == ponCfg)
	{
		return (module_eponSetup());
	}
	else if (P2P_SW_APP == ponCfg)
	{
        status = module_eponSetup();
        status |= mvEponP2pModeSet(US_ON);
        status |= mvEponP2pForceModeSet(US_ON);
		return (status);
	}
	else
	{
		return (US_RC_FAIL);
	}
}

/*******************************************************************************
* sycl_init_system()
*******************************************************************************/
int sycl_init_system(SYCL_WAN_TYPE_T* wanType)
{
    us_sw_app_t         sysCfg = NONE_SW_APP;
	us_enabled_t        force = US_DISABLED;
    tpm_init_pon_type_t wanTech;
    int32_t             rcode;

    tpmKernelExist(); /* Set TPM driver file description */

    /*Get WAN type*/
	rcode = syscl_get_wan_params_hook(&wanTech, &force);
	if (rcode != US_RC_OK) {
		fprintf(stderr, "wan tech params GET operation failed\n");
        return(US_RC_FAIL);
    }

	switch (wanTech) {
		case TPM_EPON:
			fprintf(stderr, "epon\n");
			sysCfg = EPON_SW_APP;
			*wanType = SYCL_EPON;
			break;

		case TPM_GPON:
			fprintf(stderr, "gpon\n");
			sysCfg = GPON_SW_APP;
			*wanType = SYCL_GPON;
			break;

		case TPM_P2P:
			sysCfg = P2P_SW_APP;
			*wanType = SYCL_P2P;
			break;

		default:
			sysCfg = GBE_SW_APP;
			*wanType = SYCL_GBE;
			break;
	}

	if (sysCfg != GBE_SW_APP) {
		if (force == US_ENABLED)
			staticUponSetup(sysCfg);    /* PON static config */
		else
			sysCfg = dynamicUponSetup(); /* PON dynamic detection */
    }

	if (module_tpmSetup(wanTech) != US_RC_OK) {
		printf("TPM setup failed\n");
		return (US_RC_FAIL);
	}

	if (sysCfg != GBE_SW_APP) {

		if (syscl_pon_driver_init(sysCfg) != US_RC_OK) {
			printf("%cPON driver init failed\n", (sysCfg == GPON_SW_APP) ? 'G' : 'E');
			return (US_RC_FAIL);
		}

		osTaskDelay(100);

		if (syscl_pon_module_setup(sysCfg) != US_RC_OK) {
			printf("%cPON module setup failed\n", (sysCfg == GPON_SW_APP) ? 'G' : 'E');
			return (US_RC_FAIL);
		}

		if (force == US_DISABLED) {
			/* Enable GPON-EPON-P2P switchover */
			if (module_uponOperate() != 0) {
				printf("uponOperate failed\n");
				return (US_RC_FAIL);
			}
		}
	}

    return US_RC_OK;
}

/*******************************************************************************
* sycl_monitor()
*******************************************************************************/
void sycl_monitor(void)
{
    unsigned int i;

    if (MIPC_ERROR == mipc_init("sycl_monitor", 0, 0))
        return;

    osTaskDelay(30000);

    /*Monitor application processes*/
    while (1)
    {
        for (i = 0; i < sizeof(gSyclApplication)/sizeof(SYCL_APP_T); i++)
        {
            if (gSyclApplication[i].wanType & gSyclWanType)
            {
                if (0 != mipc_ping(gSyclApplication[i].name, 100000))
                {
					printf("%s is dead!\n", gSyclApplication[i].name);
                    /* Maybe do a system reboot is better*/
                    //sycl_restart_application(&gSyclApplication[i]);
                }
            }
        }
        osTaskDelay(3000);
    }


}

/*******************************************************************************
* sycl_reboot()
*******************************************************************************/
void sycl_reboot(int delay)
{
    osTaskDelay(delay);
    system("reboot");
}

/*******************************************************************************
* main
*
* DESCRIPTION:      Rest main
*
* INPUTS:            none
*
* OUTPUTS:            none
*
* RETURNS:          none
*
*******************************************************************************/
int main(int argc, char* argv[])
{
    pthread_t    monitor_thread;
    unsigned int i;
    struct rlimit rlim,rlim_new;
    int mipc_fd;


    if (US_RC_OK != sycl_init_system(&gSyclWanType))
        return US_RC_FAIL;

    /*Initialize sycl server*/
    mipc_fd = mipc_init("sycl", 0, 0);
    if (MIPC_ERROR == mipc_fd)
        return US_RC_FAIL;

    printf("\r\n");
    printf("  ****************************** \r\n");
    printf("  *** Start SFU Applications *** \r\n");
    printf("  ****************************** \r\n");
    printf("\r\n");

    if (getrlimit(RLIMIT_MSGQUEUE , &rlim)==0)
    {
        rlim_new.rlim_cur = rlim_new.rlim_max = RLIM_INFINITY;
        if (setrlimit(RLIMIT_MSGQUEUE , &rlim_new)!=0)
        {
            /* failed. try raising just to the old max */
            printf("setrlimit Failed, roll back!\r\n");
            rlim_new.rlim_cur = rlim_new.rlim_max = rlim.rlim_max;
            setrlimit(RLIMIT_MSGQUEUE , &rlim_new);
        }
		else
		{
			printf("setrlimit succeeded!\r\n");
		}
    }

    if (osTaskCreate(&monitor_thread,
                     "monitorThread",
                     (GLFUNCPTR) sycl_monitor,
                     0,
                     0,
                     80,
                     0x2800) != IAMBA_OK)
    {
        printf("%s: osTaskCreate failed for system monitor!\n\r", __FUNCTION__);
        return ERR_TASK_CREATE;
    }

    /*Create application processes one by one*/
    for (i = 0; i < sizeof(gSyclApplication)/sizeof(SYCL_APP_T); i++)
    {
        if (gSyclApplication[i].wanType & gSyclWanType)
        {
            printf("> Starting %-8s               ", gSyclApplication[i].name);
              if (0 == sycl_start_application(&gSyclApplication[i]))
              {
                  printf("[Done].\r\n");
              }
              else
              {
                  printf("[Failed]!\r\n");
              }
          }
    }
    printf("\r\n");
    printf("\r\n");

    //system("ifconfig eth0 1.1.1.1 netmask 255.255.255.0 up");
    system("ifconfig eth0 up");
    system("ifconfig pon0 3.3.3.3 netmask 255.255.255.0 up");
    system("ifconfig eth1 up");

    mthread_register_mq(mipc_fd, sycl_mipc_server_handler);
    mthread_start();

    return(US_RC_OK);
}
