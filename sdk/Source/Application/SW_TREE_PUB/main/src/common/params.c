/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "tpm_types.h"
#include "tpm_internal_types.h"
#include "tpm_mng_if.h"
#include "ezxml.h"
#include "common.h"
#include "params.h"
#include "params_mng.h"
#include "ponOnuMngIf.h"
#include "ponOnuInternals.h"
#include "oam_stack_comm.h"
#include "OsGlueLayer.h"

extern GL_SEMAPHORE_ID ponCfgFileSemId;

bool  db_hexadec_arg (const char    *arg)
{
    bool  rc = false;

    if (arg != NULL)
    {
        if ((arg[1] == 'x') || (arg[1] == 'X'))
            rc = true;
    }

    return rc;
}

/*******************************************************************************
*                          Internal functions
*******************************************************************************/
static int save_xml_file (ezxml_t xmlHead, char *cfg_file)
{
	int     fd;
	char    *fbuf;
	ssize_t fsize;
	int     rval = US_RC_OK;

	osSemTake(ponCfgFileSemId, GL_SUSPEND);

	fd = open(cfg_file, O_TRUNC|O_WRONLY, 0);
	if (fd < 0) {
		perror("Error opening XML file for writing ");
		osSemGive(ponCfgFileSemId);
		return US_RC_FAIL;
	}

	fbuf = ezxml_toxml(xmlHead);
	if (fbuf == NULL)
        return US_RC_FAIL;

	fsize = write(fd, fbuf, strlen(fbuf));
	if (fsize <= 0) {
		perror("Error opening XML file for writing ");
		rval = US_RC_FAIL;
	}

	if (fd >= 0)
		close(fd);

	osSemGive(ponCfgFileSemId);

	if (fbuf != NULL)
		free(fbuf);

	return(rval);
}

static ezxml_t get_xml_head_ptr (char   *cfg_file)
{
    ezxml_t     xmlHead;

    xmlHead = ezxml_parse_file(cfg_file);

    if (xmlHead == NULL)
    {
        printf("Failed to open XML configuration file - %s\n", cfg_file);
    }

    return xmlHead;
}

static uint32_t get_dig_number (const char  *arg)
{
    uint32_t    val = 0;

    if ((arg[1] == 'x') || (arg[1] == 'X'))
        sscanf(&arg[2], "%x", &val);
    else
        val = atoi(arg);

    return val;
}

static int get_int_param (ezxml_t   xml,
                          char      *name,
                          uint32_t  *num)
{
    ezxml_t     xmlElement;

    xmlElement = ezxml_child(xml, name);
    if (xmlElement == NULL)
    {
        return US_RC_NOT_FOUND;
    }
    else if (xmlElement->txt == NULL)
    {
        printf("NULL value for %s\n", name);
        return US_RC_NOT_FOUND;
    }

    *num = get_dig_number(xmlElement->txt);
    return US_RC_OK;
}

static int set_int_param (ezxml_t xml, char *name, uint32_t num)
{
	ezxml_t xmlElement;
	char    *num_txt;

	xmlElement = ezxml_child(xml, name);
	if (xmlElement == NULL)
		return US_RC_NOT_FOUND;

	num_txt = calloc(12, sizeof(char));
	if (num_txt == 0) {
		printf("Error allocating memory\n");
		return US_RC_FAIL;
	}

	snprintf(num_txt, 11, "%d", num);
	if (ezxml_set_txt(xmlElement, num_txt) == NULL) {
		printf("Failed in XML text set\n");
		return US_RC_FAIL;
	}

	return US_RC_OK;
}


static int get_char_param (ezxml_t   xml,
                           char      *name,
                           uint8_t   *str,
                           uint8_t   len)
{
    ezxml_t     xmlElement;

    xmlElement = ezxml_child(xml, name);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s\n", name);
        return US_RC_NOT_FOUND;
    }
    else if (xmlElement->txt == NULL)
    {
        printf("NULL value for %s \n", name);
        return US_RC_NOT_FOUND;
    }

    memcpy(str, xmlElement->txt, len);
    str[len] = 0;
    return US_RC_OK;
}

static int get_debug_port_attrs (ezxml_t    xml,
                                 uint32_t   *num,
                                 uint32_t   *valid)
{
    const char  *attr_str;
    int         rc = US_RC_FAIL;

    if ((attr_str = ezxml_attr(xml, US_XML_ID_ATTR)) != NULL)
    {
        *num = get_dig_number(attr_str);

        if ((attr_str = ezxml_attr(xml, US_XML_VALID_ATTR)) != NULL)
        {
            *valid = get_dig_number(attr_str);
            rc = US_RC_OK;
        }
        else
        {
            printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_VALID_ATTR);
        }
    }
    else
    {
        printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_ID_ATTR);
    }

    return rc;
}

static int get_rx_queue_attrs(ezxml_t            xml,
                              uint32_t           gmac,
                              tpm_init_gmac_rx_t *gmac_rx,
                              int                max_rx_queues_num)
{
    const char  *attr_str;
    int         que_id;

    attr_str = ezxml_attr(xml, US_XML_ID_ATTR);
    if (attr_str == NULL)
    {
        printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_ID_ATTR);
        return(US_RC_FAIL);
    }
    que_id = get_dig_number(attr_str);

    if (que_id >= max_rx_queues_num)
    {
        printf("%s: Invalid ID number %s\n", __FUNCTION__, attr_str);
        return(US_RC_FAIL);
    }

    attr_str = ezxml_attr(xml, US_XML_SIZE_ATTR);
    if (attr_str == NULL)
    {
        printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_SIZE_ATTR);
        return(US_RC_FAIL);
    }

    gmac_rx[gmac].rx_queue[que_id].queue_size  = get_dig_number(attr_str);
    gmac_rx[gmac].valid = 1;
    gmac_rx[gmac].rx_queue[que_id].valid = 1;

    return US_RC_OK;
}





static int get_tx_queue_attrs(ezxml_t            xml,
                           uint32_t           port,
                           tpm_init_gmac_tx_t *gmac_tx,
                           int                max_tx_queues_num)
{
    const char  *attr_str;
    int         que_id;
    int         rc = US_RC_FAIL;

    if ((attr_str = ezxml_attr(xml, US_XML_ID_ATTR)) != NULL)
    {
        que_id = get_dig_number(attr_str);

        if (que_id < max_tx_queues_num)
        {
            if ((attr_str = ezxml_attr(xml, US_XML_QUEUE_SHED_ATTR)) != NULL)
            {
                gmac_tx[port].tx_queue[que_id].sched_method = get_dig_number(attr_str);

                if ((attr_str = ezxml_attr(xml, US_XML_QUEUE_OWNER_ATTR)) != NULL)
                {
                    gmac_tx[port].tx_queue[que_id].queue_owner = get_dig_number(attr_str);

                    if ((attr_str = ezxml_attr(xml, US_XML_QUEUE_OW_Q_NUM_ATTR)) != NULL)
                    {
                        gmac_tx[port].tx_queue[que_id].owner_queue_num = get_dig_number(attr_str);

                        if ((attr_str = ezxml_attr(xml, US_XML_SIZE_ATTR)) != NULL)
                        {
                            gmac_tx[port].tx_queue[que_id].queue_size = get_dig_number(attr_str);

                            if ((attr_str = ezxml_attr(xml, US_XML_QUEUE_WEIGHT_ATTR)) != NULL)
                            {
                                gmac_tx[port].tx_queue[que_id].queue_weight = get_dig_number(attr_str);
                                gmac_tx[port].valid = 1;
                                gmac_tx[port].tx_queue[que_id].valid = 1;
                                rc = US_RC_OK;
                            }
                            else
                            {
                                printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_QUEUE_WEIGHT_ATTR);
                            }
                        }
                        else
                        {
                            printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_SIZE_ATTR);
                        }
                    }
                    else
                    {
                        printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_QUEUE_OW_Q_NUM_ATTR);
                    }
                }
                else
                {
                    printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_QUEUE_OWNER_ATTR);
                }
            }
            else
            {
                printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_QUEUE_SHED_ATTR);
            }
        }
        else
        {
            printf("%s: Invalid ID number %s\n", __FUNCTION__, attr_str);
        }
    }
    else
    {
        printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_ID_ATTR);
    }

    return rc;
}

static int get_pnc_range_attrs (ezxml_t               xml,
                                tpm_init_pnc_range_t  *pnc_range,
                                int                   max_pnc_ranges)
{
    const char  *attr_str;
    int         range_id;
    int         rc = US_RC_FAIL;

    if ((attr_str = ezxml_attr(xml, US_XML_ID_ATTR)) != NULL)
    {
        range_id = get_dig_number(attr_str);

        if (range_id < max_pnc_ranges)
        {
            if ((attr_str = ezxml_attr(xml, US_XML_NUM_ATTR)) != NULL)
            {
                pnc_range[range_id].range_num = get_dig_number(attr_str);

                if ((attr_str = ezxml_attr(xml, US_XML_TYPE_ATTR)) != NULL)
                {
                    pnc_range[range_id].range_type = get_dig_number(attr_str);

                    if ((attr_str = ezxml_attr(xml, US_XML_SIZE_ATTR)) != NULL)
                    {
                        pnc_range[range_id].range_size = get_dig_number(attr_str);

                        if ((attr_str = ezxml_attr(xml, US_XML_TPM_MIN_RES_LVL_ATTR)) != NULL)
                        {
                            pnc_range[range_id].min_reset_level = get_dig_number(attr_str);

                        }
                        else
                        {
                            printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_TPM_MIN_RES_LVL_ATTR);
                        }
                    }
                    else
                    {
                        printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_SIZE_ATTR);
                    }
                }
                else
                {
                    printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_TYPE_ATTR);
                }
            }
            else
            {
                printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_NUM_ATTR);
            }
        }
        else
        {
            printf("%s: Invalid ID number %s\n", __FUNCTION__, attr_str);
        }
    }
    else
    {
        printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_ID_ATTR);
    }

    return rc;
}

static int get_vlan_ety_attrs (ezxml_t      xml,
                               uint32_t     *etypes,
                               int          max_etypes)
{
    const char  *attr_str;
    int         reg_id;
    int         rc = US_RC_FAIL;

    if ((attr_str = ezxml_attr(xml, US_XML_ID_ATTR)) != NULL)
    {
        reg_id = get_dig_number(attr_str);

        if (reg_id < max_etypes)
        {
            if ((attr_str = ezxml_attr(xml, US_XML_TYPE_ATTR)) != NULL)
            {
                etypes[reg_id] = get_dig_number(attr_str);
                rc = US_RC_OK;
            }
            else
            {
                printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_TYPE_ATTR);
            }
        }
        else
        {
            printf("%s: Invalid ID number %s\n", __FUNCTION__, attr_str);
        }
    }
    else
    {
        printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_ID_ATTR);
    }

    return rc;
}

static int get_filter_tpid_attrs(ezxml_t xml, tpm_init_tpid_comb_t *tpid)
{
	const char *attr_str;
	const char *v1_tpid = NULL;
	const char *v2_tpid = NULL;
	int rc = US_RC_FAIL;

	attr_str = ezxml_attr(xml, US_XML_TYPE_ATTR);
	if (attr_str != NULL) {
		v1_tpid = attr_str;
		v2_tpid = strstr(attr_str, ",");

		tpid->v1_tpid = get_dig_number(v1_tpid);
		if (v2_tpid != NULL) {
			v2_tpid++;
			tpid->v2_tpid = get_dig_number(v2_tpid);
		} else
			tpid->v2_tpid = MV_TPM_UN_INITIALIZED_INIT_PARAM;
		rc = US_RC_OK;
	} else
		printf("%s: Failed to get %s\n", __func__, US_XML_TYPE_ATTR);

	return rc;
}

static int get_oam_txq_attrs  (ezxml_t      xml,
                               uint32_t     *txq)
{
    const char  *attr_str;
    int         id_num;
    int         rc = US_RC_FAIL;
    uint32_t    tmp_val, tmp_val1;

    if ((attr_str = ezxml_attr(xml, US_XML_ID_ATTR)) != NULL)
    {
        id_num = get_dig_number(attr_str);

        if (id_num < EPON_API_MAX_NUM_OF_MAC)
        {
            if ((attr_str = ezxml_attr(xml, US_XML_NUM_ATTR)) != NULL)
            {
                tmp_val = get_dig_number(attr_str);
                //printf("tmp_val = 0x%8x\n", tmp_val);
                tmp_val1 = ((tmp_val & MRVL_EOAM_QUEUE_MASK) << (id_num*4));
                //printf("tmp_val1 = 0x%8x\n", tmp_val1);
                *txq = *txq | tmp_val1;
                rc = US_RC_OK;
            }
            else
            {
                printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_NUM_ATTR);
            }
        }
        else
        {
            printf("%s: Invalid ID number %s\n", __FUNCTION__, attr_str);
        }
    }
    else
    {
        printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_ID_ATTR);
    }

    return rc;
}


/*******************************************************************************
* get_wan_tech_param()
*
* DESCRIPTION:      Get WAN type from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  wanTech - EPON, GPON, ETH or P2P
*           force   - WAN tech. is set permanently or may be changed using
*                     dynamic detection feature
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_wan_tech_param (tpm_init_pon_type_t    *wanTech,
                            us_enabled_t           *force)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;
    uint32_t    val, val1;

	if ((wanTech == NULL) || (force == NULL)) {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *wanTech = TPM_NONE;
    *force   = US_DISABLED;

	xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_WAN);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_WAN_E);
	if (xmlElement == NULL) {
		printf("Failed to find %s in XML config. file %s\n", US_XML_WAN_E, US_XML_CFG_FILE_WAN);
        rc = US_RC_NOT_FOUND;
		goto get_wan_tech_param_exit;
    }

        rc = get_int_param (xmlElement, US_XML_WAN_TYPE_E, &val);
        rc |= get_int_param (xmlElement, US_XML_WAN_TYPE_FORCE_E, &val1);
        
	if (rc == US_RC_OK) {
        *wanTech = (tpm_init_pon_type_t)val;
        *force   = (us_enabled_t)val1;

#ifdef US_DEBUG_PRINT
        printf("wanTech = %d force = %d \n", *wanTech, *force);
#endif
    }

get_wan_tech_param_exit:
    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* set_wan_tech_param()
*
* DESCRIPTION:      Set WAN type and save XML configuration file
*
* INPUTS:
*
* OUTPUTS:  wanTech - EPON, GPON, ETH or P2P
*           force   - WAN tech. is set permanently or may be changed using
*                     dynamic detection feature
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int set_wan_tech_param (tpm_init_pon_type_t    wanTech,
						us_enabled_t           force)
{
	ezxml_t     xmlHead;
	ezxml_t     xmlElement;
	int         rc;

	xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_WAN);

	if (xmlHead == NULL)
		return US_RC_FAIL;

	xmlElement = ezxml_child(xmlHead, US_XML_WAN_E);
	if (xmlElement == NULL) {
		printf("Failed to find %s in XML config. file %s\n", US_XML_WAN_E, US_XML_CFG_FILE_WAN);
		rc = US_RC_NOT_FOUND;
		goto set_wan_tech_param_exit;
	}

	rc = set_int_param (xmlElement, US_XML_WAN_TYPE_E, (uint32_t)wanTech);
	rc |= set_int_param (xmlElement, US_XML_WAN_TYPE_FORCE_E, (uint32_t)force);

	if (rc != US_RC_OK) {
		printf("Failed to set XML parameter\n");
		goto set_wan_tech_param_exit;
	}

	rc = save_xml_file(xmlHead, US_XML_CFG_FILE_WAN);

set_wan_tech_param_exit:
    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_omci_etype_param()
*
* DESCRIPTION:      Get OMCI ETY from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  ety - Ethernet type
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_omci_etype_param (uint32_t    *ety)
{
    int         rc;
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;

    if (ety == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *ety = 0;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_OMCI);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_OMCI_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_OMCI_E, US_XML_CFG_FILE_OMCI);
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_OMCI_ETY_E, ety);

    if (rc == US_RC_OK)
    {
#ifdef US_DEBUG_PRINT
        printf("OMCI ETY = 0x%4.4X\n", *ety);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_debug_port_params()
*
* DESCRIPTION:      Get debug port from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  valid - whether the debug port is valid
*           num   - debug port number
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_debug_port_params (uint32_t    *valid, uint32_t    *num)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc = US_RC_OK;

    if ((valid == NULL) ||
        (num   == NULL))
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *valid  = 0;
    *num    = 0;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, US_XML_DEBUG_PORT_E, -1);
    if (xmlElement == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("Failed to find %s in XML config. file %s\n", US_XML_DEBUG_PORT_E, US_XML_CFG_FILE_TPM);
#endif
        rc = US_RC_NOT_FOUND;
    }
    else
    {
        get_debug_port_attrs (xmlElement, num, valid);

#ifdef US_DEBUG_PRINT
        printf("DEBUG port = %d valid = %d\n", *num, *valid);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_igmp_snoop_params()
*
* DESCRIPTION:      Get IGMP snooping parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  enable - whether the IGMP snooping is enabled
*           que    - queue number
*           all    - whether all packets should be verified
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_igmp_snoop_params (uint32_t    *enable,
                               uint32_t    *que)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc = US_RC_OK;

    if ((enable == NULL) ||
        (que    == NULL))
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *enable  = 0;
    *que     = 0;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_IGMP_SNOOP_E);
    if (xmlElement == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("Failed to find %s in XML config. file %s\n", US_XML_IGMP_SNOOP_E, US_XML_CFG_FILE_TPM);
#endif
        rc = US_RC_NOT_FOUND;
    }
    else
    {
        rc = get_int_param (xmlElement, US_XML_ENABLED_E, enable);
        rc |= get_int_param (xmlElement, US_XML_CPU_Q_E, que);

#ifdef US_DEBUG_PRINT
        printf("IGMP snooping: enable %d que %d\n", *enable, *que);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

int     get_igmp_pkt_frwd_mode (uint32_t *igmpPktFrwdMode)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc = US_RC_OK;
    int         IgmpPktFrwdModeTmp;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL) 
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_IGMP_SNOOP_E);
    if (xmlElement == NULL) 
    {
#ifdef US_DEBUG_PRINT
        printf("Failed to find %s in XML config. file %s\n", US_XML_IGMP_SNOOP_E, US_XML_CFG_FILE_TPM);
#endif
        rc = US_RC_NOT_FOUND;
    }
    else 
    {
        rc |= get_int_param (xmlElement, IGMP_PKT_FRWD_MODE_UNI_0, &IgmpPktFrwdModeTmp);
        //printf("get_int_param returns %d\n", rc);
        igmpPktFrwdMode[0] = IgmpPktFrwdModeTmp;

        rc |= get_int_param (xmlElement, IGMP_PKT_FRWD_MODE_UNI_1, &IgmpPktFrwdModeTmp);
        //printf("get_int_param returns %d\n", rc);
        igmpPktFrwdMode[1] = IgmpPktFrwdModeTmp;
        
        rc |= get_int_param (xmlElement, IGMP_PKT_FRWD_MODE_UNI_2, &IgmpPktFrwdModeTmp);
        //printf("get_int_param returns %d\n", rc);
        igmpPktFrwdMode[2] = IgmpPktFrwdModeTmp;
        rc |= get_int_param (xmlElement, IGMP_PKT_FRWD_MODE_UNI_3, &IgmpPktFrwdModeTmp);
        //printf("get_int_param returns %d\n", rc);
        igmpPktFrwdMode[3] = IgmpPktFrwdModeTmp;

        //printf("%d, %d, %d, %d\n",igmpPktFrwdMode[0],igmpPktFrwdMode[1],igmpPktFrwdMode[2],igmpPktFrwdMode[3]);
    }

    ezxml_free(xmlHead); 
    return rc;
}


/*******************************************************************************
* get_gmac_mh_en_params()
*
* DESCRIPTION:      Get GMAC Marvell Header enable parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  gmac0_mh_en  - GMAC0 Marvell Header Enable configuration
*           gmac1_mh_en  - GMAC0 Marvell Header Enable configuration
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_gmac_mh_en_params   (uint32_t               *gmac0_mh_en,
                                 uint32_t               *gmac1_mh_en)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc = US_RC_OK;

    if ((gmac0_mh_en == NULL) ||
        (gmac1_mh_en == NULL))
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *gmac0_mh_en     = 0;
    *gmac1_mh_en     = 0;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, US_XML_GMAC_CONFIG_E, 0, US_XML_GMAC_MH_ENA_E, -1);
    if (xmlElement == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("Failed to find %s in XML config. file %s\n", US_XML_GMAC_CONN_E, US_XML_CFG_FILE_TPM);
#endif
        rc = US_RC_NOT_FOUND;
    }
    else
    {
        rc =  get_int_param (xmlElement, US_XML_GMAC_0_MH_ENA_E, gmac0_mh_en);
        rc |= get_int_param (xmlElement, US_XML_GMAC_1_MH_ENA_E, gmac1_mh_en);
#ifdef US_DEBUG_PRINT
        printf("======================================\n");
        printf("        GMAC Marvell Header Enable    \n");
        printf("======================================\n");
        printf("gmac0_mh_en %d \n", *gmac0_mh_en);
        printf("gmac1_mh_en %d \n", *gmac1_mh_en);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_gmac_conn_params()
*
* DESCRIPTION:      Get GMAC connection parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  num_tcont_llid  - TCONT/LLID number
*           gmac0_con       - GMAC0 connection type
*           gmac1_con       - GMAC1 connection type
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_gmac_conn_params (uint32_t              *num_tcont_llid)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc = US_RC_OK;

    if  (num_tcont_llid == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *num_tcont_llid = 0;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, US_XML_GMAC_CONFIG_E, 0, US_XML_GMAC_CONN_E, -1);
    if (xmlElement == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("Failed to find %s in XML config. file %s\n", US_XML_GMAC_CONN_E, US_XML_CFG_FILE_TPM);
#endif
        rc = US_RC_NOT_FOUND;
    }
    else
    {
        rc = get_int_param (xmlElement, US_XML_TCONT_LLID_E, num_tcont_llid);
#ifdef US_DEBUG_PRINT
        printf("======================================\n");
        printf("        GMAC connection               \n");
        printf("======================================\n");
        printf("num_tcont_llid %d \n", *num_tcont_llid);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}
/*******************************************************************************
* get_gmac_pool_bufs_params()
*
* DESCRIPTION:      Get GMAC Buffer Pool parameters from XML configuration file
*
* INPUTS:   max_gmacs_num    - MAX GMACs in the system
*
* OUTPUTS:  gmac_bufs       - pointer to the gmac_buffer pool data
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_gmac_pool_bufs_params      (tpm_init_gmac_bufs_t *gmac_bufs,
                                        int                max_gmacs_num)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    ezxml_t     xmlGmac;
    const char  *xmlStr;
    int         rc = US_RC_OK;
    uint32_t    gmac;
    uint32_t    large_pkt_pool_bufs;
    uint32_t    small_pkt_pool_bufs;

    if (gmac_bufs == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    if (max_gmacs_num == 0)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: Invalid max_gmacs_num \n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    memset(gmac_bufs, 0, sizeof(tpm_init_gmac_bufs_t)*max_gmacs_num);

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, US_XML_GMAC_CONFIG_E, 0, US_XML_GMAC_BM_BUF_E, -1);
    if (xmlElement == NULL)
    {
        rc = US_RC_FAIL;
    }
    else
    {
        xmlGmac = ezxml_child(xmlElement, US_XML_GMAC_E);
        if (xmlGmac)
        {
            for (;xmlGmac;xmlGmac = xmlGmac->next)
            {
                xmlStr = ezxml_attr(xmlGmac, US_XML_ID_ATTR);
                if (xmlStr == NULL)
                {
                    printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_ID_ATTR);
                    rc = US_RC_FAIL;
                    break;
                }
                gmac = get_dig_number(xmlStr);
                if ((int)gmac > (max_gmacs_num-1))
                {
                    printf("%s: Invalid GMAC id number - %s\n", __FUNCTION__, xmlStr);
                    rc = US_RC_FAIL;
                    break;
                }

                xmlStr = ezxml_attr(xmlGmac, US_XML_LARGE_POOL_BUF_ATTR);
                if (xmlStr == NULL)
                {
                    printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_LARGE_POOL_BUF_ATTR);
                    rc = US_RC_FAIL;
                    break;
                }
                large_pkt_pool_bufs = get_dig_number(xmlStr);

                xmlStr = ezxml_attr(xmlGmac, US_XML_SHORT_POOL_BUF_ATTR);
                if (xmlStr == NULL)
                {
                    printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_SHORT_POOL_BUF_ATTR);
                    rc = US_RC_FAIL;
                    break;
                }
                small_pkt_pool_bufs = get_dig_number(xmlStr);

                gmac_bufs[gmac].valid = 1;
                gmac_bufs[gmac].large_pkt_buffers = large_pkt_pool_bufs;
                gmac_bufs[gmac].small_pkt_buffers = small_pkt_pool_bufs;
            }
        }

#ifdef US_DEBUG_PRINT
        printf("======================================\n");
        printf("               GMAC BUFS              \n");
        printf("======================================\n");
        for (gmac = 0; gmac < max_gmacs_num; gmac++)
        {
            if (gmac_bufs[gmac].valid)
            {
                printf("gmac %d  large_bufs %d  small_bufs %d\n",
                       gmac, gmac_bufs[gmac].large_pkt_buffers, gmac_bufs[gmac].small_pkt_buffers);
            }
        }
        printf("======================================\n");
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}



/*******************************************************************************
* get_gmac_rxq_params()
*
* DESCRIPTION:
*
* INPUTS:   max_gmacs_num    - MAX GMACs in the system
*           max_rx_queues_num - MAX possible rx_queues per GMAC
*
* OUTPUTS:  gmac_rx             - pointer to the gmac rx_queues data
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_gmac_rxq_params    (tpm_init_gmac_rx_t *gmac_rx,
                                int                 max_gmacs_num,
                                int                 max_rx_queues_num)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    const char  *xmlStr;
    ezxml_t     xmlGmac, xmlQue;
    int         rc = US_RC_OK;
    uint32_t    gmac;
    uint32_t    stop_parsing=0;
#ifdef US_DEBUG_PRINT
    uint32_t    rxq;
#endif

    if (gmac_rx == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    if (max_gmacs_num == 0)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: Invalid max_gmacs_num \n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }
    if (max_rx_queues_num == 0)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: Invalid max_tx_queues_num \n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    memset(gmac_rx, 0, sizeof(tpm_init_gmac_rx_t)*max_gmacs_num);

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
    {
        return US_RC_FAIL;
    }

    xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, US_XML_GMAC_CONFIG_E, 0, US_XML_GMAC_RXQUEUES_E, -1);
    if (xmlElement == NULL)
    {
        rc = US_RC_FAIL;
    }
    else
    {
        xmlGmac = ezxml_child(xmlElement, US_XML_GMAC_E);
        if (xmlGmac)
        {
            for (;(xmlGmac && !stop_parsing);xmlGmac = xmlGmac->next)
            {
                xmlStr = ezxml_attr(xmlGmac, US_XML_ID_ATTR);
                if (xmlStr == NULL)
                {
                    printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_ID_ATTR);
                    rc = US_RC_FAIL;
                    break;
                }
                gmac = get_dig_number(xmlStr);
                if ((int)gmac > (max_gmacs_num-1))
                {
                    printf("%s: Invalid GMAC id number - %s\n", __FUNCTION__, xmlStr);
                    rc = US_RC_FAIL;
                    break;
                }

                /* Get GMAC rx queue sizes */
                for (xmlQue = ezxml_child(xmlGmac, US_XML_QUEUE_E); xmlQue; xmlQue = xmlQue->next)
                {
                    rc = get_rx_queue_attrs(xmlQue, gmac, gmac_rx, max_rx_queues_num);

                    if (rc != US_RC_OK)
                    {
                        stop_parsing = 1;
                        break;
                    }
                }

            }
        }

#ifdef US_DEBUG_PRINT
        printf("======================================\n");
        printf("               GMAC RXQs              \n");
        printf("======================================\n");
        for (gmac = 0; gmac < max_gmacs_num; gmac++)
        {
            if (gmac_rx[gmac].valid)
            {
                for (rxq = 0; rxq < max_rx_queues_num; rxq++)
                {
                    if (gmac_rx[gmac].rx_queue[rxq].valid)
                    {
                        printf("gmac %d rxq %d size %d\n",
                               gmac, rxq, gmac_rx[gmac].rx_queue[rxq].queue_size);
                    }
                }
            }
        }
        printf("======================================\n");
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}


/*******************************************************************************
* get_gmac_tx_params()
*
* DESCRIPTION:      Get GMAC TX parameters from XML configuration file
*
* INPUTS:   max_tx_ports_num    - MAX TX ports in the system
*           max_tx_queues_num   - MAX TX queues per port
*
* OUTPUTS:  gmac_tx             - pointer to the TX port data
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_gmac_tx_params      (tpm_init_gmac_tx_t *gmac_tx,
                                 int                max_tx_ports_num,
                                 int                max_tx_queues_num)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    ezxml_t     xmlGmac;
    ezxml_t     xmlQueMap;
    ezxml_t     xmlQue;
    int         rc = US_RC_OK;
    uint32_t    port;
    const char  *xmlStr;
    uint8_t     stop_parsing = 0;

    if (gmac_tx == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    if ((max_tx_ports_num == 0) || (max_tx_queues_num == 0))
    {
#ifdef US_DEBUG_PRINT
        printf("%s: Invalid max_int_ports_num \n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    memset(gmac_tx, 0, sizeof(tpm_init_gmac_tx_t)*max_tx_ports_num);

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, US_XML_TX_MOD_PARAMS_E, -1);
    if (xmlElement == NULL)
    {
        rc = US_RC_FAIL;
    }
    else
    {
        for (xmlGmac = ezxml_child(xmlElement, US_XML_TX_MOD_E);
             xmlGmac && !stop_parsing;
             xmlGmac = xmlGmac->next)
        {
            xmlStr = ezxml_attr(xmlGmac, US_XML_ID_ATTR);
            if (xmlStr == NULL)
            {
                printf("%s: Failed to get %s\n", __FUNCTION__, US_XML_ID_ATTR);
                rc = US_RC_FAIL;
                break;
            }

            port = get_dig_number(xmlStr);
            if (port > (uint32_t)max_tx_ports_num)
            {
                printf("%s: Invalid GMAC id number - %s\n", __FUNCTION__, xmlStr);
                rc = US_RC_FAIL;
                break;
            }

            xmlQueMap = ezxml_child(xmlGmac, US_XML_QUEUE_MAP_E);
            for (xmlQue = ezxml_child(xmlQueMap, US_XML_QUEUE_E); xmlQue; xmlQue = xmlQue->next)
            {
                rc = get_tx_queue_attrs(xmlQue, port, gmac_tx, max_tx_queues_num);

                if (rc != US_RC_OK)
                {
                    stop_parsing = 1;
                    break;
                }
            }
        }
#ifdef US_DEBUG_PRINT
    {
        int     que;

        printf("======================================\n");
        printf("               GMAC TX                \n");
        printf("======================================\n");
        for (port = 0; port < max_tx_ports_num; port++)
        {
            if (gmac_tx[port].valid)
            {
                printf("TX port %d  \n", port);
                printf("------------\n");

                for (que=0; que < max_tx_queues_num; que++)
                {
                    if (gmac_tx[port].tx_queue[que].valid)
                    {
                        printf("TX queue %d: owner %d owner num %d size %d weight %d sched %d\n",
                               que,
                               gmac_tx[port].tx_queue[que].queue_owner,
                               gmac_tx[port].tx_queue[que].owner_queue_num,
                               gmac_tx[port].tx_queue[que].queue_size,
                               gmac_tx[port].tx_queue[que].queue_weight,
                               gmac_tx[port].tx_queue[que].sched_method);
                    }
                }
            }
        }
        printf("======================================\n");
    }
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_pnc_range_params()
*
* DESCRIPTION:      Get PnC ranges parameters from XML configuration file
*
* INPUTS:   max_pnc_ranges_num  - MAX PnC ranges in the system
*
* OUTPUTS:  pnc_range           - pointer to the internal PnC range table
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_pnc_range_params    (tpm_init_pnc_range_t   *pnc_range,
                                 int                    max_pnc_ranges_num)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    ezxml_t     xmlRange;
    int         rc = US_RC_OK;

    if (pnc_range == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    if (max_pnc_ranges_num == 0)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: Invalid max_pnc_ranges_num \n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    memset(pnc_range, 0, sizeof(tpm_init_pnc_range_t)*max_pnc_ranges_num);

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0, US_XML_TPM_PNC_E, 0, US_XML_TPM_PNC_RANGE_PARAMS_E, -1);
    if (xmlElement == NULL)
    {
        rc = US_RC_FAIL;
    }
    else
    {
        for (xmlRange = ezxml_child(xmlElement, US_XML_TPM_PNC_RANGE_E); xmlRange; xmlRange = xmlRange->next)
        {
            rc = get_pnc_range_attrs (xmlRange, pnc_range, max_pnc_ranges_num);

            if (rc != US_RC_OK)
                break;
        }

#ifdef US_DEBUG_PRINT
    {
        int range;

        printf("======================================\n");
        printf("              PnC range               \n");
        printf("======================================\n");
        for (range = 0; range < max_pnc_ranges_num; range++)
        {
            if (pnc_range[range].valid)
                printf("%d: num %d type %d size %d base_lu %d last_lu %d dir_bm 0x%x min_res_level %d res_win_size %d\n",
                       range,
                       pnc_range[range].range_num,
                       pnc_range[range].range_type,
                       pnc_range[range].range_size,
                       pnc_range[range].base_lu_id,
                       pnc_range[range].last_lu_range,
                       pnc_range[range].dir_bm,
                       pnc_range[range].min_reset_level,
                       pnc_range[range].start_res_win);
        }
        printf("======================================\n");
    }
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_ds_mh_config_params()
*
* DESCRIPTION:      Get downstream MH parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  ds_mh_set - from PnC RI or Rx ctrl reg
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_ds_mh_config_params(tpm_init_mh_src_t *dsMhConf)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc = US_RC_OK;
    uint32_t    val;

    if (dsMhConf == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *dsMhConf = TPM_MH_SRC_RX_CTRL;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM);
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_DS_MH_SET_E, &val);

    if (rc == US_RC_OK)
    {
        *dsMhConf = (tpm_init_mh_src_t)val;

#ifdef US_DEBUG_PRINT
        printf("dsMhConf = %d\n", *dsMhConf);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_validation_enabled_config_params()
*
* DESCRIPTION:      Get validation enable parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  validation_enabled - enable or not TPM validation
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_validation_enabled_config_params(tpm_init_tpm_validation_t *validEn)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;
    uint32_t    val;

    if (validEn == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *validEn = TPM_VALID_DISABLED;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM);
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_VALID_EN_E, &val);

    if (rc == US_RC_OK)
    {
        *validEn = (tpm_init_tpm_validation_t)val;

#ifdef US_DEBUG_PRINT
        printf("validEn = %d\n", *validEn);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_epon_connect_tpm_params()
*
* DESCRIPTION:      Get EPON connect TPM APIS parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  connected - disconnect or connected.
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_epon_connect_tpm_params(OAM_PON_CONNECT_TYPE_E* connected)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;
    uint32_t    val;

    if (connected == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *connected = OAM_PON_CONNECT;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_PON);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_EPON_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_PON);
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_EPON_CONNECT_E, &val);

    if (rc == US_RC_OK)
    {
        *connected = (OAM_PON_CONNECT_TYPE_E)val;

#ifdef US_DEBUG_PRINT
        printf("connected = %d\n", *connected);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_epon_silence_mode_params()
*
* DESCRIPTION:      Get EPON silence mode parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  connected - disconnect or connected.
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_epon_silence_mode_params(OAM_SILENCE_MODE_TYPE_E* silence_enabled)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;
    uint32_t    val;

    if (silence_enabled == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *silence_enabled = OAM_SILENCE_DISABLE;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_PON);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_EPON_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_PON);
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_EPON_SILENCE_E, &val);

    if (rc == US_RC_OK)
    {
        *silence_enabled = (OAM_SILENCE_MODE_TYPE_E)val;

#ifdef US_DEBUG_PRINT
        printf("silence mode  = %d\n", *silence_enabled);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}


/*******************************************************************************
* get_epon_traffic_queue_from_xml()
*
* DESCRIPTION: Get EPON traffic target queue from XML configuration file
*
* INPUTS:   
* 
* OUTPUTS:  Target queue number.
*
* RETURNS:  
*
*******************************************************************************/
int get_epon_traffic_queue_from_xml(unsigned char *us_queue, unsigned char *ds_queue)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc=US_RC_FAIL;
    uint32_t     val=0;

    if ((us_queue == NULL) || (ds_queue == NULL))
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_PON);

    if (xmlHead == NULL) 
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_EPON_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_PON);
        rc = US_RC_NOT_FOUND;
        return rc;
    }
  
    rc = get_int_param (xmlElement, US_XML_EPON_US_QUEUE_E, &val);
    if (rc == US_RC_OK)
    {
        *us_queue = val;

#ifdef US_DEBUG_PRINT
        printf("EPON US traffic target queue = %d\n", *us_queue);
#endif
    }

    rc = get_int_param (xmlElement, US_XML_EPON_DS_QUEUE_E, &val);
    if (rc == US_RC_OK)
    {
        *ds_queue = val;

#ifdef US_DEBUG_PRINT
        printf("EPON DS traffic target queue = %d\n", *ds_queue);
#endif
    }

    ezxml_free(xmlHead); 
    return rc;
}


/*******************************************************************************
* get_epon_oam_rx_q_params()
*
* DESCRIPTION:      Get EPON CPU OAM RX Q parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  OAM CPU RX Queue value
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_epon_oam_rx_q_params(uint32_t  *oam_cpu_rx_queue)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;
    uint32_t    val;

    if (oam_cpu_rx_queue == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    /* default queue is 7 - highest queue */
    *oam_cpu_rx_queue = 7;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_PON);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_EPON_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_PON);
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_EPON_OAM_RXQ_E, &val);

    if (rc == US_RC_OK)
    {
        *oam_cpu_rx_queue = val;

#ifdef US_DEBUG_PRINT
        printf("oam_cpu_rx_queue = %d\n", *oam_cpu_rx_queue);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_epon_oam_txq_params()
*
* DESCRIPTION:      Get EPON OAM TX Q parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  OAM TX Queues per LLID
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_epon_oam_txq_params(uint32_t  *oam_tx_queues)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    ezxml_t     xmlLlid;
    int         rc;
    uint32_t    val;

    if (oam_tx_queues == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    /* default queue is 6 */
    *oam_tx_queues = 6;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_PON);

    if (xmlHead == NULL)
        return US_RC_FAIL;


    xmlElement = ezxml_get(xmlHead, US_XML_EPON_E, 0, US_XML_EPON_OAM_TXQ_E, -1);
    if (xmlElement == NULL)
    {
        rc = US_RC_NOT_FOUND;
    }
    else
    {
        for (xmlLlid = ezxml_child(xmlElement, US_XML_EPON_LLID_E); xmlLlid; xmlLlid = xmlLlid->next)
        {
            rc = get_oam_txq_attrs (xmlLlid, oam_tx_queues);

            if (rc != US_RC_OK)
                break;
        }

#ifdef US_DEBUG_PRINT
        printf("============================\n");
        printf("OAM TXQ = 0x%x   \n", *oam_tx_queues);
        printf("============================\n");
#endif

    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_sw_gwy_init_params()
*
* DESCRIPTION:      Get sw gwy init from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  sw_gwy_init- run the TPM API's for default sw_gwy forwarding to CPU
*           (relevant for HGU only)
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_sw_gwy_init_params (uint32_t  *sw_init)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;
    uint32_t    val;

    if (sw_init == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *sw_init = 0;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM);
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_SW_GWY_INIT_E, &val);

    if (rc == US_RC_OK)
    {
        *sw_init =  val;

#ifdef US_DEBUG_PRINT
        printf("sw_init = %d\n", *sw_init);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}



/*******************************************************************************
* get_cfg_pnc_parse_param()
*
* DESCRIPTION:      Get mode of how to configure the PNC PARSE in LSP
*
* INPUTS:
*
* OUTPUTS:  cfgPncParse - enabled/disabled
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_cfg_pnc_parse_param (tpm_init_cfg_pnc_parse_t    *cfgPncParse)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;
    uint32_t    val;

    if (cfgPncParse == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *cfgPncParse = TPM_NONE;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
    if (xmlElement == NULL)
    {
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_CFG_PNC_PARSER, &val);

    if (rc == US_RC_OK)
    {
        *cfgPncParse = (tpm_init_cfg_pnc_parse_t)val;

#ifdef US_DEBUG_PRINT
        printf("cfgPncParse = %d\n", *cfgPncParse);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_trace_debug_info_param()
*
* DESCRIPTION:      Get mode of how to configure the PNC PARSE in LSP
*
* INPUTS:
*
* OUTPUTS:  cfgPncParse - enabled/disabled
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_trace_debug_info_param (uint32_t    *trace_debug_info)
{

    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;
    uint32_t    val;

    if (trace_debug_info == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *trace_debug_info = 0x00000000;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
    if (xmlElement == NULL)
    {
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_TRACE_DEBUG_info, &val);

    if (rc == US_RC_OK)
    {
        *trace_debug_info = val;

#ifdef US_DEBUG_PRINT
        printf("trace_debug_info = 0x%x\n", *trace_debug_info);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}
/*******************************************************************************
* get_wifi_vitual_uni_enable_params()
*
* DESCRIPTION:      Get WiFi via GMAC1 feature parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  enable - whether the WiFi via GMAC1 is enabled
*           port   - port number for WiFi identification
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_wifi_vitual_uni_enable_params (uint32_t    *enable,
                                           uint32_t    *port)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc = US_RC_OK;
    uint32_t    val1, val2;

    if ( (enable == NULL) || (port == NULL) )
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif

        printf("get_wifi_vitual_uni_enable_params => NULL pointer\n");
        return US_RC_FAIL;
    }

    *enable = 0;
	*port = 5;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
    if (xmlElement == NULL)
    {
        rc = US_RC_NOT_FOUND;
    }
    else {
        rc = get_int_param (xmlElement, US_XML_WIFI_ENABLED, &val1);
/* add port parameter to the XML - in the future - meantime default port: UNI_4 = 5 */
/*		rc |= get_int_param (xmlElement, US_XML_WIFI_PORT, &val2);*/
		val2 = 5;
    }

    if (rc == US_RC_OK)
    {
        *enable = val1;
		*port = val2;

#ifdef US_DEBUG_PRINT
        printf("wifi_vir_uni_enable => en= %d  port= %d\n", *enable, *port);
#endif
    }

    ezxml_free(xmlHead);
    return rc;

}

/*******************************************************************************
* get_double_tag_support_params()
*
* DESCRIPTION:      Get double tag support parameter from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  enable - whether the WiFi via GMAC1 is enabled
*           port   - port number for WiFi identification
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_double_tag_support_params (uint32_t    *dbl_tag)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc = US_RC_OK;
    uint32_t    val;

    if ( dbl_tag == NULL )
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif

        return US_RC_FAIL;
    }

    *dbl_tag = 0;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
    if (xmlElement == NULL)
    {
        rc = US_RC_NOT_FOUND;
    }
    else {
        rc = get_int_param (xmlElement, US_XML_DBL_TAG_SUPP, &val);
    }

    if (rc == US_RC_OK)
    {
        *dbl_tag = val;

#ifdef US_DEBUG_PRINT
        printf("get_double_tag_support_params => dbl_tag= %d\n", *dbl_tag);
#endif
    }

    ezxml_free(xmlHead);
    return rc;

}

#if 0
/*******************************************************************************
* get_double_tag_support_params()
*
* DESCRIPTION:      Get double tag support parameter from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  enable - whether the WiFi via GMAC1 is enabled
*           port   - port number for WiFi identification
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_default_vlan_tpid_params (uint16_t    *v1_tpid,
                                      uint16_t    *v2_tpid)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc = US_RC_OK;
    uint32_t    val1, val2;

    if ( (v1_tpid == NULL) || (v2_tpid == NULL) )
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif

        return US_RC_FAIL;
    }

    *v1_tpid = *v2_tpid = 0x8100;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
    if (xmlElement == NULL)
    {
        rc = US_RC_NOT_FOUND;
    }
    else {
        rc |= get_int_param (xmlElement, US_XML_DEF_V1_TPID, &val1);
        rc |= get_int_param (xmlElement, US_XML_DEF_V2_TPID, &val2);
    }

    if (rc == US_RC_OK)
    {
        *v1_tpid = val1;
        *v2_tpid = val2;

#ifdef US_DEBUG_PRINT
        printf("get_double_tag_support_params => v1_tpid= 0x%x  v2_tpid= 0x%x\n", *v1_tpid, *v2_tpid);
#endif
    }

    ezxml_free(xmlHead);
    return rc;

}
#endif

/*******************************************************************************
* get_default_vlan_tpid_params()
*
* DESCRIPTION:      Get double tag support parameter from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  enable - whether the WiFi via GMAC1 is enabled
*           port   - port number for WiFi identification
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_default_vlan_tpid_params(uint32_t *opt_num, tpm_init_tpid_comb_t *opt)
{
	ezxml_t xmlHead;
	ezxml_t xmlElement;
	ezxml_t xmlTpid;
	int optNum;
	int rc;

	if ((opt_num == NULL) || (opt == NULL)) {
#ifdef US_DEBUG_PRINT
		printk(KERN_ERR "%s: NULL pointer\n", __func__);
#endif
		return US_RC_FAIL;
	}

	optNum = 0;

	xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);
	if (xmlHead == NULL)
		return US_RC_FAIL;

	xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0, US_XML_TPM_VLAN_FILTER_TPID_E, -1);
	if (xmlElement == NULL) {
		printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_VLAN_FILTER_TPID_E, US_XML_CFG_FILE_TPM);
		rc = US_RC_NOT_FOUND;
	} else {
		for (xmlTpid = ezxml_child(xmlElement, US_XML_TPM_FILTER_TPID_E); \
			xmlTpid && (optNum < TPM_MAX_TPID_COMB_NUM); \
			xmlTpid = xmlTpid->next, optNum++) {
			rc = get_filter_tpid_attrs(xmlTpid, &(opt[optNum]));
			if (rc != US_RC_OK)
			break;
		}
		*opt_num = optNum;
	}

	return rc;
}

/*******************************************************************************
* get_pon_def_params()
*
* DESCRIPTION: Get PON default parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  param[PON_XML_DEF_PAR_SERIAL_NUM]		- serial number
*           param[PON_XML_DEF_PAR_PASSWORD]			- password
*           param[PON_XML_DEF_PAR_DIS_SERIAL_NUM]	- whether SN should be disabled
*           param[PON_XML_DEF_PARAM_CLEAR_GEM]		- whether gem ports should be removed on reset
*           param[PON_XML_DEF_PARAM_CLEAR_TCONT]	- whether tconts should be removed on reset
*   		param[PON_XML_DEF_PARAM_SERIAL_NUM_SRC]	- whether serial number should be taken from xml
*   													or should the digit part be taken from MAC address
*   		param[PON_XML_DEF_PARAM_XVR_POLARITY]	- tranceiver polarity
*   		param[PON_XML_DEF_PARAM_DG_POLARITY]	- dying gasp polarity
*   		param[PON_XML_DEF_PARAM_RESTORE_GEM]	- whether restore or not GEM ports after returning
*   													from state-7 (EMG STOP)
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_pon_def_params (void **param)
{
	ezxml_t     xmlHead;
	ezxml_t     xmlElement;
	int         rc;
	int			paramIdx;
	unsigned char   *sn;
	unsigned char   *pswd;
    unsigned char xml_sn[US_XML_PON_SN_CHAR_LEN + 1];

    memset(xml_sn, 0, sizeof(xml_sn));

	if (param == NULL) {
#ifdef US_DEBUG_PRINT
		printf("%s: parameters list is a NULL pointer\n", __FUNCTION__);
#endif
		return US_RC_FAIL;
	}

	sn = (unsigned char *)param[PON_XML_DEF_PAR_SERIAL_NUM];
	pswd = (unsigned char *)param[PON_XML_DEF_PAR_PASSWORD];

	for (paramIdx = PON_XML_DEF_PAR_SERIAL_NUM; paramIdx < PON_XML_DEF_PARAM_MAX; paramIdx++) {
		if (param[paramIdx] == NULL) {
#ifdef US_DEBUG_PRINT
			printf("%s: parameter #%d is a NULL pointer\n", __FUNCTION__, paramIdx);
#endif
			return US_RC_FAIL;
		}
	}

	xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_PON);

	if (xmlHead == NULL)
		return US_RC_FAIL;

	xmlElement = ezxml_child(xmlHead, US_XML_PON_E);
	if (xmlElement == NULL) {
		printf("Failed to find %s in XML config. file %s\n", US_XML_PON_E, US_XML_CFG_FILE_PON);
		rc = US_RC_NOT_FOUND;

	} else {

		sn[0] = '\0';
        rc = get_char_param(xmlElement, US_XML_PON_SN_E, xml_sn, US_XML_PON_SN_CHAR_LEN);
        rc |= convertXmlSn2MsgSn(xml_sn, sn);
		pswd[0] = '\0';
		rc |= get_char_param(xmlElement, US_XML_PON_PASSWD_E, pswd, US_XML_PON_PASSWD_LEN);
		*(uint32_t *)param[PON_XML_DEF_PAR_DIS_SERIAL_NUM] = 0;
		rc |= get_int_param (xmlElement, US_XML_PON_DIS_SN_E, (uint32_t *)param[PON_XML_DEF_PAR_DIS_SERIAL_NUM]);
		*(uint32_t *)param[PON_XML_DEF_PARAM_CLEAR_GEM] = 0;
		rc |= get_int_param(xmlElement, US_XML_PON_GEM_RST_E, (uint32_t *)param[PON_XML_DEF_PARAM_CLEAR_GEM]);
		*(uint32_t *)param[PON_XML_DEF_PARAM_CLEAR_TCONT] = 0;
		rc |= get_int_param(xmlElement, US_XML_PON_TCONT_RST_E, (uint32_t *)param[PON_XML_DEF_PARAM_CLEAR_TCONT]);
		*(uint32_t *)param[PON_XML_DEF_PARAM_SERIAL_NUM_SRC] = 0;
		rc |= get_int_param(xmlElement, US_XML_PON_SN_SRC_E, (uint32_t *)param[PON_XML_DEF_PARAM_SERIAL_NUM_SRC]);
        *(uint32_t *)param[PON_XML_DEF_PARAM_XVR_BURST_ENABLE_POLARITY] = 0;
		rc |= get_int_param(xmlElement, US_XML_PON_XVR_BURST_EN_POL_E, (uint32_t *)param[PON_XML_DEF_PARAM_XVR_BURST_ENABLE_POLARITY]);
        *(uint32_t *)param[PON_XML_DEF_PARAM_XVR_POLARITY] = 0;
		rc |= get_int_param(xmlElement, US_XML_PON_XVR_POL_E, (uint32_t *)param[PON_XML_DEF_PARAM_XVR_POLARITY]);
        *(uint32_t *)param[P2P_XML_DEF_PARAM_XVR_BURST_ENABLE_POLARITY] = 1;
		rc |= get_int_param(xmlElement, US_XML_P2P_XVR_BURST_EN_POL_E, (uint32_t *)param[P2P_XML_DEF_PARAM_XVR_BURST_ENABLE_POLARITY]);
        *(uint32_t *)param[P2P_XML_DEF_PARAM_XVR_POLARITY] = 0;
		rc |= get_int_param(xmlElement, US_XML_P2P_XVR_POL_E, (uint32_t *)param[P2P_XML_DEF_PARAM_XVR_POLARITY]);
		*(uint32_t *)param[PON_XML_DEF_PARAM_DG_POLARITY] = 1;
		rc |= get_int_param(xmlElement, US_XML_PON_DG_POL_E, (uint32_t *)param[PON_XML_DEF_PARAM_DG_POLARITY]);
		*(uint32_t *)param[PON_XML_DEF_PARAM_RESTORE_GEM] = 1;
		rc |= get_int_param(xmlElement, US_XML_PON_GEM_RESTOR_E, (uint32_t *)param[PON_XML_DEF_PARAM_RESTORE_GEM]);
        *(uint32_t *)param[PON_XML_DEF_PARAM_FEC_HYST] = 1;
		rc |= get_int_param(xmlElement, US_XML_PON_FEC_HYST_E, (uint32_t *)param[PON_XML_DEF_PARAM_FEC_HYST]);
        *(uint32_t *)param[PON_XML_DEF_PARAM_COUPLING_MODE] = 1;
        rc |= get_int_param(xmlElement, US_XML_PON_COUPLING_MODE_E, (uint32_t *)param[PON_XML_DEF_PARAM_COUPLING_MODE]);

#ifdef US_DEBUG_PRINT
		printf("PON default params: SN %s pswd %s dis %d\n", sn, pswd,
			   *(uint32_t *)param[PON_XML_DEF_PAR_DIS_SERIAL_NUM]);
#endif
	}

	ezxml_free(xmlHead);
	return rc;
}

/*******************************************************************************
* get_epon_def_params()
*
* DESCRIPTION: Get EPON default parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  xvr_pol       - XVR polarity (0 - high, 1 - low)
*           dg_pol        - Dying Gasp polarity (1 - high, 0 - low)
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_epon_def_params     (unsigned long  *pon_xvr_burst_en_pol, 
                                 unsigned long  *pon_xvr_pol, 
                                 unsigned long  *p2p_xvr_burst_en_pol, 
                                 unsigned long  *p2p_xvr_pol, 
								 unsigned long  *dg_pol, 
								 unsigned long  *pkt_2k_supported)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_PON);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_EPON_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_PON);
        rc = US_RC_NOT_FOUND;
    }
    else
    {
        *pon_xvr_burst_en_pol = 0;
        rc = get_int_param (xmlElement, US_XML_PON_XVR_BURST_EN_POL_E, (uint32_t*)pon_xvr_burst_en_pol);
        *pon_xvr_pol = 0;
        rc = get_int_param (xmlElement, US_XML_PON_XVR_POL_E, (uint32_t*)pon_xvr_pol);
        *p2p_xvr_burst_en_pol = 0;
        rc = get_int_param (xmlElement, US_XML_P2P_XVR_BURST_EN_POL_E, (uint32_t*)p2p_xvr_burst_en_pol);
        *p2p_xvr_pol = 0;
        rc = get_int_param (xmlElement, US_XML_P2P_XVR_POL_E, (uint32_t*)p2p_xvr_pol);
        *dg_pol = 1;
        rc |= get_int_param (xmlElement, US_XML_EPON_DG_POL_E, (uint32_t*)dg_pol);
        *pkt_2k_supported = 1;
        rc |= get_int_param(xmlElement, US_XML_EPON_2KB_SUPP_E, (uint32_t*)pkt_2k_supported);
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_pon_dg_polarity()
*
* DESCRIPTION: Get PON Dying Gasp polarity value from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  dg_pol - Daing Gasp polarity is either 0 for high or 1 for low
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_pon_dg_polarity     (unsigned long  *dg_pol)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_PON);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_PON_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_PON_E, US_XML_CFG_FILE_PON);
        rc = US_RC_NOT_FOUND;
    }
    else
    {
        *dg_pol = 1;
        rc = get_int_param (xmlElement, US_XML_PON_DG_POL_E, (uint32_t*)dg_pol);
        if (rc == US_RC_NOT_FOUND) {
            printf("Failed to find %s tag in XML config. file \n", US_XML_PON_DG_POL_E);
            *dg_pol = 1;
            rc = US_RC_OK;
        }
    }

    ezxml_free(xmlHead);
    return rc;

}

/*******************************************************************************
* get_vlan_etypes_params()
*
* DESCRIPTION:      Get VLAN ETY parameters from XML configuration file
*
* INPUTS:   vlan_ety_reg_num    - Number of VLAN ETY registers in the system
*
* OUTPUTS:  etypes              - pointer to the VLANs ETY table
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_vlan_etypes_params (uint32_t    *etypes,
                                int         vlan_ety_reg_num)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    ezxml_t     xmlEty;
    int         rc = US_RC_OK, i;

    if (etypes == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    if (vlan_ety_reg_num == 0)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: Invalid vlan_ety_reg_num \n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    for (i=0; i<vlan_ety_reg_num; i++)
        etypes[i] = MV_TPM_UN_INITIALIZED_INIT_PARAM;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0, US_XML_TPM_VLAN_TYPE_E, -1);
    if (xmlElement == NULL)
    {
        rc = US_RC_NOT_FOUND;
    }
    else
    {
        for (xmlEty = ezxml_child(xmlElement, US_XML_TPM_ETY_E); xmlEty; xmlEty = xmlEty->next)
        {
            rc = get_vlan_ety_attrs (xmlEty, etypes, vlan_ety_reg_num);

            if (rc != US_RC_OK)
                break;
        }

#ifdef US_DEBUG_PRINT
    {
        int  reg;

        printf("============================\n");
        printf("  Reg#   VLAN etype   \n");
        printf("============================\n");
        for (reg = 0; reg < vlan_ety_reg_num; reg++)
        {
            printf("   %d        0x%x\n", reg, etypes[reg]);
        }
        printf("============================\n");
    }
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_modification_params()
*
* DESCRIPTION: Get TPM modification parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  config  - configuration parameters of Modification table
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int     get_modification_params (tpm_init_mod_params_t  *config)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    ezxml_t     xmlModParam;
    int         rc  = US_RC_OK;

    if (config == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0, US_XML_TPM_MOD_E, -1);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_MOD_E, US_XML_CFG_FILE_TPM);
        rc = US_RC_NOT_FOUND;
    }
    else
    {
        //config->reserved_num = 0;
        //rc = get_int_param (xmlElement, US_XML_TPM_MOD_RES_NUM_E, &config->reserved_num);

#ifdef US_DEBUG_PRINT
        printf("MOD params: Reserved num = %d\n", config->reserved_num);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_config_mode_param()
*
* DESCRIPTION:      Get TPM configuration mode parameter from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  mode - TPM_CFG_MODE_APPL   - XML configuration parameters processed by application
*                  TPM_CFG_MODE_KERNEL - XML configuration parameters processed by kernel
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_config_mode_param (tpm_config_mode_t *mode)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;
    uint32_t    val;

    if (mode == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *mode = TPM_CFG_MODE_KERNEL;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM);
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_GET_CFG_MODE_E, &val);

    if (rc == US_RC_OK)
    {
        *mode = (tpm_config_mode_t)val;

#ifdef US_DEBUG_PRINT
        printf("TPM configuration mode = %d\n", *mode);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_epon_igmp_config_params()
*
* DESCRIPTION:      Get EPON IGMP parameters from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  EPON IGMP supported
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_epon_igmp_config_params(uint32_t *igmpSupp)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;

    if (igmpSupp == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *igmpSupp = 0;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_PON);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_EPON_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_PON);
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_EPON_IGMP_SUPP_E, igmpSupp);

    if (rc == US_RC_OK)
    {
#ifdef US_DEBUG_PRINT
        printf("============================\n");
        printf("IGMP supported = %d\n", *igmpSupp);
        printf("============================\n");
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

/*******************************************************************************
* get_omci_txq_param()
*
* DESCRIPTION:      Get OMCI CPU TXQ from XML configuration file
*
* INPUTS:
*
* OUTPUTS:  OMCI CPU TXQ number
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_omci_txq_param(uint32_t *cpuTxq)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    int         rc;

    if (cpuTxq == NULL)
    {
#ifdef US_DEBUG_PRINT
        printf("%s: NULL pointer\n", __FUNCTION__);
#endif
        return US_RC_FAIL;
    }

    *cpuTxq = 6;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_OMCI);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_OMCI_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_OMCI_E, US_XML_CFG_FILE_OMCI);
        rc = US_RC_NOT_FOUND;
    }
    else
        rc = get_int_param (xmlElement, US_XML_CPU_Q_E, cpuTxq);

    if (rc == US_RC_OK)
    {
#ifdef US_DEBUG_PRINT
        printf("============================\n");
        printf("CPU TXQ = %d\n", *cpuTxq);
        printf("============================\n");
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}


int get_ety_dsa_enable(uint32_t *ety_dsa_enable)
{
	ezxml_t xmlHead;
	ezxml_t xmlElement;
	int rc = US_RC_OK;

	if (ety_dsa_enable == NULL) {
#ifdef US_DEBUG_PRINT
		printk(KERN_ERR "%s: NULL pointer\n", __func__);
#endif
		return US_RC_FAIL;
	}

	*ety_dsa_enable = 0;

	xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_TPM);

	if (xmlHead == NULL)
		return US_RC_FAIL;

	xmlElement = ezxml_child(xmlHead, US_XML_TRAFFIC_SETTING_E);
	if (xmlElement == NULL) {
#ifdef US_DEBUG_PRINT
		printk(KERN_ERR "Failed to find %s in XML config. file %s\n", US_XML_TRAFFIC_SETTING_E, g_pstr_xml_cfg_file);
#endif
		rc = US_RC_NOT_FOUND;
	} else {
		rc = get_int_param(xmlElement, US_XML_ETY_DSA_ENABLE_E, ety_dsa_enable);

#ifdef US_DEBUG_PRINT
		printk("ety_dsa_enable %d \n", *ety_dsa_enable);
#endif
	}

	/*ezxml_free(xmlHead); */
	return rc;
}


/*******************************************************************************
* get_apm_alarm_params()
*
* DESCRIPTION:     
*
* INPUTS:
*
* OUTPUTS:  apm_alarm_poll_interval -
*           apm_alarm_max_entity_num   - 
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_apm_alarm_params (unsigned int *apm_alarm_poll_interval, unsigned int *apm_alarm_max_entity_num, bool *boolvalue)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    ezxml_t     xmlActivationElement;
    int         rc;
    uint32_t    val;
    const char  *attr_str;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_APM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_APM_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_APM_E, US_XML_CFG_FILE_APM);
        rc = US_RC_NOT_FOUND;
    }
    else
    {       
        rc = get_int_param (xmlElement, US_XML_APM_ALARM_POLL_INTERVAL_E, apm_alarm_poll_interval);

        rc |= get_int_param (xmlElement, US_XML_APM_ALARM_MAX_ENTITY_NUM_E, apm_alarm_max_entity_num);

        if ((xmlActivationElement = ezxml_child (xmlElement, US_XML_APM_ALARM_ACTIVATION_E)) == 0)
        {
            printf("ezxml_get() of %s failed\n", US_XML_APM_ALARM_ACTIVATION_E);
            return US_RC_NOT_FOUND;
        }

        if ((attr_str = ezxml_attr(xmlActivationElement, US_XML_ENABLED_E)) != 0)
        {
            if (strcmp(attr_str, "true") == 0)
            {
                *boolvalue = true;
            }
            else if (strcmp(attr_str, "false") == 0)
            {
                *boolvalue = false;
            }
            else
            {
                printf("attribute %s has invalid value '%s'", US_XML_ENABLED_E, attr_str);
                return US_RC_FAIL;
            }
        }
        else
        {
            printf("ezxml_get() of %s failed\n", US_XML_ENABLED_E);
            return US_RC_NOT_FOUND;
        }
        
    }        

    if (rc == US_RC_OK)
    {
#ifdef US_DEBUG_PRINT
        printf("APM alarm poll interval = %d max entity num = %d boolvalue = %d\n", *apm_alarm_poll_interval, *apm_alarm_max_entity_num, *boolvalue);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}



/*******************************************************************************
* get_apm_pm_params()
*
* DESCRIPTION:     
*
* INPUTS:
*
* OUTPUTS:  apm_pm_poll_interval -
*           apm_pm_max_entity_num   - 
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_apm_pm_params (unsigned int *apm_pm_poll_interval, unsigned int *apm_pm_max_entity_num, bool *boolvalue)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    ezxml_t     xmlActivationElement;
    int         rc;
    uint32_t    val;
    const char  *attr_str;

    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_APM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_APM_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_APM_E, US_XML_CFG_FILE_APM);
        rc = US_RC_NOT_FOUND;
    }
    else
    {
        rc = get_int_param (xmlElement, US_XML_APM_PM_POLL_INTERVAL_E, apm_pm_poll_interval);

        rc |= get_int_param (xmlElement, US_XML_APM_PM_MAX_ENTITY_NUM_E, apm_pm_max_entity_num);

        if ((xmlActivationElement = ezxml_child (xmlElement, US_XML_APM_PM_ACTIVATION_E)) == 0)
        {
            printf("ezxml_get() of %s failed\n", US_XML_APM_PM_ACTIVATION_E);
            return US_RC_NOT_FOUND;
        }

        if ((attr_str = ezxml_attr(xmlActivationElement, US_XML_ENABLED_E)) != 0)
        {
            if (strcmp(attr_str, "true") == 0)
            {
                *boolvalue = true;
            }
            else if (strcmp(attr_str, "false") == 0)
            {
                *boolvalue = false;
            }
            else
            {
                printf("attribute %s has invalid value '%s'", US_XML_ENABLED_E, attr_str);
                return US_RC_FAIL;
            }
        }
        else
        {
            printf("ezxml_get() of %s failed\n", US_XML_ENABLED_E);
            return US_RC_NOT_FOUND;
        }
    }        

    if (rc == US_RC_OK)
    {
#ifdef US_DEBUG_PRINT
        printf("APM pm poll interval = %d max entity num = %d boolvalue = %d\n", *apm_pm_poll_interval, *apm_pm_max_entity_num, *boolvalue);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}



/*******************************************************************************
* get_apm_avc_params()
*
* DESCRIPTION:     
*
* INPUTS:
*
* OUTPUTS:  apm_avc_poll_interval -
*           apm_avc_max_entity_num   - 
*
* RETURNS:  US_RC_OK, US_RC_FAIL or US_RC_NOT_FOUND
*
*******************************************************************************/
int get_apm_avc_params (unsigned int *apm_avc_poll_interval, unsigned int *apm_avc_max_entity_num, bool *boolvalue)
{
    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    ezxml_t     xmlActivationElement;
    int         rc;
    uint32_t    val;
    const char  *attr_str;


    xmlHead = get_xml_head_ptr(US_XML_CFG_FILE_APM);

    if (xmlHead == NULL)
        return US_RC_FAIL;

    xmlElement = ezxml_child(xmlHead, US_XML_APM_E);
    if (xmlElement == NULL)
    {
        printf("Failed to find %s in XML config. file %s\n", US_XML_APM_E, US_XML_CFG_FILE_APM);
        rc = US_RC_NOT_FOUND;
    }
    else
    {
        rc = get_int_param (xmlElement, US_XML_APM_AVC_POLL_INTERVAL_E, apm_avc_poll_interval);

        rc |= get_int_param (xmlElement, US_XML_APM_AVC_MAX_ENTITY_NUM_E, apm_avc_max_entity_num);

        if ((xmlActivationElement = ezxml_child (xmlElement, US_XML_APM_AVC_ACTIVATION_E)) == 0)
        {
            printf("ezxml_get() of %s failed\n", US_XML_APM_AVC_ACTIVATION_E);
            return US_RC_NOT_FOUND;
        }

        if ((attr_str = ezxml_attr(xmlActivationElement, US_XML_ENABLED_E)) != 0)
        {
            if (strcmp(attr_str, "true") == 0)
            {
                *boolvalue = true;
            }
            else if (strcmp(attr_str, "false") == 0)
            {
                *boolvalue = false;
            }
            else
            {
                printf("attribute %s has invalid value '%s'", US_XML_ENABLED_E, attr_str);
                return US_RC_FAIL;
            }
        }
        else
        {
            printf("ezxml_get() of %s failed\n", US_XML_ENABLED_E);
            return US_RC_NOT_FOUND;
        }
    }        

    if (rc == US_RC_OK)
    {
#ifdef US_DEBUG_PRINT
        printf("APM avc poll interval = %d max entity num = %d boolvalue = %d\n", *apm_avc_poll_interval, *apm_avc_max_entity_num, *boolvalue);
#endif
    }

    ezxml_free(xmlHead);
    return rc;
}

int hexC2decimalN(char hexC, unsigned char *decimalN)
{
    if (hexC >= '0' && hexC <= '9')
    {
        *decimalN = hexC - '0';
    }
    else if (hexC >= 'a' && hexC <= 'f')
    {
        *decimalN = 10 + (hexC - 'a');
    }
    else if (hexC >= 'A' && hexC <= 'F')
    {
        *decimalN = 10 + (hexC - 'A');
    }
    else
    {
        return -1;
    }

    return 0;
}

int convertXmlSn2MsgSn(unsigned char *xml_sn, unsigned char *msg_sn)
{
    int ret = 0;
    int i_xml;
    int i_msg;
    unsigned char num_high;
    unsigned char num_low;

    /* copy vendor id */
    memcpy(msg_sn, xml_sn, US_XML_PON_SN_VENDOR_ID_CHAR_LEN);

    /* convert vendor specific sn */
    i_xml = US_XML_PON_SN_VENDOR_ID_CHAR_LEN;
    i_msg = US_XML_PON_SN_VENDOR_ID_CHAR_LEN;
    
    while(i_xml < US_XML_PON_SN_CHAR_LEN)
    {
        ret = hexC2decimalN(xml_sn[i_xml++], &num_high);
        if (ret != 0)
        {
            return -1;
        }

        ret = hexC2decimalN(xml_sn[i_xml++], &num_low);
        if (ret != 0)
        {
            return -1;
        }

        msg_sn[i_msg++] = num_high * 16 + num_low;
    }

    return 0;
}


