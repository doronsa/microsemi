/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
#include <assert.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include <link.h>
#include <elf.h>
#include <termios.h>
#include <ctype.h>
#include <syslog.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/sem.h>
#include <sys/mman.h>
#include <sys/syslog.h>
#include <sys/msg.h>
#include <sys/resource.h>
#include <netinet/in.h>
#include <asm/ioctls.h>
#include <asm/mman.h>
#include <asm/unistd.h>
#include <arpa/telnet.h>
#include <arpa/inet.h>

extern pthread_key_t  g_clish_ostream_key;
extern int grantpt(int fd);
extern int unlockpt(int fd);
extern char* ptsname(int fd);

#define syslog_debug(x...)
#define MIN(a,b) (((a)<(b))?(a):(b))
#define DEFAULT_SHELL "/bin/sh"
#define CONFIG_FEATURE_DEVPTS
//#define CONFIG_FEATURE_TELNETD_INETD

#ifdef DEBUG
#define TELCMDS
#define TELOPTS
#endif

#define BUFSIZE 4000

#ifdef CONFIG_FEATURE_IPV6
#define SOCKET_TYPE AF_INET6
typedef struct sockaddr_in6 sockaddr_type;
#else
#define SOCKET_TYPE AF_INET
typedef struct sockaddr_in sockaddr_type;
#endif

#ifdef CONFIG_LOGIN
static const char *loginpath = "/bin/login";
#else
static const char *loginpath;
#endif

/* shell name and arguments */

static const char *argv_init[] = {0, 0};

/* structure that describes a session */

struct tsession {
#ifdef CONFIG_FEATURE_TELNETD_INETD
    int sockfd_read, sockfd_write, ptyfd;
#else /* CONFIG_FEATURE_TELNETD_INETD */
    struct tsession *next;
    int sockfd, ptyfd;
#endif /* CONFIG_FEATURE_TELNETD_INETD */
    int shell_pid;
    /* two circular buffers */
    char *buf1, *buf2;
    int rdidx1, wridx1, size1;
    int rdidx2, wridx2, size2;
};

static int maxfd;

static struct tsession *sessions;

static char *
remove_iacs(struct tsession *ts, int *pnum_totty) {
    unsigned char *ptr0 = (unsigned char *)ts->buf1 + ts->wridx1;
    unsigned char *ptr = ptr0;
    unsigned char *totty = ptr;
    unsigned char *end = ptr + MIN(BUFSIZE - ts->wridx1, ts->size1);
    int processed;
    int num_totty;

    while (ptr < end) {
        if (*ptr != IAC) {
            int c = *ptr;
            *totty++ = *ptr++;
            /* We now map \r\n ==> \r for pragmatic reasons.
             * Many client implementations send \r\n when
             * the user hits the CarriageReturn key.
             */
            if (c == '\r' && (*ptr == '\n' || *ptr == 0) && ptr < end)
                ptr++;
        }
        else {
            /*
             * TELOPT_NAWS support!
             */
            if ((ptr+2) >= end) {
                /* only the beginning of the IAC is in the
                buffer we were asked to process, we can't
                process this char. */
                break;
            }

            /*
             * IAC -> SB -> TELOPT_NAWS -> 4-byte -> IAC -> SE
             */
            else if (ptr[1] == SB && ptr[2] == TELOPT_NAWS) {
                struct winsize ws;
                if ((ptr+8) >= end)
                    break;  /* incomplete, can't process */
                ws.ws_col = (ptr[3] << 8) | ptr[4];
                ws.ws_row = (ptr[5] << 8) | ptr[6];
                (void) ioctl(ts->ptyfd, TIOCSWINSZ, (char *)&ws);
                ptr += 9;
            }
            else {
                /* skip 3-byte IAC non-SB cmd */
#ifdef DEBUG
                fprintf(stderr, "Ignoring IAC %s,%s\n",
                    TELCMD(*(ptr+1)), TELOPT(*(ptr+2)));
#endif
                ptr += 3;
            }
        }
    }

    processed = ptr - ptr0;
    num_totty = totty - ptr0;
    /* the difference between processed and num_to tty
       is all the iacs we removed from the stream.
       Adjust buf1 accordingly. */
    ts->wridx1 += processed - num_totty;
    ts->size1 -= processed - num_totty;
    *pnum_totty = num_totty;
    /* move the chars meant for the terminal towards the end of the
    buffer. */
    return memmove(ptr - num_totty, ptr0, num_totty);
}

static int
getpty(char *line)
{
    int p;
#ifdef CONFIG_FEATURE_DEVPTS
    p = open("/dev/ptmx", 2);
    if (p > 0) {
        grantpt(p);
        unlockpt(p);
        strcpy(line, ptsname(p));
        return(p);
    }
#else
    struct stat stb;
    int i;
    int j;

    strcpy(line, "/dev/ptyXX");

    for (i = 0; i < 16; i++) {
        line[8] = "pqrstuvwxyzabcde"[i];
        line[9] = '0';
        if (stat(line, &stb) < 0) {
            continue;
        }
        for (j = 0; j < 16; j++) {
            line[9] = j < 10 ? j + '0' : j - 10 + 'a';
#ifdef DEBUG
            fprintf(stderr, "Trying to open device: %s\n", line);
#endif
            if ((p = open(line, O_RDWR | O_NOCTTY)) >= 0) {
                line[5] = 't';
                return p;
            }
        }
    }
#endif /* CONFIG_FEATURE_DEVPTS */
    return -1;
}


static void
send_iac(struct tsession *ts, unsigned char command, int option)
{
    /* We rely on that there is space in the buffer for now.  */
    char *b = ts->buf2 + ts->rdidx2;
    *b++ = IAC;
    *b++ = command;
    *b++ = option;
    ts->rdidx2 += 3;
    ts->size2 += 3;
}


static struct tsession *
#ifdef CONFIG_FEATURE_TELNETD_INETD
make_new_session(int sockfd)
#else /* CONFIG_FEATURE_TELNETD_INETD */
make_new_session(int sockfd)
#endif /* CONFIG_FEATURE_TELNETD_INETD */
{
    struct termios termbuf;
    int pty, pid, fd[5];
    char tty_name[32];
    struct tsession *ts = malloc(sizeof(struct tsession) + BUFSIZE * 2);

    memset(ts, 0 , sizeof(struct tsession) + BUFSIZE * 2);
    //ts->size1= ts->size2 = 0;
#if 1
    ts->buf1 = (char *)(&ts[1]);
    ts->buf2 = ts->buf1 + BUFSIZE;

#ifdef CONFIG_FEATURE_TELNETD_INETD
    ts->sockfd_read = sockfd;
    ts->sockfd_write = sockfd;
#else /* CONFIG_FEATURE_TELNETD_INETD */
    ts->sockfd = sockfd;
#endif /* CONFIG_FEATURE_TELNETD_INETD */

    /* Got a new connection, set up a tty and spawn a shell.  */

    syslog_debug(LOG_ERR, "syslog_debug test fd(%d)\n", sockfd);
    pty = getpty(tty_name);
    //DB(" %s, sockfd(%d), pty(%d),ttyname(%s) \n", __FUNCTION__, sockfd, pty, tty_name );

    if (pty < 0) {
        syslog_debug(LOG_ERR, "All terminals in use!");
        return 0;
    }

    if (pty > maxfd)
        maxfd = pty;

    ts->ptyfd = pty;

    /* Make the telnet client understand we will echo characters so it
     * should not do it locally. We don't tell the client to run linemode,
     * because we want to handle line editing and tab completion and other
     * stuff that requires char-by-char support.
     */

    send_iac(ts, DO, TELOPT_ECHO);
    send_iac(ts, DO, TELOPT_NAWS);
    send_iac(ts, DO, TELOPT_LFLOW);
    send_iac(ts, WILL, TELOPT_ECHO);
    send_iac(ts, WILL, TELOPT_SGA);
#endif
    if ((pid = fork()) < 0) {
        syslog_debug(LOG_ERR, "Could not fork");
    }
    //DB("-1- pid(%d), fork pid(%d)\n", getpid(), pid);
    if (pid == 0) {
        /* In child, open the child's side of the tty.  */
        int i;
#if 1
        //DB("-2- pid(%d), maxfd(%d)\n", getpid(), maxfd);
        syslog_debug(LOG_ERR, "-2- pid(%d), maxfd(%d)\n", getpid(), maxfd);
        for(i = 0; i <= maxfd; i++)
        {   
            close(i);
        }
        /* make new process group */
        setsid();

        syslog_debug(LOG_ERR, " %s, sockfd(%d), pty(%d),ttyname(%s) \n", __FUNCTION__, sockfd, pty, tty_name );

        if ((fd[0] = open(tty_name, O_RDWR /*| O_NOCTTY*/)) < 0) {
            syslog_debug(LOG_ERR, "Could not open tty");
            exit(1);
        }
        //syslog_debug(LOG_ERR, "-0-dup new fd(%d)\n", fd);
        fd[1] = dup(0);
        fd[2] = dup(0);

        for(i = 0; i< 5; i++)
        {
            syslog_debug(LOG_ERR, "-dup new fd[%d](%d)\n", i, fd[i]);
        }

        tcsetpgrp(0, getpid());

        /* The pseudo-terminal allocated to the client is configured to operate in
         * cooked mode, and with XTABS CRMOD enabled (see tty(4)).
         */

        tcgetattr(0, &termbuf);
        termbuf.c_lflag |= ECHO; /* if we use readline we dont want this */
        termbuf.c_oflag |= ONLCR|XTABS;
        termbuf.c_iflag |= ICRNL;
        termbuf.c_iflag &= ~IXOFF;
        /*termbuf.c_lflag &= ~ICANON;*/
        tcsetattr(0, TCSANOW, &termbuf);

        // print_login_issue(issuefile, 0);
#endif
        /* exec shell, with correct argv and env */
        syslog_debug(LOG_ERR, "start execv ");
        execv("/bin/sh", (char *const *)argv_init);

        /* NOT REACHED */
        syslog_debug(LOG_ERR, "execv error");
        exit(1);
    }

    ts->shell_pid = pid;

    return ts;
}

#ifndef CONFIG_FEATURE_TELNETD_INETD
static void
free_session(struct tsession *ts)
{
    struct tsession *t = sessions;

    /* Unlink this telnet session from the session list.  */
    if (t == ts)
        sessions = ts->next;
    else {
        while(t->next != ts)
            t = t->next;
        t->next = ts->next;
    }

    kill(ts->shell_pid, SIGKILL);

    wait4(ts->shell_pid, 0, 0, 0);

    close(ts->ptyfd);
//strong do not close father's fd   close(ts->sockfd);

    if (ts->ptyfd == maxfd || ts->sockfd == maxfd)
        maxfd--;
    if (ts->ptyfd == maxfd || ts->sockfd == maxfd)
        maxfd--;

    free(ts);
}
#endif /* CONFIG_FEATURE_TELNETD_INETD */

int linux_shell(int fd)
{
    struct tsession *new_ts;
    int exit_flag = 0;
   
    fd_set rdfdset, wrfdset;
    int selret;
    int maxlen, w, r;

#ifndef CONFIG_LOGIN
    loginpath = DEFAULT_SHELL;
#endif

    argv_init[0] = "sh";

    {//strong add
        new_ts = make_new_session(fd);
        if (new_ts) 
        {
            #ifndef CONFIG_FEATURE_TELNETD_INETD
            new_ts->next = sessions;
            #endif
            sessions = new_ts;
            if (fd > maxfd)
                maxfd = fd;
        }
    }
    //syslog_debug(LOG_ERR, "-1a-after make_new_session, ts(%08x), ts->size1(%d), size2(%d),ptyfd(%d)\n",
    //            new_ts, new_ts->size1, new_ts->size2, new_ts->ptyfd );

    do {
        struct tsession *ts;
        FD_ZERO(&rdfdset);
        FD_ZERO(&wrfdset);

        /* select on the master socket, all telnet sockets and their
         * ptys if there is room in their respective session buffers.
         */

#ifndef CONFIG_FEATURE_TELNETD_INETD
        //strong FD_SET(master_fd, &rdfdset);
#endif /* CONFIG_FEATURE_TELNETD_INETD */

        ts = sessions;
        syslog_debug(LOG_ERR, "-2-after make_new_session, ts(%08x), ts->size1(%d), size2(%d),ptyfd(%d)\n", 
                ts, ts->size1, ts->size2, ts->ptyfd );
#ifndef CONFIG_FEATURE_TELNETD_INETD
        while (ts) {
#endif /* CONFIG_FEATURE_TELNETD_INETD */
            /* buf1 is used from socket to pty
             * buf2 is used from pty to socket
             */
            if (ts->size1 > 0) {
                FD_SET(ts->ptyfd, &wrfdset);  /* can write to pty */
            }
            if (ts->size1 < BUFSIZE) {
#ifdef CONFIG_FEATURE_TELNETD_INETD
                FD_SET(ts->sockfd_read, &rdfdset); /* can read from socket */
#else /* CONFIG_FEATURE_TELNETD_INETD */
                FD_SET(ts->sockfd, &rdfdset); /* can read from socket */
#endif /* CONFIG_FEATURE_TELNETD_INETD */
            }
            if (ts->size2 > 0) {
#ifdef CONFIG_FEATURE_TELNETD_INETD
                FD_SET(ts->sockfd_write, &wrfdset); /* can write to socket */
#else /* CONFIG_FEATURE_TELNETD_INETD */
                FD_SET(ts->sockfd, &wrfdset); /* can write to socket */
#endif /* CONFIG_FEATURE_TELNETD_INETD */
            }
            if (ts->size2 < BUFSIZE) {
                FD_SET(ts->ptyfd, &rdfdset);  /* can read from pty */
            }
            break; //strong
            
#ifndef CONFIG_FEATURE_TELNETD_INETD
		/* Masked the unreachable code */
        /*    ts = ts->next; */
        }
#endif /* CONFIG_FEATURE_TELNETD_INETD */

        syslog_debug(LOG_ERR, "start select (%d), ts->sockfd(%d) \n", maxfd, ts->sockfd );
        selret = select(maxfd + 1, &rdfdset, &wrfdset, 0, 0);

        if (!selret)
            break;

#ifndef CONFIG_FEATURE_TELNETD_INETD
        /* First check for and accept new sessions.  */

        /* Then check for data tunneling.  */

        ts = sessions;
        while (ts) { /* For all sessions...  */
#endif /* CONFIG_FEATURE_TELNETD_INETD */
#ifndef CONFIG_FEATURE_TELNETD_INETD
            struct tsession *next = ts->next; /* in case we free ts. */
#endif /* CONFIG_FEATURE_TELNETD_INETD */

            syslog_debug(LOG_ERR, "select ok, ts(%08x), ts->size1(%d), size2(%d),ptyfd(%d)\n", ts, ts->size1, ts->size2, ts->ptyfd );
            if (ts->size1 && FD_ISSET(ts->ptyfd, &wrfdset)) {
                int num_totty;
                char *ptr;
                /* Write to pty from buffer 1.  */
                syslog_debug(LOG_ERR, "----------1--w pty-(%s)------------\n", ptr);
                ptr = remove_iacs(ts, &num_totty);

                w = write(ts->ptyfd, ptr, num_totty);
                if (w < 0) {
                    syslog_debug(LOG_ERR, "----failed------(%s)-%d----\n",__FUNCTION__, __LINE__);
#ifdef CONFIG_FEATURE_TELNETD_INETD
                    exit(0);
#else /* CONFIG_FEATURE_TELNETD_INETD */
                    free_session(ts);
                    ts = next;
                    continue;  
#endif /* CONFIG_FEATURE_TELNETD_INETD */
                }
                ts->wridx1 += w;
                ts->size1 -= w;
                if (ts->wridx1 == BUFSIZE)
                    ts->wridx1 = 0;
            }

#ifdef CONFIG_FEATURE_TELNETD_INETD
            if (ts->size2 && FD_ISSET(ts->sockfd_write, &wrfdset)) {
#else /* CONFIG_FEATURE_TELNETD_INETD */
            if (ts->size2 && FD_ISSET(ts->sockfd, &wrfdset)) {
#endif /* CONFIG_FEATURE_TELNETD_INETD */
                /* Write to socket from buffer 2.  */
                
                maxlen = MIN(BUFSIZE - ts->wridx2, ts->size2);
#ifdef CONFIG_FEATURE_TELNETD_INETD
                w = write(ts->sockfd_write, ts->buf2 + ts->wridx2, maxlen);
            
                if (w < 0)
                {
                    syslog_debug(LOG_ERR, "----failed------(%s)-%d----\n",__FUNCTION__, __LINE__);
                    exit(0);
                }
#else /* CONFIG_FEATURE_TELNETD_INETD */
                syslog_debug(LOG_ERR, "----------2--w sock-(%s)--------%d----\n",ts->buf2 + ts->wridx2, __LINE__);
                w = write(ts->sockfd, ts->buf2 + ts->wridx2, maxlen);
                if (w < 0) {
                 syslog_debug(LOG_ERR, "----failed------(%s)-%d----\n",__FUNCTION__, __LINE__);
                    
                    free_session(ts);
                    ts = next;
                    continue;
                }
    
#endif /* CONFIG_FEATURE_TELNETD_INETD */
                ts->wridx2 += w;
                ts->size2 -= w;
                if (ts->wridx2 == BUFSIZE)
                    ts->wridx2 = 0;
            }
    
#ifdef CONFIG_FEATURE_TELNETD_INETD
            if (ts->size1 < BUFSIZE && FD_ISSET(ts->sockfd_read, &rdfdset)) {
#else /* CONFIG_FEATURE_TELNETD_INETD */
            if (ts->size1 < BUFSIZE && FD_ISSET(ts->sockfd, &rdfdset)) {
#endif /* CONFIG_FEATURE_TELNETD_INETD */
                /* Read from socket to buffer 1. */
                    
                maxlen = MIN(BUFSIZE - ts->rdidx1,
                        BUFSIZE - ts->size1);
#ifdef CONFIG_FEATURE_TELNETD_INETD
                r = read(ts->sockfd_read, ts->buf1 + ts->rdidx1, maxlen);
                if (!r || (r < 0 && errno != EINTR))
                {
                    syslog_debug(LOG_ERR, "----failed------(%s)-%d----\n",__FUNCTION__, __LINE__);
                    exit(0);
                }
#else /* CONFIG_FEATURE_TELNETD_INETD */
                r = read(ts->sockfd, ts->buf1 + ts->rdidx1, maxlen);

                 syslog_debug(LOG_ERR, "----------3---r sock (%s)---r(%d)---------\n", ts->buf1 + ts->rdidx1, r);

                if (!r || (r < 0 && errno != EINTR)) {
                 syslog_debug(LOG_ERR, "----failed------(%s)-%d----\n",__FUNCTION__, __LINE__);

                    free_session(ts);
                    ts = next;
                    continue;
                }
#endif /* CONFIG_FEATURE_TELNETD_INETD */
                if (!*(ts->buf1 + ts->rdidx1 + r - 1)) {
                    r--;
                    if (!r)
                        continue;
                }
                ts->rdidx1 += r;
                ts->size1 += r;
                if (ts->rdidx1 == BUFSIZE)
                    ts->rdidx1 = 0;
            }

            if (ts->size2 < BUFSIZE && FD_ISSET(ts->ptyfd, &rdfdset)) {
                /* Read from pty to buffer 2.  */
             syslog_debug(LOG_ERR, "----------4---r pty(%s)------------\n", ts->buf2 + ts->rdidx2);
                
                maxlen = MIN(BUFSIZE - ts->rdidx2,
                        BUFSIZE - ts->size2);
                r = read(ts->ptyfd, ts->buf2 + ts->rdidx2, maxlen);
                if (!r || (r < 0 && errno != EINTR)) {
                     syslog_debug(LOG_ERR, "----failed------(%s)-%d----\n",__FUNCTION__, __LINE__);
                    
#ifdef CONFIG_FEATURE_TELNETD_INETD
                    exit(0);
#else /* CONFIG_FEATURE_TELNETD_INETD */
                    free_session(ts);
                    ts = next;
                    //strong
                    exit_flag = 1;
                    break;
#endif /* CONFIG_FEATURE_TELNETD_INETD */
                }
                ts->rdidx2 += r;
                ts->size2 += r;
                if (ts->rdidx2 == BUFSIZE)
                    ts->rdidx2 = 0;
            }

            if (ts->size1 == 0) {
                ts->rdidx1 = 0;
                ts->wridx1 = 0;
            }
            if (ts->size2 == 0) {
                ts->rdidx2 = 0;
                ts->wridx2 = 0;
            }
#ifndef CONFIG_FEATURE_TELNETD_INETD
            ts = next;
        }
#endif /* CONFIG_FEATURE_TELNETD_INETD */

        if(exit_flag == 1)
        {
            break;
        }

    } while (1);

    syslog_debug(LOG_ERR,"shell exit, father exit \n");
    //DB("shell exit, father exit \n");
    return 0;
}


int appl_shell (void)
{
    int  fd = open("/dev/ttyS0",  O_RDWR | O_NOCTTY);
    struct termios oldtio, newtio;
    FILE *o_stream = (FILE *)pthread_getspecific(g_clish_ostream_key);

    if (-1 == fd)
    {
	printf("failed to open ttyS0\n");
	return 0;
    }
    
    // Save configuration
    tcgetattr(fd, &oldtio); 
    tcgetattr(fd, &newtio); 

    // IGNPAR ignore PAR, ICRNL takes the input enter as newline
    newtio.c_iflag = IGNPAR | ICRNL;
    newtio.c_lflag = 0;

    tcflush(fd, TCIFLUSH); 
    tcsetattr(fd, TCSANOW, &newtio);

    
    if(fileno(stdout)!=fileno(o_stream))
    {
        fd = fileno(o_stream);
    }
    
    linux_shell(fd);
    
    tcflush(fd, TCIFLUSH); 
    tcsetattr(fd, TCSANOW, &oldtio);
    close (fd);
    return 0;
}
