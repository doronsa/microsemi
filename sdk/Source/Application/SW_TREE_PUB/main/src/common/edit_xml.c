/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "tpm_types.h"
#include "tpm_internal_types.h"
#include "tpm_mng_if.h"
#include "../incl/ezxml.h"
#include "edit_xml.h"
#include "common.h"
#include "../incl/params.h"
#include "params_mng.h"
#include "ponOnuMngIf.h"
#include "ponOnuInternals.h"
//#include "CTC_STACK_comm.h"
#include "../incl/main_cli.h"




bool_t set_tag_value(ezxml_t xml,char* value)
{
	char* temp = NULL;
	if (strcmp(value,"*")==0)
	{
		printf("The current value of tag\t%s is\t%s\n",xml->name, xml->txt);
		return BOOL_TRUE;
	}
	else{
		temp = (char*)malloc(sizeof(xml->txt));
		strcpy(temp,xml->txt);
		xml->txt = value;
		printf("The %s tag value changed\t%s->%s\n",xml->name, temp, xml->txt);
		free(temp);
		return BOOL_TRUE;
	}
	return BOOL_FALSE;
}

bool_t set_attr_value(ezxml_t xml,char* attr_name,char* value)
{
	char* temp = NULL;
	if (strcmp(value,"*")==0)
	{
		printf("The current value of attribue\t%s is\t%s\n",attr_name, ezxml_attr(xml, attr_name));
		return BOOL_TRUE;
	}
	else{
		temp = (char*)malloc(sizeof(ezxml_attr(xml, attr_name)));
		strcpy(temp,ezxml_attr(xml, attr_name));
		ezxml_set_attr(xml,attr_name,value);
		printf("The %s attribute value changed\t%s->%s\n",attr_name, temp, ezxml_attr(xml, attr_name));
		free(temp);
		return BOOL_TRUE;
		}
	return BOOL_FALSE;
}

bool_t setBooleanValue(ezxml_t parentElement, char *childElmName, char *attrName, char* new_value)
{
    ezxml_t     xmlElement;
    const char  *attr_str;
	
	
    if ((xmlElement = ezxml_child(parentElement, childElmName)) == 0)
    {
        printf("ezxml_get() of %s failed", childElmName);
        return BOOL_FALSE;
    }
    else
    {
        if ((attr_str = ezxml_attr(xmlElement, attrName)) != NULL)
        {
            if (strcmp(new_value,"1")==0) 
            {
                return(set_attr_value(xmlElement,attrName,"true"));
            }
            else if (strcmp(new_value,"0")==0) 
            {
                return(set_attr_value(xmlElement,attrName,"false"));
            }
            else
            {
                printf("attribute %s has invalid value '%s'", attrName, attr_str);
                return BOOL_FALSE;
            }
        }
        else
        {
            printf("ezxml_get() of %s failed", attrName);
            return BOOL_FALSE;
        }
    }

    return BOOL_TRUE;
}


bool_t write_to_file(char* str,char* filename)
{
	int fd1;
	int fd2;
	char oldFileName[100];
	ezxml_t xmlHead;
	char* oldFileStr = NULL;
	strcpy(oldFileName,filename);
	strcat(oldFileName,".old");
	fd1 = open(oldFileName,O_CREAT|O_WRONLY|O_TRUNC, 0 );
	if (fd1<0)
	{
		printf("can't open new file\n");
		return BOOL_FALSE;
	}
	xmlHead = ezxml_parse_file(filename);
	oldFileStr = ezxml_toxml(xmlHead);
	write(fd1,(void*)oldFileStr, strlen(oldFileStr));
	close(fd1);
	ezxml_free(xmlHead);
	fd2 = open(filename,O_CREAT|O_WRONLY|O_TRUNC, 0 );
	if (fd1<2)
	{
		printf("can't open new file\n");
		return BOOL_FALSE;
	}
	write(fd2,(void*)str, strlen(str));
	close(fd2);
	return BOOL_TRUE;
}

//***************************************************************************
//EPON & GPON
//***************************************************************************

void Update_help (const clish_shell_t    *instance,const lub_argv_t       *argv)
{
    us_enabled_t        force;
	tpm_init_pon_type_t wanTech;
	char* choise = NULL;
	get_wan_tech_param(&wanTech, &force);
	choise = (char*)lub_argv__get_arg(argv,0);
	switch (wanTech) 
    {
        case TPM_EPON:
			if (strcmp(choise,"all")==0)
			{
				printUpdateHelpALL();
				printUpdateHelpEpon();
				printUpdateHelpTpmEpon();
			}
			else if (strcmp(choise,"pon")==0)
			{
				printUpdateHelpALL();
				printUpdateHelpEpon();
			}
			else if (strcmp(choise,"tpm")==0)
				printUpdateHelpTpmEpon();
			else if (strcmp(choise,"omci")==0)
				printf("No omci for Epon\n");
			else
				printf("wrong input\n");
            break;

        case TPM_GPON:
			if (strcmp(choise,"all")==0)
			{
				printUpdateHelpALL();
				printUpdateHelpGpon();
				printUpdateHelpTpmGpon();
				printUpdateHelpOmci();
			}
			else if (strcmp(choise,"pon")==0)
			{
				printUpdateHelpALL();
				printUpdateHelpGpon();
			}
			else if (strcmp(choise,"tpm")==0)
				printUpdateHelpTpmGpon();
			else if (strcmp(choise,"omci")==0)
				printUpdateHelpOmci();
			else
				printf("wrong input");
            break;
    }/* switch */
}

void printUpdateHelpALL()
{
	printf("+------------------------------------------------------------------------------------------------+\n");
	printf("|*************************Commands for pon_type_xml_cfg_file.xml*********************************|\n");
	printf("+------------------------------------------------------------------------------------------------+\n");
	printf("|Command                            inputs                                                       |\n");
	printf("|=======                            ======                                                       |\n");
	printf("|update epon_WAN_type               1. value     - new value for WAN/WAN_type                    |\n");  
	printf("|                                                                                                |\n");	
	printf("|update epon_WAN_type_force         1. value     - new value for WAN/WAN_type_force              |\n"); 
	printf("+------------------------------------------------------------------------------------------------+\n\n");
}


void printUpdateHelpEpon()
{
	printf("+------------------------------------------------------------------------------------------------+\n");
	printf("|*********************************Commands for epon_xml_cfg_file*********************************|\n");
	printf("+------------------------------------------------------------------------------------------------+\n");
	printf("|Command                            inputs                                                       |\n");
	printf("|=======                            ======                                                       |\n");
	printf("|update epon_connect_tpm_params     1. value     - new value for EPON/EPON_connect_tpm           |\n");
	printf("|                                                                                                |\n");
	printf("|update epon_def_params             1. xvr_pol   - new value for EPON/EPON_XVR_polarity          |\n");
	printf("|                                   2. dg_pol    - new value for EPON/EPON_DG_polarity           |\n");
	printf("|                                                                                                |\n");
	printf("|update epon_oam_rx_q_params        1. value     - new value for EPON/EPON_OAM_RX_queue          |\n");
	printf("|                                                                                                |\n");
	printf("|update epon_oam_txq_params         1. id        - choosen LLID                                  |\n");
	printf("|                                   2. num       - new value for EPON/EPON_OAM_TX_queue/LLID/num |\n");
	printf("|                                                                                                |\n");
	printf("|update epon_igmp_config_params     1. value     - new value for EPON/EPON_IGMP_supported        |\n");  
	printf("+------------------------------------------------------------------------------------------------+\n\n");
}

void printUpdateHelpTpmEpon()
{
	printf("+----------------------------------------------------------------------------------------------------------------------------------------------------------+\n");
	printf("|************************************************************Commands for tpm_xml_cfg_file_epon************************************************************|\n");
	printf("+----------------------------------------------------------------------------------------------------------------------------------------------------------+\n");
	printf("|Command                                         inputs                                                                                                    |\n");
	printf("|=======                                         ======                                                                                                    |\n");
	printf("|update tpm_epon_pppoe_add_enable                1. new_value                 - new value for traffic_setting/pppoe_add_enable                             |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_num_vlan_tags                   1. new_value                 - new value for traffic_setting/num_vlan_tags                                |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_ttl_illegal_action              1. new_value                 - new value for traffic_setting/ttl_illegal_action                           |\n"); 
	printf("|                                                                                                                                                          |\n");	
	printf("|update epon_ety_dsa_enable                      1. new_value                 - new value for traffic_setting/ety_dsa_enable                               |\n"); 
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_tcp_flag_check                  1. new_value                 - new value for traffic_setting/tcp_flag_check                               |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_cpu_trap_rx_queue               1. new_value                 - new value for traffic_setting/cpu_trap_rx_queue                            |\n"); 
	printf("|                                                                                                                                                          |\n");
	printf("|update tpm_epon_mtu_setting_enabled             1. new_value                 - new value for mtu_setting/mtu_setting_enabled                              |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_ipv4_mtu_ds                     1. new_value                 - new value for mtu_setting/ipv4_mtu_ds                                      |\n");
	printf("|                                                                                                                                                          |\n");
	printf("|update tpm_epon_ipv4_pppoe_mtu_us               1. new_value                 - new value for mtu_setting/ipv4_pppoe_mtu_us                                |\n");
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_igmap_snoop_enabled             1. new_value                 - new value for igmp_snoop/enabled                                           |\n");
	printf("|                                                                                                                                                          |\n");
	printf("|update tpm_epon_igmp_cpu_rx_queue               1. new_value                 - new value for igmp_snoop/igmp_cpu_rx_queue                                 |\n");
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_igmp_frwrd_mode_wan             1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_wan                               |\n");
	printf("|                                                                                                                                                          |\n");
	printf("|update tpm_epon_igmp_frwrd_mode_uni0            1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_uni0                              |\n");
	printf("|                                                                                                                                                          |\n");
	printf("|update tpm_epon_igmp_frwrd_mode_uni1            1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_uni1                              |\n");
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_igmp_frwrd_mode_uni2            1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_uni2                              |\n"); 
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_igmp_frwrd_mode_uni3            1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_uni3                              |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_igmp_frwrd_mode_uni4            1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_uni4                              |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_mc_pppoe_enable                 1. new_value                 - new value for multicast/mc_pppoe_enable                                    |\n"); 
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_mc_per_uni_vlan_xlat            1. new_value                 - new value for multicast/mc_per_uni_vlan_xlat                               |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_mc_filter_mode                  1. new_value                 - new value for multicast/mc_filter_mode                                     |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_mc_hwf_queue                    1. new_value                 - new value for multicast/mc_hwf_queue                                       |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_mc_cpu_queue                    1. new_value                 - new value for multicast/mc_cpu_queue                                       |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_gmac_conn_params                1. new_value                 - new value for port_init/gmac_config/pon_config/num_tcont_llid              |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_gmac_mh_en_params               1. gmac0_mh_en               - new value for port_init/gmac_config/gmac_mh_en/gmac0_mh_en                 |\n");  
	printf("|                                                2. gmac1_mh_en               - new value for port_init/gmac_config/gmac_mh_en/gmac0_mh_en                 |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_gmac_pool_bufs_params           1. id                        - choosen tag                                                                |\n");  
	printf("|                                                2. large_pkt_pool_bufs       - new value for port_init/gmac_config/gmac_bm_buffers/large_pkt_pool_bufs    |\n");  
	printf("|                                                3. small_pkt_pool_bufs       - new value for port_init/gmac_config/gmac_bm_buffers/large_pkt_pool_bufs    |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_gmac_rxq_params                 1. gmac id                   - choosen gmac tag                                                           |\n");  
	printf("|                                                2. queue id                  - choosen queue tag                                                          |\n");  
	printf("|                                                3. queue size                - new value for port_init/gmac_config/gmac_rx_queues/gmac id/queue size      |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_gmac_tx_params                  1. tx_mod id                 - choosen tx_mod tag                                                         |\n");  
	printf("|                                                2. queue id                  - choosen queue tag                                                          |\n");  
	printf("|                                                3. sched_method              - new value for port_init/tx_module_parameters/tx_mod/queue_map/sched_method |\n");  
	printf("|                                                4. owner                     - new value for port_init/tx_module_parameters/tx_mod/queue_map/owner        |\n");  
	printf("|                                                5. owner_q_num               - new value for port_init/tx_module_parameters/tx_mod/queue_map/owner_q_num  |\n");  
	printf("|                                                6. size                      - new value for port_init/tx_module_parameters/tx_mod/queue_map/size         |\n");  
	printf("|                                                7. weight                    - new value for port_init/tx_module_parameters/tx_mod/queue_map/weight       |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_config_mode_param               1. new_value                 - new value for TPM/GET_config_mode                                          |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_validation_enabled_config_param 1. new_value                 - new value for TPM/validation_enable                                        |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_cfg_pnc_parse_param             1. new_value                 - new value for TPM/pnc_config                                               |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_ds_mh_config_param              1. new_value                 - new value for TPM/ds_mh_set                                                |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_cpu_wan_egr_loopback            1. new_value                 - new value for TPM/cpu_wan_egr_loopback                                     |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_gmac1_virt_uni                  1. new_value                 - new value for TPM/gmac1_virt_uni                                           |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_default_vlan_tpid_params        1. id                        - choosen id                                                                 |\n");  
	printf("|                                                2. type                      - new value for TPM/vlan_filter_tpid/filter_tpid/type                        |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_trace_debug_info                1. new_value                 - new value for TPM/trace_debug_info                                         |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_mod_tpid_params                 1. id                        - choosen tag                                                                |\n");  
	printf("|                                                2. type                      - new value for TPM/vlan_mod_tpid/mod_tpid/type                              |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_chain_params                    1. id                        - choosen tag                                                                |\n");  
	printf("|                                                2. type                      - new value for TPM/modification/chain_parameters/chain/type                 |\n");  
	printf("|                                                3. num                       - new value for TPM/modification/chain_parameters/chain/num                  |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_udp_checksum_use_init           1. new_value                 -  new value for TPM/modification/udp_checksum_use_init                      |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_udp_checksum_calc               1. new_value                 -  new value for TPM/modification/udp_checksum_calc                          |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_epon_range_params                    1. num                       - choosen tag                                                                |\n");  
	printf("|                                                2. type                      - new value for TPM/PnC/range_parameters/range/type                          |\n");  
	printf("|                                                3. size                      - new value for TPM/PnC/range_parameters/range/size                          |\n");  
	printf("|                                                4. cntr_grp                  - new value for TPM/PnC/range_parameters/range/cntr_grp                      |\n");  
	printf("|                                                5. lu_mask                   - new value for TPM/PnC/range_parameters/range/lu_mask                       |\n");  
	printf("|                                                6. min_reset_level           - new value for TPM/PnC/range_parameters/range/min_reset_level               |\n"); 
	printf("|                                                                                                                                                          |\n");		
	printf("|update tpm_epon_catch_all_pkt_action            1. new_value                 -  new value for TPM/PnC/catch_all_pkt_action                                |\n");  
	printf("+----------------------------------------------------------------------------------------------------------------------------------------------------------+\n\n");
}

void printUpdateHelpOmci()
{
	printf("+---------------------------------------------------------------------------------------------------------------------+\n");
	printf("|*******************************************Commands for omci_xml_cfg_file********************************************|\n");
	printf("+---------------------------------------------------------------------------------------------------------------------+\n");
	printf("|Command                                   inputs                                                                     |\n");
	printf("|=======                                   ======                                                                     |\n");
	printf("|update omci_etype_param                   1. value       - new value for OMCI/OMCI_ETY                               |\n");  
	printf("|                                                                                                                     |\n");		
	printf("|update omci_HW_calls                      1. value       - new value for OMCI/HW_Calls                               |\n");  //***************************
	printf("|                                                                                                                     |\n");		
	printf("|update omci_txq_param                     1. value      - new value for OMCI/CPU_queue                               |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_voice_support                 1. value      - new value for OMCI_MIB/voiceSupport/enabled                |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_voip_Support                  1. value      - new value for OMCI_MIB/voipSupport/enabled                 |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_voice_support                 1. value      - new value for OMCI_MIB/ipHostDataSupport/enabled           |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_Ip_Host_Data_support          1. value      - new value for OMCI_MIB/voiceSupport/enabled                |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_Voip_Protocol_Sip             1. value      - new value for OMCI_MIB/voipProtocolSip/enabled             |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_Voip_Config_By_Omci           1. value      - new value for OMCI_MIB/voipConfigByOmci/enabled            |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_Ont_Type_Sfu                  1. value      - new value for OMCI_MIB/ontTypeSfu/enabled                  |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_Ethernet_ports                1. value      - new value for OMCI_MIB/ethernetInfo/numPorts               |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_Num_pots                      1. value      - new value for OMCI_MIB/potsInfo/numPorts                   |\n"); 
	printf("|                                                                                                                     |\n");			
	printf("|update omci_Ethernet_Num_ports            1. value      - new value for OMCI_MIB/ethernetSlotInfo/slotNumber         |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_POTS_slot_number              1. value      - new value for OMCI_MIB/potsSlotInfo/slotNumber             |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_ANI_slot_number               1. value      - new value for OMCI_MIB/aniSlotInfo/slotNumber              |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_VEIP_slot_number              1. value      - new value for OMCI_MIB/veipSlotInfo/slotNumber             |\n"); 
	printf("|                                                                                                                     |\n");		
	printf("|update omci_first_Non_Omci_Tcont_Info     1. value      - new value for OMCI_MIB/firstNonOmciTcontInfo/tcontId       |\n"); 
	printf("+---------------------------------------------------------------------------------------------------------------------+\n\n");
}

void printUpdateHelpGpon()
{
	printf("+--------------------------------------------------------------------------------------------------+\n");
	printf("|**********************************Commands for gpon_xml_cfg_file**********************************|\n");
	printf("+--------------------------------------------------------------------------------------------------+\n");
	printf("|Command                            inputs                                                         |\n");
	printf("|=======                            ======                                                         |\n");
	printf("|update gpon_serial_num             1. serial_num  - new value for PON/PON_serial_num              |\n"); 
	printf("|                                                                                                  |\n");	
	printf("|update gpon_default_sn_src         1. sn_src      - new value for PON/PON_serial_src              |\n"); 
	printf("|                                                                                                  |\n");		
	printf("|update gpon_default_password       1. password    - new value for PON/PON_passwd                  |\n"); 
	printf("|                                                                                                  |\n");		
	printf("|update gpon_default_dis_sn         1. dis         - new value for PON/PON_dis_sn                  |\n"); 	
	printf("|                                                                                                  |\n");		
	printf("|update gpon_default_gem            1. gem         - new value for PON/PON_gem_reset               |\n"); 	
	printf("|                                                                                                  |\n");		
	printf("|update gpon_default_tcont          1. tcont       - new value for PON/PON_tcont_reset             |\n"); 	
	printf("|                                                                                                  |\n");		
	printf("|update gpon_dg_polarity            1. dg         - new value for PON/PON_DG_polarity              |\n"); 	
	printf("|                                                                                                  |\n");		
	printf("|update gpon_default_xvr_pol        1. xvr_pol    - new value for PON/PON_XVR_polarity             |\n");
	printf("+--------------------------------------------------------------------------------------------------+\n\n");
}


void printUpdateHelpTpmGpon()
{
	printf("+----------------------------------------------------------------------------------------------------------------------------------------------------------+\n");
	printf("|************************************************************Commands for tpm_xml_cfg_file_gpon************************************************************|\n");
	printf("+----------------------------------------------------------------------------------------------------------------------------------------------------------+\n");
	printf("|Command                                         inputs                                                                                                    |\n");
	printf("|=======                                         ======                                                                                                    |\n");
	printf("|update tpm_gpon_pppoe_add_enable                1. new_value                 - new value for traffic_setting/pppoe_add_enable                             |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_num_vlan_tags                   1. new_value                 - new value for traffic_setting/num_vlan_tags                                |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_ttl_illegal_action              1. new_value                 - new value for traffic_setting/ttl_illegal_action                           |\n"); 
	printf("|                                                                                                                                                          |\n");	
	printf("|update epon_ety_dsa_enable                      1. new_value                 - new value for traffic_setting/ety_dsa_enable                               |\n");
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_tcp_flag_check                  1. new_value                 - new value for traffic_setting/tcp_flag_check                               |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_default_vlan_tpid_params        1. id                        - choosen id                                                                 |\n");  
	printf("|                                                2. type                      - new value for TPM/vlan_filter_tpid/filter_tpid/type                        |\n");  
	printf("|update tpm_gpon_cpu_trap_rx_queue               1. new_value                 - new value for traffic_setting/cpu_trap_rx_queue                            |\n"); 
	printf("|                                                                                                                                                          |\n");
	printf("|update tpm_gpon_mtu_setting_enabled             1. new_value                 - new value for mtu_setting/mtu_setting_enabled                              |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_ipv4_mtu_ds                     1. new_value                 - new value for mtu_setting/ipv4_mtu_ds                                      |\n");
	printf("|                                                                                                                                                          |\n");
	printf("|update tpm_gpon_ipv4_pppoe_mtu_us               1. new_value                 - new value for mtu_setting/ipv4_pppoe_mtu_us                                |\n");
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_igmap_snoop_enabled             1. new_value                 - new value for igmp_snoop/enabled                                           |\n");
	printf("|                                                                                                                                                          |\n");
	printf("|update tpm_gpon_igmp_cpu_rx_queue               1. new_value                 - new value for igmp_snoop/igmp_cpu_rx_queue                                 |\n");
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_igmp_frwrd_mode_wan             1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_wan                               |\n");
	printf("|                                                                                                                                                          |\n");
	printf("|update tpm_gpon_igmp_frwrd_mode_uni0            1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_uni0                              |\n");
	printf("|                                                                                                                                                          |\n");
	printf("|update tpm_gpon_igmp_frwrd_mode_uni1            1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_uni1                              |\n");
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_igmp_frwrd_mode_uni2            1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_uni2                              |\n"); 
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_igmp_frwrd_mode_uni3            1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_uni3                              |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_igmp_frwrd_mode_uni4            1. new_value                 - new value for igmp_snoop/igmp_frwrd_mode_uni4                              |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_mc_pppoe_enable                 1. new_value                 - new value for multicast/mc_pppoe_enable                                    |\n"); 
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_mc_per_uni_vlan_xlat            1. new_value                 - new value for multicast/mc_per_uni_vlan_xlat                               |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_mc_filter_mode                  1. new_value                 - new value for multicast/mc_filter_mode                                     |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_mc_hwf_queue                    1. new_value                 - new value for multicast/mc_hwf_queue                                       |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_mc_cpu_queue                    1. new_value                 - new value for multicast/mc_cpu_queue                                       |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_gmac_conn_params                1. new_value                 - new value for port_init/gmac_config/pon_config/num_tcont_llid              |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_gmac_mh_en_params               1. gmac0_mh_en               - new value for port_init/gmac_config/gmac_mh_en/gmac0_mh_en                 |\n");  
	printf("|                                                2. gmac1_mh_en               - new value for port_init/gmac_config/gmac_mh_en/gmac0_mh_en                 |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_gmac_pool_bufs_params           1. id                        - choosen tag                                                                |\n");  
	printf("|                                                2. large_pkt_pool_bufs       - new value for port_init/gmac_config/gmac_bm_buffers/large_pkt_pool_bufs    |\n");  
	printf("|                                                3. small_pkt_pool_bufs       - new value for port_init/gmac_config/gmac_bm_buffers/large_pkt_pool_bufs    |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_gmac_rxq_params                 1. gmac id                   - choosen gmac tag                                                           |\n");  
	printf("|                                                2. queue id                  - choosen queue tag                                                          |\n");  
	printf("|                                                3. queue size                - new value for port_init/gmac_config/gmac_rx_queues/gmac id/queue size      |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_gmac_tx_params                  1. tx_mod id                 - choosen tx_mod tag                                                         |\n");  
	printf("|                                                2. queue id                  - choosen queue tag                                                          |\n");  
	printf("|                                                3. sched_method              - new value for port_init/tx_module_parameters/tx_mod/queue_map/sched_method |\n");  
	printf("|                                                4. owner                     - new value for port_init/tx_module_parameters/tx_mod/queue_map/owner        |\n");  
	printf("|                                                5. owner_q_num               - new value for port_init/tx_module_parameters/tx_mod/queue_map/owner_q_num  |\n");  
	printf("|                                                6. size                      - new value for port_init/tx_module_parameters/tx_mod/queue_map/size         |\n");  
	printf("|                                                7. weight                    - new value for port_init/tx_module_parameters/tx_mod/queue_map/weight       |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_config_mode_param               1. new_value                 - new value for TPM/GET_config_mode                                          |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_validation_enabled_config_param 1. new_value                 - new value for TPM/validation_enable                                        |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_cfg_pnc_parse_param             1. new_value                 - new value for TPM/pnc_config                                               |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_ds_mh_config_param              1. new_value                 - new value for TPM/ds_mh_set                                                |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_cpu_wan_egr_loopback            1. new_value                 - new value for TPM/cpu_wan_egr_loopback                                     |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_gmac1_virt_uni                  1. new_value                 - new value for TPM/gmac1_virt_uni                                           |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_default_vlan_tpid_params        1. vlan1_tpid                 - new value for TPM/vlan1_tpid                                              |\n");  
	printf("|                                                2. vlan2_tpid                 - new value for TPM/vlan2_tpid                                              |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_trace_debug_info                1. new_value                 - new value for TPM/trace_debug_info                                         |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_mod_tpid_params                 1. id                        - choosen tag                                                                |\n");  
	printf("|                                                2. type                      - new value for TPM/vlan_mod_tpid/mod_tpid/type                              |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_chain_params                    1. id                        - choosen tag                                                                |\n");  
	printf("|                                                2. type                      - new value for TPM/modification/chain_parameters/chain/type                 |\n");  
	printf("|                                                3. num                       - new value for TPM/modification/chain_parameters/chain/num                  |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_udp_checksum_use_init           1. new_value                 -  new value for TPM/modification/udp_checksum_use_init                      |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_udp_checksum_calc               1. new_value                 -  new value for TPM/modification/udp_checksum_calc                          |\n");  
	printf("|                                                                                                                                                          |\n");	
	printf("|update tpm_gpon_range_params                    1. num                       - choosen tag                                                                |\n");  
	printf("|                                                2. type                      - new value for TPM/PnC/range_parameters/range/type                          |\n");  
	printf("|                                                3. size                      - new value for TPM/PnC/range_parameters/range/size                          |\n");  
	printf("|                                                4. cntr_grp                  - new value for TPM/PnC/range_parameters/range/cntr_grp                      |\n");  
	printf("|                                                5. lu_mask                   - new value for TPM/PnC/range_parameters/range/lu_mask                       |\n");  
	printf("|                                                6. min_reset_level           - new value for TPM/PnC/range_parameters/range/min_reset_level               |\n"); 
	printf("|                                                                                                                                                          |\n");		
	printf("|update tpm_gpon_catch_all_pkt_action            1. new_value              -  new value for TPM/PnC/catch_all_pkt_action                                   |\n");  
	printf("+----------------------------------------------------------------------------------------------------------------------------------------------------------+\n\n");
}

bool_t set_WAN_type (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_WAN); // parse the wanted XML file

    if (xmlHead != NULL) // if not failed
	{
		xmlElement = ezxml_child(xmlHead, US_XML_WAN_E); // go to child tag
		if (xmlElement != NULL) // if not failed
		{
			subElement = ezxml_child(xmlElement, US_XML_WAN_TYPE_E); // go to child tag
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0)); // set the tag to the new value
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_WAN)== BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", US_XML_WAN_TYPE_E, US_XML_CFG_FILE_WAN);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_WAN_E, US_XML_CFG_FILE_WAN);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s isn't exist\n",US_XML_CFG_FILE_WAN);
		rcode = BOOL_FALSE;
		}

	return rcode;
}


bool_t set_WAN_type_force (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_WAN);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_WAN_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, US_XML_WAN_TYPE_FORCE_E);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_WAN)== BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", US_XML_WAN_TYPE_FORCE_E, US_XML_CFG_FILE_WAN);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_WAN_E, US_XML_CFG_FILE_WAN);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s isn't exist\n",US_XML_CFG_FILE_WAN);
		rcode = BOOL_FALSE;
		}
return rcode;
}



//***************************************************************************
//EPON
//***************************************************************************

bool_t set_epon_connect_tpm_params (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_EPON_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, US_XML_EPON_CONNECT_E);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_EPON)== BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_CONNECT_E, US_XML_CFG_FILE_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s isn't exist\n",US_XML_CFG_FILE_EPON);
		rcode = BOOL_FALSE;
		}
return rcode;
}


bool_t set_epon_def_params (const clish_shell_t    *instance,
                            const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement1,subElement2;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_EPON_E);
		if (xmlElement != NULL)
		{
			subElement1 = ezxml_child(xmlElement, US_XML_EPON_XVR_POL_E);
			subElement2 = ezxml_child(xmlElement, US_XML_EPON_DG_POL_E);
			if (subElement1 != NULL && subElement2 != NULL)
			{
				set_tag_value(subElement1,(char*)lub_argv__get_arg(argv,0));
				set_tag_value(subElement2,(char*)lub_argv__get_arg(argv,1));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_EPON)== BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s or %s in XML config. file %s\n", US_XML_EPON_XVR_POL_E,US_XML_EPON_DG_POL_E, US_XML_CFG_FILE_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s isn't exist\n",US_XML_CFG_FILE_EPON);
		rcode = BOOL_FALSE;
		}
return rcode;
}


bool_t set_epon_oam_rx_q_params (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_EPON_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, US_XML_EPON_OAM_RXQ_E);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_EPON)== BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_OAM_RXQ_E, US_XML_CFG_FILE_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s isn't exist\n",US_XML_CFG_FILE_EPON);
		rcode = BOOL_FALSE;
		}
return rcode;
}


bool_t set_epon_igmp_config_params (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_EPON_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, US_XML_EPON_IGMP_SUPP_E);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_EPON)== BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_IGMP_SUPP_E, US_XML_CFG_FILE_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s isn't exist\n",US_XML_CFG_FILE_EPON);
		rcode = BOOL_FALSE;
		}
return rcode;
}

bool_t set_epon_silence_mode (const clish_shell_t    *instance,
                              const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_EPON_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, US_XML_EPON_SILENCE_E);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_EPON)== BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_SILENCE_E, US_XML_CFG_FILE_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s isn't exist\n",US_XML_CFG_FILE_EPON);
		rcode = BOOL_FALSE;
		}
return rcode;
}


bool_t set_epon_oam_txq_params (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
	int         Flag = 0;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_EPON_E, 0, US_XML_EPON_OAM_TXQ_E, -1);
		if (xmlElement != NULL)
		{
			for (subElement = ezxml_child(xmlElement, US_XML_EPON_LLID_E); subElement; subElement = subElement->next) // serach for the wanted id number
				{
					if ((subElement == NULL) || (strcmp(ezxml_attr(subElement,US_XML_ID_ATTR),(char*)lub_argv__get_arg(argv,0))==0) )
						break;
				}
			
			if (subElement != NULL)
			{
				set_attr_value(subElement,US_XML_NUM_ATTR,(char*)lub_argv__get_arg(argv,1));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_EPON)== BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find wanted id %s in XML config. file %s\n", (char*)lub_argv__get_arg(argv,0), US_XML_CFG_FILE_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_EPON_E, US_XML_CFG_FILE_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s isn't exist\n",US_XML_CFG_FILE_EPON);
		rcode = BOOL_FALSE;
		}
return rcode;
}

//***************************************************************************************************
//GPON
//***************************************************************************************************

bool_t  set_gpon_serial_num (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_PON_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, US_XML_PON_SN_E);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_GPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", US_XML_PON_SN_E, US_XML_CFG_FILE_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_PON_E, US_XML_CFG_FILE_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s isn't exist\n",US_XML_CFG_FILE_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t set_gpon_def_password (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       new_value = NULL;
    new_value = (char*)lub_argv__get_arg(argv,0);
	return(set_gpon_def_params(1,new_value));
}

bool_t set_gpon_def_dis (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       new_value = NULL;
    new_value = (char*)lub_argv__get_arg(argv,0);
	return(set_gpon_def_params(2,new_value));
}

bool_t set_gpon_def_gem (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       new_value = NULL;
    new_value = (char*)lub_argv__get_arg(argv,0);
	return(set_gpon_def_params(3,new_value));
}

bool_t set_gpon_def_tcont (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       new_value = NULL;
    new_value = (char*)lub_argv__get_arg(argv,0);
	return(set_gpon_def_params(4,new_value));
}

bool_t set_gpon_def_sn_src (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       new_value = NULL;
    new_value = (char*)lub_argv__get_arg(argv,0);
	return(set_gpon_def_params(5,new_value));
}

bool_t set_gpon_def_xvr_pol (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       new_value = NULL;
    new_value = (char*)lub_argv__get_arg(argv,0);
	return(set_gpon_def_params(6,new_value));
}

bool_t set_gpon_def_params (int choise, char* new_value)
{

    ezxml_t     xmlHead;
    ezxml_t     xmlElement;
    ezxml_t     subElement;
	char*		next_tag = NULL;
	char*       str = NULL;
    bool_t      rcode = BOOL_TRUE;
	
	switch(choise)
		{
		case 1:
			next_tag = US_XML_PON_PASSWD_E;
			// pswd
			break;
		case 2:
			next_tag = US_XML_PON_DIS_SN_E;
			// dis
			break;
		case 3:
			next_tag = US_XML_PON_GEM_RST_E;
			// gem
			break;
		case 4:
			next_tag = US_XML_PON_TCONT_RST_E;
			// tcont
			break;
		case 5:
			next_tag = US_XML_PON_SN_SRC_E;
			// sn_src
			break;
		case 6:
			next_tag = US_XML_PON_XVR_POL_E;
			// xvr_pol
			break;
		}

	    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_PON_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, next_tag);
			if (subElement != NULL)
			{
				set_tag_value(subElement,new_value);
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_GPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", next_tag, US_XML_CFG_FILE_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_PON_E, US_XML_CFG_FILE_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s isn't exist\n",US_XML_CFG_FILE_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}




bool_t set_gpon_dg_polarity (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_PON_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, US_XML_PON_DG_POL_E);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_GPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", US_XML_PON_DG_POL_E, US_XML_CFG_FILE_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_PON_E, US_XML_CFG_FILE_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s isn't exist\n",US_XML_CFG_FILE_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}


//***************************************************************************************************
//OMCI
//***************************************************************************************************
bool_t  update_omci_etype_param (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_OMCI_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, US_XML_OMCI_ETY_E);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", US_XML_OMCI_ETY_E, US_XML_CFG_FILE_OMCI);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_OMCI_E, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  update_omci_HW_cells (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_OMCI_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, ELM_OMCI_HW_CALLS);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_HW_CALLS, US_XML_CFG_FILE_OMCI);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_OMCI_E, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  update_omci_txq_param (const clish_shell_t    *instance,
                            const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_OMCI_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, US_XML_CPU_Q_E);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", US_XML_CPU_Q_E, US_XML_CFG_FILE_OMCI);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_OMCI_E, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}


bool_t  setOmciStartupVoiceSupport (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			setBooleanValue(xmlElement, ELM_voiceSupport, ATTR_enabled, (char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}


bool_t  setOmciStartupVoipSupport  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			setBooleanValue(xmlElement, ELM_voipSupport, ATTR_enabled, (char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}


bool_t  setOmciStartupIpHostDataSupport  (const clish_shell_t    *instance,
                                          const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			setBooleanValue(xmlElement, ELM_ipHostDataSupport, ATTR_enabled, (char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  setOmciStartupVoipConfigByOmci  (const clish_shell_t    *instance,
                                          const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			setBooleanValue(xmlElement, ELM_voipConfigByOmci, ATTR_enabled, (char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  setOmciStartupVoipProtocolSip  (const clish_shell_t    *instance,
                                          const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			setBooleanValue(xmlElement, ELM_voipProtocolSip, ATTR_enabled, (char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  setOmciStartupOntTypeSfu  (const clish_shell_t    *instance,
                                          const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;
    
#if 0 /* masked, to be updated */
    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			setBooleanValue(xmlElement, ELM_ontTypeSfu, ATTR_enabled, (char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}
#endif

	return rcode;
}

bool_t  setOmciCellcap  	(const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			setBooleanValue(xmlElement, ELM_omciCellCap, ATTR_enabled, (char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}


bool_t  setOmciStartupEthernetports  (const clish_shell_t    *instance,
                                      const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, ELM_ethernetInfo);
			if (subElement != NULL)
			{
				set_attr_value(subElement, ATTR_numPorts,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", ELM_ethernetInfo, US_XML_CFG_FILE_OMCI);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}
	
	
bool_t  setOmciStartupNumPorts  (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, ELM_potsInfo);
			if (subElement != NULL)
			{
				set_attr_value(subElement, ATTR_numPorts,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", ELM_potsInfo, US_XML_CFG_FILE_OMCI);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}


bool_t  setOmciStartupEthernetNumPorts  (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, ELM_ethernetSlotInfo);
			if (subElement != NULL)
			{
				set_attr_value(subElement, ATTR_slotNumber,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", ELM_ethernetSlotInfo, US_XML_CFG_FILE_OMCI);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  setOmciStartupPOTSslotnumber  (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, ELM_potsSlotInfo);
			if (subElement != NULL)
			{
				set_attr_value(subElement, ATTR_slotNumber,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", ELM_potsSlotInfo, US_XML_CFG_FILE_OMCI);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  setOmciStartupVEIPslotnumber  (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, ELM_veipSlotInfo);
			if (subElement != NULL)
			{
				set_attr_value(subElement, ATTR_slotNumber,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", ELM_veipSlotInfo, US_XML_CFG_FILE_OMCI);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}


bool_t  setOmciStartupANIslotnumber  (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, ELM_aniSlotInfo);
			if (subElement != NULL)
			{
				set_attr_value(subElement, ATTR_slotNumber,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", ELM_aniSlotInfo, US_XML_CFG_FILE_OMCI);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  setOmciStartupfirstNonOmciTcontInfo  (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, ELM_firstNonOmciTcontInfo);
			if (subElement != NULL)
			{
				set_attr_value(subElement, ATTR_tcontId,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", ELM_firstNonOmciTcontInfo, US_XML_CFG_FILE_OMCI);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}								 



bool_t  setOmciStartupgemPortPmByApmActivation  (const clish_shell_t    *instance,
                                                 const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_MIB);
		if (xmlElement != NULL)
		{
			setBooleanValue(xmlElement, "gemPortPmByApmActivation", ATTR_enabled, (char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_MIB, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}
//*************************************************************************************
bool_t  setOmciDebugconsoleOutput  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_OMCI);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, ELM_OMCI_DEBUG);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, ELM_consoleOutput);
			if (subElement != NULL)
			{
				set_attr_value(subElement, ATTR_flags,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_OMCI)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n", ELM_consoleOutput, US_XML_CFG_FILE_OMCI);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", ELM_OMCI_DEBUG, US_XML_CFG_FILE_OMCI);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_OMCI);
		rcode = BOOL_FALSE;
		}

	return rcode;
}								 

//***************************************************************************
// EPON TPM
//***************************************************************************
bool_t  set_epon_pnc_range_params  (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
	char*       str = NULL;
	char* 		num = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);
	num = (char*)lub_argv__get_arg(argv,0); // rad 1st element - the choosen num

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0, US_XML_TPM_PNC_E, 0, US_XML_TPM_PNC_RANGE_PARAMS_E, -1);
		if (xmlElement != NULL)
		{
			for (subElement = ezxml_child(xmlElement, US_XML_TPM_PNC_RANGE_E); subElement; subElement = subElement->next) // search for the wanted tag eith the inputed num
				{
				if ((strcmp(ezxml_attr(subElement,US_XML_NUM_ATTR),num)==0) || subElement == NULL) // if not found stay NULL else stay with value
					break;
				}
			if (subElement != NULL)
			{
				set_attr_value(subElement,US_XML_TYPE_ATTR,(char*)lub_argv__get_arg(argv,1)); // change the attr's
				set_attr_value(subElement,US_XML_SIZE_ATTR,(char*)lub_argv__get_arg(argv,2));
				set_attr_value(subElement,"cntr_grp",(char*)lub_argv__get_arg(argv,3));
				set_attr_value(subElement,"lu_mask",(char*)lub_argv__get_arg(argv,4));
				set_attr_value(subElement,US_XML_TPM_MIN_RES_LVL_ATTR,(char*)lub_argv__get_arg(argv,5));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_TPM_PNC_RANGE_E, US_XML_CFG_FILE_TPM_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_PNC_RANGE_PARAMS_E, US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}		

bool_t  set_epon_default_vlan_tpid_params  (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
	char*       str = NULL;
	char*       id = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement1;
    ezxml_t     subElement2;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;
	id = (char*)lub_argv__get_arg(argv,0);
    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
		if (xmlElement != NULL)
		{
			subElement1 = ezxml_child(xmlElement, US_XML_TPM_VLAN_FILTER_TPID_E);
			if (subElement1 != NULL)
			{
				for (subElement2 = ezxml_child(subElement1, US_XML_TPM_FILTER_TPID_E); subElement2; subElement2 = subElement2->next) // search for the wanted tag eith the inputed num
				{
					if ((strcmp(ezxml_attr(subElement2,US_XML_ID_ATTR),id)==0) || subElement2 == NULL) // if not found stay NULL else stay with value
						break;
				}
				if (subElement2 != NULL)
				{
					set_attr_value(subElement2,US_XML_TYPE_ATTR,(char*)lub_argv__get_arg(argv,1)); // change the attr's
					str = ezxml_toxml(xmlHead);
					if (str != NULL)
					{
						if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
						{
							printf("failed to write file\n");
						}
						free(str);
					}
					else{
						printf("Can't perform ezxml_toxml\n");
						rcode = BOOL_FALSE;
						}
				}
				else{
					printf("Failed to find wanted id %s in tag %s in XML config. file %s\n", id, US_XML_TPM_FILTER_TPID_E, US_XML_CFG_FILE_TPM_EPON);
					rcode = BOOL_FALSE;
					}
			}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_epon_ds_mh_config_params  (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement,  "ds_mh_set");
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n",  "ds_mh_set", US_XML_CFG_FILE_TPM_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}								  


bool_t  set_epon_cfg_pnc_parse_param  (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, "pnc_config");
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n",  "pnc_config", US_XML_CFG_FILE_TPM_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										  

bool_t  set_epon_validation_enabled_config_params  (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, "validation_enable");
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n",  "validation_enable", US_XML_CFG_FILE_TPM_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}											   

bool_t  set_epon_config_mode_param  (const clish_shell_t    *instance,
									const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, US_XML_GET_CFG_MODE_E);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n",  US_XML_GET_CFG_MODE_E, US_XML_CFG_FILE_TPM_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}						

bool_t  set_epon_gmac_tx_params  (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       str = NULL;
	char*       txmod_id = NULL;
	char*       queue_id = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement1;
    ezxml_t     subElement2;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);
	
	txmod_id = (char*)lub_argv__get_arg(argv,0); // get the txmod_id and the queue id from the user.
	queue_id= (char*)lub_argv__get_arg(argv,1);
	
    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, US_XML_TX_MOD_PARAMS_E, -1);
		if (xmlElement != NULL)
		{
			for (subElement1 = ezxml_child(xmlElement, US_XML_TX_MOD_E);subElement1;subElement1 = subElement1->next) // search for the wanted tag eith the inputed id
				{
				if (strcmp(ezxml_attr(subElement1, US_XML_ID_ATTR),txmod_id) == 0 || (subElement1 == NULL)) // if not found stay NULL else stay with value
					break;
				}
			if (subElement1 != NULL)
			{
				for (subElement2 = ezxml_get(subElement1, US_XML_QUEUE_MAP_E,0,US_XML_QUEUE_E,-1);subElement2;subElement2 = subElement2->next)
					{
						if ((strcmp(ezxml_attr(subElement2, US_XML_ID_ATTR),queue_id) == 0) || subElement2 == NULL)
						break;
					}
				if (subElement2 != NULL)
					{
					set_attr_value(subElement2,US_XML_QUEUE_SHED_ATTR,(char*)lub_argv__get_arg(argv,2));
					set_attr_value(subElement2,US_XML_QUEUE_OWNER_ATTR,(char*)lub_argv__get_arg(argv,3));
					set_attr_value(subElement2,US_XML_QUEUE_OW_Q_NUM_ATTR,(char*)lub_argv__get_arg(argv,4));
					set_attr_value(subElement2,US_XML_SIZE_ATTR,(char*)lub_argv__get_arg(argv,5));
					set_attr_value(subElement2,US_XML_QUEUE_WEIGHT_ATTR,(char*)lub_argv__get_arg(argv,6));
					str = ezxml_toxml(xmlHead);
					if (str != NULL)
						{
						if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
							{
							printf("failed to write file\n");
							}
						free(str);
						}
					else{
						printf("Can't perform ezxml_toxml\n");
						rcode = BOOL_FALSE;
						}
					}
				else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_QUEUE_E, US_XML_CFG_FILE_TPM_EPON);
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_TX_MOD_E, US_XML_CFG_FILE_TPM_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TX_MOD_PARAMS_E, US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									


bool_t  set_epon_gmac_rxq_params  (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       str = NULL;
	char*       gmac_id = NULL;
	char*       queue_id = NULL; 
    ezxml_t     xmlHead;
    ezxml_t     subElement1;
    ezxml_t     subElement2;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);
	
	gmac_id = (char*)lub_argv__get_arg(argv,0);
	queue_id = (char*)lub_argv__get_arg(argv,1); // get the txmod_id and the queue id from the user.
	
    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, "gmac_config", 0, "gmac_rx_queues", -1);
		if (xmlElement != NULL)
		{
		    for (subElement1 = ezxml_child(xmlElement, US_XML_GMAC_E);(subElement1);subElement1 = subElement1->next) // search for the wanted tag eith the inputed id
				{
				if ((strcmp(ezxml_attr(subElement1, US_XML_ID_ATTR),gmac_id) == 0) || (subElement1 == NULL)) // if not found stay NULL else stay with value
					break;
				}
			if (subElement1 != NULL)
			{
				for (subElement2 = ezxml_child(subElement1, US_XML_QUEUE_E); subElement2; subElement2 = subElement2->next)
					{
						if ((strcmp(ezxml_attr(subElement2, US_XML_ID_ATTR),queue_id) == 0) || subElement2 == NULL)
						break;
					}
				if (subElement2 != NULL)
					{
					set_attr_value(subElement2,US_XML_SIZE_ATTR,(char*)lub_argv__get_arg(argv,2));
					str = ezxml_toxml(xmlHead);
					if (str != NULL)
						{
						if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
							{
							printf("failed to write file\n");
							}
						free(str);
						}
					else{
						printf("Can't perform ezxml_toxml\n");
						rcode = BOOL_FALSE;
						}
					}
				else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_QUEUE_E, US_XML_CFG_FILE_TPM_EPON);
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_GMAC_E, US_XML_CFG_FILE_TPM_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "gmac_rx_queues", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									


bool_t  set_epon_gmac_pool_bufs_params  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    char*       id = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement1;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);
	
	id = (char*)lub_argv__get_arg(argv,0); // get the txmod_id and the queue id from the user.
	
    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, "gmac_config", 0, "gmac_bm_buffers", -1);
		if (xmlElement != NULL)
		{
		    for (subElement1 = ezxml_child(xmlElement, US_XML_GMAC_E);subElement1;subElement1 = subElement1->next)
				{
					if((strcmp(ezxml_attr(subElement1, US_XML_ID_ATTR),id)==0) || (subElement1 == NULL)) // if not found stay NULL else stay with value
					break;
				}
			if (subElement1 != NULL)
			{
				set_attr_value(subElement1,US_XML_LARGE_POOL_BUF_ATTR,(char*)lub_argv__get_arg(argv,1));
				set_attr_value(subElement1,US_XML_SHORT_POOL_BUF_ATTR,(char*)lub_argv__get_arg(argv,2));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
					{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
						{
						printf("failed to write file\n");
						}
					free(str);
					}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_GMAC_E, US_XML_CFG_FILE_TPM_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "gmac_bm_buffers", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									
									
bool_t  set_epon_gmac_mh_en_params  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement1;
    ezxml_t     subElement2;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "port_init", 0, "gmac_config", 0, "gmac_mh_en", -1);
		if (xmlElement != NULL)
		{
			subElement1 = ezxml_child(xmlElement,"gmac0_mh_en");
			subElement2 = ezxml_child(xmlElement,"gmac1_mh_en");
			if (subElement1 != NULL && subElement2 != NULL)
			{
				set_tag_value(subElement1,(char*)lub_argv__get_arg(argv,0));
				set_tag_value(subElement2,(char*)lub_argv__get_arg(argv,1));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s or %s in XML config. file %s\n",  "gmac0_mh_en", "gmac1_mh_en", US_XML_CFG_FILE_TPM_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "gmac_mh_en", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										


bool_t  set_epon_gmac_conn_params  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, "gmac_config", 0,"pon_config", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										


bool_t  set_epon_pppoe_add_enable  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,"pppoe_add_enable", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "pppoe_add_enable", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									


bool_t  set_epon_num_vlan_tags  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,"num_vlan_tags", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "num_vlan_tags", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}												


bool_t  set_epon_ttl_illegal_action  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,"ttl_illegal_action", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "ttl_illegal_action", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									


bool_t  set_epon_tcp_flag_check  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,"tcp_flag_check", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "tcp_flag_check", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										

bool_t  set_epon_cpu_trap_rx_queue  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,"cpu_trap_rx_queue", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "cpu_trap_rx_queue", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}
bool_t  set_epon_ety_dsa_enable    (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,US_XML_ETY_DSA_ENABLE_E, -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_ETY_DSA_ENABLE_E, US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}													

bool_t  set_epon_mtu_setting_enabled  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "mtu_setting", 0,"mtu_setting_enabled", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mtu_setting_enabled", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										


bool_t  set_epon_ipv4_mtu_ds  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "mtu_setting", 0,"ipv4_mtu_ds", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "ipv4_mtu_ds", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}											

bool_t  set_epon_ipv4_mtu_us             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "mtu_setting", 0,"ipv4_mtu_us", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "ipv4_mtu_us", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									
									
bool_t  set_epon_ipv4_pppoe_mtu_us             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "mtu_setting", 0,"ipv4_pppoe_mtu_us", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "ipv4_pppoe_mtu_us", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										


bool_t  set_epon_igmap_snoop_enabled             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,US_XML_ENABLED_E, -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s %s in XML config. file %s\n", "igmp_snoop", US_XML_ENABLED_E, US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}											


bool_t  set_epon_igmp_cpu_rx_queue             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_cpu_rx_queue", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_cpu_rx_queue", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										
								
bool_t  set_epon_igmp_frwrd_mode_wan             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_wan", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_wan", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										


bool_t  set_epon_igmp_frwrd_mode_uni0             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_uni0", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_uni0", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}												

bool_t  set_epon_igmp_frwrd_mode_uni1             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_uni1", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_uni1", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	

bool_t  set_epon_igmp_frwrd_mode_uni2             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_uni2", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_uni2", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	

bool_t  set_epon_igmp_frwrd_mode_uni3             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_uni3", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_uni3", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	


bool_t  set_epon_igmp_frwrd_mode_uni4             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_uni4", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_uni4", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	

bool_t  set_epon_mc_pppoe_enable             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "multicast", 0,"mc_pppoe_enable", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mc_pppoe_enable", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	


bool_t  set_epon_mc_per_uni_vlan_xlat             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "multicast", 0,"mc_per_uni_vlan_xlat", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mc_per_uni_vlan_xlat", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										
									

bool_t  set_epon_mc_filter_mode             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "multicast", 0,"mc_filter_mode", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mc_filter_mode", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	

bool_t  set_epon_mc_hwf_queue             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "multicast", 0,"mc_hwf_queue", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mc_hwf_queue", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	


bool_t  set_epon_mc_cpu_queue             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "multicast", 0,"mc_cpu_queue", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mc_cpu_queue", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_epon_cpu_wan_egr_loopback             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		    xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,"cpu_wan_egr_loopback", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "cpu_wan_egr_loopback", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_epon_gmac1_virt_uni             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,"gmac1_virt_uni", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "gmac1_virt_uni", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_epon_trace_debug_info       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,"trace_debug_info", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "trace_debug_info", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_epon_catch_all_pkt_action       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,US_XML_TPM_PNC_E, 0,"catch_all_pkt_action", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "catch_all_pkt_action", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_epon_udp_checksum_calc       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,US_XML_TPM_MOD_E, 0,"udp_checksum_calc", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "udp_checksum_calc", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_epon_udp_checksum_use_init       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,US_XML_TPM_MOD_E, 0,"udp_checksum_use_init", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "udp_checksum_use_init", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_epon_chain_params       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	char*       id = NULL;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);
	id = (char*)lub_argv__get_arg(argv,0);
	
    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,US_XML_TPM_MOD_E, 0,"chain_parameters", 0,"chain", -1);
		if (xmlElement != NULL)
		{
			for (xmlElement;xmlElement;xmlElement = xmlElement->next)
			{
				if(strcmp(ezxml_attr(xmlElement, US_XML_ID_ATTR),id)==0 || xmlElement == NULL)
					break;
			}
				
			if (xmlElement != NULL)
			{
				set_attr_value(xmlElement,US_XML_TYPE_ATTR,(char*)lub_argv__get_arg(argv,1));
				set_attr_value(xmlElement,US_XML_NUM_ATTR,(char*)lub_argv__get_arg(argv,2));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n",  "chain",  US_XML_CFG_FILE_TPM_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "chain", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										

bool_t  set_epon_mod_tpid_params       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	char*       id = NULL;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_EPON);
	id = (char*)lub_argv__get_arg(argv,0);
	
    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,"vlan_mod_tpid", 0,"mod_tpid", -1);
		if (xmlElement != NULL)
		{
			for (xmlElement;xmlElement;xmlElement = xmlElement->next)
			{
				if(strcmp(ezxml_attr(xmlElement, US_XML_ID_ATTR),id)==0 || xmlElement == NULL)
					break;
			}
				
			if (xmlElement != NULL)
			{
				set_attr_value(xmlElement,US_XML_TYPE_ATTR,(char*)lub_argv__get_arg(argv,1));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_EPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n",  "chain",  US_XML_CFG_FILE_TPM_EPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "chain", US_XML_CFG_FILE_TPM_EPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_EPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									

//***************************************************************************
// GPON TPM
//***************************************************************************
bool_t  set_gpon_pnc_range_params  (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
	char*       str = NULL;
	char* 		num = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);
	num = (char*)lub_argv__get_arg(argv,0); // rad 1st element - the choosen num

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0, US_XML_TPM_PNC_E, 0, US_XML_TPM_PNC_RANGE_PARAMS_E, -1);
		if (xmlElement != NULL)
		{
			for (subElement = ezxml_child(xmlElement, US_XML_TPM_PNC_RANGE_E); subElement; subElement = subElement->next) // search for the wanted tag eith the inputed num
				{
				if ((strcmp(ezxml_attr(subElement,US_XML_NUM_ATTR),num)==0) || subElement == NULL) // if not found stay NULL else stay with value
					break;
				}
			if (subElement != NULL)
			{
				set_attr_value(subElement,US_XML_TYPE_ATTR,(char*)lub_argv__get_arg(argv,1)); // change the attr's
				set_attr_value(subElement,US_XML_SIZE_ATTR,(char*)lub_argv__get_arg(argv,2));
				set_attr_value(subElement,"cntr_grp",(char*)lub_argv__get_arg(argv,3));
				set_attr_value(subElement,"lu_mask",(char*)lub_argv__get_arg(argv,4));
				set_attr_value(subElement,US_XML_TPM_MIN_RES_LVL_ATTR,(char*)lub_argv__get_arg(argv,5));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_TPM_PNC_RANGE_E, US_XML_CFG_FILE_TPM_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_PNC_RANGE_PARAMS_E, US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}		

bool_t  set_gpon_default_vlan_tpid_params  (const clish_shell_t    *instance,
                                            const lub_argv_t       *argv)
{
	char*       str = NULL;
	char*       id = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement1;
    ezxml_t     subElement2;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;
	id = (char*)lub_argv__get_arg(argv,0);
    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
		if (xmlElement != NULL)
		{
			subElement1 = ezxml_child(xmlElement, US_XML_TPM_VLAN_FILTER_TPID_E);
			if (subElement1 != NULL)
			{
				for (subElement2 = ezxml_child(subElement1, US_XML_TPM_FILTER_TPID_E); subElement2; subElement2 = subElement2->next) // search for the wanted tag eith the inputed num
				{
					if ((strcmp(ezxml_attr(subElement2,US_XML_ID_ATTR),id)==0) || subElement2 == NULL) // if not found stay NULL else stay with value
						break;
				}
				if (subElement2 != NULL)
				{
					set_attr_value(subElement2,US_XML_TYPE_ATTR,(char*)lub_argv__get_arg(argv,1)); // change the attr's
					str = ezxml_toxml(xmlHead);
					if (str != NULL)
					{
						if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
						{
							printf("failed to write file\n");
						}
						free(str);
					}
					else{
						printf("Can't perform ezxml_toxml\n");
						rcode = BOOL_FALSE;
						}
				}
				else{
					printf("Failed to find wanted id %s in tag %s in XML config. file %s\n", id, US_XML_TPM_FILTER_TPID_E, US_XML_CFG_FILE_TPM_GPON);
					rcode = BOOL_FALSE;
					}
			}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_gpon_ds_mh_config_params  (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement,  "ds_mh_set");
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n",  "ds_mh_set", US_XML_CFG_FILE_TPM_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}								  

bool_t  set_gpon_ety_dsa_enable    (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,US_XML_ETY_DSA_ENABLE_E, -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_ETY_DSA_ENABLE_E, US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}					

bool_t  set_gpon_cfg_pnc_parse_param  (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, "pnc_config");
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n",  "pnc_config", US_XML_CFG_FILE_TPM_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										  

bool_t  set_gpon_validation_enabled_config_params  (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, "validation_enable");
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n",  "validation_enable", US_XML_CFG_FILE_TPM_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}											   

bool_t  set_gpon_config_mode_param  (const clish_shell_t    *instance,
									const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_child(xmlHead, US_XML_TPM_E);
		if (xmlElement != NULL)
		{
			subElement = ezxml_child(xmlElement, US_XML_GET_CFG_MODE_E);
			if (subElement != NULL)
			{
				set_tag_value(subElement,(char*)lub_argv__get_arg(argv,0));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s in XML config. file %s\n",  US_XML_GET_CFG_MODE_E, US_XML_CFG_FILE_TPM_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}						

bool_t  set_gpon_gmac_tx_params  (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       str = NULL;
	char*       txmod_id = NULL;
	char*       queue_id = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement1;
    ezxml_t     subElement2;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);
	
	txmod_id = (char*)lub_argv__get_arg(argv,0); // get the txmod_id and the queue id from the user.
	queue_id= (char*)lub_argv__get_arg(argv,1);
	
    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, US_XML_TX_MOD_PARAMS_E, -1);
		if (xmlElement != NULL)
		{
			for (subElement1 = ezxml_child(xmlElement, US_XML_TX_MOD_E);subElement1;subElement1 = subElement1->next) // search for the wanted tag eith the inputed id
				{
				if (strcmp(ezxml_attr(subElement1, US_XML_ID_ATTR),txmod_id) == 0 || (subElement1 == NULL)) // if not found stay NULL else stay with value
					break;
				}
			if (subElement1 != NULL)
			{
				for (subElement2 = ezxml_get(subElement1, US_XML_QUEUE_MAP_E,0,US_XML_QUEUE_E,-1);subElement2;subElement2 = subElement2->next)
					{
						if ((strcmp(ezxml_attr(subElement2, US_XML_ID_ATTR),queue_id) == 0) || subElement2 == NULL)
						break;
					}
				if (subElement2 != NULL)
					{
					set_attr_value(subElement2,US_XML_QUEUE_SHED_ATTR,(char*)lub_argv__get_arg(argv,2));
					set_attr_value(subElement2,US_XML_QUEUE_OWNER_ATTR,(char*)lub_argv__get_arg(argv,3));
					set_attr_value(subElement2,US_XML_QUEUE_OW_Q_NUM_ATTR,(char*)lub_argv__get_arg(argv,4));
					set_attr_value(subElement2,US_XML_SIZE_ATTR,(char*)lub_argv__get_arg(argv,5));
					set_attr_value(subElement2,US_XML_QUEUE_WEIGHT_ATTR,(char*)lub_argv__get_arg(argv,6));
					str = ezxml_toxml(xmlHead);
					if (str != NULL)
						{
						if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
							{
							printf("failed to write file\n");
							}
						free(str);
						}
					else{
						printf("Can't perform ezxml_toxml\n");
						rcode = BOOL_FALSE;
						}
					}
				else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_QUEUE_E, US_XML_CFG_FILE_TPM_GPON);
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_TX_MOD_E, US_XML_CFG_FILE_TPM_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TX_MOD_PARAMS_E, US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									


bool_t  set_gpon_gmac_rxq_params  (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
	char*       str = NULL;
	char*       gmac_id = NULL;
	char*       queue_id = NULL; 
    ezxml_t     xmlHead;
    ezxml_t     subElement1;
    ezxml_t     subElement2;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);
	
	gmac_id = (char*)lub_argv__get_arg(argv,0);
	queue_id = (char*)lub_argv__get_arg(argv,1); // get the txmod_id and the queue id from the user.
	
    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, "gmac_config", 0, "gmac_rx_queues", -1);
		if (xmlElement != NULL)
		{
		    for (subElement1 = ezxml_child(xmlElement, US_XML_GMAC_E);(subElement1);subElement1 = subElement1->next) // search for the wanted tag eith the inputed id
				{
				if ((strcmp(ezxml_attr(subElement1, US_XML_ID_ATTR),gmac_id) == 0) || (subElement1 == NULL)) // if not found stay NULL else stay with value
					break;
				}
			if (subElement1 != NULL)
			{
				for (subElement2 = ezxml_child(subElement1, US_XML_QUEUE_E); subElement2; subElement2 = subElement2->next)
					{
						if ((strcmp(ezxml_attr(subElement2, US_XML_ID_ATTR),queue_id) == 0) || subElement2 == NULL)
						break;
					}
				if (subElement2 != NULL)
					{
					set_attr_value(subElement2,US_XML_SIZE_ATTR,(char*)lub_argv__get_arg(argv,2));
					str = ezxml_toxml(xmlHead);
					if (str != NULL)
						{
						if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
							{
							printf("failed to write file\n");
							}
						free(str);
						}
					else{
						printf("Can't perform ezxml_toxml\n");
						rcode = BOOL_FALSE;
						}
					}
				else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_QUEUE_E, US_XML_CFG_FILE_TPM_GPON);
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_GMAC_E, US_XML_CFG_FILE_TPM_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "gmac_rx_queues", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									


bool_t  set_gpon_gmac_pool_bufs_params  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    char*       id = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement1;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);
	
	id = (char*)lub_argv__get_arg(argv,0); // get the txmod_id and the queue id from the user.
	
    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, "gmac_config", 0, "gmac_bm_buffers", -1);
		if (xmlElement != NULL)
		{
		    for (subElement1 = ezxml_child(xmlElement, US_XML_GMAC_E);subElement1;subElement1 = subElement1->next)
				{
					if((strcmp(ezxml_attr(subElement1, US_XML_ID_ATTR),id)==0) || (subElement1 == NULL)) // if not found stay NULL else stay with value
					break;
				}
			if (subElement1 != NULL)
			{
				set_attr_value(subElement1,US_XML_LARGE_POOL_BUF_ATTR,(char*)lub_argv__get_arg(argv,1));
				set_attr_value(subElement1,US_XML_SHORT_POOL_BUF_ATTR,(char*)lub_argv__get_arg(argv,2));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
					{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
						{
						printf("failed to write file\n");
						}
					free(str);
					}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n", US_XML_GMAC_E, US_XML_CFG_FILE_TPM_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "gmac_bm_buffers", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									
									
bool_t  set_gpon_gmac_mh_en_params  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement1;
    ezxml_t     subElement2;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "port_init", 0, "gmac_config", 0, "gmac_mh_en", -1);
		if (xmlElement != NULL)
		{
			subElement1 = ezxml_child(xmlElement,"gmac0_mh_en");
			subElement2 = ezxml_child(xmlElement,"gmac1_mh_en");
			if (subElement1 != NULL && subElement2 != NULL)
			{
				set_tag_value(subElement1,(char*)lub_argv__get_arg(argv,0));
				set_tag_value(subElement2,(char*)lub_argv__get_arg(argv,1));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s or %s in XML config. file %s\n",  "gmac0_mh_en", "gmac1_mh_en", US_XML_CFG_FILE_TPM_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "gmac_mh_en", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										


bool_t  set_gpon_gmac_conn_params  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_PORT_INIT_E, 0, "gmac_config", 0,"pon_config", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", US_XML_TPM_E, US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										


bool_t  set_gpon_pppoe_add_enable  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,"pppoe_add_enable", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "pppoe_add_enable", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									


bool_t  set_gpon_num_vlan_tags  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,"num_vlan_tags", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "num_vlan_tags", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}												


bool_t  set_gpon_ttl_illegal_action  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,"ttl_illegal_action", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "ttl_illegal_action", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									


bool_t  set_gpon_tcp_flag_check  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,"tcp_flag_check", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "tcp_flag_check", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										

bool_t  set_gpon_cpu_trap_rx_queue  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TRAFFIC_SETTING_E, 0,"cpu_trap_rx_queue", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "cpu_trap_rx_queue", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}													

bool_t  set_gpon_mtu_setting_enabled  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "mtu_setting", 0,"mtu_setting_enabled", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mtu_setting_enabled", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										


bool_t  set_gpon_ipv4_mtu_ds  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "mtu_setting", 0,"ipv4_mtu_ds", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "ipv4_mtu_ds", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}											

bool_t  set_gpon_ipv4_mtu_us             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "mtu_setting", 0,"ipv4_mtu_us", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "ipv4_mtu_us", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}									
									
bool_t  set_gpon_ipv4_pppoe_mtu_us             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "mtu_setting", 0,"ipv4_pppoe_mtu_us", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "ipv4_pppoe_mtu_us", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										


bool_t  set_gpon_igmap_snoop_enabled             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,US_XML_ENABLED_E, -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s %s in XML config. file %s\n", "igmp_snoop", US_XML_ENABLED_E, US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}											


bool_t  set_gpon_igmp_cpu_rx_queue             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_cpu_rx_queue", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_cpu_rx_queue", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										
								
bool_t  set_gpon_igmp_frwrd_mode_wan             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_wan", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_wan", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										


bool_t  set_gpon_igmp_frwrd_mode_uni0             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_uni0", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_uni0", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}												

bool_t  set_gpon_igmp_frwrd_mode_uni1             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_uni1", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_uni1", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	

bool_t  set_gpon_igmp_frwrd_mode_uni2             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_uni2", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_uni2", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	

bool_t  set_gpon_igmp_frwrd_mode_uni3             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_uni3", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_uni3", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	


bool_t  set_gpon_igmp_frwrd_mode_uni4             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "igmp_snoop", 0,"igmp_frwrd_mode_uni4", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "igmp_frwrd_mode_uni4", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	

bool_t  set_gpon_mc_pppoe_enable             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "multicast", 0,"mc_pppoe_enable", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mc_pppoe_enable", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	


bool_t  set_gpon_mc_per_uni_vlan_xlat             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "multicast", 0,"mc_per_uni_vlan_xlat", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mc_per_uni_vlan_xlat", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										
									

bool_t  set_gpon_mc_filter_mode             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "multicast", 0,"mc_filter_mode", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mc_filter_mode", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	

bool_t  set_gpon_mc_hwf_queue             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, "multicast", 0,"mc_hwf_queue", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mc_hwf_queue", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}	


bool_t  set_gpon_mc_cpu_queue             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, "multicast", 0,"mc_cpu_queue", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "mc_cpu_queue", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_gpon_cpu_wan_egr_loopback             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		    xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,"cpu_wan_egr_loopback", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "cpu_wan_egr_loopback", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_gpon_gmac1_virt_uni             (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,"gmac1_virt_uni", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "gmac1_virt_uni", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_gpon_trace_debug_info       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,"trace_debug_info", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "trace_debug_info", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_gpon_catch_all_pkt_action       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,US_XML_TPM_PNC_E, 0,"catch_all_pkt_action", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "catch_all_pkt_action", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_gpon_udp_checksum_calc       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,US_XML_TPM_MOD_E, 0,"udp_checksum_calc", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "udp_checksum_calc", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_gpon_udp_checksum_use_init       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);

    if (xmlHead != NULL)
	{
	    xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,US_XML_TPM_MOD_E, 0,"udp_checksum_use_init", -1);
		if (xmlElement != NULL)
		{
			set_tag_value(xmlElement,(char*)lub_argv__get_arg(argv,0));
			str = ezxml_toxml(xmlHead);
			if (str != NULL)
			{
				if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
				{
					printf("failed to write file\n");
				}
				free(str);
			}
			else{
				printf("Can't perform ezxml_toxml\n");
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "udp_checksum_use_init", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}

bool_t  set_gpon_chain_params       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	char*       id = NULL;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);
	id = (char*)lub_argv__get_arg(argv,0);
	
    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,US_XML_TPM_MOD_E, 0,"chain_parameters", 0,"chain", -1);
		if (xmlElement != NULL)
		{
			for (xmlElement;xmlElement;xmlElement = xmlElement->next)
			{
				if(strcmp(ezxml_attr(xmlElement, US_XML_ID_ATTR),id)==0 || xmlElement == NULL)
					break;
			}
				
			if (xmlElement != NULL)
			{
				set_attr_value(xmlElement,US_XML_TYPE_ATTR,(char*)lub_argv__get_arg(argv,1));
				set_attr_value(xmlElement,US_XML_NUM_ATTR,(char*)lub_argv__get_arg(argv,2));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n",  "chain",  US_XML_CFG_FILE_TPM_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "chain", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}										

bool_t  set_gpon_mod_tpid_params       (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
	char*       str = NULL;
    ezxml_t     xmlHead;
    ezxml_t     subElement;
	char*       id = NULL;
	ezxml_t     xmlElement;
    bool_t      rcode = BOOL_TRUE;

    xmlHead = ezxml_parse_file(US_XML_CFG_FILE_TPM_GPON);
	id = (char*)lub_argv__get_arg(argv,0);
	
    if (xmlHead != NULL)
	{
		xmlElement = ezxml_get(xmlHead, US_XML_TPM_E, 0,"vlan_mod_tpid", 0,"mod_tpid", -1);
		if (xmlElement != NULL)
		{
			for (xmlElement;xmlElement;xmlElement = xmlElement->next)
			{
				if(strcmp(ezxml_attr(xmlElement, US_XML_ID_ATTR),id)==0 || xmlElement == NULL)
					break;
			}
				
			if (xmlElement != NULL)
			{
				set_attr_value(xmlElement,US_XML_TYPE_ATTR,(char*)lub_argv__get_arg(argv,1));
				str = ezxml_toxml(xmlHead);
				if (str != NULL)
				{
					if (write_to_file(str,US_XML_CFG_FILE_TPM_GPON)==BOOL_FALSE)
					{
						printf("failed to write file\n");
					}
					free(str);
				}
				else{
					printf("Can't perform ezxml_toxml\n");
					rcode = BOOL_FALSE;
					}
			}
			else{
				printf("Failed to find %s with wanted id in XML config. file %s\n",  "chain",  US_XML_CFG_FILE_TPM_GPON);
				rcode = BOOL_FALSE;
				}
		}
		else{
			printf("Failed to find %s in XML config. file %s\n", "chain", US_XML_CFG_FILE_TPM_GPON);
			rcode = BOOL_FALSE;
			}
		ezxml_free(xmlHead);
	}
	else{
		printf("File %s doesn't exist\n",US_XML_CFG_FILE_TPM_GPON);
		rcode = BOOL_FALSE;
		}

	return rcode;
}