/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/if_ether.h>
#include <net/if.h>
#include <linux/sockios.h>
#include <string.h>
#include <stdio.h>
#include <malloc.h>

#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <error.h>
#include <net/route.h>
#include "vendor.h"

static unsigned char  g_wan_pri        = 0;
static unsigned short g_wan_vid        = 0;
static unsigned int   g_wan_ip_address = 0;
static unsigned int   g_wan_ip_mask    = 0;
static unsigned int   g_wan_gate_way   = 0;

unsigned char username[24+1] = "admin";
unsigned char passwd[12+1]   = "admin";


int MvExtOamResetOnu(void)
{

    system("reboot ");
    
    return 0;
}

int MvExtOamUpgradeFirmware(unsigned char image_id, unsigned char file[64])
{
    printf("%s(), %d entered!\n", __FUNCTION__, __LINE__);
    
    return 0;
}

int  MvExtOamGetMac(unsigned char *pMac)
{
    char *device = "pon0";
    int   i;
    struct ifreq req;
    int   err;
    int   s;

    if (NULL == pMac)
    {
        return ERROR;
    }
     
    strcpy(req.ifr_name, device);
    s   = socket(AF_INET, SOCK_DGRAM, 0);

    if (s < 0)
    {
        printf("Socket create error (%s)\r\n", __FUNCTION__);
        return ERROR;
    }
    
    err = ioctl(s,SIOCGIFHWADDR, &req);
    close(s);

    memset(pMac, 0, 6*sizeof(char));    
    if (err != -1)
        memcpy(pMac, req.ifr_hwaddr.sa_data, ETH_ALEN);   
     
    return 0;
}

int  MvExtOamGetEth0Mac(unsigned char *pMac)
{
    char *device = "eth0";
    int   i;
    struct ifreq req;
    int   err;
    int   s;

    if (NULL == pMac)
    {
        return ERROR;
    }
     
    strcpy(req.ifr_name, device);
    s   = socket(AF_INET, SOCK_DGRAM, 0);
    if (s < 0)
    {
        printf("Socket create error (%s)\r\n", __FUNCTION__);
        return ERROR;
    }    
    err = ioctl(s,SIOCGIFHWADDR, &req);
    close(s);

    memset(pMac, 0, 6*sizeof(char));    
    if (err != -1)
        memcpy(pMac, req.ifr_hwaddr.sa_data, ETH_ALEN);   
     
    return 0;
}

int MvExtOamGetSn(onu_sn *pOnusn)
{ 
    if (NULL == pOnusn)
    {
        return ERROR;
    }
       
    memcpy(pOnusn->vendor_id, ONU_VENDOR_ID, VENDOR_SN_LEN);
    memcpy(pOnusn->onu_model, ONU_MODEL, strlen(ONU_MODEL));    
    memcpy(pOnusn->software_version, ONU_SW_VER, strlen(ONU_SW_VER));    
    memcpy(pOnusn->hardware_version, ONU_HW_VER, strlen(ONU_HW_VER));    

    if (NULL == pOnusn->onu_id)
    {
        printf("memory alloc fail\n");
        return ERROR;
    }

    MvExtOamGetMac(pOnusn->onu_id);

    return 0;
}

int MvExtOamGetCap(onu_cap  *pCap)
{  
    if (NULL == pCap)
    {
        return ERROR;
    }
      
    pCap->service_supported        = 0x7;
    pCap->ge_ethernet_ports_number = 1;
    pCap->ge_ports_bitmap.lsb      = 0x08;
    pCap->ge_ports_bitmap.msb      = 0x00;
    pCap->fe_ethernet_ports_number = 3;
    pCap->fe_ports_bitmap.msb      = 0x00;
    pCap->fe_ports_bitmap.lsb      = 0x07;
    pCap->pots_ports_number        = 2;
    pCap->e1_ports_number          = 0;
    pCap->upstream_queues_number   = ONU_UPSTREAM_QUEUES_NUMBER;
    pCap->max_upstream_queues_per_port   = ONU_MAX_UPSTREAM_QUEUES_PER_PORT;
    pCap->downstream_queues_number       = ONU_DOWNSTREAM_QUEUES_NUMBER;
    pCap->max_downstream_queues_per_port = ONU_MAX_DOWNSTREAM_QUEUES_PER_PORT;
    pCap->battery_backup                 = ONU_88F6560_BATTERYBAK;
    
    return 0;
}

int MvExtOamGetCapplus(onu_cap_plus *pCapplus)
{ 
    if (NULL == pCapplus)
    {
        return ERROR;
    }
    pCapplus->onu_type                        = MRVL_ONU_TYPE_SFU;
    pCapplus->multi_llid_support_capabilities = ONU_88F6560_PON_MULT_LLID_SUPPORT;
    pCapplus->onu_optical_protection_type     = MRVL_ONU_OPTICAL_PROTECTION_NOT_SUPPORT;
    pCapplus->pon_ports_number                = ONU_88F6560_PON_IF_NUMBER;
    pCapplus->slot_number                     = 0; /*for sfu*/

    return 0;
}

int MvExtOamGetCapplus3(onu_cap_plus_3 *pCapplus3)
{

    if (NULL == pCapplus3)
    {
        return ERROR;
    }
       
    pCapplus3->ipv6_supported      = ONU_88F6560_IPV6_SUPPORT;
    pCapplus3->power_ctl_supported = MRVL_ONU_OPTICAL_PROTECTION_NOT_SUPPORT;

    return 0;
}

int MvExtOamGetAuthData(onu_auth *pAuthData)
{
    FILE *fp = NULL;
    int num = 0;
    unsigned char loid_uid[ONU_LOID_USER_NAME_LENGTH+1] = {0};
    unsigned char loid_pwd[ONU_LOID_PASSWD_LENGTH+1]    = {0};    
       
    if (NULL == pAuthData)
    {
        return ERROR;
    }
       
    /* If LOID file does not exist, return the default LOID values. */
    if ((fp = fopen(ONU_LOID_FILE, "r+")) == NULL)
    {
        memcpy(pAuthData->loid,     username, strlen(username));        
        memcpy(pAuthData->password, passwd,   strlen(passwd));    
    }    
    else
    {
        /* Read LOID user name from file */
        num = fread(loid_uid, 1, ONU_LOID_USER_NAME_LENGTH, fp); 
        if (num != ONU_LOID_USER_NAME_LENGTH)
        {
            memcpy(pAuthData->loid,     username, strlen(username));        
            memcpy(pAuthData->password, passwd,   strlen(passwd));    
        }
        else
        {
            memcpy(pAuthData->loid, loid_uid, strlen(loid_uid));

            /* read LOID password from file */
            num = fread(loid_pwd, 1, ONU_LOID_PASSWD_LENGTH, fp); 
            if (num != ONU_LOID_PASSWD_LENGTH)
            {
                memcpy(pAuthData->password, passwd, strlen(passwd));    
            }
            else
            {
                memcpy(pAuthData->password, loid_pwd,  strlen(loid_pwd));
            }
        }

        fclose(fp);
    }

    return 0;
}

int MvExtOamSetAuthData(onu_auth *pAuthData)
{
    FILE *fp  = NULL;
    int   num = 0;
    unsigned char loid_uid[ONU_LOID_USER_NAME_LENGTH+1] = {0};
    unsigned char loid_pwd[ONU_LOID_PASSWD_LENGTH+1]    = {0};    
       
    if (NULL == pAuthData)
    {
        return ERROR;
    }

    memset(username, 0, sizeof(username));
    memset(passwd,   0, sizeof(passwd));
       
    /* copy LOID values to local memory */   
    memcpy(loid_uid, pAuthData->loid, strlen(pAuthData->loid));    
    memcpy(loid_pwd, pAuthData->password, strlen(pAuthData->password));        
            
    /* If LOID file does not exist, set LOID values to temporary values. */
    if ((fp = fopen(ONU_LOID_FILE, "w+")) == NULL)
    {
        memcpy(username, pAuthData->loid,     strlen(pAuthData->loid));        
        memcpy(passwd,   pAuthData->password, strlen(pAuthData->password));            
    }    
    else
    {
        /*write LOID user name to file*/
        num = fwrite(loid_uid, 1, ONU_LOID_USER_NAME_LENGTH, fp); 
        if (num != ONU_LOID_USER_NAME_LENGTH)
        {
            memcpy(username,pAuthData->loid, strlen(pAuthData->loid));        
            memcpy(passwd,pAuthData->password, strlen(pAuthData->password));    
        }
        else
        {
            /*write LOID password to file*/
            num = fwrite(loid_pwd, 1, ONU_LOID_PASSWD_LENGTH, fp); 
            if (num != ONU_LOID_PASSWD_LENGTH)
            {
                memcpy(passwd, pAuthData->password, strlen(pAuthData->password));    
            }
        }

        fclose(fp);
    }

    return 0;
}

int MvExtOamGetWanInfo(onu_wan_config  *pWaninfo)
{    
    if (NULL == pWaninfo )
    {
        return ERROR;
    }
      
    pWaninfo->mng_ip        = g_wan_ip_address;
    pWaninfo->mng_mask      = g_wan_ip_mask;
    pWaninfo->mng_gw        = g_wan_gate_way;
    pWaninfo->data_cvlan    = g_wan_vid;
    pWaninfo->data_priority = g_wan_pri;
    
    return 0;
}

 
int MvExtOamSetIfAddr(const char *ifname, const char  *ipaddr, const char *netmask, const char *gateway)
{
    int ret = -1;
    struct ifreq req;
    struct sockaddr_in *host = NULL;
    struct rtentry rte;

    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if ( -1 == sockfd)
    {
        perror("create socket error \r\n");
        return ret;
    }

    /*config ip address*/
    bzero(&req, sizeof(struct ifreq));
    strcpy(req.ifr_name, ifname);
    host = (struct sockaddr_in *)&req.ifr_addr;
    host->sin_family = AF_INET;

    if ( 1 != inet_pton(AF_INET, ipaddr, &(host->sin_addr)))
    {
        perror("transfer ipaddr error \r\n");
        close(sockfd);
        sockfd = -1;
        return ret;
    }

    if ( ioctl(sockfd, SIOCSIFADDR, &req) < 0 )
    {
        perror("config ipaddr error \r\n");
        close(sockfd);
        sockfd = -1;
        return ret;
    }

    /*config netmask*/
    bzero(&req, sizeof(struct ifreq));
    strcpy(req.ifr_name, ifname);
    host = (struct sockaddr_in *)&req.ifr_addr;
    host->sin_family = AF_INET;

    if ( 1 != inet_pton(AF_INET, netmask, &(host->sin_addr)))
    {
        perror("transfer netmask error \r\n");
        close(sockfd);
        sockfd = -1;
        return ret;
    }

    if ( ioctl(sockfd, SIOCSIFNETMASK, &req) < 0 )
    {
        perror("config netmask error \r\n");
        close(sockfd);
        sockfd = -1;
        return ret;
    }

    /*config gateway*/
    bzero(&rte, sizeof(struct rtentry));
    rte.rt_flags = (RTF_UP | RTF_GATEWAY);
    host = (struct sockaddr_in *)&rte.rt_gateway;
    host->sin_family = AF_INET;
    if ( 1 != inet_pton(AF_INET, gateway, &(host->sin_addr)))
    {
        perror("transfer gateway error \r\n");
        close(sockfd);
        sockfd = -1;
        return ret;
    }


    host = (struct sockaddr_in *)&rte.rt_dst;
    host->sin_family = AF_INET;
    if ( ioctl(sockfd, SIOCADDRT, &rte) < 0 )
    {
        perror("config gateway error \r\n");
        close(sockfd);
        sockfd = -1;
        return ret;
    }

    close(sockfd);
    ret = 0;

    return ret;
}

int MvExtOamSetIfIpv6Addr(const char *ifname, const char  *ipaddr, const char prefix, const char *gateway)
{
    int ret = -1;
    int sockfd6;
    struct ifreq ifr;
    struct sockaddr_in6 *host = NULL;
    struct in6_rtmsg route;

    OAM_IPv6_REQ ifreq6;
    ifreq6.ifr6_prefixlen = prefix;

    sockfd6 = socket(AF_INET6, SOCK_DGRAM, 0);
    if ( -1 == sockfd6)
    {
        perror("create socket error \r\n");
        return ret;
    }

    if ( 1 != inet_pton(AF_INET6, ipaddr, &ifreq6.ifr6_addr))
    {
        perror("transfer ipv6addr error \r\n");
        close(sockfd6);
        sockfd6 = -1;
        return ret;
    }

    /*config ip address*/
    bzero(&ifr, sizeof(struct ifreq));
    strcpy(ifr.ifr_name, ifname);
    ioctl(sockfd6,SIOGIFINDEX, &ifr);
    ifreq6.ifr6_ifindex = ifr.ifr_ifindex;

    if ( ioctl(sockfd6, SIOCSIFADDR, &ifreq6) < 0 )
    {
        perror("config ipv6addr error \r\n");
        close(sockfd6);
        sockfd6 = -1;
        return ret;
    }

    /*config gateway*/
    bzero(&route, sizeof(struct in6_rtmsg));
    if ( 1 != inet_pton(AF_INET6, gateway, &(route.rtmsg_dst)))
    {
        perror("transfer gateway error \r\n");
        close(sockfd6);
        sockfd6 = -1;
        return ret;
    }

   if ( 1 != inet_pton(AF_INET6, "::", &(route.rtmsg_gateway)))
    {
        perror("transfer :: error \r\n");
        close(sockfd6);
        sockfd6 = -1;
        return ret;
    }

    route.rtmsg_dst_len = prefix;
    route.rtmsg_flags = RTF_UP | RTF_HOST;
    route.rtmsg_ifindex = ifr.ifr_ifindex;
    route.rtmsg_metric = 1;
    if ( ioctl(sockfd6, SIOCADDRT, &route) < 0 )
    {
        perror("config gateway error \r\n");
        close(sockfd6);
        sockfd6 = -1;
        return ret;
    }

    close(sockfd6);
    ret = 0;

    return ret;

}
