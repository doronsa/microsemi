#!/bin/sh

###echo "CURDIR = " ${CURDIR}

COMMON_CONFIG=${CURDIR}/xml_globals
### echo "COMMON_CONFIG = " ${COMMON_CONFIG}
XML_DIR=${CURDIR}/xml_commands
### echo "XML_DIR = " ${XML_DIR}

### create xml directory
mkdir -p ${XML_DIR}

### Copy common XML files
cp -f ${COMMON_CONFIG}/*.xml ${XML_DIR}

### Add application commands to the global-commands.xml
### echo "TPM_DIR = " ${TPM_DIR}
##################################
if [ -e "${TPM_DIR}" ]
then
    END_OF_VIEW=`grep -n "</VIEW>" ${XML_DIR}/global-commands.xml | cut -f1 -d":"`
    ### echo "END_OF_VIEW = " ${END_OF_VIEW}
    head -n$((END_OF_VIEW-1)) ${XML_DIR}/global-commands.xml > /tmp/temp.$$

    cat ${TPM_DIR}/config/tpm-global-com.txt | while read line
    do
       echo $line >> /tmp/temp.$$
    done

    ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"
    ### cat /tmp/temp.$$
    ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"

    cp -f /tmp/temp.$$ ${XML_DIR}/global-commands.xml
    rm -f /tmp/temp.$$

    ### Copy TPM XML files
    cp -f ${TPM_DIR}/config/*.xml ${XML_DIR}

fi

### echo "TPMA_DIR = " ${TPMA_DIR}
##################################
if [ -e "${TPMA_DIR}" ]
then
    END_OF_VIEW=`grep -n "</VIEW>" ${XML_DIR}/global-commands.xml | cut -f1 -d":"`;
    ### echo "END_OF_VIEW = " ${END_OF_VIEW}
    head -n$((END_OF_VIEW-1)) ${XML_DIR}/global-commands.xml > /tmp/temp.$$;

    cat ${TPMA_DIR}/config/tpma-global-com.txt | while read line
    do
       echo $line >> /tmp/temp.$$
    done

    ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"
    ### cat /tmp/temp.$$;
    ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"

    cp -f /tmp/temp.$$ ${XML_DIR}/global-commands.xml;
    rm -f /tmp/temp.$$

    ### Copy TPMA XML files
    cp -f ${TPMA_DIR}/config/*.xml ${XML_DIR}

fi

### echo "OAM_DIR = " ${OAM_DIR}
##################################
if [ -e "${OAM_DIR}" ]
then
    END_OF_VIEW=`grep -n "</VIEW>" ${XML_DIR}/global-commands.xml | cut -f1 -d":"`
    ### echo "END_OF_VIEW = " ${END_OF_VIEW}
    head -n$((END_OF_VIEW-1)) ${XML_DIR}/global-commands.xml > /tmp/temp.$$

    cat ${OAM_DIR}/config/ctc-global-com.txt | while read line
    do
       echo $line >> /tmp/temp.$$
    done

    ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"
    ### cat /tmp/temp.$$
    ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"

    cp -f /tmp/temp.$$ ${XML_DIR}/global-commands.xml
    rm -f /tmp/temp.$$

    ### Copy CTC XML files
    cp -f ${OAM_DIR}/config/*.xml ${XML_DIR}

fi

### echo "MID_DIR = " ${MID_DIR}
##################################
if [ -e "${MID_DIR}" ]
then
    END_OF_VIEW=`grep -n "</VIEW>" ${XML_DIR}/global-commands.xml | cut -f1 -d":"`
    ### echo "END_OF_VIEW = " ${END_OF_VIEW}
    head -n$((END_OF_VIEW-1)) ${XML_DIR}/global-commands.xml > /tmp/temp.$$

    cat ${MID_DIR}/config/midware-global-com.txt | while read line
    do
       echo $line >> /tmp/temp.$$
    done

    ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"
    ### cat /tmp/temp.$$
    ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"

    cp -f /tmp/temp.$$ ${XML_DIR}/global-commands.xml
    rm -f /tmp/temp.$$

    ### Copy MID XML files
    cp -f ${MID_DIR}/config/*.xml ${XML_DIR}

fi


### echo "APM_DIR = " ${APM_DIR}
##################################
if [ -e "${APM_DIR}" ]
then
    ### Copy APM XML files
    cp -f ${APM_DIR}/config/*.xml ${XML_DIR}
fi

### echo "OMCI_DIR = " ${OMCI_DIR}
##################################
if [ -e "${OMCI_DIR}" ]
then
    END_OF_VIEW=`grep -n "</VIEW>" ${XML_DIR}/global-commands.xml | cut -f1 -d":"`
    ### echo "END_OF_VIEW = " ${END_OF_VIEW}
    head -n$((END_OF_VIEW-1)) ${XML_DIR}/global-commands.xml > /tmp/temp.$$

    cat ${OMCI_DIR}/config/omci-global-com.txt | while read line
    do
       echo $line >> /tmp/temp.$$
    done

    ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"
    ### cat /tmp/temp.$$
    ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"

    cp -f /tmp/temp.$$ ${XML_DIR}/global-commands.xml
    rm -f /tmp/temp.$$

    ### Copy OMCI XML files
    cp -f ${OMCI_DIR}/config/*.xml ${XML_DIR}
fi

### echo "COM_DIR = " ${COM_DIR}
##################################
if [ -e "${COM_DIR}" ]
then
    ### Copy Common XML files
    cp -f ${COM_DIR}/config/*.xml ${XML_DIR}
fi

### echo "IGMP_DIR = " ${IGMP_DIR}
##################################
if [ -e "${IGMP_DIR}" ]
then
    ### Copy IGMP XML files
    cp -f ${IGMP_DIR}/config/*.xml ${XML_DIR}
fi

### echo "FLASH_DIR = " ${FLASH_DIR}
##################################
if [ -e "${FLASH_DIR}" ]
then
    ### Copy Flash XML files
    cp -f ${FLASH_DIR}/config/*.xml ${XML_DIR}
fi

### echo "MMP_DIR = " ${MMP_DIR}
##################################
if [ -e "${MMP_DIR}" ]
then
    if [ -e "${MMP_DIR}/config/mmp-global-com.txt" ];then
        END_OF_VIEW=`grep -n "</VIEW>" ${XML_DIR}/global-commands.xml | cut -f1 -d":"`
        ### echo "END_OF_VIEW = " ${END_OF_VIEW}
        head -n$((END_OF_VIEW-1)) ${XML_DIR}/global-commands.xml > /tmp/temp.$$

        cat ${MMP_DIR}/config/mmp-global-com.txt | while read line
        do
           echo $line >> /tmp/temp.$$
        done

        ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"
        ### cat /tmp/temp.$$
        ### echo "+++++++++++++++++++++++++++++++++++++++++++++++++++"

        cp -f /tmp/temp.$$ ${XML_DIR}/global-commands.xml
        rm -f /tmp/temp.$$

        ### Copy MMP XML files
        cp -f ${MMP_DIR}/config/*.xml ${XML_DIR}
    fi
fi

if [ -e "${I2C_DIR}" ]
then
    ### Copy I2C XML files
    cp -f ${I2C_DIR}/config/*.xml ${XML_DIR}
fi

if [ -e "${LBDT_DIR}" ]
then
    ### Copy LBDT XML files
    cp -f ${LBDT_DIR}/config/*.xml ${XML_DIR}
fi

cp -f ${CURDIR}/config/*.xml ${XML_DIR}


