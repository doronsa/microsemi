#/*******************************************************************************
#Copyright (C) Marvell International Ltd. and its affiliates
#
#This software file (the "File") is owned and distributed by Marvell
#International Ltd. and/or its affiliates ("Marvell") under the following
#licensing terms.
#
#********************************************************************************
#Marvell Commercial License Option
#
#If you received this File from Marvell and you have entered into a commercial
#license agreement (a "Commercial License") with Marvell, the File is licensed
#to you under the terms of the applicable Commercial License.
#
#*******************************************************************************/

#################################################
# Include common makefile header
#################################################
SW_TREE_DIR = $(CURDIR)
include $(SW_TREE_DIR)/makHdr.include


LIBS = xml clishlib mthread mipc main_common mng_rtos mng_trace mng_utils pon upon_lib cust tpm flash \
       midware_cli mng_common_cli flash_cli i2c mng_timer  lbdt_cli

APPS = rstp igmp voip midware upon apm oam omci sycl misc clish


.PHONY: all clean $(LIBS) $(APPS) mk_dir

all: mk_dir $(LIBS) $(APPS)

mk_dir: build/lib build/bin

build/lib:
	mkdir -p build/lib

build/bin:
	mkdir -p build/bin

xml:
	@echo "------------------------------------------------------------------------------------"
	@echo Create xml_commands
	@echo "------------------------------------------------------------------------------------"
	chmod a+x  $(SW_TREE_DIR)/main/xml_dir_create.sh
	CURDIR=$(SW_TREE_DIR)/main  TPM_DIR=$(SW_TREE_DIR)/tpm IGMP_DIR=$(SW_TREE_DIR)/igmp \
	OAM_DIR=$(SW_TREE_DIR)/mng/oam MID_DIR=$(SW_TREE_DIR)/mng/midware APM_DIR=$(SW_TREE_DIR)/mng/apm \
	OMCI_DIR=$(SW_TREE_DIR)/mng/omci COM_DIR=$(SW_TREE_DIR)/mng/common FLASH_DIR=$(SW_TREE_DIR)/mng/flash \
	MMP_DIR=$(SW_TREE_DIR)/mmp I2C_DIR=$(SW_TREE_DIR)/mng/i2c LBDT_DIR=$(SW_TREE_DIR)/mng/misc/lbdt $(APP_FLAGS) $(SW_TREE_DIR)/main/xml_dir_create.sh

mthread:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling mthread
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mthread/obj

mipc:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling mipc
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mipc/obj

main_common:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling main_common
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/main/src/common/obj

mng_rtos:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling mng_rtos
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_rtos/obj

mng_trace:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling mng_trace
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_trace/obj

mng_utils:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling mng_util
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_utils/obj

mng_timer:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling mng_timer
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_timer/obj

mng_cfg:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling mng_cfg
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_cfg/obj

mng_common_cli:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling mng_common_cli
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_clish/obj

pon:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling pon
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/pon/obj

cust:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling cust
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/cust/obj

tpm:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling tpm
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/tpm/appl/tpm_api/obj
	make -C $(SW_TREE_DIR)/tpm/appl/tpm_clish/obj

flash:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling flash
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/flash/appl/flash_app/obj

i2c:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling i2c
	@echo "------------------------------------------------------------------------------------"
#	make -C $(SW_TREE_DIR)/mng/i2c/appl/i2c_app/obj
	make -C $(SW_TREE_DIR)/mng/i2c/appl/i2c_clish/obj

mng_cli:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling mng_cli
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_clish/obj

flash_cli:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling flash_cli
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/flash/appl/flash_clish/obj

midware_cli:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling midware_cli
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/midware/cli

sycl:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling sycl
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/main/src/sycl

misc:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling misc
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/misc/obj
	
lbdt_cli:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling lbdt_cli
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/misc/lbdt_cli/obj

rstp:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling rstp
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/rstp/obj all

igmp:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling igmp
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/igmp/obj all

voip:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling voip
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/voip/obj all

midware:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling midware
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/midware/obj all

upon:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling upon
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/upon/obj all

upon_lib:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling upon_lib
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/mng/upon/obj -f Makefile_lib all
apm:
	@echo "------------------------------------------------------------------------------------"
	@echo Copy apm
	@echo "------------------------------------------------------------------------------------"
	cp -fr $(SW_TREE_DIR)/mng/apm/obj/libapm_mipc_client.so $(SW_TREE_DIR)/build/lib
	cp -fr $(SW_TREE_DIR)/mipc/api_defs/alarm/libalarm_mipc_client.so $(SW_TREE_DIR)/build/lib
	cp -fr $(SW_TREE_DIR)/mipc/api_defs/pm/libpm_mipc_client.so $(SW_TREE_DIR)/build/lib
	cp -fr $(SW_TREE_DIR)/mipc/api_defs/avc/libavc_mipc_client.so $(SW_TREE_DIR)/build/lib
	cp -fr $(SW_TREE_DIR)/mng/apm/obj/apm $(SW_TREE_DIR)/build/bin

oam:
	@echo "------------------------------------------------------------------------------------"
	@echo Copy oam
	@echo "------------------------------------------------------------------------------------"
	cp -fr $(SW_TREE_DIR)/mng/oam/obj/liboam_mipc_client.so $(SW_TREE_DIR)/build/lib
	cp -fr $(SW_TREE_DIR)/mng/oam/obj/oam_stack $(SW_TREE_DIR)/build/bin

omci:
	@echo "------------------------------------------------------------------------------------"
	@echo Copy omci
	@echo "------------------------------------------------------------------------------------"
	cp -fr $(SW_TREE_DIR)/mng/omci/obj/libomci_mipc_client.so $(SW_TREE_DIR)/build/lib
	cp -fr $(SW_TREE_DIR)/mng/omci/obj/omci $(SW_TREE_DIR)/build/bin

clishlib:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling clishlib
	@echo "------------------------------------------------------------------------------------"
	CLISH_DIR=$(SW_TREE_DIR)/clish/core/clish-0.7.3 CROSS_COMPILE=${CROSS_COMPILE} $(SW_TREE_DIR)/clish/core/compile_clish.sh

clish:
	@echo "------------------------------------------------------------------------------------"
	@echo Compiling clish
	@echo "------------------------------------------------------------------------------------"
	make -C $(SW_TREE_DIR)/clish/appl/obj

clean:
	@echo "------------------------------------------------------------------------------------"
	@echo Cleaning everything
	@echo "------------------------------------------------------------------------------------"
	rm -fr $(SW_TREE_DIR)/main/xml_commands
	rm -fr $(SW_TREE_DIR)/build/lib/*.so
	rm -fr $(SW_TREE_DIR)/build/bin/*
	rm -fr $(SW_TREE_DIR)/clish/core/clish-0.7.3/config.log
	rm -fr $(SW_TREE_DIR)/clish/core/clish-0.7.3/config.status
	touch  $(SW_TREE_DIR)/clish/core/clish-0.7.3/config.status
	rm -fr $(SW_TREE_DIR)/mng/midware/sqlite3/config.log
	rm -fr $(SW_TREE_DIR)/mng/midware/sqlite3/config.status
	touch  $(SW_TREE_DIR)/mng/midware/sqlite3/config.status
	make -C $(SW_TREE_DIR)/mthread/obj clean
	make -C $(SW_TREE_DIR)/mipc/obj clean
	make -C $(SW_TREE_DIR)/main/src/common/obj clean
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_rtos/obj clean
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_trace/obj clean
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_utils/obj clean
	make -C $(SW_TREE_DIR)/mng/pon/obj clean
	make -C $(SW_TREE_DIR)/mng/cust/obj clean
	make -C $(SW_TREE_DIR)/tpm/appl/tpm_api/obj clean
	make -C $(SW_TREE_DIR)/tpm/appl/tpm_clish/obj clean
	make -C $(SW_TREE_DIR)/rstp/obj clean
	make -C $(SW_TREE_DIR)/igmp/obj clean
	make -C $(SW_TREE_DIR)/voip/obj clean
	make -C $(SW_TREE_DIR)/mng/midware/obj clean
	make -C $(SW_TREE_DIR)/mng/upon/obj clean
	make -C $(SW_TREE_DIR)/mng/upon/obj -f Makefile_lib clean
	make -C $(SW_TREE_DIR)/clish/core/clish-0.7.3 clean
	make -C $(SW_TREE_DIR)/clish/appl/obj clean
	make -C $(SW_TREE_DIR)/mng/flash/appl/flash_app/obj clean
	make -C $(SW_TREE_DIR)/main/src/sycl clean
	make -C $(SW_TREE_DIR)/mng/midware/cli clean
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_clish/obj clean
	make -C $(SW_TREE_DIR)/mng/flash/appl/flash_clish/obj clean
	make -C $(SW_TREE_DIR)/mng/i2c/appl/i2c_app/obj clean
	make -C $(SW_TREE_DIR)/mng/i2c/appl/i2c_clish/obj clean
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_timer/obj clean
	make -C $(SW_TREE_DIR)/mng/common/appl/mng_cfg/obj clean
	make -C $(SW_TREE_DIR)/mng/misc/obj clean
	make -C $(SW_TREE_DIR)/mng/misc/lbdt_cli/obj clean
