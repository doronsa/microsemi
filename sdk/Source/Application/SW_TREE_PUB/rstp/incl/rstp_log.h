/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : RSTP                                                      **/
/**                                                                          **/
/**  FILE        : RSTP_log.h                                               **/
/**                                                                          **/
/**  DESCRIPTION : Definition of RSTP module                                 **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *                 															  
 *         Ken  - initial version created.   14/March/2011          
 *                                                                              
 ******************************************************************************/

#ifndef _RSTP_LOG_H_
#define _RSTP_LOG_H_

#define RSTP_LOG_MODULE  "RSTP"

typedef enum
{
	RSTP_LOG_NONE = 0x00,
	RSTP_LOG_ERROR,		
	RSTP_LOG_ALARM,		/*such alarm not importance*/
	RSTP_LOG_INFO , /*informat ,such as pkt received*/
	RSTP_LOG_DEBUG,
	RSTP_LOG_DEBUG_EXTPKT,	
	RSTP_LOG_DEBUG_PKT,
}RSTP_LOG_SEVERITY_T;

typedef enum
{
	RSTP_LOG_TO_FILE = 0x00 ,
	RSTP_LOG_TO_STDOUT ,
}RSTP_LOG_TERMINAL_T;


void  RSTP_log_printf(const char * func_name, RSTP_LOG_SEVERITY_T level,const char * format, ...);
INT32 RSTP_log_init();

#endif
