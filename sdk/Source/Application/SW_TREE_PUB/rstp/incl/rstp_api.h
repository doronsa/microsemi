/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : RSTP                                                      **/
/**                                                                          **/
/**  FILE        : rstp_interface.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : Definition of RSTP interfaces                             **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *                 															  
 *         Ken  - initial version created.   10/March/2010           
 *                                                                              
 ******************************************************************************/
#ifndef _RSTP_INTERFACE_H
#define _RSTP_INTERFACE_H

#include "globals.h"
 
INT32 RSTP_get_enable(bool *enable);
INT32 RSTP_set_enable(bool enable);
INT32 RSTP_get_bridge_priority(UINT32 *priority);
INT32 RSTP_set_bridge_priority(UINT32 bridge_priority);
INT32 RSTP_get_time(UINT32 *fwd_delay,UINT32 *hello,UINT32 *max_age);
INT32 RSTP_set_time(UINT32 fwd_delay,UINT32 hello,UINT32 max_age);
INT32 RSTP_get_priority(UINT32 port_id,UINT32 *priority);
INT32 RSTP_set_priority(UINT32 port_id,UINT32 priority);
INT32 RSTP_port_get_enable(UINT32 port_id,bool *enable);
INT32 RSTP_port_set_enable(UINT32 port_id,bool enable);


#endif



