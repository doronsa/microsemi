#ifndef _RSTP_COM_HEAD_H
#define _RSTP_COM_HEAD_H

#define RSTP_EXIT_OK				            				0

#define RSTP_ERROR_BASE								(-29000)
#define RSTP_ERROR_NOT_INITIALIZED			            			(RSTP_ERROR_BASE-13)
#endif