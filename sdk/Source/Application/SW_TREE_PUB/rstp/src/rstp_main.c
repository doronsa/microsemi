/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.                    
*                                                                                
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED  
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD  
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS  
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".      
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS      
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY    
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),      
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL     
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).                               
******************************************************************************** 
*      main.c                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:                                                      
*                                                                                
* DATE CREATED: July 12, 2010     
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.4 $                                                           
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "common.h"
#include "params_mng.h"
#include "globals.h"
#include "errorCode.h"
#include "mng_trace.h"
#include "ponOnuMngIf.h"
#include "mipc.h"

extern int32_t mvTpmDrvFdExist(void);
extern int32_t mvCustDrvFdExist(void);
extern void    rstp_mipc_server_handler(char* inMsg, unsigned int size);


/*******************************************************************************
* main
*
* DESCRIPTION:      Rest main
*
* INPUTS:			none
*
* OUTPUTS:			none
*
* RETURNS:          none
*
*******************************************************************************/
int main(int argc, char* argv[])
{
  int32_t             rcode;
  int                 mipc_fd;

  //printf("############ RSTP Init Start     ############\n");
  if (RSTP_init() != US_RC_OK) { printf("T&W RSTP init failed\n"); return (US_RC_FAIL); }

  //printf("############ RSTP Init End       ############\n");

  //server_start(argv[1]);
  mipc_fd = mipc_init("rstp", 0, 0);
  if (MIPC_ERROR == mipc_fd)  
  { 	  
	  return; 
  }   
  mthread_register_mq(mipc_fd, rstp_mipc_server_handler);
  mthread_start();

  return(US_RC_OK);
}

