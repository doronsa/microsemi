#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>
#include "globals.h"
#include "rstp_log.h"
#include "rstp_comm.h"

/*---------------------------------------------------*/
static RSTP_LOG_SEVERITY_T      g_rstp_log_level        = RSTP_LOG_DEBUG;
static RSTP_LOG_TERMINAL_T      g_rstp_log_terminal     = RSTP_LOG_TO_STDOUT    ;/*out log terminal mode*/
static INT8                     g_rstp_log_trace_normal = 0;

/*---------------------------------------------------*/
static INT32 RSTP_log_trace_check_if_valid();
/*---------------------------------------------------*/
/*log level*/
void RSTP_log_severity_level_set(RSTP_LOG_SEVERITY_T level)
{
    if(level <= RSTP_LOG_DEBUG_PKT )
    {
        g_rstp_log_level = level ;
    }
}

RSTP_LOG_SEVERITY_T RSTP_log_severity_level_get()
{

    return g_rstp_log_level ;
}

/*log terminal mode ,to tty or file*/
void RSTP_log_terminal_mode_set(RSTP_LOG_TERMINAL_T mode)
{
    if(mode <= RSTP_LOG_TO_STDOUT )
    {
        g_rstp_log_terminal = mode ;
    }
}

RSTP_LOG_TERMINAL_T RSTP_log_terminal_mode_get()
{
    return g_rstp_log_terminal ;
}

/*log terminal implementation*/
static void RSTP_LOG_TERMINAL_Tty_out(const char *format, va_list param)
{
    vprintf(format, param); 
}

static void RSTP_LOG_TERMINAL_Traceout(const char *format, va_list param)
{
    FILE *fd = 0;
    
    if(RSTP_log_trace_check_if_valid() != RSTP_EXIT_OK)
    {
        printf("rstp log terminal tracerout not valid\r\n");        

        return ;
    }

    fd =fopen("/tmp/rstp_config.log", "a");
    if(fd != NULL)
    {
        vfprintf(fd, format, param);

        fclose(fd);        
    }
}


void RSTP_log_printf(const char *func_name, RSTP_LOG_SEVERITY_T level, const char *format, ...)
{ 
    va_list arg_ptr; 
    
    if(RSTP_log_severity_level_get() < level)
    {
        return;
    }

    if(RSTP_log_terminal_mode_get() == RSTP_LOG_TO_FILE)
    {
        va_start(arg_ptr, format); 
        RSTP_LOG_TERMINAL_Traceout(format, arg_ptr); 
        va_end(arg_ptr); 
    }
    else if(RSTP_log_terminal_mode_get() == RSTP_LOG_TO_STDOUT)
    {
        va_start(arg_ptr, format); 
        RSTP_LOG_TERMINAL_Tty_out  (format, arg_ptr); 
        va_end(arg_ptr); 
    }
}

/*-------------------------------------------------------------------------------
* LOG AND TRACEOUT INNER HELP API 
*-------------------------------------------------------------------------------*/
static INT32 RSTP_log_trace_check_if_valid()
{
    if(g_rstp_log_trace_normal != TRUE)
    {
        printf("Error,rstp log traceout is not initialzed!\r\n"); 

        return RSTP_ERROR_NOT_INITIALIZED ;
    }

    return RSTP_EXIT_OK;
}

/*-------------------------------------------------------------------------------
* LOG AND TRACEOUT INIT VIEW
*-------------------------------------------------------------------------------*/
INT32 RSTP_log_init()
{
    static FILE       *fd = NULL; 
    
    fd =fopen("/tmp/rstp_config.log", "r"); 
    if(fd==NULL)  
    { 
        printf("Error note :\n");
        printf("    log file '/tmp/rstp_config.log'failed\n");
        printf("    the log dump function will be affected.\n");
        printf("    pls check the entir-variable or check log file\n");

        g_rstp_log_trace_normal = 0;
    } 
    else
    {
        g_rstp_log_trace_normal = TRUE;

        fclose(fd);
    }
    return 0;
}


INT32 RSTP_STACK_log_terminate()
{
    g_rstp_log_trace_normal = 0;    
    return 0;
}

