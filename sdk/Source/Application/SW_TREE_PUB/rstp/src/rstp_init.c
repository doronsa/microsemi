/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.                    
*                                                                                
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED  
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD  
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS  
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".      
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS      
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY    
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),      
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL     
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).                               
******************************************************************************** 
*      main.c                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:   Ken                                                   
*                                                                                
* DATE CREATED: March 10, 2011
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.3 $                                                           
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "globals.h"
#include "rstp_api.h"
#include "rstp_log.h"

static INT32 os_chmod(INT8 *pathname,INT32 mode)
{
    return chmod(pathname,(mode_t)mode);
}

static BOOL RSTP_packet_check_if_initialized()
{
    BOOL  l_init_flag = FALSE;
    char buff[255] ={0};
     /*start cli pthread deamon*/
    FILE *fd =fopen("/tmp/rstp_config.log", "r");
    if(fd == NULL)
    {
        fd =fopen("/tmp/rstp_config.log", "w+");
        if(fd == NULL)
        {
            printf("/tmp/rstp_config.log' ,please configure the envir-Variable first");
            return FALSE ;
        }
    }

    os_chmod("/tmp/rstp_config.log",438);
        
    while(fgets(buff,255,fd)!= NULL)
    {
        if(strstr(buff,"<RSTP statak initialized") != NULL)
        {
            if(strstr(buff,"true") != NULL)
            {
                l_init_flag = TRUE;
                break;
            }
            else
            {
                break;
            }
        }
        memset(buff, 0, sizeof(buff));
    }
    
    fclose(fd);
    
    return l_init_flag ;
}

#if 0
int rstp_main(int argc, char* argv[])
{
    printf("RSTP module started!\n");

    RSTP_packet_check_if_initialized();
    RSTP_log_init();
    
    return 0;
}
#endif
int RSTP_init()
{
    //printf("RSTP module started!\n");

    RSTP_packet_check_if_initialized();
    RSTP_log_init();

	return(0);
}
