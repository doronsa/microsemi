/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.                    
*                                                                                
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED  
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD  
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS  
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".      
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS      
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY    
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),      
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL     
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).                               
******************************************************************************** 
*      rstp.c                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:   Ken                                                   
*                                                                                
* DATE CREATED: March 10, 2011
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.1.1.1 $                                                           
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "globals.h"
#include "rstp_api.h"
#include "rstp_log.h"

INT32 RSTP_get_enable(bool *enable)
{
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);    
    return OK;
}

INT32 RSTP_set_enable(bool enable)
{
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"Input parameter: enable %s\n",enable?"true":"false");
    return OK;
}

INT32 RSTP_get_bridge_priority(UINT32 *priority)
{
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    return OK;
}

INT32 RSTP_set_bridge_priority(UINT32 bridge_priority)
{
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"Input parameter: bridge_priority %d\n",bridge_priority);
    return OK;
}

INT32 RSTP_get_time(UINT32 *fwd_delay,UINT32 *hello,UINT32 *max_age)
{
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    return OK;
}

INT32 RSTP_set_time(UINT32 fwd_delay,UINT32 hello,UINT32 max_age)
{
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"Input parameters: fwd_delay %d hello %d max_age %d\n",fwd_delay,hello,max_age);
    return OK;
}

INT32 RSTP_get_priority(UINT32 port_id,UINT32 *priority)
{
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"Input parameter: port_id %d\n",port_id);
    return OK;
}

INT32 RSTP_set_priority(UINT32 port_id,UINT32 priority)
{
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"Input parameters: port_id %d priority %d\n",port_id,priority);
    return OK;
}

INT32 RSTP_port_get_enable(UINT32 port_id,bool *enable)
{
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"Input parameter: port_id %d\n",port_id);
    return OK;
}

INT32 RSTP_port_set_enable(UINT32 port_id,bool enable)
{
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\n",__FILE__,__LINE__,__func__);
    RSTP_log_printf(RSTP_LOG_MODULE,RSTP_LOG_DEBUG,"Input parameters: port_id %d enable %s\n",port_id,enable?"true":"false");
    return OK;
}


