/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.                    
*                                                                                
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED  
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD  
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS  
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".      
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS      
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY    
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),      
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL     
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).                               
******************************************************************************** 
*      voip_api.c                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:   Ken                                                   
*                                                                                
* DATE CREATED: March 10, 2011
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1.2 $                                                           
*******************************************************************************/
#include <string.h>
#include <stdbool.h>

#include "voip_api.h"
#include "voip_log.h"
#include "ezxml.h"

/* Global parameters for VoIP IP host */
static voip_ip_host_f_l_argument g_voip_host_param;
static voip_ip_host_f_l_argument g_current_voip_host_param;
#define VOIP_IPADDRESS_STRING_LENG 20
static  int8_t gs_sting_addr_buff[VOIP_IPADDRESS_STRING_LENG];

#define MRVL_SFU_PON_DEV_IF_NAME "pon0"

static char *configuration_src    = "/etc/xml_params/sipapp.xml";
static char *configuration_dst    = "/etc/xml_params/sipapp.xml";
static char *sip_application      = "sipapp";
static char *sip_application_path = "/usr/bin/";

// In memory database for parsed XML data
static struct voip_xml_node GLOBAL_PARAMETER[] = 
{
    {"voice_ip_mode",      "00",            NULL, 3},
    {"iad_ip_addr",        "10.38.58.100",  NULL, 3},
    {"iad_net_mask",       "255.255.255.0", NULL, 3},
    {"iad_def_gw",         "10.38.58.1",    NULL, 3},
    {"pppoe_mode",         "00",            NULL, 3},
    {"pppoe_user",         "admin",         NULL, 3},
    {"pppoe_passwd",       "admin",         NULL, 3},
    {"vlan_tag_mode",      "01",            NULL, 3},
    {"cvlan_id",           "00",            NULL, 3},
    {"svlan_id",           "00",            NULL, 3},
    {"priority",           "5",             NULL, 3},
    {NULL,                 "",              NULL, 0}
};

static struct voip_xml_node SIP_CONFIG_NODE[] = 
{
    {"server_ip",               "10.34.45.67", NULL, 3},
    {"serv_com_port",           "3333",        NULL, 3},
    {"back_server_ip",          "10.38.58.99", NULL, 3},
    {"back_serv_com_port",      "5555",        NULL, 3},
    {"reg_server_ip",           "23.45.67.56", NULL, 3},
    {"reg_serv_com_port",       "8888",        NULL, 3},
    {"back_reg_server_ip",      "33.44.55.66", NULL, 3},
    {"back_reg_serv_com_port",  "aaaa",        NULL, 3},
    {"outbound_server_ip",      "10.20.30.40", NULL, 3},
    {"outbound_serv_com_port",  "678",         NULL, 3},
    {"reg_interval",            "99",          NULL, 3},
    {"heart_beat_switch",       "89",          NULL, 3},
    {"heart_beat_cycle",        "89",          NULL, 3},
    {"heart_beat_count",        "88",          NULL, 3}, 
    {NULL,                      "",            NULL, 0}
};

static struct voip_xml_node SIP_ACCOUNT0[] = 
{
    {"status",         "1",    NULL, 3},
    {"local_line_id",  "0",    NULL, 3},
    {"name",           "fxs0", NULL, 3},
    {"number",         "1111", NULL, 3},
    {"passwd",         "aaaa", NULL, 3},   
    {NULL,             "",     NULL, 0}
};

static struct voip_xml_node SIP_ACCOUNT1[] = 
{
    {"status",        "1",      NULL, 3},
    {"local_line_id", "1",      NULL, 3},
    {"name",          "fxs1",   NULL, 3},
    {"number",        "2222",   NULL, 3},
    {"passwd",        "bbbb",   NULL, 3},   
    {NULL,             "",      NULL, 0}
};


static struct voip_xml_node SIP_FAX[] = 
{
    {"t38_enable",    "1",     NULL, 3},
    {"fax_control",   "1",     NULL, 3},
    {NULL,            "",      NULL, 0}
};

static struct voip_xml_node SIP_LOGGING[] = 
{
    {"enable_sip",    "0",     NULL, 3},
    {"log_level",     "2",     NULL, 3},
    {NULL,            "",      NULL, 0}
};

static struct voip_xml_node SIP_DIAL_PLAN[] = 
{
    {"number_length", "4",     NULL, 3},
    {NULL,            "",      NULL, 0}
};

static struct voip_xml_node SIP_CODEC7[] = 
{
    {"name",         "G726-40", NULL, 5},
    {"payload_type", "103",     NULL, 5},
    {"ptime",        "20",      NULL, 5},
    {"status",       "1",       NULL, 5},
    {NULL,           "",        NULL, 0}
};

static struct voip_xml_node SIP_CODEC6[] = 
{
    {"name",         "G726-32", NULL, 5},
    {"payload_type", "104",     NULL, 5},
    {"ptime",        "20",      NULL, 5},
    {"status",       "1",       NULL, 5},
    {NULL,           "",        NULL, 0}
};


static struct voip_xml_node SIP_CODEC5[] = 
{
    {"name",         "G726-24", NULL, 5},
    {"payload_type", "105",     NULL, 5},
    {"ptime",        "20",      NULL, 5},
    {"status",       "1",       NULL, 5},
    {NULL,           "",        NULL, 0}
};

static struct voip_xml_node SIP_CODEC4[] = 
{
    {"name",         "G726-16", NULL, 5},
    {"payload_type", "106",     NULL, 5},
    {"ptime",        "20",      NULL, 5},
    {"status",       "1",       NULL, 5},
    {NULL,           "",        NULL, 0}
};

static struct voip_xml_node SIP_CODEC3[] = 
{
    {"name",         "G729",    NULL, 5},
    {"payload_type", "18",      NULL, 5},
    {"ptime",        "20",      NULL, 5},
    {"status",       "1",       NULL, 5},
    {NULL,           "",        NULL, 0}
};

static struct voip_xml_node SIP_CODEC2[] = 
{
    {"name",         "PCMA",    NULL, 5},
    {"payload_type", "8",       NULL, 5},
    {"ptime",        "20",      NULL, 5},
    {"status",       "1",       NULL, 5},
    {NULL,           "",        NULL, 0}
};

static struct voip_xml_node SIP_CODEC1[] = 
{
    {"name",         "PCMU",    NULL, 5},
    {"payload_type", "0",       NULL, 5},
    {"ptime",        "20",      NULL, 5},
    {"status",       "1",       NULL, 5},
    {NULL,           "",        NULL, 0}
};

static struct voip_xml_node SIP_CODECS[] = 
{
    {"codec",        "",        SIP_CODEC1, 4},
    {"codec",        "",        SIP_CODEC2, 4},
    {"codec",        "",        SIP_CODEC3, 4},
    {"codec",        "",        SIP_CODEC4, 4},
    {"codec",        "",        SIP_CODEC5, 4},
    {"codec",        "",        SIP_CODEC6, 4},
    {"codec",        "",        SIP_CODEC7, 4},    
    {NULL,           "",        NULL,       0}
};

static struct voip_xml_node SIP_MEDIA[] = {
    {"configuration_file",      "mmp_cfg.xml;/etc/mmp/mmp_cfg.xml;/usr/bin/mmp_cfg.xml", NULL,3},
    {"cid_profile_type1",       "1",   NULL,        3},
    {"cid_profile_type2",       "2",   NULL,        3},
    {"lec_echo_tail",           "16",  NULL,        3},    
    {"dial_tone_profile_id",    "2",   NULL,        3},
    {"dial_tone_id",            "1",   NULL,        3},
    {"ring_tone_profile_id",    "0",   NULL,        3},
    {"ring_tone_id",            "0",   NULL,        3},    
    {"ringback_tone_profile_id","2",   NULL,        3},
    {"ringback_tone_id",        "2",   NULL,        3},
    {"busy_tone_profile_id",    "2",   NULL,        3},
    {"busy_tone_id",            "3",   NULL,        3},          
    {"codecs",                  "",    SIP_CODECS,  3},            
    {NULL,                      "",    NULL,        0}
};

static struct voip_xml_node XML_MENU_NODE[] = 
{
    {"sip_port",            "5060",                  NULL,                    2},
    {"rtp_port",            "4000",                  NULL,                    2},   
    {"host_addr",           "10.5.25.212",           NULL,                    2},
    {"ipc_server_port",     "43344",                 NULL,                    2},
    {"ipc_client_port",     "43345",                 NULL,                    2},   
    {"proxy",               "<uri><![CDATA[sip:10.5.25.28;lr]]></uri>", NULL, 2},          
    {"global_parameter",    "",                      GLOBAL_PARAMETER,        2},
    {"sip_parameter",       "",                      SIP_CONFIG_NODE,         2},
    {"account",             "",                      SIP_ACCOUNT0,            2},
    {"account",             "",                      SIP_ACCOUNT1,            2},
    {"fax",                 "",                      SIP_FAX,                 2},
    {"logging",             "",                      SIP_LOGGING,             2},
    {"dial_plan",           "",                      SIP_DIAL_PLAN,           2},
    {"media",               "",                      SIP_MEDIA,               2}, 
    {NULL,                  "",                      NULL,                    0}
};

static struct voip_xml_node XML_HEADER[] = {
    {"sip_app",        "",          XML_MENU_NODE, 1},
    {NULL,             "",          NULL,          0}
};

static struct voip_xml_node XML_ROOT[] = 
{
    {"",               "",          XML_HEADER, 0},
    {NULL,             "",          NULL,       0}
};

/******************************************************************************
 *
 * Function   : make_node
 *              
 * Description: This routine translates text string to id
 *              
 * Parameters:  node        [IN]  -  xml node 
 *              xmlBuffer   [IN]  -  xml buffer to write to 
 *              
 * Returns    : void
 *              
 ******************************************************************************/

static void make_node (struct voip_xml_node *node, char *xmlBuffer)
{
    int  i = 0;
    char tempbuf[60];

    if (!node->child)
        return;

    for (i = 0; node->child[i].tagname; i++)
    {
        BLANK(node->depth);
        sprintf (xmlBuffer, "%s<%s>", xmlBuffer, node->child[i].tagname);

        if (node->child[i].child)
        {
            sprintf (xmlBuffer, "%s\n", xmlBuffer);
            make_node (&node->child[i], xmlBuffer);
        }
        else
        {
            if (strcmp("proxy", node->child[i].tagname) == 0)
            {
                printf("%s: *****ZG       #%d proxy node->child[i].tagvalue = '%s'\n", __FUNCTION__, i, node->child[i].tagvalue);
                if (strncmp("<uri>", node->child[i].tagvalue, strlen("<uri>")) == 0)
                {
                    sprintf (xmlBuffer, "%s%s", xmlBuffer, (node->child[i].tagvalue));
                }
                else
                {
                    sprintf (tempbuf, "<uri>%s</uri>", (node->child[i].tagvalue));
                    sprintf (xmlBuffer, "%s%s", xmlBuffer, tempbuf);
                }
            }
            else
            {
                sprintf (xmlBuffer, "%s%s", xmlBuffer, (node->child[i].tagvalue));
            }
        } 

        if (!strcmp( node->child[i].tagvalue,"" ))
            BLANK(node->depth);

        sprintf (xmlBuffer, "%s</%s>\n", xmlBuffer, node->child[i].tagname);

        if (strcmp("proxy", node->child[i].tagname) == 0)
        {
            printf("%s: *****ZG    FRAG proxy xmlBuffer '%s'\n", __FUNCTION__, &xmlBuffer[strlen(xmlBuffer) - 60]);
        }
    }
}


/******************************************************************************
 *
 * Function   : make_xml_file
 *              
 * Description: This routine generates a new XML file
 *              
 * Parameters:  xmlFileName     [IN]  -  xml file name
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/

static INT32 make_xml_file (char *xmlFileName)
{
    FILE *xmlFile = NULL;
    char xmlBuffer[8192];

    memset (xmlBuffer, 0, sizeof (xmlBuffer));

    if (!(xmlFile = fopen (xmlFileName, "w")))
    {
        return ERROR;
    }

    make_node(XML_ROOT, xmlBuffer);
    fwrite (xmlBuffer, strlen (xmlBuffer), 1, xmlFile);
    fclose (xmlFile);

    return OK;
}

/*****************************************************************************
* FUNCTION:    xml_config_set
*
* PURPOSE:     copy xml node value to a config structure
*
* PARAMETERS:  xmlconf     [IN]  -  xml node store to
*              format      [IN]  - 
*
* RETURNS:     
*
*****************************************************************************/
static void xml_config_set(char * xmlconf, char* format,...)
{  
    va_list   argptr;

    if (!xmlconf )
    {
        VOIP_DBG("illegal value");
    }

    va_start  (argptr, format);
    vsnprintf (xmlconf, BUFSZ, format, argptr);
    va_end    (argptr);     
}

/******************************************************************************
 *
 * Function   : updateHostIpAddr
 *              
 * Description: This routine updates VoIP host IP address
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/

static INT32 updateHostIpAddr(char *hostIp)
{
    if (NULL != hostIp)
        xml_config_set(XML_MENU_NODE[2].tagvalue, hostIp);

    return OK;
}

/******************************************************************************
 *
 * Function   : updateSipServerAddr
 *              
 * Description: This routine updates SIP proxy server address
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/

static INT32 updateSipServerAddr(int potsPort, char *proxy)
{
    // The data that is written is proxy 
    char   l_proxy[80+4];

    memset(l_proxy, 0, sizeof(l_proxy));
    sprintf(l_proxy, "<uri><![CDATA[sip:%s;lr]]></uri>", proxy);

    xml_config_set(XML_MENU_NODE[5].tagvalue, l_proxy);

    return OK;
}


/******************************************************************************
 *
 * Function   : updateSipAccountData
 *              
 * Description: This routine updates the account data per POTS port
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/

static INT32 updateSipAccountData(int potsPort, char *userName, char *password, char *aor)
{
    // The data that is written are 
    //   1. User data for POTs #1 - name, password and number=AOR
    //   2. User data for POTs #2 - name, password and number=AOR
    //   3. SIP port (5060 usually)


    if (potsPort == 0)
    {
        if (userName != NULL && strlen(userName) != 0)
        {
            xml_config_set(SIP_ACCOUNT0[2].tagvalue, userName);
        }
        if (aor != NULL && strlen(aor) != 0)
        {
            xml_config_set(SIP_ACCOUNT0[3].tagvalue, aor);
        }        
        if (password != NULL && strlen(password) != 0)
        {
            xml_config_set(SIP_ACCOUNT0[4].tagvalue, password);  
        }
    }

    else if (potsPort == 1)
    {
        if (userName != NULL && strlen(userName) != 0)
        {
            xml_config_set(SIP_ACCOUNT1[2].tagvalue, userName);
        }
        if (aor != NULL && strlen(aor) != 0)
        {
            xml_config_set(SIP_ACCOUNT1[3].tagvalue, aor);
        }        
        if (password != NULL && strlen(password) != 0)
        {
            xml_config_set(SIP_ACCOUNT1[4].tagvalue, password);
        }
    }
    else
    {
        VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG, "%s: Unexpected portPort %d\n\r", __FUNCTION__, potsPort);
    }
    return OK;
}


/******************************************************************************
 *
 * Function   : updateSipTcpUdpPort
 *              
 * Description: This routine updates the SIP TCP/UDP port
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/

static INT32 updateSipTcpUdpPort(int sipPort)
{
    char buf[20];

    sprintf(buf, "%d", sipPort);

    xml_config_set(XML_MENU_NODE[0].tagvalue, buf);

    return OK;
}

/******************************************************************************
 *
 * Function   : dial_plan_config_parse
 *              
 * Description: This routine extracts dial plan parameters
 *              
 * parameters:  root                 [IN]   -  xml tree
 *              logging_parameter    [OUT]  -  xml nodes to store to
 *
 * Returns    : INT32
 *              
 ******************************************************************************/

static int dial_plan_config_parse(ezxml_t root, voip_xml_node_t dial_plan_parameter)
{
    VOIP_DBG(""); 
    ezxml_t tag; 

    tag = ezxml_get(root,"dial_plan",0,"number_length",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(dial_plan_parameter[0].tagvalue, ezxml_txt(tag) );
    }

    return 0;    
}


/******************************************************************************
 *
 * Function   : logging_config_parse
 *              
 * Description: This routine extracts logging parameters
 *              
 * parameters:  root                 [IN]   -  xml tree
 *              logging_parameter    [OUT]  -  xml nodes to store to
 *
 * Returns    : INT32
 *              
 ******************************************************************************/

static int logging_config_parse(ezxml_t root, voip_xml_node_t logging_parameter)
{
    VOIP_DBG(""); 
    ezxml_t tag; 

    tag = ezxml_get(root,"logging",0,"enable_sip",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(logging_parameter[0].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"logging",0,"log_level",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(logging_parameter[1].tagvalue, ezxml_txt(tag) );
    }

    return 0;    
}


/******************************************************************************
 *
 * Function   : media_config_parse
 *              
 * Description: This routine extracts media parameters
 *              
 * parameters:  root                 [IN]   -  xml tree
 *              logging_parameter    [OUT]  -  xml nodes to store to
 *
 * Returns    : INT32
 *              
 ******************************************************************************/

static int media_config_parse(ezxml_t root, voip_xml_node_t media_parameter)
{
    VOIP_DBG(""); 
    ezxml_t tag; 

    tag = ezxml_get(root,"media",0,"configuration_file",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[0].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"media",0,"cid_profile_type1",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[1].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"media",0,"cid_profile_type2",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[2].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"media",0,"lec_echo_tail",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[3].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"media",0,"dial_tone_profile_id",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[4].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"media",0,"dial_tone_id",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[5].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"media",0,"ring_tone_profile_id",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[6].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"media",0,"ring_tone_id",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[7].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"media",0,"ringback_tone_profile_id",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[8].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"media",0,"ringback_tone_id",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[9].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"media",0,"busy_tone_profile_id",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[10].tagvalue, ezxml_txt(tag) ); 
    }

    tag = ezxml_get(root,"media",0,"busy_tone_id",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(media_parameter[11].tagvalue, ezxml_txt(tag) );  
    }
#if 0
    for (i=0; i<8; i++)
    {
        char var[64];
        sprintf(var, "SIP_CODEC%d", i);
        codec_config_parse(root, var, i);
    }
#endif	 
    return 0;

}


/******************************************************************************
 *
 * Function   : fax_config_parse
 *              
 * Description: This routine extracts FAX parameters
 *              
 * parameters:  root                 [IN]   -  xml tree
 *              logging_parameter    [OUT]  -  xml nodes to store to
 *
 * Returns    : INT32
 *              
 ******************************************************************************/

static int fax_config_parse(ezxml_t root, voip_xml_node_t fax_parameter)
{
    VOIP_DBG(""); 
    ezxml_t tag; 

    tag = ezxml_get(root,"fax",0,"t38_enable",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(fax_parameter[0].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"fax",0,"fax_control",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(fax_parameter[1].tagvalue, ezxml_txt(tag) );

    }

    return 0;    
}


/******************************************************************************
 *
 * Function   : global_config_parse
 *              
 * Description: This routine extracts global CTC parameters
 *              
 * parameters:  root                 [IN]   -  xml tree
 *              logging_parameter    [OUT]  -  xml nodes to store to
 *
 * Returns    : INT32
 *              
 ******************************************************************************/

static int global_config_parse(ezxml_t root, voip_xml_node_t global)
{
    VOIP_DBG(""); 
    ezxml_t tag; 

    tag = ezxml_get(root,"global_parameter",0,"voice_ip_mode",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(global[0].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"global_parameter",0,"iad_ip_addr",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(global[1].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"global_parameter",0,"iad_net_mask",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(global[2].tagvalue, ezxml_txt(tag) );

    }

    tag = ezxml_get(root,"global_parameter",0,"iad_def_gw",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(global[3].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"global_parameter",0,"pppoe_mode",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(global[4].tagvalue, ezxml_txt(tag) );

    }

    tag = ezxml_get(root,"global_parameter",0,"pppoe_user",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(global[5].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"global_parameter",0,"pppoe_passwd",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(global[6].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"global_parameter",0,"vlan_tag_mode",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(global[7].tagvalue, ezxml_txt(tag) );

    }

    tag = ezxml_get(root,"global_parameter",0,"cvlan_id",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(global[8].tagvalue, ezxml_txt(tag) );

    }

    tag = ezxml_get(root,"global_parameter",0,"svlan_id",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(global[9].tagvalue, ezxml_txt(tag) );

    }

    tag = ezxml_get(root,"global_parameter",0,"priority",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(global[10].tagvalue, ezxml_txt(tag) );
    }

    return 0;     

}


/******************************************************************************
 *
 * Function   : account_config_parse
 *              
 * Description: This routine extracts top level user account SIP parameters
 *              
 * parameters:  root                 [IN]   -  xml tree
 *              logging_parameter    [OUT]  -  xml nodes to store to
 *
 * Returns    : INT32
 *              
 ******************************************************************************/

static int account_config_parse(ezxml_t root, voip_xml_node_t sip_account, int id)
{
    VOIP_DBG(""); 
    ezxml_t tag; 

    tag = ezxml_get(root,"account",id,"status",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_account[0].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"account",id,"local_line_id",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_account[1].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"account",id,"name",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_account[2].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"account",id,"number",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_account[3].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"account",id,"passwd",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_account[4].tagvalue, ezxml_txt(tag) );
    }

    return 0;

}


/******************************************************************************
 *
 * Function   : sip_config_parse
 *              
 * Description: This routine extracts top level SIP parameters server_ip, serv_com_port etc
 *              
 * parameters:  root                 [IN]   -  xml tree
 *              logging_parameter    [OUT]  -  xml nodes to store to
 *
 * Returns    : INT32
 *              
 ******************************************************************************/

static int sip_config_parse(ezxml_t root, voip_xml_node_t sip_parameter)
{
    VOIP_DBG(""); 
    ezxml_t tag;   

    tag = ezxml_get(root,"sip_parameter", 0, "server_ip",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[0].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "serv_com_port",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[1].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "back_server_ip",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[2].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "back_serv_com_port",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[3].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "reg_server_ip",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[4].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "reg_server_com_port",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[5].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "bak_reg_server_ip",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[6].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "bak_reg_serv_com_port",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[7].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "outbound_server_ip",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[8].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "outbound_serv_com_port",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[9].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "reg_interval",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[10].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "heart_beat_switch",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[11].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "heart_beat_cycle",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[12].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"sip_parameter", 0, "heart_beat_count",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(sip_parameter[13].tagvalue, ezxml_txt(tag) );
    }

    return 0;
}


/******************************************************************************
 *
 * Function   : first_config_parse
 *              
 * Description: This routine extracts top level parameters sip_port, rtp_port etc
 *              
 * parameters:  root                 [IN]   -  xml tree
 *              logging_parameter    [OUT]  -  xml nodes to store to
 *
 * Returns    : INT32
 *              
 ******************************************************************************/

static int first_config_parse(ezxml_t root, voip_xml_node_t menu_parameter)
{
    VOIP_DBG(""); 
    ezxml_t tag; 

    tag = ezxml_get(root,"sip_port",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(menu_parameter[0].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"rtp_port",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(menu_parameter[1].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"host_addr",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(menu_parameter[2].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"ipc_server_port",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(menu_parameter[3].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"ipc_client_port",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        xml_config_set(menu_parameter[4].tagvalue, ezxml_txt(tag) );
    }

    tag = ezxml_get(root,"proxy",0,"uri",-1); 
    if (tag)
    {
        VOIP_DBG("found tag %s, value is %s",ezxml_name(tag), ezxml_txt(tag));
        char buf[64];
        sprintf(buf,"<![CDATA[%s]]>", ezxml_txt(tag));        
        xml_config_set(menu_parameter[5].tagvalue, buf );
    }

    return 0;    
}


/******************************************************************************
 *
 * Function   : xml_init_config
 *              
 * Description: This routine parses the SIP XML parameters files into memory
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/

static INT32 xml_init_config(char *configuration_file)
{
    ezxml_t root;

    if (!configuration_file )
    {
        printf("%s: illegal configuration file %s\n", __FUNCTION__, configuration_file);
        return ERROR;
    }

    root = ezxml_parse_file(configuration_file);
    if (!root)
    {
        printf("%s: unable to parse configuration file %s\n", __FUNCTION__, configuration_file);
        return ERROR;
    }

    first_config_parse(root, XML_MENU_NODE);
    global_config_parse(root, GLOBAL_PARAMETER);    
    sip_config_parse(root, SIP_CONFIG_NODE);
    account_config_parse(root, SIP_ACCOUNT0, 0);
    account_config_parse(root, SIP_ACCOUNT1, 1);  
    fax_config_parse(root,SIP_FAX);
    logging_config_parse(root,SIP_LOGGING);   
    dial_plan_config_parse(root, SIP_DIAL_PLAN);    
    media_config_parse(root,SIP_MEDIA);

    ezxml_free(root);

    return OK;

}

/******************************************************************************
 *
 * Function   : VOIP_updateSipTcpUdpPort
 *              
 * Description: This routine updates TCP or UDP port for IP host
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_updateSipTcpUdpPort_F(int sipPort)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%s In, sipPort[%d] ----->\r\n", __FUNCTION__, sipPort);

    /* parses the SIP XML file and places into memory database */
    if (xml_init_config(configuration_src) == ERROR)
        return ERROR;

    /* updates database to set TCP or UDP port for IP host     */
    updateSipTcpUdpPort(sipPort);

    /* Writes the SIP XML file back to file system             */
    if (make_xml_file(configuration_dst) != 0)
    {
        printf("%s: make_xml_file failed\n", __FUNCTION__);
        return ERROR;
    }
    
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%s out <-----\r\n", __FUNCTION__);
    
    return OK;
}

INT8* VOIP_osInetNtoa_F(UINT32 ip_address)
{
    memset(gs_sting_addr_buff, 0, sizeof(gs_sting_addr_buff));
    sprintf(gs_sting_addr_buff,"%d.%d.%d.%d", (ip_address&0xff000000)>>24, (ip_address&0x00ff0000)>>16,
           (ip_address&0x0000ff00)>>8, (ip_address&0x0ff));

    return gs_sting_addr_buff ;
}

/******************************************************************************
 *
 * Function   : VOIP_configIpHostParam_F
 *              
 * Description: This routine updates IP host parameters
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
STATUS VOIP_configIpHostParam_F(UINT32 ip_address, UINT32 ip_mask, UINT32 gate_way, UINT32 vid, UINT32 pbits)
{
    int    l_ret = 0;    
    char   pri_str[4];
    char   vid_str[8];
    UINT32 route_Destination;
    UINT32 new_route_Destination;

    char ifconfig_command1[64]     = "ifconfig ";
    char vconfig_command1[64]      = "vconfig add ";
    char vconfig_command2[64]      = "vconfig set_egress_map ";
    char vconfig_command2_tail[8]  = " 0 ";
    char vconfig_command3[64]      = "vconfig rem ";
    char route_add_command3[128]   = "route add -net ";
    char route_del_command3[128]   = "route del -net ";
    char ifconfig_command4[64];

    /*The following CMD should be kept .*/
    strcat(ifconfig_command1, MRVL_SFU_PON_DEV_IF_NAME);
    strcat(vconfig_command1,  MRVL_SFU_PON_DEV_IF_NAME);
    strcat(vconfig_command2,  MRVL_SFU_PON_DEV_IF_NAME);
    strcat(vconfig_command3,  MRVL_SFU_PON_DEV_IF_NAME);
    /*CMD end*/

    if ((!ip_address) || (!ip_mask) || (!gate_way) || (!vid))
    {
        VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG, "Illegal config, return error :ip(0x%x) mask(0x%x) gw(0x%x) vlan(%d) pbits(%d)\n",
                           ip_address, ip_mask, gate_way, vid, pbits);  
        
        return ERROR;
    }

    if ((ip_address == g_current_voip_host_param.ip) &&
       (ip_mask    == g_current_voip_host_param.netmask) &&
       (gate_way   == g_current_voip_host_param.gateway) &&
       (vid        == g_current_voip_host_param.vid))
    {
        return OK;
    }
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG, "onu remote mng configure:ip(0x%x) mask(0x%x) gw(0x%x) vlan(%d) pbits(%d)\n",
                       ip_address, ip_mask, gate_way, vid, pbits);

    /* Delete old VID */
    if (g_current_voip_host_param.vid && (g_current_voip_host_param.vid != vid) && (g_current_voip_host_param.vid != VOIP_DEFAULT_UNTAG_RULE))
    {
        memset(vid_str, 0, sizeof(vid_str)); 
        memset(ifconfig_command4, 0, sizeof(ifconfig_command4)); 
        
        sprintf(vid_str, ".%d ", g_current_voip_host_param.vid);
        strcat(ifconfig_command4, vconfig_command3);
        strcat(ifconfig_command4, vid_str);   

        VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_DEBUG, "%s:%s\n",__FUNCTION__, ifconfig_command4);
        l_ret = system(ifconfig_command4);  //Remove v_pon
        
        if (l_ret < 0)
        {
             VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_ERROR, "%s:%s fail.rt %d\n",__FUNCTION__, ifconfig_command4, l_ret);
        }
        usleep(10000);   

        g_current_voip_host_param.vid = 0;
    }

    /* restore old ip address of pon0 */
    if(((g_current_voip_host_param.vid == 0) || (g_current_voip_host_param.vid == VOIP_DEFAULT_UNTAG_RULE)) && 
       ((vid != 0) && (vid != VOIP_DEFAULT_UNTAG_RULE)))
    {
        memset(ifconfig_command4, 0, sizeof(ifconfig_command4)); 
        
        sprintf(ifconfig_command4, "ifconfig pon0 3.3.3.3 netmask 255.0.0.0 up");

        VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_DEBUG, "%s:%s\n",__FUNCTION__, ifconfig_command4);
        l_ret = system(ifconfig_command4);  //Remove v_pon
        
        if (l_ret < 0)
        {
             VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_ERROR, "%s:%s fail.rt %d\n",__FUNCTION__, ifconfig_command4, l_ret);
        }
        usleep(10000);   

        g_current_voip_host_param.vid = 0;
    }    

    /* Add new VID */
    if (vid && (vid != VOIP_DEFAULT_UNTAG_RULE) && (g_current_voip_host_param.vid != vid))
    {
        memset(ifconfig_command4, 0, sizeof(ifconfig_command4)); 
        memset(vid_str, 0, sizeof(vid_str)); 
        sprintf(vid_str, " %d ", vid);

        strcat(ifconfig_command4, vconfig_command1);
        strcat(ifconfig_command4, vid_str);         

        VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_DEBUG, "%s:%s\n",__FUNCTION__, ifconfig_command4);

        l_ret = system(ifconfig_command4); //Add vid interface
        if (l_ret < 0)
        {
            VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_ERROR, "%s:%s fail.rt %d\n",__FUNCTION__, ifconfig_command4, l_ret);

            return ERROR ;
         }
    }
    
    /* Add P-bits */
    if ((pbits < VOIP_PBITS_NO_CARE_VALUE) && (vid != VOIP_DEFAULT_UNTAG_RULE))
    {
        memset(ifconfig_command4, 0, sizeof(ifconfig_command4)); //Add pri
        memset(pri_str, 0, sizeof(pri_str));
        memset(vid_str, 0, sizeof(vid_str));
        sprintf(vid_str, ".%d ", vid);
        sprintf(pri_str, " %d ", pbits);
        
        strcat(ifconfig_command4, vconfig_command2);
        strcat(ifconfig_command4, vid_str);
        strcat(ifconfig_command4, pri_str);          
        strcat(ifconfig_command4, vconfig_command2_tail);
        VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_DEBUG, "%s:%s\n",__FUNCTION__, ifconfig_command4);

        l_ret = system(ifconfig_command4);   
        if (l_ret < 0)
        {
            VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_ERROR, "%s:%s fail.rt %d\n",__FUNCTION__, ifconfig_command4, l_ret);
            return l_ret ;
        }
    }
    
    /*Set ip and netmask*/
    memset(ifconfig_command4, 0, sizeof(ifconfig_command4)); 

    strcat(ifconfig_command4, ifconfig_command1);    
    if (vid && (vid != VOIP_DEFAULT_UNTAG_RULE))
    {
        sprintf(vid_str, ".%d ", vid);    
        strcat(ifconfig_command4, vid_str); 
    }
    strcat(ifconfig_command4, " ");

    strcat(ifconfig_command4, VOIP_osInetNtoa_F(ip_address));
    strcat(ifconfig_command4, " netmask ");
    strcat(ifconfig_command4, VOIP_osInetNtoa_F(ip_mask));

    usleep(10000);
    VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_DEBUG, "%s:%s\n",__FUNCTION__, ifconfig_command4);

    l_ret = system(ifconfig_command4);
    if (l_ret < 0)
    {
        VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_ERROR, "%s:%s fail.rt %d\n",__FUNCTION__, ifconfig_command4, l_ret);

        return ERROR ;
    }

    /*set gw*/     
    if (g_current_voip_host_param.gateway)
    {
        route_Destination     = g_current_voip_host_param.ip & g_current_voip_host_param.netmask; 
        new_route_Destination = ip_address & ip_mask ;

        if ((g_current_voip_host_param.ip == ip_address) && (g_current_voip_host_param.netmask == ip_mask) && 
            (g_current_voip_host_param.vid == vid) && (g_current_voip_host_param.gateway != gate_way))
        {
            strcat(route_del_command3, VOIP_osInetNtoa_F(route_Destination));
            strcat(route_del_command3, " netmask ");
            strcat(route_del_command3, VOIP_osInetNtoa_F(g_current_voip_host_param.netmask)); 

            VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_DEBUG, "%s\r\n",route_del_command3);

            l_ret = system(route_del_command3);
            if (l_ret <0)
            {
                VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_ERROR, "%s:%s fail.rt %d\n",__FUNCTION__,  route_del_command3, l_ret);
                return l_ret ;
            }
            usleep(100000);            
        }

        /*Add route*/
        strcat(route_add_command3,VOIP_osInetNtoa_F(new_route_Destination));
        strcat(route_add_command3," netmask ");
        strcat(route_add_command3,VOIP_osInetNtoa_F(ip_mask));
        strcat(route_add_command3," gw ");
        strcat(route_add_command3,VOIP_osInetNtoa_F(gate_way));
        strcat(route_add_command3," metric 20 ");

        VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_DEBUG, "%s\r\n",route_add_command3);

        l_ret = system(route_add_command3);
        if(l_ret <0)
        {
            VOIP_log_printf(VOIP_LOG_MODULE, VOIP_LOG_ERROR, "%s:%s fail.rt %d\n",__FUNCTION__,  vconfig_command2, l_ret);
            return l_ret ;
        }
    }
    g_current_voip_host_param.ip        = ip_address;
    g_current_voip_host_param.netmask   = ip_mask;
    g_current_voip_host_param.gateway   = gate_way;
    g_current_voip_host_param.vid       = vid;
    g_current_voip_host_param.pbits     = pbits;    

    return l_ret;
}

/******************************************************************************
 *
 * Function   : VOIP_updateHostIpAddr
 *              
 * Description: This routine updates host IP address
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_updateHostIpAddr_F(UINT32 ip_address, UINT32 ip_mask, UINT32 gate_way)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%s In, ip_address[0x%x] ip_mask[0x%x] gate_way[0x%x]----->\r\n", 
                       __FUNCTION__, ip_address, ip_mask, gate_way);

    /* parses the SIP XML file and places into memory database */
    if (xml_init_config(configuration_src) == ERROR)
        return ERROR;

    /* updates database to set host IP address      */
    updateHostIpAddr(VOIP_osInetNtoa_F(ip_address));

    /* Writes the SIP XML file back to file system  */
    if (make_xml_file(configuration_dst) != 0)
    {
        printf("%s: make_xml_file failed\n", __FUNCTION__);
        return ERROR;
    }

    /* Save IP, netmask, gateway */
    g_voip_host_param.ip        = ip_address;
    g_voip_host_param.netmask   = ip_mask;
    g_voip_host_param.gateway   = gate_way;    

    /* Set IP host parameters */
    if (g_voip_host_param.ip != 0)
    {
        VOIP_configIpHostParam_F(g_voip_host_param.ip, g_voip_host_param.netmask, g_voip_host_param.gateway,
                                 g_voip_host_param.vid, g_voip_host_param.pbits);
    }

    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%s out <-----\r\n", __FUNCTION__);    

    return OK;
}

/******************************************************************************
 *
 * Function   : VOIP_updateHostVidPbits
 *              
 * Description: This routine updates VID and P-bits of host IP address
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_updateHostVidPbits_F(int vid, int pbits)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%s In, vid[%d] pbits[%d] ----->\r\n", __FUNCTION__, vid, pbits);

    /* Save VID and P-bits */
    g_voip_host_param.vid   = vid;
    g_voip_host_param.pbits = pbits;  
 
    /* Set IP host parameters */
    if (g_voip_host_param.ip)
    {
        VOIP_configIpHostParam_F(g_voip_host_param.ip, g_voip_host_param.netmask, g_voip_host_param.gateway,
                                 g_voip_host_param.vid, g_voip_host_param.pbits);
    }
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%s out <-----\r\n", __FUNCTION__);

    return OK;
}

/******************************************************************************
 *
 * Function   : VOIP_initIpHostParam
 *              
 * Description: This routine initialize VoIP IP host parameters
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_initIpHostParam_F(void)
{
    memset(&g_voip_host_param,         0, sizeof(g_voip_host_param));
    memset(&g_current_voip_host_param, 0, sizeof(g_current_voip_host_param));    
    
    return OK;   
}

/******************************************************************************
 *
 * Function   : VOIP_updateSipServerAddr
 *              
 * Description: This routine updates proxy server address
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_updateSipServerAddr_F(int potsPort, char *proxy)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%s In, potsPort[%d] proxy[%s]----->\r\n", __FUNCTION__, potsPort, proxy);

    /* parses the SIP XML file and places into memory database */
    if (xml_init_config(configuration_src) == ERROR)
        return ERROR;

    /* updates database to set host IP address      */
    updateSipServerAddr(potsPort, proxy);

    /* Writes the SIP XML file back to file system  */
    if (make_xml_file(configuration_dst) != 0)
    {
        printf("%s: make_xml_file failed\n", __FUNCTION__);
        return ERROR;
    }
    
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%s out <-----\r\n", __FUNCTION__);

    return OK;
}

/******************************************************************************
 *
 * Function   : updateSipAccountData
 *              
 * Description: This routine updates the account data per POTS port
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_updateSipAccountData_F(int potsPort, char *userName, char *password, char *aor)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%s In, potsPort[%d] aor[%s]----->\r\n", __FUNCTION__, potsPort, aor);

    /* parses the SIP XML file and places into memory database */
    if (xml_init_config(configuration_src) == ERROR)
        return ERROR;

    /* updates database to set SIP account      */
    updateSipAccountData(potsPort, userName, password, aor);

    /* Writes the SIP XML file back to file system  */
    if (make_xml_file(configuration_dst) != 0)
    {
        printf("%s: make_xml_file failed\n", __FUNCTION__);
        return ERROR;
    }

    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%s out <-----\r\n", __FUNCTION__);

    return OK;
}

INT32 VOIP_setPortActiveState_F(UINT32 port_id, VOIP_ST_PORT_ACTIVE_STATE *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: port_id %d port active state %d\r\n",port_id,*info);
    return OK;
}

INT32 VOIP_getPortActiveState_F(UINT32 port_id, VOIP_ST_PORT_ACTIVE_STATE *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: port_id %d\r\n",port_id);
    return OK;
}

INT32 VOIP_getIadInfo_F(VOIP_ST_IADINFO *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    return OK;
}

INT32 VOIP_setH248ParamConfig_F(VOIP_ST_H248_PARAMETER_CONFIG *info)
{
    int i =0;
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: h248 parameter config\r\n");
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"mg_port %d mgcip 0x%ux mgccom_port_num %d \
                       back_mgcip 0x%ux back_mgccom_port_num %d \r\n", info->mg_port, info->mgcip, info->mgccom_port_num,info->back_mgcip, info->back_mgccom_port_num);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"active_mgc %d reg_mode %d heart_beat_mode %d \
                       heart_beat_cycle %d, heart_beat_count %d \r\n", info->active_mgc, info->reg_mode, \
                       info->heart_beat_mode,info->heart_beat_cycle, info->heart_beat_count);

    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"mid:\r\n");
    for(i=0;i<VOIP_H248_MID_SIZE;i++)
    {
        VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%d ",info->mid[i]);
        if(7==(i%8))
        {
            VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"\r\n");
        }
    }
    return OK;
}

INT32 VOIP_getH248ParamConfig_F(VOIP_ST_H248_PARAMETER_CONFIG *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    return OK;
}

INT32 VOIP_setH248UserTIDInfo_F(UINT32 port_id, VOIP_ST_H248_USER_TIDINFO *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: port_id %d user tidinfo%s\r\n",port_id, info->user_tid_name);
    return OK;
}

INT32 VOIP_getH248UserTIDInfo_F(UINT32 port_id, VOIP_ST_H248_USER_TIDINFO *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: port_id %d\r\n",port_id);
    return OK;
}

INT32 VOIP_setH248RtpTIDConfig_F(VOIP_ST_H248_RTP_TID_CONFIG *info)
{
    int i = 0;
    
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: h248 rtp tid config\r\n");
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"rtp_tid_mode %d\r\n",info->rtp_tid_mode);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"rtp_tid_digit_length %d\r\n",info->rtp_tid_digit_length);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"num_of_rtp_tid %d\r\n",info->num_of_rtp_tid);

    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"rtp_tid_prefix:\r\n");
    for(i=0;i<info->num_of_rtp_tid;i++)
    {
        
        VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%d ",info->rtp_tid_prefix[i]);
        if(7==(i%8))
        {
            VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"\r\n");
        }
    }

    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"rtp_tid_digit_begin:\r\n");
    for(i=0;i<info->num_of_rtp_tid;i++)
    {
        
        VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%d ",info->rtp_tid_digit_begin[i]);
        if(7==(i%8))
        {
            VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"\r\n");
        }
    }
    
    return OK;
}

INT32 VOIP_getH248RtpTIDConfig_F(VOIP_ST_H248_RTP_TID_CONFIG *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    return OK;
}

INT32 VOIP_setSipParamConfig_F(VOIP_ST_SIP_PARAMETER_CONFIG *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"mg_port %d server_ip 0x%ux serv_com_port %d \
                       back_server_ip 0x%ux back_serv_com_port%d\r\n",info->mg_port,info->server_ip,info->serv_com_port,\
                       info->back_server_ip, info->back_serv_com_port);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"active_proxy_server %d reg_server_ip 0x%ux reg_serv_com_port %d \
                       back_reg_server_ip 0x%ux back_reg_serv_com_port%d\r\n",info->active_proxy_server,info->reg_server_ip,info->reg_serv_com_port,\
                       info->back_reg_server_ip, info->back_reg_serv_com_port);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"outbound_server_ip 0x%ux outbound_serv_com_port %d \
                       reg_interval %d \r\n",info->outbound_server_ip,info->outbound_serv_com_port,info->reg_interval);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"heart_beat_switch %d heart_beat_cycle %d \
                       heart_beat_count %d",info->heart_beat_switch,info->heart_beat_cycle,info->heart_beat_count);
    return OK;
}

INT32 VOIP_getSipParamConfig_F(VOIP_ST_SIP_PARAMETER_CONFIG *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    return OK;
}

INT32 VOIP_setSipUserParamConfig_F(UINT32 port_id, VOIP_ST_SIP_USER_PARAMETER_CONFIG *info)
{
    int i = 0;
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: port_id %d\r\n",port_id);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: sip user parameter config\r\n");
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"user_name %s\r\n",info->user_name);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"passwd %s\r\n",info->passwd);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"sip_port_num:\r\n");
    for(i=0;i<VOIP_SIP_PORT_NUM_SIZE;i++)
    {
        VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%d ",info->sip_port_num[i]);
        if(7==(i%8))
        {
            VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"\r\n");
        }
    }
        
    return OK;
}

INT32 VOIP_getSipUserParamConfig_F(UINT32 port_id, VOIP_ST_SIP_USER_PARAMETER_CONFIG *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: port_id %d\r\n",port_id);
    return OK;
}

INT32 VOIP_setFaxModemConfig_F(VOIP_ST_FAX_MODEM_CONFIG *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: fax modem config t38_enable %d fax_control %d\r\n",\
    info->t38_enable,info->fax_control);
    return OK;
}

INT32 VOIP_getFaxModemConfig_F(VOIP_ST_FAX_MODEM_CONFIG *info)                                                        
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    return OK;
}

INT32 VOIP_getH248IadOperStatus_F(VOIP_ST_H248_IAD_OPERATION_STATUS *info)                                    
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    return OK;
}

INT32 VOIP_getPotsStatus_F(UINT32 port_id, VOIP_ST_POTS_STATUS *info)                                                
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: port_id %d\r\n",port_id);
    return OK;
}

INT32 VOIP_setIadOperation_F(VOIP_ST_IAD_OPERATION *info)                                                                
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: iad operation %d\r\n",*info);
    return OK;
}

INT32 VOIP_setSipDigitmap_F(VOIP_ST_SIP_DIGITAL_MAP *info)                                                                
{
    int i = 0;
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: sip digital map - sip_unit[]:\r\n");
    for(i=0;i<VOIP_SIP_DIGIT_UNIT_SIZE;i++)
    {
        VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"%d ",info->sip_unit[i]);
        if(7==(i%8))
        {
            VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"\r\n");
        }
    }
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"\r\n");
    return OK;
}

INT32 VOIP_getH248RtpTIDInfo_F(VOIP_ST_H248_RTP_TID_INFO *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: num_of_rtp_tid %d rtp_tid_name %s\r\n",info->num_of_rtp_tid, info->rtp_tid_name);
    return OK;
}

INT32 VOIP_setGlobalParam_F(VOIP_GLOBAL_PARAM_T *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"Input parameter: global parameter config\r\n");
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"voice_ip_mode %d iad_ip_addr %d iad_net_mask %d iad_def_gw %d pppoe_mode %d\r\n",\
    info->voice_ip_mode,info->iad_ip_addr,info->iad_net_mask,info->iad_def_gw,info->pppoe_mode);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"pppoe_user %s\r\n",info->pppoe_user);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"pppoe_passwd %s\r\n",info->pppoe_passwd);
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"vlan_tag_mode %d cvlan_id %d svlan_id %d priority %d\r\n",\
    info->vlan_tag_mode,info->cvlan_id,info->svlan_id,info->priority);
    return OK;
}

INT32 VOIP_getGlobalParam_F(VOIP_GLOBAL_PARAM_T *info)
{
    VOIP_log_printf(VOIP_LOG_MODULE,VOIP_LOG_DEBUG,"In file %s line %d, function %s is not implemented!\r\n",__FILE__,__LINE__,__func__);
    return OK;
}
