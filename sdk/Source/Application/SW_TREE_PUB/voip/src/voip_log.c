#include <stdio.h>
#include "voip_log.h"
#include "voip_comm.h"

/*--------------------------------------------------*/
static VOIP_LOG_SEVERITY_T      g_voip_log_level        = VOIP_LOG_DEBUG;
static VOIP_LOG_TERMINAL_T      g_voip_log_terminal     = VOIP_LOG_TO_STDOUT    ;/*out log terminal mode*/
static INT8                     g_voip_log_trace_normal = 0;

/*---------------------------------------------------*/
static INT32 VOIP_log_trace_check_if_valid();
/*---------------------------------------------------*/
/*log level*/
void VOIP_log_severity_level_set(VOIP_LOG_SEVERITY_T level)
{
    if(level <= VOIP_LOG_DEBUG_PKT )
    {
        g_voip_log_level = level ;
    }
}

VOIP_LOG_SEVERITY_T VOIP_log_severity_level_get()
{

    return g_voip_log_level ;
}

/*log terminal mode ,to tty or file*/
void VOIP_log_terminal_mode_set(VOIP_LOG_TERMINAL_T mode)
{
    if(mode <= VOIP_LOG_TO_STDOUT )
    {
        g_voip_log_terminal = mode ;
    }
}

VOIP_LOG_TERMINAL_T VOIP_log_terminal_mode_get()
{
    return g_voip_log_terminal ;
}

/*log terminal implementation*/
static void VOIP_LOG_TERMINAL_Tty_out(const char *format, va_list param)
{
    vprintf(format, param); 
}

static void VOIP_LOG_TERMINAL_Traceout(const char *format, va_list param)
{
    FILE *fd = 0;
    
    if(VOIP_log_trace_check_if_valid() != VOIP_EXIT_OK)
    {
        printf("voip log terminal tracerout not valid\r\n");        

        return ;
    }

    fd =fopen("/tmp/voip_config.log", "a");
    if(fd != NULL)
    {
        vfprintf(fd, format, param);

        fclose(fd);        
    }
}


void VOIP_log_printf(const char *func_name, VOIP_LOG_SEVERITY_T level, const char *format, ...)
{ 
    va_list arg_ptr; 
    
    if(VOIP_log_severity_level_get() < level)
    {
        return;
    }

    if(VOIP_log_terminal_mode_get() == VOIP_LOG_TO_FILE)
    {
        va_start(arg_ptr, format); 
        VOIP_LOG_TERMINAL_Traceout(format, arg_ptr); 
        va_end(arg_ptr); 
    }
    else if(VOIP_log_terminal_mode_get() == VOIP_LOG_TO_STDOUT)
    {
        va_start(arg_ptr, format); 
        VOIP_LOG_TERMINAL_Tty_out  (format, arg_ptr); 
        va_end(arg_ptr); 
    }
}

/*-------------------------------------------------------------------------------
* LOG AND TRACEOUT INNER HELP API 
*-------------------------------------------------------------------------------*/
static INT32 VOIP_log_trace_check_if_valid()
{
    if(g_voip_log_trace_normal != TRUE)
    {
        printf("Error,voip log traceout is not initialzed!\r\n"); 

        return VOIP_ERROR_NOT_INITIALIZED ;
    }

    return VOIP_EXIT_OK;
}

/*-------------------------------------------------------------------------------
* LOG AND TRACEOUT INIT VIEW
*-------------------------------------------------------------------------------*/
INT32 VOIP_log_init()
{
    static FILE       *fd = NULL; 
    
    fd =fopen("/tmp/voip_config.log", "r"); 
    if(fd==NULL)  
    { 
        printf("Error note :\n");
        printf("    log file '/tmp/voip_config.log'failed\n");
        printf("    the log dump function will be affected.\n");
        printf("    pls check the entir-variable or check log file\n");

        g_voip_log_trace_normal = 0;
    } 
    else
    {
        g_voip_log_trace_normal = TRUE;

        fclose(fd);
    }
    return 0;
}


INT32 VOIP_STACK_log_terminate()
{
    g_voip_log_trace_normal = 0;    
    return 0;
}

