/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2010, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : VOIP                                                      **/
/**                                                                          **/
/**  FILE        : VOIP_log.h                                               **/
/**                                                                          **/
/**  DESCRIPTION : Definition of VoIP module                                 **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *                 															  
 *         Ken  - initial version created.   14/March/2011          
 *                                                                              
 ******************************************************************************/

#ifndef _VOIP_LOG_H_
#define _VOIP_LOG_H_


#include <stdarg.h>
#include "stdio.h"
#include "globals.h"

#define VOIP_LOG_MODULE  "VOIP"

typedef enum
{
	VOIP_LOG_NONE = 0x00,
	VOIP_LOG_ERROR,		
	VOIP_LOG_ALARM,		/*such alarm not importance*/
	VOIP_LOG_INFO , /*informat ,such as pkt received*/
	VOIP_LOG_DEBUG,
	VOIP_LOG_DEBUG_EXTPKT,	
	VOIP_LOG_DEBUG_PKT,
}VOIP_LOG_SEVERITY_T;

typedef enum
{
	VOIP_LOG_TO_FILE = 0x00 ,
	VOIP_LOG_TO_STDOUT ,
}VOIP_LOG_TERMINAL_T;


void  VOIP_log_printf(const char * func_name, VOIP_LOG_SEVERITY_T level,const char * format, ...);
INT32 VOIP_log_init();

#endif
