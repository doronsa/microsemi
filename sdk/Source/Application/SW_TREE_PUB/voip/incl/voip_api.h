/******************************************************************************/
/**                                                                          **/
/**  COPYRIGHT (C) 2011, Marvell Semiconductors Inc.                         **/ 
/**--------------------------------------------------------------------------**/
/******************************************************************************/
/******************************************************************************/
/**                                                                          **/
/**  MODULE      : VOIP                                                      **/
/**                                                                          **/
/**  FILE        : voip_interface.h                                          **/
/**                                                                          **/
/**  DESCRIPTION : Definition of VOIP interfaces                             **/
/**                                                                          **/
/****************************************************************************** 
 **                                                                             
 *   MODIFICATION HISTORY:                                                      
 *                                                                               
 *         Ken  - initial version created.   10/March/2010           
 *                                                                              
 ******************************************************************************/
#ifndef _VOIP_INTERFACE_H
#define _VOIP_INTERFACE_H


#include "globals.h"



enum VOIP_management_object_port_type_t {
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_NONE = 0x00,
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_ETHERNET_PORT = 0x01,
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_VOIP_PORT = 0x02,
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_ADSL2_PLUS_PORT = 0x03,
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_VDSL2_PORT = 0x04,
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_E1_PORT = 0x05,
};
typedef enum VOIP_management_object_port_type_t VOIP_management_object_port_type_t;

enum VOIP_ST_PORT_ACTIVE_STATE {
    VOIP_ETH_PORT_ADMIN_DEACTIVATE = 0x00,
    VOIP_ETH_PORT_ADMIN_ACTIVATE = 0x01,
};
typedef enum VOIP_ST_PORT_ACTIVE_STATE VOIP_ST_PORT_ACTIVE_STATE;

enum VOIP_PROTOCOL_T {
    VOIP_PROTOCOL_H248 = 0x00,
    VOIP_PROTOCOL_SIP = 0x01,
};
typedef enum VOIP_PROTOCOL_T VOIP_PROTOCOL_T;
#define IGMP_BYTES_IN_MAC_ADDRESS 6
#define VOIP_SW_VERSION_SIZE 32
#define VOIP_SW_TIME_SIZE 32
#define VOIP_DEFAULT_UNTAG_RULE   (4096+1)

#define VOIP_PBITS_NO_CARE_VALUE 8

typedef UINT8 VOIP_ETH_MAC_T[IGMP_BYTES_IN_MAC_ADDRESS];

struct VOIP_ST_IADINFO {
    VOIP_ETH_MAC_T mac_address;
    VOIP_PROTOCOL_T voip_protocol;
    UINT8 sw_version[VOIP_SW_VERSION_SIZE];
    UINT8 sw_time[VOIP_SW_TIME_SIZE];
    UINT8 user_count;
};
typedef struct VOIP_ST_IADINFO VOIP_ST_IADINFO;
#define VOIP_H248_MID_SIZE 64

enum VOIP_H248_ACTIVE_MGC_T {
    VOIP_H248_ACTIVE_MGC_BACKUP = 0,
    VOIP_H248_ACTIVE_MGC_PRIMARY = 1,
};
typedef enum VOIP_H248_ACTIVE_MGC_T VOIP_H248_ACTIVE_MGC_T;

enum VOIP_H248_REG_MODE_T {
    VOIP_H248_REG_MODE_IP = 0,
    VOIP_H248_REG_MODE_DOMAIN = 1,
    VOIP_H248_REG_MODE_DEVICE_NAME = 2,
};
typedef enum VOIP_H248_REG_MODE_T VOIP_H248_REG_MODE_T;

enum VOIP_H248_HEART_BEAT_MODE_T {
    VOIP_H248_HEART_BEAT_MODE_OFF = 0,
    VOIP_H248_HEART_BEAT_MODE_STO = 1,
    VOIP_H248_HEART_BEAT_MODE_OTHER = 0xFF,
};
typedef enum VOIP_H248_HEART_BEAT_MODE_T VOIP_H248_HEART_BEAT_MODE_T;

struct VOIP_ST_H248_PARAMETER_CONFIG {
    UINT16 mg_port;
    UINT32 mgcip;
    UINT32 mgccom_port_num;
    UINT32 back_mgcip;
    UINT16 back_mgccom_port_num;
    VOIP_H248_ACTIVE_MGC_T active_mgc;
    VOIP_H248_REG_MODE_T reg_mode;
    UINT8 mid[VOIP_H248_MID_SIZE];
    VOIP_H248_HEART_BEAT_MODE_T heart_beat_mode;
    UINT16 heart_beat_cycle;
    UINT8 heart_beat_count;
};
typedef struct VOIP_ST_H248_PARAMETER_CONFIG VOIP_ST_H248_PARAMETER_CONFIG;
#define VOIP_H248_USER_TID_NAME_SIZE 32

struct VOIP_ST_H248_USER_TIDINFO {
    UINT8 user_tid_name[VOIP_H248_USER_TID_NAME_SIZE];
};
typedef struct VOIP_ST_H248_USER_TIDINFO VOIP_ST_H248_USER_TIDINFO;
#define VOIP_H248_RTP_TID_PREFIX_SIZE 16
#define VOIP_H248_RTP_TID_DIGIT_SIZE 8

enum VOIP_H248_RTP_TID_ALIGNED_TYPE_T {
    VOIP_H248_RTP_TID_MODE_ALIGNED = 0x00,
    VOIP_H248_RTP_TID_MODE_UNALIGNED = 0x01,
};
typedef enum VOIP_H248_RTP_TID_ALIGNED_TYPE_T VOIP_H248_RTP_TID_ALIGNED_TYPE_T;

struct VOIP_ST_H248_RTP_TID_CONFIG {
    UINT16 num_of_rtp_tid;
    UINT8 rtp_tid_prefix[VOIP_H248_RTP_TID_PREFIX_SIZE];
    UINT8 rtp_tid_digit_begin[VOIP_H248_RTP_TID_DIGIT_SIZE];
    VOIP_H248_RTP_TID_ALIGNED_TYPE_T rtp_tid_mode;
    UINT16 rtp_tid_digit_length;
};
typedef struct VOIP_ST_H248_RTP_TID_CONFIG VOIP_ST_H248_RTP_TID_CONFIG;

enum VOIP_SIP_HEART_BEAT_T {
    VOIP_SIP_HEART_BEAT_ON = 0,
    VOIP_SIP_HEART_BEAT_OFF = 1,
};
typedef enum VOIP_SIP_HEART_BEAT_T VOIP_SIP_HEART_BEAT_T;

struct VOIP_ST_SIP_PARAMETER_CONFIG {
    UINT16 mg_port;
    UINT32 server_ip;
    UINT32 serv_com_port;
    UINT32 back_server_ip;
    UINT32 back_serv_com_port;
    UINT32 active_proxy_server;
    UINT32 reg_server_ip;
    UINT32 reg_serv_com_port;
    UINT32 back_reg_server_ip;
    UINT32 back_reg_serv_com_port;
    UINT32 outbound_server_ip;
    UINT32 outbound_serv_com_port;
    UINT32 reg_interval;
    VOIP_SIP_HEART_BEAT_T heart_beat_switch;
    UINT16 heart_beat_cycle;
    UINT16 heart_beat_count;
};
typedef struct VOIP_ST_SIP_PARAMETER_CONFIG VOIP_ST_SIP_PARAMETER_CONFIG;
#define VOIP_SIP_PORT_NUM_SIZE 16
#define VOIP_SIP_USER_NAME_SIZE 32
#define VOIP_SIP_PASSWD_SIZE 16

struct VOIP_ST_SIP_USER_PARAMETER_CONFIG {
    UINT8 sip_port_num[VOIP_SIP_PORT_NUM_SIZE];
    UINT8 user_name[VOIP_SIP_USER_NAME_SIZE];
    UINT8 passwd[VOIP_SIP_PASSWD_SIZE];
};
typedef struct VOIP_ST_SIP_USER_PARAMETER_CONFIG VOIP_ST_SIP_USER_PARAMETER_CONFIG;

enum VOIP_FAX_MODE_T {
    VOIP_FAX_MODE_TRANSPARENT = 0,
    VOIP_FAX_MODE_T38 = 1,
};
typedef enum VOIP_FAX_MODE_T VOIP_FAX_MODE_T;

enum VOIP_FAX_CONTROL_T {
    VOIP_FAX_CONTROL_NEGOTIATED = 0,
    VOIP_FAX_CONTROL_AUTO_VBD = 1,
};
typedef enum VOIP_FAX_CONTROL_T VOIP_FAX_CONTROL_T;

struct VOIP_ST_FAX_MODEM_CONFIG {
    VOIP_FAX_MODE_T t38_enable;
    VOIP_FAX_CONTROL_T fax_control;
};
typedef struct VOIP_ST_FAX_MODEM_CONFIG VOIP_ST_FAX_MODEM_CONFIG;

enum VOIP_ST_H248_IAD_OPERATION_STATUS {
    VOIP_h248_OPER_STATUS_REGISTERING = 0,
    VOIP_h248_OPER_STATUS_REGISTERED = 1,
    VOIP_h248_OPER_STATUS_FAULT = 2,
    VOIP_h248_OPER_STATUS_DEREGISTER = 3,
    VOIP_h248_OPER_STATUS_REBOOT = 4,
    VOIP_IAD_OPER_STATUS_OTHER = 255,
};
typedef enum VOIP_ST_H248_IAD_OPERATION_STATUS VOIP_ST_H248_IAD_OPERATION_STATUS;

enum VOIP_IAD_PORT_STATUS_T {
    VOIP_IAD_PORT_STATUS_REGISTERING = 0,
    VOIP_IAD_PORT_STATUS_IDLE = 1,
    VOIP_IAD_PORT_STATUS_OFFHOOK = 2,
    VOIP_IAD_PORT_STATUS_DAILING = 3,
    VOIP_IAD_PORT_STATUS_RINGING = 4,
    VOIP_IAD_PORT_STATUS_RINGBACK = 5,
    VOIP_IAD_PORT_STATUS_CONNECTING = 6,
    VOIP_IAD_PORT_STATUS_CONNECTED = 7,
    VOIP_IAD_PORT_STATUS_DISCONNECTING = 8,
    VOIP_IAD_PORT_STATUS_REGISTER_FAILED = 9,
    VOIP_IAD_PORT_STATUS_INACTIVE = 10,
};
typedef enum VOIP_IAD_PORT_STATUS_T VOIP_IAD_PORT_STATUS_T;

enum VOIP_IAD_PORT_SERVICE_STATUS_T {
    VOIP_IAD_PORT_SERVICE_STATUS_END_LOCAL = 0,
    VOIP_IAD_PORT_SERVICE_STATUS_END_REMOTE = 1,
    VOIP_IAD_PORT_SERVICE_STATUS_END_AUTO = 2,
    VOIP_IAD_PORT_SERVICE_STATUS_NORMAL = 3,
};
typedef enum VOIP_IAD_PORT_SERVICE_STATUS_T VOIP_IAD_PORT_SERVICE_STATUS_T;

enum VOIP_IAD_PORT_CODEC_MODE_T {
    VOIP_IAD_PORT_CODEC_G711A = 0,
    VOIP_IAD_PORT_CODEC_G729 = 1,
    VOIP_IAD_PORT_CODEC_G711U = 2,
    VOIP_IAD_PORT_CODEC_G723 = 3,
    VOIP_IAD_PORT_CODEC_G726 = 4,
    VOIP_IAD_PORT_CODEC_T38 = 5,
};
typedef enum VOIP_IAD_PORT_CODEC_MODE_T VOIP_IAD_PORT_CODEC_MODE_T;

struct VOIP_ST_POTS_STATUS {
    VOIP_IAD_PORT_STATUS_T port_status;
    VOIP_IAD_PORT_SERVICE_STATUS_T service_status;
    VOIP_IAD_PORT_CODEC_MODE_T codec_mode;
};
typedef struct VOIP_ST_POTS_STATUS VOIP_ST_POTS_STATUS;

enum VOIP_ST_IAD_OPERATION {
    VOIP_OPERATION_TYPE_REGISTER = 0,
    VOIP_OPERATION_TYPE_DEREGISTER = 1,
    VOIP_OPERATION_TYPE_RESET = 2,
};
typedef enum VOIP_ST_IAD_OPERATION VOIP_ST_IAD_OPERATION;
#define VOIP_SIP_DIGIT_UNIT_SIZE 1024

enum VOIP_SIP_digit_map_update_state_t {
    VOIP_SIP_DIGIT_MAP_UPDATE_IDLE = 0,
    VOIP_SIP_DIGIT_MAP_UPDATE_INPROGRESS = 0 + 1,
    VOIP_SIP_DIGIT_MAP_UPDATE_SUCCESSFUL = 0 + 2,
    VOIP_SIP_DIGIT_MAP_UPDATE_FAILED = 0 + 3,
};
typedef enum VOIP_SIP_digit_map_update_state_t VOIP_SIP_digit_map_update_state_t;

struct VOIP_ST_SIP_DIGITAL_MAP {
    UINT8 sip_unit[VOIP_SIP_DIGIT_UNIT_SIZE];
};
typedef struct VOIP_ST_SIP_DIGITAL_MAP VOIP_ST_SIP_DIGITAL_MAP;

struct VOIP_ST_H248_RTP_TID_INFO {
    UINT16 num_of_rtp_tid;
    UINT8 rtp_tid_name[32];
};
typedef struct VOIP_ST_H248_RTP_TID_INFO VOIP_ST_H248_RTP_TID_INFO;

enum VOIP_IP_MODE_E {
    VOIP_IP_MODE_STATIC_IP = 0,
    VOIP_IP_MODE_DHCP = 1,
    VOIP_IP_MODE_PPPOE = 2,
};
typedef enum VOIP_IP_MODE_E VOIP_IP_MODE_E;

enum VOIP_PPPOE_MODE_E {
    VOIP_PPPOE_MODE_AUTO = 0,
    VOIP_PPPOE_MODE_CHAP = 1,
    VOIP_PPPOE_MODE_PAP = 2,
};
typedef enum VOIP_PPPOE_MODE_E VOIP_PPPOE_MODE_E;

enum VOIP_VLAN_TAG_E {
    VOIP_TAGGED_FLAG_TRANSPARENT = 0,
    VOIP_TAGGED_FLAG_TAG = 1,
    VOIP_TAGGED_FLAG_VLAN_STACK = 2,
};
typedef enum VOIP_VLAN_TAG_E VOIP_VLAN_TAG_E;
#define VOIP_PPPOE_USER_LEN 32
#define VOIP_PPPOE_PASSWORD_LEN 32

struct VOIP_GLOBAL_PARAM_T {
    VOIP_IP_MODE_E voice_ip_mode;
    UINT32 iad_ip_addr;
    UINT32 iad_net_mask;
    UINT32 iad_def_gw;
    VOIP_PPPOE_MODE_E pppoe_mode;
    UINT8 pppoe_user[VOIP_PPPOE_USER_LEN];
    UINT8 pppoe_passwd[VOIP_PPPOE_PASSWORD_LEN];
    VOIP_VLAN_TAG_E vlan_tag_mode;
    UINT16 cvlan_id;
    UINT16 svlan_id;
    UINT16 priority;
};
typedef struct VOIP_GLOBAL_PARAM_T VOIP_GLOBAL_PARAM_T;

struct IPC_VOIP_ST_PORT_ACTIVE_STATE {
    long rcode;
    VOIP_ST_PORT_ACTIVE_STATE portActive;
};
typedef struct IPC_VOIP_ST_PORT_ACTIVE_STATE IPC_VOIP_ST_PORT_ACTIVE_STATE;

struct IPC_VOIP_ST_IADINFO {
    long rcode;
    VOIP_ST_IADINFO iadInfo;
};
typedef struct IPC_VOIP_ST_IADINFO IPC_VOIP_ST_IADINFO;

struct IPC_VOIP_GLOBAL_PARAM_T {
    long rcode;
    VOIP_GLOBAL_PARAM_T globalParam;
};
typedef struct IPC_VOIP_GLOBAL_PARAM_T IPC_VOIP_GLOBAL_PARAM_T;

struct IPC_VOIP_ST_H248_PARAMETER_CONFIG {
    long rcode;
    VOIP_ST_H248_PARAMETER_CONFIG h248Param;
};
typedef struct IPC_VOIP_ST_H248_PARAMETER_CONFIG IPC_VOIP_ST_H248_PARAMETER_CONFIG;

struct IPC_VOIP_ST_H248_USER_TIDINFO {
    long rcode;
    VOIP_ST_H248_USER_TIDINFO h248User;
};
typedef struct IPC_VOIP_ST_H248_USER_TIDINFO IPC_VOIP_ST_H248_USER_TIDINFO;

struct IPC_VOIP_ST_H248_RTP_TID_CONFIG {
    long rcode;
    VOIP_ST_H248_RTP_TID_CONFIG h248RtpTidCfg;
};
typedef struct IPC_VOIP_ST_H248_RTP_TID_CONFIG IPC_VOIP_ST_H248_RTP_TID_CONFIG;

struct IPC_VOIP_ST_H248_RTP_TID_INFO {
    long rcode;
    VOIP_ST_H248_RTP_TID_INFO h248RtpTidInfo;
};
typedef struct IPC_VOIP_ST_H248_RTP_TID_INFO IPC_VOIP_ST_H248_RTP_TID_INFO;

struct IPC_VOIP_ST_SIP_PARAMETER_CONFIG {
    long rcode;
    VOIP_ST_SIP_PARAMETER_CONFIG sipParamCfg;
};
typedef struct IPC_VOIP_ST_SIP_PARAMETER_CONFIG IPC_VOIP_ST_SIP_PARAMETER_CONFIG;

struct IPC_VOIP_ST_SIP_USER_PARAMETER_CONFIG {
    long rcode;
    VOIP_ST_SIP_USER_PARAMETER_CONFIG sipUserParamCfg;
};
typedef struct IPC_VOIP_ST_SIP_USER_PARAMETER_CONFIG IPC_VOIP_ST_SIP_USER_PARAMETER_CONFIG;

struct IPC_VOIP_ST_FAX_MODEM_CONFIG {
    long rcode;
    VOIP_ST_FAX_MODEM_CONFIG faxModem;
};
typedef struct IPC_VOIP_ST_FAX_MODEM_CONFIG IPC_VOIP_ST_FAX_MODEM_CONFIG;

struct IPC_VOIP_ST_H248_IAD_OPERATION_STATUS {
    long rcode;
    VOIP_ST_H248_IAD_OPERATION_STATUS h248Iad;
};
typedef struct IPC_VOIP_ST_H248_IAD_OPERATION_STATUS IPC_VOIP_ST_H248_IAD_OPERATION_STATUS;

struct IPC_VOIP_ST_POTS_STATUS {
    long rcode;
    VOIP_ST_POTS_STATUS potsStatus;
};
typedef struct IPC_VOIP_ST_POTS_STATUS IPC_VOIP_ST_POTS_STATUS;

struct voip_setportactivestate_f_1_argument {
    UINT32 arg1;
    IPC_VOIP_ST_PORT_ACTIVE_STATE arg2;
};
typedef struct voip_setportactivestate_f_1_argument voip_setportactivestate_f_1_argument;

struct voip_seth248usertidinfo_f_1_argument {
    UINT32 arg1;
    IPC_VOIP_ST_H248_USER_TIDINFO arg2;
};
typedef struct voip_seth248usertidinfo_f_1_argument voip_seth248usertidinfo_f_1_argument;

struct voip_setsipuserparamconfig_f_1_argument {
    UINT32 arg1;
    IPC_VOIP_ST_SIP_USER_PARAMETER_CONFIG arg2;
};
typedef struct voip_setsipuserparamconfig_f_1_argument voip_setsipuserparamconfig_f_1_argument;

struct voip_ip_host_f_l_argument {
    UINT32 ip;
    UINT32 netmask;
    UINT32 gateway;
    UINT32 vid;
    UINT32 pbits;
};
typedef struct voip_ip_host_f_l_argument voip_ip_host_f_l_argument;

struct voip_xml_node
{
    char *tagname;
    char tagvalue[64];    
    struct voip_xml_node *child;    
    int depth;
};

typedef struct voip_xml_node *voip_xml_node_t;

#ifdef DEBUG
#define VOIP_DBG(format, ...)  printf("%s(%d)" format "\n",__FUNCTION__,__LINE__, ##__VA_ARGS__)
#else
#define VOIP_DBG(format, ...)
#endif


#define SPRINT_TECHNIQUE 1

#ifdef SPRINT_TECHNIQUE
#define BLANK(n) {int j=n*4; for(j--;j>=0;j--) sprintf(xmlBuffer, "%s%c", xmlBuffer, 32);}
#else
#define BLANK(n) {int j; for(j=0; j < n; j++) fprintf(xmlFile, "    ");}
#endif

#define BUFSZ 64

#if 0
typedef enum
{
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_NONE                 = 0x00,
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_ETHERNET_PORT        = 0x01,
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_VOIP_PORT            = 0x02,
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_ADSL2_PLUS_PORT      = 0x03,
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_VDSL2_PORT           = 0x04,
    VOIP_MANAGEMENT_OBJECT_PORT_TYPE_E1_PORT              = 0x05,
}VOIP_management_object_port_type_t;

typedef enum
{
    VOIP_ETH_PORT_ADMIN_DEACTIVATE    = 0x00,
    VOIP_ETH_PORT_ADMIN_ACTIVATE    = 0x01,
}VOIP_ST_PORT_ACTIVE_STATE;

typedef enum
{
    VOIP_PROTOCOL_H248              = 0x00,
    VOIP_PROTOCOL_SIP               = 0x01,
}VOIP_PROTOCOL_T;

#define IGMP_BYTES_IN_MAC_ADDRESS 6
#define VOIP_SW_VERSION_SIZE                    32
#define VOIP_SW_TIME_SIZE                        32

typedef UINT8 VOIP_ETH_MAC_T [IGMP_BYTES_IN_MAC_ADDRESS];

typedef struct
{
    VOIP_ETH_MAC_T                mac_address;                      /**< The IAD MAC address */
    VOIP_PROTOCOL_T               voip_protocol;                    /**< The supported VOIP protocol */
    UINT8                         sw_version[VOIP_SW_VERSION_SIZE]; /**< The IAD software version */
    UINT8                         sw_time[VOIP_SW_TIME_SIZE];       /**< The LAD software time */
    UINT8                         user_count;                       /**< The VOIP user count */
}VOIP_ST_IADINFO;

#define VOIP_H248_MID_SIZE                        64

typedef enum
{
    VOIP_H248_ACTIVE_MGC_BACKUP         = 0,
    VOIP_H248_ACTIVE_MGC_PRIMARY         = 1
}VOIP_H248_ACTIVE_MGC_T;

typedef enum
{
    VOIP_H248_REG_MODE_IP                    = 0,
    VOIP_H248_REG_MODE_DOMAIN                = 1,
    VOIP_H248_REG_MODE_DEVICE_NAME           = 2
}VOIP_H248_REG_MODE_T;

typedef enum
{
    VOIP_H248_HEART_BEAT_MODE_OFF               = 0,
    VOIP_H248_HEART_BEAT_MODE_STO               = 1,
    VOIP_H248_HEART_BEAT_MODE_OTHER             = 0xFF
}VOIP_H248_HEART_BEAT_MODE_T;

typedef struct
{
    UINT16                            mg_port;                                      /**< MG port number */
    UINT32                            mgcip;                                        /**< Primary softswitch platform IP address */
    UINT16                            mgccom_port_num;                              /**< Primary softswitch platform port number */
    UINT32                             back_mgcip;                                   /**< Backup softswitch platform IP address */
    UINT16                            back_mgccom_port_num;                         /**< Backup softswitch platform port number */
    VOIP_H248_ACTIVE_MGC_T          active_mgc;                                   /**< Active MGC that ONU registered, 0x00-backup, 0x01:primary*/
    VOIP_H248_REG_MODE_T            reg_mode;                                     /**< Register mode:default MGCP domain name using mode */
    UINT8                             mid[VOIP_H248_MID_SIZE];                           /**< MG domain name */
    VOIP_H248_HEART_BEAT_MODE_T     heart_beat_mode;                            /**< Heart beat mode, default is 0x01 */
    UINT16                            heart_beat_cycle;                             /**< Heartbeat Cycle,default is 60s */
    UINT8                             heart_beat_count;                             /**< Heartbeat detect quantity,default is 3 times */
}VOIP_ST_H248_PARAMETER_CONFIG;

#define VOIP_H248_USER_TID_NAME_SIZE                32

typedef struct
{
    UINT8                             user_tid_name[VOIP_H248_USER_TID_NAME_SIZE];      /* User TID name */
}VOIP_ST_H248_USER_TIDINFO;

#define VOIP_H248_RTP_TID_PREFIX_SIZE            16
#define VOIP_H248_RTP_TID_DIGIT_SIZE              8

typedef enum
{
    VOIP_H248_RTP_TID_MODE_ALIGNED       = 0x00,
    VOIP_H248_RTP_TID_MODE_UNALIGNED    = 0x01
}VOIP_H248_RTP_TID_ALIGNED_TYPE_T;

typedef struct
{
    UINT8                       num_of_rtp_tid;                                     /**< number of RTP TID*/
    UINT8                       rtp_tid_prefix[VOIP_H248_RTP_TID_PREFIX_SIZE];            /**< RTP prefix(only for H.248) */
    UINT8                       rtp_tid_digit_begin[VOIP_H248_RTP_TID_DIGIT_SIZE];         /**< RTP TID digit begin value */
    VOIP_H248_RTP_TID_ALIGNED_TYPE_T        rtp_tid_mode;                               /**< RTP TID digit align mode */
    UINT8                       rtp_tid_digit_length;                               /**< RTP TID digit length, valid when mode=0*/
}VOIP_ST_H248_RTP_TID_CONFIG;

typedef enum
{
    VOIP_SIP_HEART_BEAT_ON                   = 0,
    VOIP_SIP_HEART_BEAT_OFF                  = 1
}VOIP_SIP_HEART_BEAT_T;

typedef struct
{
    UINT16                        mg_port;                         /**< MG port number */
    UINT32                        server_ip;                       /**< Active SIP agent server IP address  */
    UINT16                        serv_com_port;                   /**< Active SIP agent server port number */
    UINT32                        back_server_ip;                  /**< Standby SIP agent server IP address */
    UINT16                        back_serv_com_port;              /**< Standby SIP agent server port number */
    UINT32                        active_proxy_server;          /**< SIP Proxy that ONU registered*/
    UINT32                        reg_server_ip;                   /**< Active SIP register server IP address  */
    UINT16                        reg_serv_com_port;              /**< Active SIP register server port number */
    UINT32                        back_reg_server_ip;              /**< Standby SIP register server IP address */
    UINT16                        back_reg_serv_com_port;          /**< Standby SIP register server port number */
    UINT32                        outbound_server_ip;           /**< OutBound server IP address */
    UINT16                        outbound_serv_com_port;       /**< OutBound server port number */
    UINT32                        reg_interval;                    /**< Register refresh period, unit is second */
    VOIP_SIP_HEART_BEAT_T         heart_beat_switch;               /**< SIP Heartbeat enable switch,0-open,1-close */
    UINT16                        heart_beat_cycle;                /**< Heartbeat Cycle,default is 60s */
    UINT16                        heart_beat_count;                /**< Heartbeat detect quantity,default is 3 times */
}VOIP_ST_SIP_PARAMETER_CONFIG;

#define VOIP_SIP_PORT_NUM_SIZE                  16
#define VOIP_SIP_USER_NAME_SIZE                  32
#define VOIP_SIP_PASSWD_SIZE                      16

typedef struct
{
    UINT8                         sip_port_num[VOIP_SIP_PORT_NUM_SIZE];        /**< SIP port phone number */
    UINT8                         user_name[VOIP_SIP_USER_NAME_SIZE];          /**< SIP port user name */
    UINT8                         passwd[VOIP_SIP_PASSWD_SIZE];                /**< SIP port keyword */
}VOIP_ST_SIP_USER_PARAMETER_CONFIG;

typedef enum
{
    VOIP_FAX_MODE_TRANSPARENT                = 0,           
    VOIP_FAX_MODE_T38                        = 1
}VOIP_FAX_MODE_T;

typedef enum
{
    VOIP_FAX_CONTROL_NEGOTIATED         = 0,
    VOIP_FAX_CONTROL_AUTO_VBD           = 1
}VOIP_FAX_CONTROL_T;

typedef struct
{
    VOIP_FAX_MODE_T              t38_enable;          /**< voice T38 enable */
    VOIP_FAX_CONTROL_T          fax_control;         /**< voice Fax control */
}VOIP_ST_FAX_MODEM_CONFIG;

typedef enum
{
    VOIP_h248_OPER_STATUS_REGISTERING    = 0,
    VOIP_h248_OPER_STATUS_REGISTERED     = 1,
    VOIP_h248_OPER_STATUS_FAULT          = 2,
    VOIP_h248_OPER_STATUS_DEREGISTER     = 3,
    VOIP_h248_OPER_STATUS_REBOOT         = 4,
    VOIP_IAD_OPER_STATUS_OTHER          = 255
}VOIP_ST_H248_IAD_OPERATION_STATUS;

typedef enum
{
    VOIP_IAD_PORT_STATUS_REGISTERING    = 0,
    VOIP_IAD_PORT_STATUS_IDLE           = 1,
    VOIP_IAD_PORT_STATUS_OFFHOOK        = 2,
    VOIP_IAD_PORT_STATUS_DAILING        = 3,
    VOIP_IAD_PORT_STATUS_RINGING        = 4,
    VOIP_IAD_PORT_STATUS_RINGBACK       = 5,
    VOIP_IAD_PORT_STATUS_CONNECTING     = 6,
    VOIP_IAD_PORT_STATUS_CONNECTED      = 7,
    VOIP_IAD_PORT_STATUS_DISCONNECTING  = 8,
    VOIP_IAD_PORT_STATUS_REGISTER_FAILED= 9,
    VOIP_IAD_PORT_STATUS_INACTIVE       = 10
}VOIP_IAD_PORT_STATUS_T;

typedef enum
{
    VOIP_IAD_PORT_SERVICE_STATUS_END_LOCAL    = 0,
    VOIP_IAD_PORT_SERVICE_STATUS_END_REMOTE   = 1,
    VOIP_IAD_PORT_SERVICE_STATUS_END_AUTO     = 2,
    VOIP_IAD_PORT_SERVICE_STATUS_NORMAL       = 3
}VOIP_IAD_PORT_SERVICE_STATUS_T;

typedef enum
{
    VOIP_IAD_PORT_CODEC_G711A           = 0,
    VOIP_IAD_PORT_CODEC_G729            = 1,
    VOIP_IAD_PORT_CODEC_G711U           = 2,
    VOIP_IAD_PORT_CODEC_G723            = 3,
    VOIP_IAD_PORT_CODEC_G726            = 4,
    VOIP_IAD_PORT_CODEC_T38             = 5
}VOIP_IAD_PORT_CODEC_MODE_T;


typedef struct
{
    VOIP_IAD_PORT_STATUS_T          port_status;    /**< User port status */
    VOIP_IAD_PORT_SERVICE_STATUS_T  service_status; /**< User port service type */
    VOIP_IAD_PORT_CODEC_MODE_T      codec_mode;     /**< User port codec mode */
}VOIP_ST_POTS_STATUS;


typedef enum
{
    VOIP_OPERATION_TYPE_REGISTER            = 0,
    VOIP_OPERATION_TYPE_DEREGISTER          = 1,
    VOIP_OPERATION_TYPE_RESET               = 2
}VOIP_ST_IAD_OPERATION;

#define VOIP_SIP_DIGIT_UNIT_SIZE    1024

typedef enum
{
    VOIP_SIP_DIGIT_MAP_UPDATE_IDLE = 0,
    VOIP_SIP_DIGIT_MAP_UPDATE_INPROGRESS ,  
    VOIP_SIP_DIGIT_MAP_UPDATE_SUCCESSFUL ,  
    VOIP_SIP_DIGIT_MAP_UPDATE_FAILED ,  
}VOIP_SIP_digit_map_update_state_t;

typedef struct
{
    UINT8        sip_unit[VOIP_SIP_DIGIT_UNIT_SIZE];
}VOIP_ST_SIP_DIGITAL_MAP;


#define VOIP_H248_RTP_TID_NAME_SIZE            32
typedef struct
{
    unsigned char                         num_of_rtp_tid;                               /**< number of RTP TID*/
    unsigned char                         rtp_tid_name[VOIP_H248_RTP_TID_NAME_SIZE];    /**< RTP TID name(only for H.248) */
}VOIP_ST_H248_RTP_TID_INFO;


/*! \enum VOIP_IP_MODE_E
 *  \brief net mode of IAD device
 */
typedef enum
{
    VOIP_IP_MODE_STATIC_IP          = 0,
    VOIP_IP_MODE_DHCP               = 1,
    VOIP_IP_MODE_PPPOE              = 2
}VOIP_IP_MODE_E;

/*! \enum VOIP_PPPOE_MODE_E
 *  \brief net mode of IAD device
 */
typedef enum
{
    VOIP_PPPOE_MODE_AUTO                 = 0,           
    VOIP_PPPOE_MODE_CHAP                 = 1, /** Change Handshake Authentication Protocol */
    VOIP_PPPOE_MODE_PAP                  = 2  /** Password Authentication Protocol */
}VOIP_PPPOE_MODE_E;

/*! \enum VOIP_VLAN_TAG_E
 *  \brief vlan mode of IAD device
 */
typedef enum
{
    VOIP_TAGGED_FLAG_TRANSPARENT    = 0,           
    VOIP_TAGGED_FLAG_TAG            = 1,       
    VOIP_TAGGED_FLAG_VLAN_STACK     = 2  
}VOIP_VLAN_TAG_E;

#define VOIP_PPPOE_USER_LEN         32
#define VOIP_PPPOE_PASSWORD_LEN     32

/*! \struct VOIP_GLOBAL_PARAM_T
 *  \brief voip mg profile configure.
 */
typedef struct
{
    VOIP_IP_MODE_E              voice_ip_mode; /** The IP address configured mode*/
    UINT32                      iad_ip_addr;   /** The IAD IP address            */
    UINT32                      iad_net_mask;  /** The IAD NET mask              */
    UINT32                      iad_def_gw;    /** The IAD default gateway       */
    VOIP_PPPOE_MODE_E           pppoe_mode;    /** The PPPOE mode                */
    UINT8                       pppoe_user[VOIP_PPPOE_USER_LEN];       /** The PPPOE user name */
    UINT8                       pppoe_passwd[VOIP_PPPOE_PASSWORD_LEN];/** The PPPOE password  */
    VOIP_VLAN_TAG_E             vlan_tag_mode; /** Voice data transmit with flag */
    UINT16                      cvlan_id;      /** The CVLAN of voice data       */
    UINT16                      svlan_id;      /** The SVLAN of voice data       */
    UINT8                       priority;      /** The voice priority            */
}VOIP_GLOBAL_PARAM_T;
#endif
/******************************************************************************
 *
 * Function   : VOIP_updateSipTcpUdpPort
 *              
 * Description: This routine updates TCP or UDP port for IP host
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_updateSipTcpUdpPort_F(int sipPort);

/******************************************************************************
 *
 * Function   : VOIP_updateHostIpAddr
 *              
 * Description: This routine updates host IP address
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_updateHostIpAddr_F(UINT32 ip_address, UINT32 ip_mask, UINT32 gate_way);

/******************************************************************************
 *
 * Function   : VOIP_updateHostVidPbits
 *              
 * Description: This routine updates VID and P-bits of host IP address
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_updateHostVidPbits_F(int vid, int pbits);

/******************************************************************************
 *
 * Function   : VOIP_initIpHostParam
 *              
 * Description: This routine initialize VoIP IP host parameters
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_initIpHostParam_F(void);

/******************************************************************************
 *
 * Function   : VOIP_updateSipServerAddr
 *              
 * Description: This routine updates proxy server address
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_updateSipServerAddr_F(int potsPort, char *proxy);

/******************************************************************************
 *
 * Function   : updateSipAccountData
 *              
 * Description: This routine updates the account data per POTS port
 *              
 * Returns    : INT32
 *              
 ******************************************************************************/
INT32 VOIP_updateSipAccountData_F(int potsPort, char *userName, char *password, char *aor);


INT32 VOIP_setPortActiveState_F(UINT32 port_id, VOIP_ST_PORT_ACTIVE_STATE *info);
INT32 VOIP_getPortActiveState_F(UINT32 port_id, VOIP_ST_PORT_ACTIVE_STATE *info);
INT32 VOIP_getIadInfo_F(VOIP_ST_IADINFO *info);
INT32 VOIP_setGlobalParam_F(VOIP_GLOBAL_PARAM_T *info); /*TW not provided*/
INT32 VOIP_getGlobalParam_F(VOIP_GLOBAL_PARAM_T *info); /*TW not provided*/
INT32 VOIP_setH248ParamConfig_F(VOIP_ST_H248_PARAMETER_CONFIG *info);
INT32 VOIP_getH248ParamConfig_F(VOIP_ST_H248_PARAMETER_CONFIG *info);
INT32 VOIP_setH248UserTIDInfo_F(UINT32 port_id, VOIP_ST_H248_USER_TIDINFO *info);
INT32 VOIP_getH248UserTIDInfo_F(UINT32 port_id, VOIP_ST_H248_USER_TIDINFO *info);
INT32 VOIP_setH248RtpTIDConfig_F(VOIP_ST_H248_RTP_TID_CONFIG *info);
INT32 VOIP_getH248RtpTIDConfig_F(VOIP_ST_H248_RTP_TID_CONFIG *info);
INT32 VOIP_getH248RtpTIDInfo_F(VOIP_ST_H248_RTP_TID_INFO *info); /*TW not provided*/
INT32 VOIP_setSipParamConfig_F(VOIP_ST_SIP_PARAMETER_CONFIG *info);
INT32 VOIP_getSipParamConfig_F(VOIP_ST_SIP_PARAMETER_CONFIG *info);
INT32 VOIP_setSipUserParamConfig_F(UINT32 port_id, VOIP_ST_SIP_USER_PARAMETER_CONFIG *info);
INT32 VOIP_getSipUserParamConfig_F(UINT32 port_id, VOIP_ST_SIP_USER_PARAMETER_CONFIG *info);
INT32 VOIP_setFaxModemConfig_F(VOIP_ST_FAX_MODEM_CONFIG *info);
INT32 VOIP_getFaxModemConfig_F(VOIP_ST_FAX_MODEM_CONFIG *info);                                                        
INT32 VOIP_getH248IadOperStatus_F(VOIP_ST_H248_IAD_OPERATION_STATUS *info);                                    
INT32 VOIP_getPotsStatus_F(UINT32 port_id, VOIP_ST_POTS_STATUS *info);                                                
INT32 VOIP_setIadOperation_F(VOIP_ST_IAD_OPERATION *info);                                                                
INT32 VOIP_setSipDigitmap_F(VOIP_ST_SIP_DIGITAL_MAP *info);                                                                

#endif
