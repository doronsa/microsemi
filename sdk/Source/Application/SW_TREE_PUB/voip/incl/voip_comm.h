#ifndef _VOIP_COM_HEAD_H
#define _VOIP_COM_HEAD_H

#define VOIP_EXIT_OK				            				0

#define VOIP_ERROR_BASE								(-27000)
#define VOIP_ERROR_NOT_INITIALIZED			            			(VOIP_ERROR_BASE-13)
#endif