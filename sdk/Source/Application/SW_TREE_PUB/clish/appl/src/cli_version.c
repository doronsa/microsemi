/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include "../incl/main_cli.h"
#include "../incl/version.h"

#define SDK_VERSION_FILE    "/etc/version.txt"

/********************************************************************************/
/*                             Main CLI functions                               */
/********************************************************************************/
const char* get_SDK_sw_version(void)
{
    return SDK_VERSION;
}

const char* get_SDK_sw_version_build_date(void)
{
    return SDK_VERSION_DATE;
}

static bool read_version(char *version)
{
    FILE  *filehandle;
    char  line[200];
    char  *token;
    bool  rc = false;

    if ((filehandle = fopen(SDK_VERSION_FILE, "r")) == NULL)
    {
        printf("fopen %s failed. %s(%d)\n", SDK_VERSION_FILE, strerror(errno), errno);
        return false;
    }

    // Read in a line, cleanup \n\r at end of line and copy string
    if (fgets(line, sizeof(line), filehandle) != 0)
    {
        token = strtok(line, "\n\r");

        if (strlen(token) > 0)
        {
            strcpy(version, token);
            rc = true;
        }
    }

    if (fclose(filehandle) != 0)
    {
        printf("%s: fclose failed. %s(%d)\n", __FUNCTION__, strerror(errno), errno);
    }
    return rc;
}

bool_t  main_cli_print_version (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    char    sdk_ver[80];
    bool    rc;

    rc = read_version(sdk_ver);

    printf(" =========================== \n");
    printf(" SW application version %s\n", get_SDK_sw_version());
    if (rc == true)
    {
        printf(" SDK version %s\n", sdk_ver);
    }
    printf(" =========================== \n");
    return BOOL_TRUE;
}

bool_t  main_cli_print_module_versions (const clish_shell_t    *instance,
                                        const lub_argv_t       *argv)
{
    printf(" =========================== \n");
    printf(" SW version %s\n", get_SDK_sw_version());
    printf(" =========================== \n");
	#ifdef MMP_CLI
    printf(" VoIP MMP   %s\n", mmp_get_version());
	#endif
    printf(" =========================== \n");
    return BOOL_TRUE;
}

