/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/********************************************************************************
*      cli.c
*
* DESCRIPTION:
*
*
* CREATED BY:  ShpundA
*
* DATE CREATED: June 13, 2010
*
* DEPENDENCIES:
*
*
* FILE REVISION NUMBER:
*       $Revision: 1.6 $
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"
#include "main_cli.h"

#include "clish/../tinyrl/private.h"
#include "clish/shell/private.h"


#ifdef TPM_CLI
#include "tpm_cli.h"
#endif
#if 0
#ifdef TPMA_CLI
#include "tpma_cli.h"
#endif
#endif
#ifdef MNG_COM_CLI
#include "mng_com_cli.h"
#endif


#ifdef APM_CLI
#include "apm_cli.h"
#endif

#ifdef OMCI_CLI
#include "omci_cli.h"
#endif

#ifdef OAM_CLI
#include "oam_cli.h"
#endif
#ifdef IGMP_CLI
#include "igmp_Cli.h"
#endif
#ifdef MMP_CLI
#include "libmmpcli.h"
#endif
#ifdef FLASH_CLI
#include "flash_cli.h"
#endif
#ifdef MIDWARE_CLI
#include "midware_cli.h"
#endif
#ifdef I2C_CLI
#include "i2c_cli.h"
#endif

#ifdef LBDT_CLI
#include "lbdt_cli.h"
#endif

#include "edit_xml.h"

char    cli_version[] = "CLI v1.0";

extern int appl_shell (void);

/********************************************************************************/
/*                               CLI main part                                  */
/********************************************************************************/
static bool_t cli_shell_init_callback(const clish_shell_t *shell)
{
    return BOOL_TRUE;
}

static void cli_shell_shutdown_callback(const clish_shell_t *shell)
{
}

static void cli_shell_cmd_line_callback(const clish_shell_t    *instance,
                                            const char             *cmd_line)
{
}

static bool_t cli_shell_script_callback(const clish_shell_t *instance,
                                        const char          *script)
{
    return BOOL_TRUE;
}

static  bool_t cli_shell_access_callback(const clish_shell_t    *instance,
                                         const char             *access)
{
    return BOOL_TRUE;
}

static bool_t  print_cli_version (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    /*modified by ken, replace printf with tinyrl_printf*/
    tinyrl_printf(instance->tinyrl,"          %s\r\n", cli_version);
    tinyrl_printf(instance->tinyrl,"********************************************\r\n");
    return BOOL_TRUE;
}


const clish_shell_builtin_t cli_cmd_list [] =
{
#ifdef TPM_CLI
#include "tpm_cli_cmd_list_file.h"
#endif
//#ifdef TPMA_CLI
//#include "tpma_cli_cmd_list_file.h"
//
//#endif
#ifdef MNG_COM_CLI
#include "mng_com_cli_cmd_list_file.h"
#endif
#ifdef APM_CLI
#include "apm_cli_cmd_list_file.h"
#endif
#ifdef OMCI_CLI
#include "omci_cli_cmd_list_file.h"
#endif
#ifdef OAM_CLI
#include "oam_cli_cmd_list_file.h"
#endif
#ifdef IGMP_CLI
#include "igmp_cli_cmd_list_file.h"
#endif
#ifdef MMP_CLI
#include "mmp_cli_cmd_list_file.h"
#endif
#ifdef FLASH_CLI
#include "flash_cli_cmd_list_file.h"
#endif
#ifdef MIDWARE_CLI
#include "midware_cli_cmd_list_file.h"
#endif
#ifdef I2C_CLI
#include "i2c_cli_cmd_list_file.h"
#endif
#ifdef LBDT_CLI
#include "lbdt_cli_cmd_list_file.h"
#endif
#include "main_cli_cmd_list_file.h"
#include "edit_xml_cli_cmd_list_file_epon.h"
#include "edit_xml_cli_cmd_list_file_gpon.h"
#include "edit_xml_cli_cmd_list_file.h"
  {"print_cli_version",  print_cli_version},
  {"appl_shell",  appl_shell},
  {NULL,NULL}
};


static clish_shell_hooks_t  cli_hooks =
{
  cli_shell_init_callback,
  cli_shell_access_callback,
  cli_shell_cmd_line_callback,
  cli_shell_script_callback,
  cli_shell_shutdown_callback,
  cli_cmd_list
};


int cli_startup(void)
{
    int     result = 0;

    if(clish_telnet_server_spawn(&cli_hooks, NULL) == BOOL_FALSE)
    {
        printf("telnet server spawn failed, telnet function will be masked!!!\n");
    }


    if (clish_shell_spawn_and_wait(&cli_hooks, NULL) == BOOL_FALSE)
    {
        printf("The CLI process failed to be spawned\n");
        result = -1;
    }

    return result;
}

void cli_shutdown(void)
{
    clish_shutdown();
}


