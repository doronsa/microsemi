#include <stdio.h>
#include <rpc/rpc.h> /* always need this */
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/resource.h>

#include "common.h"
#include "tpm_types.h"
#include "params.h"
#include "cli.h"
#include "params_mng.h"
#ifdef MMP_CLI
#include "libmmpcli.h"
#endif
#include "globals.h"
#include "errorCode.h"
#include "OmciGlobalData.h"
#include "apm.h"
#include "mng_trace.h"
#include "ponOnuMngIf.h"

int main(int argc, char* argv[])
{
   int32_t            rcode;
   char              *path;
   int                sock = RPC_ANYSOCK;
   struct rlimit rlim;
   struct rlimit rlim_new;
   struct sockaddr_un addr;

#define RLIMIT_MSGQUEUE 12

    if (getrlimit(RLIMIT_MSGQUEUE , &rlim)==0)
    {
        rlim_new.rlim_cur = rlim_new.rlim_max = RLIM_INFINITY;
        if (setrlimit(RLIMIT_MSGQUEUE , &rlim_new)!=0)
        {
            /* failed. try raising just to the old max */
            rlim_new.rlim_cur = rlim_new.rlim_max = rlim.rlim_max;
            setrlimit(RLIMIT_MSGQUEUE , &rlim_new);
        }
    }

   tpmKernelExist(); /* Set TPM driver file description */
#ifdef MMP_CLI
   libmmpcli_init("/etc/xml_params/mmp_cfg.xml");
#endif
   //printf("############ CLI Client Running  ############\n");
   mipc_init("clish", 0, 0);
   tpm_cli_db_init();
   login_md5_groups_init();
   cli_startup();
   //mipc_start_default_server();
   cli_shutdown();
   mipc_deinit();
}


