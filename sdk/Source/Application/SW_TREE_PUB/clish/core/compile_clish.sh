#!/bin/sh

cd ${CLISH_DIR} #../../clish-0.7.3

echo "!!!!!!!!!!! CROSS_COMPILE = $CROSS_COMPILE"


######### Should be removed #################
chmod 755 autom4te.cache/requests
chmod 755 config.log
chmod 755 config.status
######### Should be removed #################


export CC=${CROSS_COMPILE}gcc
export LD=${CROSS_COMPILE}ld
export CXX=${CROSS_COMPILE}g++

hostoption="--host=arm-linux"
needs_distclean="no"
config_platform=`head config.log | grep '\-\-host\=arm\-linux'`




if [ "${CROSS_COMPILE}" != "" ];
then
echo "Configuring clish for arm-linux"
host_option="--host=arm-linux"
fi


if [ "${config_platform}" != "" ];
then
if [ "${CROSS_COMPILE}" = "" ];
then
echo "Need to run distclean first because clish was configured for arm previously and now we are compiling for x86"
needs_distclean="yes"
fi
fi

if [ "${config_platform}" = "" ];
then
if [ "${CROSS_COMPILE}" != "" ];
then
echo "Need to run distclean first because clish was configured for x86 previously and now we are compiling for arm"
needs_distclean="yes"
fi
fi

if [ ! -e "${CLISH_DIR}/.libs/libclish.a" ];
then 
if [ -e "${CLISH_DIR}/.libs/libclish.la" ];
then
echo "Need to run distclean first because clish libraries are not present but la files are present"
needs_distclean="yes"
fi
fi



if [ "${needs_distclean}" = "yes" ];
then
echo "Running make distclean"
make distclean
fi

config_status=`tail -1 config.log`

if [ "${config_status}" != "configure: exit 0" ];
then
echo "Running:./configure  ${host_option} --disable-shared --enable-debug"
./configure  ${host_option} --disable-shared --enable-debug
fi
make

