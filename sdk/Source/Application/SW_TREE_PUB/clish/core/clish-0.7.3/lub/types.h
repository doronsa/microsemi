/*
 * types.h
 */
/**
\ingroup  lub
\defgroup lub_types types

\brief This provides some primative types not found in ANSI-C.
@{
*/

#ifndef _lub_types_h
#define _lub_types_h


#include <rpc/types.h>
/**
 * A boolean type for ANSI-C
 */
/*
typedef enum
{
	BOOL_FALSE,
	BOOL_TRUE
} bool_t;

*/
#define BOOL_FALSE FALSE
#define BOOL_TRUE TRUE

/** @} */
#endif /* _lub_types_h */
