/*
 * shell_new.c
 */
#include "private.h"

#include <assert.h>
#include <stdlib.h>
/*-------------------------------------------------------- */
static void
clish_shell_init(clish_shell_t             *this,
                 const clish_shell_hooks_t *hooks,
                 void                      *cookie,
                 FILE                      *istream)
{
    /* initialise the tree of views */
    lub_bintree_init(&this->view_tree,
                    clish_view_bt_offset(),
                    clish_view_bt_compare,
                    clish_view_bt_getkey);

    /* initialise the tree of views */
    lub_bintree_init(&this->ptype_tree,
                    clish_ptype_bt_offset(),
                    clish_ptype_bt_compare,
                    clish_ptype_bt_getkey);

    assert((NULL != hooks) && (NULL != hooks->script_fn));
    
    /* set up defaults */
    this->client_hooks    = hooks;
    this->client_cookie   = cookie;
    this->view            = NULL;
    this->viewid          = NULL;
    this->global          = NULL;
    this->startup         = NULL;
    this->state           = SHELL_STATE_INITIALISING;
    this->overview        = NULL;
    clish_shell_iterator_init(&this->iter);
    /*
          !!! Warning:
          modified by ken, but just assume that only 2 connection way: serial and telnet.
          If file mode added, here must change.
      */
    if((fileno(istream)) == (fileno(stdin)))
    {
        this->tinyrl          = clish_shell_tinyrl_new(istream,
                                               stdout,
                                               0);
    }
    else
    {
        /*for telnet first initialized as stdout and will be updated later with the new function tinyrl__set_ostream()*/
    this->tinyrl          = clish_shell_tinyrl_new(istream,
                                                   stdout,
                                                   0);
    }
    this->current_file    = NULL;
}
/*-------------------------------------------------------- */
clish_shell_t *
clish_shell_new(const clish_shell_hooks_t *hooks,
                void                      *cookie,
                FILE                      *istream)
{
    clish_shell_t *this = malloc(sizeof(clish_shell_t));

    if(this)
    {
        clish_shell_init(this,hooks,cookie,istream);

        if(hooks->init_fn)
        {
            /* now call the client initialisation */
            if(BOOL_TRUE != hooks->init_fn(this))
            {
                this->state = SHELL_STATE_CLOSING;
            }
        }
    }
    return this;
}
/*-------------------------------------------------------- */
