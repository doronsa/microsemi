/*
 * shell_new.c
 */
#include "tinyrl/private.h"
#include "tinyrl/vt100.h"
#include "private.h"
#include "lub/string.h"
#include "clish/shell.h"
#include "clish/md5.h"


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <unistd.h>
#include <pthread.h>
#include <dirent.h>

#include<sys/types.h>   
#include<sys/socket.h>   
#include<netdb.h>   

#include <arpa/telnet.h>

#define DEFAULT_TELNET_PORT 23
#define MAX_TELNET_SESSION_NUM 4
#define MAX_TELNET_SESSION_OPTION_SEND_BYTE 6
#define MAX_TELNET_SESSION_OPTION_RCV_BYTE 100
#define TELNET_ECHO 1
#define TELNET_SUPPRESS_GO_AHEAD 3

/*
 * if CLISH_PATH is unset in the environment then this is the value used. 
 */
const char *default_path = "/etc/xml_commands";

const clish_shell_login_t cli_login_groups[LOGIN_GROUP_NUM] = 
{
    {"admin","admin"}
};
clish_shell_login_md5_t cli_login_md5_groups[LOGIN_GROUP_NUM]={0};

static int g_clish_telnet_session_num = 0;
static pthread_mutex_t g_clish_telnet_session_num_mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_key_t  g_clish_ostream_key;



/*-------------------------------------------------------- */
/* 
 * The context structure is used to simplify the cleanup of 
 * a CLI session when a thread is cancelled.
 */
typedef struct _context context_t;
struct _context
{
    pthread_t                  pthread;
    const clish_shell_hooks_t *hooks;
    void                      *cookie;
    FILE                      *istream;
    FILE                      *ostream;
    clish_shell_t             *shell;
    clish_pargv_t             *pargv;
    char                      *prompt;
};

typedef enum 
{
    NOT_FIND=0, 
    FIND_WONT, 
    FIND_DONT,
    FIND_WILL,
    FIND_DO
} FIND_ECHO;

/*find the option of echo in received messages*/
static FIND_ECHO find_echo_opt(unsigned char *rev_msg, int len);
/*find the options of echo and go ahead in received messages*/
static void find_echo_and_suppress_go_ahead_opt(unsigned char *rcv_msg, int len, FIND_ECHO * echo_opt, FIND_ECHO * suppress_go_ahead_opt);

/*the following 3 functions are used for socket communication */
static int connect_init(int port,int *listen_fd1); 
static int connect_sendMessage(int com_fd,char *buf,int len);
static int connect_receiveMessage(int com_fd,char *buf,int len);
    


/*-------------------------------------------------------- */
/* perform a simple tilde substitution for the home directory
 * defined in HOME
 */
static char *
clish_shell_tilde_expand(const char *path)
{
    char       *home_dir = getenv("HOME");
    char       *result   = NULL;
    const char *p        = path;
    const char *segment  = path;
    int         count    = 0;
    while(*p)
    {
        if('~' == *p)
        {
            if(count)
            {
                lub_string_catn(&result,segment,count);
                segment += (count + 1); /* skip the tilde in the path */
                count = -1;
            }
            lub_string_cat(&result,home_dir);
        }
        count++;
        p++;
    }
    if(count)
    {
        lub_string_catn(&result,segment,count);
    }
    return result;
}
/*-------------------------------------------------------- */
void 
clish_shell_load_files(clish_shell_t *this)
{
    const char *path = getenv("CLISH_PATH");
    char       *buffer;
    char       *dirname;
    
    if(NULL == path)
    {
        /* use the default path */
        path = default_path;
    }
    /* take a copy of the path */
    buffer = clish_shell_tilde_expand(path);

    /* now loop though each directory */
    for(dirname = strtok(buffer,";");
        dirname;
        dirname = strtok(NULL,";"))
    {
        DIR           *dir;
        struct dirent *entry;
        
        /* search this directory for any XML files */
        dir = opendir(dirname);
        if(NULL == dir)
        {
            /* maxk: Disabel annoying print outs **/
            /* tinyrl_printf(this->tinyrl,
                          "*** Failed to open '%s' directory\n",
                          dirname); */
            continue;
        }
        for(entry = readdir(dir);
            entry;
            entry = readdir(dir))
        {
            const char *extension = strrchr(entry->d_name,'.');
            /* check the filename */
            if(NULL != extension)
            {
                if(0 == strcmp(".xml",extension))
                {
                    char *filename = NULL;
                    
                    /* build the filename */
                    lub_string_cat(&filename,dirname);
                    lub_string_cat(&filename,"/");
                    lub_string_cat(&filename,entry->d_name);
                    
                    /* load this file */
                    (void)clish_shell_xml_read(this,filename);
                    
                    /* release the resource */
                    lub_string_free(filename);
                }
            }
        }
        /* all done for this directory */
        closedir(dir);
    }
    /* tidy up */
    lub_string_free(buffer);
#ifdef DEBUG
        clish_shell_dump(this);
#endif
}
/*-------------------------------------------------------- */
/*
 * This is invoked when the thread ends or is cancelled.
 */
static void
clish_shell_cleanup(context_t *context)
{
#ifdef __vxworks
    int last_state;
    /* we need to avoid recursion issues that exit in VxWorks */
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,&last_state);
#endif /* __vxworks */
    
    if(context->shell)
    {
        /*
         * Time to kill off the instance and terminate the thread 
         */
        clish_shell_delete(context->shell);
        context->shell = NULL;
    }
    if(context->pargv)
    {
        clish_pargv_delete(context->pargv);
        context->pargv = NULL;
    }
    if(context->prompt)
    {
        lub_string_free(context->prompt);
        context->prompt = NULL;
    }
    /*if it is socket telnet session*/
    if(NULL!=context->istream&&fileno(stdin)!=fileno(context->istream))
    {
        fclose(context->istream);
        fclose(context->ostream);
        pthread_mutex_lock(&g_clish_telnet_session_num_mutex);
        g_clish_telnet_session_num--;
        pthread_mutex_unlock(&g_clish_telnet_session_num_mutex);
    }

#ifdef __vxworks
    pthread_setcancelstate(last_state,&last_state);
#endif /* __vxworks */
}

static bool_t check_LoginAccount_Passwd(char * loginAccount,char * passwd)
{
    int i=0,j=0;
    MD5Context md5c;
    char passwd_md5[MD5_LEN];    
    for(i=0;i<LOGIN_GROUP_NUM;i++)
    {
        if(strcmp(loginAccount,cli_login_md5_groups[i].login_name)==0)
        {
            MD5Init( &md5c );
            MD5Update( &md5c, passwd, strlen(passwd));
            MD5Final( passwd_md5, &md5c );

            if(memcmp(passwd_md5,cli_login_md5_groups[i].login_passwd_md5,MD5_LEN)==0)
            {
                return BOOL_TRUE;
            }
        }
    }
    return BOOL_FALSE;
    
}

/*-------------------------------------------------------- */
/*
 * This provides the thread of execution for a shell instance
 */
static void *
clish_shell_thread(void *arg)
{
    context_t     *context = arg;
    bool_t         running = BOOL_TRUE;
    clish_shell_t *this;
    int            last_type;
    unsigned char * loginAccount = NULL;
    unsigned char * passwd = NULL;
    FILE          *fd = NULL;

    /* make sure we can only be cancelled at controlled points */
    pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED,&last_type);
    /* register a cancellation handler */
    pthread_cleanup_push((void(*)(void*))clish_shell_cleanup,context);
    
    /* create a shell object... */
    this = context->shell = clish_shell_new(context->hooks,
                                            context->cookie,
                                            context->istream);
    assert(this);

    /*
     * Check the shell isn't closing down
     */
    if(this && (SHELL_STATE_CLOSING != this->state))
    {
        /*
         * load the XML files found in the current CLISH path 
         */
        clish_shell_load_files(this);

        /* start off with the default inputs stream */
        fd = fdopen(fileno(context->istream),"r");
        if(fd)
            (void)clish_shell_push_file(this,
                                        fd,
                                        stdout,
                                        BOOL_TRUE);
        
        pthread_setspecific(g_clish_ostream_key,(void *)(stdout));
        /*leave some time to let finish printing*/
        /*sleep(2);*/

		#if 0
        /*now it is for login*/
        while(1)
        {
            tinyrl_printf(this->tinyrl,"login:");
            loginAccount= tinyrl_readline(this->tinyrl,"",(void *)context);
            if(NULL==loginAccount)
            {
                pthread_exit((void*)BOOL_TRUE);
            }
            if(!strcmp(loginAccount, ""))
            {
                continue;
            }
            tinyrl_printf(this->tinyrl,"passwd:");
            tinyrl_disable_echo(this->tinyrl,'*');
            passwd = tinyrl_readline(this->tinyrl,this->tinyrl->prompt,(void *)context);
            tinyrl_enable_echo(this->tinyrl);
            
            if(NULL==passwd)
            {
                free(loginAccount);
                pthread_exit((void*)BOOL_TRUE);
            }
            
            if(loginAccount!=NULL&&passwd!=NULL)
            {
                if(BOOL_TRUE==check_LoginAccount_Passwd(loginAccount,passwd))
                {
                    /*tinyrl_printf(this->tinyrl,"login successful\r\n");*/
                    free(loginAccount);
                    loginAccount = NULL;
                    free(passwd);
                    loginAccount = NULL;
                    break;
                }
            }
            
            if(loginAccount!=NULL)
            {
                free(loginAccount);
                loginAccount = NULL;
            }
            
            if(passwd!=NULL)
            {
                free(passwd);
                loginAccount = NULL;
            }
            
            tinyrl_printf(this->tinyrl,"Access denied\r\n");
        }
		#endif

        /*the following code are moved from clish_shell_tinyrl_new() because when login we should let cr nl key to work in normal way*/
        if(NULL!=this->tinyrl)
            clish_shell_tinyrl_init(this->tinyrl);
        /* This is the starting point for a shell, if this fails then 
         * we cannot start the shell...
         */
        running = clish_shell_startup(this);
        pthread_testcancel();
            
        /* Loop reading and executing lines until the user quits. */
        while(running)
        {
            const clish_command_t *cmd;
            const clish_view_t    *view;

            if((SHELL_STATE_SCRIPT_ERROR == this->state) && 
               (BOOL_TRUE == tinyrl__get_isatty(this->tinyrl)))
            {
                /* interactive session doesn't automatically exit on error */
                this->state = SHELL_STATE_READY;
            }
            /* only bother to read the next line if there hasn't been a script error */
            if(this->state != SHELL_STATE_SCRIPT_ERROR)
            {
                /* obtain the prompt */
                view = clish_shell__get_view(this);
                assert(view);
                
                context->prompt = clish_view__get_prompt(view,clish_shell__get_viewid(this));
                assert(context->prompt);

                /* get input from the user */
                running = clish_shell_readline(this,context->prompt,&cmd,&context->pargv);
                lub_string_free(context->prompt);

                context->prompt = NULL;
                
                if(running && cmd && context->pargv)
                {
                    /* execute the provided command */
                    if(BOOL_FALSE == clish_shell_execute(this,cmd,&context->pargv))
                    {
                        /* there was an error */
                        tinyrl_ding(this->tinyrl);

                        /* 
                         * what we do now depends on whether we are set up to
                         * stop on error on not.
                         */
                        if((BOOL_TRUE == this->current_file->stop_on_error) &&
                        (BOOL_FALSE == tinyrl__get_isatty(this->tinyrl)))
                        {
                            this->state = SHELL_STATE_SCRIPT_ERROR;
                        }
                    }
                }
            }
            if((BOOL_FALSE == running) || 
               (this->state == SHELL_STATE_SCRIPT_ERROR))
            {
                /* we've reached the end of a file (or a script error has occured)
                 * unwind the file stack to see whether 
                 * we need to exit
                 */
                running = clish_shell_pop_file(this);
            }
            /* test for cancellation */
            pthread_testcancel();
        }
    }
    /* be a good pthread citizen */
    pthread_cleanup_pop(1);

    return (void*)BOOL_TRUE;
}
/*-------------------------------------------------------- */
static context_t *
_clish_shell_spawn(const pthread_attr_t      *attr,
                   const clish_shell_hooks_t *hooks,
                   void                      *cookie,
                   FILE                      *istream)
{
    int rtn;
    context_t *context = malloc(sizeof(context_t));
    assert(context);
    
    if(context)
    {
        context->hooks   = hooks;
        context->cookie  = cookie;
        context->istream = istream;
        context->shell   = NULL;
        context->prompt  = NULL;
        context->pargv   = NULL;
        
        /* and set it free */
        rtn = pthread_create(&context->pthread,
                             attr,
                             clish_shell_thread,
                             context);
        if(0 != rtn)
        {
            free(context);
            context = NULL;
        }
    }
    return context;
}
/*-------------------------------------------------------- */
static int
_clish_shell_spawn_and_wait(const clish_shell_hooks_t *hooks,
                            void                      *cookie,
                            FILE                      *file)
{
    void      *result = NULL;
    context_t *context = _clish_shell_spawn(NULL,hooks,cookie,file);
    
    if(context)
    {
        /* join the shell's thread and wait for it to exit */
        (void)pthread_join(context->pthread,&result);
        return (int)BOOL_TRUE;
    }
    return (int)BOOL_FALSE;
}
/*-------------------------------------------------------- */
int 
clish_shell_spawn_and_wait(const clish_shell_hooks_t *hooks,
                           void                      *cookie)
{
    return _clish_shell_spawn_and_wait(hooks,cookie,stdin);
}
/*-------------------------------------------------------- */
bool_t
clish_shell_spawn(pthread_t                 *pthread,
                  const pthread_attr_t      *attr,
                  const clish_shell_hooks_t *hooks,
                  void                      *cookie)
{
    context_t *context;
    bool_t     result = BOOL_FALSE;
    
    /* spawn the thread... */
    context = _clish_shell_spawn(attr,hooks,cookie,stdin);
   
    if(NULL != context)
    {
        result   = BOOL_TRUE;
        if(NULL != pthread)
        {
            *pthread = context->pthread;
        }
    }
    return result;
}
/*-------------------------------------------------------- */
bool_t
clish_shell_spawn_from_file(const clish_shell_hooks_t *hooks,
                            void                      *cookie,
                            const char                *filename)
{
    bool_t result = BOOL_FALSE;
    if(NULL != filename)
    {
        FILE *file = fopen(filename,"r");
        if (NULL != file)
        {
            /* spawn the thread and wait for it to exit */
            result = _clish_shell_spawn_and_wait(hooks,cookie,file) ? BOOL_TRUE : BOOL_FALSE;
    
            fclose(file);
        }
    }
    return result;
}

/*-------------------------------------------------------- */
/*
 * This provides the thread of execution for a shell instance from socket
 */
static void *
clish_shell_thread_from_socket(void *arg)
{
    context_t     *context = arg;
    bool_t         running = BOOL_TRUE;
    clish_shell_t *this;
    int            last_type;
    unsigned char * loginAccount = NULL;
    unsigned char * passwd = NULL;
    int stdout_restore_fd = -1;
    int stdout_redirect_fd = -1;
    bool_t res_dup = BOOL_TRUE;
    char name[16];

	sprintf(name, "cli_%d", g_clish_telnet_session_num);
	//if (-1 == mipc_init(name, 0, 0))  
	//{ 	  
	//	return; 
	//}   

    /*do not join telnet*/
    pthread_detach(pthread_self());    
    /* make sure we can only be cancelled at controlled points */
    pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED,&last_type);
    /* register a cancellation handler */
    pthread_cleanup_push((void(*)(void*))clish_shell_cleanup,context);
    
    /* create a shell object... */
    this = context->shell = clish_shell_new(context->hooks,
                                            context->cookie,
                                            context->istream);
    assert(this);
    
    /*
     * Check the shell isn't closing down
     */
    if(this && (SHELL_STATE_CLOSING != this->state))
    {
        /*
         * load the XML files found in the current CLISH path 
         */
        clish_shell_load_files(this);
        /* start off with the default inputs stream */
 
        (void)clish_shell_push_file(this,
                                    context->istream,
                                    context->ostream,
                                    BOOL_TRUE);
        pthread_setspecific(g_clish_ostream_key,(void *)(context->ostream));
        /*leave some time to let finish printing*/
        /*telnet no need*/
        /*sleep(2);*/

		#if 0
        /*now it is for login*/
        while(1)
        {
            tinyrl_printf(this->tinyrl,"login:");
            loginAccount= tinyrl_readline(this->tinyrl,"",(void *)context);
            if(NULL==loginAccount)
            {
                pthread_exit((void*)BOOL_TRUE);
            }            
            tinyrl_printf(this->tinyrl,"passwd:");
            tinyrl_disable_echo(this->tinyrl,'*');
            passwd = tinyrl_readline(this->tinyrl,this->tinyrl->prompt,(void *)context);
            tinyrl_enable_echo(this->tinyrl);
            if(NULL==passwd)
            {
                free(loginAccount);
                pthread_exit((void*)BOOL_TRUE);
            }

            
            
            if(loginAccount!=NULL&&passwd!=NULL)
            {
                if(BOOL_TRUE==check_LoginAccount_Passwd(loginAccount,passwd))
                {
                    /*tinyrl_printf(this->tinyrl,"login successful\r\n");*/
                    free(loginAccount);
                    loginAccount = NULL;
                    free(passwd);
                    loginAccount = NULL;
                    break;
                }
            }
            
            if(loginAccount!=NULL)
            {
                free(loginAccount);
                loginAccount = NULL;
            }
            
            if(passwd!=NULL)
            {
                free(passwd);
                loginAccount = NULL;
            }
            
            tinyrl_printf(this->tinyrl,"Access denied\r\n");
        }
		#endif
		
        /*the following code are moved from clish_shell_tinyrl_new() because when login we should let cr nl key to work in normal way*/
        if(NULL!=this->tinyrl)
            clish_shell_tinyrl_init(this->tinyrl);
        
        /* This is the starting point for a shell, if this fails then 
         * we cannot start the shell...
         */
        running = clish_shell_startup(this);
       
        pthread_testcancel();
            
        /* Loop reading and executing lines until the user quits. */
        while(running)
        {
            const clish_command_t *cmd;
            const clish_view_t    *view;

            if((SHELL_STATE_SCRIPT_ERROR == this->state) && 
               (BOOL_TRUE == tinyrl__get_isatty(this->tinyrl)))
            {
                /* interactive session doesn't automatically exit on error */
                this->state = SHELL_STATE_READY;
            }
            /* only bother to read the next line if there hasn't been a script error */
            if(this->state != SHELL_STATE_SCRIPT_ERROR)
            {
                /* obtain the prompt */	
                view = clish_shell__get_view(this);
                assert(view);
                
                context->prompt = clish_view__get_prompt(view,clish_shell__get_viewid(this));
                assert(context->prompt);

                /* get input from the user */
                running = clish_shell_readline(this,context->prompt,&cmd,&context->pargv);
                lub_string_free(context->prompt);

                context->prompt = NULL;
                
                if(running && cmd && context->pargv)
                {
                    res_dup = BOOL_TRUE;
                    
                    stdout_restore_fd = dup(1);
                    if(stdout_restore_fd<0)
                    {
                        printf(this->tinyrl,"dup stdout_restore_fd  failed!\r\n");
                        this->state = SHELL_STATE_SCRIPT_ERROR;
                        res_dup = BOOL_FALSE;
                    }

                    if(res_dup)
                    {
                        stdout_redirect_fd = dup2(fileno(context->ostream),1);
                        if(stdout_redirect_fd<0)
                        {
                            close(stdout_restore_fd);
                            stdout_restore_fd = -1;
                            tinyrl_printf(this->tinyrl,"dup2 stdout_redirect_fd  failed!\r\n");
                            this->state = SHELL_STATE_SCRIPT_ERROR;
                            res_dup = BOOL_FALSE;
                        }
                    }
                    
                    /* execute the provided command */
                    if(res_dup && (BOOL_FALSE == clish_shell_execute(this,cmd,&context->pargv)))
                    {
                        /* there was an error */
                        tinyrl_ding(this->tinyrl);

                        /* 
                         * what we do now depends on whether we are set up to
                         * stop on error on not.
                         */
                        if((BOOL_TRUE == this->current_file->stop_on_error) &&
                        (BOOL_FALSE == tinyrl__get_isatty(this->tinyrl)))
                        {
                            this->state = SHELL_STATE_SCRIPT_ERROR;
                        }
                    }

                    if(res_dup && (dup2(stdout_restore_fd,stdout_redirect_fd)<0))
                    {
                        tinyrl_printf(this->tinyrl,"dup2 stdout_restore_fd stdout_redirect_fd  failed!\r\n");
                        this->state = SHELL_STATE_SCRIPT_ERROR;
                    }
                    if(res_dup)
                    {
                        close(stdout_restore_fd);
                    }
                    
                    stdout_restore_fd = -1;
                    stdout_redirect_fd = -1;
                    
                }
            }
            if((BOOL_FALSE == running) || 
               (this->state == SHELL_STATE_SCRIPT_ERROR))
            {
                /* we've reached the end of a file (or a script error has occured)
                 * unwind the file stack to see whether 
                 * we need to exit
                 */
                running = clish_shell_pop_file(this);
            }
            /* test for cancellation */
            pthread_testcancel();
        }
    }
    /* be a good pthread citizen */
    pthread_cleanup_pop(1);

    return (void*)BOOL_TRUE;
}

/*-------------------------------------------------------- */  
static context_t *
_clish_shell_spawn_from_socket(const pthread_attr_t      *attr,
                   const clish_shell_hooks_t *hooks,
                   void                      *cookie,
                   FILE                      *istream,
                   FILE                      *ostream)
{
    int rtn;
    context_t *context = malloc(sizeof(context_t));
    assert(context);
    if(context)
    {
        context->hooks   = hooks;
        context->cookie  = cookie;
        context->istream = istream;
        context->ostream = ostream;
        context->shell   = NULL;
        context->prompt  = NULL;
        context->pargv   = NULL;
        
        /* and set it free */
        rtn = pthread_create(&context->pthread,
                             attr,
                             clish_shell_thread_from_socket,
                             context);
        if(0 != rtn)
        {
            free(context);
            context = NULL;
        }
    }
    return context;
}

/*-------------------------------------------------------- */
static bool_t
clish_shell_spawn_from_socket(const clish_shell_hooks_t *hooks,
                            void                        *cookie,
                            FILE                        *istream,
                            FILE                        *ostream)
{
    bool_t result = BOOL_FALSE;

    context_t *context = _clish_shell_spawn_from_socket(NULL,hooks,cookie,istream,ostream);

    if(NULL!=context)
        result = BOOL_TRUE;
    return result;
}

/*-------------------------------------------------------- */
/*
 * This is invoked when the tel_net thread ends or is cancelled.
 */
static void
clish_telnet_server_cleanup(context_t *context)
{
#ifdef __vxworks
    int last_state;
    /* we need to avoid recursion issues that exit in VxWorks */
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE,&last_state);
#endif /* __vxworks */

    if(context->istream)
        close((int)(context->istream));
    free(context);

#ifdef __vxworks
    pthread_setcancelstate(last_state,&last_state);
#endif /* __vxworks */
}
/*-------------------------------------------------------- */
/**
 * added by ken, 
 * the thread function of telnet server
 */

static void * _clish_telnet_server(void *arg)
{
    int listen_fd=0,com_fd=0,dup_fd=0;
    struct sockaddr_in client;
    int size= sizeof(struct sockaddr);
    int            last_type;
    context_t     *context = arg;
    FILE *istream=NULL; /*istream used for tel_net socket to file*/
    FILE *ostream=NULL; /*ostream used for tel_net socket to file*/
    unsigned char send_msg[MAX_TELNET_SESSION_OPTION_SEND_BYTE];
    unsigned char rec_msg[MAX_TELNET_SESSION_OPTION_RCV_BYTE];
    int rec_msg_len = 0;    
    int i=0;
    FIND_ECHO result_find_echo = NOT_FIND;
    FIND_ECHO result_find_suppress_go_ahead = NOT_FIND;

    
    pthread_detach(pthread_self());
    /* make sure we can only be cancelled at controlled points */
    pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED,&last_type);
    /* register a cancellation handler */
    pthread_cleanup_push((void(*)(void*))clish_telnet_server_cleanup,(void *)listen_fd);

    
    pthread_key_create(&g_clish_ostream_key,NULL); 

    memset(send_msg,0,sizeof(send_msg));
    memset(rec_msg,0,sizeof(rec_msg));
	send_msg[0] = IAC;
	send_msg[1] = WILL;
	send_msg[2] = TELNET_ECHO;
    send_msg[3] = IAC;
	send_msg[4] = WILL;
	send_msg[5] = TELNET_SUPPRESS_GO_AHEAD;
                
    if(0!=connect_init(DEFAULT_TELNET_PORT,&listen_fd))
    {
        printf("connect_init failed for _clish_telnet_server\n");
        return (void *)BOOL_FALSE;
    }

    /*here just record listen_fd for cleanup, not the real istream*/
    context->istream = (FILE *)listen_fd;
    pthread_testcancel(); 
    while(1)
    {
        /*set up the connection*/
        size= sizeof(client);
        memset(&client,0,sizeof(struct sockaddr_in));
        com_fd = accept(listen_fd,(struct sockaddr *)&client,&size); 
        if(com_fd < 0)
        {
           continue; 
        }
        
        if(g_clish_telnet_session_num<MAX_TELNET_SESSION_NUM)
        {            

            /*closing work should be finished by every telnet session,here can not close com_fd*/
            ostream = fdopen(com_fd,"w"); 
            if (NULL == ostream) 
            {
                connect_sendMessage(com_fd,"Failed to open socket to file istream",strlen("Failed to open socket to file istream"));
                close(com_fd);
                com_fd = 0;
                continue;
            }
            
            dup_fd = dup(com_fd);
            if(dup_fd < 0)
            {
                if(ostream) 
                {
                    fclose(ostream);
                    ostream = NULL; 
                }            
                close(com_fd);
                com_fd = 0;
                continue;
            }
            istream = fdopen(dup_fd,"r"); 
            if (NULL== istream) 
            {
                connect_sendMessage(com_fd,"Failed to open dup socket to file ostream",strlen("Failed to open dup socket to file ostream"));
                /* fclose(istream); */
                istream = NULL;
                com_fd = 0;
                continue;
            }
            result_find_echo = NOT_FIND;
            result_find_suppress_go_ahead = NOT_FIND;            
            memset(rec_msg,0,sizeof(rec_msg));

            connect_sendMessage(com_fd,send_msg,MAX_TELNET_SESSION_OPTION_SEND_BYTE);
 
            do{
                rec_msg_len = connect_receiveMessage(com_fd,rec_msg,MAX_TELNET_SESSION_OPTION_RCV_BYTE);            
                if(0==rec_msg_len)
                {   
                    if(ostream) 
                    {
                        fclose(ostream);
                        ostream = NULL; 
                    }
                    com_fd = 0;
                    if(istream) 
                    {                    
                        fclose(istream);
                        istream = NULL;
                    }
                    dup_fd = 0;
                    continue;
                }
                find_echo_and_suppress_go_ahead_opt(rec_msg, rec_msg_len,&result_find_echo,&result_find_suppress_go_ahead);
            }while(NOT_FIND==result_find_echo || NOT_FIND==result_find_suppress_go_ahead);

            if(FIND_DO!=result_find_echo || FIND_DO!=result_find_suppress_go_ahead)
            {
                connect_sendMessage(com_fd,"you must support echo and suppress go ahead!",strlen("you must support echo and suppress go ahead!"));
                if(ostream) 
                {
                    fclose(ostream);
                    ostream = NULL; 
                }
                com_fd = 0;                
                if(istream) 
                {                    
                    fclose(istream);
                    istream = NULL;
                }
                dup_fd = 0;
                continue;
            }

            /******************************/
            if(BOOL_TRUE==clish_shell_spawn_from_socket(context->hooks,context->cookie,istream,ostream))
            {
                pthread_mutex_lock(&g_clish_telnet_session_num_mutex);
                g_clish_telnet_session_num++;
                pthread_mutex_unlock(&g_clish_telnet_session_num_mutex);
            }
        }                
        else
        {
            connect_sendMessage(com_fd,"too many telnet sessions!",strlen("too many telnet sessions!"));
            close(com_fd);
            com_fd = 0;
        }
        
        pthread_testcancel();
    }

    close(listen_fd);
    listen_fd = 0;
    pthread_key_delete(g_clish_ostream_key);  
    pthread_cleanup_pop(1);
    return (void *)BOOL_TRUE;
}
/*-------------------------------------------------------- */
/**
 * added by ken, 
 * create the thread for telnet server
 */

bool_t clish_telnet_server_spawn(const clish_shell_hooks_t *hooks,
                           void                      *cookie)
{
    int rtn;
    pthread_t pthread;

    context_t *context = malloc(sizeof(context_t));
    if(context)
    {
        context->hooks   = hooks;
        context->cookie  = cookie;
        rtn=pthread_create(&pthread, NULL, _clish_telnet_server,context);
        if(rtn!=0)
        {
            free(context);
            context = NULL;
        }
    }
    return BOOL_TRUE;
}

#if 1
/***************************************************
MD5
****************************************************/


#ifdef sgi
#define HIGHFIRST
#endif

#ifdef sun
#define HIGHFIRST
#endif

#ifndef HIGHFIRST
#define byteReverse(buf, len)    /* Nothing */
#else
/*-------------------------------------------------------- */
/*
* Note: this code is harmless on little-endian machines.
*/
void byteReverse(buf, longs)
    unsigned char *buf; unsigned longs;
{
    uint32 t;
    do {
    t = (uint32) ((unsigned) buf[3] << 8 | buf[2]) << 16 |
        ((unsigned) buf[1] << 8 | buf[0]);
    *(uint32 *) buf = t;
    buf += 4;
    } while (--longs);
}
#endif
/*-------------------------------------------------------- */
/*
* Start MD5 accumulation. Set bit count to 0 and buffer to mysterious
* initialization constants.
*/
void MD5Init(MD5Context *ctx)
{
    ctx->buf[0] = 0x67452301;
    ctx->buf[1] = 0xefcdab89;
    ctx->buf[2] = 0x98badcfe;
    ctx->buf[3] = 0x10325476;

    ctx->bits[0] = 0;
    ctx->bits[1] = 0;
}
/*-------------------------------------------------------- */
/*
* Update context to reflect the concatenation of another buffer full
* of bytes.
*/
void MD5Update(MD5Context *ctx, unsigned char *buf, unsigned len)
    
{
    uint32 t;

    /* Update bitcount */

    t = ctx->bits[0];
    if ((ctx->bits[0] = t + ((uint32) len << 3)) < t)
    ctx->bits[1]++;     /* Carry from low to high */
    ctx->bits[1] += len >> 29;

    t = (t >> 3) & 0x3f;    /* Bytes already in shsInfo->data */

    /* Handle any leading odd-sized chunks */

    if (t) {
    unsigned char *p = (unsigned char *) ctx->in + t;

    t = 64 - t;
    if (len < t) {
        memcpy(p, buf, len);
        return;
    }
    memcpy(p, buf, t);
    byteReverse(ctx->in, 16);
    MD5Transform(ctx->buf, (uint32 *) ctx->in);
    buf += t;
    len -= t;
    }
    /* Process data in 64-byte chunks */

    while (len >= 64) {
    memcpy(ctx->in, buf, 64);
    byteReverse(ctx->in, 16);
    MD5Transform(ctx->buf, (uint32 *) ctx->in);
    buf += 64;
    len -= 64;
    }

    /* Handle any remaining bytes of data. */

    memcpy(ctx->in, buf, len);
}
/*-------------------------------------------------------- */
/*
* Final wrapup - pad to 64-byte boundary with the bit pattern 
* 1 0* (64-bit count of bits processed, MSB-first)
*/
void MD5Final(unsigned char digest[16], MD5Context *ctx)
{
    unsigned count;
    unsigned char *p;

    /* Compute number of bytes mod 64 */
    count = (ctx->bits[0] >> 3) & 0x3F;

    /* Set the first char of padding to 0x80. This is safe since there is
       always at least one byte free */
    p = ctx->in + count;
    *p++ = 0x80;

    /* Bytes of padding needed to make 64 bytes */
    count = 64 - 1 - count;

    /* Pad out to 56 mod 64 */
    if (count < 8) {
    /* Two lots of padding: Pad the first block to 64 bytes */
    memset(p, 0, count);
    byteReverse(ctx->in, 16);
    MD5Transform(ctx->buf, (uint32 *) ctx->in);

    /* Now fill the next block with 56 bytes */
    memset(ctx->in, 0, 56);
    } else {
    /* Pad block to 56 bytes */
    memset(p, 0, count - 8);
    }
    byteReverse(ctx->in, 14);

    /* Append length in bits and transform */
    ((uint32 *) ctx->in)[14] = ctx->bits[0];
    ((uint32 *) ctx->in)[15] = ctx->bits[1];

    MD5Transform(ctx->buf, (uint32 *) ctx->in);
    byteReverse((unsigned char *) ctx->buf, 4);
    memcpy(digest, ctx->buf, 16);
    memset(ctx, 0, sizeof(ctx));        /* In case it's sensitive */
}
/*-------------------------------------------------------- */

/* The four core functions - F1 is optimized somewhat */

/* #define F1(x, y, z) (x & y | ~x & z) */
#define F1(x, y, z) (z ^ (x & (y ^ z)))
#define F2(x, y, z) F1(z, x, y)
#define F3(x, y, z) (x ^ y ^ z)
#define F4(x, y, z) (y ^ (x | ~z))

/* This is the central step in the MD5 algorithm. */
#define MD5STEP(f, w, x, y, z, data, s) \
    ( w += f(x, y, z) + data, w = w<<s | w>>(32-s), w += x )
/*-------------------------------------------------------- */
/*
* The core of the MD5 algorithm, this alters an existing MD5 hash to
* reflect the addition of 16 longwords of new data. MD5Update blocks
* the data and converts bytes into longwords for this routine.
*/
void MD5Transform(uint32 buf[4], uint32 in[16])
{
    register uint32 a, b, c, d;

    a = buf[0];
    b = buf[1];
    c = buf[2];
    d = buf[3];

    MD5STEP(F1, a, b, c, d, in[0] + 0xd76aa478, 7);
    MD5STEP(F1, d, a, b, c, in[1] + 0xe8c7b756, 12);
    MD5STEP(F1, c, d, a, b, in[2] + 0x242070db, 17);
    MD5STEP(F1, b, c, d, a, in[3] + 0xc1bdceee, 22);
    MD5STEP(F1, a, b, c, d, in[4] + 0xf57c0faf, 7);
    MD5STEP(F1, d, a, b, c, in[5] + 0x4787c62a, 12);
    MD5STEP(F1, c, d, a, b, in[6] + 0xa8304613, 17);
    MD5STEP(F1, b, c, d, a, in[7] + 0xfd469501, 22);
    MD5STEP(F1, a, b, c, d, in[8] + 0x698098d8, 7);
    MD5STEP(F1, d, a, b, c, in[9] + 0x8b44f7af, 12);
    MD5STEP(F1, c, d, a, b, in[10] + 0xffff5bb1, 17);
    MD5STEP(F1, b, c, d, a, in[11] + 0x895cd7be, 22);
    MD5STEP(F1, a, b, c, d, in[12] + 0x6b901122, 7);
    MD5STEP(F1, d, a, b, c, in[13] + 0xfd987193, 12);
    MD5STEP(F1, c, d, a, b, in[14] + 0xa679438e, 17);
    MD5STEP(F1, b, c, d, a, in[15] + 0x49b40821, 22);

    MD5STEP(F2, a, b, c, d, in[1] + 0xf61e2562, 5);
    MD5STEP(F2, d, a, b, c, in[6] + 0xc040b340, 9);
    MD5STEP(F2, c, d, a, b, in[11] + 0x265e5a51, 14);
    MD5STEP(F2, b, c, d, a, in[0] + 0xe9b6c7aa, 20);
    MD5STEP(F2, a, b, c, d, in[5] + 0xd62f105d, 5);
    MD5STEP(F2, d, a, b, c, in[10] + 0x02441453, 9);
    MD5STEP(F2, c, d, a, b, in[15] + 0xd8a1e681, 14);
    MD5STEP(F2, b, c, d, a, in[4] + 0xe7d3fbc8, 20);
    MD5STEP(F2, a, b, c, d, in[9] + 0x21e1cde6, 5);
    MD5STEP(F2, d, a, b, c, in[14] + 0xc33707d6, 9);
    MD5STEP(F2, c, d, a, b, in[3] + 0xf4d50d87, 14);
    MD5STEP(F2, b, c, d, a, in[8] + 0x455a14ed, 20);
    MD5STEP(F2, a, b, c, d, in[13] + 0xa9e3e905, 5);
    MD5STEP(F2, d, a, b, c, in[2] + 0xfcefa3f8, 9);
    MD5STEP(F2, c, d, a, b, in[7] + 0x676f02d9, 14);
    MD5STEP(F2, b, c, d, a, in[12] + 0x8d2a4c8a, 20);

    MD5STEP(F3, a, b, c, d, in[5] + 0xfffa3942, 4);
    MD5STEP(F3, d, a, b, c, in[8] + 0x8771f681, 11);
    MD5STEP(F3, c, d, a, b, in[11] + 0x6d9d6122, 16);
    MD5STEP(F3, b, c, d, a, in[14] + 0xfde5380c, 23);
    MD5STEP(F3, a, b, c, d, in[1] + 0xa4beea44, 4);
    MD5STEP(F3, d, a, b, c, in[4] + 0x4bdecfa9, 11);
    MD5STEP(F3, c, d, a, b, in[7] + 0xf6bb4b60, 16);
    MD5STEP(F3, b, c, d, a, in[10] + 0xbebfbc70, 23);
    MD5STEP(F3, a, b, c, d, in[13] + 0x289b7ec6, 4);
    MD5STEP(F3, d, a, b, c, in[0] + 0xeaa127fa, 11);
    MD5STEP(F3, c, d, a, b, in[3] + 0xd4ef3085, 16);
    MD5STEP(F3, b, c, d, a, in[6] + 0x04881d05, 23);
    MD5STEP(F3, a, b, c, d, in[9] + 0xd9d4d039, 4);
    MD5STEP(F3, d, a, b, c, in[12] + 0xe6db99e5, 11);
    MD5STEP(F3, c, d, a, b, in[15] + 0x1fa27cf8, 16);
    MD5STEP(F3, b, c, d, a, in[2] + 0xc4ac5665, 23);

    MD5STEP(F4, a, b, c, d, in[0] + 0xf4292244, 6);
    MD5STEP(F4, d, a, b, c, in[7] + 0x432aff97, 10);
    MD5STEP(F4, c, d, a, b, in[14] + 0xab9423a7, 15);
    MD5STEP(F4, b, c, d, a, in[5] + 0xfc93a039, 21);
    MD5STEP(F4, a, b, c, d, in[12] + 0x655b59c3, 6);
    MD5STEP(F4, d, a, b, c, in[3] + 0x8f0ccc92, 10);
    MD5STEP(F4, c, d, a, b, in[10] + 0xffeff47d, 15);
    MD5STEP(F4, b, c, d, a, in[1] + 0x85845dd1, 21);
    MD5STEP(F4, a, b, c, d, in[8] + 0x6fa87e4f, 6);
    MD5STEP(F4, d, a, b, c, in[15] + 0xfe2ce6e0, 10);
    MD5STEP(F4, c, d, a, b, in[6] + 0xa3014314, 15);
    MD5STEP(F4, b, c, d, a, in[13] + 0x4e0811a1, 21);
    MD5STEP(F4, a, b, c, d, in[4] + 0xf7537e82, 6);
    MD5STEP(F4, d, a, b, c, in[11] + 0xbd3af235, 10);
    MD5STEP(F4, c, d, a, b, in[2] + 0x2ad7d2bb, 15);
    MD5STEP(F4, b, c, d, a, in[9] + 0xeb86d391, 21);

    buf[0] += a;
    buf[1] += b;
    buf[2] += c;
    buf[3] += d;
}
/*-------------------------------------------------------- */
void login_md5_groups_init()
{
    int i=0;
    MD5Context md5c;

    for(i=0;i<LOGIN_GROUP_NUM;i++)
    {
        cli_login_md5_groups[i].login_name = lub_string_dup(cli_login_groups[i].login_name);
        MD5Init( &md5c );
        MD5Update( &md5c, cli_login_groups[i].login_passwd, strlen(cli_login_groups[i].login_passwd));
        MD5Final( cli_login_md5_groups[i].login_passwd_md5, &md5c );
    }
}
#endif
/*-------------------------------------------------------- */
/***************************************************
SOCKET init work
******************/  
static int connect_init(int port,int *listen_fd1){   
    int listen_fd,addrlen;   
    int ret;   
    int len;   
    struct sockaddr_in client_addr;   
    struct sockaddr_in server_addr;   
       
    pthread_t pthread_id;   
       
    listen_fd = socket(PF_INET, SOCK_STREAM, 0);   
    if(listen_fd == -1){   
        perror("listen_fd error");   
        return 1;   
    }

    
    /* set fast retransmit */
    addrlen = 1;
    setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &addrlen,
               sizeof(addrlen));

    memset(&server_addr,0,sizeof(server_addr));   
    server_addr.sin_family = AF_INET;   
    server_addr.sin_port = htons(port);   
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);   
       
    ret = bind(listen_fd, (struct sockaddr *)&server_addr, sizeof(server_addr));   
    if(ret == -1){   
        perror("connot bind server socket");   
        close(listen_fd);   
        return 1;   
    }   
      
    ret = listen(listen_fd, 1);       
    if(ret == -1){   
        perror("connot listen the client connect request");   
        close(listen_fd);   
        return 1;   
    }   
     *listen_fd1=listen_fd;
     return 0;
}   
/*-------------------------------------------------------- */  
static int connect_sendMessage(int com_fd,char *buf,int len){   
   if(len>0)
   {   
        send(com_fd,buf,len,0);   
        return 1;   
   }   
   else return 0;   
}   
/*-------------------------------------------------------- */
static int connect_receiveMessage(int com_fd,char *buf,int len){   
    int size;   
    size=recv(com_fd,buf,len,0);   
    if(size>0)
    {   
       buf[size]='\0';   
       return size;   
    }   
    else return 0;   
}

/*-------------------------------------------------------- */
/*find the option of echo in received messages*/
static FIND_ECHO find_echo_opt(unsigned char *rcv_msg, int len)
{
    int i = 0;
    unsigned char *p = rcv_msg;

    for(i=0;i<len-2;i++)
    {
        p=rcv_msg+i;
        if(IAC==*p && TELNET_ECHO==*(p+2))
        {
            if(WILL==*(p+1))
            {
                return FIND_WILL;
            }            
            if(WONT==*(p+1))
            {
                return FIND_WONT;
            }
            if(DO==*(p+1))
            {
                return FIND_DO;
            }
            if(DONT==*(p+1))
            {
                return FIND_DONT;
            }
    
        }
    }
    return NOT_FIND;
}

/*find the options of echo and go ahead in received messages*/
static void find_echo_and_suppress_go_ahead_opt(unsigned char *rcv_msg, int len, FIND_ECHO * echo_opt, FIND_ECHO * suppress_go_ahead_opt)
{
    int i = 0;
    unsigned char *p = rcv_msg;

    for(i=0;i<len-2;i++)
    {
        p=rcv_msg+i;
        
        if(IAC==*p && TELNET_ECHO==*(p+2) && NULL!=echo_opt)
        {
            if(WILL==*(p+1))
            {
                *echo_opt = FIND_WILL;
            }            
            if(WONT==*(p+1))
            {
                *echo_opt = FIND_WONT;
            }
            if(DO==*(p+1))
            {
                *echo_opt = FIND_DO;
            }
            if(DONT==*(p+1))
            {
                *echo_opt = FIND_DONT;
            }    
        }

        if(IAC==*p && TELNET_SUPPRESS_GO_AHEAD==*(p+2) && NULL!=suppress_go_ahead_opt)
        {
            if(WILL==*(p+1))
            {
                *suppress_go_ahead_opt = FIND_WILL;
            }            
            if(WONT==*(p+1))
            {
                *suppress_go_ahead_opt = FIND_WONT;
            }
            if(DO==*(p+1))
            {
                *suppress_go_ahead_opt = FIND_DO;
            }
            if(DONT==*(p+1))
            {
                *suppress_go_ahead_opt = FIND_DONT;
            }
    
        }
    }
}

/*-------------------------------------------------------- */
