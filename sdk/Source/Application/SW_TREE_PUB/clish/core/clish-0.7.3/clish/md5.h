#ifndef _clish_md5_h
#define _clish_md5m_h

_BEGIN_C_DECL

#ifdef __alpha
         typedef unsigned int uint32;
#else
         typedef unsigned long uint32;
#endif

struct _MD5Context
{
 uint32 buf[4];
 uint32 bits[2];
 unsigned char in[64];
};    

typedef struct _MD5Context MD5Context;
/*
* This is needed to make RSAREF happy on some MS-DOS compilers.
*/
/*typedef MD5Context MD5_CTX;  */


extern void MD5Init(MD5Context *ctx);
extern void MD5Update(MD5Context *ctx, unsigned char *buf, unsigned len);
extern void MD5Final(unsigned char digest[16], MD5Context *ctx);
extern void MD5Transform(uint32 buf[4], uint32 in[16]);

_END_C_DECL
    
#endif

