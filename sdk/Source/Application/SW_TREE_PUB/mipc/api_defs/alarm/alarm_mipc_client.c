/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "ponOnuMngIf.h"

#include "apm_alarm_api.h"

#include "alarm_mipc_defs.h"


bool apm_alarm_create_entity(uint32_t type, uint32_t param1, uint32_t param2, uint8_t alarm_class, uint8_t admin, uint32_t threshold, uint32_t clear_threshold)
{
	apm_alarm_create_entity_IN_T input;
	apm_alarm_create_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;
	input.param2 = param2;
	input.alarm_class = alarm_class;
	input.admin = admin;
	input.threshold = threshold;
	input.clear_threshold = clear_threshold;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("alarm", 0, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_alarm_delete_entity(uint32_t type, uint32_t param1)
{
	apm_alarm_delete_entity_IN_T input;
	apm_alarm_delete_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("alarm", 1, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_alarm_set_admin(uint32_t type, uint32_t param1, uint8_t admin)
{
	apm_alarm_set_admin_IN_T input;
	apm_alarm_set_admin_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;
	input.admin = admin;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("alarm", 2, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_alarm_set_threshold(uint32_t type, uint32_t param1, uint32_t threshold, uint32_t clear_threshold)
{
	apm_alarm_set_threshold_IN_T input;
	apm_alarm_set_threshold_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;
	input.threshold = threshold;
	input.clear_threshold = clear_threshold;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("alarm", 3, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_alarm_db_obj_reset(void)
{
	apm_alarm_db_obj_reset_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("alarm", 4, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_show_alarm_statistics(char* name)
{
	Apm_cli_show_alarm_statistics_IN_T input;
	Apm_cli_show_alarm_statistics_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("alarm", 5, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_show_alarm_entity(char* name, uint32_t type, uint32_t param1)
{
	Apm_cli_show_alarm_entity_IN_T input;
	Apm_cli_show_alarm_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("alarm", 6, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_show_alarm_list(char* name)
{
	Apm_cli_show_alarm_list_IN_T input;
	Apm_cli_show_alarm_list_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("alarm", 7, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_create_alarm_entity(char* name, uint32_t type, uint32_t param1, uint32_t param2, uint8_t alarm_class, uint8_t admin, uint32_t threshold, uint32_t clear_threshold)
{
	Apm_cli_create_alarm_entity_IN_T input;
	Apm_cli_create_alarm_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.param2 = param2;
	input.alarm_class = alarm_class;
	input.admin = admin;
	input.threshold = threshold;
	input.clear_threshold = clear_threshold;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("alarm", 8, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_delete_alarm_entity(char* name, uint32_t type, uint32_t param1)
{
	Apm_cli_delete_alarm_entity_IN_T input;
	Apm_cli_delete_alarm_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("alarm", 9, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_set_alarm_admin(char* name, uint32_t type, uint32_t param1, uint8_t admin)
{
	Apm_cli_set_alarm_admin_IN_T input;
	Apm_cli_set_alarm_admin_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.admin = admin;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("alarm", 10, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_set_alarm_theshold(char* name, uint32_t type, uint32_t param1, uint32_t threshold, uint32_t clear_threshold)
{
	Apm_cli_set_alarm_theshold_IN_T input;
	Apm_cli_set_alarm_theshold_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.threshold = threshold;
	input.clear_threshold = clear_threshold;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("alarm", 11, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_set_alarm_state_info(char* name, uint32_t type, uint32_t param1, uint8_t state, uint32_t info)
{
	Apm_cli_set_alarm_state_info_IN_T input;
	Apm_cli_set_alarm_state_info_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.state = state;
	input.info = info;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("alarm", 12, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


