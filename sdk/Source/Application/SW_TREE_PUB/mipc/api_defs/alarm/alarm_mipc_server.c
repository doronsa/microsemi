/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "ponOnuMngIf.h"

#include "apm_alarm_api.h"

#include "alarm_mipc_defs.h"


void alarm_mipc_server_call(char* source_module, int api_id, char* input)
{
	switch (api_id)
	{
		case 0:
		{
			apm_alarm_create_entity_IN_T* in = (apm_alarm_create_entity_IN_T*)input;
			apm_alarm_create_entity_OUT_T out;

			out.ret = apm_alarm_create_entity(in->type, in->param1, in->param2, in->alarm_class, in->admin, in->threshold, in->clear_threshold);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 0, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 1:
		{
			apm_alarm_delete_entity_IN_T* in = (apm_alarm_delete_entity_IN_T*)input;
			apm_alarm_delete_entity_OUT_T out;

			out.ret = apm_alarm_delete_entity(in->type, in->param1);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 1, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 2:
		{
			apm_alarm_set_admin_IN_T* in = (apm_alarm_set_admin_IN_T*)input;
			apm_alarm_set_admin_OUT_T out;

			out.ret = apm_alarm_set_admin(in->type, in->param1, in->admin);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 2, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 3:
		{
			apm_alarm_set_threshold_IN_T* in = (apm_alarm_set_threshold_IN_T*)input;
			apm_alarm_set_threshold_OUT_T out;

			out.ret = apm_alarm_set_threshold(in->type, in->param1, in->threshold, in->clear_threshold);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 3, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 4:
		{
			apm_alarm_db_obj_reset_OUT_T out;

			out.ret = apm_alarm_db_obj_reset();


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 4, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 5:
		{
			Apm_cli_show_alarm_statistics_IN_T* in = (Apm_cli_show_alarm_statistics_IN_T*)input;
			Apm_cli_show_alarm_statistics_OUT_T out;

			out.ret = Apm_cli_show_alarm_statistics((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 5, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 6:
		{
			Apm_cli_show_alarm_entity_IN_T* in = (Apm_cli_show_alarm_entity_IN_T*)input;
			Apm_cli_show_alarm_entity_OUT_T out;

			out.ret = Apm_cli_show_alarm_entity((void*)source_module, in->type, in->param1);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 6, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 7:
		{
			Apm_cli_show_alarm_list_IN_T* in = (Apm_cli_show_alarm_list_IN_T*)input;
			Apm_cli_show_alarm_list_OUT_T out;

			out.ret = Apm_cli_show_alarm_list((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 7, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 8:
		{
			Apm_cli_create_alarm_entity_IN_T* in = (Apm_cli_create_alarm_entity_IN_T*)input;
			Apm_cli_create_alarm_entity_OUT_T out;

			out.ret = Apm_cli_create_alarm_entity((void*)source_module, in->type, in->param1, in->param2, in->alarm_class, in->admin, in->threshold, in->clear_threshold);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 8, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 9:
		{
			Apm_cli_delete_alarm_entity_IN_T* in = (Apm_cli_delete_alarm_entity_IN_T*)input;
			Apm_cli_delete_alarm_entity_OUT_T out;

			out.ret = Apm_cli_delete_alarm_entity((void*)source_module, in->type, in->param1);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 9, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 10:
		{
			Apm_cli_set_alarm_admin_IN_T* in = (Apm_cli_set_alarm_admin_IN_T*)input;
			Apm_cli_set_alarm_admin_OUT_T out;

			out.ret = Apm_cli_set_alarm_admin((void*)source_module, in->type, in->param1, in->admin);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 10, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 11:
		{
			Apm_cli_set_alarm_theshold_IN_T* in = (Apm_cli_set_alarm_theshold_IN_T*)input;
			Apm_cli_set_alarm_theshold_OUT_T out;

			out.ret = Apm_cli_set_alarm_theshold((void*)source_module, in->type, in->param1, in->threshold, in->clear_threshold);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 11, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 12:
		{
			Apm_cli_set_alarm_state_info_IN_T* in = (Apm_cli_set_alarm_state_info_IN_T*)input;
			Apm_cli_set_alarm_state_info_OUT_T out;

			out.ret = Apm_cli_set_alarm_state_info((void*)source_module, in->type, in->param1, in->state, in->info);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 12, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		default:
			break;
	}
}


void alarm_mipc_server_handler(char* inMsg, unsigned int size)
{
	char source_module[MIPC_NAME_MAX_LEN];
	unsigned int api_id;
	
	if (MIPC_ERROR != mipc_receive_msg(source_module, &api_id, inMsg, size))
		{
			// Call APIs
		alarm_mipc_server_call(source_module, api_id, inMsg + sizeof(MIPC_MSG_HDR_T));
	}
}


