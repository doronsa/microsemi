/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _alarm_h_
#define _alarm_h_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	uint32_t type;
	uint32_t param1;
	uint32_t param2;
	uint8_t alarm_class;
	uint8_t admin;
	uint32_t threshold;
	uint32_t clear_threshold;
} apm_alarm_create_entity_IN_T;


typedef struct {
	bool ret;
} apm_alarm_create_entity_OUT_T;


typedef struct {
	uint32_t type;
	uint32_t param1;
} apm_alarm_delete_entity_IN_T;


typedef struct {
	bool ret;
} apm_alarm_delete_entity_OUT_T;


typedef struct {
	uint32_t type;
	uint32_t param1;
	uint8_t admin;
} apm_alarm_set_admin_IN_T;


typedef struct {
	bool ret;
} apm_alarm_set_admin_OUT_T;


typedef struct {
	uint32_t type;
	uint32_t param1;
	uint32_t threshold;
	uint32_t clear_threshold;
} apm_alarm_set_threshold_IN_T;


typedef struct {
	bool ret;
} apm_alarm_set_threshold_OUT_T;


typedef struct {
	bool ret;
} apm_alarm_db_obj_reset_OUT_T;


typedef struct {
	char name[256];
} Apm_cli_show_alarm_statistics_IN_T;


typedef struct {
	bool ret;
} Apm_cli_show_alarm_statistics_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
} Apm_cli_show_alarm_entity_IN_T;


typedef struct {
	bool ret;
} Apm_cli_show_alarm_entity_OUT_T;


typedef struct {
	char name[256];
} Apm_cli_show_alarm_list_IN_T;


typedef struct {
	bool ret;
} Apm_cli_show_alarm_list_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	uint32_t param2;
	uint8_t alarm_class;
	uint8_t admin;
	uint32_t threshold;
	uint32_t clear_threshold;
} Apm_cli_create_alarm_entity_IN_T;


typedef struct {
	bool ret;
} Apm_cli_create_alarm_entity_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
} Apm_cli_delete_alarm_entity_IN_T;


typedef struct {
	bool ret;
} Apm_cli_delete_alarm_entity_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	uint8_t admin;
} Apm_cli_set_alarm_admin_IN_T;


typedef struct {
	bool ret;
} Apm_cli_set_alarm_admin_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	uint32_t threshold;
	uint32_t clear_threshold;
} Apm_cli_set_alarm_theshold_IN_T;


typedef struct {
	bool ret;
} Apm_cli_set_alarm_theshold_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	uint8_t state;
	uint32_t info;
} Apm_cli_set_alarm_state_info_IN_T;


typedef struct {
	bool ret;
} Apm_cli_set_alarm_state_info_OUT_T;


#ifdef __cplusplus
}
#endif
#endif
