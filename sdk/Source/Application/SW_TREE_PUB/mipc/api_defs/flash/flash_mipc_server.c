/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "FlashExports.h"

#include "flash_mipc_defs.h"


void flash_mipc_server_call(char* source_module, int api_id, char* input)
{
	switch (api_id)
	{
		case 0:
		{
			Flash_cli_write_image_to_flash_IN_T* in = (Flash_cli_write_image_to_flash_IN_T*)input;
			Flash_cli_write_image_to_flash_OUT_T out;

			out.ret = Flash_cli_write_image_to_flash((void*)source_module, in->bank_id);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 0, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 1:
		{
			Flash_cli_show_resources_IN_T* in = (Flash_cli_show_resources_IN_T*)input;
			Flash_cli_show_resources_OUT_T out;

			out.ret = Flash_cli_show_resources((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 1, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 2:
		{
			Flash_cli_show_statistics_IN_T* in = (Flash_cli_show_statistics_IN_T*)input;
			Flash_cli_show_statistics_OUT_T out;

			out.ret = Flash_cli_show_statistics((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 2, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 3:
		{
			Flash_cli_show_sw_image_uboot_vars_IN_T* in = (Flash_cli_show_sw_image_uboot_vars_IN_T*)input;
			Flash_cli_show_sw_image_uboot_vars_OUT_T out;

			out.ret = Flash_cli_show_sw_image_uboot_vars((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 3, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 4:
		{
			Flash_cli_commit_image_IN_T* in = (Flash_cli_commit_image_IN_T*)input;
			Flash_cli_commit_image_OUT_T out;

			out.ret = Flash_cli_commit_image((void*)source_module, in->bank_id);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 4, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 5:
		{
			Flash_cli_activate_image_IN_T* in = (Flash_cli_activate_image_IN_T*)input;
			Flash_cli_activate_image_OUT_T out;

			out.ret = Flash_cli_activate_image((void*)source_module, in->bank_id, in->need_reboot);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 5, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 6:
		{
			Flash_cli_is_dual_image_configured_IN_T* in = (Flash_cli_is_dual_image_configured_IN_T*)input;
			Flash_cli_is_dual_image_configured_OUT_T out;

			out.ret = Flash_cli_is_dual_image_configured((void*)source_module, &out.isDualImage);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 6, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 7:
		{
			FlashApi_setCommittedBank_IN_T* in = (FlashApi_setCommittedBank_IN_T*)input;
			FlashApi_setCommittedBank_OUT_T out;

			out.ret = FlashApi_setCommittedBank(in->bank);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 7, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 8:
		{
			FlashApi_prepareImageActivation_IN_T* in = (FlashApi_prepareImageActivation_IN_T*)input;
			FlashApi_prepareImageActivation_OUT_T out;

			out.ret = FlashApi_prepareImageActivation(in->bank);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 8, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 9:
		{
			FlashApi_setImageToInvalid_IN_T* in = (FlashApi_setImageToInvalid_IN_T*)input;
			FlashApi_setImageToInvalid_OUT_T out;

			out.ret = FlashApi_setImageToInvalid(in->bank);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 9, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 10:
		{
			FlashApi_getBurnStatus_OUT_T out;

			out.ret = FlashApi_getBurnStatus(&out.status);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 10, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 11:
		{
			FlashApi_startWriteFlash_IN_T* in = (FlashApi_startWriteFlash_IN_T*)input;
			FlashApi_startWriteFlash_OUT_T out;

			out.ret = FlashApi_startWriteFlash(in->bank, in->filename, in->clientId);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 11, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 12:
		{
			FlashApi_abortWriteFlash_OUT_T out;

			out.ret = FlashApi_abortWriteFlash();


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 12, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 13:
		{
			FlashApi_isDualImageConfigured_OUT_T out;

			out.ret = FlashApi_isDualImageConfigured(&out.isDualImage);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 13, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		default:
			break;
	}
}


void flash_mipc_server_handler(char* inMsg, unsigned int size)
{
	char source_module[MIPC_NAME_MAX_LEN];
	unsigned int api_id;
	
	if (MIPC_ERROR != mipc_receive_msg(source_module, &api_id, inMsg, size))
		{
			// Call APIs
		flash_mipc_server_call(source_module, api_id, inMsg + sizeof(MIPC_MSG_HDR_T));
	}
}


