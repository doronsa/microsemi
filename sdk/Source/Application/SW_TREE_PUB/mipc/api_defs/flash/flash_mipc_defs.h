/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _flash_h_
#define _flash_h_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	char name[256];
	int bank_id;
} Flash_cli_write_image_to_flash_IN_T;


typedef struct {
	bool ret;
} Flash_cli_write_image_to_flash_OUT_T;


typedef struct {
	char name[256];
} Flash_cli_show_resources_IN_T;


typedef struct {
	bool ret;
} Flash_cli_show_resources_OUT_T;


typedef struct {
	char name[256];
} Flash_cli_show_statistics_IN_T;


typedef struct {
	bool ret;
} Flash_cli_show_statistics_OUT_T;


typedef struct {
	char name[256];
} Flash_cli_show_sw_image_uboot_vars_IN_T;


typedef struct {
	bool ret;
} Flash_cli_show_sw_image_uboot_vars_OUT_T;


typedef struct {
	char name[256];
	int bank_id;
} Flash_cli_commit_image_IN_T;


typedef struct {
	bool ret;
} Flash_cli_commit_image_OUT_T;


typedef struct {
	char name[256];
	int bank_id;
	int need_reboot;
} Flash_cli_activate_image_IN_T;


typedef struct {
	bool ret;
} Flash_cli_activate_image_OUT_T;


typedef struct {
	char name[256];
} Flash_cli_is_dual_image_configured_IN_T;


typedef struct {
	bool ret;
	bool isDualImage;
} Flash_cli_is_dual_image_configured_OUT_T;


typedef struct {
	UINT8 bank;
} FlashApi_setCommittedBank_IN_T;


typedef struct {
	bool ret;
} FlashApi_setCommittedBank_OUT_T;


typedef struct {
	UINT8 bank;
} FlashApi_prepareImageActivation_IN_T;


typedef struct {
	bool ret;
} FlashApi_prepareImageActivation_OUT_T;


typedef struct {
	UINT8 bank;
} FlashApi_setImageToInvalid_IN_T;


typedef struct {
	bool ret;
} FlashApi_setImageToInvalid_OUT_T;


typedef struct {
	bool ret;
	BurnStatus_S status;
} FlashApi_getBurnStatus_OUT_T;


typedef struct {
	UINT8 bank;
	char filename[256];
	char clientId[256];
} FlashApi_startWriteFlash_IN_T;


typedef struct {
	bool ret;
} FlashApi_startWriteFlash_OUT_T;


typedef struct {
	bool ret;
} FlashApi_abortWriteFlash_OUT_T;


typedef struct {
	bool ret;
	bool isDualImage;
} FlashApi_isDualImageConfigured_OUT_T;


#ifdef __cplusplus
}
#endif
#endif
