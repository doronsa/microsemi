/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "FlashExports.h"

#include "flash_mipc_defs.h"


bool Flash_cli_write_image_to_flash(char* name, int bank_id)
{
	Flash_cli_write_image_to_flash_IN_T input;
	Flash_cli_write_image_to_flash_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.bank_id = bank_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("flash", 0, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Flash_cli_show_resources(char* name)
{
	Flash_cli_show_resources_IN_T input;
	Flash_cli_show_resources_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("flash", 1, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Flash_cli_show_statistics(char* name)
{
	Flash_cli_show_statistics_IN_T input;
	Flash_cli_show_statistics_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("flash", 2, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Flash_cli_show_sw_image_uboot_vars(char* name)
{
	Flash_cli_show_sw_image_uboot_vars_IN_T input;
	Flash_cli_show_sw_image_uboot_vars_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("flash", 3, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Flash_cli_commit_image(char* name, int bank_id)
{
	Flash_cli_commit_image_IN_T input;
	Flash_cli_commit_image_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.bank_id = bank_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("flash", 4, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Flash_cli_activate_image(char* name, int bank_id, int need_reboot)
{
	Flash_cli_activate_image_IN_T input;
	Flash_cli_activate_image_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.bank_id = bank_id;
	input.need_reboot = need_reboot;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("flash", 5, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Flash_cli_is_dual_image_configured(char* name, bool* isDualImage)
{
	Flash_cli_is_dual_image_configured_IN_T input;
	Flash_cli_is_dual_image_configured_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("flash", 6, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(isDualImage, &output.isDualImage, sizeof(bool));

	return output.ret;
}


bool FlashApi_setCommittedBank(UINT8 bank)
{
	FlashApi_setCommittedBank_IN_T input;
	FlashApi_setCommittedBank_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.bank = bank;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("flash", 7, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool FlashApi_prepareImageActivation(UINT8 bank)
{
	FlashApi_prepareImageActivation_IN_T input;
	FlashApi_prepareImageActivation_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.bank = bank;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("flash", 8, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool FlashApi_setImageToInvalid(UINT8 bank)
{
	FlashApi_setImageToInvalid_IN_T input;
	FlashApi_setImageToInvalid_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.bank = bank;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("flash", 9, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool FlashApi_getBurnStatus(BurnStatus_S* status)
{
	FlashApi_getBurnStatus_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("flash", 10, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(status, &output.status, sizeof(BurnStatus_S));

	return output.ret;
}


bool FlashApi_startWriteFlash(UINT8 bank, char* filename, char* clientId)
{
	FlashApi_startWriteFlash_IN_T input;
	FlashApi_startWriteFlash_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.bank = bank;
   if (NULL != filename)
	    strcpy(input.filename, filename);
   if (NULL != clientId)
	    strcpy(input.clientId, clientId);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("flash", 11, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool FlashApi_abortWriteFlash(void)
{
	FlashApi_abortWriteFlash_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("flash", 12, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool FlashApi_isDualImageConfigured(bool* isDualImage)
{
	FlashApi_isDualImageConfigured_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("flash", 13, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(isDualImage, &output.isDualImage, sizeof(bool));

	return output.ret;
}


