/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "ponOnuMngIf.h"

#include "apm_pm_api.h"

#include "pm_mipc_defs.h"


bool apm_pm_get_data(uint32_t type, uint32_t param1, APM_PM_DATA_T* pmData)
{
	apm_pm_get_data_IN_T input;
	apm_pm_get_data_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("pm", 0, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(pmData, &output.pmData, sizeof(APM_PM_DATA_T));

	return output.ret;
}


bool apm_pm_create_entity(uint32_t type, uint32_t param1, uint32_t param2, uint16_t admin_bits, uint8_t accumulation_mode, uint32_t interval)
{
	apm_pm_create_entity_IN_T input;
	apm_pm_create_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;
	input.param2 = param2;
	input.admin_bits = admin_bits;
	input.accumulation_mode = accumulation_mode;
	input.interval = interval;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("pm", 1, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_pm_delete_entity(uint32_t type, uint32_t param1)
{
	apm_pm_delete_entity_IN_T input;
	apm_pm_delete_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("pm", 2, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_pm_set_admin(uint32_t type, uint32_t param1, uint16_t admin_bits)
{
	apm_pm_set_admin_IN_T input;
	apm_pm_set_admin_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;
	input.admin_bits = admin_bits;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("pm", 3, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_pm_set_accumulation_mode(uint32_t pm_type, uint32_t param1, uint8_t accumulation_mode)
{
	apm_pm_set_accumulation_mode_IN_T input;
	apm_pm_set_accumulation_mode_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.pm_type = pm_type;
	input.param1 = param1;
	input.accumulation_mode = accumulation_mode;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("pm", 4, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_pm_set_global_clear(uint32_t pm_type, uint32_t param1)
{
	apm_pm_set_global_clear_IN_T input;
	apm_pm_set_global_clear_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.pm_type = pm_type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("pm", 5, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_pm_synchronize(void)
{
	apm_pm_synchronize_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("pm", 6, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_pm_db_obj_reset(void)
{
	apm_pm_db_obj_reset_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("pm", 7, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_pm_set_interval(uint32_t type, uint32_t param1, uint32_t g_apm_pm_interval)
{
	apm_pm_set_interval_IN_T input;
	apm_pm_set_interval_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;
	input.g_apm_pm_interval = g_apm_pm_interval;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("pm", 8, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_pm_get_related_alarm_types(uint32_t pm_type, APM_PM_RELATED_ALARM_TYPES_T* pm_related_alarm_types)
{
	apm_pm_get_related_alarm_types_IN_T input;
	apm_pm_get_related_alarm_types_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.pm_type = pm_type;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("pm", 9, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(pm_related_alarm_types, &output.pm_related_alarm_types, sizeof(APM_PM_RELATED_ALARM_TYPES_T));

	return output.ret;
}


bool Apm_cli_show_pm_statistics(char* name)
{
	Apm_cli_show_pm_statistics_IN_T input;
	Apm_cli_show_pm_statistics_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("pm", 10, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_show_pm_entity(char* name, uint32_t type, uint32_t param1)
{
	Apm_cli_show_pm_entity_IN_T input;
	Apm_cli_show_pm_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("pm", 11, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_show_pm_list(char* name)
{
	Apm_cli_show_pm_list_IN_T input;
	Apm_cli_show_pm_list_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("pm", 12, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_set_pm_interval(char* name, uint32_t type, uint32_t param1, int arg)
{
	Apm_cli_set_pm_interval_IN_T input;
	Apm_cli_set_pm_interval_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.arg = arg;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("pm", 13, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_create_pm_entity(char* name, uint32_t type, uint32_t param1, uint32_t param2, uint16_t admin_bits, uint8_t accumulation_mode)
{
	Apm_cli_create_pm_entity_IN_T input;
	Apm_cli_create_pm_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.param2 = param2;
	input.admin_bits = admin_bits;
	input.accumulation_mode = accumulation_mode;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("pm", 14, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_delete_pm_entity(char* name, uint32_t type, uint32_t param1)
{
	Apm_cli_delete_pm_entity_IN_T input;
	Apm_cli_delete_pm_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("pm", 15, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_set_pm_accumulation_mode(char* name, uint32_t type, uint32_t param1, uint8_t accumulation_mode)
{
	Apm_cli_set_pm_accumulation_mode_IN_T input;
	Apm_cli_set_pm_accumulation_mode_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.accumulation_mode = accumulation_mode;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("pm", 16, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_set_pm_admin_bits(char* name, uint32_t type, uint32_t param1, uint16_t admin_bits)
{
	Apm_cli_set_pm_admin_bits_IN_T input;
	Apm_cli_set_pm_admin_bits_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.admin_bits = admin_bits;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("pm", 17, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_set_pm_pm_data(char* name, uint32_t type, uint32_t param1, uint32_t pm_data, uint32_t pm_index)
{
	Apm_cli_set_pm_pm_data_IN_T input;
	Apm_cli_set_pm_pm_data_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.pm_data = pm_data;
	input.pm_index = pm_index;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("pm", 18, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_set_pm_global_clear(char* name, uint32_t type, uint32_t param1)
{
	Apm_cli_set_pm_global_clear_IN_T input;
	Apm_cli_set_pm_global_clear_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("pm", 19, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_synchronize_time(char* name)
{
	Apm_cli_synchronize_time_IN_T input;
	Apm_cli_synchronize_time_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("pm", 20, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


