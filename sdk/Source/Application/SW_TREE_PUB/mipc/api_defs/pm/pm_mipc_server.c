/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "ponOnuMngIf.h"

#include "apm_pm_api.h"

#include "pm_mipc_defs.h"


void pm_mipc_server_call(char* source_module, int api_id, char* input)
{
	switch (api_id)
	{
		case 0:
		{
			apm_pm_get_data_IN_T* in = (apm_pm_get_data_IN_T*)input;
			apm_pm_get_data_OUT_T out;

			out.ret = apm_pm_get_data(in->type, in->param1, &out.pmData);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 0, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 1:
		{
			apm_pm_create_entity_IN_T* in = (apm_pm_create_entity_IN_T*)input;
			apm_pm_create_entity_OUT_T out;

			out.ret = apm_pm_create_entity(in->type, in->param1, in->param2, in->admin_bits, in->accumulation_mode, in->interval);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 1, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 2:
		{
			apm_pm_delete_entity_IN_T* in = (apm_pm_delete_entity_IN_T*)input;
			apm_pm_delete_entity_OUT_T out;

			out.ret = apm_pm_delete_entity(in->type, in->param1);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 2, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 3:
		{
			apm_pm_set_admin_IN_T* in = (apm_pm_set_admin_IN_T*)input;
			apm_pm_set_admin_OUT_T out;

			out.ret = apm_pm_set_admin(in->type, in->param1, in->admin_bits);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 3, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 4:
		{
			apm_pm_set_accumulation_mode_IN_T* in = (apm_pm_set_accumulation_mode_IN_T*)input;
			apm_pm_set_accumulation_mode_OUT_T out;

			out.ret = apm_pm_set_accumulation_mode(in->pm_type, in->param1, in->accumulation_mode);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 4, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 5:
		{
			apm_pm_set_global_clear_IN_T* in = (apm_pm_set_global_clear_IN_T*)input;
			apm_pm_set_global_clear_OUT_T out;

			out.ret = apm_pm_set_global_clear(in->pm_type, in->param1);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 5, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 6:
		{
			apm_pm_synchronize_OUT_T out;

			out.ret = apm_pm_synchronize();


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 6, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 7:
		{
			apm_pm_db_obj_reset_OUT_T out;

			out.ret = apm_pm_db_obj_reset();


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 7, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 8:
		{
			apm_pm_set_interval_IN_T* in = (apm_pm_set_interval_IN_T*)input;
			apm_pm_set_interval_OUT_T out;

			out.ret = apm_pm_set_interval(in->type, in->param1, in->g_apm_pm_interval);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 8, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 9:
		{
			apm_pm_get_related_alarm_types_IN_T* in = (apm_pm_get_related_alarm_types_IN_T*)input;
			apm_pm_get_related_alarm_types_OUT_T out;

			out.ret = apm_pm_get_related_alarm_types(in->pm_type, &out.pm_related_alarm_types);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 9, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 10:
		{
			Apm_cli_show_pm_statistics_IN_T* in = (Apm_cli_show_pm_statistics_IN_T*)input;
			Apm_cli_show_pm_statistics_OUT_T out;

			out.ret = Apm_cli_show_pm_statistics((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 10, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 11:
		{
			Apm_cli_show_pm_entity_IN_T* in = (Apm_cli_show_pm_entity_IN_T*)input;
			Apm_cli_show_pm_entity_OUT_T out;

			out.ret = Apm_cli_show_pm_entity((void*)source_module, in->type, in->param1);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 11, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 12:
		{
			Apm_cli_show_pm_list_IN_T* in = (Apm_cli_show_pm_list_IN_T*)input;
			Apm_cli_show_pm_list_OUT_T out;

			out.ret = Apm_cli_show_pm_list((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 12, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 13:
		{
			Apm_cli_set_pm_interval_IN_T* in = (Apm_cli_set_pm_interval_IN_T*)input;
			Apm_cli_set_pm_interval_OUT_T out;

			out.ret = Apm_cli_set_pm_interval((void*)source_module, in->type, in->param1, in->arg);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 13, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 14:
		{
			Apm_cli_create_pm_entity_IN_T* in = (Apm_cli_create_pm_entity_IN_T*)input;
			Apm_cli_create_pm_entity_OUT_T out;

			out.ret = Apm_cli_create_pm_entity((void*)source_module, in->type, in->param1, in->param2, in->admin_bits, in->accumulation_mode);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 14, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 15:
		{
			Apm_cli_delete_pm_entity_IN_T* in = (Apm_cli_delete_pm_entity_IN_T*)input;
			Apm_cli_delete_pm_entity_OUT_T out;

			out.ret = Apm_cli_delete_pm_entity((void*)source_module, in->type, in->param1);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 15, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 16:
		{
			Apm_cli_set_pm_accumulation_mode_IN_T* in = (Apm_cli_set_pm_accumulation_mode_IN_T*)input;
			Apm_cli_set_pm_accumulation_mode_OUT_T out;

			out.ret = Apm_cli_set_pm_accumulation_mode((void*)source_module, in->type, in->param1, in->accumulation_mode);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 16, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 17:
		{
			Apm_cli_set_pm_admin_bits_IN_T* in = (Apm_cli_set_pm_admin_bits_IN_T*)input;
			Apm_cli_set_pm_admin_bits_OUT_T out;

			out.ret = Apm_cli_set_pm_admin_bits((void*)source_module, in->type, in->param1, in->admin_bits);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 17, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 18:
		{
			Apm_cli_set_pm_pm_data_IN_T* in = (Apm_cli_set_pm_pm_data_IN_T*)input;
			Apm_cli_set_pm_pm_data_OUT_T out;

			out.ret = Apm_cli_set_pm_pm_data((void*)source_module, in->type, in->param1, in->pm_data, in->pm_index);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 18, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 19:
		{
			Apm_cli_set_pm_global_clear_IN_T* in = (Apm_cli_set_pm_global_clear_IN_T*)input;
			Apm_cli_set_pm_global_clear_OUT_T out;

			out.ret = Apm_cli_set_pm_global_clear((void*)source_module, in->type, in->param1);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 19, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 20:
		{
			Apm_cli_synchronize_time_IN_T* in = (Apm_cli_synchronize_time_IN_T*)input;
			Apm_cli_synchronize_time_OUT_T out;

			out.ret = Apm_cli_synchronize_time((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 20, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		default:
			break;
	}
}


void pm_mipc_server_handler(char* inMsg, unsigned int size)
{
	char source_module[MIPC_NAME_MAX_LEN];
	unsigned int api_id;
	
	if (MIPC_ERROR != mipc_receive_msg(source_module, &api_id, inMsg, size))
		{
			// Call APIs
		pm_mipc_server_call(source_module, api_id, inMsg + sizeof(MIPC_MSG_HDR_T));
	}
}


