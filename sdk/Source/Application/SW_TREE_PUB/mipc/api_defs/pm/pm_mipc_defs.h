/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _pm_h_
#define _pm_h_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	uint32_t type;
	uint32_t param1;
} apm_pm_get_data_IN_T;


typedef struct {
	bool ret;
	APM_PM_DATA_T pmData;
} apm_pm_get_data_OUT_T;


typedef struct {
	uint32_t type;
	uint32_t param1;
	uint32_t param2;
	uint16_t admin_bits;
	uint8_t accumulation_mode;
	uint32_t interval;
} apm_pm_create_entity_IN_T;


typedef struct {
	bool ret;
} apm_pm_create_entity_OUT_T;


typedef struct {
	uint32_t type;
	uint32_t param1;
} apm_pm_delete_entity_IN_T;


typedef struct {
	bool ret;
} apm_pm_delete_entity_OUT_T;


typedef struct {
	uint32_t type;
	uint32_t param1;
	uint16_t admin_bits;
} apm_pm_set_admin_IN_T;


typedef struct {
	bool ret;
} apm_pm_set_admin_OUT_T;


typedef struct {
	uint32_t pm_type;
	uint32_t param1;
	uint8_t accumulation_mode;
} apm_pm_set_accumulation_mode_IN_T;


typedef struct {
	bool ret;
} apm_pm_set_accumulation_mode_OUT_T;


typedef struct {
	uint32_t pm_type;
	uint32_t param1;
} apm_pm_set_global_clear_IN_T;


typedef struct {
	bool ret;
} apm_pm_set_global_clear_OUT_T;


typedef struct {
	bool ret;
} apm_pm_synchronize_OUT_T;


typedef struct {
	bool ret;
} apm_pm_db_obj_reset_OUT_T;


typedef struct {
	uint32_t type;
	uint32_t param1;
	uint32_t g_apm_pm_interval;
} apm_pm_set_interval_IN_T;


typedef struct {
	bool ret;
} apm_pm_set_interval_OUT_T;


typedef struct {
	uint32_t pm_type;
} apm_pm_get_related_alarm_types_IN_T;


typedef struct {
	bool ret;
	APM_PM_RELATED_ALARM_TYPES_T pm_related_alarm_types;
} apm_pm_get_related_alarm_types_OUT_T;


typedef struct {
	char name[256];
} Apm_cli_show_pm_statistics_IN_T;


typedef struct {
	bool ret;
} Apm_cli_show_pm_statistics_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
} Apm_cli_show_pm_entity_IN_T;


typedef struct {
	bool ret;
} Apm_cli_show_pm_entity_OUT_T;


typedef struct {
	char name[256];
} Apm_cli_show_pm_list_IN_T;


typedef struct {
	bool ret;
} Apm_cli_show_pm_list_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	int arg;
} Apm_cli_set_pm_interval_IN_T;


typedef struct {
	bool ret;
} Apm_cli_set_pm_interval_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	uint32_t param2;
	uint16_t admin_bits;
	uint8_t accumulation_mode;
} Apm_cli_create_pm_entity_IN_T;


typedef struct {
	bool ret;
} Apm_cli_create_pm_entity_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
} Apm_cli_delete_pm_entity_IN_T;


typedef struct {
	bool ret;
} Apm_cli_delete_pm_entity_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	uint8_t accumulation_mode;
} Apm_cli_set_pm_accumulation_mode_IN_T;


typedef struct {
	bool ret;
} Apm_cli_set_pm_accumulation_mode_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	uint16_t admin_bits;
} Apm_cli_set_pm_admin_bits_IN_T;


typedef struct {
	bool ret;
} Apm_cli_set_pm_admin_bits_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	uint32_t pm_data;
	uint32_t pm_index;
} Apm_cli_set_pm_pm_data_IN_T;


typedef struct {
	bool ret;
} Apm_cli_set_pm_pm_data_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
} Apm_cli_set_pm_global_clear_IN_T;


typedef struct {
	bool ret;
} Apm_cli_set_pm_global_clear_OUT_T;


typedef struct {
	char name[256];
} Apm_cli_synchronize_time_IN_T;


typedef struct {
	bool ret;
} Apm_cli_synchronize_time_OUT_T;


#ifdef __cplusplus
}
#endif
#endif
