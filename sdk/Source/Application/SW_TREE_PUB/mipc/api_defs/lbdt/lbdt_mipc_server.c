/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include "globals.h"

#include "lbdt_api.h"

#include "lbdt_mipc_defs.h"


void lbdt_mipc_server_call(char* source_module, int api_id, char* input)
{
	switch (api_id)
	{
		case 0:
		{
			lbdt_print_data_info_IN_T* in = (lbdt_print_data_info_IN_T*)input;
			lbdt_print_data_info_OUT_T out;

			out.ret = lbdt_print_data_info((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 0, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 1:
		{
			lbdt_set_log_level_IN_T* in = (lbdt_set_log_level_IN_T*)input;
			lbdt_set_log_level_OUT_T out;

			out.ret = lbdt_set_log_level((void*)source_module, in->level);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 1, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 2:
		{
			lbdt_set_ethernet_type_IN_T* in = (lbdt_set_ethernet_type_IN_T*)input;
			lbdt_set_ethernet_type_OUT_T out;

			out.ret = lbdt_set_ethernet_type((void*)source_module, in->ethernet_type);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 2, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 3:
		{
			lbdt_set_pkt_interval_IN_T* in = (lbdt_set_pkt_interval_IN_T*)input;
			lbdt_set_pkt_interval_OUT_T out;

			out.ret = lbdt_set_pkt_interval((void*)source_module, in->interval);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 3, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 4:
		{
			lbdt_set_recovery_uni_interval_IN_T* in = (lbdt_set_recovery_uni_interval_IN_T*)input;
			lbdt_set_recovery_uni_interval_OUT_T out;

			out.ret = lbdt_set_recovery_uni_interval((void*)source_module, in->timeinterval);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 4, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 5:
		{
			lbdt_set_lbdt_manage_IN_T* in = (lbdt_set_lbdt_manage_IN_T*)input;
			lbdt_set_lbdt_manage_OUT_T out;

			out.ret = lbdt_set_lbdt_manage((void*)source_module, in->portId, in->loopswitch);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 5, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 6:
		{
			lbdt_set_port_shut_down_option_IN_T* in = (lbdt_set_port_shut_down_option_IN_T*)input;
			lbdt_set_port_shut_down_option_OUT_T out;

			out.ret = lbdt_set_port_shut_down_option((void*)source_module, in->portId, in->loopctrl);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 6, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 7:
		{
			lbdt_tag_del_IN_T* in = (lbdt_tag_del_IN_T*)input;
			lbdt_tag_del_OUT_T out;

			out.ret = lbdt_tag_del((void*)source_module, in->portId);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 7, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 8:
		{
			lbdt_tag_add_IN_T* in = (lbdt_tag_add_IN_T*)input;
			lbdt_tag_add_OUT_T out;

			out.ret = lbdt_tag_add((void*)source_module, in->portId, in->svlan, in->cvlan, in->untagctrl);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 8, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 9:
		{
			lbdt_get_alarm_OUT_T out;

			out.ret = lbdt_get_alarm(&out.loop_alarm);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 9, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 10:
		{
			lbdt_port_tag_information_set_IN_T* in = (lbdt_port_tag_information_set_IN_T*)input;
			lbdt_port_tag_information_set_OUT_T out;

			out.ret = lbdt_port_tag_information_set(in->portId, &in->portTagInfo);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 10, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 11:
		{
			lbdt_port_tag_information_del_IN_T* in = (lbdt_port_tag_information_del_IN_T*)input;
			lbdt_port_tag_information_del_OUT_T out;

			out.ret = lbdt_port_tag_information_del(in->portId);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 11, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 12:
		{
			lbdt_packet_interval_set_IN_T* in = (lbdt_packet_interval_set_IN_T*)input;
			lbdt_packet_interval_set_OUT_T out;

			out.ret = lbdt_packet_interval_set(in->pktInter);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 12, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 13:
		{
			lbdt_recovery_interval_time_set_IN_T* in = (lbdt_recovery_interval_time_set_IN_T*)input;
			lbdt_recovery_interval_time_set_OUT_T out;

			out.ret = lbdt_recovery_interval_time_set(in->recIntervalTime);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 13, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 14:
		{
			lbdt_port_manage_set_IN_T* in = (lbdt_port_manage_set_IN_T*)input;
			lbdt_port_manage_set_OUT_T out;

			out.ret = lbdt_port_manage_set(in->portId, in->portManage);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 14, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 15:
		{
			lbdt_port_down_option_set_IN_T* in = (lbdt_port_down_option_set_IN_T*)input;
			lbdt_port_down_option_set_OUT_T out;

			out.ret = lbdt_port_down_option_set(in->portId, in->portDownOpt);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 15, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		default:
			break;
	}
}


void lbdt_mipc_server_handler(char* inMsg, unsigned int size)
{
	char source_module[MIPC_NAME_MAX_LEN];
	unsigned int api_id;
	
	if (MIPC_ERROR != mipc_receive_msg(source_module, &api_id, inMsg, size))
		{
			// Call APIs
		lbdt_mipc_server_call(source_module, api_id, inMsg + sizeof(MIPC_MSG_HDR_T));
	}
}


