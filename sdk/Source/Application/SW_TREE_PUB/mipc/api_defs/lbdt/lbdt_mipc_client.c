/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include "globals.h"

#include "lbdt_api.h"

#include "lbdt_mipc_defs.h"


BOOL lbdt_print_data_info(char* name)
{
	lbdt_print_data_info_IN_T input;
	lbdt_print_data_info_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("lbdt", 0, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


BOOL lbdt_set_log_level(char* name, UINT32 level)
{
	lbdt_set_log_level_IN_T input;
	lbdt_set_log_level_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.level = level;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("lbdt", 1, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


BOOL lbdt_set_ethernet_type(char* name, UINT32 ethernet_type)
{
	lbdt_set_ethernet_type_IN_T input;
	lbdt_set_ethernet_type_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.ethernet_type = ethernet_type;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("lbdt", 2, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


BOOL lbdt_set_pkt_interval(char* name, UINT32 interval)
{
	lbdt_set_pkt_interval_IN_T input;
	lbdt_set_pkt_interval_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.interval = interval;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("lbdt", 3, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


BOOL lbdt_set_recovery_uni_interval(char* name, UINT32 timeinterval)
{
	lbdt_set_recovery_uni_interval_IN_T input;
	lbdt_set_recovery_uni_interval_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.timeinterval = timeinterval;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("lbdt", 4, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


BOOL lbdt_set_lbdt_manage(char* name, UINT32 portId, UINT32 loopswitch)
{
	lbdt_set_lbdt_manage_IN_T input;
	lbdt_set_lbdt_manage_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.portId = portId;
	input.loopswitch = loopswitch;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("lbdt", 5, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


BOOL lbdt_set_port_shut_down_option(char* name, UINT32 portId, UINT32 loopctrl)
{
	lbdt_set_port_shut_down_option_IN_T input;
	lbdt_set_port_shut_down_option_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.portId = portId;
	input.loopctrl = loopctrl;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("lbdt", 6, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


BOOL lbdt_tag_del(char* name, UINT32 portId)
{
	lbdt_tag_del_IN_T input;
	lbdt_tag_del_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.portId = portId;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("lbdt", 7, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


BOOL lbdt_tag_add(char* name, UINT32 portId, UINT32 svlan, UINT32 cvlan, UINT32 untagctrl)
{
	lbdt_tag_add_IN_T input;
	lbdt_tag_add_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.portId = portId;
	input.svlan = svlan;
	input.cvlan = cvlan;
	input.untagctrl = untagctrl;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("lbdt", 8, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 lbdt_get_alarm(UINT32* loop_alarm)
{
	lbdt_get_alarm_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("lbdt", 9, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(loop_alarm, &output.loop_alarm, sizeof(UINT32));

	return output.ret;
}


INT32 lbdt_port_tag_information_set(UINT32 portId, LBDT_TAG_INFO_S* portTagInfo)
{
	lbdt_port_tag_information_set_IN_T input;
	lbdt_port_tag_information_set_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.portId = portId;
   if (NULL != portTagInfo)
	    memcpy(&input.portTagInfo, portTagInfo, sizeof(LBDT_TAG_INFO_S));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("lbdt", 10, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 lbdt_port_tag_information_del(UINT32 portId)
{
	lbdt_port_tag_information_del_IN_T input;
	lbdt_port_tag_information_del_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.portId = portId;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("lbdt", 11, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 lbdt_packet_interval_set(UINT32 pktInter)
{
	lbdt_packet_interval_set_IN_T input;
	lbdt_packet_interval_set_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.pktInter = pktInter;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("lbdt", 12, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 lbdt_recovery_interval_time_set(UINT32 recIntervalTime)
{
	lbdt_recovery_interval_time_set_IN_T input;
	lbdt_recovery_interval_time_set_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.recIntervalTime = recIntervalTime;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("lbdt", 13, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 lbdt_port_manage_set(UINT32 portId, UINT16 portManage)
{
	lbdt_port_manage_set_IN_T input;
	lbdt_port_manage_set_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.portId = portId;
	input.portManage = portManage;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("lbdt", 14, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 lbdt_port_down_option_set(UINT32 portId, UINT16 portDownOpt)
{
	lbdt_port_down_option_set_IN_T input;
	lbdt_port_down_option_set_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.portId = portId;
	input.portDownOpt = portDownOpt;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("lbdt", 15, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


