/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _lbdt_h_
#define _lbdt_h_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	char name[256];
} lbdt_print_data_info_IN_T;


typedef struct {
	BOOL ret;
} lbdt_print_data_info_OUT_T;


typedef struct {
	char name[256];
	UINT32 level;
} lbdt_set_log_level_IN_T;


typedef struct {
	BOOL ret;
} lbdt_set_log_level_OUT_T;


typedef struct {
	char name[256];
	UINT32 ethernet_type;
} lbdt_set_ethernet_type_IN_T;


typedef struct {
	BOOL ret;
} lbdt_set_ethernet_type_OUT_T;


typedef struct {
	char name[256];
	UINT32 interval;
} lbdt_set_pkt_interval_IN_T;


typedef struct {
	BOOL ret;
} lbdt_set_pkt_interval_OUT_T;


typedef struct {
	char name[256];
	UINT32 timeinterval;
} lbdt_set_recovery_uni_interval_IN_T;


typedef struct {
	BOOL ret;
} lbdt_set_recovery_uni_interval_OUT_T;


typedef struct {
	char name[256];
	UINT32 portId;
	UINT32 loopswitch;
} lbdt_set_lbdt_manage_IN_T;


typedef struct {
	BOOL ret;
} lbdt_set_lbdt_manage_OUT_T;


typedef struct {
	char name[256];
	UINT32 portId;
	UINT32 loopctrl;
} lbdt_set_port_shut_down_option_IN_T;


typedef struct {
	BOOL ret;
} lbdt_set_port_shut_down_option_OUT_T;


typedef struct {
	char name[256];
	UINT32 portId;
} lbdt_tag_del_IN_T;


typedef struct {
	BOOL ret;
} lbdt_tag_del_OUT_T;


typedef struct {
	char name[256];
	UINT32 portId;
	UINT32 svlan;
	UINT32 cvlan;
	UINT32 untagctrl;
} lbdt_tag_add_IN_T;


typedef struct {
	BOOL ret;
} lbdt_tag_add_OUT_T;


typedef struct {
	INT32 ret;
	UINT32 loop_alarm;
} lbdt_get_alarm_OUT_T;


typedef struct {
	UINT32 portId;
	LBDT_TAG_INFO_S portTagInfo;
} lbdt_port_tag_information_set_IN_T;


typedef struct {
	INT32 ret;
} lbdt_port_tag_information_set_OUT_T;


typedef struct {
	UINT32 portId;
} lbdt_port_tag_information_del_IN_T;


typedef struct {
	INT32 ret;
} lbdt_port_tag_information_del_OUT_T;


typedef struct {
	UINT32 pktInter;
} lbdt_packet_interval_set_IN_T;


typedef struct {
	INT32 ret;
} lbdt_packet_interval_set_OUT_T;


typedef struct {
	UINT32 recIntervalTime;
} lbdt_recovery_interval_time_set_IN_T;


typedef struct {
	INT32 ret;
} lbdt_recovery_interval_time_set_OUT_T;


typedef struct {
	UINT32 portId;
	UINT16 portManage;
} lbdt_port_manage_set_IN_T;


typedef struct {
	INT32 ret;
} lbdt_port_manage_set_OUT_T;


typedef struct {
	UINT32 portId;
	UINT16 portDownOpt;
} lbdt_port_down_option_set_IN_T;


typedef struct {
	INT32 ret;
} lbdt_port_down_option_set_OUT_T;


#ifdef __cplusplus
}
#endif
#endif
