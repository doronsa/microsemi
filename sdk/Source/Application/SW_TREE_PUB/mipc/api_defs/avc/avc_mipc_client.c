/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "ponOnuMngIf.h"

#include "apm_avc_api.h"

#include "avc_mipc_defs.h"


bool apm_avc_create_entity(uint32_t type, uint32_t param1, uint32_t param2, uint8_t admin)
{
	apm_avc_create_entity_IN_T input;
	apm_avc_create_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;
	input.param2 = param2;
	input.admin = admin;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("avc", 0, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_avc_delete_entity(uint32_t type, uint32_t param1)
{
	apm_avc_delete_entity_IN_T input;
	apm_avc_delete_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("avc", 1, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_avc_set_admin(uint32_t type, uint32_t param1, uint8_t admin)
{
	apm_avc_set_admin_IN_T input;
	apm_avc_set_admin_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;
	input.admin = admin;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("avc", 2, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_avc_set_param2(uint32_t type, uint32_t param1, uint32_t param2)
{
	apm_avc_set_param2_IN_T input;
	apm_avc_set_param2_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.type = type;
	input.param1 = param1;
	input.param2 = param2;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("avc", 3, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool apm_avc_db_obj_reset(void)
{
	apm_avc_db_obj_reset_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("avc", 4, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_show_avc_statistics(char* name)
{
	Apm_cli_show_avc_statistics_IN_T input;
	Apm_cli_show_avc_statistics_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("avc", 5, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_show_avc_entity(char* name, uint32_t type, uint32_t param1)
{
	Apm_cli_show_avc_entity_IN_T input;
	Apm_cli_show_avc_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("avc", 6, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_show_avc_list(char* name)
{
	Apm_cli_show_avc_list_IN_T input;
	Apm_cli_show_avc_list_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("avc", 7, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_show_pm_related_alarm_types(char* name, uint32_t pm_type)
{
	Apm_cli_show_pm_related_alarm_types_IN_T input;
	Apm_cli_show_pm_related_alarm_types_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.pm_type = pm_type;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("avc", 8, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_create_avc_entity(char* name, uint32_t type, uint32_t param1, uint32_t param2, uint8_t admin)
{
	Apm_cli_create_avc_entity_IN_T input;
	Apm_cli_create_avc_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.param2 = param2;
	input.admin = admin;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("avc", 9, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_delete_avc_entity(char* name, uint32_t type, uint32_t param1)
{
	Apm_cli_delete_avc_entity_IN_T input;
	Apm_cli_delete_avc_entity_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("avc", 10, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_set_avc_admin(char* name, uint32_t type, uint32_t param1, uint8_t admin)
{
	Apm_cli_set_avc_admin_IN_T input;
	Apm_cli_set_avc_admin_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.admin = admin;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("avc", 11, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_set_avc_value_int(char* name, uint32_t type, uint32_t param1, uint32_t value)
{
	Apm_cli_set_avc_value_int_IN_T input;
	Apm_cli_set_avc_value_int_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
	input.value = value;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("avc", 12, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Apm_cli_set_avc_value_str(char* name, uint32_t type, uint32_t param1, char* value)
{
	Apm_cli_set_avc_value_str_IN_T input;
	Apm_cli_set_avc_value_str_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.type = type;
	input.param1 = param1;
   if (NULL != value)
	    strcpy(input.value, value);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("avc", 13, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


