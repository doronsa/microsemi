/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "ponOnuMngIf.h"

#include "apm_avc_api.h"

#include "avc_mipc_defs.h"


void avc_mipc_server_call(char* source_module, int api_id, char* input)
{
	switch (api_id)
	{
		case 0:
		{
			apm_avc_create_entity_IN_T* in = (apm_avc_create_entity_IN_T*)input;
			apm_avc_create_entity_OUT_T out;

			out.ret = apm_avc_create_entity(in->type, in->param1, in->param2, in->admin);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 0, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 1:
		{
			apm_avc_delete_entity_IN_T* in = (apm_avc_delete_entity_IN_T*)input;
			apm_avc_delete_entity_OUT_T out;

			out.ret = apm_avc_delete_entity(in->type, in->param1);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 1, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 2:
		{
			apm_avc_set_admin_IN_T* in = (apm_avc_set_admin_IN_T*)input;
			apm_avc_set_admin_OUT_T out;

			out.ret = apm_avc_set_admin(in->type, in->param1, in->admin);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 2, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 3:
		{
			apm_avc_set_param2_IN_T* in = (apm_avc_set_param2_IN_T*)input;
			apm_avc_set_param2_OUT_T out;

			out.ret = apm_avc_set_param2(in->type, in->param1, in->param2);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 3, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 4:
		{
			apm_avc_db_obj_reset_OUT_T out;

			out.ret = apm_avc_db_obj_reset();


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 4, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 5:
		{
			Apm_cli_show_avc_statistics_IN_T* in = (Apm_cli_show_avc_statistics_IN_T*)input;
			Apm_cli_show_avc_statistics_OUT_T out;

			out.ret = Apm_cli_show_avc_statistics((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 5, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 6:
		{
			Apm_cli_show_avc_entity_IN_T* in = (Apm_cli_show_avc_entity_IN_T*)input;
			Apm_cli_show_avc_entity_OUT_T out;

			out.ret = Apm_cli_show_avc_entity((void*)source_module, in->type, in->param1);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 6, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 7:
		{
			Apm_cli_show_avc_list_IN_T* in = (Apm_cli_show_avc_list_IN_T*)input;
			Apm_cli_show_avc_list_OUT_T out;

			out.ret = Apm_cli_show_avc_list((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 7, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 8:
		{
			Apm_cli_show_pm_related_alarm_types_IN_T* in = (Apm_cli_show_pm_related_alarm_types_IN_T*)input;
			Apm_cli_show_pm_related_alarm_types_OUT_T out;

			out.ret = Apm_cli_show_pm_related_alarm_types((void*)source_module, in->pm_type);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 8, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 9:
		{
			Apm_cli_create_avc_entity_IN_T* in = (Apm_cli_create_avc_entity_IN_T*)input;
			Apm_cli_create_avc_entity_OUT_T out;

			out.ret = Apm_cli_create_avc_entity((void*)source_module, in->type, in->param1, in->param2, in->admin);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 9, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 10:
		{
			Apm_cli_delete_avc_entity_IN_T* in = (Apm_cli_delete_avc_entity_IN_T*)input;
			Apm_cli_delete_avc_entity_OUT_T out;

			out.ret = Apm_cli_delete_avc_entity((void*)source_module, in->type, in->param1);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 10, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 11:
		{
			Apm_cli_set_avc_admin_IN_T* in = (Apm_cli_set_avc_admin_IN_T*)input;
			Apm_cli_set_avc_admin_OUT_T out;

			out.ret = Apm_cli_set_avc_admin((void*)source_module, in->type, in->param1, in->admin);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 11, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 12:
		{
			Apm_cli_set_avc_value_int_IN_T* in = (Apm_cli_set_avc_value_int_IN_T*)input;
			Apm_cli_set_avc_value_int_OUT_T out;

			out.ret = Apm_cli_set_avc_value_int((void*)source_module, in->type, in->param1, in->value);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 12, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 13:
		{
			Apm_cli_set_avc_value_str_IN_T* in = (Apm_cli_set_avc_value_str_IN_T*)input;
			Apm_cli_set_avc_value_str_OUT_T out;

			out.ret = Apm_cli_set_avc_value_str((void*)source_module, in->type, in->param1, in->value);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 13, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		default:
			break;
	}
}


void avc_mipc_server_handler(char* inMsg, unsigned int size)
{
	char source_module[MIPC_NAME_MAX_LEN];
	unsigned int api_id;
	
	if (MIPC_ERROR != mipc_receive_msg(source_module, &api_id, inMsg, size))
		{
			// Call APIs
		avc_mipc_server_call(source_module, api_id, inMsg + sizeof(MIPC_MSG_HDR_T));
	}
}


