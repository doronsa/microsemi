/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _avc_h_
#define _avc_h_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	uint32_t type;
	uint32_t param1;
	uint32_t param2;
	uint8_t admin;
} apm_avc_create_entity_IN_T;


typedef struct {
	bool ret;
} apm_avc_create_entity_OUT_T;


typedef struct {
	uint32_t type;
	uint32_t param1;
} apm_avc_delete_entity_IN_T;


typedef struct {
	bool ret;
} apm_avc_delete_entity_OUT_T;


typedef struct {
	uint32_t type;
	uint32_t param1;
	uint8_t admin;
} apm_avc_set_admin_IN_T;


typedef struct {
	bool ret;
} apm_avc_set_admin_OUT_T;


typedef struct {
	uint32_t type;
	uint32_t param1;
	uint32_t param2;
} apm_avc_set_param2_IN_T;


typedef struct {
	bool ret;
} apm_avc_set_param2_OUT_T;


typedef struct {
	bool ret;
} apm_avc_db_obj_reset_OUT_T;


typedef struct {
	char name[256];
} Apm_cli_show_avc_statistics_IN_T;


typedef struct {
	bool ret;
} Apm_cli_show_avc_statistics_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
} Apm_cli_show_avc_entity_IN_T;


typedef struct {
	bool ret;
} Apm_cli_show_avc_entity_OUT_T;


typedef struct {
	char name[256];
} Apm_cli_show_avc_list_IN_T;


typedef struct {
	bool ret;
} Apm_cli_show_avc_list_OUT_T;


typedef struct {
	char name[256];
	uint32_t pm_type;
} Apm_cli_show_pm_related_alarm_types_IN_T;


typedef struct {
	bool ret;
} Apm_cli_show_pm_related_alarm_types_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	uint32_t param2;
	uint8_t admin;
} Apm_cli_create_avc_entity_IN_T;


typedef struct {
	bool ret;
} Apm_cli_create_avc_entity_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
} Apm_cli_delete_avc_entity_IN_T;


typedef struct {
	bool ret;
} Apm_cli_delete_avc_entity_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	uint8_t admin;
} Apm_cli_set_avc_admin_IN_T;


typedef struct {
	bool ret;
} Apm_cli_set_avc_admin_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	uint32_t value;
} Apm_cli_set_avc_value_int_IN_T;


typedef struct {
	bool ret;
} Apm_cli_set_avc_value_int_OUT_T;


typedef struct {
	char name[256];
	uint32_t type;
	uint32_t param1;
	char value[256];
} Apm_cli_set_avc_value_str_IN_T;


typedef struct {
	bool ret;
} Apm_cli_set_avc_value_str_OUT_T;


#ifdef __cplusplus
}
#endif
#endif
