/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include "globals.h"

#include "igmp_api.h"

#include "igmp_mipc_defs.h"


INT32 IGMP_set_multicast_vlan(UINT32 port_id, MULTICAST_VLAN_T* multicast_vlan)
{
	IGMP_set_multicast_vlan_IN_T input;
	IGMP_set_multicast_vlan_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;
   if (NULL != multicast_vlan)
	    memcpy(&input.multicast_vlan, multicast_vlan, sizeof(MULTICAST_VLAN_T));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 0, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 IGMP_eth_get_multicast_vlan(UINT32 port_id, MULTICAST_VLAN_T* multicast_vlan)
{
	IGMP_eth_get_multicast_vlan_IN_T input;
	IGMP_eth_get_multicast_vlan_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 1, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(multicast_vlan, &output.multicast_vlan, sizeof(MULTICAST_VLAN_T));

	return output.ret;
}


INT32 IGMP_set_multicast_tag_strip(UINT32 port_id, MULTICAST_PORT_TAG_OPER_T* tag_strip)
{
	IGMP_set_multicast_tag_strip_IN_T input;
	IGMP_set_multicast_tag_strip_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;
   if (NULL != tag_strip)
	    memcpy(&input.tag_strip, tag_strip, sizeof(MULTICAST_PORT_TAG_OPER_T));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 2, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 IGMP_get_multicast_tag_strip(UINT32 port_id, MULTICAST_PORT_TAG_OPER_T* tag_strip)
{
	IGMP_get_multicast_tag_strip_IN_T input;
	IGMP_get_multicast_tag_strip_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 3, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(tag_strip, &output.tag_strip, sizeof(MULTICAST_PORT_TAG_OPER_T));

	return output.ret;
}


INT32 IGMP_set_multicast_switch(MULTICAST_PROTOCOL_T* multicast_protocol)
{
	IGMP_set_multicast_switch_IN_T input;
	IGMP_set_multicast_switch_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != multicast_protocol)
	    memcpy(&input.multicast_protocol, multicast_protocol, sizeof(MULTICAST_PROTOCOL_T));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 4, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 IGMP_get_multicast_switch(MULTICAST_PROTOCOL_T* multicast_protocol)
{
	IGMP_get_multicast_switch_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 5, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(multicast_protocol, &output.multicast_protocol, sizeof(MULTICAST_PROTOCOL_T));

	return output.ret;
}


INT32 IGMP_set_multicast_group_num(UINT32 port_id, UINT16 group_num)
{
	IGMP_set_multicast_group_num_IN_T input;
	IGMP_set_multicast_group_num_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;
	input.group_num = group_num;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 6, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 IGMP_get_multicast_group_num(UINT32 port_id, UINT16* group_num)
{
	IGMP_get_multicast_group_num_IN_T input;
	IGMP_get_multicast_group_num_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 7, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(group_num, &output.group_num, sizeof(UINT16));

	return output.ret;
}


INT32 IGMP_set_multicast_control(MULTICAST_CONTROL_T* multicast_control)
{
	IGMP_set_multicast_control_IN_T input;
	IGMP_set_multicast_control_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != multicast_control)
	    memcpy(&input.multicast_control, multicast_control, sizeof(MULTICAST_CONTROL_T));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 8, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 IGMP_get_multicast_control(MULTICAST_CONTROL_T* multicast_control)
{
	IGMP_get_multicast_control_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 9, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(multicast_control, &output.multicast_control, sizeof(MULTICAST_CONTROL_T));

	return output.ret;
}


INT32 IGMP_get_fast_leave_ability(MULTICAST_FAST_LEAVE_ABILITY_T* fast_leave_ability)
{
	IGMP_get_fast_leave_ability_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 10, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(fast_leave_ability, &output.fast_leave_ability, sizeof(MULTICAST_FAST_LEAVE_ABILITY_T));

	return output.ret;
}


INT32 IGMP_set_fast_leave_admin_control(IGMP_FAST_LEAVE_ADMIN_STATE_T* fast_leave_admin_state)
{
	IGMP_set_fast_leave_admin_control_IN_T input;
	IGMP_set_fast_leave_admin_control_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != fast_leave_admin_state)
	    memcpy(&input.fast_leave_admin_state, fast_leave_admin_state, sizeof(IGMP_FAST_LEAVE_ADMIN_STATE_T));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 11, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 IGMP_get_fast_leave_admin_state(IGMP_FAST_LEAVE_ADMIN_STATE_T* fast_leave_admin_state)
{
	IGMP_get_fast_leave_admin_state_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 12, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(fast_leave_admin_state, &output.fast_leave_admin_state, sizeof(IGMP_FAST_LEAVE_ADMIN_STATE_T));

	return output.ret;
}


INT32 IGMP_set_acl_rule(mrvl_eth_mc_port_ctrl_t* mc_acl)
{
	IGMP_set_acl_rule_IN_T input;
	IGMP_set_acl_rule_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != mc_acl)
	    memcpy(&input.mc_acl, mc_acl, sizeof(mrvl_eth_mc_port_ctrl_t));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 13, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 IGMP_set_serv_rule(mrvl_eth_mc_port_serv_t* mc_serv)
{
	IGMP_set_serv_rule_IN_T input;
	IGMP_set_serv_rule_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != mc_serv)
	    memcpy(&input.mc_serv, mc_serv, sizeof(mrvl_eth_mc_port_serv_t));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 14, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 IGMP_set_preview_rule(mrvl_eth_mc_port_preview_t* mc_preview)
{
	IGMP_set_preview_rule_IN_T input;
	IGMP_set_preview_rule_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != mc_preview)
	    memcpy(&input.mc_preview, mc_preview, sizeof(mrvl_eth_mc_port_preview_t));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("igmp", 15, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


