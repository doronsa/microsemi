/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include "globals.h"

#include "igmp_api.h"

#include "igmp_mipc_defs.h"


void igmp_mipc_server_call(char* source_module, int api_id, char* input)
{
	switch (api_id)
	{
		case 0:
		{
			IGMP_set_multicast_vlan_IN_T* in = (IGMP_set_multicast_vlan_IN_T*)input;
			IGMP_set_multicast_vlan_OUT_T out;

			out.ret = IGMP_set_multicast_vlan(in->port_id, &in->multicast_vlan);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 0, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 1:
		{
			IGMP_eth_get_multicast_vlan_IN_T* in = (IGMP_eth_get_multicast_vlan_IN_T*)input;
			IGMP_eth_get_multicast_vlan_OUT_T out;

			out.ret = IGMP_eth_get_multicast_vlan(in->port_id, &out.multicast_vlan);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 1, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 2:
		{
			IGMP_set_multicast_tag_strip_IN_T* in = (IGMP_set_multicast_tag_strip_IN_T*)input;
			IGMP_set_multicast_tag_strip_OUT_T out;

			out.ret = IGMP_set_multicast_tag_strip(in->port_id, &in->tag_strip);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 2, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 3:
		{
			IGMP_get_multicast_tag_strip_IN_T* in = (IGMP_get_multicast_tag_strip_IN_T*)input;
			IGMP_get_multicast_tag_strip_OUT_T out;

			out.ret = IGMP_get_multicast_tag_strip(in->port_id, &out.tag_strip);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 3, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 4:
		{
			IGMP_set_multicast_switch_IN_T* in = (IGMP_set_multicast_switch_IN_T*)input;
			IGMP_set_multicast_switch_OUT_T out;

			out.ret = IGMP_set_multicast_switch(&in->multicast_protocol);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 4, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 5:
		{
			IGMP_get_multicast_switch_OUT_T out;

			out.ret = IGMP_get_multicast_switch(&out.multicast_protocol);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 5, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 6:
		{
			IGMP_set_multicast_group_num_IN_T* in = (IGMP_set_multicast_group_num_IN_T*)input;
			IGMP_set_multicast_group_num_OUT_T out;

			out.ret = IGMP_set_multicast_group_num(in->port_id, in->group_num);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 6, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 7:
		{
			IGMP_get_multicast_group_num_IN_T* in = (IGMP_get_multicast_group_num_IN_T*)input;
			IGMP_get_multicast_group_num_OUT_T out;

			out.ret = IGMP_get_multicast_group_num(in->port_id, &out.group_num);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 7, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 8:
		{
			IGMP_set_multicast_control_IN_T* in = (IGMP_set_multicast_control_IN_T*)input;
			IGMP_set_multicast_control_OUT_T out;

			out.ret = IGMP_set_multicast_control(&in->multicast_control);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 8, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 9:
		{
			IGMP_get_multicast_control_OUT_T out;

			out.ret = IGMP_get_multicast_control(&out.multicast_control);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 9, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 10:
		{
			IGMP_get_fast_leave_ability_OUT_T out;

			out.ret = IGMP_get_fast_leave_ability(&out.fast_leave_ability);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 10, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 11:
		{
			IGMP_set_fast_leave_admin_control_IN_T* in = (IGMP_set_fast_leave_admin_control_IN_T*)input;
			IGMP_set_fast_leave_admin_control_OUT_T out;

			out.ret = IGMP_set_fast_leave_admin_control(&in->fast_leave_admin_state);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 11, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 12:
		{
			IGMP_get_fast_leave_admin_state_OUT_T out;

			out.ret = IGMP_get_fast_leave_admin_state(&out.fast_leave_admin_state);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 12, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 13:
		{
			IGMP_set_acl_rule_IN_T* in = (IGMP_set_acl_rule_IN_T*)input;
			IGMP_set_acl_rule_OUT_T out;

			out.ret = IGMP_set_acl_rule(&in->mc_acl);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 13, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 14:
		{
			IGMP_set_serv_rule_IN_T* in = (IGMP_set_serv_rule_IN_T*)input;
			IGMP_set_serv_rule_OUT_T out;

			out.ret = IGMP_set_serv_rule(&in->mc_serv);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 14, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 15:
		{
			IGMP_set_preview_rule_IN_T* in = (IGMP_set_preview_rule_IN_T*)input;
			IGMP_set_preview_rule_OUT_T out;

			out.ret = IGMP_set_preview_rule(&in->mc_preview);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 15, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		default:
			break;
	}
}


void igmp_mipc_server_handler(char* inMsg, unsigned int size)
{
	char source_module[MIPC_NAME_MAX_LEN];
	unsigned int api_id;
	
	if (MIPC_ERROR != mipc_receive_msg(source_module, &api_id, inMsg, size))
		{
			// Call APIs
		igmp_mipc_server_call(source_module, api_id, inMsg + sizeof(MIPC_MSG_HDR_T));
	}
}


