/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _igmp_h_
#define _igmp_h_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	UINT32 port_id;
	MULTICAST_VLAN_T multicast_vlan;
} IGMP_set_multicast_vlan_IN_T;


typedef struct {
	INT32 ret;
} IGMP_set_multicast_vlan_OUT_T;


typedef struct {
	UINT32 port_id;
} IGMP_eth_get_multicast_vlan_IN_T;


typedef struct {
	INT32 ret;
	MULTICAST_VLAN_T multicast_vlan;
} IGMP_eth_get_multicast_vlan_OUT_T;


typedef struct {
	UINT32 port_id;
	MULTICAST_PORT_TAG_OPER_T tag_strip;
} IGMP_set_multicast_tag_strip_IN_T;


typedef struct {
	INT32 ret;
} IGMP_set_multicast_tag_strip_OUT_T;


typedef struct {
	UINT32 port_id;
} IGMP_get_multicast_tag_strip_IN_T;


typedef struct {
	INT32 ret;
	MULTICAST_PORT_TAG_OPER_T tag_strip;
} IGMP_get_multicast_tag_strip_OUT_T;


typedef struct {
	MULTICAST_PROTOCOL_T multicast_protocol;
} IGMP_set_multicast_switch_IN_T;


typedef struct {
	INT32 ret;
} IGMP_set_multicast_switch_OUT_T;


typedef struct {
	INT32 ret;
	MULTICAST_PROTOCOL_T multicast_protocol;
} IGMP_get_multicast_switch_OUT_T;


typedef struct {
	UINT32 port_id;
	UINT16 group_num;
} IGMP_set_multicast_group_num_IN_T;


typedef struct {
	INT32 ret;
} IGMP_set_multicast_group_num_OUT_T;


typedef struct {
	UINT32 port_id;
} IGMP_get_multicast_group_num_IN_T;


typedef struct {
	INT32 ret;
	UINT16 group_num;
} IGMP_get_multicast_group_num_OUT_T;


typedef struct {
	MULTICAST_CONTROL_T multicast_control;
} IGMP_set_multicast_control_IN_T;


typedef struct {
	INT32 ret;
} IGMP_set_multicast_control_OUT_T;


typedef struct {
	INT32 ret;
	MULTICAST_CONTROL_T multicast_control;
} IGMP_get_multicast_control_OUT_T;


typedef struct {
	INT32 ret;
	MULTICAST_FAST_LEAVE_ABILITY_T fast_leave_ability;
} IGMP_get_fast_leave_ability_OUT_T;


typedef struct {
	IGMP_FAST_LEAVE_ADMIN_STATE_T fast_leave_admin_state;
} IGMP_set_fast_leave_admin_control_IN_T;


typedef struct {
	INT32 ret;
} IGMP_set_fast_leave_admin_control_OUT_T;


typedef struct {
	INT32 ret;
	IGMP_FAST_LEAVE_ADMIN_STATE_T fast_leave_admin_state;
} IGMP_get_fast_leave_admin_state_OUT_T;


typedef struct {
	mrvl_eth_mc_port_ctrl_t mc_acl;
} IGMP_set_acl_rule_IN_T;


typedef struct {
	INT32 ret;
} IGMP_set_acl_rule_OUT_T;


typedef struct {
	mrvl_eth_mc_port_serv_t mc_serv;
} IGMP_set_serv_rule_IN_T;


typedef struct {
	INT32 ret;
} IGMP_set_serv_rule_OUT_T;


typedef struct {
	mrvl_eth_mc_port_preview_t mc_preview;
} IGMP_set_preview_rule_IN_T;


typedef struct {
	INT32 ret;
} IGMP_set_preview_rule_OUT_T;


#ifdef __cplusplus
}
#endif
#endif
