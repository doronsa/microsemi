/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include "globals.h"

#include "rstp_mipc_defs.h"


INT32 RSTP_get_enable(bool* enable)
{
	RSTP_get_enable_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("rstp", 0, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(enable, &output.enable, sizeof(bool));

	return output.ret;
}


INT32 RSTP_set_enable(bool enable)
{
	RSTP_set_enable_IN_T input;
	RSTP_set_enable_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.enable = enable;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("rstp", 1, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 RSTP_get_bridge_priority(UINT32* priority)
{
	RSTP_get_bridge_priority_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("rstp", 2, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(priority, &output.priority, sizeof(UINT32));

	return output.ret;
}


INT32 RSTP_set_bridge_priority(UINT32 bridge_priority)
{
	RSTP_set_bridge_priority_IN_T input;
	RSTP_set_bridge_priority_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.bridge_priority = bridge_priority;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("rstp", 3, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 RSTP_get_time(UINT32* fwd_delay, UINT32* hello, UINT32* max_age)
{
	RSTP_get_time_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("rstp", 4, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(fwd_delay, &output.fwd_delay, sizeof(UINT32));
	memcpy(hello, &output.hello, sizeof(UINT32));
	memcpy(max_age, &output.max_age, sizeof(UINT32));

	return output.ret;
}


INT32 RSTP_set_time(UINT32 fwd_delay, UINT32 hello, UINT32 max_age)
{
	RSTP_set_time_IN_T input;
	RSTP_set_time_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.fwd_delay = fwd_delay;
	input.hello = hello;
	input.max_age = max_age;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("rstp", 5, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 RSTP_get_priority(UINT32 port_id, UINT32* priority)
{
	RSTP_get_priority_IN_T input;
	RSTP_get_priority_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("rstp", 6, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(priority, &output.priority, sizeof(UINT32));

	return output.ret;
}


INT32 RSTP_set_priority(UINT32 port_id, UINT32 priority)
{
	RSTP_set_priority_IN_T input;
	RSTP_set_priority_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;
	input.priority = priority;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("rstp", 7, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 RSTP_port_get_enable(UINT32 port_id, bool* enable)
{
	RSTP_port_get_enable_IN_T input;
	RSTP_port_get_enable_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("rstp", 8, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(enable, &output.enable, sizeof(bool));

	return output.ret;
}


INT32 RSTP_port_set_enable(UINT32 port_id, bool enable)
{
	RSTP_port_set_enable_IN_T input;
	RSTP_port_set_enable_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;
	input.enable = enable;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("rstp", 9, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


