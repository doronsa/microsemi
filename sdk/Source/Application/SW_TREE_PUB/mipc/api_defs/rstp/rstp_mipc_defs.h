/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _rstp_h_
#define _rstp_h_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	INT32 ret;
	bool enable;
} RSTP_get_enable_OUT_T;


typedef struct {
	bool enable;
} RSTP_set_enable_IN_T;


typedef struct {
	INT32 ret;
} RSTP_set_enable_OUT_T;


typedef struct {
	INT32 ret;
	UINT32 priority;
} RSTP_get_bridge_priority_OUT_T;


typedef struct {
	UINT32 bridge_priority;
} RSTP_set_bridge_priority_IN_T;


typedef struct {
	INT32 ret;
} RSTP_set_bridge_priority_OUT_T;


typedef struct {
	INT32 ret;
	UINT32 fwd_delay;
	UINT32 hello;
	UINT32 max_age;
} RSTP_get_time_OUT_T;


typedef struct {
	UINT32 fwd_delay;
	UINT32 hello;
	UINT32 max_age;
} RSTP_set_time_IN_T;


typedef struct {
	INT32 ret;
} RSTP_set_time_OUT_T;


typedef struct {
	UINT32 port_id;
} RSTP_get_priority_IN_T;


typedef struct {
	INT32 ret;
	UINT32 priority;
} RSTP_get_priority_OUT_T;


typedef struct {
	UINT32 port_id;
	UINT32 priority;
} RSTP_set_priority_IN_T;


typedef struct {
	INT32 ret;
} RSTP_set_priority_OUT_T;


typedef struct {
	UINT32 port_id;
} RSTP_port_get_enable_IN_T;


typedef struct {
	INT32 ret;
	bool enable;
} RSTP_port_get_enable_OUT_T;


typedef struct {
	UINT32 port_id;
	bool enable;
} RSTP_port_set_enable_IN_T;


typedef struct {
	INT32 ret;
} RSTP_port_set_enable_OUT_T;


#ifdef __cplusplus
}
#endif
#endif
