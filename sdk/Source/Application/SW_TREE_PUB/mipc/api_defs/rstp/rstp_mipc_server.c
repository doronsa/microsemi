/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include "globals.h"

#include "rstp_mipc_defs.h"


void rstp_mipc_server_call(char* source_module, int api_id, char* input)
{
	switch (api_id)
	{
		case 0:
		{
			RSTP_get_enable_OUT_T out;

			out.ret = RSTP_get_enable(&out.enable);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 0, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 1:
		{
			RSTP_set_enable_IN_T* in = (RSTP_set_enable_IN_T*)input;
			RSTP_set_enable_OUT_T out;

			out.ret = RSTP_set_enable(in->enable);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 1, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 2:
		{
			RSTP_get_bridge_priority_OUT_T out;

			out.ret = RSTP_get_bridge_priority(&out.priority);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 2, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 3:
		{
			RSTP_set_bridge_priority_IN_T* in = (RSTP_set_bridge_priority_IN_T*)input;
			RSTP_set_bridge_priority_OUT_T out;

			out.ret = RSTP_set_bridge_priority(in->bridge_priority);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 3, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 4:
		{
			RSTP_get_time_OUT_T out;

			out.ret = RSTP_get_time(&out.fwd_delay, &out.hello, &out.max_age);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 4, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 5:
		{
			RSTP_set_time_IN_T* in = (RSTP_set_time_IN_T*)input;
			RSTP_set_time_OUT_T out;

			out.ret = RSTP_set_time(in->fwd_delay, in->hello, in->max_age);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 5, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 6:
		{
			RSTP_get_priority_IN_T* in = (RSTP_get_priority_IN_T*)input;
			RSTP_get_priority_OUT_T out;

			out.ret = RSTP_get_priority(in->port_id, &out.priority);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 6, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 7:
		{
			RSTP_set_priority_IN_T* in = (RSTP_set_priority_IN_T*)input;
			RSTP_set_priority_OUT_T out;

			out.ret = RSTP_set_priority(in->port_id, in->priority);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 7, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 8:
		{
			RSTP_port_get_enable_IN_T* in = (RSTP_port_get_enable_IN_T*)input;
			RSTP_port_get_enable_OUT_T out;

			out.ret = RSTP_port_get_enable(in->port_id, &out.enable);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 8, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 9:
		{
			RSTP_port_set_enable_IN_T* in = (RSTP_port_set_enable_IN_T*)input;
			RSTP_port_set_enable_OUT_T out;

			out.ret = RSTP_port_set_enable(in->port_id, in->enable);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 9, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		default:
			break;
	}
}


void rstp_mipc_server_handler(char* inMsg, unsigned int size)
{
	char source_module[MIPC_NAME_MAX_LEN];
	unsigned int api_id;
	
	if (MIPC_ERROR != mipc_receive_msg(source_module, &api_id, inMsg, size))
		{
			// Call APIs
		rstp_mipc_server_call(source_module, api_id, inMsg + sizeof(MIPC_MSG_HDR_T));
	}
}


