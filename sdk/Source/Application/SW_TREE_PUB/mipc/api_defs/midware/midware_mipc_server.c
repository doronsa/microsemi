/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "midware_expo.h"

#include "midware_mipc_defs.h"


void midware_mipc_server_call(char* source_module, int api_id, char* input)
{
	switch (api_id)
	{
		case 0:
		{
			midware_insert_entry_IN_T* in = (midware_insert_entry_IN_T*)input;
			midware_insert_entry_OUT_T out;

			out.ret = midware_insert_entry(in->table_id, (void*)&in->entry, in->size);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 0, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 1:
		{
			midware_insert_group_entry_IN_T* in = (midware_insert_group_entry_IN_T*)input;
			midware_insert_group_entry_OUT_T out;

			out.ret = midware_insert_group_entry(in->table_id, in->entry_num, (void*)&in->entry, in->size);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 1, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 2:
		{
			midware_update_entry_IN_T* in = (midware_update_entry_IN_T*)input;
			midware_update_entry_OUT_T out;

			out.ret = midware_update_entry(in->table_id, in->bitmap, (void*)&in->entry, in->size);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 2, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 3:
		{
			midware_remove_entry_IN_T* in = (midware_remove_entry_IN_T*)input;
			midware_remove_entry_OUT_T out;

			out.ret = midware_remove_entry(in->table_id, (void*)&in->entry_context, in->size);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 3, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 4:
		{
			midware_reset_table_IN_T* in = (midware_reset_table_IN_T*)input;
			midware_reset_table_OUT_T out;

			out.ret = midware_reset_table(in->table_id);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 4, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 5:
		{
			midware_reset_table_data_IN_T* in = (midware_reset_table_data_IN_T*)input;
			midware_reset_table_data_OUT_T out;

			out.ret = midware_reset_table_data(in->table_id);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 5, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 6:
		{
			midware_reset_db_OUT_T out;

			out.ret = midware_reset_db();


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 6, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 7:
		{
			midware_save_db_OUT_T out;

			out.ret = midware_save_db();


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 7, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 8:
		{
			midware_get_entry_IN_T* in = (midware_get_entry_IN_T*)input;
			midware_get_entry_OUT_T out;

			out.ret = midware_get_entry(in->table_id, in->bitmap, (void*)&in->entry, in->size);

			memcpy(&out.entry, &in->entry, in->size);

			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 8, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 9:
		{
			midware_get_first_entry_IN_T* in = (midware_get_first_entry_IN_T*)input;
			midware_get_first_entry_OUT_T out;

			out.ret = midware_get_first_entry(in->table_id, in->bitmap, (void*)&in->entry, in->size);

			memcpy(&out.entry, &in->entry, in->size);

			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 9, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 10:
		{
			midware_get_next_entry_IN_T* in = (midware_get_next_entry_IN_T*)input;
			midware_get_next_entry_OUT_T out;

			out.ret = midware_get_next_entry(in->table_id, (void*)&in->entry, in->size);

			memcpy(&out.entry, &in->entry, in->size);

			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 10, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 11:
		{
			midware_update_entry_asynchronous_IN_T* in = (midware_update_entry_asynchronous_IN_T*)input;
			midware_update_entry_asynchronous_OUT_T out;

			out.ret = midware_update_entry_asynchronous(in->table_id, in->bitmap, (void*)&in->entry, in->size);


			break;
		}

		case 12:
		{
			Midware_cli_print_table_info_IN_T* in = (Midware_cli_print_table_info_IN_T*)input;
			Midware_cli_print_table_info_OUT_T out;

			out.ret = Midware_cli_print_table_info((void*)source_module, in->table_id);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 12, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 13:
		{
			Midware_cli_insert_entry_IN_T* in = (Midware_cli_insert_entry_IN_T*)input;
			Midware_cli_insert_entry_OUT_T out;

			out.ret = Midware_cli_insert_entry((void*)source_module, in->table_id, in->arg);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 13, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 14:
		{
			Midware_cli_update_entry_IN_T* in = (Midware_cli_update_entry_IN_T*)input;
			Midware_cli_update_entry_OUT_T out;

			out.ret = Midware_cli_update_entry((void*)source_module, in->table_id, in->arg);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 14, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 15:
		{
			Midware_cli_remove_entry_IN_T* in = (Midware_cli_remove_entry_IN_T*)input;
			Midware_cli_remove_entry_OUT_T out;

			out.ret = Midware_cli_remove_entry((void*)source_module, in->table_id, in->arg);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 15, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 16:
		{
			Midware_cli_reset_table_IN_T* in = (Midware_cli_reset_table_IN_T*)input;
			Midware_cli_reset_table_OUT_T out;

			out.ret = Midware_cli_reset_table((void*)source_module, in->table_id);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 16, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 17:
		{
			Midware_cli_reset_table_data_IN_T* in = (Midware_cli_reset_table_data_IN_T*)input;
			Midware_cli_reset_table_data_OUT_T out;

			out.ret = Midware_cli_reset_table_data((void*)source_module, in->table_id);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 17, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 18:
		{
			Midware_cli_save_db_IN_T* in = (Midware_cli_save_db_IN_T*)input;
			Midware_cli_save_db_OUT_T out;

			out.ret = Midware_cli_save_db((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 18, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 19:
		{
			Midware_cli_get_entry_IN_T* in = (Midware_cli_get_entry_IN_T*)input;
			Midware_cli_get_entry_OUT_T out;

			out.ret = Midware_cli_get_entry((void*)source_module, in->table_id, in->arg);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 19, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 20:
		{
			Midware_cli_show_table_all_entry_IN_T* in = (Midware_cli_show_table_all_entry_IN_T*)input;
			Midware_cli_show_table_all_entry_OUT_T out;

			out.ret = Midware_cli_show_table_all_entry((void*)source_module, in->table_id);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 20, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 21:
		{
			Midware_cli_set_print_level_IN_T* in = (Midware_cli_set_print_level_IN_T*)input;
			Midware_cli_set_print_level_OUT_T out;

			out.ret = Midware_cli_set_print_level((void*)source_module, in->level);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 21, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		default:
			break;
	}
}


void midware_mipc_server_handler(char* inMsg, unsigned int size)
{
	char source_module[MIPC_NAME_MAX_LEN];
	unsigned int api_id;
	
	if (MIPC_ERROR != mipc_receive_msg(source_module, &api_id, inMsg, size))
		{
			// Call APIs
		midware_mipc_server_call(source_module, api_id, inMsg + sizeof(MIPC_MSG_HDR_T));
	}
}


