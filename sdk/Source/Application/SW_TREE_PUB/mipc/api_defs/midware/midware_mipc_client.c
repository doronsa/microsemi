/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "midware_expo.h"

#include "midware_mipc_defs.h"


ONU_STATUS midware_insert_entry(MIDWARE_TABLE_ID_E table_id, VOID* entry, UINT32 size)
{
	midware_insert_entry_IN_T input;
	midware_insert_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.table_id = table_id;
   if (NULL != entry)
	    memcpy(&input.entry, entry, size);
	input.size = size;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("midware", 0, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


ONU_STATUS midware_insert_group_entry(MIDWARE_TABLE_ID_E table_id, UINT32 entry_num, VOID* entry, UINT32 size)
{
	midware_insert_group_entry_IN_T input;
	midware_insert_group_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.table_id = table_id;
	input.entry_num = entry_num;
   if (NULL != entry)
	    memcpy(&input.entry, entry, size);
	input.size = size;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("midware", 1, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


ONU_STATUS midware_update_entry(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, VOID* entry, UINT32 size)
{
	midware_update_entry_IN_T input;
	midware_update_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.table_id = table_id;
	input.bitmap = bitmap;
   if (NULL != entry)
	    memcpy(&input.entry, entry, size);
	input.size = size;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("midware", 2, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


ONU_STATUS midware_remove_entry(MIDWARE_TABLE_ID_E table_id, VOID* entry_context, UINT32 size)
{
	midware_remove_entry_IN_T input;
	midware_remove_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.table_id = table_id;
   if (NULL != entry_context)
	    memcpy(&input.entry_context, entry_context, size);
	input.size = size;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("midware", 3, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


ONU_STATUS midware_reset_table(MIDWARE_TABLE_ID_E table_id)
{
	midware_reset_table_IN_T input;
	midware_reset_table_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.table_id = table_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("midware", 4, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


ONU_STATUS midware_reset_table_data(MIDWARE_TABLE_ID_E table_id)
{
	midware_reset_table_data_IN_T input;
	midware_reset_table_data_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.table_id = table_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("midware", 5, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


ONU_STATUS midware_reset_db(void)
{
	midware_reset_db_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("midware", 6, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


ONU_STATUS midware_save_db(void)
{
	midware_save_db_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("midware", 7, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


ONU_STATUS midware_get_entry(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, VOID* entry, UINT32 size)
{
	midware_get_entry_IN_T input;
	midware_get_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.table_id = table_id;
	input.bitmap = bitmap;
   if (NULL != entry)
	    memcpy(&input.entry, entry, size);
	input.size = size;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("midware", 8, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(entry, &output.entry, size);
	size = output.size;

	return output.ret;
}


ONU_STATUS midware_get_first_entry(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, VOID* entry, UINT32 size)
{
	midware_get_first_entry_IN_T input;
	midware_get_first_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.table_id = table_id;
	input.bitmap = bitmap;
   if (NULL != entry)
	    memcpy(&input.entry, entry, size);
	input.size = size;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("midware", 9, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(entry, &output.entry, size);
	size = output.size;

	return output.ret;
}


ONU_STATUS midware_get_next_entry(MIDWARE_TABLE_ID_E table_id, VOID* entry, UINT32 size)
{
	midware_get_next_entry_IN_T input;
	midware_get_next_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.table_id = table_id;
   if (NULL != entry)
	    memcpy(&input.entry, entry, size);
	input.size = size;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("midware", 10, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(entry, &output.entry, size);
	size = output.size;

	return output.ret;
}


ONU_STATUS midware_update_entry_asynchronous(MIDWARE_TABLE_ID_E table_id, UINT32 bitmap, VOID* entry, UINT32 size)
{
	midware_update_entry_asynchronous_IN_T input;
	midware_update_entry_asynchronous_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.table_id = table_id;
	input.bitmap = bitmap;
   if (NULL != entry)
	    memcpy(&input.entry, entry, size);
	input.size = size;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_async_msg("midware", 11, &input, sizeof(input)))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Midware_cli_print_table_info(char* name, MIDWARE_TABLE_ID_E table_id)
{
	Midware_cli_print_table_info_IN_T input;
	Midware_cli_print_table_info_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.table_id = table_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("midware", 12, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Midware_cli_insert_entry(char* name, MIDWARE_TABLE_ID_E table_id, char* arg)
{
	Midware_cli_insert_entry_IN_T input;
	Midware_cli_insert_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.table_id = table_id;
   if (NULL != arg)
	    strcpy(input.arg, arg);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("midware", 13, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Midware_cli_update_entry(char* name, MIDWARE_TABLE_ID_E table_id, char* arg)
{
	Midware_cli_update_entry_IN_T input;
	Midware_cli_update_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.table_id = table_id;
   if (NULL != arg)
	    strcpy(input.arg, arg);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("midware", 14, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Midware_cli_remove_entry(char* name, MIDWARE_TABLE_ID_E table_id, char* arg)
{
	Midware_cli_remove_entry_IN_T input;
	Midware_cli_remove_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.table_id = table_id;
   if (NULL != arg)
	    strcpy(input.arg, arg);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("midware", 15, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Midware_cli_reset_table(char* name, MIDWARE_TABLE_ID_E table_id)
{
	Midware_cli_reset_table_IN_T input;
	Midware_cli_reset_table_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.table_id = table_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("midware", 16, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Midware_cli_reset_table_data(char* name, MIDWARE_TABLE_ID_E table_id)
{
	Midware_cli_reset_table_data_IN_T input;
	Midware_cli_reset_table_data_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.table_id = table_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("midware", 17, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Midware_cli_save_db(char* name)
{
	Midware_cli_save_db_IN_T input;
	Midware_cli_save_db_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("midware", 18, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Midware_cli_get_entry(char* name, MIDWARE_TABLE_ID_E table_id, char* arg)
{
	Midware_cli_get_entry_IN_T input;
	Midware_cli_get_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.table_id = table_id;
   if (NULL != arg)
	    strcpy(input.arg, arg);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("midware", 19, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Midware_cli_show_table_all_entry(char* name, MIDWARE_TABLE_ID_E table_id)
{
	Midware_cli_show_table_all_entry_IN_T input;
	Midware_cli_show_table_all_entry_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.table_id = table_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("midware", 20, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool Midware_cli_set_print_level(char* name, UINT32 level)
{
	Midware_cli_set_print_level_IN_T input;
	Midware_cli_set_print_level_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);
	input.level = level;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("midware", 21, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


