/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _midware_h_
#define _midware_h_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	MIDWARE_TABLE_ID_E table_id;
	char entry[2048];
	UINT32 size;
} midware_insert_entry_IN_T;


typedef struct {
	ONU_STATUS ret;
} midware_insert_entry_OUT_T;


typedef struct {
	MIDWARE_TABLE_ID_E table_id;
	UINT32 entry_num;
	char entry[2048];
	UINT32 size;
} midware_insert_group_entry_IN_T;


typedef struct {
	ONU_STATUS ret;
} midware_insert_group_entry_OUT_T;


typedef struct {
	MIDWARE_TABLE_ID_E table_id;
	UINT32 bitmap;
	char entry[2048];
	UINT32 size;
} midware_update_entry_IN_T;


typedef struct {
	ONU_STATUS ret;
} midware_update_entry_OUT_T;


typedef struct {
	MIDWARE_TABLE_ID_E table_id;
	char entry_context[2048];
	UINT32 size;
} midware_remove_entry_IN_T;


typedef struct {
	ONU_STATUS ret;
} midware_remove_entry_OUT_T;


typedef struct {
	MIDWARE_TABLE_ID_E table_id;
} midware_reset_table_IN_T;


typedef struct {
	ONU_STATUS ret;
} midware_reset_table_OUT_T;


typedef struct {
	MIDWARE_TABLE_ID_E table_id;
} midware_reset_table_data_IN_T;


typedef struct {
	ONU_STATUS ret;
} midware_reset_table_data_OUT_T;


typedef struct {
	ONU_STATUS ret;
} midware_reset_db_OUT_T;


typedef struct {
	ONU_STATUS ret;
} midware_save_db_OUT_T;


typedef struct {
	MIDWARE_TABLE_ID_E table_id;
	UINT32 bitmap;
	char entry[2048];
	UINT32 size;
} midware_get_entry_IN_T;


typedef struct {
	ONU_STATUS ret;
	char entry[2048];
	UINT32 size;
} midware_get_entry_OUT_T;


typedef struct {
	MIDWARE_TABLE_ID_E table_id;
	UINT32 bitmap;
	char entry[2048];
	UINT32 size;
} midware_get_first_entry_IN_T;


typedef struct {
	ONU_STATUS ret;
	char entry[2048];
	UINT32 size;
} midware_get_first_entry_OUT_T;


typedef struct {
	MIDWARE_TABLE_ID_E table_id;
	char entry[2048];
	UINT32 size;
} midware_get_next_entry_IN_T;


typedef struct {
	ONU_STATUS ret;
	char entry[2048];
	UINT32 size;
} midware_get_next_entry_OUT_T;


typedef struct {
	MIDWARE_TABLE_ID_E table_id;
	UINT32 bitmap;
	char entry[2048];
	UINT32 size;
} midware_update_entry_asynchronous_IN_T;


typedef struct {
	ONU_STATUS ret;
} midware_update_entry_asynchronous_OUT_T;


typedef struct {
	char name[256];
	MIDWARE_TABLE_ID_E table_id;
} Midware_cli_print_table_info_IN_T;


typedef struct {
	bool ret;
} Midware_cli_print_table_info_OUT_T;


typedef struct {
	char name[256];
	MIDWARE_TABLE_ID_E table_id;
	char arg[256];
} Midware_cli_insert_entry_IN_T;


typedef struct {
	bool ret;
} Midware_cli_insert_entry_OUT_T;


typedef struct {
	char name[256];
	MIDWARE_TABLE_ID_E table_id;
	char arg[256];
} Midware_cli_update_entry_IN_T;


typedef struct {
	bool ret;
} Midware_cli_update_entry_OUT_T;


typedef struct {
	char name[256];
	MIDWARE_TABLE_ID_E table_id;
	char arg[256];
} Midware_cli_remove_entry_IN_T;


typedef struct {
	bool ret;
} Midware_cli_remove_entry_OUT_T;


typedef struct {
	char name[256];
	MIDWARE_TABLE_ID_E table_id;
} Midware_cli_reset_table_IN_T;


typedef struct {
	bool ret;
} Midware_cli_reset_table_OUT_T;


typedef struct {
	char name[256];
	MIDWARE_TABLE_ID_E table_id;
} Midware_cli_reset_table_data_IN_T;


typedef struct {
	bool ret;
} Midware_cli_reset_table_data_OUT_T;


typedef struct {
	char name[256];
} Midware_cli_save_db_IN_T;


typedef struct {
	bool ret;
} Midware_cli_save_db_OUT_T;


typedef struct {
	char name[256];
	MIDWARE_TABLE_ID_E table_id;
	char arg[256];
} Midware_cli_get_entry_IN_T;


typedef struct {
	bool ret;
} Midware_cli_get_entry_OUT_T;


typedef struct {
	char name[256];
	MIDWARE_TABLE_ID_E table_id;
} Midware_cli_show_table_all_entry_IN_T;


typedef struct {
	bool ret;
} Midware_cli_show_table_all_entry_OUT_T;


typedef struct {
	char name[256];
	UINT32 level;
} Midware_cli_set_print_level_IN_T;


typedef struct {
	bool ret;
} Midware_cli_set_print_level_OUT_T;


#ifdef __cplusplus
}
#endif
#endif
