/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _voip_h_
#define _voip_h_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	int sipPort;
} VOIP_updateSipTcpUdpPort_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_updateSipTcpUdpPort_F_OUT_T;


typedef struct {
	UINT32 ip_address;
	UINT32 ip_mask;
	UINT32 gate_way;
} VOIP_updateHostIpAddr_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_updateHostIpAddr_F_OUT_T;


typedef struct {
	int vid;
	int pbits;
} VOIP_updateHostVidPbits_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_updateHostVidPbits_F_OUT_T;


typedef struct {
	int potsPort;
	char proxy[256];
} VOIP_updateSipServerAddr_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_updateSipServerAddr_F_OUT_T;


typedef struct {
	int potsPort;
	char userName[256];
	char password[256];
	char aor[256];
} VOIP_updateSipAccountData_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_updateSipAccountData_F_OUT_T;


typedef struct {
	UINT32 port_id;
	VOIP_ST_PORT_ACTIVE_STATE info;
} VOIP_setPortActiveState_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_setPortActiveState_F_OUT_T;


typedef struct {
	UINT32 port_id;
} VOIP_getPortActiveState_F_IN_T;


typedef struct {
	INT32 ret;
	VOIP_ST_PORT_ACTIVE_STATE info;
} VOIP_getPortActiveState_F_OUT_T;


typedef struct {
	INT32 ret;
	VOIP_ST_IADINFO info;
} VOIP_getIadInfo_F_OUT_T;


typedef struct {
	VOIP_GLOBAL_PARAM_T info;
} VOIP_setGlobalParam_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_setGlobalParam_F_OUT_T;


typedef struct {
	INT32 ret;
	VOIP_GLOBAL_PARAM_T info;
} VOIP_getGlobalParam_F_OUT_T;


typedef struct {
	VOIP_ST_H248_PARAMETER_CONFIG info;
} VOIP_setH248ParamConfig_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_setH248ParamConfig_F_OUT_T;


typedef struct {
	INT32 ret;
	VOIP_ST_H248_PARAMETER_CONFIG info;
} VOIP_getH248ParamConfig_F_OUT_T;


typedef struct {
	UINT32 port_id;
	VOIP_ST_H248_USER_TIDINFO info;
} VOIP_setH248UserTIDInfo_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_setH248UserTIDInfo_F_OUT_T;


typedef struct {
	UINT32 port_id;
} VOIP_getH248UserTIDInfo_F_IN_T;


typedef struct {
	INT32 ret;
	VOIP_ST_H248_USER_TIDINFO info;
} VOIP_getH248UserTIDInfo_F_OUT_T;


typedef struct {
	VOIP_ST_H248_RTP_TID_CONFIG info;
} VOIP_setH248RtpTIDConfig_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_setH248RtpTIDConfig_F_OUT_T;


typedef struct {
	INT32 ret;
	VOIP_ST_H248_RTP_TID_CONFIG info;
} VOIP_getH248RtpTIDConfig_F_OUT_T;


typedef struct {
	INT32 ret;
	VOIP_ST_H248_RTP_TID_INFO info;
} VOIP_getH248RtpTIDInfo_F_OUT_T;


typedef struct {
	VOIP_ST_SIP_PARAMETER_CONFIG info;
} VOIP_setSipParamConfig_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_setSipParamConfig_F_OUT_T;


typedef struct {
	INT32 ret;
	VOIP_ST_SIP_PARAMETER_CONFIG info;
} VOIP_getSipParamConfig_F_OUT_T;


typedef struct {
	UINT32 port_id;
	VOIP_ST_SIP_USER_PARAMETER_CONFIG info;
} VOIP_setSipUserParamConfig_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_setSipUserParamConfig_F_OUT_T;


typedef struct {
	UINT32 port_id;
} VOIP_getSipUserParamConfig_F_IN_T;


typedef struct {
	INT32 ret;
	VOIP_ST_SIP_USER_PARAMETER_CONFIG info;
} VOIP_getSipUserParamConfig_F_OUT_T;


typedef struct {
	VOIP_ST_FAX_MODEM_CONFIG info;
} VOIP_setFaxModemConfig_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_setFaxModemConfig_F_OUT_T;


typedef struct {
	INT32 ret;
	VOIP_ST_FAX_MODEM_CONFIG info;
} VOIP_getFaxModemConfig_F_OUT_T;


typedef struct {
	INT32 ret;
	VOIP_ST_H248_IAD_OPERATION_STATUS info;
} VOIP_getH248IadOperStatus_F_OUT_T;


typedef struct {
	UINT32 port_id;
} VOIP_getPotsStatus_F_IN_T;


typedef struct {
	INT32 ret;
	VOIP_ST_POTS_STATUS info;
} VOIP_getPotsStatus_F_OUT_T;


typedef struct {
	VOIP_ST_IAD_OPERATION info;
} VOIP_setIadOperation_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_setIadOperation_F_OUT_T;


typedef struct {
	VOIP_ST_SIP_DIGITAL_MAP info;
} VOIP_setSipDigitmap_F_IN_T;


typedef struct {
	INT32 ret;
} VOIP_setSipDigitmap_F_OUT_T;


#ifdef __cplusplus
}
#endif
#endif
