/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include "voip_api.h"

#include "voip_mipc_defs.h"


void voip_mipc_server_call(char* source_module, int api_id, char* input)
{
	switch (api_id)
	{
		case 0:
		{
			VOIP_updateSipTcpUdpPort_F_IN_T* in = (VOIP_updateSipTcpUdpPort_F_IN_T*)input;
			VOIP_updateSipTcpUdpPort_F_OUT_T out;

			out.ret = VOIP_updateSipTcpUdpPort_F(in->sipPort);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 0, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 1:
		{
			VOIP_updateHostIpAddr_F_IN_T* in = (VOIP_updateHostIpAddr_F_IN_T*)input;
			VOIP_updateHostIpAddr_F_OUT_T out;

			out.ret = VOIP_updateHostIpAddr_F(in->ip_address, in->ip_mask, in->gate_way);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 1, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 2:
		{
			VOIP_updateHostVidPbits_F_IN_T* in = (VOIP_updateHostVidPbits_F_IN_T*)input;
			VOIP_updateHostVidPbits_F_OUT_T out;

			out.ret = VOIP_updateHostVidPbits_F(in->vid, in->pbits);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 2, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 3:
		{
			VOIP_updateSipServerAddr_F_IN_T* in = (VOIP_updateSipServerAddr_F_IN_T*)input;
			VOIP_updateSipServerAddr_F_OUT_T out;

			out.ret = VOIP_updateSipServerAddr_F(in->potsPort, in->proxy);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 3, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 4:
		{
			VOIP_updateSipAccountData_F_IN_T* in = (VOIP_updateSipAccountData_F_IN_T*)input;
			VOIP_updateSipAccountData_F_OUT_T out;

			out.ret = VOIP_updateSipAccountData_F(in->potsPort, in->userName, in->password, in->aor);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 4, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 5:
		{
			VOIP_setPortActiveState_F_IN_T* in = (VOIP_setPortActiveState_F_IN_T*)input;
			VOIP_setPortActiveState_F_OUT_T out;

			out.ret = VOIP_setPortActiveState_F(in->port_id, &in->info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 5, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 6:
		{
			VOIP_getPortActiveState_F_IN_T* in = (VOIP_getPortActiveState_F_IN_T*)input;
			VOIP_getPortActiveState_F_OUT_T out;

			out.ret = VOIP_getPortActiveState_F(in->port_id, &out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 6, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 7:
		{
			VOIP_getIadInfo_F_OUT_T out;

			out.ret = VOIP_getIadInfo_F(&out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 7, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 8:
		{
			VOIP_setGlobalParam_F_IN_T* in = (VOIP_setGlobalParam_F_IN_T*)input;
			VOIP_setGlobalParam_F_OUT_T out;

			out.ret = VOIP_setGlobalParam_F(&in->info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 8, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 9:
		{
			VOIP_getGlobalParam_F_OUT_T out;

			out.ret = VOIP_getGlobalParam_F(&out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 9, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 10:
		{
			VOIP_setH248ParamConfig_F_IN_T* in = (VOIP_setH248ParamConfig_F_IN_T*)input;
			VOIP_setH248ParamConfig_F_OUT_T out;

			out.ret = VOIP_setH248ParamConfig_F(&in->info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 10, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 11:
		{
			VOIP_getH248ParamConfig_F_OUT_T out;

			out.ret = VOIP_getH248ParamConfig_F(&out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 11, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 12:
		{
			VOIP_setH248UserTIDInfo_F_IN_T* in = (VOIP_setH248UserTIDInfo_F_IN_T*)input;
			VOIP_setH248UserTIDInfo_F_OUT_T out;

			out.ret = VOIP_setH248UserTIDInfo_F(in->port_id, &in->info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 12, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 13:
		{
			VOIP_getH248UserTIDInfo_F_IN_T* in = (VOIP_getH248UserTIDInfo_F_IN_T*)input;
			VOIP_getH248UserTIDInfo_F_OUT_T out;

			out.ret = VOIP_getH248UserTIDInfo_F(in->port_id, &out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 13, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 14:
		{
			VOIP_setH248RtpTIDConfig_F_IN_T* in = (VOIP_setH248RtpTIDConfig_F_IN_T*)input;
			VOIP_setH248RtpTIDConfig_F_OUT_T out;

			out.ret = VOIP_setH248RtpTIDConfig_F(&in->info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 14, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 15:
		{
			VOIP_getH248RtpTIDConfig_F_OUT_T out;

			out.ret = VOIP_getH248RtpTIDConfig_F(&out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 15, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 16:
		{
			VOIP_getH248RtpTIDInfo_F_OUT_T out;

			out.ret = VOIP_getH248RtpTIDInfo_F(&out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 16, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 17:
		{
			VOIP_setSipParamConfig_F_IN_T* in = (VOIP_setSipParamConfig_F_IN_T*)input;
			VOIP_setSipParamConfig_F_OUT_T out;

			out.ret = VOIP_setSipParamConfig_F(&in->info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 17, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 18:
		{
			VOIP_getSipParamConfig_F_OUT_T out;

			out.ret = VOIP_getSipParamConfig_F(&out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 18, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 19:
		{
			VOIP_setSipUserParamConfig_F_IN_T* in = (VOIP_setSipUserParamConfig_F_IN_T*)input;
			VOIP_setSipUserParamConfig_F_OUT_T out;

			out.ret = VOIP_setSipUserParamConfig_F(in->port_id, &in->info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 19, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 20:
		{
			VOIP_getSipUserParamConfig_F_IN_T* in = (VOIP_getSipUserParamConfig_F_IN_T*)input;
			VOIP_getSipUserParamConfig_F_OUT_T out;

			out.ret = VOIP_getSipUserParamConfig_F(in->port_id, &out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 20, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 21:
		{
			VOIP_setFaxModemConfig_F_IN_T* in = (VOIP_setFaxModemConfig_F_IN_T*)input;
			VOIP_setFaxModemConfig_F_OUT_T out;

			out.ret = VOIP_setFaxModemConfig_F(&in->info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 21, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 22:
		{
			VOIP_getFaxModemConfig_F_OUT_T out;

			out.ret = VOIP_getFaxModemConfig_F(&out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 22, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 23:
		{
			VOIP_getH248IadOperStatus_F_OUT_T out;

			out.ret = VOIP_getH248IadOperStatus_F(&out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 23, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 24:
		{
			VOIP_getPotsStatus_F_IN_T* in = (VOIP_getPotsStatus_F_IN_T*)input;
			VOIP_getPotsStatus_F_OUT_T out;

			out.ret = VOIP_getPotsStatus_F(in->port_id, &out.info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 24, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 25:
		{
			VOIP_setIadOperation_F_IN_T* in = (VOIP_setIadOperation_F_IN_T*)input;
			VOIP_setIadOperation_F_OUT_T out;

			out.ret = VOIP_setIadOperation_F(&in->info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 25, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 26:
		{
			VOIP_setSipDigitmap_F_IN_T* in = (VOIP_setSipDigitmap_F_IN_T*)input;
			VOIP_setSipDigitmap_F_OUT_T out;

			out.ret = VOIP_setSipDigitmap_F(&in->info);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 26, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		default:
			break;
	}
}


void voip_mipc_server_handler(char* inMsg, unsigned int size)
{
	char source_module[MIPC_NAME_MAX_LEN];
	unsigned int api_id;
	
	if (MIPC_ERROR != mipc_receive_msg(source_module, &api_id, inMsg, size))
		{
			// Call APIs
		voip_mipc_server_call(source_module, api_id, inMsg + sizeof(MIPC_MSG_HDR_T));
	}
}


