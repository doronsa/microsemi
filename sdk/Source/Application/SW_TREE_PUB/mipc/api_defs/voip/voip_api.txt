name = voip
id = VOIP_MODULE_ID
version = 1.0

#include "voip_api.h"

S INT32 VOIP_updateSipTcpUdpPort_F(IN int sipPort);
S INT32 VOIP_updateHostIpAddr_F(IN UINT32 ip_address, IN UINT32 ip_mask, IN UINT32 gate_way);
S INT32 VOIP_updateHostVidPbits_F(IN int vid, IN int pbits);
S INT32 VOIP_updateSipServerAddr_F(IN int potsPort, IN char *proxy);
S INT32 VOIP_updateSipAccountData_F(IN int potsPort, IN char *userName, IN char *password, IN char *aor);
S INT32 VOIP_setPortActiveState_F(IN UINT32 port_id, IN VOIP_ST_PORT_ACTIVE_STATE *info);
S INT32 VOIP_getPortActiveState_F(IN UINT32 port_id, OUT VOIP_ST_PORT_ACTIVE_STATE *info);
S INT32 VOIP_getIadInfo_F(OUT VOIP_ST_IADINFO *info);
S INT32 VOIP_setGlobalParam_F(IN VOIP_GLOBAL_PARAM_T *info);
S INT32 VOIP_getGlobalParam_F(OUT VOIP_GLOBAL_PARAM_T *info);
S INT32 VOIP_setH248ParamConfig_F(IN VOIP_ST_H248_PARAMETER_CONFIG *info);
S INT32 VOIP_getH248ParamConfig_F(OUT VOIP_ST_H248_PARAMETER_CONFIG *info);
S INT32 VOIP_setH248UserTIDInfo_F(IN UINT32 port_id, IN VOIP_ST_H248_USER_TIDINFO *info);
S INT32 VOIP_getH248UserTIDInfo_F(IN UINT32 port_id, OUT VOIP_ST_H248_USER_TIDINFO *info);
S INT32 VOIP_setH248RtpTIDConfig_F(IN VOIP_ST_H248_RTP_TID_CONFIG *info);
S INT32 VOIP_getH248RtpTIDConfig_F(OUT VOIP_ST_H248_RTP_TID_CONFIG *info);
S INT32 VOIP_getH248RtpTIDInfo_F(OUT VOIP_ST_H248_RTP_TID_INFO *info); 
S INT32 VOIP_setSipParamConfig_F(IN VOIP_ST_SIP_PARAMETER_CONFIG *info);
S INT32 VOIP_getSipParamConfig_F(OUT VOIP_ST_SIP_PARAMETER_CONFIG *info);
S INT32 VOIP_setSipUserParamConfig_F(IN UINT32 port_id, IN VOIP_ST_SIP_USER_PARAMETER_CONFIG *info);
S INT32 VOIP_getSipUserParamConfig_F(IN UINT32 port_id, OUT VOIP_ST_SIP_USER_PARAMETER_CONFIG *info);
S INT32 VOIP_setFaxModemConfig_F(IN VOIP_ST_FAX_MODEM_CONFIG *info);
S INT32 VOIP_getFaxModemConfig_F(OUT VOIP_ST_FAX_MODEM_CONFIG *info);														
S INT32 VOIP_getH248IadOperStatus_F(OUT VOIP_ST_H248_IAD_OPERATION_STATUS *info);									
S INT32 VOIP_getPotsStatus_F(IN UINT32 port_id, OUT VOIP_ST_POTS_STATUS *info);												
S INT32 VOIP_setIadOperation_F(IN VOIP_ST_IAD_OPERATION *info);																
S INT32 VOIP_setSipDigitmap_F(IN VOIP_ST_SIP_DIGITAL_MAP *info);																
