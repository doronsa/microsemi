/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include "voip_api.h"

#include "voip_mipc_defs.h"


INT32 VOIP_updateSipTcpUdpPort_F(int sipPort)
{
	VOIP_updateSipTcpUdpPort_F_IN_T input;
	VOIP_updateSipTcpUdpPort_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.sipPort = sipPort;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 0, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_updateHostIpAddr_F(UINT32 ip_address, UINT32 ip_mask, UINT32 gate_way)
{
	VOIP_updateHostIpAddr_F_IN_T input;
	VOIP_updateHostIpAddr_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.ip_address = ip_address;
	input.ip_mask = ip_mask;
	input.gate_way = gate_way;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 1, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_updateHostVidPbits_F(int vid, int pbits)
{
	VOIP_updateHostVidPbits_F_IN_T input;
	VOIP_updateHostVidPbits_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.vid = vid;
	input.pbits = pbits;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 2, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_updateSipServerAddr_F(int potsPort, char* proxy)
{
	VOIP_updateSipServerAddr_F_IN_T input;
	VOIP_updateSipServerAddr_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.potsPort = potsPort;
   if (NULL != proxy)
	    strcpy(input.proxy, proxy);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 3, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_updateSipAccountData_F(int potsPort, char* userName, char* password, char* aor)
{
	VOIP_updateSipAccountData_F_IN_T input;
	VOIP_updateSipAccountData_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.potsPort = potsPort;
   if (NULL != userName)
	    strcpy(input.userName, userName);
   if (NULL != password)
	    strcpy(input.password, password);
   if (NULL != aor)
	    strcpy(input.aor, aor);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 4, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_setPortActiveState_F(UINT32 port_id, VOIP_ST_PORT_ACTIVE_STATE* info)
{
	VOIP_setPortActiveState_F_IN_T input;
	VOIP_setPortActiveState_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;
   if (NULL != info)
	    memcpy(&input.info, info, sizeof(VOIP_ST_PORT_ACTIVE_STATE));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 5, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_getPortActiveState_F(UINT32 port_id, VOIP_ST_PORT_ACTIVE_STATE* info)
{
	VOIP_getPortActiveState_F_IN_T input;
	VOIP_getPortActiveState_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 6, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_ST_PORT_ACTIVE_STATE));

	return output.ret;
}


INT32 VOIP_getIadInfo_F(VOIP_ST_IADINFO* info)
{
	VOIP_getIadInfo_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 7, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_ST_IADINFO));

	return output.ret;
}


INT32 VOIP_setGlobalParam_F(VOIP_GLOBAL_PARAM_T* info)
{
	VOIP_setGlobalParam_F_IN_T input;
	VOIP_setGlobalParam_F_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != info)
	    memcpy(&input.info, info, sizeof(VOIP_GLOBAL_PARAM_T));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 8, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_getGlobalParam_F(VOIP_GLOBAL_PARAM_T* info)
{
	VOIP_getGlobalParam_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 9, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_GLOBAL_PARAM_T));

	return output.ret;
}


INT32 VOIP_setH248ParamConfig_F(VOIP_ST_H248_PARAMETER_CONFIG* info)
{
	VOIP_setH248ParamConfig_F_IN_T input;
	VOIP_setH248ParamConfig_F_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != info)
	    memcpy(&input.info, info, sizeof(VOIP_ST_H248_PARAMETER_CONFIG));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 10, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_getH248ParamConfig_F(VOIP_ST_H248_PARAMETER_CONFIG* info)
{
	VOIP_getH248ParamConfig_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 11, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_ST_H248_PARAMETER_CONFIG));

	return output.ret;
}


INT32 VOIP_setH248UserTIDInfo_F(UINT32 port_id, VOIP_ST_H248_USER_TIDINFO* info)
{
	VOIP_setH248UserTIDInfo_F_IN_T input;
	VOIP_setH248UserTIDInfo_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;
   if (NULL != info)
	    memcpy(&input.info, info, sizeof(VOIP_ST_H248_USER_TIDINFO));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 12, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_getH248UserTIDInfo_F(UINT32 port_id, VOIP_ST_H248_USER_TIDINFO* info)
{
	VOIP_getH248UserTIDInfo_F_IN_T input;
	VOIP_getH248UserTIDInfo_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 13, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_ST_H248_USER_TIDINFO));

	return output.ret;
}


INT32 VOIP_setH248RtpTIDConfig_F(VOIP_ST_H248_RTP_TID_CONFIG* info)
{
	VOIP_setH248RtpTIDConfig_F_IN_T input;
	VOIP_setH248RtpTIDConfig_F_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != info)
	    memcpy(&input.info, info, sizeof(VOIP_ST_H248_RTP_TID_CONFIG));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 14, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_getH248RtpTIDConfig_F(VOIP_ST_H248_RTP_TID_CONFIG* info)
{
	VOIP_getH248RtpTIDConfig_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 15, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_ST_H248_RTP_TID_CONFIG));

	return output.ret;
}


INT32 VOIP_getH248RtpTIDInfo_F(VOIP_ST_H248_RTP_TID_INFO* info)
{
	VOIP_getH248RtpTIDInfo_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 16, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_ST_H248_RTP_TID_INFO));

	return output.ret;
}


INT32 VOIP_setSipParamConfig_F(VOIP_ST_SIP_PARAMETER_CONFIG* info)
{
	VOIP_setSipParamConfig_F_IN_T input;
	VOIP_setSipParamConfig_F_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != info)
	    memcpy(&input.info, info, sizeof(VOIP_ST_SIP_PARAMETER_CONFIG));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 17, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_getSipParamConfig_F(VOIP_ST_SIP_PARAMETER_CONFIG* info)
{
	VOIP_getSipParamConfig_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 18, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_ST_SIP_PARAMETER_CONFIG));

	return output.ret;
}


INT32 VOIP_setSipUserParamConfig_F(UINT32 port_id, VOIP_ST_SIP_USER_PARAMETER_CONFIG* info)
{
	VOIP_setSipUserParamConfig_F_IN_T input;
	VOIP_setSipUserParamConfig_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;
   if (NULL != info)
	    memcpy(&input.info, info, sizeof(VOIP_ST_SIP_USER_PARAMETER_CONFIG));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 19, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_getSipUserParamConfig_F(UINT32 port_id, VOIP_ST_SIP_USER_PARAMETER_CONFIG* info)
{
	VOIP_getSipUserParamConfig_F_IN_T input;
	VOIP_getSipUserParamConfig_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 20, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_ST_SIP_USER_PARAMETER_CONFIG));

	return output.ret;
}


INT32 VOIP_setFaxModemConfig_F(VOIP_ST_FAX_MODEM_CONFIG* info)
{
	VOIP_setFaxModemConfig_F_IN_T input;
	VOIP_setFaxModemConfig_F_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != info)
	    memcpy(&input.info, info, sizeof(VOIP_ST_FAX_MODEM_CONFIG));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 21, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_getFaxModemConfig_F(VOIP_ST_FAX_MODEM_CONFIG* info)
{
	VOIP_getFaxModemConfig_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 22, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_ST_FAX_MODEM_CONFIG));

	return output.ret;
}


INT32 VOIP_getH248IadOperStatus_F(VOIP_ST_H248_IAD_OPERATION_STATUS* info)
{
	VOIP_getH248IadOperStatus_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 23, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_ST_H248_IAD_OPERATION_STATUS));

	return output.ret;
}


INT32 VOIP_getPotsStatus_F(UINT32 port_id, VOIP_ST_POTS_STATUS* info)
{
	VOIP_getPotsStatus_F_IN_T input;
	VOIP_getPotsStatus_F_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.port_id = port_id;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 24, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(info, &output.info, sizeof(VOIP_ST_POTS_STATUS));

	return output.ret;
}


INT32 VOIP_setIadOperation_F(VOIP_ST_IAD_OPERATION* info)
{
	VOIP_setIadOperation_F_IN_T input;
	VOIP_setIadOperation_F_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != info)
	    memcpy(&input.info, info, sizeof(VOIP_ST_IAD_OPERATION));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 25, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


INT32 VOIP_setSipDigitmap_F(VOIP_ST_SIP_DIGITAL_MAP* info)
{
	VOIP_setSipDigitmap_F_IN_T input;
	VOIP_setSipDigitmap_F_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != info)
	    memcpy(&input.info, info, sizeof(VOIP_ST_SIP_DIGITAL_MAP));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("voip", 26, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


