/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include "sycl_mipc_defs.h"


void  sycl_reboot(int delay)
{
	sycl_reboot_IN_T input;

	input.delay = delay;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_async_msg("sycl", 0, &input, sizeof(input)))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
}


