/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "I2cMain.h"

#include "i2c_mipc_defs.h"


bool I2c_cli_show_xvr_a2d_values(char* name)
{
	I2c_cli_show_xvr_a2d_values_IN_T input;
	I2c_cli_show_xvr_a2d_values_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("i2c", 0, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool I2c_cli_show_xvr_thresholds(char* name)
{
	I2c_cli_show_xvr_thresholds_IN_T input;
	I2c_cli_show_xvr_thresholds_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("i2c", 1, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool I2c_cli_show_xvr_alarms_and_warnings(char* name)
{
	I2c_cli_show_xvr_alarms_and_warnings_IN_T input;
	I2c_cli_show_xvr_alarms_and_warnings_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("i2c", 2, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool I2c_cli_show_xvr_inventory(char* name)
{
	I2c_cli_show_xvr_inventory_IN_T input;
	I2c_cli_show_xvr_inventory_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("i2c", 3, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool I2c_cli_show_xvr_capability(char* name)
{
	I2c_cli_show_xvr_capability_IN_T input;
	I2c_cli_show_xvr_capability_OUT_T output;

	memset(&output, 0, sizeof(output));

   if (NULL != name)
	    strcpy(input.name, name);

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_cli_msg("i2c", 4, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool localI2cApi_getOnuXvrA2dValues(OnuXvrA2D_S* p_OnuXvrA2D)
{
	localI2cApi_getOnuXvrA2dValues_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("i2c", 5, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(p_OnuXvrA2D, &output.p_OnuXvrA2D, sizeof(OnuXvrA2D_S));

	return output.ret;
}


bool localI2cApi_getOnuXvrThresholds(OnuXvrThresholds_S* p_OnuXvrThresholds)
{
	localI2cApi_getOnuXvrThresholds_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("i2c", 6, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(p_OnuXvrThresholds, &output.p_OnuXvrThresholds, sizeof(OnuXvrThresholds_S));

	return output.ret;
}


bool localI2cApi_getOnuXvrAlarmsAndWarnings(OnuXvrAlarmsAndWarnings_S* p_OnuXvrAlarmsAndWarnings)
{
	localI2cApi_getOnuXvrAlarmsAndWarnings_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("i2c", 7, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(p_OnuXvrAlarmsAndWarnings, &output.p_OnuXvrAlarmsAndWarnings, sizeof(OnuXvrAlarmsAndWarnings_S));

	return output.ret;
}


bool localI2cApi_getOnuXvrInventory(OnuXvrInventory_S* p_OnuXvrInventory)
{
	localI2cApi_getOnuXvrInventory_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("i2c", 8, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(p_OnuXvrInventory, &output.p_OnuXvrInventory, sizeof(OnuXvrInventory_S));

	return output.ret;
}


void  localI2cApi_whetherOnuXvrSupportI2c(INT32* support_onu_xvr_I2c)
{
	localI2cApi_whetherOnuXvrSupportI2c_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("i2c", 9, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(support_onu_xvr_I2c, &output.support_onu_xvr_I2c, sizeof(INT32));

}


void  localI2cApi_getOnuXvrI2CFunc(UINT32* p_funcs)
{
	localI2cApi_getOnuXvrI2CFunc_OUT_T output;

	memset(&output, 0, sizeof(output));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("i2c", 10, NULL, 0, &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(p_funcs, &output.p_funcs, sizeof(UINT32));

}


bool I2cApi_apmSetOnuXvrThreshold(UINT32 alarm_type, UINT32 threshold, UINT32 clear_threshold)
{
	I2cApi_apmSetOnuXvrThreshold_IN_T input;
	I2cApi_apmSetOnuXvrThreshold_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.alarm_type = alarm_type;
	input.threshold = threshold;
	input.clear_threshold = clear_threshold;

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("i2c", 11, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	return output.ret;
}


bool I2cApi_apmGetOnuXvrStateAndValue(UINT32 alarm_type, UINT8* p_state, UINT32* P_value)
{
	I2cApi_apmGetOnuXvrStateAndValue_IN_T input;
	I2cApi_apmGetOnuXvrStateAndValue_OUT_T output;

	memset(&output, 0, sizeof(output));

	input.alarm_type = alarm_type;
   if (NULL != p_state)
	    memcpy(&input.p_state, p_state, sizeof(UINT8));

	// Call message queue or socket to send out the parameters
	if (MIPC_OK != mipc_send_sync_msg("i2c", 12, &input, sizeof(input), &output, sizeof(output), -1))
	{
		printf("%s: failed to send message\n", __FUNCTION__);
	}
	memcpy(p_state, &output.p_state, sizeof(UINT8));
	memcpy(P_value, &output.P_value, sizeof(UINT32));

	return output.ret;
}


