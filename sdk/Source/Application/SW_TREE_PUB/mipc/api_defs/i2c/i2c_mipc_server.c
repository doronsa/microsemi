/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "mipc.h"

#include <stdint.h>

#include <stdbool.h>

#include "globals.h"

#include "I2cMain.h"

#include "i2c_mipc_defs.h"


void i2c_mipc_server_call(char* source_module, int api_id, char* input)
{
	switch (api_id)
	{
		case 0:
		{
			I2c_cli_show_xvr_a2d_values_IN_T* in = (I2c_cli_show_xvr_a2d_values_IN_T*)input;
			I2c_cli_show_xvr_a2d_values_OUT_T out;

			out.ret = I2c_cli_show_xvr_a2d_values((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 0, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 1:
		{
			I2c_cli_show_xvr_thresholds_IN_T* in = (I2c_cli_show_xvr_thresholds_IN_T*)input;
			I2c_cli_show_xvr_thresholds_OUT_T out;

			out.ret = I2c_cli_show_xvr_thresholds((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 1, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 2:
		{
			I2c_cli_show_xvr_alarms_and_warnings_IN_T* in = (I2c_cli_show_xvr_alarms_and_warnings_IN_T*)input;
			I2c_cli_show_xvr_alarms_and_warnings_OUT_T out;

			out.ret = I2c_cli_show_xvr_alarms_and_warnings((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 2, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 3:
		{
			I2c_cli_show_xvr_inventory_IN_T* in = (I2c_cli_show_xvr_inventory_IN_T*)input;
			I2c_cli_show_xvr_inventory_OUT_T out;

			out.ret = I2c_cli_show_xvr_inventory((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 3, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 4:
		{
			I2c_cli_show_xvr_capability_IN_T* in = (I2c_cli_show_xvr_capability_IN_T*)input;
			I2c_cli_show_xvr_capability_OUT_T out;

			out.ret = I2c_cli_show_xvr_capability((void*)source_module);


			// send end of print flag
			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 4, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 5:
		{
			localI2cApi_getOnuXvrA2dValues_OUT_T out;

			out.ret = localI2cApi_getOnuXvrA2dValues(&out.p_OnuXvrA2D);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 5, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 6:
		{
			localI2cApi_getOnuXvrThresholds_OUT_T out;

			out.ret = localI2cApi_getOnuXvrThresholds(&out.p_OnuXvrThresholds);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 6, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 7:
		{
			localI2cApi_getOnuXvrAlarmsAndWarnings_OUT_T out;

			out.ret = localI2cApi_getOnuXvrAlarmsAndWarnings(&out.p_OnuXvrAlarmsAndWarnings);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 7, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 8:
		{
			localI2cApi_getOnuXvrInventory_OUT_T out;

			out.ret = localI2cApi_getOnuXvrInventory(&out.p_OnuXvrInventory);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 8, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 9:
		{
			localI2cApi_whetherOnuXvrSupportI2c_OUT_T out;

			localI2cApi_whetherOnuXvrSupportI2c(&out.support_onu_xvr_I2c);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 9, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 10:
		{
			localI2cApi_getOnuXvrI2CFunc_OUT_T out;

			localI2cApi_getOnuXvrI2CFunc(&out.p_funcs);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 10, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 11:
		{
			I2cApi_apmSetOnuXvrThreshold_IN_T* in = (I2cApi_apmSetOnuXvrThreshold_IN_T*)input;
			I2cApi_apmSetOnuXvrThreshold_OUT_T out;

			out.ret = I2cApi_apmSetOnuXvrThreshold(in->alarm_type, in->threshold, in->clear_threshold);


			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 11, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		case 12:
		{
			I2cApi_apmGetOnuXvrStateAndValue_IN_T* in = (I2cApi_apmGetOnuXvrStateAndValue_IN_T*)input;
			I2cApi_apmGetOnuXvrStateAndValue_OUT_T out;

			out.ret = I2cApi_apmGetOnuXvrStateAndValue(in->alarm_type, &in->p_state, &out.P_value);

			memcpy(&out.p_state, &in->p_state, sizeof(UINT8));

			// send back everyting in out data structure
			if (MIPC_OK != mipc_response_msg(source_module, 12, &out, sizeof(out)))
			{
				printf("%s: failed to send response message\n", __FUNCTION__);
			}
			break;
		}

		default:
			break;
	}
}


void i2c_mipc_server_handler(char* inMsg, unsigned int size)
{
	char source_module[MIPC_NAME_MAX_LEN];
	unsigned int api_id;
	
	if (MIPC_ERROR != mipc_receive_msg(source_module, &api_id, inMsg, size))
		{
			// Call APIs
		i2c_mipc_server_call(source_module, api_id, inMsg + sizeof(MIPC_MSG_HDR_T));
	}
}


