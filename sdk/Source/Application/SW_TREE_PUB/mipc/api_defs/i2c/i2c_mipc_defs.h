/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _i2c_h_
#define _i2c_h_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	char name[256];
} I2c_cli_show_xvr_a2d_values_IN_T;


typedef struct {
	bool ret;
} I2c_cli_show_xvr_a2d_values_OUT_T;


typedef struct {
	char name[256];
} I2c_cli_show_xvr_thresholds_IN_T;


typedef struct {
	bool ret;
} I2c_cli_show_xvr_thresholds_OUT_T;


typedef struct {
	char name[256];
} I2c_cli_show_xvr_alarms_and_warnings_IN_T;


typedef struct {
	bool ret;
} I2c_cli_show_xvr_alarms_and_warnings_OUT_T;


typedef struct {
	char name[256];
} I2c_cli_show_xvr_inventory_IN_T;


typedef struct {
	bool ret;
} I2c_cli_show_xvr_inventory_OUT_T;


typedef struct {
	char name[256];
} I2c_cli_show_xvr_capability_IN_T;


typedef struct {
	bool ret;
} I2c_cli_show_xvr_capability_OUT_T;


typedef struct {
	bool ret;
	OnuXvrA2D_S p_OnuXvrA2D;
} localI2cApi_getOnuXvrA2dValues_OUT_T;


typedef struct {
	bool ret;
	OnuXvrThresholds_S p_OnuXvrThresholds;
} localI2cApi_getOnuXvrThresholds_OUT_T;


typedef struct {
	bool ret;
	OnuXvrAlarmsAndWarnings_S p_OnuXvrAlarmsAndWarnings;
} localI2cApi_getOnuXvrAlarmsAndWarnings_OUT_T;


typedef struct {
	bool ret;
	OnuXvrInventory_S p_OnuXvrInventory;
} localI2cApi_getOnuXvrInventory_OUT_T;


typedef struct {
	INT32 support_onu_xvr_I2c;
} localI2cApi_whetherOnuXvrSupportI2c_OUT_T;


typedef struct {
	UINT32 p_funcs;
} localI2cApi_getOnuXvrI2CFunc_OUT_T;


typedef struct {
	UINT32 alarm_type;
	UINT32 threshold;
	UINT32 clear_threshold;
} I2cApi_apmSetOnuXvrThreshold_IN_T;


typedef struct {
	bool ret;
} I2cApi_apmSetOnuXvrThreshold_OUT_T;


typedef struct {
	UINT32 alarm_type;
	UINT8 p_state;
} I2cApi_apmGetOnuXvrStateAndValue_IN_T;


typedef struct {
	bool ret;
	UINT8 p_state;
	UINT32 P_value;
} I2cApi_apmGetOnuXvrStateAndValue_OUT_T;


#ifdef __cplusplus
}
#endif
#endif
