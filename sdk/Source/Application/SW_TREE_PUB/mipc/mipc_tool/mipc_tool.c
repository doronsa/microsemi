#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MIPC_NAME_MAX_LEN 64
#define MIPC_LINE_MAX_LEN 512
#define MIPC_API_MAX_NUM  256
#define MIPC_PARA_MAX_NUM 16
#define MIPC_PARA_MAX_SIZE 2048
#define MIPC_PARA_MAX_STR_SIZE 256
#define MIPC_INCLUDE_MAX_NUM  32

#define MIPC_PARA_IN 0x1
#define MIPC_PARA_OUT 0x2

typedef struct
{
	char paraType[MIPC_NAME_MAX_LEN];
	char paraName[MIPC_NAME_MAX_LEN];
	int  isPointer;
	int  isIn;
} MIPC_API_PARA_INFO_T;
	
typedef struct
{
	char retType[MIPC_NAME_MAX_LEN];
	int  isPointer;
	int  isVoid;
} MIPC_API_RET_INFO_T;	

typedef struct
{	int  syncType;//0: async, 1: sync, 2: CLI
	int  timeout;  //in ms
	char apiName[MIPC_NAME_MAX_LEN];

	MIPC_API_RET_INFO_T retInfo;
	
	int numOfParas;
	int numOfInputParas;
	int numOfOutputParas;
	MIPC_API_PARA_INFO_T param[MIPC_PARA_MAX_NUM];
	
} MIPC_API_INFO_T;

typedef struct
{
	char            name[MIPC_NAME_MAX_LEN];
	char            id[MIPC_NAME_MAX_LEN];
	char            ver[MIPC_NAME_MAX_LEN];
	int             numOfApi;
	MIPC_API_INFO_T api[MIPC_API_MAX_NUM];
	int             numOfIncludes;
	char            includes[MIPC_INCLUDE_MAX_NUM][MIPC_LINE_MAX_LEN];
} MIPC_MODULE_INFO_T;

static MIPC_MODULE_INFO_T gMipcModuleInfo;

void mipc_print_module_info(MIPC_MODULE_INFO_T* module_info)
{
	int i, j;
	printf("name = %s, id = %s, ver = %s, numofApi = %d\n", module_info->name, module_info->id, module_info->ver, module_info->numOfApi);
	for (i = 0; i < module_info->numOfApi; i++)
	{
		printf("syncType=%d, timeout=%d, apiName=%s, numOfPara=%d, IN(%d)OUT(%d)\n", module_info->api[i].syncType, module_info->api[i].timeout, module_info->api[i].apiName, module_info->api[i].numOfParas, module_info->api[i].numOfInputParas, module_info->api[i].numOfOutputParas);
		printf("retType=%s, isVoid=%d, isPtr=%d\n", module_info->api[i].retInfo.retType, module_info->api[i].retInfo.isVoid, module_info->api[i].retInfo.isPointer);
		for (j = 0; j < module_info->api[i].numOfParas; j++)
		{
			printf("	ParaType=%s, ParaName=%s, IsPointer=%d, IsIn=%d\n", module_info->api[i].param[j].paraType, module_info->api[i].param[j].paraName, module_info->api[i].param[j].isPointer, module_info->api[i].param[j].isIn);
		}
		printf("\n\n");
	}
}

char* mipc_str_skip_space(char* str)
{
	while (*str == ' ')
	{
		str++;
	}
	if (*str == '\0' || *str == '\n' || *str == '\r')
	{
		return NULL;
	}
	return str;
}

char* mipc_str_get_first_word(char* str, char* word)
{
	while (((*str >= '0') && (*str <= '9')) ||
	       ((*str >= 'a') && (*str <= 'z')) ||
		   ((*str >= 'A') && (*str <= 'Z')) ||
		   (*str == '_'))
	{
		*word = *str;
		word++;
		str++;
	}
	*word = '\0';
	
	if (*str == '\0')
	{
		return NULL;
	}
	return str;
}

char* mipc_gets_skip_empty_lines(FILE* fd, char* line, int size)
{
	char temp[MIPC_LINE_MAX_LEN];
	
	while (!feof(fd))
	{
		fgets(line, size, fd);
		if (NULL == mipc_str_skip_space(line))
		{
			continue; 
		}
		else
		{
			return line;
		}
	}
	return NULL;
}

int mipc_get_value_of_name(char* line, char* name, char* value)
{
	char tempName[MIPC_NAME_MAX_LEN];
	char* temp = line;
	
	// Get Name
	temp = mipc_str_skip_space(temp);
	if (NULL == temp)
	{
		printf("the line is empty!\n");
		return -1;
	}

	temp = mipc_str_get_first_word(temp,tempName);
	if (NULL == temp)
	{
		printf("No word found in this line: %s\n", line);
		return -1;
	}

	if (0 != strcasecmp(tempName, name))
	{
		printf("Cannot find %s in %s\n", name, line);
		return -1;
	}

	// '='
	temp = mipc_str_skip_space(temp);
	if (temp[0] != '=')
	{
		printf("= not found in this line: %s,%s\n", line, temp);
		return -1;
	}
	
	// Get Value
	temp = mipc_str_skip_space(&temp[1]);
	if (NULL == mipc_str_get_first_word(temp, value))
	{
		printf("No value found in this line: %s\n", line);
		return -1;
	}

	return 0;
}

int mipc_get_api_info(char* line, MIPC_API_INFO_T* api_info)
{
	char tempName[MIPC_NAME_MAX_LEN];
	char* temp = line;
	
	temp = mipc_str_skip_space(temp);
	if (NULL == temp)
	{
		printf("Empty Line\n");
		return -1;
	}

	// Get syncType
	temp = mipc_str_get_first_word(temp, tempName);
	if (NULL == temp)
	{
	    printf("Get sync type failed: %s\n", line);
		return -1;
	}
	if (!strcmp("S", tempName))
	{
		api_info->syncType = 1;
		temp = mipc_str_skip_space(temp);
		if (NULL == temp)
		{
			printf("Get time out failed: %s\n", line);
			return -1;
		}
		if (temp[0] == '(')
		{
			//time out
			temp++;
			temp = mipc_str_skip_space(temp);
			if (NULL == temp)
			{
				printf("Get time out failed: %s\n", line);
				return -1;
			}
			temp = mipc_str_get_first_word(temp, tempName);
			if (NULL == temp)
			{
				printf("Get time out failed: %s\n", line);
				return -1;
			}
			else
			{
				api_info->timeout = atoi(tempName);
				//skip ')'
				temp = mipc_str_skip_space(temp);
				if (NULL == temp)
				{
					printf("Get time out failed: %s\n", line);
					return -1;
				}
				temp++;
			}
		}
	}
	else
	if (!strcmp("A", tempName))
	{
		api_info->syncType = 0;
	}
	else
	if (!strcmp("CLI", tempName))
	{
		api_info->syncType = 2;
	}
	else
	{
		printf("Get sync type failed: %s\n", line);
		return -1;
	}
	
	//mipc_print_module_info(&gMipcModuleInfo);
	
	//Get Return Type
	temp = mipc_str_skip_space(temp);
	if (NULL == temp)
	{
		printf("Get return type failed: %s\n", line);
		return -1;
	}
	
	temp = mipc_str_get_first_word(temp, tempName);
	if (NULL == temp)
	{
	    printf("Get return type failed: %s\n", line);
		return -1;
	}
	
	if (!strcasecmp("void", tempName))
	{
		api_info->retInfo.isVoid = 1;
	}
	strcpy(api_info->retInfo.retType, tempName);
	
	temp = mipc_str_skip_space(temp);
	if (NULL == temp)
	{
		printf("Get return type failed: %s\n", line);
		return -1;
	}
	if (temp[0] == '*')
	{
		temp++;
		api_info->retInfo.isPointer = 1;
		api_info->retInfo.isVoid = 0;
	}
	
	// Get function name
	temp = mipc_str_skip_space(temp);
	if (NULL == temp)
	{
		printf("Get function name failed: %s\n", line);
		return -1;
	}
	
	temp = mipc_str_get_first_word(temp, tempName);
	if (NULL == temp)
	{
	    printf("Get function name failed: %s\n", line);
		return -1;
	}
	strcpy(api_info->apiName, tempName);
	
	// Get Parameters
	api_info->numOfParas = 0;
	temp = mipc_str_skip_space(temp);
	if (NULL == temp)
	{
		printf("Get parameters failed: %s\n", line);
		return -1;
	}
	if (temp[0] != '(')
	{
		printf("Get parameters failed: %s\n", line);
		return -1;
	}
	//skip '('
	temp++;
	temp = mipc_str_skip_space(temp);
	if (NULL == temp)
	{
		printf("Get parameters failed: %s\n", line);
		return -1;
	}
	if (temp[0] == ')')
	{
		return 0;
	}
	// First parameter
	temp = mipc_str_get_first_word(temp, tempName);
	if (NULL == temp)
	{
	    printf("Get parameters failed: %s\n", line);
		return -1;
	}
	
	if (!strcasecmp("void", tempName))
	{
		temp = mipc_str_skip_space(temp);
		if (NULL == temp)
		{
			printf("Get parameters failed: %s\n", line);
			return -1;
		}
		if (temp[0] == ')')
		{
			return 0;
		}
		else
		{
			printf("Lack of IN/OUT indication: %s\n", line);
			return -1;
		}
	}
	else
	if (!strcmp("IN", tempName))
	{
		api_info->param[api_info->numOfParas].isIn = MIPC_PARA_IN;
		api_info->numOfInputParas++;
	}
	else
	if (!strcmp("OUT", tempName))
	{
		api_info->param[api_info->numOfParas].isIn = MIPC_PARA_OUT;
		api_info->numOfOutputParas++;
	}
	else
	if (!strcmp("IO", tempName))
	{
		api_info->param[api_info->numOfParas].isIn = MIPC_PARA_OUT|MIPC_PARA_IN;
		api_info->numOfInputParas++;
		api_info->numOfOutputParas++;
	}
	else
	{
		printf("Lack of IN/OUT indication: %s\n", line);
		return -1;
	}
	
	temp = mipc_str_skip_space(temp);
	if (NULL == temp)
	{
		printf("Get para %d failed: %s\n", api_info->numOfParas+1, line);
		return -1;
	}
	
	temp = mipc_str_get_first_word(temp, tempName);
	if (NULL == temp)
	{
	    printf("Get para %d failed: %s\n", api_info->numOfParas+1, line);
		return -1;
	}
	strcpy(api_info->param[api_info->numOfParas].paraType, tempName);
	temp = mipc_str_skip_space(temp);
	if (NULL == temp)
	{
		printf("Get para %d failed: %s\n", api_info->numOfParas+1, line);
		return -1;
	}
	if (temp[0] == '*')
	{
		api_info->param[api_info->numOfParas].isPointer = 1;
		temp++;
	}
	temp = mipc_str_skip_space(temp);
	if (NULL == temp)
	{
		printf("Get para name %d failed: %s\n", api_info->numOfParas+1, line);
		return -1;
	}
	
	temp = mipc_str_get_first_word(temp, tempName);
	if (NULL == temp)
	{
	    printf("Get para name %d failed: %s\n", api_info->numOfParas+1, line);
		return -1;
	}
	strcpy(api_info->param[api_info->numOfParas].paraName, tempName);
	
	api_info->numOfParas++;
	
	do
	{
		temp = index(temp, ',');
		if (NULL == temp)
		{
			return 0;
		}
		temp++;
		
		temp = mipc_str_skip_space(temp);
		if (NULL == temp)
		{
			printf("Get parameter %d failed: %s\n", api_info->numOfParas+1, line);
			return -1;
		}

		temp = mipc_str_get_first_word(temp, tempName);
		if (NULL == temp)
		{
			printf("Get parameter %d failed: %s\n", api_info->numOfParas+1, line);
			return -1;
		}
		
		if (!strcmp("IN", tempName))
		{
			api_info->param[api_info->numOfParas].isIn = MIPC_PARA_IN;
			api_info->numOfInputParas++;
		}
		else
		if (!strcmp("OUT", tempName))
		{
			api_info->param[api_info->numOfParas].isIn = MIPC_PARA_OUT;
			api_info->numOfOutputParas++;
		}
		else
		if (!strcmp("IO", tempName))
		{
			api_info->param[api_info->numOfParas].isIn = MIPC_PARA_IN|MIPC_PARA_OUT;
			api_info->numOfInputParas++;
			api_info->numOfOutputParas++;
		}
		else
		{
			printf("Lack of IN/OUT indication before parameter %d: %s\n", api_info->numOfParas+1, line);
			return -1;
		}
		
		temp = mipc_str_skip_space(temp);
		if (NULL == temp)
		{
			printf("Get para %d failed: %s\n", api_info->numOfParas+1, line);
			return -1;
		}
		
		temp = mipc_str_get_first_word(temp, tempName);
		if (NULL == temp)
		{
			printf("Get para %d failed: %s\n", api_info->numOfParas+1, line);
			return -1;
		}
		strcpy(api_info->param[api_info->numOfParas].paraType, tempName);
		temp = mipc_str_skip_space(temp);
		if (NULL == temp)
		{
			printf("Get para %d failed: %s\n", api_info->numOfParas+1, line);
			return -1;
		}
		if (temp[0] == '*')
		{
			api_info->param[api_info->numOfParas].isPointer = 1;
			temp++;
		}
		temp = mipc_str_skip_space(temp);
		if (NULL == temp)
		{
			printf("Get para name %d failed: %s\n", api_info->numOfParas+1, line);
			return -1;
		}
		
		temp = mipc_str_get_first_word(temp, tempName);
		if (NULL == temp)
		{
			printf("Get para name %d failed: %s\n", api_info->numOfParas+1, line);
			return -1;
		}
		strcpy(api_info->param[api_info->numOfParas].paraName, tempName);
		api_info->numOfParas++;
	} while (1);
	
	return 0;
}

int mipc_get_module_info(FILE* fd, MIPC_MODULE_INFO_T* module_info)
{
	char line[MIPC_LINE_MAX_LEN];
	char* temp;
	
	if (NULL == mipc_gets_skip_empty_lines(fd, line, MIPC_LINE_MAX_LEN))
	{
		printf("Nothing in this script file!\n");
		return -1;
	}

	if (mipc_get_value_of_name(line, "name", module_info->name) < 0)
	{
		printf("Canot find name definition!\n");
		return -1;
	}
	
	if (NULL == mipc_gets_skip_empty_lines(fd, line, MIPC_LINE_MAX_LEN))
	{
		printf("Canot find id definition!\n");
		return -1;
	}
	
	if (mipc_get_value_of_name(line, "id", module_info->id) < 0)
	{
		printf("Canot find id definition!\n");
		return -1;
	}
	
	if (NULL == mipc_gets_skip_empty_lines(fd, line, MIPC_LINE_MAX_LEN))
	{
		printf("Canot find version definition!\n");
		return -1;
	}
		
	if (mipc_get_value_of_name(line, "version", module_info->ver) < 0)
	{
		printf("Canot find version definition!\n");
		return -1;
	}
	
	module_info->numOfIncludes = 0;
	module_info->numOfApi = 0;
	
	while (!feof(fd))
	{
	    memset(line, '\0', MIPC_LINE_MAX_LEN);
		if (NULL != mipc_gets_skip_empty_lines(fd, line, MIPC_LINE_MAX_LEN))
		{
			temp = mipc_str_skip_space(line);
			if ('#' == temp[0])
			{
				strcpy(&module_info->includes[module_info->numOfIncludes][0], temp);
				module_info->numOfIncludes++;
			}
			else
			{
				if (mipc_get_api_info(temp, &module_info->api[module_info->numOfApi]) < 0)
				{
					printf("Get API Info failed!\n");
					return -1;
				}
				module_info->numOfApi++;
			}
		}
		//printf("%s\n", line);
		
		//mipc_print_module_info(&gMipcModuleInfo);
	}
}

void mipc_gen_api_h(MIPC_MODULE_INFO_T* module_info)
{
	
}

int mipc_gen_internal_h(MIPC_MODULE_INFO_T* module_info)
{
	FILE* fd = NULL;
	char fileName[MIPC_NAME_MAX_LEN];
	int i, j;
	MIPC_API_INFO_T* pApi;
	
	sprintf(fileName, "%s_mipc_defs.h", module_info->name);
	fd = fopen(fileName, "w+");
	if (NULL == fd)
	{
		printf("cannot create file %s \n", fileName);
		return -1;
	}
	
	fprintf(fd, "/*******************************************************************************\n");
    fprintf(fd, "Copyright (C) Marvell International Ltd. and its affiliates\n\n");
	fprintf(fd, "This software file (the \"File\") is owned and distributed by Marvell\n");
	fprintf(fd, "International Ltd. and/or its affiliates (\"Marvell\") under the following\n");
	fprintf(fd, "licensing terms.\n\n");
	fprintf(fd, "********************************************************************************\n");
	fprintf(fd, "Marvell Commercial License Option\n\n");
	fprintf(fd, "If you received this File from Marvell and you have entered into a commercial\n");
	fprintf(fd, "license agreement (a \"Commercial License\") with Marvell, the File is licensed\n");
	fprintf(fd, "to you under the terms of the applicable Commercial License.\n\n");
	fprintf(fd, "*******************************************************************************/\n\n");

    fprintf(fd, "#ifndef _%s_h_\n", module_info->name);
    fprintf(fd, "#define _%s_h_\n", module_info->name);
	fprintf(fd, "#ifdef __cplusplus\n");
	fprintf(fd, "extern \"C\" {\n");
	fprintf(fd, "#endif\n\n");

	
	for (i = 0; i < module_info->numOfApi; i++)
	{
		// Define input data structure
		pApi = &module_info->api[i];
		if (pApi->numOfInputParas > 0)
		{
			fprintf(fd, "typedef struct {\n");
			for (j = 0; j < pApi->numOfParas; j++)
			{
				if (pApi->param[j].isIn & MIPC_PARA_IN)
				{
					if ((0 == strcasecmp("char", pApi->param[j].paraType)) && (pApi->param[j].isPointer))
					{
						fprintf(fd, "	char %s[%d];\n", pApi->param[j].paraName, MIPC_PARA_MAX_STR_SIZE);
					}
					else
					if ((0 == strcasecmp("void", pApi->param[j].paraType)) && (pApi->param[j].isPointer))
					{
						fprintf(fd, "	char %s[%d];\n", pApi->param[j].paraName, MIPC_PARA_MAX_SIZE);
					}
					else
					{
						fprintf(fd, "	%s %s;\n", pApi->param[j].paraType, pApi->param[j].paraName);
					}
				}
			}
			fprintf(fd, "} %s_IN_T;\n\n\n", pApi->apiName);
		}
		
		// Define output data structure
		pApi = &module_info->api[i];
		if ((pApi->numOfOutputParas > 0) || !pApi->retInfo.isVoid)
		{
			fprintf(fd, "typedef struct {\n");
			if (!pApi->retInfo.isVoid)
			{
				if ((0 == strcasecmp("char", pApi->retInfo.retType)) && (pApi->retInfo.isPointer))
				{
					fprintf(fd, "	char ret[%d];\n", MIPC_PARA_MAX_STR_SIZE);
				}
				else
				{
					fprintf(fd, "	%s ret;\n", pApi->retInfo.retType);
				}
			}
			for (j = 0; j < pApi->numOfParas; j++)
			{
				if (pApi->param[j].isIn & MIPC_PARA_OUT)
				{
					if ((0 == strcasecmp("char", pApi->param[j].paraType)) && (pApi->param[j].isPointer))
					{
						fprintf(fd, "	char %s[%d];\n", pApi->param[j].paraName, MIPC_PARA_MAX_STR_SIZE);
					}
					else
					if ((0 == strcasecmp("void", pApi->param[j].paraType)) && (pApi->param[j].isPointer))
					{
						fprintf(fd, "	char %s[%d];\n", pApi->param[j].paraName, MIPC_PARA_MAX_SIZE);
					}
					else
					{
						fprintf(fd, "	%s %s;\n", pApi->param[j].paraType, pApi->param[j].paraName);
					}
				}
			}
			
			fprintf(fd, "} %s_OUT_T;\n\n\n", pApi->apiName);
		}
	}

	fprintf(fd, "#ifdef __cplusplus\n");
	fprintf(fd, "}\n");
	fprintf(fd, "#endif\n");
	fprintf(fd, "#endif\n");

	fclose(fd);
}

void mipc_gen_client_function(FILE* fd, const char* module_name, unsigned int api_id, MIPC_API_INFO_T* pApi)
{
	int i;
	
	//Print function header
	if (pApi->retInfo.isVoid)
	{
		fprintf(fd, "void ");
	}
	else
	{
		fprintf(fd, "%s", pApi->retInfo.retType);
	}
	if (pApi->retInfo.isPointer)
	{
		fprintf(fd, "* ");
	}
	else
	{
		fprintf(fd, " ");
	}
	
	fprintf(fd, "%s(", pApi->apiName);
	if (pApi->numOfParas > 0)
	{
		for (i = 0; i < pApi->numOfParas; i++)
		{
			fprintf(fd, "%s", pApi->param[i].paraType);
			if (pApi->param[i].isPointer)
			{
				fprintf(fd, "* ");
			}
			else
			{
				fprintf(fd, " ");
			}
			fprintf(fd, "%s", pApi->param[i].paraName);
			if (i < pApi->numOfParas - 1)
			{
				fprintf(fd, ", ");
			}
		}
	}	
	else
	{
		fprintf(fd, "void");
	}
	fprintf(fd, ")\n");
	fprintf(fd, "{\n");
	if (pApi->numOfInputParas > 0)
	{
		fprintf(fd, "	%s_IN_T input;\n", pApi->apiName);
	}
	if (pApi->numOfOutputParas > 0 || (!pApi->retInfo.isVoid && !pApi->retInfo.isPointer) ||
	   (!pApi->retInfo.isVoid && pApi->retInfo.isPointer && (0 == strcasecmp("char", pApi->retInfo.retType))))
	{
		fprintf(fd, "	%s_OUT_T output;\n", pApi->apiName);
	}
	if (!pApi->retInfo.isVoid && pApi->retInfo.isPointer)
	{
		if (0 == strcasecmp("char", pApi->retInfo.retType))
		{
			fprintf(fd, "	static char ret[%d];\n", MIPC_PARA_MAX_STR_SIZE);
		}
		else
		{
			fprintf(fd, "	static %s ret;\n", pApi->retInfo.retType);
		}
	}
	fprintf(fd, "\n");
	
	if (pApi->numOfOutputParas > 0 || (!pApi->retInfo.isVoid && !pApi->retInfo.isPointer) ||
	   (!pApi->retInfo.isVoid && pApi->retInfo.isPointer && (0 == strcasecmp("char", pApi->retInfo.retType))))
	{
		fprintf(fd, "	memset(&output, 0, sizeof(output));\n");
		fprintf(fd, "\n");
	}
	
	if (pApi->numOfInputParas > 0)
	{
		for (i = 0; i < pApi->numOfParas; i++)
		{
			if (pApi->param[i].isIn & MIPC_PARA_IN)
			{
				if (!pApi->param[i].isPointer)
				{
					fprintf(fd, "	input.%s = %s;\n", pApi->param[i].paraName, pApi->param[i].paraName);
				}
				else
				{
					if (0 == strcasecmp("void", pApi->param[i].paraType))
					{
						//void* !!!!
						fprintf(fd, "   if (NULL != %s)\n", pApi->param[i].paraName);
						fprintf(fd, "	    memcpy(&input.%s, %s, %s);\n", pApi->param[i].paraName, pApi->param[i].paraName, pApi->param[i+1].paraName);
					}
					else
					if (0 == strcasecmp("char", pApi->param[i].paraType))
					{
						fprintf(fd, "   if (NULL != %s)\n", pApi->param[i].paraName);
						fprintf(fd, "	    strcpy(input.%s, %s);\n", pApi->param[i].paraName, pApi->param[i].paraName);
					}
					else
					{
					    fprintf(fd, "   if (NULL != %s)\n", pApi->param[i].paraName);
						fprintf(fd, "	    memcpy(&input.%s, %s, sizeof(%s));\n", pApi->param[i].paraName, pApi->param[i].paraName, pApi->param[i].paraType);
					}
				}
			}
		}
		fprintf(fd, "\n");
	}
	
	fprintf(fd, "	// Call message queue or socket to send out the parameters\n");
	if (pApi->syncType == 0)
	{
		if (pApi->numOfInputParas > 0)
		{
			fprintf(fd, "	if (MIPC_OK != mipc_send_async_msg(\"%s\", %d, &input, sizeof(input)))\n", module_name, api_id);
		}
		else
		{
			fprintf(fd, "	if (MIPC_OK != mipc_send_async_msg(\"%s\", %d, NULL, 0))\n", module_name, api_id);
		}
	}
	else if (pApi->syncType == 1)
	{
		if (pApi->numOfInputParas > 0)
		{
			if (pApi->numOfOutputParas > 0 || !pApi->retInfo.isVoid)
			{
				fprintf(fd, "	if (MIPC_OK != mipc_send_sync_msg(\"%s\", %d, &input, sizeof(input), &output, sizeof(output), %d))\n", module_name, api_id, pApi->timeout?pApi->timeout: -1);
			}
			else
			{
				fprintf(fd, "	if (MIPC_OK != mipc_send_sync_msg(\"%s\", %d, &input, sizeof(input), NULL, 0, %d))\n", module_name, api_id, pApi->timeout?pApi->timeout: -1);
			}
		}
		else
		{
			if (pApi->numOfOutputParas > 0 || !pApi->retInfo.isVoid)
			{
				fprintf(fd, "	if (MIPC_OK != mipc_send_sync_msg(\"%s\", %d, NULL, 0, &output, sizeof(output), %d))\n", module_name, api_id, pApi->timeout?pApi->timeout: -1);
			}
			else
			{
				fprintf(fd, "	if (MIPC_OK != mipc_send_sync_msg(\"%s\", %d, NULL, 0, NULL, 0, %d))\n", module_name, api_id, pApi->timeout?pApi->timeout: -1);
			}
		}	
	}
	else if (pApi->syncType == 2)
	{
		if (pApi->numOfInputParas > 0)
		{
			if (pApi->numOfOutputParas > 0 || !pApi->retInfo.isVoid)
			{
				fprintf(fd, "	if (MIPC_OK != mipc_send_cli_msg(\"%s\", %d, &input, sizeof(input), &output, sizeof(output), %d))\n", module_name, api_id, pApi->timeout?pApi->timeout: -1);
			}
			else
			{
				fprintf(fd, "	if (MIPC_OK != mipc_send_cli_msg(\"%s\", %d, &input, sizeof(input), NULL, 0, %d))\n", module_name, api_id, pApi->timeout?pApi->timeout: -1);
			}
		}
		else
		{
			if (pApi->numOfOutputParas > 0 || !pApi->retInfo.isVoid)
			{
				fprintf(fd, "	if (MIPC_OK != mipc_send_cli_msg(\"%s\", %d, NULL, 0, &output, sizeof(output), %d))\n", module_name, api_id, pApi->timeout?pApi->timeout: -1);
			}
			else
			{
				fprintf(fd, "	if (MIPC_OK != mipc_send_cli_msg(\"%s\", %d, NULL, 0, NULL, 0, %d))\n", module_name, api_id, pApi->timeout?pApi->timeout: -1);
			}
		}	
	}
	fprintf(fd, "	{\n");
	fprintf(fd, "		printf(\"%%s: failed to send message\\n\", __FUNCTION__);\n");
	fprintf(fd, "	}\n");
	
	if (pApi->numOfOutputParas > 0)
	{
		for (i = 0; i < pApi->numOfParas; i++)
		{
			if (pApi->param[i].isIn & MIPC_PARA_OUT)
			{
				if (!pApi->param[i].isPointer)
				{
					fprintf(fd, "	%s = output.%s;\n", pApi->param[i].paraName, pApi->param[i].paraName);
				}
				else
				{
					if (0 == strcasecmp("void", pApi->param[i].paraType))
					{
						//void* !!!!
						fprintf(fd, "	memcpy(%s, &output.%s, %s);\n", pApi->param[i].paraName, pApi->param[i].paraName, pApi->param[i+1].paraName);
					}
					else if (0 == strcasecmp("char", pApi->param[i].paraType))
					{
						fprintf(fd, "	if (NULL != %s)\n", pApi->param[i].paraName);
						fprintf(fd, "	    strcpy(%s, output.%s);\n", pApi->param[i].paraName, pApi->param[i].paraName);
					}
					else
					{
						fprintf(fd, "	memcpy(%s, &output.%s, sizeof(%s));\n", pApi->param[i].paraName, pApi->param[i].paraName, pApi->param[i].paraType);
					}
				}
			}
		}
		fprintf(fd, "\n");
	}
	if (!pApi->retInfo.isVoid)
	{
		if (pApi->retInfo.isPointer)
		{
			if (0 == strcasecmp("char", pApi->retInfo.retType))
			{
				fprintf(fd, "	strcpy(ret, output.ret);\n");
				fprintf(fd, "	return ret;\n");
			}
			else
			{
			    fprintf(fd, "	memcpy(&ret, &output.ret, sizeof(%s));\n", pApi->retInfo.retType);
				fprintf(fd, "	return &ret;\n");
			}
		}
		else
		{
			fprintf(fd, "	return output.ret;\n");
		}
	}
	
	fprintf(fd, "}\n\n\n");
}

int mipc_gen_client_c(MIPC_MODULE_INFO_T* module_info)
{
	FILE* fd = NULL;
	char fileName[MIPC_NAME_MAX_LEN];
	int i;
	
	sprintf(fileName, "%s_mipc_client.c", module_info->name);
	fd = fopen(fileName, "w+");
	if (NULL == fd)
	{
		printf("cannot create file %s \n", fileName);
		return -1;
	}
	
	fprintf(fd, "/*******************************************************************************\n");
    fprintf(fd, "Copyright (C) Marvell International Ltd. and its affiliates\n\n");
	fprintf(fd, "This software file (the \"File\") is owned and distributed by Marvell\n");
	fprintf(fd, "International Ltd. and/or its affiliates (\"Marvell\") under the following\n");
	fprintf(fd, "licensing terms.\n\n");
	fprintf(fd, "********************************************************************************\n");
	fprintf(fd, "Marvell Commercial License Option\n\n");
	fprintf(fd, "If you received this File from Marvell and you have entered into a commercial\n");
	fprintf(fd, "license agreement (a \"Commercial License\") with Marvell, the File is licensed\n");
	fprintf(fd, "to you under the terms of the applicable Commercial License.\n\n");
	fprintf(fd, "*******************************************************************************/\n\n");

	fprintf(fd, "#include <stdlib.h>\n");
	fprintf(fd, "#include <stdio.h>\n");
	fprintf(fd, "#include <string.h>\n");
	fprintf(fd, "#include <stdbool.h>\n");
	fprintf(fd, "#include \"mipc.h\"\n\n");
	for (i = 0; i < module_info->numOfIncludes; i++)
	{
		fprintf(fd, "%s\n", module_info->includes[i]);
	}
	fprintf(fd, "#include \"%s_mipc_defs.h\"\n\n", module_info->name);
	fprintf(fd, "\n");
	for (i = 0; i < module_info->numOfApi; i++)
	{
		mipc_gen_client_function(fd, module_info->name, i, &module_info->api[i]);
	}
	
	fclose(fd);
}


void mipc_gen_server_function(FILE* fd, const char* src_module, unsigned int api_id, MIPC_API_INFO_T* pApi)
{
	int i;
	
	if (pApi->numOfInputParas > 0)
	{
		fprintf(fd, "			%s_IN_T* in = (%s_IN_T*)input;\n", pApi->apiName, pApi->apiName);
	}
	if ((pApi->numOfOutputParas > 0) || (!pApi->retInfo.isVoid && !pApi->retInfo.isPointer) 
	   || (!pApi->retInfo.isVoid && pApi->retInfo.isPointer && (0 == strcasecmp("char", pApi->retInfo.retType))))
	{
		fprintf(fd, "			%s_OUT_T out;\n", pApi->apiName);
	}
	if (!pApi->retInfo.isVoid && pApi->retInfo.isPointer)
	{
		fprintf(fd, "			%s* ret;\n", pApi->retInfo.retType);
	}
	
	fprintf(fd, "\n");
	
	if (!pApi->retInfo.isVoid && pApi->retInfo.isPointer)
	{
		fprintf(fd, "			ret = ");
	}
	else
	if (!pApi->retInfo.isVoid && !pApi->retInfo.isPointer)
	{
		fprintf(fd, "			out.ret = ");
	}
	else
	{
		fprintf(fd, "			");
	}
	
	fprintf(fd, "%s(", pApi->apiName);

	for (i = 0; i < pApi->numOfParas; i++)
	{
		// the first para of CLI APIs
		if ((0 == i) && (pApi->syncType == 2))
		{
			fprintf(fd, "(void*)source_module");
		}
		else
		{
			if (pApi->param[i].isIn & MIPC_PARA_IN)
			{
				if (!pApi->param[i].isPointer)
				{
					fprintf(fd, "in->%s", pApi->param[i].paraName);
				}
				else
				{
					if (0 == strcasecmp("void", pApi->param[i].paraType))
					{
						//void* !!!!
						fprintf(fd, "(void*)&in->%s", pApi->param[i].paraName);
					}
					else
					if (0 == strcasecmp("char", pApi->param[i].paraType))
					{
						fprintf(fd, "in->%s", pApi->param[i].paraName);
					}
					else
					{
						fprintf(fd, "&in->%s", pApi->param[i].paraName);
					}
				}
			}
			else
			if (pApi->param[i].isIn & MIPC_PARA_OUT)
			{
				if (!pApi->param[i].isPointer)
				{
					fprintf(fd, "out.%s", pApi->param[i].paraName);
				}
				else
				{
					if (0 == strcasecmp("void", pApi->param[i].paraType))
					{
						//void* !!!!
						fprintf(fd, "(void*)&out.%s", pApi->param[i].paraName);
					}
					else
					if (0 == strcasecmp("char", pApi->param[i].paraType))
					{
						fprintf(fd, "out.%s", pApi->param[i].paraName);
					}
					else
					{
						fprintf(fd, "&out.%s", pApi->param[i].paraName);
					}
				}
			}
		}
		if (i < pApi->numOfParas - 1)
		{
			fprintf(fd, ", ");
		}
	}
	
	fprintf(fd, ");\n");
	fprintf(fd, "\n");
	
	if (!pApi->retInfo.isVoid && pApi->retInfo.isPointer)
	{
		if (0 == strcasecmp("char", pApi->retInfo.retType))
		{
			fprintf(fd, "			strcpy(out.ret, ret);\n");
		}
		else
		{
			fprintf(fd, "			memcpy(&out.ret, ret, sizeof(%s));\n", pApi->retInfo.retType);
		}
	}
	for (i = 0; i < pApi->numOfParas; i++)
	{
		if ((pApi->param[i].isIn & MIPC_PARA_IN) && (pApi->param[i].isIn & MIPC_PARA_OUT))
		{
			if (!pApi->param[i].isPointer)
			{
				//fprintf(fd, out.%s", pApi->param[i].paraName);
			}
			else
			{
				if (0 == strcasecmp("void", pApi->param[i].paraType))
				{
					//void* !!!!
					fprintf(fd, "			memcpy(&out.%s, &in->%s, in->%s);\n", pApi->param[i].paraName, pApi->param[i].paraName, pApi->param[i+1].paraName);
				}
				else
				if (0 == strcasecmp("char", pApi->param[i].paraType))
				{
					fprintf(fd, "			strcpy(out.%s, in->%s);\n", pApi->param[i].paraName, pApi->param[i].paraName);
				}
				else
				{
					fprintf(fd, "			memcpy(&out.%s, &in->%s, sizeof(%s));\n", pApi->param[i].paraName, pApi->param[i].paraName, pApi->param[i].paraType);
				}
			}
		}
	}
	
	fprintf(fd, "\n");
	if (pApi->syncType == 2)
	{
		fprintf(fd, "			// send end of print flag\n");
		fprintf(fd, "			if (MIPC_OK != mipc_response_msg(source_module, 0xFFFE, NULL, 0))\n", api_id);
		fprintf(fd, "			{\n");
		fprintf(fd, "				printf(\"%%s: failed to send response message\\n\", __FUNCTION__);\n");
		fprintf(fd, "			}\n");

	}
	
	if (pApi->syncType != 0)
	{
		if ((pApi->numOfOutputParas > 0) || !pApi->retInfo.isVoid)
		{
			fprintf(fd, "			// send back everyting in out data structure\n");
			fprintf(fd, "			if (MIPC_OK != mipc_response_msg(source_module, %d, &out, sizeof(out)))\n", api_id);
			fprintf(fd, "			{\n");
			fprintf(fd, "				printf(\"%%s: failed to send response message\\n\", __FUNCTION__);\n");
			fprintf(fd, "			}\n");
		}
	}
}


int mipc_gen_server_c(MIPC_MODULE_INFO_T* module_info)
{
	FILE* fd = NULL;
	char fileName[MIPC_NAME_MAX_LEN];
	int i;
	
	sprintf(fileName, "%s_mipc_server.c", module_info->name);
	fd = fopen(fileName, "w+");
	if (NULL == fd)
	{
		printf("cannot create file %s \n", fileName);
		return -1;
	}
	
	fprintf(fd, "/*******************************************************************************\n");
    fprintf(fd, "Copyright (C) Marvell International Ltd. and its affiliates\n\n");
	fprintf(fd, "This software file (the \"File\") is owned and distributed by Marvell\n");
	fprintf(fd, "International Ltd. and/or its affiliates (\"Marvell\") under the following\n");
	fprintf(fd, "licensing terms.\n\n");
	fprintf(fd, "********************************************************************************\n");
	fprintf(fd, "Marvell Commercial License Option\n\n");
	fprintf(fd, "If you received this File from Marvell and you have entered into a commercial\n");
	fprintf(fd, "license agreement (a \"Commercial License\") with Marvell, the File is licensed\n");
	fprintf(fd, "to you under the terms of the applicable Commercial License.\n\n");
	fprintf(fd, "*******************************************************************************/\n\n");
	
	fprintf(fd, "#include <stdlib.h>\n");
	fprintf(fd, "#include <stdio.h>\n");
	fprintf(fd, "#include <string.h>\n");
	fprintf(fd, "#include <stdbool.h>\n");
	fprintf(fd, "#include \"mipc.h\"\n\n");
	for (i = 0; i < module_info->numOfIncludes; i++)
	{
		fprintf(fd, "%s\n", module_info->includes[i]);
	}
	fprintf(fd, "#include \"%s_mipc_defs.h\"\n\n", module_info->name);
	fprintf(fd, "\n");
	fprintf(fd, "void %s_mipc_server_call(char* source_module, int api_id, char* input)\n", module_info->name);
	fprintf(fd, "{\n");
	fprintf(fd, "	switch (api_id)\n");
	fprintf(fd, "	{\n");
	for (i = 0; i < module_info->numOfApi; i++)
	{
	fprintf(fd, "		case %d:\n", i);
	fprintf(fd, "		{\n");
	mipc_gen_server_function(fd, module_info->name, i, &module_info->api[i]);
	fprintf(fd, "			break;\n");
	fprintf(fd, "		}\n\n");
	}
	fprintf(fd, "		default:\n");
	fprintf(fd, "			break;\n");
	fprintf(fd, "	}\n");
	fprintf(fd, "}\n\n\n");
	
	fprintf(fd, "void %s_mipc_server_handler(char* inMsg, unsigned int size)\n", module_info->name);
	fprintf(fd, "{\n");
	fprintf(fd, "	char source_module[MIPC_NAME_MAX_LEN];\n");
	fprintf(fd, "	unsigned int api_id;\n");
	fprintf(fd, "	\n");
	fprintf(fd, "	if (MIPC_ERROR != mipc_receive_msg(source_module, &api_id, inMsg, size))\n");
	fprintf(fd, "		{\n");
	fprintf(fd, "			// Call APIs\n");
	fprintf(fd, "		%s_mipc_server_call(source_module, api_id, inMsg + sizeof(MIPC_MSG_HDR_T));\n", module_info->name);
	fprintf(fd, "	}\n");
	fprintf(fd, "}\n\n\n");
	
	fclose(fd);
}


int main(int argc, char** argv)
{
	FILE* fd = NULL;
	int i = 0;

	if (argc != 2)
	{
		printf("Input script file name missed!\n");
		return -1;
	}

	fd = fopen(argv[1], "r");
	if (NULL == fd)
	{
		printf("cannot open file %s \n", argv[1]);
		return -1;
	}
	printf("Processing script %s ...\n", argv[1]);
	if (mipc_get_module_info(fd, &gMipcModuleInfo) < 0)
	{
	    fclose(fd);
		printf("\n Failed\n");
		return -1;
	}
	
	printf("Succeed\n");
	
	//mipc_print_module_info(&gMipcModuleInfo);

	mipc_gen_api_h(&gMipcModuleInfo);
	mipc_gen_internal_h(&gMipcModuleInfo);
	mipc_gen_client_c(&gMipcModuleInfo);
	mipc_gen_server_c(&gMipcModuleInfo);
	
	return 0;	
}
