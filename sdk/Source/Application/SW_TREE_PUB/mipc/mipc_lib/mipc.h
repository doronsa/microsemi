/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#ifndef _MIPC_H_
#define _MIPC_H_

#ifdef __cplusplus
extern "C" {
#endif
 
#define MIPC_DEFAULT_MAX_MSG_SIZE (4096)
#define MIPC_NAME_MAX_LEN         (64)
#define MIPC_WAIT_FOREVER         (-1)
#define MIPC_OK                   (0)
#define MIPC_ERROR                (-1) // must be less than 0



typedef struct
{
	unsigned int magic;
	char         src_name[MIPC_NAME_MAX_LEN];
	unsigned int msg_type; //0xFFFF: printing, 0xFFFE: end of printing
	unsigned int msg_size;
} MIPC_MSG_HDR_T;

/*******************************************************************************
* mipc_init
*
* DESCRIPTION:      Init MIPC mechanism
*
* INPUTS:
* name           - thread name
* maxNumOfMsg    - max number of messages hold by message queue
* maxSizeOfMsg   - max size of each single message, should be less than MIPC_DEFAULT_MAX_MSG_SIZE(4096)
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
int mipc_init(const char* name, unsigned int maxNumOfMsg, unsigned int maxSizeOfMsg);

/*******************************************************************************
* mipc_init
*
* DESCRIPTION:      Deinit MIPC mechanism, release resources
*
* INPUTS:
*  None.
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
int mipc_deinit(void);

/*******************************************************************************
* mipc_send_sync_msg
*
* DESCRIPTION:      Send synchronous message
*
* INPUTS:
*  name: destination thread name
*  msgType: message type or ID
*  inMsg: the message body to be sent
*  inSize: the size of message to be sent
*  outSize: the expected size of returned message
*  timeout: the max time to wait in case the destination message queue is full, -1 for waiting for ever
* OUTPUTS:
*  outMsg: the return message body
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
int mipc_send_sync_msg(const char* name, int msgType, void* inMsg, unsigned int inSize, void* outMsg, unsigned int outSize, int timeout);

/*******************************************************************************
* mipc_send_async_msg
*
* DESCRIPTION:      Send asynchronous message
*
* INPUTS:
*  name: destination thread name
*  msgType: message type or ID
*  inMsg: the message body to be sent
*  inSize: the size of message to be sent
* OUTPUTS:
*  None.
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
int mipc_send_async_msg(const char* name, int msgType, void* inMsg, unsigned int inSize);

/*******************************************************************************
* mipc_send_cli_msg
*
* DESCRIPTION:      Send CLI command message to a thread from CLISH
*
* INPUTS:
*  name: destination thread name
*  msgType: message type or ID
*  inMsg: the message body to be sent
*  inSize: the size of message to be sent
*  outSize: the expected size of returned message
*  timeout: the max time to wait in case the destination message queue is full, -1 for waiting for ever
* OUTPUTS:
*  outMsg: the return message body
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
int mipc_send_cli_msg(char* name, int msgType, void* inMsg, unsigned int inSize, void* outMsg, unsigned int outSize, int timeout);

/*******************************************************************************
* mipc_response_msg
*
* DESCRIPTION:      Response a message
*
* INPUTS:
*  name: destination thread name
*  msgType: message type or ID
*  inMsg: the message body to be sent
*  inSize: the size of message to be sent
* OUTPUTS:
*  None.
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
int mipc_response_msg(const char* name, int msgType, void* inMsg, unsigned int inSize);

/*******************************************************************************
* mipc_receive_msg
*
* DESCRIPTION:      Handle a message
*
* INPUTS:
*  inMsg: the message body received
*  inSize: the size of message received
* OUTPUTS:
*  src_name: the source thread name
*  msgType: message type or ID
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
int mipc_receive_msg(char* src_name, unsigned int* msgType, char* inMsg, unsigned int size);


/*******************************************************************************
* mipc_printf
*
* DESCRIPTION:      Send printing strings to remote CLISH
*
* INPUTS:
*  source_name: the remote CLISH thread name
*  format: printing format
* OUTPUTS:
*  None.
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
void mipc_printf(char* source_name, const char* format, ...);

/*******************************************************************************
* mipc_ping
*
* DESCRIPTION:      Ping a remote thread by MIPC message
*
* INPUTS:
*  name: the remote thread name
*  timeout: the time to wait, -1 for waiting for ever
* OUTPUTS:
*  None.
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
int mipc_ping(const char* name, int timeout);


/*******************************************************************************
* mipc_msg_default_handler
*
* DESCRIPTION:      The default message handler 
*
* INPUTS:
*  msg: the received message body
*  size: the received message size
*  pri: the received message priority
* OUTPUTS:
*  None.
* RETURNS:
* On success, the function returns 0
* On error different types are returned according to the case
*
* COMMENTS:
*
*******************************************************************************/
int mipc_msg_default_handler(char* msg, unsigned int size, unsigned int pri);

#ifdef __cplusplus
}
#endif
#endif

