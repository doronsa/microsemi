/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/sem.h>
#include <mqueue.h>
#include <stdarg.h>
#include "mipc.h"

#define MIPC_MSG_MAGIC (0xFF55EE88)

#define MIPC_MSG_TYPE_PRINT     (0xFFFF)
#define MIPC_MSG_TYPE_PRINT_END (0xFFFE)
#define MIPC_MSG_TYPE_PING      (0xFFFD)
#define MIPC_SERVER_THREAD_NUM   (8)
#define MIPC_DEFAULT_MAX_MSG_NUM (8)
#define MIPC_MAX_MSG_NUM_OF_RESP_Q (10)
#define MIPC_INVALID_MQ            (-1)   // must be -1, cannot be changed to other values
#define MIPC_OS_ERROR              (-1)   // must be -1, cannot be changed to other values
#define MIPC_TRUE                  (1)
#define MIPC_FALSE                 (0)

typedef struct
{
    int flag_is_hold;
    pthread_t thread_id;
	const char* name;
	int mqd;
	int resp_mqd;
	int prev_opened_mqd;
	char prev_opened_mq_name[MIPC_NAME_MAX_LEN];

} MIPC_THREAD_INFO_T;



static MIPC_THREAD_INFO_T gMipcThreadInfo[MIPC_SERVER_THREAD_NUM];
static int gMipcSemId = MIPC_ERROR;
static int gMipcInited = MIPC_FALSE;

int mipc_create_sem(void)
    {
	int semId;
    	
    	semId = semget(IPC_PRIVATE, 1, IPC_CREAT | S_IRUSR | S_IWUSR );
    	if (semId == MIPC_OS_ERROR)
    	{
		printf("mipc_create_sem: failed to create semphore(%s)!\n", strerror(errno));
    		return MIPC_ERROR;
    	}
    	if (semctl(semId, 0, SETVAL, 1) == MIPC_OS_ERROR)        
    	{        
		printf("mipc_create_sem: failed to init semphore(%s)!\n", strerror(errno));
    		return MIPC_ERROR;
    	}
	return semId;
    }

int mipc_take_sem(int semId)
{
	struct sembuf sem_op; 

    sem_op.sem_num = 0;     
    sem_op.sem_op  = -1;     
    sem_op.sem_flg = 0; 
    if (MIPC_OS_ERROR == semop(semId, &sem_op, 1))         
    {          
    	printf("mipc_take_sem: failed to take semphore(%s)!\n", strerror(errno));
    	return MIPC_ERROR;      
    }
    return MIPC_OK;
}

int mipc_give_sem(int semId)
    {
	struct sembuf sem_op; 

    sem_op.sem_num = 0;     
    sem_op.sem_op  = 1;     
    sem_op.sem_flg = 0; 
    if (MIPC_OS_ERROR == semop(semId, &sem_op, 1))         
    {          
    	printf("mipc_give_sem: failed to give semphore(%s)!\n", strerror(errno));
    	return MIPC_ERROR;      
    }
    
    return MIPC_OK;
}

int mipc_pre_init(void)
{
	gMipcSemId = mipc_create_sem();
	if (MIPC_ERROR == gMipcSemId)
	{
		return MIPC_ERROR;
	}

	memset(gMipcThreadInfo, 0, sizeof(MIPC_THREAD_INFO_T)* MIPC_SERVER_THREAD_NUM);
	return MIPC_OK;
}


int mipc_get_free_thread_info_index(void)
{
    int           i = 0;

    /* Take semphore */
	mipc_take_sem(gMipcSemId);

    for(i = 0;i < MIPC_SERVER_THREAD_NUM;i++)
    {
        if(MIPC_FALSE == gMipcThreadInfo[i].flag_is_hold)
        {
            break;
        }
    }

    /* Give semphore */
	mipc_give_sem(gMipcSemId);
    
    if (i == MIPC_SERVER_THREAD_NUM)
    {
    	return MIPC_ERROR;
}
    else
    {
    	return i;
    }
}

int mipc_find_thread_info_index(void)
{
    int i = 0;
    pthread_t thread_id = pthread_self();

    for(i = 0;i < MIPC_SERVER_THREAD_NUM;i++)
    {
        if((MIPC_TRUE == gMipcThreadInfo[i].flag_is_hold) && (thread_id == gMipcThreadInfo[i].thread_id))
        {
            return i;
        }
    }
    /*for no mipc_init thread, use main thread info*/
    return 0;
}


int mipc_mq_create(const char* name, unsigned int maxNumOfMsg, unsigned int maxSizeOfMsg)
{
	mqd_t  mqd;
	int    oflag = O_RDWR | O_CREAT | O_EXCL;
	struct mq_attr attr;


	// Fill in attributes for message queue
	attr.mq_maxmsg  = maxNumOfMsg;
	attr.mq_msgsize = maxSizeOfMsg;
	attr.mq_flags   = 0;

	mqd = mq_open(name, oflag, S_IRUSR | S_IWUSR, &attr);
	if (mqd == (mqd_t)MIPC_OS_ERROR)
	{
		if (errno == EEXIST)
		{
			mq_unlink(name);
			mqd = mq_open(name, oflag, S_IRUSR | S_IWUSR, &attr);
			if (mqd == (mqd_t)MIPC_OS_ERROR)
			{
				printf("mipc_mq_create failed to open message queue %s, errno=%d!\n", name, errno);
				return MIPC_INVALID_MQ;
			}
		}
		else
		{
			printf("mipc_mq_create failed to open message queue %s, errno=%d\n!!", name, errno);
			return MIPC_INVALID_MQ;
		}
	}

	return (int)mqd;
}

int mipc_mq_open(const char* name)
{
	mqd_t  mqd;
    int index = mipc_find_thread_info_index();

#if 0
	// If it is opened, then return directly
	if ((0 == strcmp(name, gMipcThreadInfo[index].prev_opened_mq_name)) &&
	   (-1 != gMipcThreadInfo[index].prev_opened_mqd))
	{
		return gMipcThreadInfo[index].prev_opened_mqd;
	}
#endif

	// Close the previous mqd
	if (MIPC_INVALID_MQ != gMipcThreadInfo[index].prev_opened_mqd)
	{
		mipc_mq_close(gMipcThreadInfo[index].prev_opened_mqd);
	}

	// Open the new mqd
	mqd = mq_open(name, O_RDWR);
	if (mqd == (mqd_t)MIPC_OS_ERROR)
	{
		//printf("mq_open failed to open message queue %s, errno=%d\n", name, errno);
		return MIPC_INVALID_MQ;
	}

	// Recode the new mqd
	gMipcThreadInfo[index].prev_opened_mqd = mqd;
	strcpy(gMipcThreadInfo[index].prev_opened_mq_name, name);

	return (int)mqd;
}

int mipc_mq_close(int mqd)
{
    if (MIPC_OS_ERROR == mq_close((mqd_t)mqd))
    {
        //printf("mq_close failed, mqd = %d, errno=%d\n, pid=%d, %s", mqd, errno, getpid(), gMipcThreadInfo[mipc_find_thread_info_index()].name);
		return MIPC_ERROR;
    }
    return MIPC_OK;
}

int mipc_mq_delete(const char* name)
{
    if (MIPC_OS_ERROR == mq_unlink(name))
    {
        printf("mq_unlink failed, mq = %s, errno=%d\n", name, errno);
		return MIPC_ERROR;
    }
    return MIPC_OK;
}

int mipc_mq_send(int mqd, void* message, unsigned int size, unsigned int priority, int timeout)
{
	struct timespec ts;

	if (MIPC_WAIT_FOREVER == timeout)
	{
	    if (MIPC_OS_ERROR != mq_send((mqd_t)mqd, message, size, priority))
	    {
			return MIPC_OK;
	    }
    }
    else
    {
    	clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_sec += timeout/1000;
		ts.tv_nsec += timeout%1000 * 1000 * 1000;
		if (MIPC_OS_ERROR != mq_timedsend((mqd_t)mqd, message, size, priority, &ts))
		{
			return MIPC_OK;
		}
    }
    printf("mipc_mq_send failed, mqd = %d, errno=%d\n", mqd, errno);
    return MIPC_ERROR;
}

int mipc_mq_clear(int mqd)
{
	struct timespec ts;
	char temp[MIPC_DEFAULT_MAX_MSG_SIZE];
	int rcvCount = MIPC_OS_ERROR;

	ts.tv_sec = 0;
	ts.tv_nsec = 0;

	do
	{
		rcvCount = mq_timedreceive((mqd_t)mqd, temp, MIPC_DEFAULT_MAX_MSG_SIZE, NULL, &ts);

		if (MIPC_OS_ERROR == rcvCount)
		{
			if (EINTR == errno)
			{
				continue;
			}
			// For other errors, should return
			return MIPC_OK;
		}
	} while (1);
	return MIPC_OK;
}

int mipc_mq_receive(int mqd, void* message, unsigned int size, unsigned int  *msgPriority, int timeout)
{
    int rcvCount = MIPC_OS_ERROR;
	struct timespec ts;

	if (MIPC_WAIT_FOREVER == timeout)
	{
		do
		{
			rcvCount = mq_receive((mqd_t)mqd, message, size, msgPriority);
			if (MIPC_OS_ERROR != rcvCount)
			{
				return MIPC_OK;
			}
			if (EINTR == errno)
			{
				continue;
			}
			else
			{
				break;
			}
		} while (1);
	}
    else
    {
    	clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_sec += timeout/1000;
		ts.tv_nsec += timeout%1000 * 1000 * 1000;

		do
		{
			rcvCount = mq_timedreceive((mqd_t)mqd, message, size, msgPriority, &ts);
			if (MIPC_OS_ERROR != rcvCount)
			{
				return MIPC_OK;
			}
			if (EINTR == errno)
			{
				// Have no idea how long waited, so re-wait with original time
				continue;
			}
			else
			{
				break;
			}
		} while (1);
    }

	printf("mipc_mq_receive failed, mqd = %d, errno=%d\n", mqd, errno);
	return MIPC_OK;
}

void mipc_vprintf(char* str)
{
    int  pos_of_buf1 = 0, pos_of_buf2 = 0, len = 0;
    char buf[MIPC_DEFAULT_MAX_MSG_SIZE];

    len = strlen(str);

    memset(buf, 0, MIPC_DEFAULT_MAX_MSG_SIZE);

    str[MIPC_DEFAULT_MAX_MSG_SIZE - 1]='\0';

    for(pos_of_buf1 = 0;pos_of_buf1 < len; pos_of_buf1++)
    {
        if(str[pos_of_buf1]=='\r')
        {
            if('\0'!=str[pos_of_buf1+1] && '\n'!=str[pos_of_buf1+1])
            {
                buf[pos_of_buf2++] = str[pos_of_buf1];
                buf[pos_of_buf2++] = '\n';
            }
            else
            {
                buf[pos_of_buf2++] = str[pos_of_buf1];
            }

        }
        else if(str[pos_of_buf1]=='\n')
        {
            if(0==pos_of_buf1 || '\r'!=str[pos_of_buf1-1])
            {
                buf[pos_of_buf2++] = '\r';
                buf[pos_of_buf2++] = str[pos_of_buf1];
            }
            else
            {
                buf[pos_of_buf2++]=str[pos_of_buf1];
            }
        }
        else
        {
            buf[pos_of_buf2++]=str[pos_of_buf1];
        }
    }

    printf(buf);
}

int mipc_init(const char* name, unsigned int maxNumOfMsg, unsigned int maxSizeOfMsg)
{
	char msq_name[MIPC_NAME_MAX_LEN];
    int index;
	pthread_t self = pthread_self();

	if (MIPC_FALSE == gMipcInited)
	{
		mipc_pre_init();
		gMipcInited = MIPC_TRUE;
	}

    index = mipc_get_free_thread_info_index();
    if(MIPC_ERROR == index)
    {
        printf("Can not create any more mipc thread for name %s!!\n",name);
        return MIPC_ERROR;
    }

	// the length of string "/$name_resp" should be less than MIPC_NAME_MAX_LEN - 1
    if (strlen(name)+ strlen("/_resp") + 1 > MIPC_NAME_MAX_LEN)
    {
        printf("Thread name %s is too long!!\n",name);
        return MIPC_ERROR;
    }

    //printf(" create mipc thread for name %s index is %d!!\n",name,index);

    gMipcThreadInfo[index].flag_is_hold = MIPC_TRUE;
    gMipcThreadInfo[index].thread_id = self;
	gMipcThreadInfo[index].name = name;

	if (0 == maxNumOfMsg)
	{
		maxNumOfMsg = MIPC_DEFAULT_MAX_MSG_NUM;
	}
	if (0 == maxSizeOfMsg)
	{
		maxSizeOfMsg = MIPC_DEFAULT_MAX_MSG_SIZE;
	}

	sprintf(msq_name, "/%s", name);
	gMipcThreadInfo[index].mqd = mipc_mq_create(msq_name, maxNumOfMsg, maxSizeOfMsg);
	if (MIPC_INVALID_MQ == gMipcThreadInfo[index].mqd)
	{
	    memset(&(gMipcThreadInfo[index]), 0, sizeof(MIPC_THREAD_INFO_T));
		return MIPC_ERROR;
	}

	sprintf(msq_name, "/%s_resp", name);
	gMipcThreadInfo[index].resp_mqd = mipc_mq_create(msq_name, MIPC_MAX_MSG_NUM_OF_RESP_Q, maxSizeOfMsg);
	if (MIPC_INVALID_MQ == gMipcThreadInfo[index].resp_mqd)
	{
	    mipc_mq_close(gMipcThreadInfo[index].mqd);
        mipc_mq_delete(msq_name);
        memset(&(gMipcThreadInfo[index]), 0, sizeof(MIPC_THREAD_INFO_T));
		return MIPC_ERROR;
	}

	gMipcThreadInfo[index].prev_opened_mqd = MIPC_INVALID_MQ;
	memset(gMipcThreadInfo[index].prev_opened_mq_name, '\0', sizeof(gMipcThreadInfo[index].prev_opened_mq_name));

	return gMipcThreadInfo[index].mqd;  
}

int mipc_deinit(void)
{
	char msq_name[MIPC_NAME_MAX_LEN];
    int index = mipc_find_thread_info_index();

	sprintf(msq_name, "/%s", gMipcThreadInfo[index].name);
	if (MIPC_INVALID_MQ != gMipcThreadInfo[index].mqd)
	{
		mipc_mq_close(gMipcThreadInfo[index].mqd);
	}
	mipc_mq_delete(msq_name);

	sprintf(msq_name, "/%s_resp", gMipcThreadInfo[index].name);
	if (MIPC_INVALID_MQ != gMipcThreadInfo[index].resp_mqd)
	{
		mipc_mq_close(gMipcThreadInfo[index].resp_mqd);
	}
	mipc_mq_delete(msq_name);

	if (MIPC_INVALID_MQ != gMipcThreadInfo[index].prev_opened_mqd)
	{
		mipc_mq_close(gMipcThreadInfo[index].prev_opened_mqd);
	}

    memset(&(gMipcThreadInfo[index]),0,sizeof(MIPC_THREAD_INFO_T));
	return MIPC_OK;
}

int mipc_clear_resp_msq(void)
{
	int index = mipc_find_thread_info_index();

	return mipc_mq_clear(gMipcThreadInfo[index].resp_mqd);
}

int mipc_send_msg(const char* name, int msgType, void* inMsg, unsigned int inSize, int timeout)
{
	int mqd, size;
	MIPC_MSG_HDR_T* hdr = NULL;
	char msq_name[MIPC_NAME_MAX_LEN];
	char temp[MIPC_DEFAULT_MAX_MSG_SIZE];
    int index = mipc_find_thread_info_index();

	sprintf(msq_name, "/%s", name);
	mqd = mipc_mq_open(msq_name);
	if (mqd == MIPC_INVALID_MQ)
	{
		//printf("mipc_send_msg: failed to open message queue %s by %s\n", msq_name, gMipcProcessInfo.name);
		return MIPC_ERROR;
	}

	hdr = (MIPC_MSG_HDR_T*)temp;
	hdr->magic = MIPC_MSG_MAGIC;
	strcpy(hdr->src_name, gMipcThreadInfo[index].name);
	hdr->msg_type = msgType,
	hdr->msg_size = inSize;
	size = sizeof(MIPC_MSG_HDR_T);

	if (size + inSize > MIPC_DEFAULT_MAX_MSG_SIZE)
	{
		printf("mipc_send_msg: message too big from %s to %s\n", hdr->src_name, name);
		return MIPC_ERROR;
	}

	if (inSize > 0)
	{
		memcpy(temp + sizeof(MIPC_MSG_HDR_T), (char*)inMsg, inSize);
		size += inSize;
	}
	if (MIPC_ERROR == mipc_mq_send(mqd, hdr, size, 0, timeout))
	{
		printf("mipc_send_msg: failed to send message header to message queue %s from %s\n", name, hdr->src_name);
		return MIPC_ERROR;
	}

	return MIPC_OK;
}

int mipc_send_sync_msg(const char* name, int msgType, void* inMsg, unsigned int inSize, void* outMsg, unsigned int outSize, int timeout)
{
	// Check the thread invoked mipc_init or not, if not, issue a warning
	int index = mipc_find_thread_info_index();
	if (gMipcThreadInfo[index].thread_id != pthread_self())
	{
		printf("mipc_send_sync_msg: somebody from process %s is sending sync message to %s without invoking mipc_init!!!!\n", gMipcThreadInfo[index].name, name);
	}

	mipc_clear_resp_msq();

	if (MIPC_ERROR == mipc_send_msg(name, msgType, inMsg, inSize, timeout))
	{
		return MIPC_ERROR;
	}

	if (outSize > 0)
	{
		if (MIPC_ERROR == mipc_receive_response_msg(name, msgType, outMsg, outSize, timeout))
		{
			printf("mipc_send_sync_msg: failed to get response msg from %s\n",name);
			return MIPC_ERROR;
		}
	}
	return MIPC_OK;
}


int mipc_send_cli_msg(char* name, int msgType, void* inMsg, unsigned int inSize, void* outMsg, unsigned int outSize, int timeout)
{
	char printBuf[MIPC_DEFAULT_MAX_MSG_SIZE];
	int  msg_type;

	mipc_clear_resp_msq();

	if (MIPC_ERROR == mipc_send_msg(name, msgType, inMsg, inSize, timeout))
	{
		printf("mipc_send_cli_msg: failed to send message\n");
		return MIPC_ERROR;
	}

	while (1)
	{
		if (MIPC_ERROR == mipc_receive_printf_msg(&msg_type, printBuf, MIPC_DEFAULT_MAX_MSG_SIZE))
		{
			printf("mipc_send_cli_msg: failed to get printf msg\n");
			break;
		}
		if (msg_type == MIPC_MSG_TYPE_PRINT)
		{
			//printf("%s", (char*)printBuf);
			mipc_vprintf(printBuf);
		}
		if (msg_type == MIPC_MSG_TYPE_PRINT_END)
		{
			if (outSize > 0)
			{
				if (MIPC_ERROR == mipc_receive_response_msg(name, msgType, outMsg, outSize, timeout))
				{
					printf("mipc_send_cli_msg: failed to get response msg\n");
					return MIPC_ERROR;
				}
			}
			break;
		}
	}
	return MIPC_OK;
}


int mipc_send_async_msg(const char* name, int msgType, void* inMsg, unsigned int inSize)
{
	if (MIPC_OK != mipc_send_msg(name, msgType, inMsg, inSize, -1))
	{
		printf("mipc_send_async_msg: failed to send msg\n");
		return MIPC_ERROR;
	}
	return MIPC_OK;
}

int mipc_response_msg(const char* name, int msgType, void* inMsg, unsigned int inSize)
{
	char msq_name[MIPC_NAME_MAX_LEN];

	sprintf(msq_name, "%s_resp", name);
	if (MIPC_OK != mipc_send_msg(msq_name, msgType, inMsg, inSize, -1))
	{
		printf("mipc_response_msg: failed to send msg\n");
		return MIPC_ERROR;
	}
	return MIPC_OK;
}

int mipc_receive_msg(char* src_name, unsigned int* msgType, char* inMsg, unsigned int size)
{
	MIPC_MSG_HDR_T* hdr = NULL;

	hdr = (MIPC_MSG_HDR_T*)inMsg;
	if (hdr->magic != MIPC_MSG_MAGIC)
	{
		printf("mipc_receive_msg: magic is incorrect: %08x\n", hdr->magic);
		return MIPC_ERROR;
	}

	strcpy(src_name, hdr->src_name);
	*msgType = hdr->msg_type;


	if (MIPC_OK == mipc_handle_system_msgs(hdr))
	{
		return MIPC_ERROR;
	}

	return MIPC_OK;
}

int mipc_receive_response_msg(const char* src_name, unsigned int msgType, void* outMsg, unsigned int outSize, int timeout)
{
	MIPC_MSG_HDR_T* hdr = NULL;
	char temp[MIPC_DEFAULT_MAX_MSG_SIZE];
    int index = mipc_find_thread_info_index();

	if (MIPC_ERROR == mipc_mq_receive(gMipcThreadInfo[index].resp_mqd, temp, MIPC_DEFAULT_MAX_MSG_SIZE, NULL, timeout))
	{
		printf("mipc_receive_response_msg: failed to recevie header\n");
		return MIPC_ERROR;
	}
	hdr = (MIPC_MSG_HDR_T*)temp;

	if (hdr->magic != MIPC_MSG_MAGIC)
	{
		printf("mipc_receive_response_msg: magic is incorrect: %08x\n", hdr->magic);
		return MIPC_ERROR;
	}

	if (0 != strcmp(src_name, hdr->src_name))
	{
		printf("mipc_receive_response_msg: src name mismatch: %s != %s\n", src_name, hdr->src_name);
		return MIPC_ERROR;
	}

	if (msgType != hdr->msg_type)
	{
		printf("mipc_receive_response_msg: msg type mismatch: %d != %d\n", msgType, hdr->msg_type);
		return MIPC_ERROR;
	}

	if (hdr->msg_size > outSize)
	{
		printf("mipc_receive_response_msg: size is bigger than required: %d > %d\n", hdr->msg_size, outSize);
		return MIPC_ERROR;
	}
	if (hdr->msg_size > 0)
	{
		if (NULL != outMsg)
		{
			memcpy(outMsg, temp + sizeof(MIPC_MSG_HDR_T), hdr->msg_size);
		}
	}

	return MIPC_OK;
}

int mipc_receive_printf_msg(unsigned int* msgType, void* inMsg, unsigned int inSize)
{
	MIPC_MSG_HDR_T* hdr = NULL;
	char temp[MIPC_DEFAULT_MAX_MSG_SIZE];
    int index = mipc_find_thread_info_index();

	if (MIPC_ERROR == mipc_mq_receive(gMipcThreadInfo[index].resp_mqd, temp, MIPC_DEFAULT_MAX_MSG_SIZE, NULL, MIPC_WAIT_FOREVER))
	{
		printf("mipc_receive_printf_msg: failed to recevie header\n");
		return MIPC_ERROR;
	}
	hdr = (MIPC_MSG_HDR_T*)temp;

	if (hdr->magic != MIPC_MSG_MAGIC)
	{
		printf("mipc_receive_printf_msg: magic is incorrect: %08x\n", hdr->magic);
		return MIPC_ERROR;
	}

	if (hdr->msg_size > inSize)
	{
		printf("mipc_receive_printf_msg: size is bigger than required: %d > %d\n", hdr->msg_size, inSize);
		return MIPC_ERROR;
	}
	*msgType = hdr->msg_type;
	if (hdr->msg_size > 0)
	{
		if (NULL != inMsg)
		{
			memcpy(inMsg, temp + sizeof(MIPC_MSG_HDR_T), hdr->msg_size);
		}
	}
	return MIPC_OK;
}

void mipc_printf(char* source_name, const char* format, ...)
{
	va_list          args;
    char             buf[MIPC_DEFAULT_MAX_MSG_SIZE];
	int              size;
	char             msq_name[MIPC_NAME_MAX_LEN];

	va_start(args, format);
    vsprintf(buf, format, args);
    va_end(args);

	size = strlen(buf) + 1;

	if (NULL == source_name)
	{
		printf("%s", buf);
		return;
	}

	sprintf(msq_name, "%s_resp", source_name);

	if (MIPC_OK != mipc_send_msg(msq_name, MIPC_MSG_TYPE_PRINT, buf, size, MIPC_WAIT_FOREVER))
	{
		printf("mipc_printf: failed to send msg\n");
	}
}

int mipc_ping(const char* name, int timeout)
{
	int result;
	return mipc_send_sync_msg(name, MIPC_MSG_TYPE_PING, NULL, 0, &result, sizeof(int), timeout);
}

int mipc_handle_system_msgs(MIPC_MSG_HDR_T* pMsg)
{
	if (NULL == pMsg)
	{
		return MIPC_ERROR;
	}

	switch (pMsg->msg_type)
	{
		case MIPC_MSG_TYPE_PING:
		{
			int result = 0;
			mipc_response_msg(pMsg->src_name, MIPC_MSG_TYPE_PING, &result, sizeof(int));
			return MIPC_OK;
		}
		default:
			break;
	}
	return MIPC_ERROR;
}

int mipc_msg_default_handler(char* msg, unsigned int size, unsigned int pri)
{
	char source_module[MIPC_NAME_MAX_LEN];
	unsigned int api_id;

	return mipc_receive_msg(source_module, &api_id, msg, size);
}

void mipc_show(void)
{
	int thread_index = mipc_find_thread_info_index();
	int i;
	
	printf("\n--------\n");
	printf("My Info\n");
	printf("--------\n");
	printf("Thread name=%s, PID=%d, threadId=%d, semID=%d\n", 
	        gMipcThreadInfo[thread_index].name, getpid(), pthread_self(), gMipcSemId);  
	printf("-----------------\n");
	printf("All Threads Info\n");
	printf("-----------------\n");
	printf("---------------------------------------------------------------------\n");
	printf("| ThreadId | Name      | MqId | RespMqId | OpenedMqId | OpenedMqName \n");
	printf("---------------------------------------------------------------------\n");

    for(i = 0;i < MIPC_SERVER_THREAD_NUM;i++)
    {
        if (MIPC_TRUE == gMipcThreadInfo[i].flag_is_hold)
        {
			printf("| %-8d | %-9s | %-4d | %-8d | %-10d | %-12s \n");
			printf("---------------------------------------------------------------------\n");
        }
    }
}

