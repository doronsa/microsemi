/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/********************************************************************************
*      tpm_cli_db.h
*
* DESCRIPTION:
*
*
* CREATED BY:  AyaL
*
* DATE CREATED: June 9, 2010
*
* DEPENDENCIES:
*
*
* FILE REVISION NUMBER:
*       $Revision: 1.1.1.1 $
*******************************************************************************/


#ifndef __TPM_CLI_DB_H__
#define __TPM_CLI_DB_H__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"
#include "tpm_types.h"


#define DB_CLI_MAX_NAME         16
#define DB_CLI_MAX_ENTRIES      512
#define DB_CLI_MAC_STR_LEN      25
#define DB_CLI_IPV4_STR_LEN     17
#define DB_CLI_IPV4_ADDR_LEN    4
#define DB_CLI_IPV6_STR_LEN     40
#define DB_CLI_IPV6_ADDR_LEN    16

#define DB_CLI_IPV4_FLAG        0x1
#define DB_CLI_IPV6_FLAG        0x2
#define DB_CLI_PPPOE_FLAG       0x4
#define DB_CLI_MAC_FLAG         0x8
#define DB_CLI_VLAN_FLAG        0x10
#define DB_CLI_DSCP_FLAG        0x20
#define DB_CLI_IP_PROTO_FLAG    0x40

#define PRINT(format, ...)      printf(format "\n", ##__VA_ARGS__)

#ifdef US_DEBUG_PRINT
#define DBG_PRINT(format, ...)  printf("%s(%d)" format "\n",__FUNCTION__,__LINE__, ##__VA_ARGS__)
#else
#define DBG_PRINT(format, ...)  
#endif

typedef struct
{
    char                name[DB_CLI_MAX_NAME+1];
    tpm_l2_acl_key_t    acl;

} db_cli_l2_key_t;

typedef struct
{
    char                name[DB_CLI_MAX_NAME+1];
    tpm_pkt_mod_t       mod;
    uint32_t            flags;

} db_cli_mod_t;

typedef struct
{
    char                name[DB_CLI_MAX_NAME+1];
    tpm_pkt_frwd_t      frwd;

} db_cli_frwd_t;

typedef struct
{
    char                name[DB_CLI_MAX_NAME+1];
    tpm_vlan_key_t      vlan;

} db_cli_vlan_mod_t;

typedef struct
{
    char                name[DB_CLI_MAX_NAME+1];
    tpm_l3_type_key_t   key;

} db_cli_l3_key_t;

typedef struct
{
    char                name[DB_CLI_MAX_NAME+1];
    tpm_ipv4_acl_key_t  key;

} db_cli_ipv4_key_t;

typedef struct
{
    char                name[DB_CLI_MAX_NAME+1];
    tpm_ipv6_acl_key_t  key;

} db_cli_ipv6_key_t;



/********************************************************************************/
/*                              DB utilities                                    */
/********************************************************************************/
bool_t  db_hexadec_arg (const char *arg);
void    tpm_cli_db_init (void);

/********************************************************************************/
/*                  VLAN modification table DB functions                        */
/********************************************************************************/
bool_t  db_cli_add_vlan_to_vlan_tbl (const char     *vlan_name,
                                     uint16_t       tpid,
                                     uint16_t       vid,
                                     uint16_t       vid_mask,
                                     uint8_t        cfi,
                                     uint8_t        cfi_mask,
                                     uint8_t        pbit,
                                     uint8_t        pbit_mask);
bool_t  db_cli_get_vlan_tbl_entry   (const char     *vlan_name,
                                     tpm_vlan_key_t *entry);
bool_t  db_cli_del_vlan_tbl_entry   (const char     *vlan_name);


/********************************************************************************/
/*                  L2 key table DB functions                                   */
/********************************************************************************/
bool_t  db_cli_add_ety_to_l2key_tbl(const char      *name,
                                    uint32_t        ety);
bool_t  db_cli_add_gem_to_l2key_tbl(const char      *name,
                                    uint16_t        gem);
bool_t  db_cli_add_mac_to_l2key_tbl(const char      *name,
                                    tpm_mac_key_t   *mac);
bool_t  db_cli_add_pppoe_to_l2key_tbl(const char    *name,
                                      uint16_t      session,
                                      uint16_t      protocol);
bool_t  db_cli_add_vlan_to_l2key_tbl (const char    *name,
                                      const char    *v1_name,
                                      const char    *v2_name);
bool_t  db_cli_get_l2key      (const char           *key_name,
                               tpm_l2_acl_key_t     *l2_key);
bool_t  db_cli_del_l2key_entry(const char           *l2key_name);


/********************************************************************************/
/*                  Packet mod & frwd tables DB functions                       */
/********************************************************************************/
bool_t  db_cli_add_frwd_to_db_tbl   (const char    *name,
                                     uint32_t       port,
                                     uint8_t        que,
                                     uint16_t       gem);
bool_t  db_cli_add_vlan_to_mod_tbl  (const char         *name,
                                     tpm_vlan_oper_t    oper,
                                     const char         *v1_name,
                                     const char         *v2_name);
bool_t  db_cli_add_mac_to_mod_tbl   (const char      *name,
                                     tpm_mac_key_t   *mac);
bool_t  db_cli_add_pppoe_to_mod_tbl (const char    *name,
                                     uint16_t      session,
                                     uint16_t      protocol);
bool_t  db_cli_add_dscp_to_mod_tbl  (const char  *name,
                                     uint8_t     dscp,
                                     uint8_t     mask);
bool_t  db_cli_add_proto_to_mod_tbl (const char  *name,
                                     uint8_t     proto);
bool_t  db_cli_add_ports_to_ipv4_mod_tbl (const char  *name,
                                          uint16_t    src,
                                          uint16_t    dst);
bool_t  db_cli_add_ports_to_ipv6_mod_tbl (const char  *name,
                                          uint16_t    src,
                                          uint16_t    dst);
bool_t  db_cli_add_ips_to_ipv4_mod_tbl   (const char  *name,
                                          uint8_t     *src,
                                          uint8_t     *src_mask,
                                          uint8_t     *dst,
                                          uint8_t     *dst_mask);
bool_t  db_cli_add_ips_to_ipv6_mod_tbl   (const char  *name,
                                          uint8_t     *src,
                                          uint8_t     *src_mask,
                                          uint8_t     *dst,
                                          uint8_t     *dst_mask);
bool_t  db_cli_get_pkt_frwd   (const char           *pkt_name,
                               tpm_pkt_frwd_t       *pkt_frwd);
bool_t  db_cli_get_pkt_mod    (const char           *pkt_name,
                               tpm_pkt_mod_t        *pkt_mod);

bool_t  db_cli_del_pkt_mod_entry   (const char     *mod_name);
bool_t  db_cli_del_pkt_frwd_entry  (const char     *frwd_name);


/********************************************************************************/
/*                  L3 key table DB functions                                   */
/********************************************************************************/
bool_t  db_cli_add_ety_to_l3key_tbl(const char      *name,
                                    uint32_t        ety);
bool_t  db_cli_add_pppoe_to_l3key_tbl(const char    *name,
                                      uint16_t      session,
                                      uint16_t      protocol);
bool_t  db_cli_get_l3key      (const char           *key_name,
                               tpm_l3_type_key_t    *l3_key);
bool_t  db_cli_del_l3key_entry(const char           *l2key_name);

/********************************************************************************/
/*                  IPv4 key table DB functions                                 */
/********************************************************************************/
bool_t  db_cli_get_ipv4_key (const char           *key_name,
                             tpm_ipv4_acl_key_t   *ipv4_key);
bool_t  db_cli_add_dscp_to_ipv4_key_tbl (const char  *name,
                                         uint8_t     dscp,
                                         uint8_t     mask);
bool_t  db_cli_add_proto_to_ipv4_key_tbl(const char  *name,
                                         uint8_t     proto);
bool_t  db_cli_add_ports_to_ipv4_key_tbl(const char  *name,
                                         uint16_t    src,
                                         uint16_t    dst);
bool_t  db_cli_add_ips_to_ipv4_key_tbl  (const char  *name,
                                         uint8_t     *src,
                                         uint8_t     *src_mask,
                                         uint8_t     *dst,
                                         uint8_t     *dst_mask);
bool_t  db_cli_del_ipv4_key_entry   (const char     *ipv4_key_name);

/********************************************************************************/
/*                  IPv6 key table DB functions                                 */
/********************************************************************************/
bool_t  db_cli_get_ipv6_key (const char           *key_name,
                             tpm_ipv6_acl_key_t   *ipv6_key);
bool_t  db_cli_add_dscp_to_ipv6_key_tbl (const char  *name,
                                         uint8_t     dscp,
                                         uint8_t     mask);
bool_t  db_cli_add_proto_to_ipv6_key_tbl(const char  *name,
                                         uint8_t     proto);
bool_t  db_cli_add_ports_to_ipv6_key_tbl(const char  *name,
                                         uint16_t    src,
                                         uint16_t    dst);
bool_t  db_cli_add_ips_to_ipv6_key_tbl  (const char  *name,
                                         uint8_t     *src,
                                         uint8_t     *src_mask,
                                         uint8_t     *dst,
                                         uint8_t     *dst_mask);

/********************************************************************************/
/*                  TPM CLI print functions                                     */
/********************************************************************************/
void    db_cli_print_rule_entry(tpm_rule_entry_t  *entry, uint32_t rule_idx);

void    db_cli_print_vlan_table(void);
bool_t  db_cli_print_vlan_table_entry(const char    *name);


void    db_cli_print_l2key_table(void);
bool_t  db_cli_print_l2key_table_entry(const char    *name);
bool_t  db_cli_print_l2key_table_mac(const char    *name);
bool_t  db_cli_print_l2key_table_vlan(const char    *name);
bool_t  db_cli_print_l2key_table_pppoe(const char    *name);
bool_t  db_cli_print_l2key_table_ety(const char    *name);
bool_t  db_cli_print_l2key_table_gem(const char    *name);

void    db_cli_print_frwd_table(void);
bool_t  db_cli_print_frwd_table_entry(const char    *name);

void    db_cli_print_mod_table(void);
bool_t  db_cli_print_mod_table_entry(const char    *name);

void    db_cli_print_l3key_table(void);
bool_t  db_cli_print_l3key_table_entry(const char    *name);

void    db_cli_print_ipv4_key_table(void);
bool_t  db_cli_print_ipv4_key_table_entry(const char    *name);

void    db_cli_print_ipv6_key_table(void);
bool_t  db_cli_print_ipv6_key_table_entry(const char    *name);


#endif /*__TPM_CLI_DB_H__*/
