/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "clish/shell.h"
#include "tpm_types.h"
#include "tpm_internal_types.h"
#include "tpm_cli_db.h"



bool_t  tpm_cli_mod2_print_last_pattern  (const clish_shell_t    *instance,
                                          const lub_argv_t       *argv)
{
    uint32_t    gmac;
    uint32_t    entry;
    uint32_t    num;

    /* Get parameters */
    DBG_PRINT("No parameters");

    tpm_mod2_print_tmp_pattern ();

    return BOOL_TRUE;
}

bool_t  tpm_cli_mod2_print_jump_area_range  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv)
{
    uint32_t    gmac;
    uint32_t    entry;
    uint32_t    num;

    /* Get parameters */
    gmac    = atoi(lub_argv__get_arg(argv, 0));
    entry   = atoi(lub_argv__get_arg(argv, 1));
    num     = atoi(lub_argv__get_arg(argv, 2));

    DBG_PRINT("GMAC = %d start index = %d num = %d", gmac, entry, num);

    tpm_mod2_print_jump_range ((tpm_gmacs_enum_t)gmac, entry, num);

    return BOOL_TRUE;
}

bool_t  tpm_cli_mod2_print_jump_area        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv)
{
    uint32_t    gmac;

    /* Get parameters */
    gmac    = atoi(lub_argv__get_arg(argv, 0));

    DBG_PRINT("GMAC = %d", gmac);

    tpm_mod2_print_jump_all ((tpm_gmacs_enum_t)gmac);

    return BOOL_TRUE;
}

bool_t  tpm_cli_mod2_print_main_area_range  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv)
{
    uint32_t    gmac;
    uint32_t    entry;
    uint32_t    num;

    /* Get parameters */
    gmac    = atoi(lub_argv__get_arg(argv, 0));
    entry   = atoi(lub_argv__get_arg(argv, 1));
    num     = atoi(lub_argv__get_arg(argv, 2));

    DBG_PRINT("GMAC = %d start index = %d num = %d", gmac, entry, num);

    tpm_mod2_print_main_range ((tpm_gmacs_enum_t)gmac, entry, num);

    return BOOL_TRUE;
}

bool_t  tpm_cli_mod2_print_main_area        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv)
{
    uint32_t    gmac;

    /* Get parameters */
    gmac    = atoi(lub_argv__get_arg(argv, 0));

    DBG_PRINT("GMAC = %d ", gmac);

    tpm_mod2_print_main_all ((tpm_gmacs_enum_t)gmac);

    return BOOL_TRUE;
}

bool_t  tpm_cli_mod2_print_config           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv)
{
    uint32_t    gmac;

    /* Get parameters */
    gmac    = atoi(lub_argv__get_arg(argv, 0));

    DBG_PRINT("GMAC = %d ", gmac);

    tpm_mod2_print_config ((tpm_gmacs_enum_t)gmac);

    return BOOL_TRUE;
}

bool_t  tpm_cli_mod_add                     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv)
{
    const char          name[DB_CLI_MAX_NAME+1];
    const char          *bm_str;
    uint32_t            port;
    uint32_t            bm;
    uint32_t            int_mod_bm;
    tpm_pkt_mod_t       mod_data;
    uint32_t            mod_entry = 0;
    tpm_error_code_t    rc;

    /* Get parameters */
    port    = atoi(lub_argv__get_arg(argv, 0));
    bm_str  = lub_argv__get_arg(argv, 1);
    sscanf(&bm_str[2], "%x", &bm);
    bm_str  = lub_argv__get_arg(argv, 2);
    sscanf(&bm_str[2], "%x", &int_mod_bm);
    memcpy(&name, lub_argv__get_arg(argv, 3), DB_CLI_MAX_NAME);


    DBG_PRINT("port = %d BM = 0x%x INT BM = 0x%x name = %s", port, bm, int_mod_bm, name);

    if (db_cli_get_pkt_mod (name, &mod_data) != BOOL_TRUE)
    {
        PRINT("Failed to get modification data from DB");
        return BOOL_FALSE;
    }

    rc = tpm_mod_entry_set ((tpm_trg_port_type_t)port, (tpm_pkt_mod_bm_t)bm, (tpm_pkt_mod_int_bm_t)int_mod_bm, &mod_data, &mod_entry);

    if (rc == TPM_RC_OK)
        PRINT("%d jump modification entry has been added", mod_entry);

    return BOOL_TRUE;
}

char* opCodeOperationStr[30] =
{
    "NOOP",
    "ADD",
    "CONF_VLAN",
    "ADD_VLAN",
    "CONF_DSA1",
    "CONF_DSA2",
    "ADD_DSA",
    "DEL",
    "REP2",
    "REP_LSB",
    "REP_MSB",
    "REP_VLAN",
    "DEC_TTL_LSB",
    "DEC_TTL_MSB",
    "ADD_CALC_LEN",
    "REP_LEN",
    "REP_IP_CHKSUM",
    "REP_L4_CHKSUM",
    "SKIP",
    "JUMP",
    "JUMP_SKIP2",
    "JUMP_SUBR",
    "PPPOE",
    "STORE",
    "ADD_IP_CHKSUM",
    "RESERVED",
    "RESERVED",
    "RESERVED",
    "RESERVED",
    "RESERVED"
};

char* modOwnerStr[4] =
{
    "TPM",
    "CPU"
};

bool_t  tpm_cli_mod_get                     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv)
{
    uint32_t            port;
    uint32_t            entry;
    tpm_error_code_t    rc;
    tpm_mod_rule_t      rule[TPM_MAX_MOD_RULE_NUM];
    uint16_t            valid_cmds;
    uint16_t            pnc_ref;

    /* Get parameters */
    port    = atoi(lub_argv__get_arg(argv, 0));
    entry   = atoi(lub_argv__get_arg(argv, 1));

    DBG_PRINT("port = %d entry = %d", port, entry);

    rc = tpm_mod_entry_get((tpm_trg_port_type_t)port,
                           entry,
                           &valid_cmds,
                           &pnc_ref,
                           &rule);
    if (rc == TPM_RC_OK)
    {
#ifdef TPM_Z2
        int         i;
        uint32_t    m;

        printf("=====================================================================\n");
        printf(" Index  OpCode    Operation      Data  Last  IPv4    L4   Owner  PnC \n");
        printf("                                            update update        ref \n");
        printf("=====================================================================\n");

        for (i=0; i<valid_cmds; i++)
        {
            if (i==0)
            {
                m = rule[0].entry_data.data;

                /* Print jump area entry */
                printf("  %4d   0x%2x  %15s    %4d  %1d     %1d      %1d    %s   %3d\n",
                       entry, rule[0].entry_data.opcode, opCodeOperationStr[rule[0].entry_data.opcode],
                       rule[0].entry_data.data, rule[0].entry_data.last, rule[0].entry_data.updt_ipv4,
                       rule[0].entry_data.updt_tcp, modOwnerStr[0], pnc_ref);
            }
            else
            {
                /* Print main area entries */
                printf("  %4d   0x%2x  %15s  0x%4.4x  %1d     %1d      %1d \n",
                       m++, rule[i].entry_data.opcode, opCodeOperationStr[rule[i].entry_data.opcode],
                       rule[i].entry_data.data, rule[i].entry_data.last,
                       rule[i].entry_data.updt_ipv4, rule[i].entry_data.updt_tcp);

                if (rule[i].entry_data.last)
                    break;
            }
        }
#endif
    }

    return BOOL_TRUE;
}


bool_t  tpm_cli_mod_del                     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv)
{
    uint32_t    port;
    uint32_t    entry;

    /* Get parameters */
    port    = atoi(lub_argv__get_arg(argv, 0));
    entry   = atoi(lub_argv__get_arg(argv, 1));

    DBG_PRINT("port = %d entry = %d", port, entry);

    tpm_mod_entry_del ((tpm_trg_port_type_t)port, entry);

    return BOOL_TRUE;
}

bool_t  tpm_cli_mod_inv                     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv)
{
    uint32_t    port;

    /* Get parameters */
    port = atoi(lub_argv__get_arg(argv, 0));

    DBG_PRINT("port = %d", port);

    tpm_mod_mac_inv ((tpm_trg_port_type_t)port);

    return BOOL_TRUE;
}


