/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "clish/shell.h"
#include "tpm_api.h"


static uint32_t trace_sw_xml_dbg_flag = 0;

bool_t tpm_sw_cli_add_static_mac
(
    const clish_shell_t* instance,
    const lub_argv_t*  argv
)
{
    uint32_t            owner_id;
    uint32_t            src_port;
    uint8_t             static_mac[6];

    owner_id            = atoi(lub_argv__get_arg(argv,0));
    src_port            = atoi(lub_argv__get_arg(argv,1));
    const char* mac_ptr       = lub_argv__get_arg(argv,2);

    sscanf(mac_ptr, "%x:%x:%x:%x:%x:%x",
                    &(static_mac[0]),
                    &(static_mac[1]),
                    &(static_mac[2]),
                    &(static_mac[3]),
                    &(static_mac[4]),
                    &(static_mac[5]));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s:owner_id[%d],lport[%d]\n\r \
               static_mac[%02x:%02x:%02x:%02x:%02x:%02x]\n\r",
               __FUNCTION__,
               owner_id,
               src_port,
               static_mac[0],
               static_mac[1],
               static_mac[2],
               static_mac[3],
               static_mac[4],
               static_mac[5]);


    tpm_sw_add_static_mac(owner_id, src_port, static_mac);


    return BOOL_TRUE;
}

bool_t tpm_sw_cli_del_static_mac
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
    uint32_t            owner_id;
    uint8_t             static_mac[6];

    owner_id            = atoi(lub_argv__get_arg(argv,0));
    const char* mac_ptr       = lub_argv__get_arg(argv,1);

    sscanf(mac_ptr, "%x:%x:%x:%x:%x:%x",
                    &(static_mac[0]),
                    &(static_mac[1]),
                    &(static_mac[2]),
                    &(static_mac[3]),
                    &(static_mac[4]),
                    &(static_mac[5]));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d]\n\r, \
               static_mac[%02x:%02x:%02x:%02x:%02x:%02x]\n\r",
               __FUNCTION__,owner_id,
               static_mac[0],
               static_mac[1],
               static_mac[2],
               static_mac[3],
               static_mac[4],
               static_mac[5]);


    tpm_sw_del_static_mac(owner_id, static_mac);


    return BOOL_TRUE;
}

bool_t tpm_sw_cli_set_port_max_macs
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
    uint32_t            owner_id;
    uint32_t            src_port;
    uint8_t             mac_per_port;

    owner_id            = atoi(lub_argv__get_arg(argv,0));
    src_port            = atoi(lub_argv__get_arg(argv,1));
    mac_per_port        = atoi(lub_argv__get_arg(argv,2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],mac_per_port[%d]",
                __FUNCTION__,owner_id, src_port, mac_per_port);

    tpm_sw_set_port_max_macs(owner_id, src_port, mac_per_port);

    return BOOL_TRUE;
}


bool_t tpm_sw_cli_set_port_flooding
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
   uint32_t             owner_id;
   uint32_t             src_port;
   uint32_t             mode;
   uint8_t              allow_flood;

    owner_id           = atoi(lub_argv__get_arg(argv,0));
    src_port           = atoi(lub_argv__get_arg(argv,1));
    mode               = atoi(lub_argv__get_arg(argv,2));
    allow_flood        = atoi(lub_argv__get_arg(argv,3));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],allow_flood[%d]",
                __FUNCTION__,owner_id, src_port, allow_flood);

    tpm_sw_set_port_flooding(owner_id, src_port, (tpm_flood_type_t)mode, allow_flood);

    return BOOL_TRUE;
}

 bool_t tpm_sw_cli_set_port_tagged
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
    uint32_t            owner_id;
    uint32_t            src_port;
    uint8_t             allow_tagged;

    owner_id           = atoi(lub_argv__get_arg(argv,0));
    src_port           = atoi(lub_argv__get_arg(argv,1));
    allow_tagged       = atoi(lub_argv__get_arg(argv,2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],allow_tagged[%d]",
                __FUNCTION__,owner_id, src_port, allow_tagged);

    tpm_sw_set_port_tagged(owner_id, src_port, allow_tagged);

    return BOOL_TRUE;
}

bool_t tpm_sw_cli_set_port_untagged
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
    uint32_t            owner_id;
    uint32_t            src_port;
    uint8_t             allow_untagged;

    owner_id           = atoi(lub_argv__get_arg(argv,0));
    src_port           = atoi(lub_argv__get_arg(argv,1));
    allow_untagged     = atoi(lub_argv__get_arg(argv,2));

    printf("==ENTER==%s: owner_id[%d],lport[%d],allow_untagged[%d]",
                __FUNCTION__,owner_id, src_port, allow_untagged);

    tpm_sw_set_port_untagged(owner_id, src_port, allow_untagged);

    return BOOL_TRUE;
}

bool_t tpm_sw_cli_port_add_vid
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
    uint32_t            owner_id;
    uint32_t            src_port;
    uint16_t            vid;

    owner_id           = atoi(lub_argv__get_arg(argv,0));
    src_port           = atoi(lub_argv__get_arg(argv,1));
    vid                = atoi(lub_argv__get_arg(argv,2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],vid[%d]",
                __FUNCTION__,owner_id, src_port, vid);

    tpm_sw_port_add_vid(owner_id, src_port, vid);

    return BOOL_TRUE;
}

bool_t tpm_sw_cli_port_del_vid
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
    uint32_t            owner_id;
    uint32_t            src_port;
    uint16_t            vid;

    owner_id           = atoi(lub_argv__get_arg(argv,0));
    src_port           = atoi(lub_argv__get_arg(argv,1));
    vid                = atoi(lub_argv__get_arg(argv,2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],vid[%d]",
                __FUNCTION__,owner_id, src_port, vid);

    tpm_sw_port_del_vid(owner_id, src_port, vid);

    return BOOL_TRUE;
}


bool_t tpm_sw_cli_port_set_vid_filter
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
    uint32_t            owner_id;
    uint32_t            src_port;
    uint8_t             vid_filter;

    owner_id           = atoi(lub_argv__get_arg(argv,0));
    src_port           = atoi(lub_argv__get_arg(argv,1));
    vid_filter         = atoi(lub_argv__get_arg(argv,2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],vid_filter[%d]",
                __FUNCTION__,owner_id, src_port, vid_filter);

    tpm_sw_port_set_vid_filter(owner_id, src_port, vid_filter);

    return BOOL_TRUE;
}

bool_t tpm_sw_cli_set_q_weight
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
    uint32_t            owner_id;
    uint8_t             queue_id;
    uint8_t             weight;

    owner_id           = atoi(lub_argv__get_arg(argv,0));
    queue_id           = atoi(lub_argv__get_arg(argv,1));
    weight             = atoi(lub_argv__get_arg(argv,2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],queue_id[%d],weight[%d]",
                __FUNCTION__,owner_id, queue_id, weight);

    tpm_sw_set_uni_q_weight(owner_id, queue_id, weight);

    return BOOL_TRUE;
}

bool_t tpm_sw_cli_set_ingr_police_rate_per_port
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
    uint32_t          owner_id;
    uint32_t		  uni_port;
    uint32_t 		  cir;
    uint32_t		  cbs;
    uint32_t		  ebs = 0;		

    owner_id           = atoi(lub_argv__get_arg(argv,0));
    uni_port           = atoi(lub_argv__get_arg(argv,1));
    cir                = atoi(lub_argv__get_arg(argv,2));
    cbs                = atoi(lub_argv__get_arg(argv,3));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],uni_port[%d],cir[%d],cbs[%d]",
           __FUNCTION__,
           owner_id,
           uni_port,
           cir,
           cbs);

    tpm_sw_set_uni_ingr_police_rate(owner_id, uni_port, TPM_SW_LIMIT_LAYER2, cir, cbs, ebs);

    return BOOL_TRUE;
}

bool_t tpm_sw_cli_set_tc_ingr_police_rate
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
    uint32_t          owner_id;
    uint32_t		  uni_port;
    uint32_t		  tc;
    uint32_t 		  cir;
    uint32_t		  cbs;

    owner_id           = atoi(lub_argv__get_arg(argv,0));
    uni_port           = atoi(lub_argv__get_arg(argv,1));
    tc                 = atoi(lub_argv__get_arg(argv,2));
    cir                = atoi(lub_argv__get_arg(argv,3));
    cbs                = atoi(lub_argv__get_arg(argv,4));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],uni_port[%d],tc[%d],cir[%d],cbs[%d]",
                __FUNCTION__,
                owner_id,
                uni_port,
                tc, cir, cbs);

    tpm_sw_set_uni_tc_ingr_police_rate(owner_id, uni_port, tc, cir, cbs);

    return BOOL_TRUE;
}

bool_t tpm_sw_cli_set_egr_rate_limit
(
    const const clish_shell_t* instance,
    const lub_argv_t*   argv
)
{
    uint32_t          owner_id;
    uint32_t		  trg_lport;
    uint32_t 		  frame_rate_limit_val;

    owner_id                = atoi(lub_argv__get_arg(argv,0));
    trg_lport               = atoi(lub_argv__get_arg(argv,1));
    frame_rate_limit_val    = atoi(lub_argv__get_arg(argv,2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],trg_lport[%d],frame_rate_limit_val[%d]",
                __FUNCTION__,
                owner_id,
                trg_lport,
                frame_rate_limit_val);

    tpm_sw_set_uni_egr_rate_limit(owner_id, trg_lport, TPM_SW_LIMIT_LAYER2,frame_rate_limit_val);

    return BOOL_TRUE;
}

bool_t  tpm_sw_cli_set_uni_sched (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            mode;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    printf("Owner = %d port = %d mode = %d \r\n", owner_id, port, mode);

    tpm_rc = tpm_sw_set_uni_sched (owner_id, (tpm_src_port_type_t)port, (tpm_sw_sched_type_t)mode);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set scheduling mode for port %d\r\n", port);
    }
    else
    {
        printf("Scheduling mode set OK\r\n");
    }

    return rc;
}

bool_t  tpm_sw_cli_set_uni_autoneg (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            state;
    uint16_t            autoneg_mode;    
    bool_t 		     autoneg_state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    state        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    autoneg_mode = tpm_cli_get_number(lub_argv__get_arg(argv, 3));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],state[%d],mode[%d]\r\n",
                __FUNCTION__,owner_id, port, state, autoneg_mode);

    if(state)
		autoneg_state = true;
    else
		autoneg_state = false;
	
    tpm_rc = tpm_phy_set_port_autoneg_mode(owner_id, (tpm_src_port_type_t)port, autoneg_state, (tpm_autoneg_mode_t)autoneg_mode);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set auto-negotiation to %d\r\n", state);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("Auto-negotiation state set OK\r\n");
    }

    return rc;
}

bool_t  tpm_sw_cli_set_uni_restart_autoneg (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d]\r\n",
                __FUNCTION__,owner_id, port);

    tpm_rc = tpm_phy_restart_port_autoneg (owner_id, (tpm_src_port_type_t)port);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to restart auto-negotiation\r\n");
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
	        printf("Auto-negotiation state restart OK\r\n");
    }

    return rc;
}

bool_t  tpm_sw_cli_set_uni_admin (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            state;
    bool_t 		     port_state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    state        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],state[%d]\r\n",
                __FUNCTION__,owner_id, port, state);
		
    if(state)
		port_state = true;
    else
		port_state = false;
	
    tpm_rc = tpm_phy_set_port_admin_state (owner_id, (tpm_src_port_type_t)port, port_state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set port state to %d OK\r\n", state);
    }
    else
    {
    	if (trace_sw_xml_dbg_flag)		
        	printf("port state set\r\n");
    }

    return rc;
}

bool_t  tpm_sw_cli_set_uni_flow_control (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            state;
    bool_t 		     pause_state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    state        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],state[%d]\r\n",
                __FUNCTION__,owner_id, port, state);
		
    if(state)
		pause_state = true;
    else
		pause_state = false;
	
    tpm_rc = tpm_phy_set_port_flow_control_support (owner_id, (tpm_src_port_type_t)port, pause_state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set pause state to %d\r\n", state);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("pause state set OK\r\n");
    }

    return rc;
}
bool_t  tpm_sw_cli_set_uni_speed (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t 		     speed;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    speed        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],speed[%d]\r\n",
                __FUNCTION__,owner_id, port, speed);
		
    tpm_rc = tpm_phy_set_port_speed (owner_id, (tpm_src_port_type_t)port, (tpm_phy_speed_t)speed);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set flow control state to %d\r\n", speed);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("Flow control state set OK\r\n");
    }

    return rc;
}
bool_t  tpm_sw_cli_set_uni_loopback (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            mode;
    uint16_t            state;		

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    state        = tpm_cli_get_number(lub_argv__get_arg(argv, 3));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],mode[%d], state[%d]\r\n",
                __FUNCTION__,owner_id, port, mode, state);
		
    tpm_rc = tpm_phy_set_port_loopback(owner_id, (tpm_src_port_type_t)port, (tpm_phy_loopback_mode_t)mode, (bool_t)state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set loopback state to %d\r\n", state);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("loopback state set OK\r\n");
    }

    return rc;
}

bool_t  tpm_sw_cli_set_uni_duplex (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            state;
    bool_t 		     duplex_state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    state        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],state[%d]\r\n",
                __FUNCTION__,owner_id, port, state);
		
    if(state)
		duplex_state = true;
    else
		duplex_state = false;
	
    tpm_rc = tpm_phy_set_port_duplex_mode (owner_id, (tpm_src_port_type_t)port, duplex_state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set duplex state to %d\r\n", state);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("Full duplex state set OK\r\n");
    }

    return rc;
}

bool_t  tpm_sw_cli_set_uni_default_vlan (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            vlan;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    vlan        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],vlan[%d]\r\n",
                __FUNCTION__,owner_id, port, vlan);

    tpm_rc = tpm_sw_set_port_def_vlan(owner_id, (tpm_src_port_type_t)port, vlan);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set default VLAN to %d\r\n", vlan);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("Default VLAN set OK\r\n");
    }

    return rc;
}


bool_t  tpm_sw_cli_set_uni_default_pri (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            pri;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    pri        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],pri[%d]\r\n",
                __FUNCTION__,owner_id, port, pri);

    tpm_rc = tpm_sw_set_port_def_pri (owner_id, (tpm_src_port_type_t)port, pri);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set default VLAN pri to %d\r\n", pri);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("Default VLAN pri set OK\r\n");
    }

    return rc;
}

bool_t  tpm_sw_cli_set_mac_aging_time (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            time;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    time        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d], MAC aging time[%d]\r\n",
                __FUNCTION__,owner_id, time);

    tpm_rc = tpm_sw_set_mac_age_time (owner_id, time);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set MAC aging time to %d\r\n", time);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("MAC aging time set OK\r\n");
    }

    return rc;
}
bool_t  tpm_sw_cli_set_uni_mac_learn(const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    state        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],state[%d]\r\n",
                __FUNCTION__,owner_id, port, state);

    tpm_rc = tpm_sw_set_mac_learn (owner_id, (tpm_src_port_type_t)port, (bool_t)state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set MAC learning to %d\r\n", state);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf(" MAC learning set OK\r\n");
    }

    return rc;
}


bool_t  tpm_sw_cli_set_uni_port_mirror (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            sport;
    uint32_t            dport;		
    uint16_t            mode;
    uint16_t            state = 1;		

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s:\r\n", __FUNCTION__);

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    sport        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    dport        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));		
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    state        = tpm_cli_get_number(lub_argv__get_arg(argv, 4));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],sport[%d],dport[%d],mode[%d], state[%d]\r\n",
                __FUNCTION__,owner_id, sport, dport, mode, state);
		
    tpm_rc = tpm_sw_set_port_mirror(owner_id, (tpm_src_port_type_t)sport, (tpm_src_port_type_t)dport, (tpm_sw_mirror_type_t)mode, (bool_t)state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set port mirror to %d\r\n", state);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("Port mirror set OK\r\n");
    }

    return rc;
}

bool_t  tpm_sw_cli_set_uni_mtu_size(const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            mode;		
    uint32_t            size;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    size        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
		
    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],mode[%d], size[%d\r\n",
                __FUNCTION__,owner_id, mode, size);

    tpm_rc = tpm_set_mtu_size (owner_id, (tpm_mru_type_t)mode, (bool_t)size);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set MAC[%d] MTU size to %d\r\n", mode, size);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf(" MTU size set OK\r\n");
    }

    return rc;
}
bool_t  tpm_sw_cli_set_uni_isolate_vector(const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            vector;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    vector        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],vector[%d]\r\n",
                __FUNCTION__,owner_id, port, vector);

    tpm_rc = tpm_sw_set_isolate_eth_port_vector (owner_id, (tpm_src_port_type_t)port, vector);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to isolate port vector to %08x\r\n", vector);
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("Port vector set OK\r\n");
    }

    return rc;
}
bool_t  tpm_sw_cli_add_group_vid(const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            mode;
    uint16_t            min_vid;			
    uint16_t            max_vid;	
		
    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    min_vid     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    max_vid     = tpm_cli_get_number(lub_argv__get_arg(argv, 4));		

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d],mode[%d], mix VID[%d], max VID[%d]\r\n",
                __FUNCTION__,owner_id, port, mode, min_vid, max_vid);

    tpm_rc = tpm_sw_port_add_vid_group (owner_id, (tpm_src_port_type_t)port, (tpm_vlan_member_mode_t)mode,
						min_vid, max_vid);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to add group VID\r\n");
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("Added group VID  successfully\r\n");
    }

    return rc;
}
bool_t  tpm_sw_cli_del_group_vid(const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            min_vid;			
    uint16_t            max_vid;	
		
    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    min_vid     = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    max_vid     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));		

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d],lport[%d], mix VID[%d], max VID[%d]\r\n",
                __FUNCTION__,owner_id, port, min_vid, max_vid);

    tpm_rc = tpm_sw_port_del_vid_group (owner_id, (tpm_src_port_type_t)port,
						min_vid, max_vid);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to delete group VID\r\n");
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("Delete group VID  successfully\r\n");
    }

    return rc;
}
bool_t  tpm_sw_cli_clear_dynamic_mac(const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));

    if (trace_sw_xml_dbg_flag)
        printf("==ENTER==%s: owner_id[%d]\r\n",
                __FUNCTION__,owner_id);

    tpm_rc = tpm_sw_clear_dynamic_mac (owner_id);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to clear all dynamic MAC of switch\r\n");
    }
    else
    {
        if (trace_sw_xml_dbg_flag)
        	printf("Cleared all dynamic MAC of sw successfully\r\n");
    }

    return rc;
}

bool_t  tpm_sw_cli_get_uni_autoneg (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    bool 		     autoneg_state;
    tpm_autoneg_mode_t  autoneg_mode;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    tpm_rc = tpm_phy_get_port_autoneg_mode(owner_id, (tpm_src_port_type_t)port, &autoneg_state, &autoneg_mode);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] auto-negotiation\r\n", port);
    }
    else
    {
        if(autoneg_state == true)
        {
            printf("Port[%d] auto-negotiation state configuration[Enabled],mode[%d]\r\n", port, autoneg_mode);
		 }
		 else
        {
            printf("Port[%d] auto-negotiation state configuration[Disabled]\r\n", port);
		 }			
        	
    }

    return rc;
}

bool_t  tpm_sw_cli_get_uni_admin (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    bool 		     port_state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
	
    tpm_rc = tpm_phy_get_port_admin_state (owner_id, (tpm_src_port_type_t)port, &port_state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] admin state\r\n", port);
    }
    else
    {
        if(port_state == true)
        {
            printf("Port[%d] admin state configuration[Enabled]\r\n", port);
		 }
		 else
        {
            printf("Port[%d] admin state configuration[Disabled]\r\n", port);
		 }			
        	
    }
    return rc;
}

bool_t  tpm_sw_cli_get_uni_link (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    bool		         state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
	
    tpm_rc = tpm_phy_get_port_link_status(owner_id, (tpm_src_port_type_t)port, &state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] link status\r\n", port);
    }
    else
    {
        if(state == true)
        {
            printf("Port[%d] link status[UP] \n", port);
		 }
		 else
        {
            printf("Port[%d] link status[DOWN]\r\n", port);
		 }			
        	
    }
    return rc;
}

bool_t  tpm_sw_cli_get_uni_flow_control (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    bool 		     state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
	
    tpm_rc = tpm_phy_get_port_flow_control_support (owner_id, (tpm_src_port_type_t)port, &state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] flow control configuration\r\n", port);
    }
    else
    {
        if(state == true)
        {
            printf("Port[%d] flow control configuration[Enabled] \n", port);
		 }
		 else
        {
            printf("Port[%d] flow control configuration[Disabled]\r\n", port);
		 }		
    }

    return rc;
}

bool_t  tpm_sw_cli_get_uni_flow_control_state (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    bool		     state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
	
    tpm_rc = tpm_phy_get_port_flow_control_state (owner_id, (tpm_src_port_type_t)port, &state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] flow control state\r\n", port);
    }
    else
    {
        if(state == true)
        {
            printf("Port[%d] flow control state[ON] \n", port);
		 }
		 else
        {
            printf("Port[%d] flow control state[OFF]\r\n", port);
		 }		
    }

    return rc;
}

bool_t  tpm_sw_cli_get_uni_duplex (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    bool 		          state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    tpm_rc = tpm_phy_get_port_duplex_mode (owner_id, (tpm_src_port_type_t)port, &state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] duplex mode configuration\r\n", port);
    }
    else
    {
        if(state == true)
        {
            printf("Port[%d] duplex mode configuration[Enabled] \n", port);
		 }
		 else
        {
            printf("Port[%d] duplex mode configuration[Disabled]\r\n", port);
		 }		
    }

    return rc;
}

bool_t  tpm_sw_cli_get_uni_duplex_state (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    bool		          state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    tpm_rc = tpm_phy_get_port_duplex_status (owner_id, (tpm_src_port_type_t)port, &state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] duplex mode state\r\n", port);
    }
    else
    {
        if(state == true)
        {
            printf("Port[%d] duplex mode state[Full Duplex] \n", port);
		 }
		 else
        {
            printf("Port[%d] duplex mode state[Half Duplex]\r\n", port);
		 }		
    }

    return rc;
}

bool_t  tpm_sw_cli_get_uni_speed (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    tpm_phy_speed_t 		     speed;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
		
    tpm_rc = tpm_phy_get_port_speed (owner_id, (tpm_src_port_type_t)port, &speed);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] speed mode configuration\r\n", port);
    }
    else
    {
        if(speed == TPM_PHY_SPEED_10_MBPS)
        {
            printf("Port[%d] speed mode configuration[10M] \n", port);
		 }
        else if(speed == TPM_PHY_SPEED_100_MBPS)
        {
            printf("Port[%d] speed mode configuration[100M]\r\n", port);
		 }		
        else if(speed == TPM_PHY_SPEED_1000_MBPS)
        {
            printf("Port[%d] speed mode configuration[1000M]\r\n", port);
		 }		
        else
        {
            printf("Port[%d] speed mode configuration[Unknown mode]\r\n", port);
		 }						
    }

    return rc;
}

bool_t  tpm_sw_cli_get_uni_speed_state (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    tpm_phy_speed_t 		     speed;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
		
    tpm_rc = tpm_phy_get_port_speed_mode (owner_id, (tpm_src_port_type_t)port, &speed);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] speed mode state\r\n", port);
    }
    else
    {
        if(speed == TPM_PHY_SPEED_10_MBPS)
        {
            printf("Port[%d] speed mode state[10M] \n", port);
		 }
        else if(speed == TPM_PHY_SPEED_100_MBPS)
        {
            printf("Port[%d] speed mode state[100M]\r\n", port);
		 }		
        else if(speed == TPM_PHY_SPEED_1000_MBPS)
        {
            printf("Port[%d] speed mode state[1000M]\r\n", port);
		 }		
        else
        {
            printf("Port[%d] speed mode state[Unknown mode]\r\n", port);
		 }						
    }

    return rc;
}

bool_t  tpm_sw_cli_get_uni_loopback (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            mode;
    bool            state;		

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
	
    tpm_rc = tpm_phy_get_port_loopback(owner_id, (tpm_src_port_type_t)port, (tpm_phy_loopback_mode_t)mode, &state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] loopback state\r\n", port);
    }
    else
    {
        if(state == true)
        {
            printf("Port[%d] loopback state[Enabled] \n", port);
		 }
		 else
        {
            printf("Port[%d] loopback state[Disabled]\r\n", port);
		 }		
    }

    return rc;
}

bool_t  tpm_sw_cli_get_port_flooding (const clish_shell_t    *instance,
                                   const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            mode;
    uint8_t              allow;	


    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    tpm_rc = tpm_sw_get_port_flooding (owner_id, (tpm_src_port_type_t)port, (tpm_flood_type_t)mode, &allow);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] mode[%d] flooding state\r\n", port, mode);
    }
    else
    {
        if(allow == 1)
        {
            printf("Port[%d] mode[%d] flooding state[Enabled] \n", port, mode);
		 }
		 else
        {
            printf("Port[%d] mode[%d] flooding state[Disabled]\r\n", port, mode);
		 }		
    }

    return rc;
}

bool_t  tpm_sw_cli_get_uni_default_vlan (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            vlan;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    tpm_rc = tpm_sw_get_port_def_vlan(owner_id, (tpm_src_port_type_t)port, &vlan);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] default VLAN[%d]\r\n", port, vlan);
    }
    else
    {
        printf("Port[%d] default VLAN[%d] \n", port, vlan);
    }

    return rc;
}

bool_t  tpm_sw_cli_get_uni_default_pri (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint8_t            pri;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    tpm_rc = tpm_sw_get_port_def_pri (owner_id, (tpm_src_port_type_t)port, &pri);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] default VLAN pri[%d]\r\n", port, pri);
    }
    else
    {
        printf("Port[%d] default VLAN pri[%d] \n", port, pri);
    }

    return rc;
}

bool_t  tpm_sw_cli_get_mac_aging_time (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            time;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));

    tpm_rc = tpm_sw_get_mac_age_time (owner_id, &time);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get MAC aging time\r\n");
    }
    else
    {
        printf("MAC aging time[%d] \n", time);
    }

    return rc;
}
bool_t  tpm_sw_cli_get_uni_mac_learn(const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    bool            state;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    tpm_rc = tpm_sw_get_mac_learn (owner_id, (tpm_src_port_type_t)port, &state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port[%d] MAC learning state\r\n", port);
    }
    else
    {
        if(state == true)
        {
            printf("Port[%d] MAC learning state[Enabled] \n", port);
		 }
		 else
        {
            printf("Port[%d] MAC learning state[Disabled] \n", port);
		 }		
    }

    return rc;
}


bool_t  tpm_sw_cli_get_uni_port_mirror (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            sport;
    uint32_t            dport;		
    uint16_t            mode;
    bool            state;		

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    sport        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    dport        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));		
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 3));

	
    tpm_rc = tpm_sw_get_port_mirror(owner_id, (tpm_src_port_type_t)sport, (tpm_src_port_type_t)sport, (tpm_sw_mirror_type_t)mode, &state);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get port mirror sport[%d] dport[%d], mode[%d]\r\n", sport, dport, mode);
    }
    else
    {
        if(state == true)
        {
            printf("Port mirrorsport[%d] dport[%d], mode[%d] state[Enabled] \n", sport, dport, mode);
		 }
		 else
        {
            printf("Port mirrorsport[%d] dport[%d], mode[%d] state[Disabled] \n", sport, dport, mode);
		 }		
    }

    return rc;
}

bool_t  tpm_sw_cli_get_uni_mtu_size(const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            mode;		
    uint32_t            size;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    tpm_rc = tpm_get_mtu_size (owner_id, (tpm_mru_type_t)mode, &size);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get MTU size\r\n");
    }
    else
    {
    	if(mode == TPM_NETA_MTU_GMAC0)
    	{
            printf("GMAC0 MTU size[%d] \n", size);
		}
    	else if(mode == TPM_NETA_MTU_GMAC1)
    	{
            printf("GMAC1 MTU size[%d] \n", size);
		}
    	else if(mode == TPM_NETA_MTU_PONMAC)
    	{
            printf("PON GMAC MTU size[%d] \n", size);
		}
    	else if(mode == TPM_NETA_MTU_SWITCH)
    	{
            printf("Switch MTU size[%d] \n", size);
		}			
    }

    return rc;
}
bool_t  tpm_sw_cli_get_uni_isolate_vector(const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            vector;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    tpm_rc = tpm_sw_get_isolate_eth_port_vector (owner_id, (tpm_src_port_type_t)port, &vector);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to get UNI port isolate vector\r\n");
    }
    else
    {
        printf("Port[%d] isolate vector[0x%04x] \r\n", vector);
    }
		
    return rc;
}



