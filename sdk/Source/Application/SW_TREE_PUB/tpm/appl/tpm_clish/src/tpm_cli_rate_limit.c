/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "clish/shell.h"
#include "tpm_api.h"



bool_t tpm_cli_set_queue_sched( const clish_shell_t* instance,
                                const lub_argv_t*    argv )
{
    uint32_t            direction;
    uint32_t            owner_id;
    uint32_t            sched_entity;
    uint32_t            mode;
    uint32_t            queue_id;
    uint32_t            wrr_weight;
    tpm_error_code_t    ret_val; 


    direction           = atoi(lub_argv__get_arg(argv,0));
    owner_id            = atoi(lub_argv__get_arg(argv,1));
    sched_entity        = atoi(lub_argv__get_arg(argv,2));
    mode                = atoi(lub_argv__get_arg(argv,3));
    queue_id            = atoi(lub_argv__get_arg(argv,4));
    wrr_weight          = atoi(lub_argv__get_arg(argv,5));

     
    if (((direction == TPM_DOWNSTREAM)&& (sched_entity != 0x4000))||
        ((direction == TPM_UPSTREAM)  && (sched_entity == 0x4000))){
        printf("==ERROR ==%s:Mismatch between sched_entity %d and command direction %d \n\r",__FUNCTION__, sched_entity, direction);
        return BOOL_FALSE;
    }

    if (direction == TPM_DOWNSTREAM) {
        ret_val = tpm_tm_set_wan_ingr_queue_sched(owner_id,mode,queue_id,wrr_weight);
        if ( ret_val != TPM_RC_OK){
             printf("==Fail tpm_tm_set_wan_ingr_queue_sched  %d  \n\r", ret_val);
             return BOOL_FALSE;
        }
    }else {
        if (direction == TPM_UPSTREAM) {
            ret_val = tpm_tm_set_wan_egr_queue_sched(owner_id,sched_entity,mode,queue_id,wrr_weight);
            if ( ret_val != TPM_RC_OK){
                 printf("==Fail tpm_tm_set_wan_egr_queue_sched  %d  \n\r", ret_val);
                 return BOOL_FALSE;
            }
        }
        
    }

    
    return BOOL_TRUE;
}

bool_t tpm_cli_set_queue_rate(const clish_shell_t* instance,
                              const lub_argv_t*    argv)
{
    uint32_t            direction;
    uint32_t            owner_id;
    uint32_t            sched_entity;
    uint32_t            queue_id;
    uint32_t            rate_limit;
    uint32_t            bucket_size;
    tpm_error_code_t    ret_val; 


    direction           = atoi(lub_argv__get_arg(argv,0));
    owner_id            = atoi(lub_argv__get_arg(argv,1));
    sched_entity        = atoi(lub_argv__get_arg(argv,2));
    queue_id            = atoi(lub_argv__get_arg(argv,3));
    rate_limit          = atoi(lub_argv__get_arg(argv,4));
    bucket_size         = atoi(lub_argv__get_arg(argv,5));

    //jinghua modify for DS WAN rate-limit, Oct 28 2010 
    if (//((direction == TPM_DOWNSTREAM)&& (sched_entity != 0x4000))||
        ((direction == TPM_UPSTREAM)  && (sched_entity == 0x4000))){
        printf("==ERROR ==%s:Mismatch between sched_entity %d and command direction %d \n\r",__FUNCTION__, sched_entity, direction);
        return BOOL_FALSE;
    }

    if (direction == TPM_DOWNSTREAM) {
        ret_val = tpm_tm_set_wan_q_ingr_rate_lim(owner_id,queue_id,rate_limit,bucket_size);
        if ( ret_val != TPM_RC_OK){
             printf("==Fail tpm_tm_set_wan_q_ingr_rate_lim  %d  \n\r", ret_val);
             return BOOL_FALSE;
        }
    }else {
        if (direction == TPM_UPSTREAM) {
            ret_val = tpm_tm_set_wan_queue_egr_rate_lim(owner_id,sched_entity,queue_id,rate_limit,bucket_size);
            if ( ret_val != TPM_RC_OK){
                 printf("==Fail tpm_tm_set_wan_queue_egr_rate_lim  %d  \n\r", ret_val);
                 return BOOL_FALSE;
            }
        }
    }

    
    return BOOL_TRUE;
}
bool_t tpm_cli_set_rate(const clish_shell_t* instance,
                        const lub_argv_t*    argv)
{
    uint32_t            direction;
    uint32_t            owner_id;
    uint32_t            sched_entity;
    uint32_t            rate_limit;
    uint32_t            bucket_size;
    tpm_error_code_t    ret_val; 


    direction           = atoi(lub_argv__get_arg(argv,0));
    owner_id            = atoi(lub_argv__get_arg(argv,1));
    sched_entity        = atoi(lub_argv__get_arg(argv,2));
    rate_limit          = atoi(lub_argv__get_arg(argv,3));
    bucket_size         = atoi(lub_argv__get_arg(argv,4));


    //jinghua modify for DS WAN rate-limit, Oct 28 2010 
    if (//((direction == TPM_DOWNSTREAM)&& (sched_entity != 0x4000))||
        ((direction == TPM_UPSTREAM)  && (sched_entity == 0x4000))){
        printf("==ERROR ==%s:Mismatch between sched_entity %d and command direction %d \n\r",__FUNCTION__, sched_entity, direction);
        return BOOL_FALSE;
    }

    if (direction == TPM_DOWNSTREAM) {
        ret_val = tpm_tm_set_wan_ingr_rate_lim(owner_id,rate_limit,bucket_size);
        if ( ret_val != TPM_RC_OK){
             printf("==Fail tpm_tm_set_wan_ingr_rate_lim  %d  \n\r", ret_val);
             return BOOL_FALSE;
        }
        
    }else {
        if (direction == TPM_UPSTREAM) {
            ret_val = tpm_tm_set_wan_sched_egr_rate_lim(owner_id,sched_entity,rate_limit,bucket_size);
            if ( ret_val != TPM_RC_OK){
                 printf("==Fail tpm_tm_set_wan_sched_egr_rate_lim  %d  \n\r", ret_val);
                 return BOOL_FALSE;
            }

        }
    }

    
    return BOOL_TRUE;
}

