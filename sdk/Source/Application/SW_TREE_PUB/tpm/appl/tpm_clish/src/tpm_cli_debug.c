/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/********************************************************************************
*      tpm_cli_debug.c
*
* DESCRIPTION:
*
*
* CREATED BY:  ShpundA
*
* DATE CREATED: June 13, 2010
*
* DEPENDENCIES:
*
*
* FILE REVISION NUMBER:
*       $Revision: 1.3 $
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "clish/shell.h"
#include "tpm_types.h"
#include "tpm_internal_types.h"
#include "tpm_print.h"


/********************************************************************************/
/*                      TPM CLI debug SET functions                             */
/********************************************************************************/
bool_t  tpm_cli_set_trace_level (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            level;
    uint32_t            module;
    uint32_t            set_only_level;

    /* Get parameters */
    level = atoi(lub_argv__get_arg(argv, 0));

    tpm_rc = tpm_trace_set(level);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to set trace \n");
    }
    else
    {
        printf("New trace status:\n");
        tpm_trace_status_print();
    }

    return rc;
}

bool_t  tpm_cli_set_trace_module (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            module;

    /* Get parameters */
    module   = atoi(lub_argv__get_arg(argv, 0));

    tpm_rc = tpm_trace_module_set(module, 1);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to add application module to the existing trace\n");
    }
    else
    {
        printf("New trace status:\n");
        tpm_trace_status_print();
    }

    return rc;
}

bool_t  tpm_cli_del_trace_module (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            module;

    /* Get parameters */
    module   = atoi(lub_argv__get_arg(argv, 0));

    tpm_rc = tpm_trace_module_set(module, 0);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to add application module to the existing trace\n");
    }
    else
    {
        printf("New trace status:\n");
        tpm_trace_status_print();
    }

    return rc;
}

/********************************************************************************/
/*                      TPM CLI debug PRINT functions                           */
/********************************************************************************/
bool_t  tpm_cli_print_trace (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;

    tpm_rc = tpm_trace_status_print ();

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to print trace status\n");
    }

    return rc;
}

bool_t  tpm_cli_print_misc  (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
    tpm_print_misc();
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_igmp  (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
    tpm_print_igmp();
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_owners  (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    tpm_print_owners();
    return BOOL_TRUE;
}


bool_t  tpm_cli_print_vlan_etype    (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    tpm_print_vlan_etype();
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_init_tables   (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    tpm_print_init_tables();
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_eth_ports     (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    tpm_print_etherports();
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_tx_modules    (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    tpm_print_tx_modules();
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_rx_modules    (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    tpm_print_rx_modules();
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_gmac_func     (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    tpm_print_gmac_func();
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_gmac_conf    (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
    tpm_print_gmac_config();
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_api_full_range    (const clish_shell_t    *instance,
                                         const lub_argv_t       *argv)
{
    uint32_t    api, dir;

    /* Get parameters */
    api = atoi(lub_argv__get_arg(argv, 0));
    dir = atoi(lub_argv__get_arg(argv, 1));

    tpm_print_full_api_range((tpm_api_sections_t)api, dir);
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_api_valid_range    (const clish_shell_t    *instance,
                                          const lub_argv_t       *argv)
{
    tpm_print_valid_api_ranges();
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_shadow_mod_range  (const clish_shell_t    *instance,
                                         const lub_argv_t       *argv)
{
    uint32_t    gmac, valid, start, end;

    /* Get parameters */
    gmac    = atoi(lub_argv__get_arg(argv, 0));
    valid   = atoi(lub_argv__get_arg(argv, 1));
    start   = atoi(lub_argv__get_arg(argv, 2));
    end     = atoi(lub_argv__get_arg(argv, 3));

    tpm_print_mod_shadow_range((tpm_gmacs_enum_t)gmac, valid, start, end);
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_shadow_pnc_range  (const clish_shell_t    *instance,
                                         const lub_argv_t       *argv)
{
    uint32_t    valid, start, end;

    /* Get parameters */
    valid   = atoi(lub_argv__get_arg(argv, 0));
    start   = atoi(lub_argv__get_arg(argv, 1));
    end     = atoi(lub_argv__get_arg(argv, 2));

    tpm_print_pnc_shadow_range(valid, start, end);
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_valid_pnc_range   (const clish_shell_t    *instance,
                                         const lub_argv_t       *argv)
{
    tpm_print_valid_pnc_ranges();
    return BOOL_TRUE;
}

