/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/********************************************************************************
*      tpm_cli.c
*
* DESCRIPTION:
*
*
* CREATED BY:  ShpundA
*
* DATE CREATED: June 13, 2010
*
* DEPENDENCIES:
*
*
* FILE REVISION NUMBER:
*       $Revision: 1.16 $
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "tpm_api.h"
#include "tpm_print.h"
#include "tpm_cli_db.h"


/********************************************************************************/
/*                          TPM CLI internal functions                          */
/********************************************************************************/
uint32_t tpm_cli_get_number (const char *arg)
{
    uint32_t    val = 0;

    if (db_hexadec_arg(arg) == BOOL_TRUE)
        sscanf(&arg[2], "%x", &val);
    else
        val = atoi(arg);

    return val;
}

void tpm_cli_ipv6_src_to_sw (uint32_t     *src_addr,
                                    uint8_t      *sw_addr)
{
    int         i, j;
    uint16_t    tmp;

    for (i=0,j=0; j<DB_CLI_IPV6_ADDR_LEN/2; j++)
    {
        tmp = src_addr[j];
        sw_addr[i++] = tmp >> 8;
        sw_addr[i++] = tmp & 0x00FF;
    }
}



/********************************************************************************/
/*                      TPM CLI show DB data functions                          */
/********************************************************************************/
bool_t  tpm_cli_print_vlan_table (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    db_cli_print_vlan_table();
    return BOOL_TRUE;
}


bool_t  tpm_cli_print_vlan_table_by_name (const clish_shell_t    *instance,
                                          const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_vlan_table_entry(name);
    return rc;
}


bool_t  tpm_cli_print_l2key_table (const clish_shell_t    *instance,
                                   const lub_argv_t       *argv)
{
    db_cli_print_l2key_table();
    return BOOL_TRUE;
}


bool_t  tpm_cli_print_l2key_table_by_name (const clish_shell_t    *instance,
                                           const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_l2key_table_entry(name);
    return rc;
}


bool_t  tpm_cli_print_l2key_mac (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_l2key_table_mac(name);
    return rc;
}


bool_t  tpm_cli_print_l2key_vlan (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_l2key_table_vlan(name);
    return rc;
}


bool_t  tpm_cli_print_l2key_pppoe (const clish_shell_t    *instance,
                                   const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_l2key_table_pppoe(name);
    return rc;
}


bool_t  tpm_cli_print_l2key_ety (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_l2key_table_ety(name);
    return rc;
}


bool_t  tpm_cli_print_l2key_gem (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_l2key_table_gem(name);
    return rc;
}


bool_t  tpm_cli_print_mod_table (const clish_shell_t    *instance,
                                   const lub_argv_t       *argv)
{
    db_cli_print_mod_table();
    return BOOL_TRUE;
}


bool_t  tpm_cli_print_mod_table_by_name (const clish_shell_t    *instance,
                                           const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_mod_table_entry(name);
    return rc;
}

bool_t  tpm_cli_print_frwd_table (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
    db_cli_print_frwd_table();
    return BOOL_TRUE;
}

bool_t  tpm_cli_print_frwd_table_by_name (const clish_shell_t    *instance,
                                            const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_frwd_table_entry(name);
    return rc;
}

bool_t  tpm_cli_print_l3key_table (const clish_shell_t    *instance,
                                   const lub_argv_t       *argv)
{
    db_cli_print_l3key_table();
    return BOOL_TRUE;
}


bool_t  tpm_cli_print_l3key_table_by_name (const clish_shell_t    *instance,
                                           const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_l3key_table_entry(name);
    return rc;
}

bool_t  tpm_cli_print_ipv4_key_table (const clish_shell_t    *instance,
                                      const lub_argv_t       *argv)
{
    db_cli_print_ipv4_key_table();
    return BOOL_TRUE;
}


bool_t  tpm_cli_print_ipv4_key_table_by_name (const clish_shell_t    *instance,
                                              const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_ipv4_key_table_entry(name);
    return rc;
}


bool_t  tpm_cli_print_ipv6_key_table (const clish_shell_t    *instance,
                                      const lub_argv_t       *argv)
{
    db_cli_print_ipv6_key_table();
    return BOOL_TRUE;
}


bool_t  tpm_cli_print_ipv6_key_table_by_name (const clish_shell_t    *instance,
                                              const lub_argv_t       *argv)
{
    bool_t      rc;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    rc = db_cli_print_ipv6_key_table_entry(name);
    return rc;
}



/********************************************************************************/
/*                      TPM CLI add DB functions                                */
/********************************************************************************/

bool_t  tpm_cli_add_vlan  (const clish_shell_t    *instance,
                           const lub_argv_t       *argv)
{
    bool_t      rc = BOOL_TRUE;
    uint16_t    tpid;
    uint16_t    vid;
    uint16_t    vid_mask;
    uint8_t     cfi;
    uint8_t     cfi_mask;
    uint8_t     pbit;
    uint8_t     pbit_mask;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    tpid        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    vid         = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    vid_mask    = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    cfi         = tpm_cli_get_number(lub_argv__get_arg(argv, 4));
    cfi_mask    = tpm_cli_get_number(lub_argv__get_arg(argv, 5));
    pbit        = tpm_cli_get_number(lub_argv__get_arg(argv, 6));
    pbit_mask   = tpm_cli_get_number(lub_argv__get_arg(argv, 7));

    DBG_PRINT("VLAN name = %s TPID = %x VID = %d(%4.4x) CFI = %d(%x) PBIT = %d(%x)",
              name, tpid, vid, vid_mask, cfi, cfi_mask, pbit, pbit_mask);

    rc = db_cli_add_vlan_to_vlan_tbl(name, tpid, vid, vid_mask, cfi, cfi_mask, pbit, pbit_mask);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}


bool_t  tpm_cli_add_l2key_ety  (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint32_t            ety;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    ety     = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    DBG_PRINT("Name = %s ETY = %x", name, ety);

    rc = db_cli_add_ety_to_l2key_tbl(name, ety);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}


bool_t  tpm_cli_add_l2key_gem  (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint32_t            gem;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    gem     = atoi(lub_argv__get_arg(argv, 1));

    DBG_PRINT("Name = %s GEM = %d", name, gem);

    rc = db_cli_add_gem_to_l2key_tbl(name, gem);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}


bool_t  tpm_cli_add_l2key_pppoe  (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint16_t            session, protocol;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    session = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    protocol= tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Name = %s Session = %d Proto = 0x%4.4x", name, session, protocol);

    rc = db_cli_add_pppoe_to_l2key_tbl(name, session, protocol);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}


bool_t  tpm_cli_add_l2key_mac_address  (const clish_shell_t    *instance,
                                        const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    const char          *sa;
    const char          *sa_mask;
    const char          *da;
    const char          *da_mask;
    tpm_mac_key_t       mac_key;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    da      = lub_argv__get_arg(argv, 1);
    da_mask = lub_argv__get_arg(argv, 2);
    sa      = lub_argv__get_arg(argv, 3);
    sa_mask = lub_argv__get_arg(argv, 4);

    DBG_PRINT("Name = %s   %s(%s)    %s(%s)", name, da, da_mask, sa, sa_mask);

    sscanf(da, "%x:%x:%x:%x:%x:%x",
           &(mac_key.mac_da[0]), &(mac_key.mac_da[1]), &(mac_key.mac_da[2]),
           &(mac_key.mac_da[3]), &(mac_key.mac_da[4]), &(mac_key.mac_da[5]));

    sscanf(da_mask, "%x:%x:%x:%x:%x:%x",
           &(mac_key.mac_da_mask[0]), &(mac_key.mac_da_mask[1]), &(mac_key.mac_da_mask[2]),
           &(mac_key.mac_da_mask[3]), &(mac_key.mac_da_mask[4]), &(mac_key.mac_da_mask[5]));

    sscanf(sa, "%x:%x:%x:%x:%x:%x",
           &(mac_key.mac_sa[0]), &(mac_key.mac_sa[1]), &(mac_key.mac_sa[2]),
           &(mac_key.mac_sa[3]), &(mac_key.mac_sa[4]), &(mac_key.mac_sa[5]));

    sscanf(sa_mask, "%x:%x:%x:%x:%x:%x",
           &(mac_key.mac_sa_mask[0]), &(mac_key.mac_sa_mask[1]), &(mac_key.mac_sa_mask[2]),
           &(mac_key.mac_sa_mask[3]), &(mac_key.mac_sa_mask[4]), &(mac_key.mac_sa_mask[5]));

    DBG_PRINT("DA %x:%x:%x:%x:%x:%x",
              mac_key.mac_da[0], mac_key.mac_da[1], mac_key.mac_da[2],
              mac_key.mac_da[3], mac_key.mac_da[4], mac_key.mac_da[5]);
    DBG_PRINT("DA mask %x:%x:%x:%x:%x:%x",
              mac_key.mac_da_mask[0], mac_key.mac_da_mask[1], mac_key.mac_da_mask[2],
              mac_key.mac_da_mask[3], mac_key.mac_da_mask[4], mac_key.mac_da_mask[5]);
    DBG_PRINT("SA %x:%x:%x:%x:%x:%x",
              mac_key.mac_sa[0], mac_key.mac_sa[1], mac_key.mac_sa[2],
              mac_key.mac_sa[3], mac_key.mac_sa[4], mac_key.mac_sa[5]);
    DBG_PRINT("SA mask %x:%x:%x:%x:%x:%x",
              mac_key.mac_sa_mask[0], mac_key.mac_sa_mask[1], mac_key.mac_sa_mask[2],
              mac_key.mac_sa_mask[3], mac_key.mac_sa_mask[4], mac_key.mac_sa_mask[5]);

    rc = db_cli_add_mac_to_l2key_tbl(name, &mac_key);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_l2key_vlan  (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    bool_t      rc = BOOL_TRUE;
    const char  *v1_name, *v2_name;
    const char  name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    v1_name     = lub_argv__get_arg(argv, 1);
    v2_name     = lub_argv__get_arg(argv, 2);

    DBG_PRINT("name = %s VLAN1 name = %s VLAN2 name = %s",
              name, v1_name, v2_name);

    rc = db_cli_add_vlan_to_l2key_tbl(name, v1_name, v2_name);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}


bool_t  tpm_cli_add_pkt_frwd_data  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint16_t            gem;
    uint32_t            port;
    uint8_t             que;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    port    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    que     = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    gem     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));

    DBG_PRINT("Name = %s port = 0x%4.4x queue = %d gem = %d", name, port, que, gem);

    rc = db_cli_add_frwd_to_db_tbl(name, port, que, gem);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}


bool_t  tpm_cli_add_mod_vlan  (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    int                 oper;
    const char          *v1_name, *v2_name;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    oper    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    v1_name = lub_argv__get_arg(argv, 2);
    v2_name = lub_argv__get_arg(argv, 3);

    DBG_PRINT("Name = %s oper = %d VLAN1 = %s VLAN2 = %s",
              name, oper, v1_name, v2_name);

    rc = db_cli_add_vlan_to_mod_tbl(name, (tpm_vlan_oper_t)oper, v1_name, v2_name);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_mod_mac_address  (const clish_shell_t    *instance,
                                      const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    const char          *sa;
    const char          *sa_mask;
    const char          *da;
    const char          *da_mask;
    tpm_mac_key_t       mac_key;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);

    da      = lub_argv__get_arg(argv, 1);
    da_mask = lub_argv__get_arg(argv, 2);
    sa      = lub_argv__get_arg(argv, 3);
    sa_mask = lub_argv__get_arg(argv, 4);

    DBG_PRINT("Name = %s   %s(%s)    %s(%s)", name, da, da_mask, sa, sa_mask);

    sscanf(da, "%x:%x:%x:%x:%x:%x",
           &(mac_key.mac_da[0]), &(mac_key.mac_da[1]), &(mac_key.mac_da[2]),
           &(mac_key.mac_da[3]), &(mac_key.mac_da[4]), &(mac_key.mac_da[5]));

    sscanf(da_mask, "%x:%x:%x:%x:%x:%x",
           &(mac_key.mac_da_mask[0]), &(mac_key.mac_da_mask[1]), &(mac_key.mac_da_mask[2]),
           &(mac_key.mac_da_mask[3]), &(mac_key.mac_da_mask[4]), &(mac_key.mac_da_mask[5]));

    sscanf(sa, "%x:%x:%x:%x:%x:%x",
           &(mac_key.mac_sa[0]), &(mac_key.mac_sa[1]), &(mac_key.mac_sa[2]),
           &(mac_key.mac_sa[3]), &(mac_key.mac_sa[4]), &(mac_key.mac_sa[5]));

    sscanf(sa_mask, "%x:%x:%x:%x:%x:%x",
           &(mac_key.mac_sa_mask[0]), &(mac_key.mac_sa_mask[1]), &(mac_key.mac_sa_mask[2]),
           &(mac_key.mac_sa_mask[3]), &(mac_key.mac_sa_mask[4]), &(mac_key.mac_sa_mask[5]));

    DBG_PRINT("DA %x:%x:%x:%x:%x:%x",
              mac_key.mac_da[0], mac_key.mac_da[1], mac_key.mac_da[2],
              mac_key.mac_da[3], mac_key.mac_da[4], mac_key.mac_da[5]);
    DBG_PRINT("DA mask %x:%x:%x:%x:%x:%x",
              mac_key.mac_da_mask[0], mac_key.mac_da_mask[1], mac_key.mac_da_mask[2],
              mac_key.mac_da_mask[3], mac_key.mac_da_mask[4], mac_key.mac_da_mask[5]);
    DBG_PRINT("SA %x:%x:%x:%x:%x:%x",
              mac_key.mac_sa[0], mac_key.mac_sa[1], mac_key.mac_sa[2],
              mac_key.mac_sa[3], mac_key.mac_sa[4], mac_key.mac_sa[5]);
    DBG_PRINT("SA mask %x:%x:%x:%x:%x:%x",
              mac_key.mac_sa_mask[0], mac_key.mac_sa_mask[1], mac_key.mac_sa_mask[2],
              mac_key.mac_sa_mask[3], mac_key.mac_sa_mask[4], mac_key.mac_sa_mask[5]);

    rc = db_cli_add_mac_to_mod_tbl(name, &mac_key);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_mod_pppoe  (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint16_t            session, protocol;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    session = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    protocol= tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Name = %s Session = %d Proto = 0x%4.4x", name, session, protocol);

    rc = db_cli_add_pppoe_to_mod_tbl(name, session, protocol);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_mod_dscp  (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint8_t             dscp, mask;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    dscp    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mask    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Name = %s DSCP = %d Mask = 0x%2.2x", name, dscp, mask);

    rc = db_cli_add_dscp_to_mod_tbl(name, dscp, mask);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_mod_protocol  (const clish_shell_t    *instance,
                                   const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint8_t             protocol;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    protocol = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    DBG_PRINT("Name = %s protocol = %d ", name, protocol);

    rc = db_cli_add_proto_to_mod_tbl(name, protocol);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_mod_ipv4_ports  (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint16_t            src, dst;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    src    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    dst    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Name = %s SRC port = %d DST port = %d", name, src, dst);

    rc = db_cli_add_ports_to_ipv4_mod_tbl(name, src, dst);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_mod_ipv6_ports  (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint16_t            src, dst;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    src    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    dst    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Name = %s SRC port = %d DST port = %d", name, src, dst);

    rc = db_cli_add_ports_to_ipv6_mod_tbl(name, src, dst);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_mod_ipv4_addr  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    const char          *sa;
    const char          *sa_mask;
    const char          *da;
    const char          *da_mask;
    const char          name[DB_CLI_MAX_NAME+1];
    uint32_t            src[DB_CLI_IPV4_ADDR_LEN];
    uint32_t            src_mask[DB_CLI_IPV4_ADDR_LEN];
    uint32_t            dst[DB_CLI_IPV4_ADDR_LEN];
    uint32_t            dst_mask[DB_CLI_IPV4_ADDR_LEN];
    uint8_t             src_tpm[DB_CLI_IPV4_ADDR_LEN];
    uint8_t             src_mask_tpm[DB_CLI_IPV4_ADDR_LEN];
    uint8_t             dst_tpm[DB_CLI_IPV4_ADDR_LEN];
    uint8_t             dst_mask_tpm[DB_CLI_IPV4_ADDR_LEN];
    int                 i;

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    sa      = lub_argv__get_arg(argv, 1);
    sa_mask = lub_argv__get_arg(argv, 2);
    da      = lub_argv__get_arg(argv, 3);
    da_mask = lub_argv__get_arg(argv, 4);

    sscanf(sa, "%u.%u.%u.%u", &src[0], &src[1], &src[2], &src[3]);
    sscanf(sa_mask, "%u.%u.%u.%u", &src_mask[0], &src_mask[1], &src_mask[2], &src_mask[3]);
    sscanf(da, "%u.%u.%u.%u", &dst[0], &dst[1], &dst[2], &dst[3]);
    sscanf(da_mask, "%u.%u.%u.%u", &dst_mask[0], &dst_mask[1], &dst_mask[2], &dst_mask[3]);

    DBG_PRINT("SRC %u.%u.%u.%u", src[0], src[1], src[2], src[3]);
    DBG_PRINT("SRC mask %u.%u.%u.%u", src_mask[0], src_mask[1], src_mask[2], src_mask[3]);
    DBG_PRINT("DST %u.%u.%u.%u", dst[0], dst[1], dst[2], dst[3]);
    DBG_PRINT("DST mask %u.%u.%u.%u", dst_mask[0], dst_mask[1], dst_mask[2], dst_mask[3]);

    for (i=0; i<DB_CLI_IPV4_ADDR_LEN; i++)
    {
        src_tpm[i] = src[i];
        src_mask_tpm[i] = src_mask[i];
        dst_tpm[i] = dst[i];
        dst_mask_tpm[i] = dst_mask[i];
    }

    rc = db_cli_add_ips_to_ipv4_mod_tbl(name, src_tpm, src_mask_tpm, dst_tpm, dst_mask_tpm);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_mod_ipv6_addr  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint8_t             src[DB_CLI_IPV6_ADDR_LEN];
    uint8_t             dst[DB_CLI_IPV6_ADDR_LEN];
    uint8_t             src_mask[DB_CLI_IPV6_ADDR_LEN];
    uint8_t             dst_mask[DB_CLI_IPV6_ADDR_LEN];
    uint32_t            src2[DB_CLI_IPV6_ADDR_LEN/2];
    uint32_t            dst2[DB_CLI_IPV6_ADDR_LEN/2];
    uint32_t            src_mask2[DB_CLI_IPV6_ADDR_LEN/2];
    uint32_t            dst_mask2[DB_CLI_IPV6_ADDR_LEN/2];
    const char          *sa;
    const char          *sa_mask;
    const char          *da;
    const char          *da_mask;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    sa      = lub_argv__get_arg(argv, 1);
    sa_mask = lub_argv__get_arg(argv, 2);
    da      = lub_argv__get_arg(argv, 3);
    da_mask = lub_argv__get_arg(argv, 4);

    DBG_PRINT("Name = %s   %s(%s)    %s(%s)", name, sa, sa_mask, da, da_mask);

    sscanf(sa, "%x:%x:%x:%x:%x:%x:%x:%x",
           &(src2[0]), &(src2[1]), &(src2[2]), &(src2[3]), &(src2[4]), &(src2[5]), &(src2[6]), &(src2[7]));
    sscanf(sa_mask, "%x:%x:%x:%x:%x:%x:%x:%x",
           &(src_mask2[0]), &(src_mask2[1]), &(src_mask2[2]), &(src_mask2[3]),
           &(src_mask2[4]), &(src_mask2[5]), &(src_mask2[6]), &(src_mask2[7]));
    sscanf(da, "%x:%x:%x:%x:%x:%x:%x:%x",
           &(dst2[0]), &(dst2[1]), &(dst2[2]), &(dst2[3]), &(dst2[4]), &(dst2[5]), &(dst2[6]), &(dst2[7]));
    sscanf(da_mask, "%x:%x:%x:%x:%x:%x:%x:%x",
           &(dst_mask2[0]), &(dst_mask2[1]), &(dst_mask2[2]), &(dst_mask2[3]),
           &(dst_mask2[4]), &(dst_mask2[5]), &(dst_mask2[6]), &(dst_mask2[7]));

    tpm_cli_ipv6_src_to_sw(src2, src);
    tpm_cli_ipv6_src_to_sw(src_mask2, src_mask);
    tpm_cli_ipv6_src_to_sw(dst2, dst);
    tpm_cli_ipv6_src_to_sw(dst_mask2, dst_mask);

    DBG_PRINT("SRC %2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x",
              src[0], src[1], src[2], src[3], src[4], src[5], src[6], src[7],
              src[8], src[9], src[10], src[11], src[12], src[13], src[14], src[15]);
    DBG_PRINT("SRC mask %2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x",
              src_mask[0], src_mask[1], src_mask[2], src_mask[3], src_mask[4], src_mask[5], src_mask[6], src_mask[7],
              src_mask[8], src_mask[9], src_mask[10], src_mask[11], src_mask[12], src_mask[13], src_mask[14], src_mask[15]);
    DBG_PRINT("DST %2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x",
              dst[0], dst[1], dst[2], dst[3], dst[4], dst[5], dst[6], dst[7],
              dst[8], dst[9], dst[10], dst[11], dst[12], dst[13], dst[14], dst[15]);
    DBG_PRINT("DST mask %2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x",
              dst_mask[0], dst_mask[1], dst_mask[2], dst_mask[3], dst_mask[4], dst_mask[5], dst_mask[6], dst_mask[7],
              dst_mask[8], dst_mask[9], dst_mask[10], dst_mask[11], dst_mask[12], dst_mask[13], dst_mask[14], dst_mask[15]);

    rc = db_cli_add_ips_to_ipv6_mod_tbl(name, src, src_mask, dst, dst_mask);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}


bool_t  tpm_cli_add_l3key_ety  (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint32_t            ety;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    ety     = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    DBG_PRINT("Name = %s ETY = %x", name, ety);

    rc = db_cli_add_ety_to_l3key_tbl(name, ety);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_l3key_pppoe  (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint16_t            session, protocol;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    session = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    protocol= tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Name = %s Session = %d Proto = 0x%4.4x", name, session, protocol);

    rc = db_cli_add_pppoe_to_l3key_tbl(name, session, protocol);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_ipv4_key_dscp  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint8_t             dscp, mask;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    dscp    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mask    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Name = %s DSCP = %d Mask = 0x%2.2x", name, dscp, mask);

    rc = db_cli_add_dscp_to_ipv4_key_tbl(name, dscp, mask);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_ipv4_key_protocol  (const clish_shell_t    *instance,
                                        const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint16_t            protocol;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    protocol = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    DBG_PRINT("Name = %s protocol = %d ", name, protocol);

    rc = db_cli_add_proto_to_ipv4_key_tbl(name, protocol);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_ipv4_key_ports  (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint16_t            src, dst;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    src    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    dst    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Name = %s SRC port = %d DST port = %d", name, src, dst);

    rc = db_cli_add_ports_to_ipv4_key_tbl(name, src, dst);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_ipv4_key_addr  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    const char          *sa;
    const char          *sa_mask;
    const char          *da;
    const char          *da_mask;
    const char          name[DB_CLI_MAX_NAME+1];
    uint32_t            src[DB_CLI_IPV4_ADDR_LEN];
    uint32_t            src_mask[DB_CLI_IPV4_ADDR_LEN];
    uint32_t            dst[DB_CLI_IPV4_ADDR_LEN];
    uint32_t            dst_mask[DB_CLI_IPV4_ADDR_LEN];
    uint8_t             src_tpm[DB_CLI_IPV4_ADDR_LEN];
    uint8_t             src_mask_tpm[DB_CLI_IPV4_ADDR_LEN];
    uint8_t             dst_tpm[DB_CLI_IPV4_ADDR_LEN];
    uint8_t             dst_mask_tpm[DB_CLI_IPV4_ADDR_LEN];
    int                 i;

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    sa      = lub_argv__get_arg(argv, 1);
    sa_mask = lub_argv__get_arg(argv, 2);
    da      = lub_argv__get_arg(argv, 3);
    da_mask = lub_argv__get_arg(argv, 4);

    sscanf(sa, "%u.%u.%u.%u", &src[0], &src[1], &src[2], &src[3]);
    sscanf(sa_mask, "%u.%u.%u.%u", &src_mask[0], &src_mask[1], &src_mask[2], &src_mask[3]);
    sscanf(da, "%u.%u.%u.%u", &dst[0], &dst[1], &dst[2], &dst[3]);
    sscanf(da_mask, "%u.%u.%u.%u", &dst_mask[0], &dst_mask[1], &dst_mask[2], &dst_mask[3]);

    DBG_PRINT("SRC %u.%u.%u.%u", src[0], src[1], src[2], src[3]);
    DBG_PRINT("SRC mask %u.%u.%u.%u", src_mask[0], src_mask[1], src_mask[2], src_mask[3]);
    DBG_PRINT("DST %u.%u.%u.%u", dst[0], dst[1], dst[2], dst[3]);
    DBG_PRINT("DST mask %u.%u.%u.%u", dst_mask[0], dst_mask[1], dst_mask[2], dst_mask[3]);

    for (i=0; i<DB_CLI_IPV4_ADDR_LEN; i++)
    {
        src_tpm[i] = src[i];
        src_mask_tpm[i] = src_mask[i];
        dst_tpm[i] = dst[i];
        dst_mask_tpm[i] = dst_mask[i];
    }

    rc = db_cli_add_ips_to_ipv4_key_tbl(name, src_tpm, src_mask_tpm, dst_tpm, dst_mask_tpm);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_ipv6_key_dscp  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint8_t             dscp, mask;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    dscp    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mask    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Name = %s DSCP = %d Mask = 0x%2.2x", name, dscp, mask);

    rc = db_cli_add_dscp_to_ipv6_key_tbl(name, dscp, mask);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_ipv6_key_protocol  (const clish_shell_t    *instance,
                                        const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint16_t            protocol;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    protocol = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    DBG_PRINT("Name = %s protocol = %d ", name, protocol);

    rc = db_cli_add_proto_to_ipv6_key_tbl(name, protocol);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_ipv6_key_ports  (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint16_t            src, dst;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    src    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    dst    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Name = %s SRC port = %d DST port = %d", name, src, dst);

    rc = db_cli_add_ports_to_ipv6_key_tbl(name, src, dst);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}

bool_t  tpm_cli_add_ipv6_key_addr  (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    uint8_t             src[DB_CLI_IPV6_ADDR_LEN];
    uint8_t             dst[DB_CLI_IPV6_ADDR_LEN];
    uint8_t             src_mask[DB_CLI_IPV6_ADDR_LEN];
    uint8_t             dst_mask[DB_CLI_IPV6_ADDR_LEN];
    uint32_t            src2[DB_CLI_IPV6_ADDR_LEN/2];
    uint32_t            dst2[DB_CLI_IPV6_ADDR_LEN/2];
    uint32_t            src_mask2[DB_CLI_IPV6_ADDR_LEN/2];
    uint32_t            dst_mask2[DB_CLI_IPV6_ADDR_LEN/2];
    const char          *sa;
    const char          *sa_mask;
    const char          *da;
    const char          *da_mask;
    const char          name[DB_CLI_MAX_NAME+1];

    /* Get parameters */
    memcpy(&name, lub_argv__get_arg(argv, 0), DB_CLI_MAX_NAME);
    sa      = lub_argv__get_arg(argv, 1);
    sa_mask = lub_argv__get_arg(argv, 2);
    da      = lub_argv__get_arg(argv, 3);
    da_mask = lub_argv__get_arg(argv, 4);

    DBG_PRINT("Name = %s   %s(%s)    %s(%s)", name, sa, sa_mask, da, da_mask);

    sscanf(sa, "%x:%x:%x:%x:%x:%x:%x:%x",
           &(src2[0]), &(src2[1]), &(src2[2]), &(src2[3]), &(src2[4]), &(src2[5]), &(src2[6]), &(src2[7]));
    sscanf(sa_mask, "%x:%x:%x:%x:%x:%x:%x:%x",
           &(src_mask2[0]), &(src_mask2[1]), &(src_mask2[2]), &(src_mask2[3]),
           &(src_mask2[4]), &(src_mask2[5]), &(src_mask2[6]), &(src_mask2[7]));
    sscanf(da, "%x:%x:%x:%x:%x:%x:%x:%x",
           &(dst2[0]), &(dst2[1]), &(dst2[2]), &(dst2[3]), &(dst2[4]), &(dst2[5]), &(dst2[6]), &(dst2[7]));
    sscanf(da_mask, "%x:%x:%x:%x:%x:%x:%x:%x",
           &(dst_mask2[0]), &(dst_mask2[1]), &(dst_mask2[2]), &(dst_mask2[3]),
           &(dst_mask2[4]), &(dst_mask2[5]), &(dst_mask2[6]), &(dst_mask2[7]));

    tpm_cli_ipv6_src_to_sw(src2, src);
    tpm_cli_ipv6_src_to_sw(src_mask2, src_mask);
    tpm_cli_ipv6_src_to_sw(dst2, dst);
    tpm_cli_ipv6_src_to_sw(dst_mask2, dst_mask);

    DBG_PRINT("SRC %2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x",
              src[0], src[1], src[2], src[3], src[4], src[5], src[6], src[7],
              src[8], src[9], src[10], src[11], src[12], src[13], src[14], src[15]);
    DBG_PRINT("SRC mask %2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x",
              src_mask[0], src_mask[1], src_mask[2], src_mask[3], src_mask[4], src_mask[5], src_mask[6], src_mask[7],
              src_mask[8], src_mask[9], src_mask[10], src_mask[11], src_mask[12], src_mask[13], src_mask[14], src_mask[15]);
    DBG_PRINT("DST %2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x",
              dst[0], dst[1], dst[2], dst[3], dst[4], dst[5], dst[6], dst[7],
              dst[8], dst[9], dst[10], dst[11], dst[12], dst[13], dst[14], dst[15]);
    DBG_PRINT("DST mask %2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x",
              dst_mask[0], dst_mask[1], dst_mask[2], dst_mask[3], dst_mask[4], dst_mask[5], dst_mask[6], dst_mask[7],
              dst_mask[8], dst_mask[9], dst_mask[10], dst_mask[11], dst_mask[12], dst_mask[13], dst_mask[14], dst_mask[15]);

    rc = db_cli_add_ips_to_ipv6_key_tbl(name, src, src_mask, dst, dst_mask);

    if (rc == BOOL_TRUE)
        PRINT("Action done");
    else
        PRINT("Action failed");

    return rc;
}


/********************************************************************************/
/*                      TPM CLI add rule functions                              */
/********************************************************************************/
bool_t  tpm_cli_add_l2_prim_rule    (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
#if 0  /*user space TPM CLI will be removed in future, so just mask it to avoid compiling error*/   
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index = 0;
    uint32_t            rule_num;
    uint32_t            port;
    uint32_t            rule_bm;
    uint32_t            action;
    uint32_t            stage;
    tpm_l2_acl_key_t    l2_key;
    tpm_pkt_frwd_t      pkt_frwd;
    tpm_pkt_mod_t       pkt_mod;
    tpm_l2_acl_key_t    *l2_key_ptr = NULL;
    tpm_pkt_frwd_t      *pkt_frwd_ptr = NULL;
    tpm_pkt_mod_t       *pkt_mod_ptr = NULL;
    tpm_rule_action_t   rule_action;
    const char          key_name[DB_CLI_MAX_NAME+1] = "";
    const char          frwd_name[DB_CLI_MAX_NAME+1] = "";
    const char          mod_name[DB_CLI_MAX_NAME+1] = "";
    const char          *str_ptr;
    bool_t              l2key_data_exist = BOOL_FALSE;
    bool_t              frwd_data_exist = BOOL_FALSE;
    bool_t              mod_data_exist = BOOL_FALSE;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    rule_num    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    rule_bm     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    action      = tpm_cli_get_number(lub_argv__get_arg(argv, 4));
    stage       = tpm_cli_get_number(lub_argv__get_arg(argv, 5));

    str_ptr     = lub_argv__get_arg(argv, 6);
    if (str_ptr != NULL)
    {
        memcpy(&key_name, str_ptr, DB_CLI_MAX_NAME);
        l2key_data_exist = BOOL_TRUE;
    }

    str_ptr     = lub_argv__get_arg(argv, 7);
    if (str_ptr != NULL)
    {
        memcpy(&frwd_name, str_ptr, DB_CLI_MAX_NAME);
        frwd_data_exist = BOOL_TRUE;
    }

    str_ptr     = lub_argv__get_arg(argv, 8);
    if (str_ptr != NULL)
    {
        memcpy(&mod_name, str_ptr, DB_CLI_MAX_NAME);
        mod_data_exist = BOOL_TRUE;
    }

    DBG_PRINT("Owner = %d port = %d Rule num = %d Rule BM = 0x%x Action = 0x%x Stage = %d KEY = %s FRDW = %s MOD = %s",
              owner_id, port, rule_num, rule_bm, action, stage, key_name, frwd_name, mod_name);


    /* Fill in L2 key */
    if (l2key_data_exist == BOOL_TRUE) 
    {
        if (db_cli_get_l2key (key_name, &l2_key) != BOOL_TRUE)
        {
            PRINT("Failed to get key data from DB");
        }
        else
        {
            l2_key_ptr = &l2_key;
        }
    }

    /* Fill in FRWD data */
    if (frwd_data_exist == BOOL_TRUE)
    {
        if (db_cli_get_pkt_frwd (frwd_name, &pkt_frwd) != BOOL_TRUE)
        {
            PRINT("Failed to get frwd data from DB");
        }
        else
        {
            pkt_frwd_ptr = &pkt_frwd;
        }
    }

    /* Fill in Modification data */
    if (mod_data_exist == BOOL_TRUE)
    {
        if (db_cli_get_pkt_mod (mod_name, &pkt_mod) != BOOL_TRUE)
        {
            PRINT("Failed to get modification data from DB");
        }
        else
        {
            pkt_mod_ptr  = &pkt_mod;
        }
    }

    rule_action.pkt_act     = action;
    rule_action.next_phase  = stage;

    tpm_rc = tpm_add_l2_rule(owner_id,
                                      (tpm_src_port_type_t)port,
                                      rule_num,
                                      &rule_index,
                                      (tpm_parse_fields_t)rule_bm,
                                      l2_key_ptr,
                                      pkt_frwd_ptr,
                                      pkt_mod_ptr,
                                      &rule_action);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to add L2 Prim rule number %d ", rule_num);
    }
    else
    {
        PRINT("L2 Prim rule index is %d", rule_index);
    }
#endif
    return rc;
}

bool_t  tpm_cli_add_l3_prim_rule    (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
#if 0  /*user space TPM CLI will be removed in future, so just mask it to avoid compiling error*/     
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index = 0;
    uint32_t            rule_num;
    uint32_t            port;
    uint32_t            rule_bm;
    uint32_t            flags_bm;
    uint32_t            stage;
    uint8_t             action_drop;
    tpm_l3_type_key_t   l3_key;
    tpm_l3_type_key_t   *l3_key_ptr = NULL;
    const char          *str_ptr;
    const char          key_name[DB_CLI_MAX_NAME+1] = "";

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    rule_num    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    rule_bm     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    flags_bm    = tpm_cli_get_number(lub_argv__get_arg(argv, 4));
    stage       = tpm_cli_get_number(lub_argv__get_arg(argv, 5));
    action_drop = tpm_cli_get_number(lub_argv__get_arg(argv, 6));
    str_ptr     = lub_argv__get_arg(argv, 7);

    if (str_ptr != NULL) 
    {
        memcpy(&key_name, str_ptr, DB_CLI_MAX_NAME);
    }

    DBG_PRINT("Owner = %d Port = %d Rule num = %d Rule BM = 0x%x Flags BM = 0x%x Stage = %d Action drop = %d KEY = %s ",
              owner_id, port, rule_num, rule_bm, flags_bm, stage, action_drop, key_name);

    /* Fill in L2 key */
    if (str_ptr != NULL) 
    {
        if (db_cli_get_l3key (key_name, &l3_key) != BOOL_TRUE)
        {
            PRINT("Failed to get key data from DB");
        }
        else
        {
            l3_key_ptr = &l3_key;
        }
    }

    tpm_rc = tpm_add_l3_type_rule(owner_id,
                                      (tpm_src_port_type_t)port,
                                      rule_num,
                                      &rule_index,
                                      (tpm_parse_fields_t)rule_bm,
                                      (tpm_parse_flags_t)flags_bm,
                                      l3_key_ptr,
                                      action_drop,
                                      (tpm_parse_stage_t)stage);
    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to add L3 ACL rule number %d ", rule_num);
    }
    else
    {
        PRINT("L3 ACL rule index is %d", rule_index);
    }
#endif
    return rc;
}

bool_t  tpm_cli_add_ipv4_prim_rule    (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index = 0;
    uint32_t            rule_num;
    uint32_t            port;
    uint32_t            rule_bm;
    uint32_t            flags_bm;
    uint32_t            action;
    uint32_t            stage;
    uint32_t            mod_bm;
    tpm_ipv4_acl_key_t  ipv4_key;
    tpm_pkt_frwd_t      pkt_frwd;
    tpm_pkt_mod_t       pkt_mod;
    tpm_ipv4_acl_key_t  *ipv4_key_ptr = NULL;
    tpm_pkt_frwd_t      *pkt_frwd_ptr = NULL;
    tpm_pkt_mod_t       *pkt_mod_ptr = NULL;
    tpm_rule_action_t   rule_action;
    const char          key_name[DB_CLI_MAX_NAME+1] = "";
    const char          frwd_name[DB_CLI_MAX_NAME+1] = "";
    const char          mod_name[DB_CLI_MAX_NAME+1] = "";
    const char          *str_ptr;
    bool_t              key_data_exist = BOOL_FALSE;
    bool_t              frwd_data_exist = BOOL_FALSE;
    bool_t              mod_data_exist = BOOL_FALSE;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    rule_num    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    rule_bm     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    flags_bm    = tpm_cli_get_number(lub_argv__get_arg(argv, 4));
    action      = tpm_cli_get_number(lub_argv__get_arg(argv, 5));
    stage       = tpm_cli_get_number(lub_argv__get_arg(argv, 6));
    mod_bm      = tpm_cli_get_number(lub_argv__get_arg(argv, 7));

    str_ptr = lub_argv__get_arg(argv, 8);
    if (str_ptr != NULL)
    {
        memcpy(&key_name, str_ptr, DB_CLI_MAX_NAME);
        key_data_exist = BOOL_TRUE;
    }

    str_ptr = lub_argv__get_arg(argv, 9);
    if (str_ptr != NULL)
    {
        memcpy(&frwd_name, str_ptr, DB_CLI_MAX_NAME);
        frwd_data_exist = BOOL_TRUE;
    }

    str_ptr = lub_argv__get_arg(argv, 10);
    if (str_ptr != NULL)
    {
        memcpy(&mod_name, str_ptr, DB_CLI_MAX_NAME);
        mod_data_exist = BOOL_TRUE;
    }

    DBG_PRINT("Owner = %d port = %d Rule num = %d Rule BM = 0x%x Flags BM = 0x%x\n Action = 0x%x Stage = %d Mod BM = 0x%x KEY = %s FRDW = %s MOD = %s",
              owner_id, port, rule_num, rule_bm, flags_bm, action, stage, mod_bm, key_name, frwd_name, mod_name);

    /* Fill in IPv4 key */
    if (key_data_exist == BOOL_TRUE) 
    {
        if (db_cli_get_ipv4_key (key_name, &ipv4_key) != BOOL_TRUE)
        {
            PRINT("Failed to get key data from DB");
        }
        else
        {
            ipv4_key_ptr = &ipv4_key;
        }
    }

    /* Fill in PKT data */
    if (frwd_data_exist == BOOL_TRUE)
    {
        if (db_cli_get_pkt_frwd (frwd_name, &pkt_frwd) != BOOL_TRUE)
        {
            PRINT("Failed to get frwd data from DB");
        }
        else
        {
            pkt_frwd_ptr = &pkt_frwd;
        }
    }

    if (mod_data_exist == BOOL_TRUE)
    {
        if (db_cli_get_pkt_mod (mod_name, &pkt_mod) != BOOL_TRUE)
        {
            PRINT("Failed to get modification data from DB");
        }
        else
        {
            pkt_mod_ptr  = &pkt_mod;
        }
    }

    rule_action.pkt_act     = action;
    rule_action.next_phase  = stage;

    tpm_rc = tpm_add_ipv4_rule (owner_id,
                                   (tpm_src_port_type_t)port,
                                   rule_num,
                                   &rule_index,
                                   (tpm_parse_fields_t)rule_bm,
                                   (tpm_parse_flags_t)flags_bm,
                                   ipv4_key_ptr,
                                   pkt_frwd_ptr,
                                   pkt_mod_ptr,
                                   (tpm_pkt_mod_bm_t)mod_bm,
                                   &rule_action);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to add IPv4 Prim rule number %d ", rule_num);
    }
    else
    {
        PRINT("IPv4 Prim rule index is %d", rule_index);
    }

    return rc;
}

#ifdef OLD_TPM_APIS
bool_t  tpm_cli_add_ipv4_dscp_rule    (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index = 0;
    uint32_t            rule_num;
    uint32_t            flags_bm;
    uint32_t            dscp;
    uint32_t            dscp_mask;
    uint32_t            targ_queue;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    rule_num    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    flags_bm    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    dscp        = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    dscp_mask   = tpm_cli_get_number(lub_argv__get_arg(argv, 4));
    targ_queue  = tpm_cli_get_number(lub_argv__get_arg(argv, 5));

    DBG_PRINT("Owner = %d Rule num = %d Flags BM = 0x%x\n DSCP = %d DSCP mask = 0x%x Queue = %d",
              owner_id, rule_num, flags_bm, dscp, dscp_mask, targ_queue);

    tpm_rc = tpm_add_ipv4_dscp_acl_rule (owner_id,
                                         rule_num,
                                         &rule_index,
                                         (tpm_parse_flags_t)flags_bm,
                                         dscp,
                                         dscp_mask,
                                         targ_queue);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to add IPv4 DSCP rule number %d ", rule_num);
    }
    else
    {
        PRINT("IPv4 DSCP rule index is %d", rule_index);
    }

    return rc;
}
#endif /*OLD_TPM_APIS*/

#ifdef OLD_TPM_APIS
bool_t  tpm_cli_add_ipv6_prim_rule    (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index = 0;
    uint32_t            rule_num;
    uint32_t            port;
    uint32_t            rule_bm;
    uint32_t            flags_bm;
    uint32_t            action;
    uint32_t            stage;
    uint32_t            mod_bm;
    tpm_ipv6_acl_key_t  ipv6_key;
    tpm_pkt_frwd_t      pkt_frwd;
    tpm_pkt_mod_t       pkt_mod;
    tpm_ipv6_acl_key_t  *ipv6_key_ptr = NULL;
    tpm_pkt_frwd_t      *pkt_frwd_ptr = NULL;
    tpm_pkt_mod_t       *pkt_mod_ptr = NULL;
    tpm_rule_action_t   rule_action;
    const char          key_name[DB_CLI_MAX_NAME+1] = "";
    const char          frwd_name[DB_CLI_MAX_NAME+1] = "";
    const char          mod_name[DB_CLI_MAX_NAME+1] = "";
    const char          *str_ptr;
    bool_t              key_data_exist = BOOL_FALSE;
    bool_t              frwd_data_exist = BOOL_FALSE;
    bool_t              mod_data_exist = BOOL_FALSE;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    rule_num    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    rule_bm     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    flags_bm    = tpm_cli_get_number(lub_argv__get_arg(argv, 4));
    action      = tpm_cli_get_number(lub_argv__get_arg(argv, 5));
    stage       = tpm_cli_get_number(lub_argv__get_arg(argv, 6));
    mod_bm      = tpm_cli_get_number(lub_argv__get_arg(argv, 7));

    str_ptr = lub_argv__get_arg(argv, 8);
    if (str_ptr != NULL)
    {
        memcpy(&key_name, str_ptr, DB_CLI_MAX_NAME);
        key_data_exist = BOOL_TRUE;
    }

    str_ptr = lub_argv__get_arg(argv, 9);
    if (str_ptr != NULL)
    {
        memcpy(&frwd_name, str_ptr, DB_CLI_MAX_NAME);
        frwd_data_exist = BOOL_TRUE;
    }

    str_ptr = lub_argv__get_arg(argv, 10);
    if (str_ptr != NULL)
    {
        memcpy(&mod_name, str_ptr, DB_CLI_MAX_NAME);
        mod_data_exist = BOOL_TRUE;
    }

    DBG_PRINT("Owner = %d port = %d Rule num = %d Rule BM = 0x%x Flags BM = 0x%x\n Action = 0x%x Stage = %d Mod BM = 0x%x KEY = %s FRDW = %s MOD = %s",
              owner_id, port, rule_num, rule_bm, flags_bm, action, stage, mod_bm, key_name, frwd_name, mod_name);

    /* Fill in IPv6 key */
    if (key_data_exist == BOOL_TRUE)
    {
        if (db_cli_get_ipv6_key (key_name, &ipv6_key) != BOOL_TRUE)
        {
            PRINT("Failed to get key data from DB");
        }
        else
        {
            ipv6_key_ptr = &ipv6_key;
        }
    }

    /* Fill in PKT data */
    if (frwd_data_exist == BOOL_TRUE)
    {
        if (db_cli_get_pkt_frwd (frwd_name, &pkt_frwd) != BOOL_TRUE)
        {
            PRINT("Failed to get frwd data from DB");
        }
        else
        {
            pkt_frwd_ptr = &pkt_frwd;
        }
    }

    if (mod_data_exist == BOOL_TRUE)
    {
        if (db_cli_get_pkt_mod (mod_name, &pkt_mod) != BOOL_TRUE)
        {
            PRINT("Failed to get modification data from DB");
        }
        else
        {
            pkt_mod_ptr  = &pkt_mod;
        }
    }

    rule_action.pkt_act     = action;
    rule_action.next_phase  = stage;

    tpm_rc = tpm_add_ipv6_acl_rule (owner_id,
                                    (tpm_src_port_type_t)port,
                                    rule_num,
                                    &rule_index,
                                    (tpm_parse_fields_t)rule_bm,
                                    (tpm_parse_flags_t)flags_bm,
                                    ipv6_key_ptr,
                                    pkt_frwd_ptr,
                                    pkt_mod_ptr,
                                    (tpm_pkt_mod_bm_t)mod_bm,
                                    &rule_action);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to add IPv6 Prim rule number %d ", rule_num);
    }
    else
    {
        PRINT("IPv6 Prim rule index is %d", rule_index);
    }

    return rc;
}
#endif /*OLD_TPM_APIS*/

#ifdef OLD_TPM_APIS
bool_t  tpm_cli_add_ipv6_dscp_rule    (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index = 0;
    uint32_t            rule_num;
    uint32_t            flags_bm;
    uint32_t            dscp;
    uint32_t            dscp_mask;
    uint32_t            targ_queue;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    rule_num    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    flags_bm    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    dscp        = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    dscp_mask   = tpm_cli_get_number(lub_argv__get_arg(argv, 4));
    targ_queue  = tpm_cli_get_number(lub_argv__get_arg(argv, 5));

    DBG_PRINT("Owner = %d Rule num = %d Flags BM = 0x%x\n DSCP = %d DSCP mask = 0x%x Queue = %d",
              owner_id, rule_num, flags_bm, dscp, dscp_mask, targ_queue);

    tpm_rc = tpm_add_ipv6_dscp_acl_rule (owner_id,
                                         rule_num,
                                         &rule_index,
                                         (tpm_parse_flags_t)flags_bm,
                                         dscp,
                                         dscp_mask,
                                         targ_queue);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to add IPv6 DSCP rule number %d ", rule_num);
    }
    else
    {
        PRINT("IPv6 DSCP rule index is %d", rule_index);
    }

    return rc;
}
#endif /*OLD_TPM_APIS*/


/********************************************************************************/
/*                              TPM CLI get functions                           */
/********************************************************************************/
bool_t  tpm_cli_get_section_free_size   (const clish_shell_t    *instance,
                                         const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc;
    uint32_t            dir, size = 0;
    int                 apig;

    /* Get parameters */
    apig    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    dir     = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    DBG_PRINT("API group = %d Direction = %d", apig, dir);

    tpm_rc = tpm_get_section_free_size((tpm_api_type_t)apig, &size);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Get action failed");
    }
    else
        PRINT("Section free size is %d", size);

    return rc;
}


bool_t  tpm_cli_get_next_valid_rule   (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id, rule_type;
    uint8_t             dir;
    int32_t             curr_index;
    uint32_t            next_index = 0;
    uint32_t            rule_idx = 0;
    tpm_rule_entry_t    tpm_rule;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    dir         = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    curr_index  = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    rule_type   = tpm_cli_get_number(lub_argv__get_arg(argv, 3));

    DBG_PRINT("Owner = %d Direction = %d Curr entry = %d Rule type = %d", owner_id, dir, curr_index, rule_type);

    memset(&tpm_rule,0,sizeof(tpm_rule_entry_t));

    tpm_rc = tpm_get_next_valid_rule(owner_id, curr_index, (tpm_api_type_t)rule_type, &next_index, &rule_idx, &tpm_rule);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Get action failed");
    }
    else{
        PRINT("Next valid rule is %d", next_index);
        PRINT("==========================");
        if (next_index != -1) 
            db_cli_print_rule_entry(&tpm_rule, rule_idx);
    }

    return rc;
}

bool_t  tpm_cli_get_ownerid   (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t                      rc = BOOL_TRUE;
    tpm_api_ownership_error_t   tpm_rc;
    uint32_t                    ownerid;
    int                         apig;

    /* Get parameters */
    apig    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));

    DBG_PRINT("API group = %d ", apig);

    tpm_rc = tpm_get_api_ownership(&ownerid, (tpm_api_type_t)apig);

    if (tpm_rc != API_OWNERSHIP_SUCCESS)
    {
        rc = BOOL_FALSE;
        PRINT("Get action failed");
    }
    else
        PRINT("Owner ID is %d", ownerid);

    return rc;
}


bool_t  tpm_cli_get_omci_channel   (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
    bool_t                      rc = BOOL_TRUE;
    tpm_error_code_t            tpm_rc;
    uint32_t                    is_valid;
    tpm_gem_port_key_t          gem_port;
    uint32_t                    cpu_rx_queue;
    tpm_trg_port_type_t         tcont_num;
    uint32_t                    cpu_tx_queue;

    /* Get parameters */

    tpm_rc = tpm_omci_get_channel (&is_valid, &gem_port, &cpu_rx_queue, &tcont_num, &cpu_tx_queue);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Get action failed");
    }
    else
        PRINT("Valid = %d GEM = %d RX queue = %d TX queue = %d Tcont = 0x%4.4x",
              is_valid, gem_port, cpu_rx_queue, cpu_tx_queue, tcont_num);

    return rc;
}


bool_t  tpm_cli_get_oam_channel   (const clish_shell_t    *instance,
                                   const lub_argv_t       *argv)
{
    bool_t                      rc = BOOL_TRUE;
    tpm_error_code_t            tpm_rc;
    uint32_t                    is_valid;
    uint32_t                    cpu_rx_queue;
    tpm_trg_port_type_t         llid_num;

    /* Get parameters */

    tpm_rc = tpm_oam_epon_get_channel (&is_valid, &cpu_rx_queue, &llid_num);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Get action failed");
    }
    else
        PRINT("Valid = %d RX queue = %d LLID = 0x%4.4x",
              is_valid, cpu_rx_queue, llid_num);

    return rc;
}



#define QD_FMT "%10lu %10lu %10lu %10lu %10lu "/* %10lu %10lu\n"*/
#define QD_CNT(c,f) (unsigned long)c[0].f, (unsigned long)c[1].f, (unsigned long)c[2].f, (unsigned long)c[3].f, (unsigned long)c[4].f/* , (unsigned long)c[5].f, (unsigned long)c[6].f */
#define QD_MAX 5

bool_t  tpm_cli_get_pm_counters   (const clish_shell_t    *instance,
                                   const lub_argv_t       *argv)
{

    bool_t                      rc = BOOL_TRUE;
    uint32_t                   owner_id;
    tpm_error_code_t            tpm_rc;
    tpm_src_port_type_t         port_index ;
    tpm_swport_pm_1_t   tpm_counter_pm_1[QD_MAX];
    tpm_swport_pm_3_all_t   tpm_counter_pm_3[QD_MAX];	

    memset(tpm_counter_pm_1, 0, sizeof(tpm_swport_pm_1_t) * QD_MAX);
    memset(tpm_counter_pm_3, 0, sizeof(tpm_swport_pm_3_all_t) * QD_MAX);	
	
    for (port_index = TPM_SRC_PORT_UNI_0; port_index <= TPM_SRC_PORT_UNI_3; port_index++)
   {
  
	 tpm_rc = tpm_sw_pm_1_read(0, port_index, &tpm_counter_pm_1[port_index]);
	 
	 tpm_rc = tpm_sw_pm_3_read(0, port_index, &tpm_counter_pm_3[port_index]);
	 
        if (tpm_rc != TPM_RC_OK)
        {
            PRINT("Get action failed, return %d \r\n", tpm_rc);
            rc = BOOL_FALSE;			
        }
       
   }
   
		PRINT("PortNum                           " QD_FMT,  (unsigned long)0, (unsigned long)1, (unsigned long)2, (unsigned long)3, (unsigned long)4, (unsigned long)5, (unsigned long)6);
		PRINT("--------------------------------------------------------------------------------------------------------------\n");
		
		PRINT("dropEvents                        " QD_FMT,  QD_CNT(tpm_counter_pm_3, dropEvents));
		PRINT("octets                            " QD_FMT,  QD_CNT(tpm_counter_pm_3, InGoodOctetsLo));
		PRINT("packets                           " QD_FMT,  QD_CNT(tpm_counter_pm_3, InUnicasts));
		PRINT("broadcastPackets                  " QD_FMT,  QD_CNT(tpm_counter_pm_3, InBroadcasts));
		PRINT("multicastPackets                  " QD_FMT,  QD_CNT(tpm_counter_pm_3, InMulticasts));
		PRINT("undersizePackets                  " QD_FMT,  QD_CNT(tpm_counter_pm_3, Undersize));
		PRINT("fragments                         " QD_FMT,  QD_CNT(tpm_counter_pm_3,Fragments));
		PRINT("jabbers                           " QD_FMT,  QD_CNT(tpm_counter_pm_3, Jabber));

		PRINT("packets_64Octets                  " QD_FMT,  QD_CNT(tpm_counter_pm_3, Octets64));
		PRINT("packets_65_127Octets              " QD_FMT,  QD_CNT(tpm_counter_pm_3, Octets127));
		PRINT("packets_128_255Octets             " QD_FMT,  QD_CNT(tpm_counter_pm_3, Octets255));
		PRINT("packets_256_511Octets             " QD_FMT,  QD_CNT(tpm_counter_pm_3, Octets511));
		PRINT("packets_512_1023Octets            " QD_FMT,  QD_CNT(tpm_counter_pm_3, Octets1023));
		PRINT("packets_1024_1518Octets           " QD_FMT,  QD_CNT(tpm_counter_pm_3, OctetsMax));

		PRINT("fcsErrors                         " QD_FMT,  QD_CNT(tpm_counter_pm_1, fcsErrors));
		PRINT("excessiveCollisionCounter         " QD_FMT,  QD_CNT(tpm_counter_pm_1, excessiveCollisionCounter));
		PRINT("lateCollisionCounter              " QD_FMT,  QD_CNT(tpm_counter_pm_1, lateCollisionCounter));
		PRINT("frameTooLongs                     " QD_FMT,  QD_CNT(tpm_counter_pm_1, frameTooLongs));
		PRINT("bufferOverflowsOnReceive          " QD_FMT,  QD_CNT(tpm_counter_pm_1, bufferOverflowsOnReceive));
		PRINT("bufferOverflowsOnTransmit         " QD_FMT,  QD_CNT(tpm_counter_pm_1, bufferOverflowsOnTransmit));
		PRINT("singleCollisionFrameCounter       " QD_FMT,  QD_CNT(tpm_counter_pm_1, singleCollisionFrameCounter));
		PRINT("multipleCollisionsFrameCounter    " QD_FMT,  QD_CNT(tpm_counter_pm_1, multipleCollisionsFrameCounter));
		PRINT("sqeCounter                        " QD_FMT,  QD_CNT(tpm_counter_pm_1, sqeCounter));

		PRINT("deferredTransmissionCounter       " QD_FMT,  QD_CNT(tpm_counter_pm_1, deferredTransmissionCounter));
		PRINT("internalMacTransmitErrorCounter   " QD_FMT,  QD_CNT(tpm_counter_pm_1, internalMacTransmitErrorCounter));
		PRINT("carrierSenseErrorCounter          " QD_FMT,  QD_CNT(tpm_counter_pm_1, carrierSenseErrorCounter));
		PRINT("alignmentErrorCounter             " QD_FMT,  QD_CNT(tpm_counter_pm_1, alignmentErrorCounter));
		PRINT("internalMacReceiveErrorCounter    " QD_FMT,  QD_CNT(tpm_counter_pm_1, internalMacReceiveErrorCounter));


    return rc;
}



bool_t  tpm_cli_clear_pm_counters   (const clish_shell_t    *instance,
                                   const lub_argv_t       *argv)
{
    bool_t                      rc = BOOL_TRUE;
    uint32_t                   owner_id;
    tpm_error_code_t            tpm_rc;
    tpm_src_port_type_t         port_index = TPM_SRC_PORT_WAN;
	
    for (port_index = TPM_SRC_PORT_UNI_0; port_index <= TPM_SRC_PORT_UNI_3; port_index++)
   {
        tpm_rc = tpm_sw_clear_port_counter(0, port_index);
        if (tpm_rc != TPM_RC_OK)
        {
            PRINT("Get action failed, return %d \r\n", tpm_rc);
            rc = BOOL_FALSE;			
        }
       
   }

    return rc;
}

/********************************************************************************/
/*                      TPM CLI delete functions                                */
/********************************************************************************/
bool_t  tpm_cli_erase_section   (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            ownerId;
    int                 apig;

    /* Get parameters */
    ownerId = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    apig    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    DBG_PRINT("Owner ID = %d, API group = %d", ownerId, apig);

    tpm_rc = tpm_erase_section(ownerId, (tpm_api_type_t)apig);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Erase section failed");
    }
    else
        PRINT("Done");

    return rc;
}

bool_t  tpm_cli_del_l2_prim_rule    (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index;
    uint32_t            port;
    uint32_t            rule_bm;
    tpm_l2_acl_key_t    l2_key;
    tpm_l2_acl_key_t    *l2_key_ptr = NULL;
    const char          key_name[DB_CLI_MAX_NAME+1];
    const char          *str_name;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = atoi(lub_argv__get_arg(argv, 1));
    rule_index  = atoi(lub_argv__get_arg(argv, 2));
    rule_bm     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    str_name    = lub_argv__get_arg(argv, 4);

    if (str_name != NULL) 
    {
        memcpy(&key_name, str_name, DB_CLI_MAX_NAME);
    }

    DBG_PRINT("Owner = %d port = %d Rule idx = %d Rule BM = 0x%8.8x KEY = %s",
              owner_id, port, rule_index, rule_bm, key_name);

    /* Fill in L2 key */
    if (db_cli_get_l2key (key_name, &l2_key) == BOOL_TRUE)
    {
        l2_key_ptr = &l2_key;
    }

    tpm_rc = tpm_del_l2_rule(owner_id, rule_index);
    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to delete L2 Prim rule %d ", rule_index);
    }
    else
    {
        PRINT("L2 Prim rule %d has been deleted", rule_index);
    }

    return rc;
}

bool_t  tpm_cli_del_l3_acl_rule     (const clish_shell_t    *instance,
                                     const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index;
    uint32_t            port;
    uint32_t            rule_bm;
    tpm_l3_type_key_t   l3_key;
    tpm_l3_type_key_t   *l3_key_ptr = NULL;
    const char          key_name[DB_CLI_MAX_NAME+1];
    const char          *str_name;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = atoi(lub_argv__get_arg(argv, 1));
    rule_index  = atoi(lub_argv__get_arg(argv, 2));
    rule_bm     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    str_name    = lub_argv__get_arg(argv, 4);

    if (str_name != NULL) 
    {
        memcpy(&key_name, str_name, DB_CLI_MAX_NAME);
    }

    DBG_PRINT("Owner = %d port = %d Rule idx = %d Rule BM = 0x%8.8x KEY = %s",
              owner_id, port, rule_index, rule_bm, key_name);

    /* Fill in L3 key */
    if (db_cli_get_l3key (key_name, &l3_key) == BOOL_TRUE)
    {
        l3_key_ptr = &l3_key;
    }

    tpm_rc = tpm_del_l3_type_rule (owner_id,rule_index);
    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to delete L3 rule %d ", rule_index);
    }
    else
    {
        PRINT("L3 rule %d has been deleted", rule_index);
    }

    return rc;
}

bool_t  tpm_cli_del_ipv4_acl_rule     (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index;
    uint32_t            port;
    uint32_t            rule_bm;
    tpm_ipv4_acl_key_t  ipv4_key;
    tpm_ipv4_acl_key_t  *ipv4_key_ptr = NULL;
    const char          key_name[DB_CLI_MAX_NAME+1];
    const char          *str_name;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    rule_index  = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    rule_bm     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    str_name    = lub_argv__get_arg(argv, 4);

    if (str_name != NULL) 
    {
        memcpy(&key_name, str_name, DB_CLI_MAX_NAME);
    }

    DBG_PRINT("Owner = %d port = %d Rule idx = %d Rule BM = 0x%8.8x KEY = %s",
              owner_id, port, rule_index, rule_bm, key_name);

    /* Fill in IPv4 key */
    if (db_cli_get_ipv4_key (key_name, &ipv4_key) == BOOL_TRUE)
    {
        ipv4_key_ptr = &ipv4_key;
    }

    tpm_rc = tpm_del_ipv4_rule (owner_id,rule_index);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to delete IPv4 rule %d ", rule_index);
    }
    else
    {
        PRINT("IPv4 rule %d has been deleted", rule_index);
    }

    return rc;
}

#ifdef OLD_TPM_APIS
bool_t  tpm_cli_del_ipv4_dscp_rule     (const clish_shell_t    *instance,
                                        const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    rule_index  = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    tpm_rc = tpm_del_ipv4_dscp_acl_rule (owner_id, rule_index);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to delete IPv4 DSCP rule %d ", rule_index);
    }
    else
    {
        PRINT("IPv4 DSCP rule %d has been deleted", rule_index);
    }

    return rc;
}
#endif /*OLD_TPM_APIS*/

#ifdef OLD_TPM_APIS
bool_t  tpm_cli_del_ipv6_acl_rule     (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index;
    uint32_t            port;
    uint32_t            rule_bm;
    tpm_ipv6_acl_key_t  ipv6_key;
    tpm_ipv6_acl_key_t  *ipv6_key_ptr = NULL;
    const char          key_name[DB_CLI_MAX_NAME+1];
    const char          *str_name;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    rule_index  = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    rule_bm     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    str_name    = lub_argv__get_arg(argv, 4);

    if (str_name != NULL) 
    {
        memcpy(&key_name, str_name, DB_CLI_MAX_NAME);
    }


    DBG_PRINT("Owner = %d port = %d Rule idx = %d Rule BM = 0x%8.8x KEY = %s",
              owner_id, port, rule_index, rule_bm, key_name);

    /* Fill in IPv6 key */
    if (db_cli_get_ipv6_key (key_name, &ipv6_key) == BOOL_TRUE)
    {
        ipv6_key_ptr = &ipv6_key;
    }

    tpm_rc = tpm_del_ipv6_acl_rule (owner_id,
                                    (tpm_src_port_type_t)port,
                                    rule_index,
                                    (tpm_parse_fields_t)rule_bm,
                                    ipv6_key_ptr);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to delete IPv6 rule %d ", rule_index);
    }
    else
    {
        PRINT("IPv6 rule %d has been deleted", rule_index);
    }

    return rc;
}
#endif /*OLD_TPM_APIS*/

#ifdef OLD_TPM_APIS
bool_t  tpm_cli_del_ipv6_dscp_rule     (const clish_shell_t    *instance,
                                        const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rule_index;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    rule_index  = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    tpm_rc = tpm_del_ipv6_dscp_acl_rule (owner_id, rule_index);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to delete IPv6 DSCP rule %d ", rule_index);
    }
    else
    {
        PRINT("IPv6 DSCP rule %d has been deleted", rule_index);
    }

    return rc;
}
#endif /*OLD_TPM_APIS*/


bool_t  tpm_cli_del_vid (const clish_shell_t    *instance,
                         const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            vid;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    vid         = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Owner = %d port = %d vid = %d", owner_id, port, vid);

    tpm_rc = tpm_sw_port_del_vid (owner_id, (tpm_src_port_type_t)port, vid);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to delete vid - %d", vid);
    }
    else
    {
        PRINT("%d vid has been deleted", vid);
    }

    return rc;
}

bool_t  tpm_cli_del_ipv4_mc (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            stream;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    stream      = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    DBG_PRINT("Owner = %d stream = %d", owner_id, stream);

    tpm_rc = tpm_del_ipv4_mc_stream (owner_id, stream);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to delete stream - %d", stream);
    }
    else
    {
        PRINT("%d stream has been deleted", stream);
    }

    return rc;
}

#ifdef OLD_TPM_APIS
bool_t  tpm_cli_del_ipv6_mc (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            stream;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    stream      = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    DBG_PRINT("Owner = %d stream = %d", owner_id, stream);

    tpm_rc = tpm_del_ipv6_mc_stream (owner_id, stream);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to delete stream - %d", stream);
    }
    else
    {
        PRINT("%d stream has been deleted", stream);
    }

    return rc;
}
#endif /*OLD_TPM_APIS*/

bool_t  tpm_cli_del_oam_channel (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));

    DBG_PRINT("Owner = %d ", owner_id);

    tpm_rc = tpm_oam_epon_del_channel (owner_id);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to delete channel");
    }
    else
    {
        PRINT("Channel has been deleted");
    }

    return rc;
}

bool_t  tpm_cli_del_omci_channel (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));

    DBG_PRINT("Owner = %d ", owner_id);

    tpm_rc = tpm_omci_del_channel (owner_id);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to delete channel");
    }
    else
    {
        PRINT("Channel has been deleted");
    }

    return rc;
}

bool_t  tpm_cli_del_static_mac (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint8_t             st_mac[6];
    const char          *mac_ptr;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    mac_ptr     = lub_argv__get_arg(argv, 1);


    DBG_PRINT("Owner = %d mac = %s", owner_id, mac_ptr);

    sscanf(mac_ptr, "%x:%x:%x:%x:%x:%x",
           &(st_mac[0]), &(st_mac[1]), &(st_mac[2]),
           &(st_mac[3]), &(st_mac[4]), &(st_mac[5]));

    tpm_rc = tpm_sw_del_static_mac (owner_id, st_mac);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to remove static MAC - %x:%x:%x:%x:%x:%x",
              st_mac[0], st_mac[1], st_mac[2], st_mac[3], st_mac[4], st_mac[5]);
    }
    else
    {
        PRINT("%x:%x:%x:%x:%x:%x static MAC has been deleted",
              st_mac[0], st_mac[1], st_mac[2], st_mac[3], st_mac[4], st_mac[5]);
    }

    return rc;
}

/********************************************************************************/
/*                      TPM CLI delete DB data functions                        */
/********************************************************************************/
bool_t  tpm_cli_del_vlan_entry (const clish_shell_t    *instance,  
                                const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    const char          name[DB_CLI_MAX_NAME+1];
    const char          *str_name;

    /* Get parameters */
    str_name = lub_argv__get_arg(argv, 0);

    memcpy(&name, str_name, DB_CLI_MAX_NAME);

    DBG_PRINT("Name = %s", name);

    rc = db_cli_del_vlan_tbl_entry (name);

    if (rc != BOOL_TRUE) 
    {
        PRINT("Failed to delete %s entry", name);
    }

    return rc;
}


bool_t  tpm_cli_del_mod_entry (const clish_shell_t    *instance,  
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    const char          name[DB_CLI_MAX_NAME+1];
    const char          *str_name;

    /* Get parameters */
    str_name = lub_argv__get_arg(argv, 0);

    memcpy(&name, str_name, DB_CLI_MAX_NAME);

    DBG_PRINT("Name = %s", name);

    rc = db_cli_del_pkt_mod_entry (name);

    if (rc != BOOL_TRUE) 
    {
        PRINT("Failed to delete %s entry", name);
    }

    return rc;
}


bool_t  tpm_cli_del_frwd_entry (const clish_shell_t    *instance,  
                                const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    const char          name[DB_CLI_MAX_NAME+1];
    const char          *str_name;

    /* Get parameters */
    str_name = lub_argv__get_arg(argv, 0);

    memcpy(&name, str_name, DB_CLI_MAX_NAME);

    DBG_PRINT("Name = %s", name);

    rc = db_cli_del_pkt_frwd_entry (name);

    if (rc != BOOL_TRUE) 
    {
        PRINT("Failed to delete %s entry", name);
    }

    return rc;
}


bool_t  tpm_cli_del_l2_key_entry (const clish_shell_t    *instance,  
                                  const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    const char          name[DB_CLI_MAX_NAME+1];
    const char          *str_name;

    /* Get parameters */
    str_name = lub_argv__get_arg(argv, 0);

    memcpy(&name, str_name, DB_CLI_MAX_NAME);

    DBG_PRINT("Name = %s", name);

    rc = db_cli_del_l2key_entry (name);

    if (rc != BOOL_TRUE) 
    {
        PRINT("Failed to delete %s entry", name);
    }

    return rc;
}


bool_t  tpm_cli_del_l3_key_entry (const clish_shell_t    *instance,  
                                  const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    const char          name[DB_CLI_MAX_NAME+1];
    const char          *str_name;

    /* Get parameters */
    str_name = lub_argv__get_arg(argv, 0);

    memcpy(&name, str_name, DB_CLI_MAX_NAME);

    DBG_PRINT("Name = %s", name);

    rc = db_cli_del_l3key_entry (name);

    if (rc != BOOL_TRUE) 
    {
        PRINT("Failed to delete %s entry", name);
    }

    return rc;
}

bool_t  tpm_cli_del_ipv4_key_entry (const clish_shell_t    *instance,  
                                    const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    const char          name[DB_CLI_MAX_NAME+1];
    const char          *str_name;

    /* Get parameters */
    str_name = lub_argv__get_arg(argv, 0);

    memcpy(&name, str_name, DB_CLI_MAX_NAME);

    DBG_PRINT("Name = %s", name);

    rc = db_cli_del_ipv4_key_entry (name);

    if (rc != BOOL_TRUE) 
    {
        PRINT("Failed to delete %s entry", name);
    }

    return rc;
}


bool_t  tpm_cli_del_ipv6_key_entry (const clish_shell_t    *instance,  
                                    const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    const char          name[DB_CLI_MAX_NAME+1];
    const char          *str_name;

    /* Get parameters */
    str_name = lub_argv__get_arg(argv, 0);

    memcpy(&name, str_name, DB_CLI_MAX_NAME);

    DBG_PRINT("Name = %s", name);

    rc = db_cli_del_ipv6_key_entry (name);

    if (rc != BOOL_TRUE) 
    {
        PRINT("Failed to delete %s entry", name);
    }

    return rc;
}



/********************************************************************************/
/*                      TPM CLI MC functions                                    */
/********************************************************************************/
bool_t  tpm_cli_add_ipv4_mc_stream   (const clish_shell_t    *instance,
                                      const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
#if 0  /*user space TPM CLI will be removed in future, so just mask it to avoid compiling error*/
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            ownerId;
    uint32_t            stream;
    uint32_t            vid;
    uint8_t             ignore;
    uint32_t            port_bm;
    uint8_t             src[DB_CLI_IPV4_ADDR_LEN];
    uint8_t             dst[DB_CLI_IPV4_ADDR_LEN];
    const char          *sa;
    const char          *da;
    uint32_t            ip1,ip2,ip3,ip4;

    /* Get parameters */
    ownerId = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    stream  = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    vid     = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    sa      = lub_argv__get_arg(argv, 3);
    da      = lub_argv__get_arg(argv, 4);
    ignore  = tpm_cli_get_number(lub_argv__get_arg(argv, 5));
    port_bm = tpm_cli_get_number(lub_argv__get_arg(argv, 6));

    DBG_PRINT("Owner ID = %d Stream = %d VID = %d SRC = %s DST = %s Ignore = %d Port BM = %d ",
              ownerId, stream, vid, sa, da, ignore, port_bm);

    sscanf(sa, "%d.%d.%d.%d", &ip1, &ip2, &ip3, &ip4);
    src[0] = (uint8_t)ip1;
    src[1] = (uint8_t)ip2;
    src[2] = (uint8_t)ip3;
    src[3] = (uint8_t)ip4;
    sscanf(da, "%d.%d.%d.%d", &ip1, &ip2, &ip3, &ip4);
    dst[0] = (uint8_t)ip1;
    dst[1] = (uint8_t)ip2;
    dst[2] = (uint8_t)ip3;
    dst[3] = (uint8_t)ip4;
    
    tpm_rc = tpm_add_ipv4_mc_stream(ownerId, stream, vid, src, dst, ignore, port_bm);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to add MC stream = %d", stream);
    }
    else
    {
        PRINT("MC stream = %d has been added", stream);
    }
#endif
    return rc;
}

bool_t  tpm_cli_update_ipv4_mc_stream   (const clish_shell_t    *instance,
                                         const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            ownerId;
    uint32_t            stream;
    uint32_t            port_bm;


    /* Get parameters */
    ownerId = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    stream  = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    port_bm = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Owner ID = %d Stream = %d Port BM = %d ", ownerId, stream, port_bm);

    tpm_rc = tpm_updt_ipv4_mc_stream(ownerId, stream, port_bm);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to update MC stream = %d", stream);
    }
    else
    {
        PRINT("MC stream = %d has been updated", stream);
    }

    return rc;
}

#ifdef OLD_TPM_APIS
bool_t  tpm_cli_add_ipv6_mc_stream   (const clish_shell_t    *instance,
                                      const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            ownerId;
    uint32_t            stream;
    uint32_t            vid;
    uint8_t             ignore;
    uint32_t            port_bm;
    uint8_t             src[DB_CLI_IPV6_ADDR_LEN];
    uint8_t             dst[DB_CLI_IPV6_ADDR_LEN];
    uint32_t            src2[DB_CLI_IPV6_ADDR_LEN/2];
    uint32_t            dst2[DB_CLI_IPV6_ADDR_LEN/2];
    const char          *sa;
    const char          *da;

    /* Get parameters */
    ownerId = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    stream  = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    vid     = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    sa      = lub_argv__get_arg(argv, 3);
    da      = lub_argv__get_arg(argv, 4);
    ignore  = tpm_cli_get_number(lub_argv__get_arg(argv, 5));
    port_bm = tpm_cli_get_number(lub_argv__get_arg(argv, 6));

    DBG_PRINT("Owner ID = %d Stream = %d VID = %d SRC = %s DST = %s Ignore = %d Port BM = %d ",
              ownerId, stream, vid, sa, da, ignore, port_bm);

    sscanf(sa, "%x:%x:%x:%x:%x:%x:%x:%x",
           &(src2[0]), &(src2[1]), &(src2[2]), &(src2[3]), &(src2[4]), &(src2[5]), &(src2[6]), &(src2[7]));
    sscanf(da, "%x:%x:%x:%x:%x:%x:%x:%x",
           &(dst2[0]), &(dst2[1]), &(dst2[2]), &(dst2[3]), &(dst2[4]), &(dst2[5]), &(dst2[6]), &(dst2[7]));

    tpm_cli_ipv6_src_to_sw(src2, src);
    tpm_cli_ipv6_src_to_sw(dst2, dst);

    DBG_PRINT("SRC %2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x",
              src[0], src[1], src[2], src[3], src[4], src[5], src[6], src[7],
              src[8], src[9], src[10], src[11], src[12], src[13], src[14], src[15]);
    DBG_PRINT("DST %2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x:%2.2x%2.2x",
              dst[0], dst[1], dst[2], dst[3], dst[4], dst[5], dst[6], dst[7],
              dst[8], dst[9], dst[10], dst[11], dst[12], dst[13], dst[14], dst[15]);

    tpm_rc = tpm_add_ipv6_mc_stream(ownerId, stream, vid, src, dst, ignore, port_bm);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to add MC stream = %d", stream);
    }
    else
    {
        PRINT("MC stream = %d has been added", stream);
    }

    return rc;
}
#endif /*OLD_TPM_APIS*/

#ifdef OLD_TPM_APIS
bool_t  tpm_cli_update_ipv6_mc_stream   (const clish_shell_t    *instance,
                                         const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            ownerId;
    uint32_t            stream;
    uint32_t            port_bm;


    /* Get parameters */
    ownerId = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    stream  = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    port_bm = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Owner ID = %d Stream = %d Port BM = %d ", ownerId, stream, port_bm);

    tpm_rc = tpm_updt_ipv6_mc_stream(ownerId, stream, port_bm);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to update MC stream = %d", stream);
    }
    else
    {
        PRINT("MC stream = %d has been updated", stream);
    }

    return rc;
}

#endif /*OLD_TPM_APIS*/


/********************************************************************************/
/*                      TPM CLI owner functions                                 */
/********************************************************************************/
bool_t  tpm_cli_create_owner   (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            ownerId;

    /* Get parameters */
    ownerId = tpm_cli_get_number(lub_argv__get_arg(argv, 0));

    DBG_PRINT("Owner ID = %d", ownerId);

    tpm_rc = tpm_create_ownerid(&ownerId);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to create Owner ID = %d", ownerId);
    }
    else
    {
        PRINT("Owner ID = %d has been created", ownerId);
    }

    return rc;
}

bool_t  tpm_cli_set_ownership   (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            ownerId;
    int                 apig;

    /* Get parameters */
    ownerId = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    apig    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    DBG_PRINT("Owner ID = %d, API group = %d", ownerId, apig);

    tpm_rc = tpm_request_api_ownership(ownerId, (tpm_api_type_t)apig);

    if (tpm_rc != API_OWNERSHIP_SUCCESS)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set Owner ID = %d for API group = %d (rc = %d)", ownerId, apig, tpm_rc);
    }
    else
    {
        PRINT("Owner ID = %d for API group = %d has been set", ownerId, apig);
    }

    return rc;
}


/********************************************************************************/
/*                      TPM CLI set functions                                   */
/********************************************************************************/
bool_t  tpm_cli_set_pkt_tagging (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            pkt_type;
    bool_t              enable;
    char                pkt_type_str[10];

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    pkt_type    = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    enable      = tpm_cli_get_number(lub_argv__get_arg(argv, 3));

    DBG_PRINT("Owner = %d port = %d pkt_type = %d enable = %d",
              owner_id, port, pkt_type, enable);

    /* pkt type = tagged(0), untagged(1) */
    if (pkt_type)
    {
        strcpy(pkt_type_str, "untagged");
        tpm_rc = tpm_sw_set_port_untagged(owner_id,
                                          (tpm_src_port_type_t)port,
                                          (uint8_t)enable);
    }
    else
    {
        strcpy(pkt_type_str, "tagged");
        tpm_rc = tpm_sw_set_port_tagged(owner_id,
                                        (tpm_src_port_type_t)port,
                                        (uint8_t)enable);
    }

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to %s %s packet", (enable?"enable":"disable"), pkt_type_str);
    }
    else
    {
        PRINT("%s packet has been %s", pkt_type_str, (enable?"enable":"disable"));
    }

    return rc;
}


bool_t  tpm_cli_add_vid (const clish_shell_t    *instance,
                         const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            vid;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    vid         = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Owner = %d port = %d vid = %d", owner_id, port, vid);

    tpm_rc = tpm_sw_port_add_vid (owner_id, (tpm_src_port_type_t)port, vid);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to add vid - %d", vid);
    }
    else
    {
        PRINT("%d vid has been added", vid);
    }

    return rc;
}


bool_t  tpm_cli_set_vid_filter (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint16_t            mode;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Owner = %d port = %d mode = %d", owner_id, port, mode);

    tpm_rc = tpm_sw_port_set_vid_filter (owner_id, (tpm_src_port_type_t)port, mode);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set VID filtering - %d", mode);
    }
    else
    {
        PRINT("VID filtering mode is \"%s allowed\"", mode?"":"NOT");
    }

    return rc;
}


bool_t  tpm_cli_set_port_flooding (const clish_shell_t    *instance,
                                   const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            allow;
    uint32_t            port;
    uint16_t            mode;


    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    allow          = tpm_cli_get_number(lub_argv__get_arg(argv, 3));

    DBG_PRINT("Owner = %d port = %d mode = %d, allow_flooding = %d \r\n", owner_id, port, (tpm_flood_type_t)mode, (bool_t)allow);

    tpm_rc = tpm_sw_set_port_flooding (owner_id, (tpm_src_port_type_t)port, (tpm_flood_type_t)mode, (bool_t)allow);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set port flooding - %d", mode);
    }
    else
    {
        PRINT("Port flooding is \"%s allowed\"", allow?"":"NOT");
    }

    return rc;
}


bool_t  tpm_cli_add_static_mac (const clish_shell_t    *instance,
                                const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint8_t             st_mac[6];
    const char          *mac_ptr;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mac_ptr     = lub_argv__get_arg(argv, 2);


    DBG_PRINT("Owner = %d port = %d mac = %s", owner_id, port, mac_ptr);

    sscanf(mac_ptr, "%x:%x:%x:%x:%x:%x",
           &(st_mac[0]), &(st_mac[1]), &(st_mac[2]),
           &(st_mac[3]), &(st_mac[4]), &(st_mac[5]));

    tpm_rc = tpm_sw_add_static_mac (owner_id, (tpm_src_port_type_t)port, st_mac);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to add static MAC - %x:%x:%x:%x:%x:%x",
              st_mac[0], st_mac[1], st_mac[2],
              st_mac[3], st_mac[4], st_mac[5]);
    }
    else
    {
        PRINT("%x:%x:%x:%x:%x:%x static MAC has been added",
              st_mac[0], st_mac[1], st_mac[2],
              st_mac[3], st_mac[4], st_mac[5]);
    }

    return rc;
}


bool_t  tpm_cli_set_max_macs (const clish_shell_t    *instance,
                              const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            num;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    num         = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Owner = %d port = %d mac num = %d", owner_id, port, num);

    tpm_rc = tpm_sw_set_port_max_macs (owner_id, (tpm_src_port_type_t)port, num);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set max MAC number - %d", num);
    }
    else
    {
        PRINT("%d - max MAC number has been set",num);
    }

    return rc;
}

bool_t  tpm_cli_set_queue_weight (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint8_t             queueId;
    uint16_t            weight;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    queueId     = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    weight      = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Owner = %d queueId = %d weight = %d", owner_id, queueId, weight);

    tpm_rc = tpm_sw_set_uni_q_weight (owner_id, queueId, weight);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set weight (%d) for queue %d", weight, queueId);
    }
    else
    {
        PRINT("%d - weight has been set for queue %d", weight, queueId);
    }

    return rc;
}


bool_t  tpm_cli_set_pcl_uni_rate (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            mode;
    uint32_t            cir;		
    uint32_t            cbs;
	uint32_t             ebs;	
	
    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mode         = tpm_cli_get_number(lub_argv__get_arg(argv, 2));		
    cir         = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    cbs         = tpm_cli_get_number(lub_argv__get_arg(argv, 4));
    ebs         = tpm_cli_get_number(lub_argv__get_arg(argv, 5));		

    DBG_PRINT("Owner = %d port = %d mode = %d cir = %d cbs = %d ebs =%d ", owner_id, port, mode, cir, cbs, ebs);

    tpm_rc = tpm_sw_set_uni_ingr_police_rate(owner_id, (tpm_trg_port_type_t)port, (tpm_limit_mode_t)mode, cir, cbs, ebs);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set policy rate for port %d", port);
    }
    else
    {
        PRINT("Policy rate set");
    }

    return rc;
}


bool_t  tpm_cli_set_pcl_tc_rate (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            tc;
    uint32_t            cir;
    uint32_t            cbs;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    tc          = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    cir         = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    cbs         = tpm_cli_get_number(lub_argv__get_arg(argv, 4));

    DBG_PRINT("Owner = %d port = %d tc = %d cir = %d cbs = %d", owner_id, port, tc, cir, cbs);

    tpm_rc = tpm_sw_set_uni_tc_ingr_police_rate (owner_id, (tpm_trg_port_type_t)port, tc, cir, cbs);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set tc rate for port %d", port);
    }
    else
    {
        PRINT("Policy tc rate set");
    }

    return rc;
}


bool_t  tpm_cli_set_uni_rate_limit (const clish_shell_t    *instance,
                                    const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            mode;		
    uint32_t            rate;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));		
    rate        = tpm_cli_get_number(lub_argv__get_arg(argv, 3));

    DBG_PRINT("Owner = %d port = %d mode = %d rate = %d", owner_id, port, mode, rate);

    tpm_rc = tpm_sw_set_uni_egr_rate_limit (owner_id, (tpm_trg_port_type_t)port, (tpm_limit_mode_t)mode, rate);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set rate limit for port %d", port);
    }
    else
    {
        PRINT("Rate limit set");
    }

    return rc;
}


bool_t  tpm_cli_set_wan_up_rate_limit (const clish_shell_t    *instance,
                                       const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            rate;
    uint32_t            size;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    rate        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    size        = tpm_cli_get_number(lub_argv__get_arg(argv, 3));

    DBG_PRINT("Owner = %d port = %d rate = %d size = %d", owner_id, port, rate, size);

    tpm_rc = tpm_tm_set_wan_sched_egr_rate_lim (owner_id, (tpm_trg_port_type_t)port, rate, size);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set rate limit for port %d", port);
    }
    else
    {
        PRINT("Rate limit set");
    }

    return rc;
}

bool_t  tpm_cli_set_wan_up_q_rate_limit (const clish_shell_t    *instance,
                                         const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            queueId;
    uint32_t            rate;
    uint32_t            size;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    queueId     = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    rate        = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    size        = tpm_cli_get_number(lub_argv__get_arg(argv, 4));

    DBG_PRINT("Owner = %d port = %d queueId = %d rate = %d size = %d", owner_id, port, queueId, rate, size);

    tpm_rc = tpm_tm_set_wan_queue_egr_rate_lim (owner_id, (tpm_trg_port_type_t)port, queueId, rate, size);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set rate limit for port %d", port);
    }
    else
    {
        PRINT("Rate limit set");
    }

    return rc;
}


bool_t  tpm_cli_set_wan_down_rate_limit (const clish_shell_t    *instance,
                                         const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rate;
    uint32_t            size;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    rate        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    size        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Owner = %d rate = %d size = %d", owner_id, rate, size);

    tpm_rc = tpm_tm_set_wan_ingr_rate_lim (owner_id, rate, size);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set rate limit");
    }
    else
    {
        PRINT("Rate limit set");
    }

    return rc;
}

bool_t  tpm_cli_set_wan_down_q_rate_limit (const clish_shell_t    *instance,
                                           const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            queueId;
    uint32_t            rate;
    uint32_t            size;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    queueId     = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    rate        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    size        = tpm_cli_get_number(lub_argv__get_arg(argv, 3));

    DBG_PRINT("Owner = %d queueId = %d rate = %d size = %d", owner_id, queueId, rate, size);

    tpm_rc = tpm_tm_set_wan_q_ingr_rate_lim (owner_id, queueId, rate, size);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set rate limit");
    }
    else
    {
        PRINT("Rate limit set");
    }

    return rc;
}


bool_t  tpm_cli_set_wan_sched (const clish_shell_t    *instance,
                               const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            port;
    uint32_t            mode;
    uint8_t             queueId;
    uint16_t            weight;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    port        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    mode        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    queueId     = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    weight      = tpm_cli_get_number(lub_argv__get_arg(argv, 4));

    DBG_PRINT("Owner = %d port = %d mode = %d queue = %d weight = %d", owner_id, port, mode, queueId, weight);

    tpm_rc = tpm_tm_set_wan_egr_queue_sched (owner_id, (tpm_trg_port_type_t)port, (tpm_pp_sched_type_t)mode, queueId, weight);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set scheduling mode for port %d", port);
    }
    else
    {
        PRINT("Scheduling mode set");
    }

    return rc;
}

bool_t  tpm_cli_set_omci_channel (const clish_shell_t    *instance,
                                  const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            gem;
    uint32_t            rx_q;
    uint32_t            tx_q;
    uint32_t            tcont;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    gem         = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    rx_q        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));
    tx_q        = tpm_cli_get_number(lub_argv__get_arg(argv, 3));
    tcont       = tpm_cli_get_number(lub_argv__get_arg(argv, 4));

    DBG_PRINT("Owner = %d gem = %d rx_q = %d tx_q = %d tcont = 0x%4.4x ", owner_id, gem, rx_q, tx_q, tcont);

    tpm_rc = tpm_omci_add_channel (owner_id, (tpm_gem_port_key_t)gem, rx_q, (tpm_trg_port_type_t)tcont, tx_q);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set OMCI channel for gem %d", gem);
    }
    else
    {
        PRINT("OMCI channel set");
    }

    return rc;
}

bool_t  tpm_cli_set_oam_channel (const clish_shell_t    *instance,
                                 const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    uint32_t            rx_q;
    uint32_t            llid;

    /* Get parameters */
    owner_id    = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    rx_q        = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    llid        = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    DBG_PRINT("Owner = %d rx_q = %d llid = 0x%4.4x ", owner_id, rx_q, llid);

    tpm_rc = tpm_oam_epon_add_channel (owner_id, rx_q, (tpm_trg_port_type_t)llid);

    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to set OAM channel");
    }
    else
    {
        PRINT("OAM channel set");
    }

    return rc;
}

bool_t tpm_cli_set_port_igmp_frwd_mode(const clish_shell_t *instance, 
                                                  const lub_argv_t *argv)
{
    uint32_t owner_id;
    uint32_t src_port;
    uint32_t frwd_mode;

    owner_id  = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    src_port  = tpm_cli_get_number(lub_argv__get_arg(argv, 1));
    frwd_mode = tpm_cli_get_number(lub_argv__get_arg(argv, 2));

    if (TPM_RC_OK == tpm_set_port_igmp_frwd_mode(src_port, frwd_mode))
    {
        PRINT("Port IGMP forward mode set");
        return BOOL_TRUE;
    }
    else
    {
        PRINT("Failed to set port IGMP forward mode");
        return BOOL_FALSE;
    }
}

bool_t tpm_cli_set_igmp_cpu_rx_queue(const clish_shell_t *instance, 
                                               const lub_argv_t *argv)
{
    uint32_t owner_id;
    uint32_t queue;

    owner_id = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    queue    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    if (TPM_RC_OK == tpm_set_igmp_cpu_rx_queue(queue))
    {
        PRINT("IGMP CPU RX queue set");
        return BOOL_TRUE;
    }
    else
    {
        PRINT("Failed to set IGMP CPU RX queue");
        return BOOL_FALSE;
    }
}


#if 0
bool_t  tpm_cli_igmp_config (const clish_shell_t    *instance,
                             const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            enable, owner_id;

    /* Get parameters */
    owner_id = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    enable   = tpm_cli_get_number(lub_argv__get_arg(argv, 1));


    DBG_PRINT("ower_id = %d, enable = %d", owner_id, enable);

    if (enable)
    {
    	tpm_rc = tpm_proc_enable_igmp (owner_id);
    }
    else
    {
    	tpm_rc = tpm_proc_disable_igmp (owner_id);
    }
    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to config IGMP");
    }
    else
    {
        PRINT("Config IGMP");
    }

    return rc;
}
#endif

/********************************************************************************/
/*                      TPM CLI init functions                                 */
/********************************************************************************/
bool_t  tpm_cli_init (const clish_shell_t    *instance,
                      const lub_argv_t       *argv)
{
    uint32_t    rc = BOOL_TRUE;

    rc = tpm_init();

    if (rc)
    {
        rc = BOOL_FALSE;
        PRINT("Failed to initialize TPM data");
    }
    else
    {
        PRINT("Action done");
    }

    return rc;
}

/********************************************************************************/
/*                      TPM CLI MIB Reset functions                             */
/********************************************************************************/
bool_t  tpm_cli_mib_reset (const clish_shell_t    *instance,
                           const lub_argv_t       *argv)
{
    bool_t              rc = BOOL_TRUE;
    tpm_error_code_t    tpm_rc = ERR_GENERAL;
    uint32_t            owner_id;
    tpm_reset_level_enum_t reset_level;


    /* Get parameters */
    owner_id       = tpm_cli_get_number(lub_argv__get_arg(argv, 0));
    reset_level    = tpm_cli_get_number(lub_argv__get_arg(argv, 1));

    tpm_rc = tpm_mib_reset(owner_id, reset_level);
    if (tpm_rc != TPM_RC_OK)
    {
        rc = BOOL_FALSE;
        printf("Failed to MIB Reset TPM \n");
    }

    return rc;
}
