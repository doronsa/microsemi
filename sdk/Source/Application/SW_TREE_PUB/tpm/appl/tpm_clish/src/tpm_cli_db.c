/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell 
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.  

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdbool.h>
#include "tpm_cli_db.h"
#include "tpm_print.h"

#define IPV6_ADDR_LEN (16)

/********************************************************************************/
/*                                  DB CLI tables                               */
/********************************************************************************/
static db_cli_l2_key_t      tpm_cli_l2_key_table[DB_CLI_MAX_ENTRIES];
static int                  number_of_l2_key_entries = 0;
char                        l2_key_empty_name[] = "l2_key_empty";

static db_cli_mod_t         tpm_cli_mod_table[DB_CLI_MAX_ENTRIES];
static int                  number_of_mod_entries = 0;
char                        mod_empty_name[] = "mod_empty";

static db_cli_frwd_t        tpm_cli_frwd_table[DB_CLI_MAX_ENTRIES];
static int                  number_of_frwd_entries = 0;
char                        frwd_empty_name[] = "frwd_empty";

static db_cli_vlan_mod_t    tpm_cli_vlan_table[DB_CLI_MAX_ENTRIES];
static int                  number_of_vlan_entries = 0;
char                        vlan_empty_name[] = "vlan_empty";

static db_cli_l3_key_t      tpm_cli_l3_key_table[DB_CLI_MAX_ENTRIES];
static int                  number_of_l3_key_entries = 0;
char                        l3_key_empty_name[] = "l3_key_empty";

static db_cli_ipv4_key_t    tpm_cli_ipv4_key_table[DB_CLI_MAX_ENTRIES];
static int                  number_of_ipv4_key_entries = 0;
char                        ipv4_key_empty_name[] = "ipv4_key_empty";

static db_cli_ipv6_key_t    tpm_cli_ipv6_key_table[DB_CLI_MAX_ENTRIES];
static int                  number_of_ipv6_key_entries = 0;
char                        ipv6_key_empty_name[] = "ipv6_key_empty";


/********************************************************************************/
/*                          DB ENUM string tables                               */
/********************************************************************************/
db_enum_string_t    db_tpm_src_port_str[] =
{
    BuildEnumString(TPM_SRC_PORT_UNI_0),     
    BuildEnumString(TPM_SRC_PORT_UNI_1),     
    BuildEnumString(TPM_SRC_PORT_UNI_2),    
    BuildEnumString(TPM_SRC_PORT_UNI_3),    
    BuildEnumString(TPM_SRC_PORT_UNI_4),    
    BuildEnumString(TPM_SRC_PORT_UNI_5),
    BuildEnumString(TPM_SRC_PORT_UNI_6),
    BuildEnumString(TPM_SRC_PORT_UNI_7),
    BuildEnumString(TPM_SRC_PORT_UNI_VIRT),
    BuildEnumString(TPM_SRC_PORT_WAN),
    BuildEnumString(TPM_SRC_PORT_UNI_ANY),
    BuildEnumString(TPM_SRC_PORT_WAN_OR_LAN),
    BuildEnumString(TPM_SRC_PORT_ILLEGAL)
};

db_enum_string_t    db_tpm_parse_stage_str[] =
{
    BuildEnumString(STAGE_L2_PRIM),
    BuildEnumString(STAGE_L3_TYPE),
    BuildEnumString(STAGE_IPv4),
    BuildEnumString(STAGE_IPv6_GEN),
    BuildEnumString(STAGE_IPv6_DIP),
    BuildEnumString(STAGE_IPv6_NH),
    BuildEnumString(STAGE_IPV6_L4),
    BuildEnumString(STAGE_CTC_CM),    
    BuildEnumString(STAGE_DONE)
};

db_enum_string_t    db_tpm_vlan_operation_str[] =
{
    BuildEnumString(VLANOP_NOOP),                        
    BuildEnumString(VLANOP_EXT_TAG_MOD),               
    BuildEnumString(VLANOP_EXT_TAG_DEL),              
    BuildEnumString(VLANOP_EXT_TAG_INS),             
    BuildEnumString(VLANOP_EXT_TAG_MOD_INS),         
    BuildEnumString(VLANOP_INS_2TAG), 
    BuildEnumString(VLANOP_MOD_2TAG),
    BuildEnumString(VLANOP_SWAP_TAGS),
    BuildEnumString(VLANOP_DEL_2TAG),          
    BuildEnumString(VLANOP_INT_TAG_MOD),       
    BuildEnumString(VLANOP_EXT_TAG_DEL_INT_MOD),
    BuildEnumString(VLANOP_SPLIT_MOD_PBIT),    
    BuildEnumString(VLANOP_ILLEGAL) 
};

/********************************************************************************/
/*                                    DB Utils                                  */
/********************************************************************************/
bool_t  db_hexadec_arg (const char    *arg)
{
    bool_t  rc = BOOL_FALSE;

    if (arg != NULL)
    {
        if ((arg[1] == 'x') || (arg[1] == 'X'))
            rc = BOOL_TRUE;
    }

    return rc;
}


void    tpm_cli_db_init (void)
{
    memset(tpm_cli_l2_key_table,   0, sizeof(db_cli_l2_key_t)*DB_CLI_MAX_ENTRIES);
    memset(tpm_cli_mod_table,      0, sizeof(db_cli_mod_t)*DB_CLI_MAX_ENTRIES);
    memset(tpm_cli_frwd_table,     0, sizeof(db_cli_frwd_t)*DB_CLI_MAX_ENTRIES);
    memset(tpm_cli_vlan_table,     0, sizeof(db_cli_vlan_mod_t)*DB_CLI_MAX_ENTRIES);
    memset(tpm_cli_l3_key_table,   0, sizeof(db_cli_l3_key_t)*DB_CLI_MAX_ENTRIES);
    memset(tpm_cli_ipv4_key_table, 0, sizeof(db_cli_ipv4_key_t)*DB_CLI_MAX_ENTRIES);
    memset(tpm_cli_ipv6_key_table, 0, sizeof(db_cli_ipv6_key_t)*DB_CLI_MAX_ENTRIES);

    /* Add empty entries */
    strcpy(tpm_cli_l2_key_table[0].name, l2_key_empty_name);
    number_of_l2_key_entries = 1;
    
    strcpy(tpm_cli_mod_table[0].name, mod_empty_name);
    tpm_cli_mod_table[0].flags |= DB_CLI_VLAN_FLAG;
    number_of_mod_entries = 1;
    
    strcpy(tpm_cli_frwd_table[0].name, frwd_empty_name);
    number_of_frwd_entries = 1;
    
    strcpy(tpm_cli_vlan_table[0].name, vlan_empty_name);
    number_of_vlan_entries = 1;
    
    strcpy(tpm_cli_l3_key_table[0].name, l3_key_empty_name);
    number_of_l3_key_entries = 1;
    
    strcpy(tpm_cli_ipv4_key_table[0].name, ipv4_key_empty_name);
    number_of_ipv4_key_entries = 1;
    
    strcpy(tpm_cli_ipv6_key_table[0].name, ipv6_key_empty_name);
    number_of_ipv6_key_entries = 1;
}


/********************************************************************************/
/*                          VLAN table DB functions                             */
/********************************************************************************/
static bool_t db_cli_get_empty_vlan_entry (int *idx)
{
    bool_t      found = BOOL_FALSE; 
    int         i; 

    if (number_of_vlan_entries >= DB_CLI_MAX_ENTRIES)
        return BOOL_FALSE;

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_vlan_table[i].name[0] == '\0') 
        {
            *idx = i;
            found = BOOL_TRUE;
            break;
        }
    }

    return found;
}

static bool_t db_cli_match_name_to_index_vlan_tbl (const char *name, int *idx)
{

    bool_t      found = BOOL_FALSE; 
    int         i; 

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_vlan_table[i].name[0] != '\0') 
        {
            if (strcmp(name, tpm_cli_vlan_table[i].name) == 0) 
            {
                *idx = i;
                found = BOOL_TRUE;
                break;
            }
        }
    }

    return found;
}

static bool_t  db_cli_looking_for_a_vlan_name (const char *name, int *idx)
{
    bool_t  rc;

    *idx = 0;

    if (db_cli_match_name_to_index_vlan_tbl(name, idx) == BOOL_FALSE) 
    {
        /* Looking for a first empty entry */
        if ((rc = db_cli_get_empty_vlan_entry(idx)) == BOOL_TRUE)
        {
            /* Initialize found entry */
            memset(&tpm_cli_vlan_table[*idx], 0, sizeof(db_cli_vlan_mod_t));
    
            strncpy(tpm_cli_vlan_table[*idx].name, name, DB_CLI_MAX_NAME);
            tpm_cli_vlan_table[*idx].name[DB_CLI_MAX_NAME] = '\0';
            number_of_vlan_entries++;
        }
        else
        {
            PRINT("VLAN DB table is full - %d", number_of_vlan_entries);
        }
    }
    else
    {
        /* VLAN name already exist on the DB */
        rc = BOOL_FALSE;
        PRINT(" %s - VLAN name already exist", name);
    }

    return rc;
}

bool_t  db_cli_add_vlan_to_vlan_tbl (const  char    *vlan_name, 
                                     uint16_t       tpid, 
                                     uint16_t       vid, 
                                     uint16_t       vid_mask, 
                                     uint8_t        cfi, 
                                     uint8_t        cfi_mask, 
                                     uint8_t        pbit, 
                                     uint8_t        pbit_mask)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_vlan_name(vlan_name, &idx)) == BOOL_TRUE) 
    {
        /* Add VLAN data to DB */
        tpm_cli_vlan_table[idx].vlan.tpid       = tpid;
		tpm_cli_vlan_table[idx].vlan.tpid_mask  = 0xffff;
        tpm_cli_vlan_table[idx].vlan.vid        = vid;
        tpm_cli_vlan_table[idx].vlan.vid_mask   = vid_mask;
        tpm_cli_vlan_table[idx].vlan.cfi        = cfi;
        tpm_cli_vlan_table[idx].vlan.cfi_mask   = cfi_mask;
        tpm_cli_vlan_table[idx].vlan.pbit       = pbit;
        tpm_cli_vlan_table[idx].vlan.pbit_mask  = pbit_mask;
    }

    return rc;
}

bool_t  db_cli_get_vlan_tbl_entry   (const char     *vlan_name,
                                     tpm_vlan_key_t *entry)
{
    bool_t  rc = BOOL_FALSE;
    int     idx;

    if ((rc = db_cli_match_name_to_index_vlan_tbl(vlan_name, &idx)) == BOOL_TRUE) 
    {
        memcpy(entry, &tpm_cli_vlan_table[idx].vlan, sizeof(tpm_vlan_key_t));
    }

    return rc;
}

bool_t  db_cli_del_vlan_tbl_entry   (const char     *vlan_name)
{
    bool_t  rc = BOOL_FALSE;
    int     idx;

    /* Empty VLAN name cannot be removed */
    if (strcmp(vlan_name, vlan_empty_name) != 0)
    {
        if ((rc = db_cli_match_name_to_index_vlan_tbl(vlan_name, &idx)) == BOOL_TRUE) 
        {
            tpm_cli_vlan_table[idx].name[0] = '\0';
            number_of_vlan_entries--;
        }
    }

    return rc;
}

/********************************************************************************/
/*                           Frwd packet DB functions                           */
/********************************************************************************/
static bool_t db_cli_get_empty_frwd_entry (int *idx)
{
    bool_t      found = BOOL_FALSE; 
    int         i; 

    if (number_of_frwd_entries >= DB_CLI_MAX_ENTRIES)
        return BOOL_FALSE;

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_frwd_table[i].name[0] == '\0') 
        {
            *idx = i;
            found = BOOL_TRUE;
            break;
        }
    }

    return found;
}

static bool_t db_cli_match_name_to_index_frwd_tbl (const char *name, int *idx)
{

    bool_t      found = BOOL_FALSE; 
    int         i; 

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_frwd_table[i].name[0] != '\0')
        {
            if (strcmp(name, tpm_cli_frwd_table[i].name) == 0) 
            {
                *idx = i;
                found = BOOL_TRUE;
                break;
            }
        }
    }

    return found;
}

static bool_t  db_cli_looking_for_a_frwd_name (const char *name, int *idx)
{
    bool_t  rc;

    *idx = 0;

    if ((rc = db_cli_match_name_to_index_frwd_tbl(name, idx)) != BOOL_TRUE) 
    {
        /* Looking for a first empty entry */
        if ((rc = db_cli_get_empty_frwd_entry(idx)) == BOOL_TRUE)
        {
            /* Initialize found entry */
            memset(&tpm_cli_frwd_table[*idx], 0, sizeof(db_cli_frwd_t));
    
            strncpy(tpm_cli_frwd_table[*idx].name, name, DB_CLI_MAX_NAME);
            tpm_cli_frwd_table[*idx].name[DB_CLI_MAX_NAME] = '\0';
            number_of_frwd_entries++;
        }
    }

    return rc;
}

bool_t  db_cli_get_pkt_frwd (const char           *pkt_name,
                             tpm_pkt_frwd_t       *pkt_frwd)
{
    bool_t  rc = BOOL_FALSE;
    int     idx;

    if ((rc = db_cli_match_name_to_index_frwd_tbl(pkt_name, &idx)) == BOOL_TRUE) 
    {
        pkt_frwd->gem_port  = tpm_cli_frwd_table[idx].frwd.gem_port;
        pkt_frwd->trg_queue = tpm_cli_frwd_table[idx].frwd.trg_queue;
        pkt_frwd->trg_port  = tpm_cli_frwd_table[idx].frwd.trg_port;
    }

    return rc;
}

bool_t  db_cli_add_frwd_to_db_tbl   (const char    *name, 
                                     uint32_t       port, 
                                     uint8_t        que, 
                                     uint16_t       gem)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_frwd_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add frwd data to DB */
        tpm_cli_frwd_table[idx].frwd.trg_port     = port;
        tpm_cli_frwd_table[idx].frwd.trg_queue    = que;
        tpm_cli_frwd_table[idx].frwd.gem_port     = gem;
    }

    return rc;
}

bool_t  db_cli_del_pkt_frwd_entry   (const char     *frwd_name)
{
    bool_t  rc = BOOL_FALSE;
    int     idx;

    /* Empty FRWD name cannot be removed */
    if (strcmp(frwd_name, frwd_empty_name) != 0)
    {
        if ((rc = db_cli_match_name_to_index_frwd_tbl(frwd_name, &idx)) == BOOL_TRUE) 
        {
            tpm_cli_frwd_table[idx].name[0] = '\0';
            number_of_frwd_entries--;
        }
    }

    return rc;
}


/********************************************************************************/
/*                      Packet modification DB functions                        */
/********************************************************************************/
static bool_t db_cli_get_empty_mod_entry (int *idx)
{
    bool_t      found = BOOL_FALSE; 
    int         i; 

    if (number_of_mod_entries >= DB_CLI_MAX_ENTRIES)
        return BOOL_FALSE;

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_mod_table[i].name[0] == '\0') 
        {
            *idx = i;
            found = BOOL_TRUE;
            break;
        }
    }

    return found;
}

static bool_t db_cli_match_name_to_index_mod_tbl (const char *name, int *idx)
{

    bool_t      found = BOOL_FALSE; 
    int         i; 

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_mod_table[i].name[0] != '\0')
        {
            if (strcmp(name, tpm_cli_mod_table[i].name) == 0) 
            {
                *idx = i;
                found = BOOL_TRUE;
                break;
            }
        }
    }

    return found;
}

static bool_t  db_cli_looking_for_a_mod_name (const char *name, int *idx)
{
    bool_t  rc;

    *idx = 0;

    if ((rc = db_cli_match_name_to_index_mod_tbl(name, idx)) != BOOL_TRUE) 
    {
        /* Looking for a first empty entry */
        if ((rc = db_cli_get_empty_mod_entry(idx)) == BOOL_TRUE)
        {
            /* Initialize found entry */
            memset(&tpm_cli_mod_table[*idx], 0, sizeof(db_cli_mod_t));
    
            strncpy(tpm_cli_mod_table[*idx].name, name, DB_CLI_MAX_NAME);
            tpm_cli_mod_table[*idx].name[DB_CLI_MAX_NAME] = '\0';
            number_of_mod_entries++;
        }
    }

    return rc;
}

bool_t  db_cli_get_pkt_mod  (const char           *pkt_name,
                             tpm_pkt_mod_t        *pkt_mod)
{
    bool_t  rc = BOOL_FALSE;
    int     idx;

    if ((rc = db_cli_match_name_to_index_mod_tbl(pkt_name, &idx)) == BOOL_TRUE) 
    {
        memcpy(pkt_mod, &tpm_cli_mod_table[idx].mod, sizeof(tpm_pkt_mod_t));
    }

    return rc;
}

bool_t  db_cli_del_pkt_mod_entry   (const char     *mod_name)
{
    bool_t  rc = BOOL_FALSE;
    int     idx;

    /* Empty Modification name cannot be removed */
    if (strcmp(mod_name, mod_empty_name) != 0)
    {
        if ((rc = db_cli_match_name_to_index_mod_tbl(mod_name, &idx)) == BOOL_TRUE) 
        {
            tpm_cli_mod_table[idx].name[0] = '\0';
            number_of_mod_entries--;
        }
    }

    return rc;
}

bool_t  db_cli_add_vlan_to_mod_tbl (const char         *name,
                                    tpm_vlan_oper_t    oper, 
                                    const char         *v1_name, 
                                    const char         *v2_name)
{
    bool_t      rc;
    int         idx, v1_idx, v2_idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_mod_name(name, &idx)) == BOOL_TRUE) 
    {
        tpm_cli_mod_table[idx].mod.vlan_mod.vlan_op  = oper;
        tpm_cli_mod_table[idx].flags |= DB_CLI_VLAN_FLAG;

        /* Add VLAN1 data to DB */
        if (v1_name != NULL) 
        {
            if ((rc = db_cli_match_name_to_index_vlan_tbl(v1_name, &v1_idx)) == BOOL_TRUE)
            {
                tpm_cli_mod_table[idx].mod.vlan_mod.vlan1_out.tpid        = tpm_cli_vlan_table[v1_idx].vlan.tpid;
				tpm_cli_mod_table[idx].mod.vlan_mod.vlan1_out.tpid_mask	  = 0xffff;
                tpm_cli_mod_table[idx].mod.vlan_mod.vlan1_out.vid         = tpm_cli_vlan_table[v1_idx].vlan.vid;
                tpm_cli_mod_table[idx].mod.vlan_mod.vlan1_out.vid_mask    = tpm_cli_vlan_table[v1_idx].vlan.vid_mask;
                tpm_cli_mod_table[idx].mod.vlan_mod.vlan1_out.cfi         = tpm_cli_vlan_table[v1_idx].vlan.cfi;
                tpm_cli_mod_table[idx].mod.vlan_mod.vlan1_out.cfi_mask    = tpm_cli_vlan_table[v1_idx].vlan.cfi_mask;
                tpm_cli_mod_table[idx].mod.vlan_mod.vlan1_out.pbit        = tpm_cli_vlan_table[v1_idx].vlan.pbit;
                tpm_cli_mod_table[idx].mod.vlan_mod.vlan1_out.pbit_mask   = tpm_cli_vlan_table[v1_idx].vlan.pbit_mask;
            }
            else
            {
                PRINT("%s - unknown VLAN name", v1_name);
            }
        }
        else
        {
            PRINT("NULL VLAN1 name");
        }

        if (rc == BOOL_TRUE) 
        {
            /* Add VLAN2 data to DB */
            if (v2_name != NULL) 
            {
                if ((rc = db_cli_match_name_to_index_vlan_tbl(v2_name, &v2_idx)) == BOOL_TRUE)
                {
                    tpm_cli_mod_table[idx].mod.vlan_mod.vlan2_out.tpid        = tpm_cli_vlan_table[v2_idx].vlan.tpid;
					tpm_cli_mod_table[idx].mod.vlan_mod.vlan2_out.tpid_mask	  = 0xffff;
                    tpm_cli_mod_table[idx].mod.vlan_mod.vlan2_out.vid         = tpm_cli_vlan_table[v2_idx].vlan.vid;
                    tpm_cli_mod_table[idx].mod.vlan_mod.vlan2_out.vid_mask    = tpm_cli_vlan_table[v2_idx].vlan.vid_mask;
                    tpm_cli_mod_table[idx].mod.vlan_mod.vlan2_out.cfi         = tpm_cli_vlan_table[v2_idx].vlan.cfi;
                    tpm_cli_mod_table[idx].mod.vlan_mod.vlan2_out.cfi_mask    = tpm_cli_vlan_table[v2_idx].vlan.cfi_mask;
                    tpm_cli_mod_table[idx].mod.vlan_mod.vlan2_out.pbit        = tpm_cli_vlan_table[v2_idx].vlan.pbit;
                    tpm_cli_mod_table[idx].mod.vlan_mod.vlan2_out.pbit_mask   = tpm_cli_vlan_table[v2_idx].vlan.pbit_mask;
                }
                else
                {
                    PRINT("%s - unknown VLAN name", v2_name);
                }
            }
        }
    }

    return rc;
}

bool_t  db_cli_add_mac_to_mod_tbl   (const char      *name,
                                     tpm_mac_key_t   *mac)
{
    bool_t      rc = BOOL_TRUE;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_mod_name(name, &idx)) == BOOL_TRUE) 
    {
       memcpy(&(tpm_cli_mod_table[idx].mod.mac_mod), mac, sizeof(tpm_mac_key_t));
       tpm_cli_mod_table[idx].flags |= DB_CLI_MAC_FLAG;

       DBG_PRINT("===DA %x:%x:%x:%x:%x:%x", 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da[0], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da[1], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da[2], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da[3],
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da[4], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da[5]);
       DBG_PRINT("===DA mask %x:%x:%x:%x:%x:%x", 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da_mask[0], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da_mask[1], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da_mask[2], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da_mask[3], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da_mask[4], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_da_mask[5]);
       DBG_PRINT("===SA %x:%x:%x:%x:%x:%x", 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa[0], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa[1], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa[2], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa[3], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa[4], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa[5]);
       DBG_PRINT("===SA mask %x:%x:%x:%x:%x:%x", 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa_mask[0], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa_mask[1], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa_mask[2], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa_mask[3], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa_mask[4], 
                 tpm_cli_mod_table[idx].mod.mac_mod.mac_sa_mask[5]);
    }

    return rc;
}

bool_t  db_cli_add_pppoe_to_mod_tbl (const char    *name,
                                     uint16_t      session,
                                     uint16_t      protocol)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_mod_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add PPPoE to DB */
        tpm_cli_mod_table[idx].mod.pppoe_mod.ppp_session = session;
        tpm_cli_mod_table[idx].mod.pppoe_mod.ppp_proto   = protocol;
        tpm_cli_mod_table[idx].flags |= DB_CLI_PPPOE_FLAG;
    }

    return rc;
}

bool_t  db_cli_add_dscp_to_mod_tbl  (const char  *name,
                                     uint8_t     dscp,
                                     uint8_t     mask)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_mod_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add DSCP to DB */
        tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dscp      = dscp;
        tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dscp_mask = mask;
        tpm_cli_mod_table[idx].flags |= DB_CLI_DSCP_FLAG;
    }

    return rc;
}


bool_t  db_cli_add_proto_to_mod_tbl (const char  *name,
                                     uint8_t     proto)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_mod_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add IPv4 protocol to DB */
        tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_proto = proto;
        tpm_cli_mod_table[idx].flags |= DB_CLI_IP_PROTO_FLAG;
    }

    return rc;
}


bool_t  db_cli_add_ports_to_ipv4_mod_tbl (const char  *name,
                                          uint16_t    src,
                                          uint16_t    dst)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_mod_name(name, &idx)) == BOOL_TRUE) 
    {
        if (tpm_cli_mod_table[idx].flags & DB_CLI_IPV6_FLAG) 
        {
            PRINT("This modification is used for IPv6 only");
            return BOOL_FALSE;
        }

        /* Add ports to DB */
        tpm_cli_mod_table[idx].mod.l3.ipv4_mod.l4_src_port = src;
        tpm_cli_mod_table[idx].mod.l3.ipv4_mod.l4_dst_port = dst;

        tpm_cli_mod_table[idx].flags |= DB_CLI_IPV4_FLAG;
    }

    return rc;
}


bool_t  db_cli_add_ports_to_ipv6_mod_tbl (const char  *name,
                                          uint16_t    src,
                                          uint16_t    dst)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_mod_name(name, &idx)) == BOOL_TRUE) 
    {
        if (tpm_cli_mod_table[idx].flags & DB_CLI_IPV4_FLAG) 
        {
            PRINT("This modification is used for IPv4 only");
            return BOOL_FALSE;
        }

        /* Add ports to DB */
        tpm_cli_mod_table[idx].mod.l3.ipv6_mod.l4_src_port = src;
        tpm_cli_mod_table[idx].mod.l3.ipv6_mod.l4_dst_port = dst;

        tpm_cli_mod_table[idx].flags |= DB_CLI_IPV6_FLAG;
    }

    return rc;
}


bool_t  db_cli_add_ips_to_ipv4_mod_tbl   (const char  *name,
                                          uint8_t     *src,
                                          uint8_t     *src_mask,
                                          uint8_t     *dst,
                                          uint8_t     *dst_mask)
{
    bool_t      rc;
    int         idx;


    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_mod_name(name, &idx)) == BOOL_TRUE) 
    {
        if (tpm_cli_mod_table[idx].flags & DB_CLI_IPV6_FLAG) 
        {
            PRINT("This modification is used for IPv6only");
            return BOOL_FALSE;
        }

        /* Add IP addresses to DB */
        memcpy(&(tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_src_ip_add), src, DB_CLI_IPV4_ADDR_LEN);
        DBG_PRINT("===SRC %u.%u.%u.%u", 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_src_ip_add[0], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_src_ip_add[1], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_src_ip_add[2], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_src_ip_add[3]);

        memcpy(&(tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_src_ip_add_mask), src_mask, DB_CLI_IPV4_ADDR_LEN);
        DBG_PRINT("===SRC mask %u.%u.%u.%u", 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_src_ip_add_mask[0], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_src_ip_add_mask[1], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_src_ip_add_mask[2], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_src_ip_add_mask[3]);

        memcpy(&(tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dst_ip_add), dst, DB_CLI_IPV4_ADDR_LEN);
        DBG_PRINT("===DST %u.%u.%u.%u", 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dst_ip_add[0], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dst_ip_add[1], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dst_ip_add[2], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dst_ip_add[3]);

        memcpy(&(tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dst_ip_add_mask), dst_mask, DB_CLI_IPV4_ADDR_LEN);
        DBG_PRINT("===DST mask %u.%u.%u.%u", 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dst_ip_add_mask[0], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dst_ip_add_mask[1], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dst_ip_add_mask[2], 
                  tpm_cli_mod_table[idx].mod.l3.ipv4_mod.ipv4_dst_ip_add_mask[3]);

        tpm_cli_mod_table[idx].flags |= DB_CLI_IPV4_FLAG;
    }

    return rc;
}


bool_t  db_cli_add_ips_to_ipv6_mod_tbl   (const char  *name,
                                          uint8_t     *src,
                                          uint8_t     *src_mask,
                                          uint8_t     *dst,
                                          uint8_t     *dst_mask)
{
    bool_t      rc;
    int         idx;
    char        ipv6_str[DB_CLI_IPV6_STR_LEN]; 

    DBG_PRINT("SRC  %s", db_ipv6_to_str(src, ipv6_str));
    DBG_PRINT("SRC mask %s", db_ipv6_to_str(src_mask, ipv6_str));
    DBG_PRINT("DST  %s", db_ipv6_to_str(dst, ipv6_str));
    DBG_PRINT("DST mask %s", db_ipv6_to_str(dst_mask, ipv6_str));

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_mod_name(name, &idx)) == BOOL_TRUE) 
    {
        if (tpm_cli_mod_table[idx].flags & DB_CLI_IPV4_FLAG) 
        {
            PRINT("This modification is used for IPv4 only");
            return BOOL_FALSE;
        }

        /* Add IP addresses to DB */
        memcpy(&(tpm_cli_mod_table[idx].mod.l3.ipv6_mod.ipv6_src_ip_add), src, DB_CLI_IPV6_ADDR_LEN);
        memcpy(&(tpm_cli_mod_table[idx].mod.l3.ipv6_mod.ipv6_src_ip_add_mask), src_mask, DB_CLI_IPV6_ADDR_LEN);
        memcpy(&(tpm_cli_mod_table[idx].mod.l3.ipv6_mod.ipv6_dst_ip_add), dst, DB_CLI_IPV6_ADDR_LEN);
        memcpy(&(tpm_cli_mod_table[idx].mod.l3.ipv6_mod.ipv6_dst_ip_add_mask), dst_mask, DB_CLI_IPV6_ADDR_LEN);

        tpm_cli_mod_table[idx].flags |= DB_CLI_IPV6_FLAG;
    }

    return rc;
}


/********************************************************************************/
/*                           L2 key DB functions                                */
/********************************************************************************/
static bool_t db_cli_get_empty_l2key_entry (int *idx)
{
    bool_t      found = BOOL_FALSE; 
    int         i; 

    if (number_of_l2_key_entries >= DB_CLI_MAX_ENTRIES)
        return BOOL_FALSE;

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_l2_key_table[i].name[0] == '\0') 
        {
            *idx = i;
            found = BOOL_TRUE;
            break;
        }
    }

    return found;
}

static bool_t db_cli_match_name_to_index_l2key_tbl (const char *name, int *idx)
{

    bool_t      found = BOOL_FALSE; 
    int         i; 

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_l2_key_table[i].name[0] != '\0')
        {
            if (strcmp(name, tpm_cli_l2_key_table[i].name) == 0) 
            {
                *idx = i;
                found = BOOL_TRUE;
                break;
            }
        }
    }

    return found;
}

static bool_t  db_cli_looking_for_a_l2key_name (const char *name, int *idx)
{
    bool_t  rc;

    *idx = 0;

    if ((rc = db_cli_match_name_to_index_l2key_tbl(name, idx)) != BOOL_TRUE) 
    {
        /* Looking for a first empty entry */
        if ((rc = db_cli_get_empty_l2key_entry(idx)) == BOOL_TRUE)
        {
            /* Initialize found entry */
            memset(&tpm_cli_l2_key_table[*idx], 0, sizeof(db_cli_l2_key_t));
    
            strncpy(tpm_cli_l2_key_table[*idx].name, name, DB_CLI_MAX_NAME);
            tpm_cli_l2_key_table[*idx].name[DB_CLI_MAX_NAME] = '\0';
            number_of_l2_key_entries++;
        }
    }

    return rc;
}

bool_t  db_cli_get_l2key      (const char           *key_name, 
                               tpm_l2_acl_key_t     *l2_key)
{
    bool_t      rc;
    int         idx;

    if ((rc = db_cli_match_name_to_index_l2key_tbl(key_name, &idx)) == BOOL_TRUE) 
    {
        memcpy(l2_key, &tpm_cli_l2_key_table[idx].acl, sizeof(tpm_l2_acl_key_t));
    }

    return rc;
}

bool_t  db_cli_del_l2key_entry   (const char     *l2key_name)
{
    bool_t  rc = BOOL_FALSE;
    int     idx;

    /* Empty l2key name cannot be removed */
    if (strcmp(l2key_name, l2_key_empty_name) != 0)
    {
        if ((rc = db_cli_match_name_to_index_l2key_tbl(l2key_name, &idx)) == BOOL_TRUE) 
        {
            tpm_cli_l2_key_table[idx].name[0] = '\0';
            number_of_l2_key_entries--;
        }
    }

    return rc;
}

bool_t  db_cli_add_ety_to_l2key_tbl(const char *name, uint32_t ety)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_l2key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add ETY to DB */
        tpm_cli_l2_key_table[idx].acl.ether_type = ety;
    }

    return rc;
}

bool_t  db_cli_add_mac_to_l2key_tbl(const char *name, tpm_mac_key_t *mac)
{
    bool_t      rc = BOOL_TRUE;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_l2key_name(name, &idx)) == BOOL_TRUE) 
    {
       memcpy(&(tpm_cli_l2_key_table[idx].acl.mac), mac, sizeof(tpm_mac_key_t));

       DBG_PRINT("===DA %x:%x:%x:%x:%x:%x", 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da[0], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da[1], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da[2], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da[3],
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da[4], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da[5]);
       DBG_PRINT("===DA mask %x:%x:%x:%x:%x:%x", 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da_mask[0], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da_mask[1], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da_mask[2], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da_mask[3], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da_mask[4], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_da_mask[5]);
       DBG_PRINT("===SA %x:%x:%x:%x:%x:%x", 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa[0], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa[1], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa[2], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa[3], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa[4], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa[5]);
       DBG_PRINT("===SA mask %x:%x:%x:%x:%x:%x", 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa_mask[0], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa_mask[1], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa_mask[2], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa_mask[3], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa_mask[4], 
                 tpm_cli_l2_key_table[idx].acl.mac.mac_sa_mask[5]);
    }

    return rc;
}

bool_t  db_cli_add_vlan_to_l2key_tbl (const char    *name, 
                                      const char    *v1_name, 
                                      const char    *v2_name)
{
    bool_t      rc = BOOL_TRUE;
    int         idx, v1_idx, v2_idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_l2key_name(name, &idx)) == BOOL_TRUE) 
    {

        /* Add VLAN1 data to DB */
        if (v1_name != NULL) 
        {
            if ((rc = db_cli_match_name_to_index_vlan_tbl(v1_name, &v1_idx)) == BOOL_TRUE)
            {
                tpm_cli_l2_key_table[idx].acl.vlan1.tpid        = tpm_cli_vlan_table[v1_idx].vlan.tpid;
				tpm_cli_l2_key_table[idx].acl.vlan1.tpid_mask	= 0xffff;
                tpm_cli_l2_key_table[idx].acl.vlan1.vid         = tpm_cli_vlan_table[v1_idx].vlan.vid;
                tpm_cli_l2_key_table[idx].acl.vlan1.vid_mask    = tpm_cli_vlan_table[v1_idx].vlan.vid_mask;
                tpm_cli_l2_key_table[idx].acl.vlan1.cfi         = tpm_cli_vlan_table[v1_idx].vlan.cfi;
                tpm_cli_l2_key_table[idx].acl.vlan1.cfi_mask    = tpm_cli_vlan_table[v1_idx].vlan.cfi_mask;
                tpm_cli_l2_key_table[idx].acl.vlan1.pbit        = tpm_cli_vlan_table[v1_idx].vlan.pbit;
                tpm_cli_l2_key_table[idx].acl.vlan1.pbit_mask   = tpm_cli_vlan_table[v1_idx].vlan.pbit_mask;
            }
            else
            {
                PRINT("%s - unknown VLAN name", v1_name);
            }
        }
        else
        {
            PRINT("NULL VLAN1 name");
        }

        if (rc == BOOL_TRUE) 
        {
            /* Add VLAN2 data to DB */
            if (v2_name != NULL) 
            {
                if ((rc = db_cli_match_name_to_index_vlan_tbl(v2_name, &v2_idx)) == BOOL_TRUE)
                {
                    tpm_cli_l2_key_table[idx].acl.vlan2.tpid        = tpm_cli_vlan_table[v2_idx].vlan.tpid;
					tpm_cli_l2_key_table[idx].acl.vlan2.tpid_mask	= 0xffff;
                    tpm_cli_l2_key_table[idx].acl.vlan2.vid         = tpm_cli_vlan_table[v2_idx].vlan.vid;
                    tpm_cli_l2_key_table[idx].acl.vlan2.vid_mask    = tpm_cli_vlan_table[v2_idx].vlan.vid_mask;
                    tpm_cli_l2_key_table[idx].acl.vlan2.cfi         = tpm_cli_vlan_table[v2_idx].vlan.cfi;
                    tpm_cli_l2_key_table[idx].acl.vlan2.cfi_mask    = tpm_cli_vlan_table[v2_idx].vlan.cfi_mask;
                    tpm_cli_l2_key_table[idx].acl.vlan2.pbit        = tpm_cli_vlan_table[v2_idx].vlan.pbit;
                    tpm_cli_l2_key_table[idx].acl.vlan2.pbit_mask   = tpm_cli_vlan_table[v2_idx].vlan.pbit_mask;
                }
                else
                {
                    PRINT("%s - unknown VLAN name", v2_name);
                }
            }
        }
    }

    return rc;
}

bool_t  db_cli_add_pppoe_to_l2key_tbl(const char    *name, 
                                      uint16_t      session, 
                                      uint16_t      protocol)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_l2key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add PPPoE to DB */
        tpm_cli_l2_key_table[idx].acl.pppoe_hdr.ppp_session = session;
        tpm_cli_l2_key_table[idx].acl.pppoe_hdr.ppp_proto   = protocol;
    }

    return rc;
}

bool_t  db_cli_add_gem_to_l2key_tbl(const char      *name, 
                                    uint16_t        gem)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_l2key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add GEM to DB */
        tpm_cli_l2_key_table[idx].acl.gem_port = gem;
    }

    return rc;
}

/********************************************************************************/
/*                           L3 key DB functions                                */
/********************************************************************************/
static bool_t db_cli_get_empty_l3key_entry (int *idx)
{
    bool_t      found = BOOL_FALSE; 
    int         i; 

    if (number_of_l3_key_entries >= DB_CLI_MAX_ENTRIES)
        return BOOL_FALSE;

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_l3_key_table[i].name[0] == '\0') 
        {
            *idx = i;
            found = BOOL_TRUE;
            break;
        }
    }

    return found;
}

static bool_t db_cli_match_name_to_index_l3key_tbl (const char *name, int *idx)
{

    bool_t      found = BOOL_FALSE; 
    int         i; 

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_l3_key_table[i].name[0] != '\0')
        {
            if (strcmp(name, tpm_cli_l3_key_table[i].name) == 0) 
            {
                *idx = i;
                found = BOOL_TRUE;
                break;
            }
        }
    }

    return found;
}

static bool_t  db_cli_looking_for_a_l3key_name (const char *name, int *idx)
{
    bool_t  rc;

    *idx = 0;

    if ((rc = db_cli_match_name_to_index_l3key_tbl(name, idx)) != BOOL_TRUE) 
    {
        /* Looking for a first empty entry */
        if ((rc = db_cli_get_empty_l3key_entry(idx)) == BOOL_TRUE)
        {
            /* Initialize found entry */
            memset(&tpm_cli_l3_key_table[*idx], 0, sizeof(db_cli_l3_key_t));
    
            strncpy(tpm_cli_l3_key_table[*idx].name, name, DB_CLI_MAX_NAME);
            tpm_cli_l3_key_table[*idx].name[DB_CLI_MAX_NAME] = '\0';
            number_of_l3_key_entries++;
        }
    }

    return rc;
}

bool_t  db_cli_get_l3key      (const char           *key_name, 
                               tpm_l3_type_key_t    *l3_key)
{
    bool_t      rc;
    int         idx;

    if ((rc = db_cli_match_name_to_index_l3key_tbl(key_name, &idx)) == BOOL_TRUE) 
    {
        memcpy(l3_key, &tpm_cli_l3_key_table[idx].key, sizeof(tpm_l3_type_key_t));
    }

    return rc;
}

bool_t  db_cli_del_l3key_entry   (const char     *l3key_name)
{
    bool_t  rc = BOOL_FALSE;
    int     idx;

    /* Empty l3key name cannot be removed */
    if (strcmp(l3key_name, l3_key_empty_name) != 0)
    {
        if ((rc = db_cli_match_name_to_index_l3key_tbl(l3key_name, &idx)) == BOOL_TRUE) 
        {
            tpm_cli_l3_key_table[idx].name[0] = '\0';
            number_of_l3_key_entries--;
        }
    }

    return rc;
}

bool_t  db_cli_add_pppoe_to_l3key_tbl(const char    *name, 
                                      uint16_t      session, 
                                      uint16_t      protocol)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_l3key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add PPPoE to DB */
        tpm_cli_l3_key_table[idx].key.pppoe_key.ppp_session = session;
        tpm_cli_l3_key_table[idx].key.pppoe_key.ppp_proto   = protocol;
    }

    return rc;
}

bool_t  db_cli_add_ety_to_l3key_tbl(const char *name, uint32_t ety)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_l3key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add ETY to DB */
        tpm_cli_l3_key_table[idx].key.ether_type_key = ety;
    }

    return rc;
}

/********************************************************************************/
/*                           IPv4 key DB functions                              */
/********************************************************************************/
static bool_t db_cli_get_empty_ipv4_entry (int *idx)
{
    bool_t      found = BOOL_FALSE; 
    int         i; 

    if (number_of_ipv4_key_entries >= DB_CLI_MAX_ENTRIES)
        return BOOL_FALSE;

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_ipv4_key_table[i].name[0] == '\0') 
        {
            *idx = i;
            found = BOOL_TRUE;
            break;
        }
    }

    return found;
}

static bool_t db_cli_match_name_to_index_ipv4_tbl (const char *name, int *idx)
{

    bool_t      found = BOOL_FALSE; 
    int         i; 

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_ipv4_key_table[i].name[0] != '\0')
        {
            if (strcmp(name, tpm_cli_ipv4_key_table[i].name) == 0) 
            {
                *idx = i;
                found = BOOL_TRUE;
                break;
            }
        }
    }

    return found;
}

static bool_t  db_cli_looking_for_a_ipv4_key_name (const char *name, int *idx)
{
    bool_t  rc;

    *idx = 0;

    if ((rc = db_cli_match_name_to_index_ipv4_tbl(name, idx)) != BOOL_TRUE) 
    {
        /* Looking for a first empty entry */
        if ((rc = db_cli_get_empty_ipv4_entry(idx)) == BOOL_TRUE)
        {
            /* Initialize found entry */
            memset(&tpm_cli_ipv4_key_table[*idx], 0, sizeof(db_cli_ipv4_key_t));
    
            strncpy(tpm_cli_ipv4_key_table[*idx].name, name, DB_CLI_MAX_NAME);
            tpm_cli_ipv4_key_table[*idx].name[DB_CLI_MAX_NAME] = '\0';
            number_of_ipv4_key_entries++;
        }
    }

    return rc;
}

bool_t  db_cli_get_ipv4_key (const char           *key_name,
                             tpm_ipv4_acl_key_t   *ipv4_key)
{
    bool_t      rc;
    int         idx;

    if ((rc = db_cli_match_name_to_index_ipv4_tbl(key_name, &idx)) == BOOL_TRUE) 
    {
        memcpy(ipv4_key, &tpm_cli_ipv4_key_table[idx].key, sizeof(tpm_ipv4_acl_key_t));
    }

    return rc;
}

bool_t  db_cli_del_ipv4_key_entry   (const char     *ipv4_key_name)
{
    bool_t  rc = BOOL_FALSE;
    int     idx;

    /* Empty ipv4 key name cannot be removed */
    if (strcmp(ipv4_key_name, ipv4_key_empty_name) != 0)
    {
        if ((rc = db_cli_match_name_to_index_ipv4_tbl(ipv4_key_name, &idx)) == BOOL_TRUE) 
        {
            tpm_cli_ipv4_key_table[idx].name[0] = '\0';
            number_of_ipv4_key_entries--;
        }
    }

    return rc;
}

bool_t  db_cli_add_dscp_to_ipv4_key_tbl(const char  *name,
                                        uint8_t     dscp,
                                        uint8_t     mask)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_ipv4_key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add DSCP to DB */
        tpm_cli_ipv4_key_table[idx].key.ipv4_dscp       = dscp;
        tpm_cli_ipv4_key_table[idx].key.ipv4_dscp_mask  = mask;
    }

    return rc;
}

bool_t  db_cli_add_proto_to_ipv4_key_tbl(const char  *name,
                                         uint8_t     proto)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_ipv4_key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add IPv4 protocol to DB */
        tpm_cli_ipv4_key_table[idx].key.ipv4_proto = proto;
    }

    return rc;
}

bool_t  db_cli_add_ports_to_ipv4_key_tbl(const char  *name,
                                         uint16_t    src,
                                         uint16_t    dst)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_ipv4_key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add ports to DB */
        tpm_cli_ipv4_key_table[idx].key.l4_src_port = src;
        tpm_cli_ipv4_key_table[idx].key.l4_dst_port = dst;
    }

    return rc;
}

bool_t  db_cli_add_ips_to_ipv4_key_tbl  (const char  *name,
                                         uint8_t     *src,
                                         uint8_t     *src_mask,
                                         uint8_t     *dst,
                                         uint8_t     *dst_mask)
{
    bool_t      rc;
    int         idx;


    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_ipv4_key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add IP addresses to DB */
        memcpy(&(tpm_cli_ipv4_key_table[idx].key.ipv4_src_ip_add), src, DB_CLI_IPV4_ADDR_LEN);
        DBG_PRINT("===SRC %u.%u.%u.%u", 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_src_ip_add[0], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_src_ip_add[1], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_src_ip_add[2], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_src_ip_add[3]);

        memcpy(&(tpm_cli_ipv4_key_table[idx].key.ipv4_src_ip_add_mask), src_mask, DB_CLI_IPV4_ADDR_LEN);
        DBG_PRINT("===SRC mask %u.%u.%u.%u", 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_src_ip_add_mask[0], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_src_ip_add_mask[1], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_src_ip_add_mask[2], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_src_ip_add_mask[3]);

        memcpy(&(tpm_cli_ipv4_key_table[idx].key.ipv4_dst_ip_add), dst, DB_CLI_IPV4_ADDR_LEN);
        DBG_PRINT("===DST %u.%u.%u.%u", 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_dst_ip_add[0], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_dst_ip_add[1], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_dst_ip_add[2], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_dst_ip_add[3]);

        memcpy(&(tpm_cli_ipv4_key_table[idx].key.ipv4_dst_ip_add_mask), dst_mask, DB_CLI_IPV4_ADDR_LEN);
        DBG_PRINT("===DST mask %u.%u.%u.%u", 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_dst_ip_add_mask[0], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_dst_ip_add_mask[1], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_dst_ip_add_mask[2], 
                  tpm_cli_ipv4_key_table[idx].key.ipv4_dst_ip_add_mask[3]);

    }

    return rc;
}


/********************************************************************************/
/*                           IPv6 key DB functions                              */
/********************************************************************************/
static bool_t db_cli_get_empty_ipv6_entry (int *idx)
{
    bool_t      found = BOOL_FALSE; 
    int         i; 

    if (number_of_ipv6_key_entries >= DB_CLI_MAX_ENTRIES)
        return BOOL_FALSE;

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_ipv6_key_table[i].name[0] == '\0') 
        {
            *idx = i;
            found = BOOL_TRUE;
            break;
        }
    }

    return found;
}

static bool_t db_cli_match_name_to_index_ipv6_tbl (const char *name, int *idx)
{

    bool_t      found = BOOL_FALSE; 
    int         i; 

    for (i=0; i < DB_CLI_MAX_ENTRIES; i++)
    {
        if (tpm_cli_ipv6_key_table[i].name[0] != '\0')
        {
            if (strcmp(name, tpm_cli_ipv6_key_table[i].name) == 0) 
            {
                *idx = i;
                found = BOOL_TRUE;
                break;
            }
        }
    }

    return found;
}

static bool_t  db_cli_looking_for_a_ipv6_key_name (const char *name, int *idx)
{
    bool_t  rc;

    *idx = 0;

    if ((rc = db_cli_match_name_to_index_ipv6_tbl(name, idx)) != BOOL_TRUE) 
    {
        /* Looking for a first empty entry */
        if ((rc = db_cli_get_empty_ipv6_entry(idx)) == BOOL_TRUE)
        {
            /* Initialize found entry */
            memset(&tpm_cli_ipv6_key_table[*idx], 0, sizeof(db_cli_ipv6_key_t));
    
            strncpy(tpm_cli_ipv6_key_table[*idx].name, name, DB_CLI_MAX_NAME);
            tpm_cli_ipv6_key_table[*idx].name[DB_CLI_MAX_NAME] = '\0';
            number_of_ipv6_key_entries++;
        }
    }

    return rc;
}

bool_t  db_cli_get_ipv6_key (const char           *key_name,
                             tpm_ipv6_acl_key_t   *ipv6_key)
{
    bool_t      rc;
    int         idx;

    if ((rc = db_cli_match_name_to_index_ipv6_tbl(key_name, &idx)) == BOOL_TRUE) 
    {
        memcpy(ipv6_key, &tpm_cli_ipv6_key_table[idx].key, sizeof(tpm_ipv6_acl_key_t));
    }

    return rc;
}

bool_t  db_cli_del_ipv6_key_entry   (const char     *ipv6_key_name)
{
    bool_t  rc = BOOL_FALSE;
    int     idx;

    /* Empty ipv6 key name cannot be removed */
    if (strcmp(ipv6_key_name, ipv6_key_empty_name) != 0)
    {
        if ((rc = db_cli_match_name_to_index_ipv6_tbl(ipv6_key_name, &idx)) == BOOL_TRUE) 
        {
            tpm_cli_ipv6_key_table[idx].name[0] = '\0';
            number_of_ipv6_key_entries--;
        }
    }

    return rc;
}

bool_t  db_cli_add_dscp_to_ipv6_key_tbl(const char  *name,
                                        uint8_t     dscp,
                                        uint8_t     mask)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_ipv6_key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add DSCP to DB */
        tpm_cli_ipv6_key_table[idx].key.ipv6_dscp       = dscp;
        tpm_cli_ipv6_key_table[idx].key.ipv6_dscp_mask  = mask;
    }

    return rc;
}

bool_t  db_cli_add_proto_to_ipv6_key_tbl(const char  *name,
                                         uint8_t     proto)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_ipv6_key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add next encapsulated protocol to DB */
        tpm_cli_ipv6_key_table[idx].key.ipv6_next_header = proto;
    }

    return rc;
}

bool_t  db_cli_add_ports_to_ipv6_key_tbl(const char  *name,
                                         uint16_t    src,
                                         uint16_t    dst)
{
    bool_t      rc;
    int         idx;

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_ipv6_key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add ports to DB */
        tpm_cli_ipv6_key_table[idx].key.l4_src_port = src;
        tpm_cli_ipv6_key_table[idx].key.l4_dst_port = dst;
    }

    return rc;
}

bool_t  db_cli_add_ips_to_ipv6_key_tbl  (const char  *name,
                                         uint8_t     *src,
                                         uint8_t     *src_mask,
                                         uint8_t     *dst,
                                         uint8_t     *dst_mask)
{
    bool_t      rc;
    int         idx;
    char        ipv6_str[DB_CLI_IPV6_STR_LEN]; 

    DBG_PRINT("SRC  %s", db_ipv6_to_str(src, ipv6_str));
    DBG_PRINT("SRC mask %s", db_ipv6_to_str(src_mask, ipv6_str));
    DBG_PRINT("DST  %s", db_ipv6_to_str(dst, ipv6_str));
    DBG_PRINT("DST mask %s", db_ipv6_to_str(dst_mask, ipv6_str));

    /* Looking for the specified name */
    if ((rc = db_cli_looking_for_a_ipv6_key_name(name, &idx)) == BOOL_TRUE) 
    {
        /* Add IP addresses to DB */
        memcpy(&(tpm_cli_ipv6_key_table[idx].key.ipv6_src_ip_add), src, DB_CLI_IPV6_ADDR_LEN);
        memcpy(&(tpm_cli_ipv6_key_table[idx].key.ipv6_src_ip_add_mask), src_mask, DB_CLI_IPV6_ADDR_LEN);
        memcpy(&(tpm_cli_ipv6_key_table[idx].key.ipv6_dst_ip_add), dst, DB_CLI_IPV6_ADDR_LEN);
        memcpy(&(tpm_cli_ipv6_key_table[idx].key.ipv6_dst_ip_add_mask), dst_mask, DB_CLI_IPV6_ADDR_LEN);
    }

    return rc;
}



/********************************************************************************/
/*                           CLI print functions                                */
/********************************************************************************/
void    db_cli_print_tbl_size(char      *tbl_name,
                              uint32_t  curr_num)
{
    PRINT("=================");
    PRINT("%s DB table", tbl_name);
    PRINT("=================");
    PRINT("MAX entries number     : %d", DB_CLI_MAX_ENTRIES);
    PRINT("Current entries number : %d", curr_num);
}


static void    db_cli_print_vlan_data (tpm_vlan_key_t     *vlan)
{
    PRINT("TPID                    0x%4.4x", vlan->tpid);
    PRINT("VID                     %d", vlan->vid);
    PRINT("VID mask                0x%4.4x", vlan->vid_mask);
    PRINT("CFI                     0x%2.2x", vlan->cfi);
    PRINT("CFI mask                0x%2.2x", vlan->cfi_mask);
    PRINT("PBIT                    0x%2.2x", vlan->pbit);
    PRINT("PBIT mask               0x%2.2x", vlan->pbit_mask);
}

static void db_cli_print_vlan_table_header (void)
{
    PRINT("====================================================================");
    PRINT("        Name         TPID  VID  VID mask CFI CFI mask PBIT PBIT mask");
    PRINT("====================================================================");
}

static void db_cli_print_vlan_entry (const char     *name,
                                     tpm_vlan_key_t *entry)
{
    PRINT("%20s %4.4x  %4d   %4.4x    %x    %2.2x      %x     %2.2x",
          name, entry->tpid, entry->vid, entry->vid_mask,
          entry->cfi, entry->cfi_mask, entry->pbit, entry->pbit_mask);
}

static void db_cli_print_vlan_params_header (void)
{
    PRINT("=================================================");
    PRINT("  TPID  VID  VID mask CFI CFI mask PBIT PBIT mask");
    PRINT("=================================================");
}

static void db_cli_print_vlan_params (tpm_vlan_key_t *entry)
{
    PRINT("  %4.4x  %4d   %4.4x    %1.1x   %2.2x      %2.2x     %2.2x",
          entry->tpid, entry->vid, entry->vid_mask,
          entry->cfi, entry->cfi_mask, entry->pbit, entry->pbit_mask);
}


void    db_cli_print_vlan_table(void)
{
    int     i, j;

    db_cli_print_tbl_size("VLAN", number_of_vlan_entries);
    db_cli_print_vlan_table_header();

    for (i=0,j=0; (i<DB_CLI_MAX_ENTRIES) && (j<number_of_vlan_entries); i++) 
    {
        if (tpm_cli_vlan_table[i].name[0] != '\0') 
        {
            j++;
            db_cli_print_vlan_entry(tpm_cli_vlan_table[i].name,
                                    &tpm_cli_vlan_table[i].vlan);
        }
    }
}

bool_t    db_cli_print_vlan_table_entry(const char    *name)
{
    int             idx;
    tpm_vlan_key_t  entry;
    bool_t          rc;

    if ((rc = db_cli_get_vlan_tbl_entry(name, &entry)) == BOOL_TRUE)
    {
        db_cli_print_vlan_table_header();
        db_cli_print_vlan_entry(name, &entry);
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}


static void db_cli_print_l2key_table_header (void)
{
    PRINT("=======================================================================================");
    PRINT("        Name           ETY  GEM       PPPoE               SA               DA          ");
    PRINT("                                 session   proto        SA mask          DA mask       ");
    PRINT("=======================================================================================");
}

static void db_cli_print_l2key_entry (const char      *name,
                                     tpm_l2_acl_key_t *entry)
{
    char        smac_str[DB_CLI_MAC_STR_LEN]; 
    char        dmac_str[DB_CLI_MAC_STR_LEN]; 

    PRINT("%20s  %4.4x  %4.4x %8.8x  %4.4x    %s  %s",
          name, entry->ether_type, entry->gem_port,
          entry->pppoe_hdr.ppp_session, entry->pppoe_hdr.ppp_proto,
          db_mac_to_str(entry->mac.mac_sa, smac_str),
          db_mac_to_str(entry->mac.mac_da, dmac_str));
    PRINT("                                                   %s  %s",
          db_mac_to_str(entry->mac.mac_sa_mask, smac_str),
          db_mac_to_str(entry->mac.mac_da_mask, dmac_str));
}


void    db_cli_print_l2key_table(void)
{
    int     i, j;

    db_cli_print_tbl_size("L2 key", number_of_l2_key_entries);
    PRINT("NOTE !!!!!!!!!!!!!!");
    PRINT("Only partial data is shown");

    db_cli_print_l2key_table_header();

    for (i=0,j=0; (i<DB_CLI_MAX_ENTRIES) && (j<number_of_l2_key_entries); i++) 
    {
        if (tpm_cli_l2_key_table[i].name[0] != '\0') 
        {
            j++;
            db_cli_print_l2key_entry(tpm_cli_l2_key_table[i].name,
                                     &tpm_cli_l2_key_table[i].acl);
        }
    }
}

bool_t    db_cli_print_l2key_table_entry(const char    *name)
{
    int                 idx;
    tpm_l2_acl_key_t    entry;
    bool_t              rc;

    if ((rc = db_cli_get_l2key(name, &entry)) == BOOL_TRUE)
    {
        db_cli_print_l2key_table_header();
        db_cli_print_l2key_entry(name, &entry);

        db_cli_print_vlan_params_header();
        db_cli_print_vlan_params(&entry.vlan1);
        db_cli_print_vlan_params(&entry.vlan2);
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}


bool_t    db_cli_print_l2key_table_mac(const char    *name)
{
    int                 idx;
    tpm_l2_acl_key_t    entry;
    char                smac_str[DB_CLI_MAC_STR_LEN] = {'\0'};
    char                dmac_str[DB_CLI_MAC_STR_LEN] = {'\0'};
    bool_t              rc;

    if ((rc = db_cli_get_l2key(name, &entry)) == BOOL_TRUE)
    {
        PRINT("==============================================================");
        PRINT("        Name                   SA                DA           ");
        PRINT("                             SA mask           DA mask        ");
        PRINT("==============================================================");

        PRINT("%20s  %s   %s",
              name, 
              db_mac_to_str(entry.mac.mac_sa, smac_str),
              db_mac_to_str(entry.mac.mac_da, dmac_str));
        PRINT("                      %s   %s",
              db_mac_to_str(entry.mac.mac_sa_mask, smac_str),
              db_mac_to_str(entry.mac.mac_da_mask, dmac_str));
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}

bool_t    db_cli_print_l2key_table_vlan(const char    *name)
{
    int                 idx;
    tpm_l2_acl_key_t    entry;
    bool_t              rc;

    if ((rc = db_cli_get_l2key(name, &entry)) == BOOL_TRUE)
    {
        db_cli_print_vlan_params_header();
        db_cli_print_vlan_params(&entry.vlan1);
        db_cli_print_vlan_params(&entry.vlan2);
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}

bool_t    db_cli_print_l2key_table_pppoe(const char    *name)
{
    int                 idx;
    tpm_l2_acl_key_t    entry;
    bool_t              rc;

    if ((rc = db_cli_get_l2key(name, &entry)) == BOOL_TRUE)
    {
        PRINT("========================================================");
        PRINT("        Name           PPPoE Session   PPPoE protocol   ");
        PRINT("========================================================");

        PRINT("%20s       %4.4x            %4.4x",
              name, entry.pppoe_hdr.ppp_session, entry.pppoe_hdr.ppp_proto);
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}


bool_t    db_cli_print_l2key_table_ety(const char    *name)
{
    int                 idx;
    tpm_l2_acl_key_t    entry;
    bool_t              rc;

    if ((rc = db_cli_get_l2key(name, &entry)) == BOOL_TRUE)
    {
        PRINT("=============================");
        PRINT("        Name           ETY   ");
        PRINT("=============================");

        PRINT("%20s   %4.4x ", name, entry.ether_type);
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}


bool_t    db_cli_print_l2key_table_gem(const char    *name)
{
    int                 idx;
    tpm_l2_acl_key_t    entry;
    bool_t              rc;

    if ((rc = db_cli_get_l2key(name, &entry)) == BOOL_TRUE)
    {
        PRINT("=============================");
        PRINT("        Name           GEM   ");
        PRINT("=============================");

        PRINT("%20s   %4d ", name, entry.gem_port);
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}

static void db_cli_print_vlan_mod_table_header (void)
{
    PRINT("===================================================================================");
    PRINT("        Name                  Operation          TPID  VID  VID  CFI CFI  PBIT PBIT  ");
    PRINT("                                                            mask     mask      mask  ");
    PRINT("===================================================================================");
}

static void db_cli_print_vlan_mod_entry (const char    *name,
                                         tpm_pkt_mod_t *mod)
{
    PRINT("%20s %26s  %4.4x %4d  %4.4x  %1.1x   %1.1x    %2.2x   %2.2x",
          name, 
          db_tpm_vlan_operation_str[mod->vlan_mod.vlan_op].enumString,
          mod->vlan_mod.vlan1_out.tpid, 
          mod->vlan_mod.vlan1_out.vid, 
          mod->vlan_mod.vlan1_out.vid_mask,
          mod->vlan_mod.vlan1_out.cfi, 
          mod->vlan_mod.vlan1_out.cfi_mask, 
          mod->vlan_mod.vlan1_out.pbit, 
          mod->vlan_mod.vlan1_out.pbit_mask);
    PRINT("                                                 %4.4x %4d  %4.4x  %1.1x   %1.1x    %2.2x   %2.2x",
          mod->vlan_mod.vlan2_out.tpid, 
          mod->vlan_mod.vlan2_out.vid, 
          mod->vlan_mod.vlan2_out.vid_mask,
          mod->vlan_mod.vlan2_out.cfi, 
          mod->vlan_mod.vlan2_out.cfi_mask, 
          mod->vlan_mod.vlan2_out.pbit, 
          mod->vlan_mod.vlan2_out.pbit_mask);
}

static void db_cli_print_mac_mod_table_header (void)
{
    PRINT("=============================================================================");
    PRINT("        Name                PPPoE               SA               DA          ");
    PRINT("                       session   proto        SA mask          DA mask       ");
    PRINT("=============================================================================");
}

static void db_cli_print_mac_mod_entry (const char      *name,
                                        tpm_pkt_mod_t   *mod)
{
    char        smac_str[DB_CLI_MAC_STR_LEN]; 
    char        dmac_str[DB_CLI_MAC_STR_LEN]; 

    PRINT("%20s  %8.8x   %4.4x    %s  %s",
          name, 
          mod->pppoe_mod.ppp_session, mod->pppoe_mod.ppp_proto,
          db_mac_to_str(mod->mac_mod.mac_sa, smac_str),
          db_mac_to_str(mod->mac_mod.mac_da, dmac_str));
    PRINT("                                         %s  %s",
          db_mac_to_str(mod->mac_mod.mac_sa_mask, smac_str),
          db_mac_to_str(mod->mac_mod.mac_da_mask, dmac_str));
}

static void db_cli_print_ipv4_mod_table_header (void)
{
    PRINT("====================================================================================");
    PRINT("        Name           Proto    DSCP       Port        IP src           IP dst      ");
    PRINT("                             code  mask  src  dst      (mask)           (mask)      ");
    PRINT("====================================================================================");
}

static void db_cli_print_ipv4_mod_entry (const char      *name,
                                         tpm_pkt_mod_t   *mod)
{
    char        ipv4_src_str[DB_CLI_IPV4_STR_LEN]; 
    char        ipv4_dst_str[DB_CLI_IPV4_STR_LEN]; 

    PRINT("%20s    %3d   %2d   0x%2.2x  %3d  %3d %15s  %15s",
          name, mod->l3.ipv4_mod.ipv4_proto,
          mod->l3.ipv4_mod.ipv4_dscp, mod->l3.ipv4_mod.ipv4_dscp_mask,
          mod->l3.ipv4_mod.l4_src_port, mod->l3.ipv4_mod.l4_dst_port,
          db_ipv4_to_str(mod->l3.ipv4_mod.ipv4_src_ip_add, ipv4_src_str),
          db_ipv4_to_str(mod->l3.ipv4_mod.ipv4_dst_ip_add, ipv4_dst_str));
    PRINT("%48s  %15s  %15s", " ",
          db_ipv4_to_str(mod->l3.ipv4_mod.ipv4_src_ip_add_mask, ipv4_src_str),
          db_ipv4_to_str(mod->l3.ipv4_mod.ipv4_dst_ip_add_mask, ipv4_dst_str));
}

static void db_cli_print_ipv6_mod_table_header (void)
{
    PRINT("===========================================================================================");
    PRINT("        Name          Next     DSCP       Port                   IP src/mask               ");
    PRINT("                     header code  mask  src  dst                 IP dst/mask               ");
    PRINT("===========================================================================================");
}

static void db_cli_print_ipv6_mod_entry (const char      *name,
                                         tpm_pkt_mod_t   *mod)
{
    char        ipv6_str[DB_CLI_IPV6_STR_LEN]; 

    PRINT("%20s  %3d    %2d   0x%2.2x   %3d   %3d  %40s",
          name, mod->l3.ipv6_mod.ipv6_next_header,
          mod->l3.ipv6_mod.ipv6_dscp, mod->l3.ipv6_mod.ipv6_dscp_mask,
          mod->l3.ipv6_mod.l4_src_port, mod->l3.ipv6_mod.l4_dst_port,
          db_ipv6_to_str(mod->l3.ipv6_mod.ipv6_src_ip_add, ipv6_str));
    PRINT("%50s  %40s", " ", db_ipv6_to_str(mod->l3.ipv6_mod.ipv6_src_ip_add_mask, ipv6_str));
    PRINT("%50s  %40s", " ", db_ipv6_to_str(mod->l3.ipv6_mod.ipv6_dst_ip_add, ipv6_str));
    PRINT("%50s  %40s", " ", db_ipv6_to_str(mod->l3.ipv6_mod.ipv6_dst_ip_add_mask, ipv6_str));
}


void    db_cli_print_mod_table(void)
{
    int     i, j, h;

    db_cli_print_tbl_size("Modification", number_of_mod_entries);

    /* Print VLAN modification table */
    for (i=0,j=0,h=0; (i<DB_CLI_MAX_ENTRIES) && (j<number_of_mod_entries); i++) 
    {
        if ((tpm_cli_mod_table[i].name[0] != '\0') &&
            (tpm_cli_mod_table[i].flags & DB_CLI_VLAN_FLAG)) 
        {
            if (!h)
            {
                db_cli_print_vlan_mod_table_header();
                h++;
            }

            j++;
            db_cli_print_vlan_mod_entry (tpm_cli_mod_table[i].name,
                                         &tpm_cli_mod_table[i].mod);
        }
    }

    /* Print MAC & PPPoE modification table */
    for (i=0,j=0,h=0; (i<DB_CLI_MAX_ENTRIES) && (j<number_of_mod_entries); i++) 
    {
        if ((tpm_cli_mod_table[i].name[0] != '\0') &&
            ((tpm_cli_mod_table[i].flags & DB_CLI_MAC_FLAG) ||
             (tpm_cli_mod_table[i].flags & DB_CLI_PPPOE_FLAG)))
        {
            if (!h)
            {
                db_cli_print_mac_mod_table_header();
                h++;
            }

            j++;
            db_cli_print_mac_mod_entry (tpm_cli_mod_table[i].name,
                                        &tpm_cli_mod_table[i].mod);
        }
    }

    /* Print IPv4 modification table */
    for (i=0,j=0,h=0; (i<DB_CLI_MAX_ENTRIES) && (j<number_of_mod_entries); i++) 
    {
        if ((tpm_cli_mod_table[i].name[0] != '\0') &&
             ((tpm_cli_mod_table[i].flags & DB_CLI_IPV4_FLAG) ||
               (!(tpm_cli_mod_table[i].flags & DB_CLI_IPV6_FLAG) &&
                 ((tpm_cli_mod_table[i].flags & DB_CLI_DSCP_FLAG) ||
                  (tpm_cli_mod_table[i].flags & DB_CLI_IP_PROTO_FLAG)
                 )
               )
             )
           ) 
        {
            if (!h)
            {
                db_cli_print_ipv4_mod_table_header();
                h++;
            }

            j++;
            db_cli_print_ipv4_mod_entry (tpm_cli_mod_table[i].name,
                                         &tpm_cli_mod_table[i].mod);
        }
    }

    /* Print IPv6 modification table */
    for (i=0,j=0,h=0; (i<DB_CLI_MAX_ENTRIES) && (j<number_of_mod_entries); i++) 
    {
        if ((tpm_cli_mod_table[i].name[0] != '\0') &&
            (tpm_cli_mod_table[i].flags & DB_CLI_IPV6_FLAG)) 
        {
            if (!h)
            {
                db_cli_print_ipv6_mod_table_header();
                h++;
            }

            j++;
            db_cli_print_ipv6_mod_entry (tpm_cli_mod_table[i].name,
                                         &tpm_cli_mod_table[i].mod);
        }
    }

}

bool_t    db_cli_print_mod_table_entry(const char    *name)
{
    int     i;
    bool_t  rc;

    if ((rc = db_cli_match_name_to_index_mod_tbl(name, &i)) == BOOL_TRUE)
    {
        if (tpm_cli_mod_table[i].flags & DB_CLI_VLAN_FLAG) 
        {
            PRINT("=================================================");
            PRINT("VLAN operation - %s",
                  db_tpm_vlan_operation_str[tpm_cli_mod_table[i].mod.vlan_mod.vlan_op].enumString);

            db_cli_print_vlan_params_header();
            db_cli_print_vlan_params(&tpm_cli_mod_table[i].mod.vlan_mod.vlan1_out);
            db_cli_print_vlan_params(&tpm_cli_mod_table[i].mod.vlan_mod.vlan2_out);
        }

        if ((tpm_cli_mod_table[i].flags & DB_CLI_MAC_FLAG) ||
            (tpm_cli_mod_table[i].flags & DB_CLI_PPPOE_FLAG))
        {
            db_cli_print_mac_mod_table_header();
            db_cli_print_mac_mod_entry (name, &tpm_cli_mod_table[i].mod);
        }

        if ((tpm_cli_mod_table[i].flags & DB_CLI_IPV4_FLAG) ||
             (!(tpm_cli_mod_table[i].flags & DB_CLI_IPV6_FLAG) &&
               ((tpm_cli_mod_table[i].flags & DB_CLI_DSCP_FLAG) ||
                (tpm_cli_mod_table[i].flags & DB_CLI_IP_PROTO_FLAG)
               )
             )
           ) 
        {
            db_cli_print_ipv4_mod_table_header();
            db_cli_print_ipv4_mod_entry (name, &tpm_cli_mod_table[i].mod);
        }

        if (tpm_cli_mod_table[i].flags & DB_CLI_IPV6_FLAG)
        {
            db_cli_print_ipv6_mod_table_header();
            db_cli_print_ipv6_mod_entry (name, &tpm_cli_mod_table[i].mod);
        }
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}

static void db_cli_print_frwd_table_header (void)
{
    PRINT("===========================================");
    PRINT("        Name           Port  Queue   Gem   ");
    PRINT("===========================================");
}

static void db_cli_print_frwd_entry (const char       *name,
                                     tpm_pkt_frwd_t   *frwd)
{
    PRINT("%20s  0x%4.4x   %1d     %4d",
          name, frwd->trg_port, frwd->trg_queue, frwd->gem_port);
}


bool_t    db_cli_print_frwd_table_entry(const char    *name)
{
    int                 idx;
    tpm_pkt_frwd_t      frwd;
    bool_t              rc;

    if ((rc = db_cli_get_pkt_frwd(name, &frwd)) == BOOL_TRUE)
    {
        db_cli_print_frwd_table_header();
        db_cli_print_frwd_entry(name, &frwd);
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}

void    db_cli_print_frwd_table(void)
{
    int     i, j;

    db_cli_print_tbl_size("Forwarding", number_of_frwd_entries);
    db_cli_print_frwd_table_header();

    for (i=0,j=0; (i<DB_CLI_MAX_ENTRIES) && (j<number_of_frwd_entries); i++) 
    {
        if (tpm_cli_frwd_table[i].name[0] != '\0') 
        {
            j++;
            db_cli_print_frwd_entry (tpm_cli_frwd_table[i].name,
                                     &tpm_cli_frwd_table[i].frwd);
        }
    }
}

static void db_cli_print_l3key_table_header (void)
{
    PRINT("==============================================");
    PRINT("        Name           ETY        PPPoE       ");
    PRINT("                             session   proto  ");
    PRINT("==============================================");
}

static void db_cli_print_l3key_entry (const char       *name,
                                     tpm_l3_type_key_t *entry)
{
    PRINT("%20s   %4.4x  %8.8x  %4.4x",
          name, entry->ether_type_key,
          entry->pppoe_key.ppp_session, 
          entry->pppoe_key.ppp_proto);
}

void    db_cli_print_l3key_table(void)
{
    int     i, j;

    db_cli_print_tbl_size("L3 key", number_of_l3_key_entries);
    db_cli_print_l3key_table_header();

    for (i=0,j=0; (i<DB_CLI_MAX_ENTRIES) && (j<number_of_l3_key_entries); i++) 
    {
        if (tpm_cli_l3_key_table[i].name[0] != '\0') 
        {
            j++;
            db_cli_print_l3key_entry(tpm_cli_l3_key_table[i].name,
                                     &tpm_cli_l3_key_table[i].key);
        }
    }
}

bool_t    db_cli_print_l3key_table_entry(const char    *name)
{
    int                 idx;
    tpm_l3_type_key_t   entry;
    bool_t              rc;

    if ((rc = db_cli_get_l3key(name, &entry)) == BOOL_TRUE)
    {
        db_cli_print_l3key_table_header();
        db_cli_print_l3key_entry(name, &entry);
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}

static void db_cli_print_ipv4_key_table_header (void)
{
    PRINT("====================================================================================");
    PRINT("        Name           Proto    DSCP       Port        IP src           IP dst      ");
    PRINT("                             code  mask  src  dst      (mask)           (mask)      ");
    PRINT("====================================================================================");
}

static void db_cli_print_ipv4_key_entry (const char         *name,
                                         tpm_ipv4_acl_key_t *entry)
{
    char        ipv4_src_str[DB_CLI_IPV4_STR_LEN]; 
    char        ipv4_dst_str[DB_CLI_IPV4_STR_LEN]; 

    PRINT("%20s    %3d   %2d   0x%2.2x  %2d   %2d  %15s  %15s",
          name, entry->ipv4_proto,
          entry->ipv4_dscp, entry->ipv4_dscp_mask,
          entry->l4_src_port, entry->l4_dst_port,
          db_ipv4_to_str(entry->ipv4_src_ip_add, ipv4_src_str),
          db_ipv4_to_str(entry->ipv4_dst_ip_add, ipv4_dst_str));
    PRINT("%48s  %15s  %15s", " ",
          db_ipv4_to_str(entry->ipv4_src_ip_add_mask, ipv4_src_str),
          db_ipv4_to_str(entry->ipv4_dst_ip_add_mask, ipv4_dst_str));
}

void    db_cli_print_ipv4_key_table(void)
{
    int     i, j;

    db_cli_print_tbl_size("IPv4 key", number_of_ipv4_key_entries);
    db_cli_print_ipv4_key_table_header();

    for (i=0,j=0; (i<DB_CLI_MAX_ENTRIES) && (j<number_of_ipv4_key_entries); i++) 
    {
        if (tpm_cli_ipv4_key_table[i].name[0] != '\0') 
        {
            j++;
            db_cli_print_ipv4_key_entry(tpm_cli_ipv4_key_table[i].name,
                                        &tpm_cli_ipv4_key_table[i].key);
        }
    }
}

bool_t    db_cli_print_ipv4_key_table_entry(const char    *name)
{
    int                 idx;
    tpm_ipv4_acl_key_t  entry;
    bool_t              rc;

    if ((rc = db_cli_get_ipv4_key(name, &entry)) == BOOL_TRUE)
    {
        db_cli_print_ipv4_key_table_header();
        db_cli_print_ipv4_key_entry(name, &entry);
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}

static void db_cli_print_ipv6_key_table_header (void)
{
    PRINT("===========================================================================================");
    PRINT("        Name          Next     DSCP       Port                   IP src/mask               ");
    PRINT("                     header code  mask  src  dst                 IP dst/mask               ");
    PRINT("===========================================================================================");
}

static void db_cli_print_ipv6_key_entry (const char         *name,
                                         tpm_ipv6_acl_key_t *entry)
{
    char        ipv6_str[DB_CLI_IPV6_STR_LEN]; 

    PRINT("%20s  %3d    %2d   0x%2.2x   %2d   %2d   %40s",
          name, entry->ipv6_next_header,
          entry->ipv6_dscp, entry->ipv6_dscp_mask,
          entry->l4_src_port, entry->l4_dst_port,
          db_ipv6_to_str(entry->ipv6_src_ip_add, ipv6_str));
    PRINT("%49s  %40s", " ", db_ipv6_to_str(entry->ipv6_src_ip_add_mask, ipv6_str));
    PRINT("%49s  %40s", " ", db_ipv6_to_str(entry->ipv6_dst_ip_add, ipv6_str));
    PRINT("%49s  %40s", " ", db_ipv6_to_str(entry->ipv6_dst_ip_add_mask, ipv6_str));
}

void    db_cli_print_ipv6_key_table(void)
{
    int     i, j;

    db_cli_print_tbl_size("IPv6 key", number_of_ipv6_key_entries);
    db_cli_print_ipv6_key_table_header();

    for (i=0,j=0; (i<DB_CLI_MAX_ENTRIES) && (j<number_of_ipv6_key_entries); i++) 
    {
        if (tpm_cli_ipv6_key_table[i].name[0] != '\0') 
        {
            j++;
            db_cli_print_ipv6_key_entry(tpm_cli_ipv6_key_table[i].name,
                                        &tpm_cli_ipv6_key_table[i].key);
        }
    }
}

bool_t    db_cli_print_ipv6_key_table_entry(const char    *name)
{
    int                 idx;
    tpm_ipv6_acl_key_t  entry;
    bool_t              rc;

    if ((rc = db_cli_get_ipv6_key(name, &entry)) == BOOL_TRUE)
    {
        db_cli_print_ipv6_key_table_header();
        db_cli_print_ipv6_key_entry(name, &entry);
    }
    else
        PRINT("Invalid entry name - %s", name);

    return rc;
}


void db_cli_print_rule_entry(tpm_rule_entry_t  *entry, uint32_t rule_idx)
{
    int         src_port, stage, oper;
    char        mac_str[DB_CLI_MAC_STR_LEN]; 
    char        mac_mask_str[DB_CLI_MAC_STR_LEN]; 
    char        ipv4_str[DB_CLI_IPV4_STR_LEN]; 
    char        ipv6_str[DB_CLI_IPV6_STR_LEN]; 

    PRINT("============");
    PRINT("L2 key:");
    PRINT("============");

    src_port = entry->l2_prim_key.src_port;
    if (src_port > TPM_SRC_PORT_UNI_ANY)
        src_port = TPM_SRC_PORT_ILLEGAL;

    PRINT("Rule Idx                %d", rule_idx);
    PRINT("Src port                %s", db_tpm_src_port_str[src_port].enumString);
    PRINT("Parse rule bitmap       0x%8.8x", entry->l2_prim_key.parse_rule_bm);
    PRINT("Parse flag bitmap       0x%8.8x", entry->l2_prim_key.parse_flags_bm);   
    PRINT("DMAC                    %s", db_mac_to_str(entry->l2_prim_key.l2_key.mac.mac_da, mac_str));
    PRINT("DMAC mask               %s", db_mac_to_str(entry->l2_prim_key.l2_key.mac.mac_da_mask, mac_mask_str));
    PRINT("SMAC                    %s", db_mac_to_str(entry->l2_prim_key.l2_key.mac.mac_sa, mac_str));
    PRINT("SMAC mask               %s", db_mac_to_str(entry->l2_prim_key.l2_key.mac.mac_sa_mask, mac_mask_str));
    PRINT("GEM port                0x%4.4x", entry->l2_prim_key.l2_key.gem_port);
    PRINT("ETY                     0x%4.4x", entry->l2_prim_key.l2_key.ether_type);
    PRINT("VLAN1:");
    db_cli_print_vlan_data(&(entry->l2_prim_key.l2_key.vlan1));
    PRINT("VLAN2:");
    db_cli_print_vlan_data(&(entry->l2_prim_key.l2_key.vlan2));
    PRINT("PPPoE:");
    PRINT("Session                 0x%4.4x", entry->l2_prim_key.l2_key.pppoe_hdr.ppp_session);
    PRINT("Protocol                0x%4.4x", entry->l2_prim_key.l2_key.pppoe_hdr.ppp_proto);
    PRINT("============");
    PRINT("L3 key:");
    PRINT("============");
    
    src_port = entry->l3_type_key.src_port;
    if (src_port > TPM_SRC_PORT_UNI_ANY)
        src_port = TPM_SRC_PORT_ILLEGAL;   
        
    PRINT("Src port                %s", db_tpm_src_port_str[src_port].enumString);  
    PRINT("Parse rule bitmap       0x%8.8x", entry->l3_type_key.parse_rule_bm);
    PRINT("Parse flags bitmap      0x%8.8x", entry->l3_type_key.parse_flags_bm);
    PRINT("ETY                     0x%4.4x", entry->l3_type_key.l3_key.ether_type_key);
    PRINT("PPPoE:");
    PRINT("Session                 0x%4.4x", entry->l3_type_key.l3_key.pppoe_key.ppp_session);
    PRINT("Protocol                0x%4.4x", entry->l3_type_key.l3_key.pppoe_key.ppp_proto);
    PRINT("Action drop             0x%8.8x", entry->l3_type_key.rule_action.pkt_act);

    stage = entry->l3_type_key.rule_action.next_phase;
    if (stage > STAGE_DONE)
        PRINT("Next parsing stage      INVALID");
    else 
        PRINT("Next parsing stage      %s", db_tpm_parse_stage_str[stage].enumString);

    PRINT("============");
    PRINT("IPv4 key:");
    PRINT("============");
    
    src_port = entry->ipv4_key.src_port;
    if (src_port > TPM_SRC_PORT_UNI_ANY)
        src_port = TPM_SRC_PORT_ILLEGAL;  
        
    PRINT("Src port                %s", db_tpm_src_port_str[src_port].enumString);      
    PRINT("Parse rule bitmap       0x%8.8x", entry->ipv4_key.parse_rule_bm);
    PRINT("Parse flags bitmap      0x%8.8x", entry->ipv4_key.parse_flags_bm);

    PRINT("ACL:");
    PRINT("DSCP                    0x%2.2x", entry->ipv4_key.ipv4_key.ipv4_dscp);
    PRINT("DSCP mask               0x%2.2x", entry->ipv4_key.ipv4_key.ipv4_dscp_mask);
    PRINT("Protocol                %d", entry->ipv4_key.ipv4_key.ipv4_proto);
    PRINT("SIP                     %s", db_ipv4_to_str(entry->ipv4_key.ipv4_key.ipv4_src_ip_add, ipv4_str));
    PRINT("SIP mask                %s", db_ipv4_to_str(entry->ipv4_key.ipv4_key.ipv4_src_ip_add_mask, ipv4_str));
    PRINT("DIP                     %s", db_ipv4_to_str(entry->ipv4_key.ipv4_key.ipv4_dst_ip_add, ipv4_str));
    PRINT("DIP mask                %s", db_ipv4_to_str(entry->ipv4_key.ipv4_key.ipv4_dst_ip_add_mask, ipv4_str));
    PRINT("Src port                %d", entry->ipv4_key.ipv4_key.l4_src_port);
    PRINT("Dst port                %d", entry->ipv4_key.ipv4_key.l4_dst_port);
    PRINT("Packet data:");
    PRINT("Target port             0x%4.4x", entry->ipv4_key.pkt_frwd.trg_port);
    PRINT("GEM port                0x%4.4x", entry->ipv4_key.pkt_frwd.gem_port);
    PRINT("Target queue            %d", entry->ipv4_key.pkt_frwd.trg_queue);
    PRINT("Next Phase              0x%8.8x", entry->ipv4_key.rule_action.next_phase);
    PRINT("Action                  0x%8.8x", entry->ipv4_key.rule_action.pkt_act);
    PRINT("Packet modification data:");

    oper = entry->ipv4_key.pkt_mod.vlan_mod.vlan_op;
    if (oper > VLANOP_ILLEGAL)
        oper = VLANOP_ILLEGAL;

    PRINT("Operation               %s", db_tpm_vlan_operation_str[oper].enumString);
    PRINT("VLAN1:");
    db_cli_print_vlan_data(&(entry->ipv4_key.pkt_mod.vlan_mod.vlan1_out));
    PRINT("VLAN2:");
    db_cli_print_vlan_data(&(entry->ipv4_key.pkt_mod.vlan_mod.vlan2_out));
    PRINT("============");
    PRINT("IPv4 MC key:");
    PRINT("============");
    PRINT("Target port             0x%4.4x", entry->ipv4_mc_key.dest_port_bm);
    PRINT("SIP                     %s", db_ipv4_to_str(entry->ipv4_mc_key.ipv4_src_add, ipv4_str));
    PRINT("DIP                     %s", db_ipv4_to_str(entry->ipv4_mc_key.ipv4_dest_add, ipv4_str));
    PRINT("Ignore IPv4 src         %d", entry->ipv4_mc_key.ignore_ipv4_src);
    PRINT("============");
    PRINT("IPv6 key:");
    PRINT("============");
    PRINT("Parse rule bitmap       0x%8.8x", entry->ipv6_key.parse_rule_bm);
    PRINT("Parse flags bitmap      0x%8.8x", entry->ipv6_key.parse_flags_bm);

    src_port = entry->ipv6_key.src_port;
    if (src_port > TPM_SRC_PORT_UNI_ANY)
        src_port = TPM_SRC_PORT_ILLEGAL;

    PRINT("Src port                %s", db_tpm_src_port_str[src_port].enumString);

    PRINT("ACL:");
    PRINT("DSCP                    0x%2.2x", entry->ipv6_key.ipv6_key.ipv6_dscp);
    PRINT("DSCP mask               0x%2.2x", entry->ipv6_key.ipv6_key.ipv6_dscp_mask);
    PRINT("SIP                     %s", db_ipv6_to_str(entry->ipv6_key.ipv6_key.ipv6_src_ip_add, ipv6_str));
    PRINT("SIP mask                %s", db_ipv6_to_str(entry->ipv6_key.ipv6_key.ipv6_src_ip_add_mask, ipv6_str));
    PRINT("DIP                     %s", db_ipv6_to_str(entry->ipv6_key.ipv6_key.ipv6_dst_ip_add, ipv6_str));
    PRINT("DIP mask                %s", db_ipv6_to_str(entry->ipv6_key.ipv6_key.ipv6_dst_ip_add_mask, ipv6_str));
    PRINT("Src port                %d", entry->ipv6_key.ipv6_key.l4_src_port);
    PRINT("Dst port                %d", entry->ipv6_key.ipv6_key.l4_dst_port);
    PRINT("Packet data:");
    PRINT("Target port             0x%4.4x", entry->ipv6_key.pkt_frwd.trg_port);
    PRINT("GEM port                0x%4.4x", entry->ipv6_key.pkt_frwd.gem_port);
    PRINT("Target queue            %d", entry->ipv6_key.pkt_frwd.trg_queue);
    PRINT("Action                  0x%8.8x", entry->ipv6_key.rule_action.pkt_act);
    PRINT("Next Phase              0x%8.8x", entry->ipv6_key.rule_action.next_phase);
    PRINT("Packet modification data:");

    oper = entry->ipv6_key.pkt_mod.vlan_mod.vlan_op;
    if (oper > VLANOP_ILLEGAL)
        oper = VLANOP_ILLEGAL;

    PRINT("Operation               %s", db_tpm_vlan_operation_str[oper].enumString);
    PRINT("VLAN1:");
    db_cli_print_vlan_data(&(entry->ipv6_key.pkt_mod.vlan_mod.vlan1_out));
    PRINT("VLAN2:");
    db_cli_print_vlan_data(&(entry->ipv6_key.pkt_mod.vlan_mod.vlan2_out));
/*    PRINT("============");
    PRINT("IPv6 MC key:");
    PRINT("============");
    PRINT("Target port             0x%4.4x", entry->ipv6_mc_key.dest_port_bm);
    PRINT("SIP                     %s", db_ipv6_to_str(entry->ipv6_mc_key.ipv6_src_add, ipv6_str));
    PRINT("DIP                     %s", db_ipv6_to_str(entry->ipv6_mc_key.ipv6_dest_add, ipv6_str));
    PRINT("VID                     %d", entry->ipv6_mc_key.vid);
    PRINT("Ignore IPv4 src         %d", entry->ipv6_mc_key.ignore_ipv6_src);
*/
}

/********************************************************************************/
/*                                Print Utils                                   */
/********************************************************************************/

char*   db_mac_to_str (uint8_t*  addr, char*   str)
{
    if ((str != NULL) && (addr != NULL))
    {
        str[0] = '\0';
        sprintf(str, "%02x:%02x:%02x:%02x:%02x:%02x",
                addr[0], addr[1], addr[2], addr[3], addr[4], addr[5]);
        return str;
    }

    return NULL;
}

char*   db_ipv4_to_str (uint8_t*  ipaddr, char*   str)
{
    if ((str != NULL) && (ipaddr != NULL))
    {
        str[0] = '\0';
        sprintf(str, "%u.%u.%u.%u", ipaddr[0], ipaddr[1], ipaddr[2], ipaddr[3]);
        return str;
    }

    return NULL;
}

char*    db_ipv6_to_str (uint8_t*  ipaddr, char*   str)
{
    int         i, j;
    uint16_t    addr[IPV6_ADDR_LEN/2];

    if ((str != NULL) && (ipaddr != NULL))
    {
        for (i=0,j=0; i<IPV6_ADDR_LEN; j++)
        {
            addr[j] = (ipaddr[i] << 8) | ipaddr[i+1];
            i += 2;
        }

        str[0] = '\0';
        sprintf(str,
                "%x:%x:%x:%x:%x:%x:%x:%x",
                addr[0], addr[1], addr[2], addr[3],
                addr[4], addr[5], addr[6], addr[7]);
        return str;
    }

    return NULL;
}
