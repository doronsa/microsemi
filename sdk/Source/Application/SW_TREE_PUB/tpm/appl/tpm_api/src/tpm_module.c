/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/********************************************************************************
* tpm_mod.c
*
* DESCRIPTION:
*
* DEPENDENCIES:
*               None
*
* CREATED BY:   Orenbh
*
* DATE CREATED:
*
* FILE REVISION NUMBER:
*               $Revision: 1.26 $
*
*
*******************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <fcntl.h>


#include "globals.h"
#include "errorCode.h"
#include "OsGlueLayer.h"

#include "tpm_types.h"
#include "tpm_internal_types.h"
#include "tpm_mng_if.h"

extern GL_SEMAPHORE_ID tpmApiSemId;

static uint32_t  dummy_ruleidx=0;

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/********************************** Packet Processor APIs *********************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

/******************************************************************************/
/********************************** Administrative APIs ***********************/
/******************************************************************************/

/*******************************************************************************
* tpm_module_init()
*
* DESCRIPTION:      init tpm kernel module
*
* INPUTS:
*
* OUTPUTS:
* owner_id           - ID of an application group.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* This function is used for error prevention and not as a security mechanism.
*
*******************************************************************************/
tpm_error_code_t tpm_module_init (tpm_init_t *tpm_init)
{
    int        fd;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_INIT_SECTION, tpm_init) < 0)
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_module_setup()
*
* DESCRIPTION:      request to start tpm kernel module initialization
*
* INPUTS:
*           wanTech  - PON product type (EPON, GPON, P2P)
*           xml_file - name of the XML configuration file
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* This function is used for error prevention and not as a security mechanism.
*
*******************************************************************************/
tpm_error_code_t tpm_module_setup (const char* xml_file)
{
    int         fd;
    tpm_setup_t tpm_init;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    if (NULL != xml_file)
    {
        strcpy(tpm_init.xml_file_path, xml_file);
    }
    else
    {
        memset(tpm_init.xml_file_path, '\0', XML_FILE_PATH_LENGTH);
    }

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SETUP_SECTION, &tpm_init) < 0)
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_create_ownerid()
*
* DESCRIPTION:      Creates an ownerId per an application group.
*
* INPUTS:
*
* OUTPUTS:
* owner_id           - ID of an application group.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* This function is used for error prevention and not as a security mechanism.
*
*******************************************************************************/
tpm_error_code_t tpm_create_ownerid (uint32_t *owner_id)
{
    int               fd;
    tpm_ioctl_admin_t tpm_admin;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_admin.amdin_cmd = MV_TPM_IOCTL_CRT_OWNER;

    if (ioctl(fd, MV_TPM_IOCTL_ADMIN_SECTION, &tpm_admin) < 0)
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        return ERR_GENERAL;
    }

    *owner_id = tpm_admin.owner_id;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_request_api_ownership()
*
* DESCRIPTION:      Establishes an ownership between owner_id and a group of APIs.
*
* INPUTS:
* owner_id           - ID of an application which requests ownership on a group of APIs.
* api_type           - the API group whom ownership is requested.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns API_OWNERSHIP_SUCCESS. On error, see tpm_api_ownership_error_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_api_ownership_error_t   tpm_request_api_ownership  (uint32_t               owner_id,
                                                        tpm_api_type_t         api_type)
{
    int               fd;
    tpm_ioctl_admin_t tpm_admin;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_admin.owner_id  = owner_id;
    tpm_admin.api_type  = api_type;
    tpm_admin.amdin_cmd = MV_TPM_IOCTL_REQ_API_G_OWNER;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADMIN_SECTION, &tpm_admin) < 0)
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return tpm_admin.api_ownership_error;
}

/*******************************************************************************
* tpm_erase_section()
*
* DESCRIPTION:      Erases a section per an application group.
*
* INPUTS:
* owner_id          - API owner id  should be used for all API calls.
* api_type          - the API PnC section to be deleted.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_erase_section (uint32_t             owner_id,
                                    tpm_api_type_t api_type)
{
    int               fd;
    tpm_ioctl_admin_t tpm_admin;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_admin.owner_id  = owner_id;
    tpm_admin.api_type  = api_type;
    tpm_admin.amdin_cmd = MV_TPM_IOCTL_DEL_SECTION;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADMIN_SECTION, &tpm_admin) < 0)
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_get_section_free_size()
*
* DESCRIPTION:      Returns the free size of an application group.
*
* INPUTS:
* api_type          - the API type to retrieve section size for.
*
* OUTPUTS:
* cur_size          - number of free entries for the API type.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_get_section_free_size (tpm_api_type_t api_type,
                                            int32_t             *cur_size)
{
    int               fd;
    tpm_ioctl_admin_t tpm_admin;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_admin.api_type   = api_type;
    tpm_admin.amdin_cmd   = MV_TPM_IOCTL_GET_SECTION_SIZE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADMIN_SECTION, &tpm_admin) < 0)
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *cur_size = tpm_admin.section_size;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_get_api_ownership()
*
* DESCRIPTION:      Retrieves OAM EPON management protocol channel information.
*
* INPUTS:
* api_type          - Specifies the API group whom owner_id is requested.
*
* OUTPUTS:
* owner_id           - specifies the ownerId of the application group
*
* RETURNS:
* On success, the function returns API_OWNERSHIP_SUCCESS.
* On error:     APIG_GROUP_UNKNOWN  when illegal API group
*               APIG_OWNERSHIP_ERROR  other errors.
*
* COMMENTS:
*
*******************************************************************************/
tpm_api_ownership_error_t   tpm_get_api_ownership (uint32_t               *owner_id,
                                                        tpm_api_type_t    api_type)
{
    printf("\n  <<tpm_oam_epon_get_channel>> NOT implemented. \n");
    owner_id = 0;
    return API_OWNERSHIP_SUCCESS;
}

/******************************************************************************/
/********************************** Data Forwarding APIs **********************/
/******************************************************************************/

/*******************************************************************************
* tpm_add_l2_rule()
*
* DESCRIPTION:      Creates a new L2 processing ACL. Supports Table mode and ACL mode.
*
* INPUTS:
* owner_id           - API owner id  should be used for all API calls.
* src_port           - The packet originating source port. The parameter determines if the packet is arriving
*                      from the WAN port, a specific LAN port or ANY LAN port.
* rule_num           - Entry index to be added in the current ACL
* parse_rule_bm      - Bitmap containing the significant flags for parsing fields of the packet.
*                      possible values for L2 API:
*                        TPM_L2_PARSE_GEMPORT|
*                        TPM_L2_PARSE_MAC_DA|TPM_L2_PARSE_MAC_SA|
*                        TPM_L2_PARSE_ONE_VLAN_TAG|TPM_L2_PARSE_TWO_VLAN_TAG|
*                        TPM_L2_PARSE_ETYPE| (Note: ETYPE recommended in tpm_add_l3_type_rule)
* parse_flags_bm     - Bitmap containing the significant flags result of the primary ACL filtering.
*                      possible values for L2 API:
*                        TPM_PARSE_FLAG_TAG1_TRUE|TPM_PARSE_FLAG_TAG1_FLASE|
*                        TPM_PARSE_FLAG_TAG2_TRUE|TPM_PARSE_FLAG_TAG2_FALSE
* l2_key             - Information to create a parsing key for the rule.
*                      Some pointers may be NULL depending on the parse_rule_bm.
* pkt_frwd           - Information for packet forwarding decision.
* pkt_mod            - Packet modification information.
* pkt_mod_bm         - Bitmap contains the set of packet fields to modify.
* rule_action        - Action associated to the rule = drop/set target/set packet modification/to CPU
*                      possible "next_phase" for L2 API   -> STAGE_L3_TYPE , STAGE_DONE
*
* OUTPUTS:
*  rule_idx         - If this API is in Table mode, this will equal to the rule_num,
*                     otherwise it will be a unique number acrosss all API's.
*
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t tpm_add_l2_rule (uint32_t            owner_id,
                                  tpm_src_port_type_t src_port,
                                  uint32_t            rule_num,
                                  uint32_t           *rule_idx,
                                  tpm_parse_fields_t  parse_rule_bm,
                                  tpm_parse_flags_t   parse_flags_bm,
                                  tpm_l2_acl_key_t   *l2_key,
                                  tpm_pkt_frwd_t     *pkt_frwd,
                                  tpm_pkt_mod_t      *pkt_mod,
                                  tpm_pkt_mod_bm_t    pkt_mod_bm,
                                  tpm_rule_action_t  *rule_action)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id      = owner_id;
    tpm_add_acl_rule.src_port      = src_port;
    tpm_add_acl_rule.rule_num      = rule_num;
    tpm_add_acl_rule.parse_rule_bm = parse_rule_bm;
    tpm_add_acl_rule.l2_acl_rule.parse_flags_bm = parse_flags_bm;
    tpm_add_acl_rule.l2_acl_rule.pkt_mod_bm     = pkt_mod_bm;
    if (l2_key != NULL)
    {
        memcpy(&(tpm_add_acl_rule.l2_acl_rule.l2_key),      l2_key,      sizeof(tpm_l2_acl_key_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.l2_acl_rule.l2_key),      0,      sizeof(tpm_l2_acl_key_t));
    }
    if (pkt_frwd != NULL)
    {
        memcpy(&(tpm_add_acl_rule.l2_acl_rule.pkt_frwd),    pkt_frwd,    sizeof(tpm_pkt_frwd_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.l2_acl_rule.pkt_frwd),    0,    sizeof(tpm_pkt_frwd_t));
    }
    if (pkt_mod != NULL)
    {
        memcpy(&(tpm_add_acl_rule.l2_acl_rule.pkt_mod),     pkt_mod,     sizeof(tpm_pkt_mod_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.l2_acl_rule.pkt_mod),     0,     sizeof(tpm_pkt_mod_t));
    }
    if (rule_action != NULL)
    {
        memcpy(&(tpm_add_acl_rule.l2_acl_rule.rule_action), rule_action, sizeof(tpm_rule_action_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.l2_acl_rule.rule_action), 0, sizeof(tpm_rule_action_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_L2_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

//  *rule_idx = dummy_ruleidx++;
    return TPM_RC_OK;
}


/*******************************************************************************
* tpm_add_l3_type_rule()
*
* DESCRIPTION:      Creates a new L3 type (ether type of pppoe proto) processing ACL.
*                   It is used for operations that are not possible to be performed
*                   in a single ACL or to ease the primary ACL processing
*                   (as a helper of the primary L2 ACL).
*                   The L3 type ACL is optional.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* src_port           - The packet originating source port. The parameter has a double function:
*                       . The source port is part of the rules parsing key.
*                       . The source port determines if the acl entry being
*                         created is for the upstream acl or the downstream acl.
*                      In case of an upstream entry, the parameter determines if the packet
*                      arrives from a specific LAN port or ANY LAN port.
* rule_num           - Entry index to be added in the current ACL
* parse_rule_bm      - Bitmap containing the significant flags for parsing fields of the packet.
*                      possible values for L3 API:
*                        TPM_L2_PARSE_ETYPE|TPM_L2_PARSE_PPPOE_SES|TPM_L2_PARSE_PPP_PROT
* parse_flags_bm     - Bitmap containing the significant flags result of the primary ACL filtering.
*                      possible values for L3 API:
*                        TPM_PARSE_FLAG_TAG1_MASK|TPM_PARSE_FLAG_TAG2_MASK|TPM_PARSE_FLAG_MTM_MASK|TPM_PARSE_FLAG_TO_CPU_MASK
* l3_key             - Structure for PPPoE proto or ether type. In order to define a rule for
*                      any ether type, the ether type value should be set to 0xFFFF
* action_drop        - If this stage is dropping the packet.
* pkt_frw            - Information for packet forwarding decision.
* rule_action        - Action associated to the rule = drop/set target/set packet modification/to CPU
*                      possible "next_phase" for L3 API   ->  STAGE_IPv4, STAGE_IPv6_GEN, STAGE_DONE
*
* OUTPUTS:
*  rule_idx         - Unique rule identification number, which is used when deleting the rule. (this is not the rule_num)
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t tpm_add_l3_type_rule (uint32_t            owner_id,
                                       tpm_src_port_type_t src_port,
                                       uint32_t            rule_num,
                                       uint32_t           *rule_idx,
                                       tpm_parse_fields_t  parse_rule_bm,
                                       tpm_parse_flags_t   parse_flags_bm,
                                       tpm_l3_type_key_t  *l3_key,
                                       tpm_pkt_frwd_t     *pkt_frwd,
                                       tpm_rule_action_t  *rule_action)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id      = owner_id;
    tpm_add_acl_rule.src_port      = src_port;
    tpm_add_acl_rule.rule_num      = rule_num;
    tpm_add_acl_rule.parse_rule_bm = parse_rule_bm;
    tpm_add_acl_rule.l3_acl_rule.parse_flags_bm = parse_flags_bm;

    if (l3_key != NULL)
    {
        memcpy(&(tpm_add_acl_rule.l3_acl_rule.l3_key), l3_key, sizeof(tpm_l3_type_key_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.l3_acl_rule.l3_key), 0, sizeof(tpm_l3_type_key_t));
    }
    if (pkt_frwd != NULL)
    {
        memcpy(&(tpm_add_acl_rule.l3_acl_rule.pkt_frwd),    pkt_frwd,    sizeof(tpm_pkt_frwd_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.l3_acl_rule.pkt_frwd),    0,    sizeof(tpm_pkt_frwd_t));
    }
    if (rule_action != NULL)
    {
        memcpy(&(tpm_add_acl_rule.l3_acl_rule.rule_action), rule_action, sizeof(tpm_rule_action_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.l3_acl_rule.rule_action), 0, sizeof(tpm_rule_action_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_L3_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

//  *rule_idx = dummy_ruleidx++;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_add_ipv4_rule()
*
* DESCRIPTION:      Creates a new IPv4 processing ACL. Supports Table mode and ACL mode.
*
* INPUTS:
* owner_id           - API owner id  should be used for all API calls.
* src_port           - The packet originating source port. The parameter determines if the packet is arriving
*                      from the WAN port, a specific LAN port or ANY LAN port.
* rule_num           - Entry index to be added in the current ACL
* parse_rule_bm      - Bitmap containing the significant flags for parsing fields of the packet.
*                      possible values for IPv4 API:
*                        TPM_IPv4_PARSE_SIP|TPM_IPv4_PARSE_DIP|
*                        TPM_IPv4_PARSE_DSCP|TPM_IPv4_PARSE_PROTO|
*                        TPM_PARSE_L4_SRC|TPM_PARSE_L4_DST
* parse_flags_bm     - Bitmap containing the significant flags result of the primary ACL filtering.
*                      possible values for IPv4 API:
*                        TPM_PARSE_FLAG_TAG1_TRUE|TPM_PARSE_FLAG_TAG1_FLASE|
*                        TPM_PARSE_FLAG_MTM_TRUE|TPM_PARSE_FLAG_MTM_FALSE|
*                        TPM_PARSE_FLAG_TO_CPU_TRUE|TPM_PARSE_FLAG_TO_CPU_FALSE|
*                        TPM_PARSE_FLAG_PPPOE_TRUE|TPM_PARSE_FLAG_PPPOE_FALSE
* ipv4_key           - Information to create an IPv4 parsing key for the rule.
*                      Some pointers may be NULL depending on the parse_rule_bm.
* pkt_frwd           - Information for packet forwarding decision.
* pkt_mod            - VLAN packet modification information.
* pkt_mod_bm         - Bitmap containing the significant fields to modify (used for GWY only - in SFU is NULL)
* rule_action        - Action associated to the rule = drop/set target/set packet modification/to CPU
*                      and the next phase.
*                      possible "next_phase" for IPv4 API   ->  STAGE_DONE
*
* OUTPUTS:
*  rule_idx         - If this API is in Table mode, this will equal to the rule_num,
*                     otherwise it will be a unique number acrosss all API's.
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t  tpm_add_ipv4_rule (uint32_t            owner_id,
                                     tpm_src_port_type_t src_port,
                                     uint32_t            rule_num,
                                     uint32_t           *rule_idx,
                                     tpm_parse_fields_t  parse_rule_bm,
                                     tpm_parse_flags_t   parse_flags_bm,
                                     tpm_ipv4_acl_key_t *ipv4_key,
                                     tpm_pkt_frwd_t     *pkt_frwd,
                                     tpm_pkt_mod_t      *pkt_mod,
                                     tpm_pkt_mod_bm_t    pkt_mod_bm,
                                     tpm_rule_action_t  *rule_action)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id      = owner_id;
    tpm_add_acl_rule.src_port      = src_port;
    tpm_add_acl_rule.rule_num      = rule_num;
    tpm_add_acl_rule.parse_rule_bm = parse_rule_bm;
    tpm_add_acl_rule.ipv4_acl_rule.parse_flags_bm = parse_flags_bm;
    tpm_add_acl_rule.ipv4_acl_rule.pkt_mod_bm     = pkt_mod_bm;

    if (ipv4_key != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv4_acl_rule.ipv4_key),    ipv4_key,    sizeof(tpm_ipv4_acl_key_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv4_acl_rule.ipv4_key),    0,    sizeof(tpm_ipv4_acl_key_t));
    }

    if (pkt_frwd != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv4_acl_rule.pkt_frwd),    pkt_frwd,    sizeof(tpm_pkt_frwd_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv4_acl_rule.pkt_frwd),    0,    sizeof(tpm_pkt_frwd_t));
    }
    if (pkt_mod != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv4_acl_rule.pkt_mod),     pkt_mod,     sizeof(tpm_pkt_mod_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv4_acl_rule.pkt_mod),     0,     sizeof(tpm_pkt_mod_t));
    }
    if (rule_action != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv4_acl_rule.rule_action), rule_action, sizeof(tpm_rule_action_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv4_acl_rule.rule_action), 0, sizeof(tpm_rule_action_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_IPv4_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

//  *rule_idx = dummy_ruleidx++;

    return TPM_RC_OK;
}

#ifdef OLD_TPM_APIS
/*******************************************************************************
* tpm_add_ipv4_dscp_acl_rule()
*
* DESCRIPTION:      Creates a new IPv4 processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* src_port           - The packet originating source port. The parameter has a double function:
*                       . The source port is part of the rules parsing key.
*                       . The source port determines if the acl entry being
*                         created is for the upstream acl or the downstream acl.
*                      In case of an upstream entry, the parameter determines if the packet
*                      arrives from a specific LAN port or ANY LAN port.
* rule_num           - Entry index to be added in the current ACL
* parse_flags_bm     - Bitmap containing the significant flags result of the primary ACL filtering.
* ipv4_key           - Information to create an IPv4 parsing key for the rule.
*                      Some pointers may be NULL depending on the parse_rule_bm.
* dscp_val           - Dscp Value or Range
* dscp_mask          - Dscp Range Mask
* target_queue       - The queue the frame is sent to on the (previously determined) target port.
*
* OUTPUTS:
* rule_idx         - Unique rule identification number, which is used when deleting the rule. (this is not the rule_num)
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_add_ipv4_dscp_acl_rule (uint32_t            owner_id,
                                                uint32_t            rule_num,
                                                uint32_t           *rule_idx,
                                                tpm_parse_flags_t   parse_flags_bm,
                                                uint8_t             dscp_val,
                                                uint8_t             dscp_mask,
                                                uint8_t             target_queue)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id                               = owner_id;
    tpm_add_acl_rule.rule_num                               = rule_num;
    tpm_add_acl_rule.ipv4_acl_rule.parse_flags_bm           = parse_flags_bm;
    tpm_add_acl_rule.ipv4_acl_rule.ipv4_key.ipv4_dscp       = dscp_val;
    tpm_add_acl_rule.ipv4_acl_rule.ipv4_key.ipv4_dscp_mask  = dscp_mask;
    tpm_add_acl_rule.ipv4_acl_rule.pkt_frwd.trg_queue       = target_queue;
    tpm_add_acl_rule.add_acl_cmd                            = MV_TPM_IOCTL_ADD_IPv4_DSCP_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

    return TPM_RC_OK;
}
#endif /*OLD_TPM_APIS*/

#ifdef OLD_TPM_APIS
/*******************************************************************************
* tpm_add_ipv6_acl_rule()
*
* DESCRIPTION:      Creates a new IPv6 processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* src_port           - The packet originating source port. The parameter has a double function:
*                       . The source port is part of the rules parsing key.
*                       . The source port determines if the acl entry being
*                         created is for the upstream acl or the downstream acl.
*                      In case of an upstream entry, the parameter determines if the packet
*                      arrives from a specific LAN port or ANY LAN port.
* rule_num           - Entry index to be added in the current ACL
* parse_rule_bm      - Bitmap containing the significant flags for parsing fields of the packet.
* parse_flags_bm     - Bitmap containing the significant flags result of the primary ACL filtering.
* ipv6_key           - Information to create an IPv6 parsing key for the rule.
*                      Some pointers may be NULL depending on the parse_rule_bm.
* pkt_frwd           - Information for packet forwarding decision.
* pkt_mod            - VLAN packet modification information.
* pkt_mod_bm         - Bitmap containing the significant fields to modify (used for GWY only - in SFU is NULL)
* rule_action        - Action associated to the rule = drop/set target/set packet modification/to CPU
*                      and the next phase (for GWY only).
*
* OUTPUTS:
*  rule_idx         - Unique rule identification number, which is used when deleting the rule. (this is not the rule_num)
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t tpm_add_ipv6_acl_rule (uint32_t            owner_id,
                                        tpm_src_port_type_t src_port,
                                        uint32_t            rule_num,
                                        uint32_t           *rule_idx,
                                        tpm_parse_fields_t  parse_rule_bm,
                                        tpm_parse_flags_t   parse_flags_bm,
                                        tpm_ipv6_acl_key_t *ipv6_key,
                                        tpm_pkt_frwd_t     *pkt_frwd,
                                        tpm_pkt_mod_t      *pkt_mod,
                                        tpm_pkt_mod_bm_t    pkt_mod_bm,
                                        tpm_rule_action_t  *rule_action)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id      = owner_id;
    tpm_add_acl_rule.src_port      = src_port;
    tpm_add_acl_rule.rule_num      = rule_num;
    tpm_add_acl_rule.parse_rule_bm = parse_rule_bm;
    tpm_add_acl_rule.ipv6_acl_rule.parse_flags_bm = parse_flags_bm;
    tpm_add_acl_rule.ipv6_acl_rule.pkt_mod_bm     = pkt_mod_bm;

    if (ipv6_key != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_acl_rule.ipv6_key),    ipv6_key,    sizeof(tpm_ipv6_acl_key_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_acl_rule.ipv6_key),    0,    sizeof(tpm_ipv6_acl_key_t));
    }
    if (pkt_frwd != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_acl_rule.pkt_frwd),    pkt_frwd,    sizeof(tpm_pkt_frwd_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_acl_rule.pkt_frwd),    0,    sizeof(tpm_pkt_frwd_t));
    }
    if (pkt_mod != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_acl_rule.pkt_mod),     pkt_mod,     sizeof(tpm_pkt_mod_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_acl_rule.pkt_mod),     0,     sizeof(tpm_pkt_mod_t));
    }
    if (rule_action != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_acl_rule.rule_action), rule_action, sizeof(tpm_rule_action_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_acl_rule.rule_action), 0, sizeof(tpm_rule_action_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_IPv6_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

    return TPM_RC_OK;
}
#endif /*OLD_TPM_APIS*/

#ifdef OLD_TPM_APIS
/*******************************************************************************
* tpm_add_ipv6_dscp_acl_rule()
*
* DESCRIPTION:      Creates a new IPv4 processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* src_port           - The packet originating source port. The parameter has a double function:
*                       . The source port is part of the rules parsing key.
*                       . The source port determines if the acl entry being
*                         created is for the upstream acl or the downstream acl.
*                      In case of an upstream entry, the parameter determines if the packet
*                      arrives from a specific LAN port or ANY LAN port.
* rule_num           - Entry index to be added in the current ACL
* parse_flags_bm     - Bitmap containing the significant flags result of the primary ACL filtering.
* ipv4_key           - Information to create an IPv4 parsing key for the rule.
*                      Some pointers may be NULL depending on the parse_rule_bm.
* dscp_val           - Dscp Value or Range
* dscp_mask          - Dscp Range Mask
* target_queue       - The queue the frame is sent to on the (previously determined) target port.
*
* OUTPUTS:
* rule_idx         - Unique rule identification number, which is used when deleting the rule. (this is not the rule_num)
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_add_ipv6_dscp_acl_rule (     uint32_t            owner_id,
                                                     uint32_t            rule_num,
                                                     uint32_t           *rule_idx,
                                                     tpm_parse_flags_t   parse_flags_bm,
                                                     uint8_t             dscp_val,
                                                     uint8_t             dscp_mask,
                                                     uint8_t             target_queue)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id                               = owner_id;
    tpm_add_acl_rule.rule_num                               = rule_num;
    tpm_add_acl_rule.ipv6_acl_rule.parse_flags_bm           = parse_flags_bm;
    tpm_add_acl_rule.ipv6_acl_rule.ipv6_key.ipv6_dscp       = dscp_val;
    tpm_add_acl_rule.ipv6_acl_rule.ipv6_key.ipv6_dscp_mask  = dscp_mask;
    tpm_add_acl_rule.ipv6_acl_rule.pkt_frwd.trg_queue       = target_queue;
    tpm_add_acl_rule.add_acl_cmd                            = MV_TPM_IOCTL_ADD_IPv6_DSCP_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

    return TPM_RC_OK;
}
#endif /*OLD_TPM_APIS*/

/*******************************************************************************
* tpm_del_l2_rule()
*
* DESCRIPTION:      Deletes an existing L2 ACL primary rule.
*                   Any of the existing entries may be deleted.
* INPUTS:
* owner_id           - API owner id  should be used for all API calls.
* rule_idx           - Rule idenitifcation number specifying the rule to be deleted.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t tpm_del_l2_rule (uint32_t            owner_id,
                                           uint32_t            rule_idx)
{

    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id      = owner_id;
    tpm_del_acl_rule.rule_idx      = rule_idx;

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_L2_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_del_l3_type_rule()
*
* DESCRIPTION:      Deletes an existing ethernet type or PPPoE proto access-list entry .
*
* INPUTS:
* owner_id           - API owner id  should be used for all API calls.
* rule_idx           - rule idenitifcation number specifying the rule to be deleted.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t tpm_del_l3_type_rule (uint32_t            owner_id,
                                           uint32_t            rule_idx)
{
    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id      = owner_id;
    tpm_del_acl_rule.rule_idx      = rule_idx;

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_L3_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_del_ipv4_rule()
*
* DESCRIPTION:      Deletes an existing IPv4 ACL rule.
*                   Both rule number and key are compulsory.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* src_port           - The packet originating source port. The parameter has a double function:
*                       . The source port is part of the rules parsing key.
*                       . The source port determines if the acl entry being
*                         created is for the upstream acl or the downstream acl.
*                      In case of an upstream entry, the parameter determines if the packet
*                      arrives from a specific LAN port or ANY LAN port.
* rule_idx           - Unique rule idenitifcation number specifying the rule to be deleted.
* parse_rule_bm      - Bitmap containing the significant flags for parsing fields of the packet.
* ipv4_key           - Information to create a parsing key for the rule.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t tpm_del_ipv4_rule (uint32_t owner_id,uint32_t rule_idx)
{
    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id      = owner_id;
    tpm_del_acl_rule.rule_idx      = rule_idx;

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_IPv4_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

#ifdef OLD_TPM_APIS
/*******************************************************************************
* tpm_proc_del_ipv4_dscp_acl_rule()
*
* DESCRIPTION:      Creates a new IPv4 processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* rule_idx           - Unique rule identification number, which is used when deleting the rule. (this is not the rule_num)
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_del_ipv4_dscp_acl_rule (     uint32_t           owner_id,
                                                     uint32_t           rule_idx)
{
    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id    = owner_id;
    tpm_del_acl_rule.rule_idx    = rule_idx;
    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_IPv4_DSCP_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
#endif /*OLD_TPM_APIS*/

#ifdef OLD_TPM_APIS
/*******************************************************************************
* tpm_del_ipv6_acl_rule()
*
* DESCRIPTION:      Deletes an existing IPv6 ACL rule.
*                   Both rule number and key are compulsory.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* src_port           - The packet originating source port. The parameter has a double function:
*                       . The source port is part of the rules parsing key.
*                       . The source port determines if the acl entry being
*                         created is for the upstream acl or the downstream acl.
*                      In case of an upstream entry, the parameter determines if the packet
*                      arrives from a specific LAN port or ANY LAN port.
* rule_idx           - Unique rule idenitifcation number specifying the rule to be deleted.
* parse_rule_bm      - Bitmap containing the significant flags for parsing fields of the packet.
* ipv6_key           - Information to create a parsing key for the rule.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_del_ipv6_acl_rule (   uint32_t            owner_id,
                                              tpm_src_port_type_t src_port,
                                              uint32_t            rule_idx,
                                              tpm_parse_fields_t  parse_rule_bm,
                                              tpm_ipv6_acl_key_t *ipv6_key)
{
    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id      = owner_id;
    tpm_del_acl_rule.rule_idx      = rule_idx;
    tpm_del_acl_rule.parse_rule_bm = parse_rule_bm;
    if (ipv6_key!=NULL)
    {
        memcpy(&(tpm_del_acl_rule.ipv6_key), ipv6_key, sizeof(tpm_ipv6_acl_key_t));
    }
    else
    {
        memset(&(tpm_del_acl_rule.ipv6_key), 0, sizeof(tpm_ipv6_acl_key_t));
    }

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_IPv6_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
#endif /*OLD_TPM_APIS*/

#ifdef OLD_TPM_APIS
/*******************************************************************************
* tpm_proc_del_ipv6_dscp_acl_rule()
*
* DESCRIPTION:      Creates a new IPv4 processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* rule_idx           - Unique rule identification number, which is used when deleting the rule. (this is not the rule_num)
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_del_ipv6_dscp_acl_rule (     uint32_t           owner_id,
                                                     uint32_t           rule_idx)
{
    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id    = owner_id;
    tpm_del_acl_rule.rule_idx    = rule_idx;
    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_IPv6_DSCP_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
#endif /*OLD_TPM_APIS*/

/*******************************************************************************
* tpm_get_next_valid_rule()
*
* DESCRIPTION:      General purpose API to retrieve the internal configuration of an existing ACL.
*
* INPUTS:
* owner_id            - API owner id  should be used for all API calls.
* current_index       - the entry index in the section (rule_num/stream_num). In case it is (-1) - the get next function
*                      will point to the first ruleof the section.
* api_type            - TPM API type
*
* OUTPUTS:
* next_index         - returns the first following index (rule_num/stream_num) after the index in the current_index
*                      parameter. It is invalid, if there is no next_index.
* rule_idx           - Unique rule identification number. Equals to stream_num for API's that are operating in table mode.
* tpm_rule           - points to a structure holding the information of a single rule,
*                      of the type specified in the rule_type param.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t    tpm_get_next_valid_rule ( uint32_t          owner_id,
                                              int32_t           current_index,
                                              tpm_api_type_t    api_type,
                                              int32_t          *next_index,
                                              uint32_t         *rule_idx,
                                              tpm_rule_entry_t *tpm_rule)
{
    int                 fd;
    tpm_ioctl_ctrl_acl_rule_t tpm_ctrl_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_ctrl_acl_rule.owner_id      = owner_id;
    tpm_ctrl_acl_rule.current_index = current_index;

    tpm_ctrl_acl_rule.rule_type     = api_type;

    tpm_ctrl_acl_rule.ctrl_acl_cmd = MV_TPM_IOCTL_GET_NEXT_VALID_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_CTRL_ACL_SECTION, &tpm_ctrl_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *next_index = tpm_ctrl_acl_rule.next_index;
    *rule_idx   = tpm_ctrl_acl_rule.rule_idx;
    memcpy(tpm_rule, &(tpm_ctrl_acl_rule.tpm_rule), sizeof(tpm_rule_entry_t));

    return TPM_RC_OK;
}

/******************************************************************************/
/********************************** MC handling APIs **************************/
/******************************************************************************/

/*******************************************************************************
* tpm_add_ipv4_mc_stream()
*
* DESCRIPTION:      Creates a new IPv4 MC stream.
*
* INPUTS:
* owner_id           - API owner id  should be used for all API calls.
* stream_num         - MC stream number.
* igmp_mode          - Defines if stream is in snooping or in proxy mode. Not relevant when sending stream to CPU.
*                        snooping_mode: Vlan_translation only
*                        proxy_mode   : snooping_mode + Replace SA_MAC, and reduce TTL.
*                                       For pppoe stream, replace unicast DA to MC DA, and delete pppoe header.
* mc_stream_pppoe    - The stream's underlying L2 protocol.  0(IPv4oE), 1(IPv4oPPPoE)
* vid                - VLAN ID (0-4095).
*                      If set to 0xFFFF - do not care about vid.
* ipv4_src_add       - IPv4 source IP address in network order.
* ipv4_src_add       - IPv4 destination IP address in network order.
* ignore_ipv4_src    - when set to 1 - the IP source is not part of the key.
* dest_port_bm       - bitmap which includes all destination UNI ports.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*                   It is APIs caller responsibility to maintain the correct number of
*
*******************************************************************************/
tpm_error_code_t    tpm_add_ipv4_mc_stream(   uint32_t            owner_id,
                                              uint32_t            stream_num,
                                              tpm_mc_igmp_mode_t  igmp_mode,
                                              uint8_t             mc_stream_pppoe,
                                              uint16_t            vid,
                                              uint8_t             ipv4_src_add[4],
                                              uint8_t             ipv4_dst_add[4],
                                              uint8_t             ignore_ipv4_src,
                                              tpm_trg_port_type_t dest_port_bm)
{
    int                 fd;
    tpm_ioctl_mc_rule_t tpm_mc_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mc_rule.owner_id                = owner_id;
    tpm_mc_rule.stream_num              = stream_num;
    tpm_mc_rule.igmp_mode               = igmp_mode;
    tpm_mc_rule.mc_stream_pppoe         = mc_stream_pppoe;
    tpm_mc_rule.vid                     = vid;
    tpm_mc_rule.dest_port_bm            = dest_port_bm;
    tpm_mc_rule.ipv4_mc.ignore_ipv4_src = ignore_ipv4_src;
    memcpy(tpm_mc_rule.ipv4_mc.ipv4_dst_add, ipv4_dst_add, 4);
    memcpy(tpm_mc_rule.ipv4_mc.ipv4_src_add, ipv4_src_add, 4);

    tpm_mc_rule.mc_cmd = MV_TPM_IOCTL_ADD_IPv4_MC_STREAM;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MC_STREAM_SECTION, &tpm_mc_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_add_ipv4_mc_stream_set_queue()
*
* DESCRIPTION:      Creates a new IPv4 MC stream with dest Queue.
*                   It is APIs caller responsibility to maintain the correct number of
*                   each stream number.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* stream_num         - MC stream number.
* vid                - VLAN ID (0-4095). If set to 4096 - stream is untagged.
*                      If set to 0xFFFF - do not care.
* ipv4_src_add       - IPv4 source IP address in network order.
* ipv4_src_add       - IPv4 destination IP address in network order.
* ignore_ipv4_src    - when set to 1 - the IP source is not part of the key.
* dest_queue          - destination queue number.
* dest_port_bm       - bitmap which includes all destination UNI ports.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_add_ipv4_mc_stream_set_queue(uint32_t owner_id,
					uint32_t stream_num,
					tpm_mc_igmp_mode_t igmp_mode,
					uint8_t mc_stream_pppoe,
					uint16_t vid,
					uint8_t ipv4_src_add[4],
					uint8_t ipv4_dst_add[4],
					uint8_t ignore_ipv4_src,
					uint16_t dest_queue,
					tpm_trg_port_type_t dest_port_bm)
{
    int 		fd;
    tpm_ioctl_mc_rule_t tpm_mc_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
	return ERR_GENERAL;

    tpm_mc_rule.owner_id		= owner_id;
    tpm_mc_rule.stream_num		= stream_num;
    tpm_mc_rule.igmp_mode		= igmp_mode;
    tpm_mc_rule.mc_stream_pppoe 	= mc_stream_pppoe;
    tpm_mc_rule.vid			= vid;
    tpm_mc_rule.dest_queue              = dest_queue;
    tpm_mc_rule.dest_port_bm		= dest_port_bm;
    tpm_mc_rule.ipv4_mc.ignore_ipv4_src = ignore_ipv4_src;
    memcpy(tpm_mc_rule.ipv4_mc.ipv4_dst_add, ipv4_dst_add, 4);
    memcpy(tpm_mc_rule.ipv4_mc.ipv4_src_add, ipv4_src_add, 4);

    tpm_mc_rule.mc_cmd = MV_TPM_IOCTL_ADD_IPv4_MC_STREAM_SET_QUEUE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MC_STREAM_SECTION, &tpm_mc_rule))
    {
	printf("%s: IOCTL failed\n\r", __FUNCTION__);
	osSemGive(tpmApiSemId);
	return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_updt_ipv4_mc_stream()
*
* DESCRIPTION:      Updates an existing IPv4 MC stream.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* stream_num         - MC stream number.
* dest_port_bm       - bitmap which includes all destination UNI ports.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t    tpm_updt_ipv4_mc_stream(  uint32_t            owner_id,
                                              uint32_t            stream_num,
                                              tpm_trg_port_type_t dest_port_bm)
{
    int                 fd;
    tpm_ioctl_mc_rule_t tpm_mc_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mc_rule.owner_id     = owner_id;
    tpm_mc_rule.stream_num   = stream_num;
    tpm_mc_rule.dest_port_bm = dest_port_bm;

    tpm_mc_rule.mc_cmd = MV_TPM_IOCTL_MOD_IPv4_MC_STREAM;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MC_STREAM_SECTION, &tpm_mc_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_del_ipv4_mc_stream()
*
* DESCRIPTION:      Deletes an existing IPv4 MC stream.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* stream_num         - MC stream number.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_del_ipv4_mc_stream (uint32_t owner_id,
                                         uint32_t stream_num)
{
    int                 fd;
    tpm_ioctl_mc_rule_t tpm_mc_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mc_rule.owner_id     = owner_id;
    tpm_mc_rule.stream_num   = stream_num;

    tpm_mc_rule.mc_cmd = MV_TPM_IOCTL_DEL_IPv4_MC_STREAM;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MC_STREAM_SECTION, &tpm_mc_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_add_ipv6_mc_stream()
*
* DESCRIPTION:      Creates a new ipv6 MC stream.
*
* INPUTS:
* owner_id           - API owner id  should be used for all API calls.
* stream_num         - MC stream number.
* igmp_mode          - Defines if stream is in snooping or in proxy mode. Not relevant when sending stream to CPU.
*                        snooping_mode: Vlan_translation only
*                        proxy_mode   : snooping_mode + Replace SA_MAC, and reduce TTL.
*                                       For pppoe stream, replace unicast DA to MC DA, and delete pppoe header.
* mc_stream_pppoe    - The stream's underlying L2 protocol.  0(ipv6oE), 1(ipv6oPPPoE)
* vid                - VLAN ID (0-4095).
*                      If set to 0xFFFF - do not care about vid.
* ipv6_src_add       - ipv6 source IP address in network order.
* ipv6_dst_add       - ipv6 destination IP address in network order.
* ignore_ipv6_src    - when set to 1 - the IP source is not part of the key.
* dest_port_bm       - bitmap which includes all destination UNI ports.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success - TPM_RC_OK
* On error different types are returned according to the case - see tpm_error_code_t.
*
* COMMENTS:
*                   It is APIs caller responsibility to maintain the correct number of
*
*******************************************************************************/
tpm_error_code_t tpm_add_ipv6_mc_stream(uint32_t		owner_id,
					uint32_t		stream_num,
					tpm_mc_igmp_mode_t	igmp_mode,
					uint8_t 		mc_stream_pppoe,
					uint16_t		vid,
					uint8_t 		ipv6_src_add[16],
					uint8_t 		ipv6_dst_add[16],
					uint8_t 		ignore_ipv6_src,
					tpm_trg_port_type_t	dest_port_bm)
{
    int                 fd;
    tpm_ioctl_mc_rule_t tpm_mc_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mc_rule.owner_id                = owner_id;
    tpm_mc_rule.stream_num              = stream_num;
    tpm_mc_rule.dest_port_bm            = dest_port_bm;
    tpm_mc_rule.vid             		= vid;
    tpm_mc_rule.ipv6_mc.ignore_ipv6_src = ignore_ipv6_src;
    tpm_mc_rule.igmp_mode               = igmp_mode;
    tpm_mc_rule.mc_stream_pppoe         = mc_stream_pppoe;
    memcpy(tpm_mc_rule.ipv6_mc.ipv6_dst_add, ipv6_dst_add, 16);
    memcpy(tpm_mc_rule.ipv6_mc.ipv6_src_add, ipv6_src_add, 16);

    tpm_mc_rule.mc_cmd = MV_TPM_IOCTL_ADD_IPv6_MC_STREAM;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MC_STREAM_SECTION, &tpm_mc_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_add_ipv6_mc_stream_set_queue()
*
* DESCRIPTION:      Creates a new ipv6 MC stream with dest Queue.
*                   It is APIs caller responsibility to maintain the correct number of
*                   each stream number.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* stream_num         - MC stream number.
* vid                - VLAN ID (0-4095). If set to 4096 - stream is untagged.
*                      If set to 0xFFFF - do not care.
* ipv6_src_add       - ipv6 source IP address in network order.
* ipv6_dst_add       - ipv6 destination IP address in network order.
* ignore_ipv6_src    - when set to 1 - the IP source is not part of the key.
* dest_queue          - destination queue number.
* dest_port_bm       - bitmap which includes all destination UNI ports.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_add_ipv6_mc_stream_set_queue(uint32_t owner_id,
					uint32_t stream_num,
					tpm_mc_igmp_mode_t igmp_mode,
					uint8_t mc_stream_pppoe,
					uint16_t vid,
					uint8_t ipv6_src_add[16],
					uint8_t ipv6_dst_add[16],
					uint8_t ignore_ipv6_src,
					uint16_t dest_queue,
					tpm_trg_port_type_t dest_port_bm)
{
    int 		fd;
    tpm_ioctl_mc_rule_t tpm_mc_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
	return ERR_GENERAL;

    tpm_mc_rule.owner_id		= owner_id;
    tpm_mc_rule.stream_num		= stream_num;
    tpm_mc_rule.dest_queue              = dest_queue;
    tpm_mc_rule.dest_port_bm		= dest_port_bm;
    tpm_mc_rule.vid				= vid;
    tpm_mc_rule.ipv6_mc.ignore_ipv6_src = ignore_ipv6_src;
    tpm_mc_rule.igmp_mode		= igmp_mode;
    tpm_mc_rule.mc_stream_pppoe 	= mc_stream_pppoe;
    memcpy(tpm_mc_rule.ipv6_mc.ipv6_dst_add, ipv6_dst_add, 16);
    memcpy(tpm_mc_rule.ipv6_mc.ipv6_src_add, ipv6_src_add, 16);

    tpm_mc_rule.mc_cmd = MV_TPM_IOCTL_ADD_IPv6_MC_STREAM_SET_QUEUE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MC_STREAM_SECTION, &tpm_mc_rule))
    {
	printf("%s: IOCTL failed\n\r", __FUNCTION__);
	osSemGive(tpmApiSemId);
	return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_updt_ipv6_mc_stream()
*
* DESCRIPTION:      Updates an existing IPv6 MC stream.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* stream_num         - MC stream number.
* dest_port_bm       - bitmap which includes all destination UNI ports.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_updt_ipv6_mc_stream (uint32_t            owner_id,
                                          uint32_t            stream_num,
                                          tpm_trg_port_type_t dest_port_bm)
{
    int                 fd;
    tpm_ioctl_mc_rule_t tpm_mc_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mc_rule.owner_id     = owner_id;
    tpm_mc_rule.stream_num   = stream_num;
    tpm_mc_rule.dest_port_bm = dest_port_bm;

    tpm_mc_rule.mc_cmd = MV_TPM_IOCTL_MOD_IPv6_MC_STREAM;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MC_STREAM_SECTION, &tpm_mc_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_del_ipv6_mc_stream()
*
* DESCRIPTION:      Deletes an existing IPv6 MC stream.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* stream_num         - MC stream number.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_del_ipv6_mc_stream (uint32_t owner_id,
                                         uint32_t stream_num)
{
    int                 fd;
    tpm_ioctl_mc_rule_t tpm_mc_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mc_rule.owner_id   = owner_id;
    tpm_mc_rule.stream_num = stream_num;

    tpm_mc_rule.mc_cmd = MV_TPM_IOCTL_DEL_IPv6_MC_STREAM;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MC_STREAM_SECTION, &tpm_mc_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/******************************************************************************/
/********************************** Management protocol APIs ******************/
/******************************************************************************/

/*******************************************************************************
* tpm_omci_add_channel()
*
* DESCRIPTION:      Establishes a communication channel for the OMCI management protocol.
*                   The API sets the gemportid, the Rx input queue in the CPU, and the
*                   Tx T-CONT and queue parameters, which are configured in the driver.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* gem_port           - for OMCI Rx frames - the gem port wherefrom the OMCI frames are received.
* cpu_rx_queue       - for OMCI Rx frames - the CPU rx queue number.
* tcont_num          - for OMCI Tx frames - the TCONT number where to send the OMCI frames.
* cpu_tx_queue       - for OMCI Tx frames - the CPU tx queue number.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_omci_add_channel (uint32_t              owner_id,
                                       tpm_gem_port_key_t    gem_port,
                                       uint32_t              cpu_rx_queue,
                                       tpm_trg_port_type_t   tcont_num,
                                       uint32_t              cpu_tx_queue)
{
    int                fd;
    tpm_ioctl_mng_ch_t tpm_mng_ch;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mng_ch.tpm_ioctl_omci_ch.owner_id     = owner_id;
    tpm_mng_ch.tpm_ioctl_omci_ch.gem_port     = gem_port;
    tpm_mng_ch.tpm_ioctl_omci_ch.cpu_rx_queue = cpu_rx_queue;
    tpm_mng_ch.tpm_ioctl_omci_ch.tcont_num    = tcont_num;
    tpm_mng_ch.tpm_ioctl_omci_ch.cpu_tx_queue = cpu_tx_queue;

    tpm_mng_ch.mng_cmd = MV_TPM_IOCTL_ADD_OMCI_CHNL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MNG_SECTION, &tpm_mng_ch))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_omci_del_channel()
*
* DESCRIPTION:      Deletes an existing communication channel for the OMCI management protocol.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_omci_del_channel (uint32_t  owner_id)
{
    int             fd;
    tpm_ioctl_mng_ch_t tpm_mng_ch;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mng_ch.tpm_ioctl_omci_ch.owner_id = owner_id;

    tpm_mng_ch.mng_cmd = MV_TPM_IOCTL_DEL_OMCI_CHNL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MNG_SECTION, &tpm_mng_ch))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_omci_get_channel()
*
* DESCRIPTION:      Retrieves the OMCI management protocol channel information.
*
* INPUTS:
*  None.
*
* OUTPUTS:
* is_valid           - indicates that the OMCI channel is valid or not.
* gem_port           - for OMCI Rx frames - the gem port wherefrom the OMCI frames are received.
* cpu_rx_queue       - for OMCI Rx frames - the CPU rx queue number.
* tcont_num          - for OMCI Tx frames - the TCONT number where to send the OMCI frames.
* cpu_tx_queue       - for OMCI Tx frames - the CPU tx queue number.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t    tpm_omci_get_channel (  uint32_t                *is_valid,
                                            tpm_gem_port_key_t      *gem_port,
                                            uint32_t                *cpu_rx_queue,
                                            tpm_trg_port_type_t     *tcont_num,
                                            uint32_t                *cpu_tx_queue)
{
    int                fd;
    tpm_ioctl_mng_ch_t tpm_mng_ch;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mng_ch.mng_cmd = MV_TPM_IOCTL_GET_OMCI_CHNL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MNG_SECTION, &tpm_mng_ch))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *is_valid     = tpm_mng_ch.tpm_ioctl_omci_ch.is_valid;
    *gem_port     = tpm_mng_ch.tpm_ioctl_omci_ch.gem_port;
    *cpu_rx_queue = tpm_mng_ch.tpm_ioctl_omci_ch.cpu_rx_queue;
    *tcont_num    = tpm_mng_ch.tpm_ioctl_omci_ch.tcont_num;
    *cpu_tx_queue = tpm_mng_ch.tpm_ioctl_omci_ch.cpu_tx_queue;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_loop_detect_add_channel()
*
* DESCRIPTION:      Establishes a communication channel for loop detection management protocol.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_loop_detect_add_channel (uint32_t owner_id, uint32_t ety)
{
    int                fd;
    tpm_ioctl_mng_ch_t tpm_mng_ch;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mng_ch.tpm_ioctl_oam_ch.owner_id     = owner_id;
    tpm_mng_ch.loopback_detect_ety           = ety;

    tpm_mng_ch.mng_cmd = MV_TPM_IOCTL_ADD_LOOP_DETECT_CHNL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MNG_SECTION, &tpm_mng_ch))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_loop_detect_del_channel()
*
* DESCRIPTION:      remove a communication channel for loop detection management protocol.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_loop_detect_del_channel (uint32_t owner_id)
{
    int                fd;
    tpm_ioctl_mng_ch_t tpm_mng_ch;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mng_ch.tpm_ioctl_oam_ch.owner_id     = owner_id;

    tpm_mng_ch.mng_cmd = MV_TPM_IOCTL_DEL_LOOP_DETECT_CHNL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MNG_SECTION, &tpm_mng_ch))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_loopback_add_channel()
*
* DESCRIPTION:      Establishes a communication channel for loopback management protocol.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_oam_loopback_add_channel (uint32_t            owner_id)
{
    int                fd;
    tpm_ioctl_mng_ch_t tpm_mng_ch;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mng_ch.tpm_ioctl_oam_ch.owner_id     = owner_id;

    tpm_mng_ch.mng_cmd = MV_TPM_IOCTL_ADD_OAM_LOOPBACK_CHNL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MNG_SECTION, &tpm_mng_ch))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_loopback_del_channel()
*
* DESCRIPTION:      Establishes a communication channel for loopback management protocol.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_oam_loopback_del_channel (uint32_t            owner_id)
{
    int                fd;
    tpm_ioctl_mng_ch_t tpm_mng_ch;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mng_ch.tpm_ioctl_oam_ch.owner_id     = owner_id;

    tpm_mng_ch.mng_cmd = MV_TPM_IOCTL_DEL_OAM_LOOPBACK_CHNL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MNG_SECTION, &tpm_mng_ch))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}



/*******************************************************************************
* tpm_oam_epon_add_channel()
*
* DESCRIPTION:      Establishes a communication channel for the OAM EPON management protocol.
*                   The API sets the Rx input queue in the CPU, and the
*                   Tx T-CONT and queue parameters, which are configured in the driver.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* cpu_rx_queue       - for OAM (EPON) Rx frames - the CPU rx queue number.
* llid_num           - for OAM (EPON)Tx frames - the LLID number where to send the OMCI frames.
* cpu_tx_queue       - for OAM (EPON) Tx frames - the CPU tx queue number.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_oam_epon_add_channel (uint32_t            owner_id,
                                           uint32_t            cpu_rx_queue,
                                           tpm_trg_port_type_t llid_num)
{
    int                fd;
    tpm_ioctl_mng_ch_t tpm_mng_ch;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mng_ch.tpm_ioctl_oam_ch.owner_id     = owner_id;
    tpm_mng_ch.tpm_ioctl_oam_ch.cpu_rx_queue = cpu_rx_queue;
    tpm_mng_ch.tpm_ioctl_oam_ch.llid_num     = llid_num;

    tpm_mng_ch.mng_cmd = MV_TPM_IOCTL_ADD_OAM_CHNL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MNG_SECTION, &tpm_mng_ch))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_oam_epon_del_channel()
*
* DESCRIPTION:      Deletes an existing communication channel for the OAM EPON management protocol.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_oam_epon_del_channel (uint32_t owner_id)
{
    int             fd;
    tpm_ioctl_mng_ch_t tpm_mng_ch;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mng_ch.tpm_ioctl_oam_ch.owner_id = owner_id;

    tpm_mng_ch.mng_cmd = MV_TPM_IOCTL_DEL_OAM_CHNL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MNG_SECTION, &tpm_mng_ch))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_oam_epon_get_channel()
*
* DESCRIPTION:      Retrieves OAM EPON management protocol channel information.
*
* INPUTS:
*  None.
*
* OUTPUTS:
* is_valid           -  indicates that the OMCI channel is valid or not.
* cpu_rx_queue       - for OAM (EPON) Rx frames - the CPU rx queue number.
* llid_num           - for OAM (EPON)Tx frames - the LLID number where to send the OMCI frames.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_oam_epon_get_channel (uint32_t            *is_valid,
                                           uint32_t            *cpu_rx_queue,
                                           tpm_trg_port_type_t *llid_num)
{
    int                fd;
    tpm_ioctl_mng_ch_t tpm_mng_ch;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mng_ch.mng_cmd = MV_TPM_IOCTL_GET_OAM_CHNL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_MNG_SECTION, &tpm_mng_ch))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *is_valid     = tpm_mng_ch.tpm_ioctl_oam_ch.owner_id;
    *cpu_rx_queue = tpm_mng_ch.tpm_ioctl_oam_ch.cpu_rx_queue;
    *llid_num     = tpm_mng_ch.tpm_ioctl_oam_ch.llid_num;

    return TPM_RC_OK;
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/********************************** Switch APIs *******************************/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

/******************************************************************************/
/******************************************************************************/
/********************************** MAC security APIs *************************/
/******************************************************************************/
/*******************************************************************************
* tpm_sw_add_static_mac()
*
* DESCRIPTION:      Creates a static MAC entry in the Switch - per port.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* port               - switch port number to configure the static MAC.
* static_mac         - MAC address.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_add_static_mac (uint32_t            owner_id,
                                        tpm_src_port_type_t port,
                                        uint8_t             static_mac[6])
{
    int                         fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id = owner_id;
    tpm_sw_mac_security.port     = port;
    memcpy(&(tpm_sw_mac_security.static_mac[0]), static_mac, 6);

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_ADD_STATIC_MAC;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_del_static_mac()
*
* DESCRIPTION:      Removes a static MAC entry from the Switch.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* static_mac         - MAC address.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_del_static_mac (uint32_t owner_id,
                                        uint8_t  static_mac[6])
{
    int                         fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id = owner_id;
    memcpy(&(tpm_sw_mac_security.static_mac[0]), static_mac, 6);

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_DEL_STATIC_MAC;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_set_port_max_macs()
*
* DESCRIPTION:      limits the number of MAC addresses per port.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* port               - switch port number to configure limit static MACs.
* mac_per_port       - max number of MAC addresses per port (1-255).
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_port_max_macs (uint32_t            owner_id,
                                           tpm_src_port_type_t port,
                                           uint8_t             mac_per_port)
{
    int                         fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id     = owner_id;
    tpm_sw_mac_security.port         = port;
    tpm_sw_mac_security.mac_per_port = mac_per_port;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_SET_PORT_MAC_LIMIT;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_set_mac_age_time
*
* DESCRIPTION:
*       This function Sets the Mac aging time.
*
* INPUTS:
*       owner_id    - APP owner id , should be used for all API calls.
*       mac_age_time    - Aging Time value
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case , see tpm_error_code_t.
*
* COMMENTS:
*        NONE.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_mac_age_time
(
    uint32_t owner_id,
    uint32_t mac_age_time
)
{
    int fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id = owner_id;
    tpm_sw_mac_security.mac_age_time = mac_age_time;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_SET_MAC_AGE_TIME;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_get_mac_age_time
*
* DESCRIPTION:
*       This function Gets the Mac address aging time.
*
* INPUTS:
*       owner_id    - APP owner id , should be used for all API calls.
*
* OUTPUTS:
*       mac_age_time    - Aging Time value
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case , see tpm_error_code_t.
*
* COMMENTS:
*        NONE.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_get_mac_age_time
(
    uint32_t owner_id,
    uint32_t * mac_age_time
)
{
    int fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id = owner_id;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_GET_MAC_AGE_TIME;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *mac_age_time = tpm_sw_mac_security.mac_age_time;

    return TPM_RC_OK;
}

/*******************************************************************************
*  tpm_sw_set_mac_learn
*
* DESCRIPTION:
*       Enable/disable automatic learning of new source MAC addresses on port
*       ingress.
*
* INPUTS:
*       owner_id    - APP owner id , should be used for all API calls.
*       port       - logical port number to set.
*       enable - GT_TRUE for enable  or GT_FALSE otherwise
*
* OUTPUTS:
*       None
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case, see tpm_error_code_t.
*
* COMMENTS:
*
* GalTis:
*
*******************************************************************************/
tpm_error_code_t  tpm_sw_set_mac_learn
(
    uint32_t owner_id,
    tpm_src_port_type_t port,
    bool enable
)
{
    int fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id = owner_id;
    tpm_sw_mac_security.port  = port;
    tpm_sw_mac_security.enable_mac_learn = enable;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_SET_MAC_LEARN;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
*  tpm_sw_get_mac_learn
*
* DESCRIPTION:
*       Enable/disable automatic learning of new source MAC addresses on port
*       ingress.
*
* INPUTS:
*       owner_id    - APP owner id , should be used for all API calls.
*       port       - logical port number to set.
*
* OUTPUTS:
*       enable - GT_TRUE for enable  or GT_FALSE otherwise
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case, see tpm_error_code_t.
*
* COMMENTS:
*
* GalTis:
*
*******************************************************************************/
tpm_error_code_t  tpm_sw_get_mac_learn
(
    uint32_t owner_id,
    tpm_src_port_type_t port,
    bool* enable
)
{
    int fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id = owner_id;
    tpm_sw_mac_security.port  = port;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_GET_MAC_LEARN;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *enable = tpm_sw_mac_security.enable_mac_learn ;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_set_mtu_size
*
* DESCRIPTION:
*       Set switch MTU size.
*
* INPUTS:
*       owner_id - APP owner id , should be used for all API calls.
*          type        - MRU type:GMAC0, GMAC1, PONMAC, switch
*       mtu          - MTU size.
*
* OUTPUTS:
*       None
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case , see tpm_error_code_t.
*
* COMMENTS:
*       None
*
*******************************************************************************/
tpm_error_code_t tpm_set_mtu_size
(
    uint32_t owner_id,
    tpm_mru_type_t type,
    uint32_t mtu
)
{
    int fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id = owner_id;
    /*Need not check src port when set MTU size*/
    tpm_sw_mac_security.port     = 0;
    tpm_sw_mac_security.mtu_type = type;
    tpm_sw_mac_security.mtu_size = mtu;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SET_MTU_SIZE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_get_mtu_size
*
* DESCRIPTION:
*       Get switch MTU size.
*
* INPUTS:
*       owner_id - APP owner id , should be used for all API calls.
*          type        - MRU type:GMAC0, GMAC1, PONMAC, switch
*
* OUTPUTS:
*       mtu          - MTU size.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case , see tpm_error_code_t.
*
* COMMENTS:
*       None
*
*******************************************************************************/
tpm_error_code_t tpm_get_mtu_size
(
    uint32_t owner_id,
    tpm_mru_type_t type,
    uint32_t* mtu
)
{
    int fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id = owner_id;
    tpm_sw_mac_security.mtu_type = type;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_GET_MTU_SIZE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *mtu = tpm_sw_mac_security.mtu_size;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_sw_set_port_flooding()
*
* DESCRIPTION:      permit or not the flooding per port
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* egress_port           - switch port number to enable/disable flooding.
* flood_mode       - flooding mode
* allow_flood        - set to 1 = permit flooding of unknown DA.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_port_flooding (uint32_t            owner_id,
                                           tpm_src_port_type_t egress_port,
                                           tpm_flood_type_t flood_mode,
                                           uint8_t             allow_flood)
{
    int                         fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id    = owner_id;
    tpm_sw_mac_security.port        = egress_port;
    tpm_sw_mac_security.allow_flood = allow_flood;
    tpm_sw_mac_security.flood_mode = flood_mode;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_SET_PORT_FLOOD;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_get_port_flooding()
*
* DESCRIPTION:      permit or not the flooding per port
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* egress_port       - switch port number to enable/disable flooding.
* flood_mode       - flooding mode
*
* OUTPUTS:
* allow_flood        - set to 1 = permit flooding .
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_get_port_flooding (uint32_t            owner_id,
                                           tpm_src_port_type_t egress_port,
                                           tpm_flood_type_t flood_mode,
                                           uint8_t * allow_flood)
{
    int                         fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id    = owner_id;
    tpm_sw_mac_security.port        = egress_port;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_GET_PORT_FLOOD;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *allow_flood = tpm_sw_mac_security.allow_flood;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_clear_dynamic_mac
*
* DESCRIPTION:
*       Clear all dynamic MAC.
*
* INPUTS:
*
* OUTPUTS:
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case , see tpm_error_code_t.
*
* COMMENTS:
*       None
*
*******************************************************************************/
tpm_error_code_t tpm_sw_clear_dynamic_mac(uint32_t owner_id)
{
    int                         fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_CLEAR_DYNAMIC_MAC;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_set_port_mirror
*
* DESCRIPTION:
*       Set port mirror.
*
* INPUTS:
*       owner_id    - APP owner id , should be used for all API calls.
*       sport         - Source port.
*       dport         - Destination port.
*       mode         - mirror mode.
*       enable         - enable/disable mirror.
*
* OUTPUTS:
*
* RETURNS:
*       GT_OK      - on success
*       GT_FAIL    - on error
*       GT_NOT_SUPPORTED - if current device does not support this feature.
*
* COMMENTS:
*       None
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_port_mirror(uint32_t owner_id,
                                                                tpm_src_port_type_t  sport,
                                                                tpm_src_port_type_t  dport,
                                                                tpm_sw_mirror_type_t mode,
                                                                bool enable)
{
    int                         fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.port_mirror.sport = sport;
    tpm_sw_mac_security.port_mirror.dport = dport;
    tpm_sw_mac_security.port_mirror.mode = mode;
    tpm_sw_mac_security.port_mirror.enable= enable;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_SET_MIRROR;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_get_port_mirror
*
* DESCRIPTION:
*       Get port mirror status.
*
* INPUTS:
*       owner_id    - APP owner id , should be used for all API calls.
*       sport         - Source port.
*       dport         - Destination port.
*       mode         - mirror mode.
*
* OUTPUTS:
*       enable         - enable/disable mirror.
*
* RETURNS:
*       GT_OK      - on success
*       GT_FAIL    - on error
*       GT_NOT_SUPPORTED - if current device does not support this feature.
*
* COMMENTS:
*       None
*
*******************************************************************************/
tpm_error_code_t tpm_sw_get_port_mirror(uint32_t owner_id,
                                                                tpm_src_port_type_t  sport,
                                                                tpm_src_port_type_t  dport,
                                                                tpm_sw_mirror_type_t mode,
                                                                bool* enable)
{
    int                         fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id    = owner_id;
    tpm_sw_mac_security.port_mirror.sport = sport;
    tpm_sw_mac_security.port_mirror.dport = dport;
    tpm_sw_mac_security.port_mirror.mode = mode;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_GET_MIRROR;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *enable = tpm_sw_mac_security.port_mirror.enable;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_sw_set_isolate_eth_port_vector()
*
* DESCRIPTION:      Set isolated port vector.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* port              - switch port number to delete the VIDs.
* port_vector               - port vector.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_isolate_eth_port_vector (uint32_t            owner_id,
                                      tpm_src_port_type_t port,
                                      uint32_t            eth_port_vector)
{
    int                         fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id    = owner_id;
    tpm_sw_mac_security.port = port;
    tpm_sw_mac_security.port_vector = eth_port_vector;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_SET_ISOLATE_PORT_VECTOR;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_sw_get_isolate_eth_port_vector()
*
* DESCRIPTION:      Get isolated port vector.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* port              - switch port number to delete the VIDs.
*
* OUTPUTS:
* port_vector        - port vector.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_get_isolate_eth_port_vector (uint32_t            owner_id,
                                      tpm_src_port_type_t port,
                                      uint32_t *           eth_port_vector)

{
    int                         fd;
    tpm_ioctl_sw_mac_security_t tpm_sw_mac_security;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_security.owner_id    = owner_id;
    tpm_sw_mac_security.port = port;

    tpm_sw_mac_security.sw_security_cmd = MV_TPM_IOCTL_SW_GET_ISOLATE_PORT_VECTOR;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_SECURITY_SECTION, &tpm_sw_mac_security))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *eth_port_vector = tpm_sw_mac_security.port_vector;

    return TPM_RC_OK;
}
/******************************************************************************/
/********************************** VLAN filtering  APIs **********************/
/******************************************************************************/

/*******************************************************************************
* tpm_sw_set_port_tagged()
*
* DESCRIPTION:      allow or drop tagged packets per port.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* port              - switch port number to apply the tagged packet filtering.
* allow_tagged      - set to 1 = allow tagged packets per port.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_port_tagged (uint32_t            owner_id,
                                         tpm_src_port_type_t port,
                                         uint8_t             allow_tagged)
{
    int                        fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id     = owner_id;
    tpm_sw_mac_vlan.port         = port;
    tpm_sw_mac_vlan.allow_tagged = allow_tagged;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_SET_PORT_TAGGED;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_set_port_untagged()
*
* DESCRIPTION:      allow or drop untagged packets per port.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* port              - switch port number to apply the untagged packet filtering.
* allow_untagged    - set to 1 = allow untagged packets per port.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_port_untagged (uint32_t            owner_id,
                                           tpm_src_port_type_t port,
                                           uint8_t             allow_untagged)
{
    int                        fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id       = owner_id;
    tpm_sw_mac_vlan.port           = port;
    tpm_sw_mac_vlan.allow_untagged = allow_untagged;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_SET_PORT_UNTAGGED;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_set_port_def_vlan
*
* DESCRIPTION:
*       The API sets port default vlan id.
*
* INPUTS:
*       owner_id   - APP owner id  should be used for all API calls.
*       port       - lport for applying the filtering of tagged packets.
*       vid        - the port vlan id.
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case  see tpm_error_code_t.
*
* COMMENTS:
*       None.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_port_def_vlan
(
    uint32_t owner_id,
    tpm_src_port_type_t port,
    uint16_t vid
)
{
    int fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id     = owner_id;
    tpm_sw_mac_vlan.port         = port;
    tpm_sw_mac_vlan.vid = vid;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_PORT_SET_DEFAULT_VLAN;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_get_port_def_vlan
*
* DESCRIPTION:
*       The API gets port default vlan id.
*
* INPUTS:
*       owner_id   - APP owner id  should be used for all API calls.
*       port       - lport for applying the filtering of tagged packets.
*
* OUTPUTS:
*       vid        - the port vlan id
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case  see tpm_error_code_t.
*
* COMMENTS:
*       None.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_get_port_def_vlan
(
    uint32_t owner_id,
    tpm_src_port_type_t port,
    uint16_t *vid
)
{
    int fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id       = owner_id;
    tpm_sw_mac_vlan.port = port;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_PORT_GET_DEFAULT_VLAN;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *vid = tpm_sw_mac_vlan.vid;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_set_port_def_pri
*
* DESCRIPTION:
*       The API sets port default priority.
*
* INPUTS:
*       owner_id   - APP owner id should be used for all API calls.
*       port       - lport for applying the filtering of tagged packets.
*       pri        - the port priority.
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
*
* COMMENTS:
*       None.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_port_def_pri
(
    uint32_t owner_id,
    tpm_src_port_type_t port,
    uint8_t  pri
)
{
    int fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id     = owner_id;
    tpm_sw_mac_vlan.port         = port;
    tpm_sw_mac_vlan.pri = pri;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_PORT_SET_DEFAULT_PRIORITY;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_get_port_def_pri
*
* DESCRIPTION:
*       The API gets port default priority.
*
* INPUTS:
*       owner_id   - APP owner id should be used for all API calls.
*       port       - lport for applying the filtering of tagged packets.
*
* OUTPUTS:
*       pri        - the port priority.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case  see tpm_error_code_t.
*
* COMMENTS:
*       None.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_get_port_def_pri
(
    uint32_t owner_id,
    tpm_src_port_type_t port,
    uint8_t  *pri
)
{
    int fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id       = owner_id;
    tpm_sw_mac_vlan.port = port;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_PORT_GET_DEFAULT_PRIORITY;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *pri = tpm_sw_mac_vlan.pri;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_sw_port_add_vid()
*
* DESCRIPTION:      Adds a VID to the list of the allowed VIDs per port.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* port              - switch port number to add the VIDs.
* vid               - VLAN ID.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_port_add_vid (uint32_t            owner_id,
                                      tpm_src_port_type_t port,
                                      uint16_t            vid)
{
    int                        fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id = owner_id;
    tpm_sw_mac_vlan.port     = port;
    tpm_sw_mac_vlan.vid      = vid;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_PORT_ADD_VID;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_clear_vid_per_port
*
* DESCRIPTION:
*       The API delete all VID from the list of VIDs allowed per lport.
*
* INPUTS:
*       owner_id - APP owner id should be used for all API calls.
*       lport     -  lport for deleting the VID.
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case - see tpm_error_code_t.
*
* COMMENTS:
*       None.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_clear_vid_per_port
(
    uint32_t            owner_id,
    tpm_src_port_type_t port
)
{
    int   fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id = owner_id;
    tpm_sw_mac_vlan.port     = port;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_PORT_CLEAR_VID;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_sw_add_all_vid_per_port
*
* DESCRIPTION:
*       The API adds all allowed VIDs from 0 to 4095 per lport.
*
* INPUTS:
*       owner_id - APP owner id should be used for all API calls.
*       port     -  port for adding the VID.
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case , see tpm_error_code_t.
*
* COMMENTS:
*       None.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_add_all_vid_per_port
(
    uint32_t            owner_id,
    tpm_src_port_type_t port
)
{
    int   fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id = owner_id;
    tpm_sw_mac_vlan.port         = port;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_PORT_ADD_ALL_VID;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}


/*******************************************************************************
* tpm_sw_set_port_vid_egress_mode()
*
* DESCRIPTION:      Set Egress mode of a VID per port.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* port              - switch port number to add the VIDs.
* vid               - VLAN ID.
* egress_mode       -egress mode
*    0 :MEMBER_EGRESS_UNMODIFIED
*     1:NOT_A_MEMBER
*     2:MEMBER_EGRESS_UNTAGGED
*    3:MEMBER_EGRESS_TAGGED
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_port_vid_egress_mode (uint32_t            owner_id,
                                      tpm_src_port_type_t port,
                                      uint16_t            vid,
                                      uint8_t egress_mode)
{
    int   fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id = owner_id;
    tpm_sw_mac_vlan.port     = port;
    tpm_sw_mac_vlan.vid      = vid;
    tpm_sw_mac_vlan.egress_mode      = egress_mode;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_PORT_VLAN_EGRESS_MODE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_port_del_vid()
*
* DESCRIPTION:      Adds a VID to the list of the allowed VIDs per port.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* port              - switch port number to delete the VIDs.
* vid               - VLAN ID.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_port_del_vid (uint32_t            owner_id,
                                      tpm_src_port_type_t port,
                                      uint16_t            vid)
{
    int                        fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id = owner_id;
    tpm_sw_mac_vlan.port     = port;
    tpm_sw_mac_vlan.vid      = vid;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_PORT_DEL_VID;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_sw_port_add_vid_group()
*
* DESCRIPTION:      Add a group of VID to the list of the allowed VIDs per port,
*                    and set the egress mode correspondingly.
*
* INPUTS:
* owner_id      - APP owner id  should be used for all API calls.
* port             - switch port number to add the VIDs.
* mode         -VLAN egress mode.
* min_vid        - min VLAN ID.
* max_vid     - max VLAN ID.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_port_add_vid_group (uint32_t            owner_id,
                                      tpm_src_port_type_t port,
                                      tpm_vlan_member_mode_t            mode,
                                      uint16_t            min_vid,
                                      uint16_t            max_vid)
{
    int                        fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id = owner_id;
    tpm_sw_mac_vlan.port     = port;
    tpm_sw_mac_vlan.egress_mode = mode;
    tpm_sw_mac_vlan.min_vid = min_vid;
    tpm_sw_mac_vlan.max_vid = max_vid;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_ADD_VID_GROUP;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_port_del_vid_group()
*
* DESCRIPTION:      Delete a group of VID to the list of the allowed VIDs per port,
*                    and set the egress mode correspondingly.
*
* INPUTS:
* owner_id      - APP owner id  should be used for all API calls.
* port             - switch port number to add the VIDs.
* min_vid        - min VLAN ID.
* max_vid     - max VLAN ID.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_port_del_vid_group (uint32_t            owner_id,
                                      tpm_src_port_type_t port,
                                      uint16_t            min_vid,
                                      uint16_t            max_vid)
{
    int                        fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id = owner_id;
    tpm_sw_mac_vlan.port     = port;
    tpm_sw_mac_vlan.min_vid = min_vid;
    tpm_sw_mac_vlan.max_vid = max_vid;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_DEL_VID_GROUP;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}


/*******************************************************************************
* tpm_sw_port_set_vid_filter()
*
* DESCRIPTION:      Set VID filtering mode per port.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* port              - switch port number to set vid filter.
* vid               - vlan filtering mode.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_port_set_vid_filter (uint32_t            owner_id,
                                             tpm_src_port_type_t src_port,
                                             uint8_t             vid_filter)
{
    int                        fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id   = owner_id;
    tpm_sw_mac_vlan.port       = src_port;
    tpm_sw_mac_vlan.vid_filter = vid_filter;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_PORT_SER_VID_FILTER;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/******************************************************************************/
/********************************** Traffic management switch APIs ************/
/******************************************************************************/
/*******************************************************************************
* tpm_tm_set_uni_sched()
*
* DESCRIPTION:      Configures the scheduling mode per Ethernet port.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* uni_port          - UNI port for setting the scheduling mode
* sched_mode        - scheduler mode per port
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_uni_sched (uint32_t            owner_id,
                                       tpm_src_port_type_t uni_port,
                                       tpm_sw_sched_type_t sched_mode)
{
    int               fd;
    tpm_ioctl_sw_tm_t tpm_sw_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_tm.owner_id      = owner_id;
    tpm_sw_tm.uni_port      = uni_port;
    tpm_sw_tm.sw_sched_mode = sched_mode;

    tpm_sw_tm.sw_tm_cmd = MV_TPM_IOCTL_SW_SET_UNI_SCHED;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_TM_SECTION, &tpm_sw_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_set_uni_q_weight()
*
* DESCRIPTION:      Configures the weight of a queue for all Ethernet UNI ports
*                   in the integrated switch.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* queue_id          - queueId for setting the weight.
* weight            - weight value per queue.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_uni_q_weight (uint32_t owner_id,
                                          uint8_t  queue_id,
                                          uint8_t  weight)
{
    int               fd;
    tpm_ioctl_sw_tm_t tpm_sw_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_tm.owner_id = owner_id;
    tpm_sw_tm.queue_id = queue_id;
    tpm_sw_tm.weight   = weight;

    tpm_sw_tm.sw_tm_cmd = MV_TPM_IOCTL_SW_UNI_QUE_WEIGHT;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_TM_SECTION, &tpm_sw_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_set_uni_ingr_police_rate
*
* DESCRIPTION:
*       The API Configures an ingress policing function for an Ethernet UNI lport.
*
* INPUTS:
*       owner_id - APP owner id, should be used for all API calls.
*       uni_port - uni lport for configuring the ingress policer function.
*       count_mode - count mode:
*                               GT_PIRL2_COUNT_FRAME
*                               GT_PIRL2_COUNT_ALL_LAYER1
*                               GT_PIRL2_COUNT_ALL_LAYER2
*                               GT_PIRL2_COUNT_ALL_LAYER3
*       cir           - comited info rate.
*          cbs            - Committed Burst Size limit (expected to be 2kBytes)
*          ebs        - Excess Burst Size limit ( 0 ~ 0xFFFFFF)
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case, see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_uni_ingr_police_rate (uint32_t owner_id,
                                                  tpm_src_port_type_t  uni_port,
                                                  tpm_limit_mode_t    count_mode,
                                                  uint32_t cir,
                                                  uint32_t cbs,
                                                  uint32_t ebs)
{
    int               fd;
    tpm_ioctl_sw_tm_t tpm_sw_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_tm.owner_id = owner_id;
    tpm_sw_tm.uni_port = uni_port;
    tpm_sw_tm.limit_mode = count_mode;
    tpm_sw_tm.cir      = cir;
    tpm_sw_tm.cbs = cbs;
    tpm_sw_tm.ebs = ebs;

    tpm_sw_tm.sw_tm_cmd = MV_TPM_IOCTL_SW_SET_UNI_INGR_POLICE_RATE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_TM_SECTION, &tpm_sw_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_sw_get_uni_ingr_police_rate
*
* DESCRIPTION:
*       The API gets an ingress policing function for an Ethernet UNI lport.
*
* INPUTS:
*       owner_id - APP owner id, should be used for all API calls.
*       uni_port - uni lport for configuring the ingress policer function.
*
*
* OUTPUTS:
*       count_mode - count mode:
*                               GT_PIRL2_COUNT_FRAME
*                               GT_PIRL2_COUNT_ALL_LAYER1
*                               GT_PIRL2_COUNT_ALL_LAYER2
*                               GT_PIRL2_COUNT_ALL_LAYER3
*       cir           - comited info rate.
*          cbs            - Committed Burst Size limit (expected to be 2kBytes)
*          ebs        - Excess Burst Size limit ( 0 ~ 0xFFFFFF)
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case, see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_sw_get_uni_ingr_police_rate (uint32_t owner_id,
                                                  tpm_src_port_type_t  uni_port,
                                                  tpm_limit_mode_t    *count_mode,
                                                  uint32_t *cir,
                                                  uint32_t *cbs,
                                                  uint32_t *ebs)
{
    int               fd;
    tpm_ioctl_sw_tm_t tpm_sw_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_tm.owner_id = owner_id;
    tpm_sw_tm.uni_port = uni_port;

    tpm_sw_tm.sw_tm_cmd = MV_TPM_IOCTL_SW_SET_UNI_INGR_POLICE_RATE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_TM_SECTION, &tpm_sw_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *count_mode = tpm_sw_tm.limit_mode;
    *cir = tpm_sw_tm.cir;
    *cbs = tpm_sw_tm.cbs;
    *ebs = tpm_sw_tm.ebs;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_sw_set_uni_tc_ingr_police_rate()
*
* DESCRIPTION:      Configures an policer function for a traffic class.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* uni_port          - UNI port.
* tc                - traffic class
* cir
* cbs
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_uni_tc_ingr_police_rate (uint32_t            owner_id,
                                                     tpm_src_port_type_t uni_port,
                                                     uint32_t            tc,
                                                     uint32_t            cir,
                                                     uint32_t            cbs)
{
    int               fd;
    tpm_ioctl_sw_tm_t tpm_sw_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_tm.owner_id = owner_id;
    tpm_sw_tm.uni_port = uni_port;
    tpm_sw_tm.tc       = tc;
    tpm_sw_tm.cir      = cir;
    tpm_sw_tm.cbs      = cbs;

    tpm_sw_tm.sw_tm_cmd = MV_TPM_IOCTL_SW_UNI_TC_INGR_POLICE_RATE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_TM_SECTION, &tpm_sw_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_set_uni_egr_rate_limit()
*
* DESCRIPTION:      Configures the egress rate limit of an Ethernet UNI port.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* trg_port          - UNI port for fonfig the egress rate limit
* limit_mode      - limit mode:    TPM_SW_LIMIT_FRAME, TPM_SW_LIMIT_LAYER1,
*                                            TPM_SW_LIMIT_LAYER2, TPM_SW_LIMIT_LAYER3
* rate_limit_val    - egress rate limit value
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_sw_set_uni_egr_rate_limit (uint32_t owner_id,
                                                tpm_src_port_type_t  trg_port,
                                                tpm_limit_mode_t limit_mode,
                                                uint32_t             rate_limit_val)
{
    int               fd;
    tpm_ioctl_sw_tm_t tpm_sw_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_tm.owner_id       = owner_id;
    tpm_sw_tm.uni_port       = trg_port;
    tpm_sw_tm.limit_mode    = limit_mode;
    tpm_sw_tm.rate_limit_val = rate_limit_val;

    tpm_sw_tm.sw_tm_cmd = MV_TPM_IOCTL_SW_SET_UNI_EGR_RATE_LIMIT;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_TM_SECTION, &tpm_sw_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_sw_get_uni_egr_rate_limit
*
* DESCRIPTION:
*       The API return the egress frame rate limit of an Ethernet UNI lport
* INPUTS:
*       owner_id        - APP owner id , should be used for all API calls.
*       trg_lport        - uni lport for configuring the egress rate limit.

*
* OUTPUTS:
*      limit_mode      - limit mode:    TPM_SW_LIMIT_FRAME, TPM_SW_LIMIT_LAYER1,
*                                            TPM_SW_LIMIT_LAYER2, TPM_SW_LIMIT_LAYER3
*      rate_limit_val    - egress rate limit value
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case , see tpm_error_code_t.
*
* COMMENTS:
*       GT_ERATE_TYPE used kbRate - frame rate valid values are:
*                                    7600,..., 9600,
*                                    10000, 20000, 30000, 40000, ..., 100000,
*                                    110000, 120000, 130000, ..., 1000000.

*
*******************************************************************************/
tpm_error_code_t tpm_sw_get_uni_egr_rate_limit(uint32_t owner_id,
                                                tpm_src_port_type_t  trg_port,
                                                tpm_limit_mode_t *limit_mode,
                                                uint32_t *rate_limit_val)
{
    int fd;
    tpm_ioctl_sw_tm_t tpm_sw_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_tm.owner_id       = owner_id;
    tpm_sw_tm.uni_port       = trg_port;

    tpm_sw_tm.sw_tm_cmd = MV_TPM_IOCTL_SW_GET_UNI_EGR_RATE_LIMIT;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_TM_SECTION, &tpm_sw_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *limit_mode = tpm_sw_tm.limit_mode;
    *rate_limit_val = tpm_sw_tm.rate_limit_val;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_clear_port_counter
*
* DESCRIPTION:
*       The API clear port pm counter.
*
* INPUTS:
*       owner_id   - APP owner id should be used for all API calls.
*
*
* OUTPUTS:
*       none.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case  see tpm_error_code_t.
*
* COMMENTS:
*       None.
*
*******************************************************************************/

tpm_error_code_t tpm_sw_clear_port_counter
(
    uint32_t owner_id ,
    tpm_src_port_type_t port
 )
{
    int                     fd;
    tpm_ioctl_swport_pm_3_t tpm_ioctl_swport_pm_3;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_ioctl_swport_pm_3.owner_id = owner_id;
    tpm_ioctl_swport_pm_3.port     = port;


    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_COUNTER_CLEAR_SECTION, &tpm_ioctl_swport_pm_3))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);


    return TPM_RC_OK;
}



/******************************************************************************/
/********************************** Switch PHY port management  APIs **/
/******************************************************************************/

/*******************************************************************************
* tpm_phy_convert_port_index()
*
* DESCRIPTION:      convert switch port index to external port index.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* switch_port          - switch port index
*
* OUTPUTS:
* extern_port    - external port index
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_phy_convert_port_index (uint32_t  owner_id,
                                                uint32_t  switch_port,
                                                tpm_src_port_type_t *extern_port)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;

    /*add this port only for convenience, not use is kernel convert function*/
    tpm_sw_phy.port = TPM_SRC_PORT_UNI_0;

    tpm_sw_phy.switch_port_id = switch_port;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_CONVERT_PORT_INDEX;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *extern_port = tpm_sw_phy.extern_port_id;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_phy_set_port_autoneg_mode
*
* DESCRIPTION:
*       The API Configures the auto negotiation state of an Ethernet  lport.
* INPUTS:
*       owner_id          - APP owner id  should be used for all API calls.
*       port                - logical port  number for setting the auto negotiation state.
*       autoneg_state  - autonegotiation state, enabled or disabled.
*       autoneg_mode   - enum:
*                        SPEED_AUTO_DUPLEX_AUTO: Auto for both speed and duplex
*                        SPEED_1000_DUPLEX_AUTO: 1000Mbps and auto duplex
*                        SPEED_100_DUPLEX_AUTO:  100Mbps and auto duplex
*                        SPEED_10_DUPLEX_AUTO:   10Mbps and auto duplex
*                        SPEED_AUTO_DUPLEX_FULL: Auto for speed only and Full duplex
*                        SPEED_AUTO_DUPLEX_HALF: Auto for speed only and Half duplex. (1000Mbps is not supported)
*                        SPEED_1000_DUPLEX_FULL: 1000Mbps Full duplex.
*                        SPEED_1000_DUPLEX_HALF: 1000Mbps half duplex.
*                        SPEED_100_DUPLEX_FULL:  100Mbps Full duplex.
*                        SPEED_100_DUPLEX_HALF:  100Mbps half duplex.
*                        SPEED_10_DUPLEX_FULL:   10Mbps Full duplex.
*                        SPEED_10_DUPLEX_HALF:   10Mbps half duplex.
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
*
* COMMENTS:
*
*
*******************************************************************************/
tpm_error_code_t tpm_phy_set_port_autoneg_mode
(
     uint32_t owner_id,
     tpm_src_port_type_t port,
     bool                autoneg_state,
     tpm_autoneg_mode_t  autoneg_mode
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;
    tpm_sw_phy.port_autoneg_state = autoneg_state;
    tpm_sw_phy.port_autoneg_mode  = autoneg_mode;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_SET_AUTONEG_MODE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_phy_get_port_autoneg_mode
*
* DESCRIPTION:
*       The API return the the auto negotiation state of an Ethernet  lport.
* INPUTS:
*       owner_id          - APP owner id  should be used for all API calls.
*       lport        - logical port  number for getting the auto negotiation state.
*
*
* OUTPUTS:
*       autoneg_state  - autonegotiation state, enabled or disabled.
*       autoneg_mode   - enum:
*                        SPEED_AUTO_DUPLEX_AUTO: Auto for both speed and duplex
*                        SPEED_1000_DUPLEX_AUTO: 1000Mbps and auto duplex
*                        SPEED_100_DUPLEX_AUTO:  100Mbps and auto duplex
*                        SPEED_10_DUPLEX_AUTO:   10Mbps and auto duplex
*                        SPEED_AUTO_DUPLEX_FULL: Auto for speed only and Full duplex
*                        SPEED_AUTO_DUPLEX_HALF: Auto for speed only and Half duplex. (1000Mbps is not supported)
*                        SPEED_1000_DUPLEX_FULL: 1000Mbps Full duplex.
*                        SPEED_1000_DUPLEX_HALF: 1000Mbps half duplex.
*                        SPEED_100_DUPLEX_FULL:  100Mbps Full duplex.
*                        SPEED_100_DUPLEX_HALF:  100Mbps half duplex.
*                        SPEED_10_DUPLEX_FULL:   10Mbps Full duplex.
*                        SPEED_10_DUPLEX_HALF:   10Mbps half duplex.                               .
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
*
* COMMENTS:
*
*
*******************************************************************************/
tpm_error_code_t tpm_phy_get_port_autoneg_mode
(
      uint32_t owner_id,
      tpm_src_port_type_t port,
    bool                *autoneg_state,
    tpm_autoneg_mode_t  *autoneg_mode
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_GET_AUTONEG_MODE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *autoneg_state = tpm_sw_phy.port_autoneg_state;
    *autoneg_mode  = tpm_sw_phy.port_autoneg_mode;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_phy_restart_port_autoneg
*
* DESCRIPTION:
*       The API restart the auto negotiation of an Ethernet  lport.
* INPUTS:
*       owner_id          - APP owner id  should be used for all API calls.
*       port        - logical port  number for restarting the auto negotiation state.
*
*
* OUTPUTS:
*        NONE.
*                                .
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
*
* COMMENTS:
*
*
*******************************************************************************/
tpm_error_code_t tpm_phy_restart_port_autoneg
(
      uint32_t owner_id,
      tpm_src_port_type_t port
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_RESTART_AUTONEG;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_phy_set_port_admin_state
*
* DESCRIPTION:
*       The API Configures the PHY port  state of an Ethernet  lport.
* INPUTS:
*       owner_id          - APP owner id  should be used for all API calls.
*       port            - logical port to set.
*       phy_port_state  - PHY port  state to set.
*                     0:normal state
*                     1:power down
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
*
* COMMENTS:
*
*
*******************************************************************************/
tpm_error_code_t tpm_phy_set_port_admin_state
(
     uint32_t owner_id,
     tpm_src_port_type_t port,
     bool  phy_port_state
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;
    tpm_sw_phy.phy_port_state = phy_port_state;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_SET_PORT_STATE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_phy_get_port_admin_state
*
* DESCRIPTION:
*       The API return the PHY port  state of an Ethernet  lport.
* INPUTS:
*       owner_id          - APP owner id  should be used for all API calls.
*       port        - logical port  number for getting the PHY port state.
*
*
* OUTPUTS:
*        phy_port_state  -  0:normal state
*                       1:power down                                    .
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
*
* COMMENTS:
*
*
*******************************************************************************/
tpm_error_code_t tpm_phy_get_port_admin_state
(
      uint32_t owner_id,
      tpm_src_port_type_t port,
      bool * phy_port_state
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_GET_PORT_STATE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *phy_port_state = tpm_sw_phy.phy_port_state;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_phy_get_port_link_status
*
* DESCRIPTION:
*       The API return realtime port link status of an Ethernet  lport.
* INPUTS:
*       owner_id          - APP owner id  should be used for all API calls.
*       port        - logical port  number for getting the port link status.
*
*
* OUTPUTS:
*        port_link_status  -  0:port link is ON
*                        1:port link is DOWN                                    .
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
*
* COMMENTS:
*
*
*******************************************************************************/
tpm_error_code_t tpm_phy_get_port_link_status
(
      uint32_t owner_id,
      tpm_src_port_type_t port,
      bool * port_link_status
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_GET_LINK_STATUS;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);
    *port_link_status = tpm_sw_phy.port_link_status;
    return TPM_RC_OK;
}
/*******************************************************************************
*tpm_phy_get_port_duplex_status
*
* DESCRIPTION:
*       The API return realtime port duplex status of an Ethernet  lport.
* INPUTS:
*       owner_id          - APP owner id  should be used for all API calls.
*       port        - logical port  number for getting the port duplex status.
*
*
* OUTPUTS:
*        port_duplex_status  -  0:half deplex mode
*                               1:full deplex mode                    .
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case, see tpm_error_code_t.
*
* COMMENTS:
*
*
*******************************************************************************/
tpm_error_code_t tpm_phy_get_port_duplex_status
(
    uint32_t owner_id,
    tpm_src_port_type_t port,
    bool * port_duplex_status
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_GET_DUPLEX_STATUS;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *port_duplex_status = tpm_sw_phy.port_duplex_enable;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_phy_get_port_speed_mode
*
* DESCRIPTION:
*       The API return realtime port speed mode of an Ethernet  lport.
* INPUTS:
*       owner_id          - APP owner id  should be used for all API calls.
*       port        - logical port  number for getting the port speed mode.
*
*
* OUTPUTS:
*        port_duplex_status  -  0:10M
*                               1:100M
*                         2:1000M
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case, see tpm_error_code_t.
*
* COMMENTS:
*
*
*******************************************************************************/
tpm_error_code_t tpm_phy_get_port_speed_mode
(
    uint32_t owner_id,
    tpm_src_port_type_t port,
    tpm_phy_speed_t   *speed
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_GET_SPEED_MODE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *speed = tpm_sw_phy.port_speed;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_phy_set_port_flow_control_support
*
* DESCRIPTION:
*       This routine will set the pause bit in Autonegotiation Advertisement
*        Register. And restart the autonegotiation.
*
* INPUTS:
*              owner_id          - APP owner id  should be used for all API calls.
*        port -    The logical port number, unless SERDES device is accessed
*                The physical address, if SERDES device is accessed
*            state   -  false:port pause is off.
*                   true:port pause is on.
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
* COMMENTS:
* data sheet register 4.10 Autonegotiation Advertisement Register
*******************************************************************************/

tpm_error_code_t tpm_phy_set_port_flow_control_support
(
  uint32_t owner_id,
  tpm_src_port_type_t  port,
  bool state
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;
    tpm_sw_phy.port_pause_state = state;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_SET_PORT_FLOW_CONTROL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_phy_get_port_flow_control_support
*
* DESCRIPTION:
*       This routine will get the pause bit in Autonegotiation Advertisement
*        Register.
*
* INPUTS:
*              owner_id   - APP owner id  should be used for all API calls.
*        port -    The logical port number, unless SERDES device is accessed
*                The physical address, if SERDES device is accessed
*
*
* OUTPUTS:
*            state   -  false:port pause is off.
*                   true:port pause is on.
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
* COMMENTS:
* data sheet register 4.10 Autonegotiation Advertisement Register
*******************************************************************************/

tpm_error_code_t tpm_phy_get_port_flow_control_support
(
  uint32_t owner_id,
  tpm_src_port_type_t  port,
  bool *state
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_GET_PORT_FLOW_CONTROL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *state = tpm_sw_phy.port_pause_state;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_phy_get_port_flow_control_state
*
* DESCRIPTION:
*     This routine will get the current pause state.
*        Register.
*
* INPUTS:
*        port -    The logical port number, unless SERDES device is accessed
*                The physical address, if SERDES device is accessed
*
*
* OUTPUTS:
*        state -
*                false: MAC Pause not implemented in the link partner or in MyPause
*                true:MAC Pause is implemented in the link partner and in MyPause
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case, see tpm_error_code_t.
* COMMENTS:
*       None.
*******************************************************************************/
tpm_error_code_t tpm_phy_get_port_flow_control_state
(
    uint32_t owner_id,
    uint32_t  port,
    bool *state
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_GET_PORT_FC_STATE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *state = tpm_sw_phy.port_pause_state;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_phy_set_port_loopback
*
*
* INPUTS:
*      owner_id          - APP owner id  should be used for all API calls.
*        port -    The logical port number, unless SERDES device is accessed
*        mode - Internal or external loopback
* enable - If GT_TRUE, enable loopback mode
*                 If GT_FALSE, disable loopback mode
*
* OUTPUTS:
* None.
*
* RETURNS:
* GT_OK - on success
* GT_FAIL - on error
*
* COMMENTS:
* data sheet register 0.14 - Loop_back
*
*******************************************************************************/
tpm_error_code_t tpm_phy_set_port_loopback
(
  uint32_t owner_id,
  tpm_src_port_type_t  port,
  tpm_phy_loopback_mode_t mode,
  bool   enable
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;
    tpm_sw_phy.port_loopback_mode = mode;
    tpm_sw_phy.port_loopback_state = enable;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_SET_PORT_LOOPBACK;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_phy_get_port_loopback
*
* INPUTS:
*      owner_id          - APP owner id  should be used for all API calls.
*         port - The logical port number, unless SERDES device is accessed
*        The physical address, if SERDES device is accessed
*        mode - Internal or external loopback
*
* OUTPUTS:
* enable - If GT_TRUE,  loopback mode is enabled
* If GT_FALSE,  loopback mode is disabled
*
* RETURNS:
* GT_OK - on success
* GT_FAIL - on error
*
* COMMENTS:
* data sheet register 0.14 - Loop_back
*
*******************************************************************************/
tpm_error_code_t tpm_phy_get_port_loopback
(
  uint32_t owner_id,
  tpm_src_port_type_t  port,
  tpm_phy_loopback_mode_t mode,
  bool   *enable
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;
    tpm_sw_phy.port_loopback_mode = mode;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_GET_PORT_LOOPBACK;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *enable = tpm_sw_phy.port_loopback_state;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_phy_set_port_duplex_mode
*
* DESCRIPTION:
*         Sets duplex mode for a specific logical port. This function will keep
*        the speed and loopback mode to the previous value, but disable others,
*        such as Autonegotiation.
*
* INPUTS:
*          owner_id - APP owner id  should be used for all API calls.
*        port        - The logical port number, unless SERDES device is accessed
*         enable    - Enable/Disable dulpex mode
*
* OUTPUTS:
*         None.
*
* RETURNS:
*         GT_OK     - on success
*         GT_FAIL     - on error
*
* COMMENTS:
*         data sheet register 0.8 - Duplex Mode
*
*******************************************************************************/
tpm_error_code_t tpm_phy_set_port_duplex_mode
(
  uint32_t owner_id,
  tpm_src_port_type_t  port,
  bool   enable
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id = owner_id;
    tpm_sw_phy.port = port;
    tpm_sw_phy.port_duplex_enable = enable;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_SET_PORT_DUPLEX_MODE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_phy_get_port_duplex_mode
*
* DESCRIPTION:
*         Gets duplex mode for a specific logical port.
*
* INPUTS:
*             owner_id   - APP owner id  should be used for all API calls.
*        port -    The logical port number, unless SERDES device is accessed
*
* OUTPUTS:
*         enable    - Enable/Disable dulpex mode
*
* RETURNS:
*         GT_OK     - on success
*         GT_FAIL     - on error
*
* COMMENTS:
*         data sheet register 0.8 - Duplex Mode
*
*******************************************************************************/
tpm_error_code_t tpm_phy_get_port_duplex_mode
(
  uint32_t owner_id,
  tpm_src_port_type_t  port,
  bool*   enable
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id = owner_id;
    tpm_sw_phy.port = port;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_GET_PORT_DUPLEX_MODE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *enable = tpm_sw_phy.port_duplex_enable;

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_phy_set_port_speed
*
* DESCRIPTION:
*       This routine will set PHY port speed.
*
* INPUTS:
*              owner_id          - APP owner id  should be used for all API calls.
*                port -    The logical port number, unless SERDES device is accessed
*                            The physical address, if SERDES device is accessed
*                speed -    PHY_SPEED_10_MBPS -10 Mbps
*                                PHY_SPEED_100_MBPS -100 Mbps
*                                 PHY_SPEED_1000_MBPS -100 Mbps.
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
* COMMENTS:
*******************************************************************************/

tpm_error_code_t tpm_phy_set_port_speed
(
  uint32_t owner_id,
  tpm_src_port_type_t  port,
  tpm_phy_speed_t speed
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;
    tpm_sw_phy.port_speed = speed;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_SET_PORT_SPEED;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_phy_get_port_speed
*
* DESCRIPTION:
*       This routine will get current PHY port speed .
*
* INPUTS:
*              owner_id   - APP owner id  should be used for all API calls.
*                port -    The logical port number, unless SERDES device is accessed
*                            The physical address, if SERDES device is accessed
*
*
* OUTPUTS:
*                speed -    PHY_SPEED_10_MBPS -10 Mbps
*                                PHY_SPEED_100_MBPS -100 Mbps
*                                 PHY_SPEED_1000_MBPS -100 Mbps.
* RETURNS:
*       On success -  TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
* COMMENTS:
* data sheet register 4.10 Autonegotiation Advertisement Register
*******************************************************************************/

tpm_error_code_t tpm_phy_get_port_speed
(
  uint32_t owner_id,
  tpm_src_port_type_t  port,
  tpm_phy_speed_t *speed
)
{
    int fd;
    tpm_ioctl_sw_phy_t tpm_sw_phy;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_phy.owner_id       = owner_id;
    tpm_sw_phy.port = port;

    tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_SW_PHY_GET_PORT_SPEED;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *speed = tpm_sw_phy.port_speed;

    return TPM_RC_OK;
}

/******************************************************************************/
/********************************** Traffic management packet processor APIs **/
/******************************************************************************/

/*******************************************************************************
* tpm_tm_set_wan_egr_queue_sched()
*
* DESCRIPTION:      Configures the scheduling mode per WAN queue.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* sched_ent         - entity for setting the scheduling mode: ex:TPM_TRG_PORT_WAN
* sched_mode        - scheduler mode per port: strict(0) / wrr(1)
* queue_id          - queue number
* wrr_weight        - weight value when WRR scheduling (1-256)
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_tm_set_wan_egr_queue_sched (uint32_t            owner_id,
                                                 tpm_trg_port_type_t sched_ent,
                                                 tpm_pp_sched_type_t sched_mode,
                                                 uint8_t             queue_id,
                                                 uint16_t            wrr_weight)
{
    int               fd;
    tpm_ioctl_tm_tm_t tpm_tm_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_tm_tm.owner_id      = owner_id;
    tpm_tm_tm.sched_ent     = sched_ent;
    tpm_tm_tm.pp_sched_mode = sched_mode;
    tpm_tm_tm.queue_id      = queue_id;
    tpm_tm_tm.wrr_weight    = wrr_weight;

    tpm_tm_tm.pp_tm_cmd = MV_TPM_IOCTL_TM_SET_WAN_EGR_QUE_SCHED;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PP_TM_SECTION, &tpm_tm_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_tm_set_wan_ingr_queue_sched()
*
* DESCRIPTION:      Configures the scheduling mode per all downstream traffic from the WAN.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* sched_mode        - scheduler mode per port: strict(0) / wrr(1)
* queue_id          - queue number
* wrr_weight        - weight value when WRR scheduling (1-256)
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_tm_set_wan_ingr_queue_sched(uint32_t            owner_id,
                                                 tpm_pp_sched_type_t sched_mode,
                                                 uint8_t             queue_id,
                                                 uint16_t            wrr_weight)
{
    int               fd;
    tpm_ioctl_tm_tm_t tpm_tm_tm;


    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_tm_tm.owner_id      = owner_id;
    tpm_tm_tm.pp_sched_mode = sched_mode;
    tpm_tm_tm.queue_id      = queue_id;
    tpm_tm_tm.wrr_weight    = wrr_weight;


    tpm_tm_tm.pp_tm_cmd = MV_TPM_IOCTL_TM_SET_WAN_INGR_QUE_SCHED;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PP_TM_SECTION, &tpm_tm_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_tm_set_wan_sched_egr_rate_lim()
*
* DESCRIPTION:      Configures the egress rate limit of upstream traffic.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* sched_ent         - entity for setting the rate limit: ex:TPM_TRG_PORT_WAN
* rate_limit_val    - egress rate limit value
* bucket_size       - bucket size value
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_tm_set_wan_sched_egr_rate_lim(uint32_t            owner_id,
                                                   tpm_trg_port_type_t sched_ent,
                                                   uint32_t            rate_limit_val,
                                                   uint32_t            bucket_size)
{
    int               fd;
    tpm_ioctl_tm_tm_t tpm_tm_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    if (rate_limit_val<TPM_RATE_LIMIT_MIN_VAL) {
        printf("%s: Failed to set rate limit %d\n\r", __FUNCTION__, rate_limit_val );
        return ERR_SW_TM_RATE_LIMIT_INVALID;
    }

    tpm_tm_tm.owner_id       = owner_id;
    tpm_tm_tm.sched_ent      = sched_ent;
    tpm_tm_tm.rate_limit_val = rate_limit_val;
    tpm_tm_tm.bucket_size    = bucket_size;

    tpm_tm_tm.pp_tm_cmd = MV_TPM_IOCTL_TM_SET_WAN_SCHED_EGR_RATE_LIMIT;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PP_TM_SECTION, &tpm_tm_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_tm_set_wan_queue_egr_rate_lim()
*
* DESCRIPTION:      Configures the upstream traffic egress rate limit for a specific
*                   queue of an upstream scheduling entity.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* sched_ent         - entity for setting the rate limit: ex:TPM_TRG_PORT_WAN
* queue_id          - queue number
* rate_limit_val    - egress rate limit value
* bucket_size       - bucket size value
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_tm_set_wan_queue_egr_rate_lim(uint32_t            owner_id,
                                                   tpm_trg_port_type_t sched_ent,
                                                   uint32_t            queue_id,
                                                   uint32_t            rate_limit_val,
                                                   uint32_t            bucket_size)
{
    int               fd;
    tpm_ioctl_tm_tm_t tpm_tm_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    if (rate_limit_val<TPM_RATE_LIMIT_MIN_VAL) {
        printf("%s: Failed to set rate limit %d\n\r", __FUNCTION__, rate_limit_val );
        return ERR_SW_TM_RATE_LIMIT_INVALID;
    }

    tpm_tm_tm.owner_id       = owner_id;
    tpm_tm_tm.sched_ent      = sched_ent;
    tpm_tm_tm.queue_id       = queue_id;
    tpm_tm_tm.rate_limit_val = rate_limit_val;
    tpm_tm_tm.bucket_size    = bucket_size;

    tpm_tm_tm.pp_tm_cmd = MV_TPM_IOCTL_TM_SET_WAN_QUE_EGR_RATE_LIMIT;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PP_TM_SECTION, &tpm_tm_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_tm_set_wan_ingr_rate_lim()
*
* DESCRIPTION:      Configures the rate limit of all downstream traffic from the WAN.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* rate_limit_val    - ingress rate limit value
* bucket_size       - bucket size value
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_tm_set_wan_ingr_rate_lim (uint32_t owner_id,
                                               uint32_t rate_limit_val,
                                               uint32_t bucket_size)
{
    int               fd;
    tpm_ioctl_tm_tm_t tpm_tm_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    if (rate_limit_val<TPM_RATE_LIMIT_MIN_VAL) {
        printf("%s: Failed to set rate limit %d\n\r", __FUNCTION__, rate_limit_val );
        return ERR_SW_TM_RATE_LIMIT_INVALID;
    }

    tpm_tm_tm.owner_id       = owner_id;
    tpm_tm_tm.rate_limit_val = rate_limit_val;
    tpm_tm_tm.bucket_size    = bucket_size;

    tpm_tm_tm.pp_tm_cmd = MV_TPM_IOCTL_TM_SET_WAN_INGR_RATE_LIMIT;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PP_TM_SECTION, &tpm_tm_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_tm_set_wan_q_ingr_rate_lim()
*
* DESCRIPTION:      Configures the egress rate limit of a specific queue for
*                   downstream traffic.
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* queue_id          - queue number
* rate_limit_val    - ingress rate limit value
* bucket_size       - bucket size value
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_tm_set_wan_q_ingr_rate_lim (uint32_t owner_id,
                                                 uint32_t queue_id,
                                                 uint32_t rate_limit_val,
                                                 uint32_t bucket_size)
{
    int               fd;
    tpm_ioctl_tm_tm_t tpm_tm_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    if (rate_limit_val<TPM_RATE_LIMIT_MIN_VAL) {
        printf("%s: Failed to set rate limit %d\n\r", __FUNCTION__, rate_limit_val );
        return ERR_SW_TM_RATE_LIMIT_INVALID;
    }
    tpm_tm_tm.owner_id       = owner_id;
    tpm_tm_tm.queue_id       = queue_id;
    tpm_tm_tm.rate_limit_val = rate_limit_val;
    tpm_tm_tm.bucket_size    = bucket_size;

    tpm_tm_tm.pp_tm_cmd = MV_TPM_IOCTL_TM_SET_WAN_QUE_INGR_RATE_LIMIT;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PP_TM_SECTION, &tpm_tm_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/******************************************************************************/
/********************************** Igmp APIs *********************************/
/******************************************************************************/

tpm_error_code_t tpm_set_port_igmp_frwd_mode(tpm_src_port_type_t  src_port,
                                                        tpm_igmp_frwd_mode_t mode)
{
    int              fd;
    tpm_ioctl_igmp_t tpm_igmp;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    memset(&tpm_igmp, 0, sizeof(tpm_igmp));

    tpm_igmp.src_port  = src_port;
    tpm_igmp.frwd_mode = mode;

    tpm_igmp.igmp_cmd  = MV_TPM_IOCTL_IGMP_SET_PORT_FRWD_MODE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_IGMP_SECTION, &tpm_igmp))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

tpm_error_code_t tpm_get_port_igmp_frwd_mode(tpm_src_port_type_t   src_port,
                                                        tpm_igmp_frwd_mode_t *mode)
{
    int              fd;
    tpm_ioctl_igmp_t tpm_igmp;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    memset(&tpm_igmp, 0, sizeof(tpm_igmp));

    tpm_igmp.src_port = src_port;

    tpm_igmp.igmp_cmd = MV_TPM_IOCTL_IGMP_GET_PORT_FRWD_MODE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_IGMP_SECTION, &tpm_igmp))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *mode = tpm_igmp.frwd_mode;

    return TPM_RC_OK;
}

tpm_error_code_t tpm_set_igmp_cpu_rx_queue(uint32_t queue)
{
    int              fd;
    tpm_ioctl_igmp_t tpm_igmp;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    memset(&tpm_igmp, 0, sizeof(tpm_igmp));

    tpm_igmp.cpu_queue = queue;

    tpm_igmp.igmp_cmd  = MV_TPM_IOCTL_IGMP_SET_CPU_RX_Q;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_IGMP_SECTION, &tpm_igmp))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

tpm_error_code_t tpm_get_igmp_cpu_rx_queue(uint32_t owner_id, uint32_t *queue)
{
    int              fd;
    tpm_ioctl_igmp_t tpm_igmp;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    memset(&tpm_igmp, 0, sizeof(tpm_igmp));

    tpm_igmp.owner_id = owner_id;

    tpm_igmp.igmp_cmd = MV_TPM_IOCTL_IGMP_GET_CPU_RX_Q;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_IGMP_SECTION, &tpm_igmp))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *queue = tpm_igmp.cpu_queue;

    return TPM_RC_OK;
}

tpm_error_code_t tpm_set_port_igmp_proxy_sa_mac(uint8_t *sa_mac)
{
    int              fd;
    tpm_ioctl_igmp_t tpm_igmp;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    memset(&tpm_igmp, 0, sizeof(tpm_igmp));
    memcpy(tpm_igmp.sa_mac, sa_mac, 6 * sizeof(uint8_t));

    tpm_igmp.igmp_cmd = MV_TPM_IOCTL_SET_IGMP_PROXY_SA_MAC;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_IGMP_SECTION, &tpm_igmp))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

tpm_error_code_t tpm_get_port_igmp_proxy_sa_mac(uint8_t *sa_mac)
{
    int              fd;
    tpm_ioctl_igmp_t tpm_igmp;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    memset(&tpm_igmp, 0, sizeof(tpm_igmp));

    tpm_igmp.igmp_cmd = MV_TPM_IOCTL_GET_IGMP_PROXY_SA_MAC;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_IGMP_SECTION, &tpm_igmp))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    memcpy(sa_mac, tpm_igmp.sa_mac, 6 * sizeof(uint8_t));

    return TPM_RC_OK;
}

#if 0
tpm_error_code_t tpm_proc_enable_igmp (uint32_t owner_id)
{
    int              fd;
    tpm_ioctl_igmp_t tpm_igmp;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_igmp.owner_id = owner_id;
    tpm_igmp.igmp_cmd = MV_TPM_IOCTL_IGMP_ENABLE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_IGMP_SECTION, &tpm_igmp))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

tpm_error_code_t tpm_proc_disable_igmp (uint32_t owner_id)
{
    int              fd;
    tpm_ioctl_igmp_t tpm_igmp;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_igmp.owner_id = owner_id;
    tpm_igmp.igmp_cmd = MV_TPM_IOCTL_IGMP_DISABLE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_IGMP_SECTION, &tpm_igmp))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
#endif

/******************************************************************************/
/********************************** Print APIs ********************************/
/******************************************************************************/

void tpm_print_etherports(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_ETHER_PORTS;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_tx_modules(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_TX_MODULES;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}


void tpm_print_rx_modules(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_RX_MODULES;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_gmac_config(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_GMAC_CONFIG;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_gmac_func(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_GMAC_FUNC;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_igmp(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_IGMP;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_misc(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_MISC;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_owners(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_OWNERS;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}


void tpm_print_vlan_etype(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_VLAN_TYPE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_valid_api_ranges(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_VALID_API_SECTIONS;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_full_api_range(tpm_api_sections_t api_section, uint32_t dir)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.api_section = api_section;
    tpm_print.dir         = dir;
    tpm_print.print_cmd   = MV_TPM_IOCTL_PRINT_FULL_API_SECTIONS;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_mod_shadow_range(tpm_gmacs_enum_t gmac,
                                uint32_t valid_only,
                                uint32_t start,
                                uint32_t end)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.gmac  = gmac;
    tpm_print.valid = valid_only;
    tpm_print.start = start;
    tpm_print.end   = end;
    tpm_print.print_cmd   = MV_TPM_IOCTL_PRINT_MOD_SHADOW_RANGE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_pnc_shadow_range(uint32_t valid_only,
                                uint32_t start,
                                uint32_t end)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.valid = valid_only;
    tpm_print.start = start;
    tpm_print.end   = end;
    tpm_print.print_cmd   = MV_TPM_IOCTL_PRINT_PNC_SHADOW_RANGE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_valid_pnc_ranges(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_VALID_PNC_RANGES;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

void tpm_print_init_tables(void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_INIT_TABLES;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);
}

int32_t tpm_mod2_print_tmp_pattern (void)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_MOD2_TMP_PATTERN;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);

    return TPM_OK;
}

int32_t tpm_mod2_print_jump_range (tpm_gmacs_enum_t gmac_port,
                                   uint32_t         mod_entry,
                                   uint32_t         num)
{

    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_print.gmac  = gmac_port;
    tpm_print.entry = mod_entry;
    tpm_print.num   = num;
    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_MOD2_JUMP_RANGE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);

    return TPM_OK;
}

int32_t tpm_mod2_print_jump_all (tpm_gmacs_enum_t gmac_port)
{

    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_print.gmac  = gmac_port;
    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_MOD2_JUMP_ALL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);

    return TPM_OK;
}

int32_t tpm_mod2_print_main_range (tpm_gmacs_enum_t gmac_port,
                                   uint32_t         mod_entry,
                                   uint32_t         num)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_print.gmac  = gmac_port;
    tpm_print.entry = mod_entry;
    tpm_print.num   = num;
    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_MOD2_MAIN_RANGE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);

    return TPM_OK;
}

int32_t tpm_mod2_print_main_all (tpm_gmacs_enum_t gmac_port)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_print.gmac  = gmac_port;
    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_MOD2_MAIN_ALL;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);

    return TPM_OK;
}


int32_t tpm_mod2_print_config (tpm_gmacs_enum_t gmac_port)
{

    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_print.gmac  = gmac_port;
    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_MOD2_CFG;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);

    return TPM_OK;
}

int32_t tpm_mod2_print_rule (tpm_gmacs_enum_t gmac_port,
                             uint32_t         mod_entry)
{
    int               fd;
    tpm_ioctl_print_t tpm_print;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_print.gmac  = gmac_port;
    tpm_print.entry = mod_entry;
    tpm_print.print_cmd = MV_TPM_IOCTL_PRINT_MOD2_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PRINT_SECTION, &tpm_print))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
    }
    osSemGive(tpmApiSemId);

    return TPM_OK;
}

/******************************************************************************/
/********************************** Trace APIs ********************************/
/******************************************************************************/

tpm_error_code_t tpm_trace_status_print(void)
{
    int               fd;
    tpm_ioctl_trace_t tpm_trace;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_trace.trace_cmd = MV_TPM_IOCTL_TRACE_STATUS;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_TRACE_SECTION, &tpm_trace))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

tpm_error_code_t tpm_trace_set(uint32_t level)
{
    int               fd;
    tpm_ioctl_trace_t tpm_trace;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_trace.level     = level;
    tpm_trace.trace_cmd = MV_TPM_IOCTL_TRACE_SET;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_TRACE_SECTION, &tpm_trace))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

tpm_error_code_t tpm_trace_module_set(uint32_t module, uint32_t  flag)
{
    int               fd;
    tpm_ioctl_trace_t tpm_trace;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_trace.module    = module;
    tpm_trace.flag      = flag;
    tpm_trace.trace_cmd = MV_TPM_IOCTL_TRACE_MODULE_SET;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_TRACE_SECTION, &tpm_trace))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/******************************************************************************/
/********************************** MOD2 APIs  ********************************/
/******************************************************************************/
int32_t tpm_mod_entry_set  (tpm_trg_port_type_t  trg_port,
                            tpm_pkt_mod_bm_t     bm,
                            tpm_pkt_mod_int_bm_t int_bm,
                            tpm_pkt_mod_t        *mod_data,
                            uint32_t             *mod_entry)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_mod_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_mod_rule.mod_rule.trg_port  = trg_port;
    tpm_add_mod_rule.mod_rule.mod_bm    = bm;
    tpm_add_mod_rule.mod_rule.int_bm    = int_bm;

    memcpy(&(tpm_add_mod_rule.mod_rule.mod_data), mod_data, sizeof(tpm_pkt_mod_t));

    tpm_add_mod_rule.add_acl_cmd = MV_TPM_IOCTL_SET_MOD_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_mod_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *mod_entry = tpm_add_mod_rule.rule_idx;

    return TPM_OK;
}

int32_t tpm_mod_entry_get   (tpm_trg_port_type_t    trg_port,
                             uint32_t               mod_entry,
                             uint16_t               *valid_cmds,
                             uint16_t               *pnc_ref,
                             tpm_mod_rule_t         *rule)
{
    int                      fd;
    tpm_ioctl_get_mod_rule_t tpm_get_mod_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_get_mod_rule.trg_port       = trg_port;
    tpm_get_mod_rule.rule_idx       = mod_entry;
    tpm_get_mod_rule.get_mod_cmd    = MV_TPM_IOCTL_GET_MOD_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_GET_MOD_SECTION, &tpm_get_mod_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *valid_cmds = tpm_get_mod_rule.valid_num;
    *pnc_ref    = tpm_get_mod_rule.pnc_ref;
    memcpy(rule, &(tpm_get_mod_rule.rule), sizeof(tpm_mod_rule_t));

    return TPM_OK;
}

int32_t tpm_mod_entry_del (tpm_trg_port_type_t  trg_port,
                           uint32_t             mod_entry)
{
    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_mod_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_mod_rule.trg_port    = trg_port;
    tpm_del_mod_rule.rule_idx    = mod_entry;
    tpm_del_mod_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_MOD_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_mod_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

int32_t tpm_mod_mac_inv (tpm_trg_port_type_t  trg_port)
{
    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_mod_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_mod_rule.trg_port    = trg_port;
    tpm_del_mod_rule.del_acl_cmd = MV_TPM_IOCTL_INV_MOD_RULES;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_mod_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}


/******************************************************************************/
/********************************** MIB Reset APIs ****************************/
/******************************************************************************/
tpm_error_code_t tpm_mib_reset(uint32_t owner_id, tpm_reset_level_enum_t reset_level)
{
    int               fd;
    tpm_ioctl_mib_reset_t tpm_mib_reset_param;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_mib_reset_param.owner_id     = owner_id;
    tpm_mib_reset_param.reset_level  = reset_level;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_RESET_SECTION, &tpm_mib_reset_param))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);
    return TPM_RC_OK;
}

/******************************************************************************/
/**********************************CTC EPON  Alarm APIs ****************************/
/******************************************************************************/
tpm_error_code_t tpm_alarm_get_eth_port
(
     uint32_t owner_id,
     uint32_t *alarm_type,
     uint8_t *port_bitmap
)
{
    int               fd;
    tpm_ioctl_alarm_t tpm_alarm;
    memset(&tpm_alarm, 0, sizeof(tpm_ioctl_alarm_t));

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_alarm.owner_id     = owner_id;
    tpm_alarm.alarm_cmd  = MV_TPM_IOCTL_ALARM_GET_ETH_PORT;

    memset((uint8_t *)&(tpm_alarm.port_bitmap[0][0]), 0, sizeof(tpm_alarm.port_bitmap));

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ALARM_SECTION, &tpm_alarm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *alarm_type = tpm_alarm.alarm_type;
    memcpy(port_bitmap, (uint8_t *)&(tpm_alarm.port_bitmap[0][0]), sizeof(tpm_alarm.port_bitmap));

    return TPM_RC_OK;
}

/******************************************************************************/
/************************** Switch Port PM APIs *******************************/
/******************************************************************************/
/*******************************************************************************
* tpm_sw_pm_1_read
*
*
* INPUTS:
*      owner_id          - APP owner id  should be used for all API calls.
*       port              - The logical port number
*      tpm_swport_pm_3   - Holds PM data
*
* OUTPUTS:
* PM data is supplied structure.
*
* RETURNS:
* TPM_RC_OK - on success, else error code
*
*******************************************************************************/
tpm_error_code_t tpm_sw_pm_1_read(uint32_t owner_id, tpm_src_port_type_t port, tpm_swport_pm_1_t *tpm_swport_pm_1)
{
    int                     fd;
    tpm_ioctl_swport_pm_1_t tpm_ioctl_swport_pm_1;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_ioctl_swport_pm_1.owner_id = owner_id;
    tpm_ioctl_swport_pm_1.port     = port;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_APM_IOCTL_PM_1_SWPORT_SECTION, &tpm_ioctl_swport_pm_1))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    memcpy(tpm_swport_pm_1, &tpm_ioctl_swport_pm_1.tpm_swport_pm_1, sizeof(tpm_ioctl_swport_pm_1.tpm_swport_pm_1));

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_pm_3_read
*
*
* INPUTS:
*      owner_id          - APP owner id  should be used for all API calls.
*       port              - The logical port number
*      tpm_swport_pm_3   - Holds PM data
*
* OUTPUTS:
* PM data is supplied structure.
*
* RETURNS:
* TPM_RC_OK - on success, else error code
*
*******************************************************************************/
tpm_error_code_t tpm_sw_pm_3_read(uint32_t owner_id, tpm_src_port_type_t port, tpm_swport_pm_3_all_t *tpm_swport_pm_3)
{
    int                     fd;
    tpm_ioctl_swport_pm_3_t tpm_ioctl_swport_pm_3;

    tpm_ioctl_swport_pm_3.owner_id = owner_id;
    tpm_ioctl_swport_pm_3.port     = port;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_ioctl_swport_pm_3.owner_id = owner_id;
    tpm_ioctl_swport_pm_3.port     = port;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_APM_IOCTL_PM_3_SWPORT_SECTION, &tpm_ioctl_swport_pm_3))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    memcpy(tpm_swport_pm_3, &tpm_ioctl_swport_pm_3.tpm_swport_pm_3, sizeof(tpm_ioctl_swport_pm_3.tpm_swport_pm_3));

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_tx_igmp_frame()
*
* DESCRIPTION: The API sends IGMP frames to either LAN or WAN side
*
* INPUTS:   owner_id    - owner id
*           tgt_port    - target port, LLID0-7/TCONT0-7/UNI0-3
*           tgt_queue   - target queue, 0-7
*           gem_port    - target gem port ID
*           buf         - Message buffer allocated by user
*           len         - Len of this buffer
*
* OUTPUTS:
*           NONE
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*           None
*
*******************************************************************************/
tpm_error_code_t tpm_tx_igmp_frame(uint32_t owner_id, tpm_trg_port_type_t  tgt_port,uint8_t tgt_queue, uint16_t gem_port, uint8_t *buf, uint16_t len)
{
    int     fd;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

     tpm_ioctl_tx_igmp_t tpm_ioctl_tx_igmp;

     tpm_ioctl_tx_igmp.owner_id = owner_id;
     tpm_ioctl_tx_igmp.tgt_port = tgt_port;
     tpm_ioctl_tx_igmp.tgt_queue = tgt_queue;
     tpm_ioctl_tx_igmp.gem_port = gem_port;
     tpm_ioctl_tx_igmp.len = len;
     tpm_ioctl_tx_igmp.dummy = 0;
     memcpy(tpm_ioctl_tx_igmp.buf, buf, len);

    if (ioctl(fd, MV_TPM_IOCTL_TX_IGMP, &tpm_ioctl_tx_igmp))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_rx_igmp_frame()
*
* DESCRIPTION:      receive OAM frame from specific LLID.
*
* INPUTS:
*                   owner_id          - APP owner id  should be used for all API calls.
*                    llid              - The logical port number
*                   src_port          - Used to identify where the packet comes from.
*                    data              - Buff
*                   length            - Buff length
*
* OUTPUTS:            none
*
* RETURNS:          On success, returns MRVL_OK.
*                    On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
tpm_error_code_t tpm_rx_igmp_frame(uint32_t owner_id, uint16_t *llid, tpm_src_port_type_t *src_port, uint8_t *buf, uint32_t *len)
{
    int                     fd;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

     tpm_ioctl_rx_igmp_t tpm_ioctl_rx_igmp;

     tpm_ioctl_rx_igmp.owner_id = owner_id;


    if (ioctl(fd, MV_TPM_IOCTL_RX_IGMP, &tpm_ioctl_rx_igmp))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }

    *llid = tpm_ioctl_rx_igmp.src_llid;
    *src_port = tpm_ioctl_rx_igmp.src_port;
    *len = tpm_ioctl_rx_igmp.len;
    memcpy(buf, tpm_ioctl_rx_igmp.buf, *len);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_set_mtu_enable()
*
* DESCRIPTION:      enable or disable MTU checking.
*
* INPUTS:
*                   enable            - set MTU checking to enable or disable.
*
* OUTPUTS:            none
*
* RETURNS:          On success, returns MRVL_OK.
*                    On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
tpm_error_code_t tpm_set_mtu_enable(
    tpm_init_mtu_setting_enable_t  enable
)
{
    int                     fd;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

     tpm_ioctl_mtu_t mtu_setting;

     mtu_setting.enable = enable;
     mtu_setting.mtu_setting_cmd = MV_TPM_IOCTL_SET_MTU_ADMIN;

    if (ioctl(fd, MV_TPM_IOCTL_MTU_SECTION, &mtu_setting))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }

    return TPM_RC_OK;

}

/*******************************************************************************
* tpm_set_mtu()
*
* DESCRIPTION:      set the MTU value for PNC to check.
*
* INPUTS:
*                   mtu               - MTU value
*                   ethertype         - IPv4 or IPv6
*                   direction         - US or DS
* OUTPUTS:            none
*
* RETURNS:          On success, returns MRVL_OK.
*                    On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
tpm_error_code_t tpm_set_mtu(
    uint32_t                  mtu,
    tpm_mtu_ethertype_t       ethertype,
    uint32_t                 direction
)
{
    int                     fd;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

     tpm_ioctl_mtu_t mtu_setting;

     mtu_setting.mtu= mtu;
     mtu_setting.direction = direction;
     mtu_setting.ethertype = ethertype;
     mtu_setting.mtu_setting_cmd = MV_TPM_IOCTL_SET_MTU;


    if (ioctl(fd, MV_TPM_IOCTL_MTU_SECTION, &mtu_setting))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_set_pppoe_mtu()
*
* DESCRIPTION:      set the PPPoE packets MTU value for PNC to check.
*
* INPUTS:
*                   pppoe_mtu         - MTU value
*                   ethertype         - IPv4 or IPv6
*                   direction         - US or DS
* OUTPUTS:            none
*
* RETURNS:          On success, returns MRVL_OK.
*                    On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
tpm_error_code_t tpm_set_pppoe_mtu(
    uint32_t                  pppoe_mtu,
    tpm_mtu_ethertype_t       ethertype,
    uint32_t                  direction
)
{
    int                     fd;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

     tpm_ioctl_mtu_t mtu_setting;

     mtu_setting.pppoe_mtu= pppoe_mtu;
     mtu_setting.direction = direction;
     mtu_setting.ethertype = ethertype;
     mtu_setting.mtu_setting_cmd = MV_TPM_IOCTL_SET_PPPOE_MTU;


    if (ioctl(fd, MV_TPM_IOCTL_MTU_SECTION, &mtu_setting))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }

    return TPM_RC_OK;

}

/*******************************************************************************
* tpm_set_mc_vid_port_vids()
*
* DESCRIPTION:      set MC VID configuration.
*
* INPUTS:
*                   owner_id                  -
*                   mc_vid                    -
*                   mc_vid_uniports_config    - vlan port config
* OUTPUTS:            none
*
* RETURNS:          On success, returns MRVL_OK.
*                    On error, returns MRVL_ERROR_EXIT.
*
*******************************************************************************/
tpm_error_code_t tpm_set_mc_vid_port_vids(
    uint32_t            owner_id,
    uint32_t            mc_vid,
    tpm_mc_vid_port_vid_set_t * mc_vid_uniports_config
)
{
    int                     fd;
    tpm_ioctl_mc_vid_t             tpm_ioctl_mc_vid;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

     tpm_ioctl_mc_vid.mc_vid = mc_vid;
     tpm_ioctl_mc_vid.owner_id = owner_id;
     tpm_ioctl_mc_vid.mc_vid_cmd = MV_TPM_IOCTL_SET_MC_VID_PORT_VIDS;
     memcpy(&(tpm_ioctl_mc_vid.port_vid_set), mc_vid_uniports_config,sizeof(tpm_mc_vid_port_vid_set_t));


    if (ioctl(fd, MV_TPM_IOCTL_MC_VLAN_SECTION, &tpm_ioctl_mc_vid))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_add_ipv6_nh_rule()
*
* DESCRIPTION:      Creates a new IPv6 NH processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* rule_num           - Entry index to be added in the current ACL
* parse_flags_bm     - Bitmap containing the significant flags result of the primary ACL filtering.
*                      possible values for IPv6 NH API:
*                         TPM_PARSE_FLAG_MTM_MASK|TPM_PARSE_FLAG_TO_CPU_MASK
* nh                 - Information to create a NH parsing key for the rule.
* pkt_frwd           - Information for packet forwarding decision.
* rule_action        - Action associated to the rule = drop/set target/set packet modification/to CPU
*                      and the next phase (for GWY only).
*                      possible "next_phase" for IPv6 NH API   ->  STAGE_IPV6_L4,STAGE_DONE
* OUTPUTS:
*  rule_idx         - Unique rule identification number, which is used when deleting the rule. (this is not the rule_num)
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t tpm_add_ipv6_nh_rule(
    uint32_t            owner_id,
    uint32_t            rule_num,
    uint32_t           *rule_idx,
    tpm_parse_flags_t   parse_flags_bm,
    tpm_nh_iter_t       nh_iter,
    uint32_t            nh,
    tpm_pkt_frwd_t     *pkt_frwd,
    tpm_rule_action_t  *rule_action
)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id                 = owner_id;
    tpm_add_acl_rule.rule_num                 = rule_num;
    tpm_add_acl_rule.parse_rule_bm            = parse_flags_bm;
	tpm_add_acl_rule.ipv6_nh_acl_rule.nh_iter = nh_iter;
    tpm_add_acl_rule.ipv6_nh_acl_rule.nh      = nh;
    if (pkt_frwd != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_nh_acl_rule.pkt_frwd),    pkt_frwd,    sizeof(tpm_pkt_frwd_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_nh_acl_rule.pkt_frwd),    0,    sizeof(tpm_pkt_frwd_t));
    }
    if (rule_action != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_nh_acl_rule.rule_action), rule_action, sizeof(tpm_rule_action_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_nh_acl_rule.rule_action), 0, sizeof(tpm_rule_action_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_IPV6_NH_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_del_ipv6_nh_rule()
*
* DESCRIPTION:      Deletes an existng IPv6 NH processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* rule_idx           - Unique rule idenitifcation number specifying the rule to be deleted.
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_del_ipv6_nh_rule  (  uint32_t            owner_id,
                                             uint32_t            rule_idx)

{

    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id      = owner_id;
    tpm_del_acl_rule.rule_idx      = rule_idx;

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_IPV6_NH_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_add_ipv6_gen_rule()
*
* DESCRIPTION:      Creates a new IPv6 gen processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* src_port           - The packet originating source port. The parameter has a double function:
*                       . The source port is part of the rules parsing key.
*                       . The source port determines if the acl entry being
*                         created is for the upstream acl or the downstream acl.
*                      In case of an upstream entry, the parameter determines if the packet
*                      arrives from a specific LAN port or ANY LAN port.
* rule_num           - Entry index to be added in the current ACL
* parse_rule_bm      - Bitmap containing the significant flags for parsing fields of the packet.
*                      possible values for IPv6 GEN API: 0
* parse_flags_bm     - Bitmap containing the significant flags result of the primary ACL filtering.
*                      possible values for IPv6 GEN API:
*                        TPM_PARSE_FLAG_TAG1_MASK|TPM_PARSE_FLAG_MTM_MASK|TPM_PARSE_FLAG_TO_CPU_MASK
* ipv6_gen_key       - Information to create an IPv6 gen parsing key for the rule.
*                      Some pointers may be NULL depending on the parse_rule_bm.
* pkt_frwd           - Information for packet forwarding decision.
* rule_action        - Action associated to the rule = drop/set target/set packet modification/to CPU
*                      and the next phase (for GWY only).
*                      possible "next_phase" for IPv6 GEN API   ->  STAGE_IPv6_DIP, ,STAGE_DONE
*
* OUTPUTS:
*  rule_idx          - Unique rule identification number, which is used when deleting the rule.
*                      (this is not the rule_num)
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_add_ipv6_gen_rule ( uint32_t            owner_id,
                                              tpm_src_port_type_t src_port,
                                              uint32_t            rule_num,
                                              uint32_t           *rule_idx,
                                              tpm_parse_fields_t  parse_rule_bm,
                                              tpm_parse_flags_t   parse_flags_bm,
                                              tpm_ipv6_gen_acl_key_t  *ipv6_gen_key,
                                              tpm_pkt_frwd_t     *pkt_frwd,
                                              tpm_pkt_mod_t      *pkt_mod,
                                              tpm_pkt_mod_bm_t    pkt_mod_bm,
                                              tpm_rule_action_t  *rule_action)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id                           = owner_id;
    tpm_add_acl_rule.src_port                           = src_port;
    tpm_add_acl_rule.rule_num                           = rule_num;
    tpm_add_acl_rule.parse_rule_bm                      = parse_rule_bm;
    tpm_add_acl_rule.ipv6_gen_acl_rule.parse_flags_bm = parse_flags_bm;
    tpm_add_acl_rule.ipv6_gen_acl_rule.pkt_mod_bm     = pkt_mod_bm;
    if (ipv6_gen_key != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_gen_acl_rule.ipv6_gen_key),    ipv6_gen_key,    sizeof(tpm_ipv6_gen_acl_key_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_gen_acl_rule.ipv6_gen_key),    0,    sizeof(tpm_ipv6_gen_acl_key_t));
    }
    if (pkt_mod != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_gen_acl_rule.pkt_mod),    pkt_mod,    sizeof(tpm_pkt_mod_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_gen_acl_rule.pkt_mod),    0,    sizeof(tpm_pkt_mod_t));
    }
    if (pkt_frwd != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_gen_acl_rule.pkt_frwd),    pkt_frwd,    sizeof(tpm_pkt_frwd_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_gen_acl_rule.pkt_frwd),    0,    sizeof(tpm_pkt_frwd_t));
    }
    if (rule_action != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_gen_acl_rule.rule_action), rule_action, sizeof(tpm_rule_action_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_gen_acl_rule.rule_action), 0, sizeof(tpm_rule_action_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_IPV6_GEN_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_del_ipv6_gen_rule()
*
* DESCRIPTION:      Deletes an existng IPv6 gen processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* rule_idx           - Unique rule idenitifcation number specifying the rule to be deleted.
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_del_ipv6_gen_rule  (   uint32_t            owner_id,
                                                 uint32_t            rule_idx)
{

    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id      = owner_id;
    tpm_del_acl_rule.rule_idx      = rule_idx;

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_IPV6_GEN_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}


/*******************************************************************************
* tpm_add_ipv6_dip_rule()
*
* DESCRIPTION:      Creates a new IPv6 DIP processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* src_port           - The packet originating source port. The parameter has a double function:
*                       . The source port is part of the rules parsing key.
*                       . The source port determines if the acl entry being
*                         created is for the upstream acl or the downstream acl.
*                      In case of an upstream entry, the parameter determines if the packet
*                      arrives from a specific LAN port or ANY LAN port.
* rule_num           - Entry index to be added in the current ACL
* parse_rule_bm      - Bitmap containing the significant flags for parsing fields of the packet.
*                      possible values for IPv6 DIP API: TPM_IPv6_PARSE_DIP
* parse_flags_bm     - Bitmap containing the significant flags result of the primary ACL filtering.
*                      possible values for IPv6 DIP API:
*                        TPM_PARSE_FLAG_TAG1_MASK|TPM_PARSE_FLAG_MTM_MASK|TPM_PARSE_FLAG_TO_CPU_MASK
* ipv6_dip_key       - Information to create an IPv6 DIP parsing key for the rule.
*                      Some pointers may be NULL depending on the parse_rule_bm.
* pkt_frwd           - Information for packet forwarding decision.
* pkt_mod            - Packet modification information.
* pkt_mod_bm         - Bitmap containing the significant fields to modify (used for GWY only - in SFU is NULL)
* rule_action        - Action associated to the rule = drop/set target/set packet modification/to CPU
*                      and the next phase (for GWY only).
*                      possible "next_phase" for IPv6 STEP1 API   ->  STAGE_IPv6_NHSTAGE_DONE
*
* OUTPUTS:
*  rule_idx          - Unique rule identification number, which is used when deleting the rule. (this is not the rule_num)
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_add_ipv6_dip_rule ( uint32_t             owner_id,
                                            tpm_src_port_type_t  src_port,
                                            uint32_t             rule_num,
                                            uint32_t            *rule_idx,
                                            tpm_parse_fields_t   parse_rule_bm,
                                            tpm_parse_flags_t    parse_flags_bm,
                                            tpm_ipv6_addr_key_t *ipv6_dip_key,
                                            tpm_pkt_frwd_t      *pkt_frwd,
                                            tpm_pkt_mod_t       *pkt_mod,
                                            tpm_pkt_mod_bm_t     pkt_mod_bm,
                                            tpm_rule_action_t   *rule_action)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id      = owner_id;
    tpm_add_acl_rule.src_port      = src_port;
    tpm_add_acl_rule.rule_num      = rule_num;
    tpm_add_acl_rule.parse_rule_bm = parse_rule_bm;
    tpm_add_acl_rule.ipv6_dip_acl_rule.parse_flags_bm = parse_flags_bm;
    tpm_add_acl_rule.ipv6_dip_acl_rule.pkt_mod_bm     = pkt_mod_bm;
    if (ipv6_dip_key != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_dip_acl_rule.ipv6_dip_key),    ipv6_dip_key,    sizeof(tpm_ipv6_addr_key_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_dip_acl_rule.ipv6_dip_key),    0,    sizeof(tpm_ipv6_addr_key_t));
    }
    if (pkt_mod != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_dip_acl_rule.pkt_mod),    pkt_mod,    sizeof(tpm_pkt_mod_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_dip_acl_rule.pkt_mod),    0,    sizeof(tpm_pkt_mod_t));
    }
    if (pkt_frwd != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_dip_acl_rule.pkt_frwd),    pkt_frwd,    sizeof(tpm_pkt_frwd_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_dip_acl_rule.pkt_frwd),    0,    sizeof(tpm_pkt_frwd_t));
    }
    if (rule_action != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_dip_acl_rule.rule_action), rule_action, sizeof(tpm_rule_action_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_dip_acl_rule.rule_action), 0, sizeof(tpm_rule_action_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_IPV6_DIP_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_del_ipv6_dip_rule()
*
* DESCRIPTION:      Deletes an existng IPv6 DIP processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* rule_idx           - Unique rule idenitifcation number specifying the rule to be deleted.
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_del_ipv6_dip_rule     ( uint32_t            owner_id,
                                                uint32_t            rule_idx)
{

    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id      = owner_id;
    tpm_del_acl_rule.rule_idx      = rule_idx;

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_IPV6_NH_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_add_ipv6_l4_ports_rule()
*
* DESCRIPTION:      Creates a new IPv6 L4 processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* src_port           - The packet originating source port. The parameter has a double function:
*                       . The source port is part of the rules parsing key.
*                       . The source port determines if the acl entry being
*                         created is for the upstream acl or the downstream acl.
*                      In case of an upstream entry, the parameter determines if the packet
*                      arrives from a specific LAN port or ANY LAN port.
* rule_num           - Entry index to be added in the current ACL
* parse_rule_bm      - Bitmap containing the significant flags for parsing fields of the packet.
*                      possible values for L4 API: TPM_PARSE_L4_SRC|TPM_PARSE_L4_DST
* parse_flags_bm     - Bitmap containing the significant flags result of the primary ACL filtering.
*                      possible values for L4 API:
*                         TPM_PARSE_FLAG_MTM_MASK|TPM_PARSE_FLAG_TO_CPU_MASK
* l4_key             - Information to create an L4 parsing key for the rule.
*                      Some pointers may be NULL depending on the parse_rule_bm.
* pkt_frwd           - Information for packet forwarding decision.
* pkt_mod            - Packet modification information.
* pkt_mod_bm         - Bitmap containing the significant fields to modify (used for GWY only - in SFU is NULL)
* rule_action        - Action associated to the rule = drop/set target/set packet modification/to CPU
*                      and the next phase (for GWY only).
*                      possible "next_phase" for L4 API   ->  STAGE_DONE
*
* OUTPUTS:
*  rule_idx          - Unique rule identification number, which is used when deleting the rule. (this is not the rule_num)
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_add_ipv6_l4_ports_rule (uint32_t                owner_id,
                                                tpm_src_port_type_t     src_port,
                                                uint32_t                rule_num,
                                                uint32_t               *rule_idx,
                                                tpm_parse_fields_t      parse_rule_bm,
                                                tpm_parse_flags_t       parse_flags_bm,
                                                tpm_l4_ports_key_t     *l4_key,
                                                tpm_pkt_frwd_t         *pkt_frwd,
                                                tpm_pkt_mod_t          *pkt_mod,
                                                tpm_pkt_mod_bm_t        pkt_mod_bm,
                                                tpm_rule_action_t      *rule_action)

{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id      = owner_id;
    tpm_add_acl_rule.src_port      = src_port;
    tpm_add_acl_rule.rule_num      = rule_num;
    tpm_add_acl_rule.parse_rule_bm = parse_rule_bm;
    tpm_add_acl_rule.ipv6_l4_ports_acl_rule.parse_flags_bm = parse_flags_bm;
    tpm_add_acl_rule.ipv6_l4_ports_acl_rule.pkt_mod_bm     = pkt_mod_bm;
    if (l4_key != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.l4_key),    l4_key,    sizeof(tpm_l4_ports_key_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.l4_key),    0,    sizeof(tpm_l4_ports_key_t));
    }
    if (pkt_mod != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.pkt_mod),    pkt_mod,    sizeof(tpm_pkt_mod_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.pkt_mod),    0,    sizeof(tpm_pkt_mod_t));
    }
    if (pkt_frwd != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.pkt_frwd),    pkt_frwd,    sizeof(tpm_pkt_frwd_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.pkt_frwd),    0,    sizeof(tpm_pkt_frwd_t));
    }
    if (rule_action != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.rule_action), rule_action, sizeof(tpm_rule_action_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.rule_action), 0, sizeof(tpm_rule_action_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_IPV6_L4_PORTS_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_del_ipv6_l4_ports_rule()
*
* DESCRIPTION:      Deletes an existng L4 processing ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* rule_idx           - Unique rule idenitifcation number specifying the rule to be deleted.
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t    tpm_del_ipv6_l4_ports_rule (uint32_t            owner_id,
                                                uint32_t            rule_idx)
{

    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id      = owner_id;
    tpm_del_acl_rule.rule_idx      = rule_idx;

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_IPV6_L4_PORTS_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
tpm_error_code_t tpm_add_ipv6_gen_5t_rule(uint32_t owner_id,
										tpm_dir_t src_dir,
										uint32_t rule_num,
										uint32_t *rule_idx,
										tpm_parse_fields_t parse_rule_bm,
										tpm_parse_flags_t parse_flags_bm,
										tpm_l4_ports_key_t *l4_key,
										tpm_ipv6_gen_acl_key_t *ipv6_gen_key,
										tpm_pkt_frwd_t *pkt_frwd,
										tpm_pkt_mod_t *pkt_mod,
										tpm_pkt_mod_bm_t pkt_mod_bm,
										tpm_rule_action_t *rule_action)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id       = owner_id;
    tpm_add_acl_rule.src_dir        = src_dir;
    tpm_add_acl_rule.rule_num       = rule_num;
    tpm_add_acl_rule.parse_rule_bm  = parse_rule_bm;
    tpm_add_acl_rule.ipv6_gen_5t_rule.parse_flags_bm = parse_flags_bm;
    tpm_add_acl_rule.ipv6_gen_5t_rule.pkt_mod_bm     = pkt_mod_bm;

    if (l4_key != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_gen_5t_rule.l4_key),    l4_key,    sizeof(tpm_l4_ports_key_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_gen_5t_rule.l4_key),    0,    sizeof(tpm_l4_ports_key_t));
    }
    if (ipv6_gen_key != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_gen_5t_rule.ipv6_gen_key),    ipv6_gen_key,    sizeof(tpm_ipv6_gen_acl_key_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_gen_5t_rule.ipv6_gen_key),    0,    sizeof(tpm_ipv6_gen_acl_key_t));
    }
    if (pkt_mod != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_gen_5t_rule.pkt_mod),    pkt_mod,    sizeof(tpm_pkt_mod_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_gen_5t_rule.pkt_mod),    0,    sizeof(tpm_pkt_mod_t));
    }
    if (pkt_frwd != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_gen_5t_rule.pkt_frwd),    pkt_frwd,    sizeof(tpm_pkt_frwd_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_gen_5t_rule.pkt_frwd),    0,    sizeof(tpm_pkt_frwd_t));
    }
    if (rule_action != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_gen_5t_rule.rule_action), rule_action, sizeof(tpm_rule_action_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_gen_5t_rule.rule_action), 0, sizeof(tpm_rule_action_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_IPV6_GEN_5T_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

    return TPM_RC_OK;
}
tpm_error_code_t tpm_del_ipv6_gen_5t_rule(uint32_t owner_id, uint32_t rule_idx)
{

    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id      = owner_id;
    tpm_del_acl_rule.rule_idx      = rule_idx;

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_IPV6_GEN_5T_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

tpm_error_code_t tpm_add_ipv6_dip_5t_rule(uint32_t owner_id,
										tpm_dir_t src_dir,
										uint32_t rule_num,
										uint32_t *rule_idx,
										tpm_parse_fields_t parse_rule_bm,
										tpm_parse_flags_t parse_flags_bm,
										tpm_l4_ports_key_t *l4_key,
										tpm_ipv6_gen_acl_key_t *ipv6_gen_key,
										tpm_ipv6_addr_key_t *ipv6_dip_key,
										tpm_pkt_frwd_t *pkt_frwd,
										tpm_pkt_mod_t *pkt_mod,
										tpm_pkt_mod_bm_t pkt_mod_bm,
										tpm_rule_action_t *rule_action)
{
	int 					 fd;
	tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

	fd = getTpmDrvFd();
	if (fd < 0)
		return ERR_GENERAL;

    tpm_add_acl_rule.owner_id       = owner_id;
    tpm_add_acl_rule.src_dir        = src_dir;
    tpm_add_acl_rule.rule_num       = rule_num;
    tpm_add_acl_rule.parse_rule_bm  = parse_rule_bm;
    tpm_add_acl_rule.ipv6_dip_5t_rule.parse_flags_bm = parse_flags_bm;
    tpm_add_acl_rule.ipv6_dip_5t_rule.pkt_mod_bm     = pkt_mod_bm;

	if (l4_key != NULL)
	{
		memcpy(&(tpm_add_acl_rule.ipv6_dip_5t_rule.l4_key),    l4_key,	  sizeof(tpm_l4_ports_key_t));
	}
	else
	{
		memset(&(tpm_add_acl_rule.ipv6_dip_5t_rule.l4_key),    0,	 sizeof(tpm_l4_ports_key_t));
	}
	if (ipv6_gen_key != NULL)
	{
		memcpy(&(tpm_add_acl_rule.ipv6_dip_5t_rule.ipv6_gen_key),	 ipv6_gen_key,	  sizeof(tpm_ipv6_gen_acl_key_t));
	}
	else
	{
		memset(&(tpm_add_acl_rule.ipv6_dip_5t_rule.ipv6_gen_key),	 0,    sizeof(tpm_ipv6_gen_acl_key_t));
	}
	if (ipv6_dip_key != NULL)
	{
		memcpy(&(tpm_add_acl_rule.ipv6_dip_5t_rule.ipv6_dip_key),	 ipv6_dip_key,	  sizeof(tpm_ipv6_addr_key_t));
	}
	else
	{
		memset(&(tpm_add_acl_rule.ipv6_dip_5t_rule.ipv6_dip_key),	 0,    sizeof(tpm_ipv6_addr_key_t));
	}
	if (pkt_mod != NULL)
	{
		memcpy(&(tpm_add_acl_rule.ipv6_dip_5t_rule.pkt_mod),	pkt_mod,	sizeof(tpm_pkt_mod_t));
	}
	else
	{
		memset(&(tpm_add_acl_rule.ipv6_dip_5t_rule.pkt_mod),	0,	  sizeof(tpm_pkt_mod_t));
	}
	if (pkt_frwd != NULL)
	{
		memcpy(&(tpm_add_acl_rule.ipv6_dip_5t_rule.pkt_frwd),	 pkt_frwd,	  sizeof(tpm_pkt_frwd_t));
	}
	else
	{
		memset(&(tpm_add_acl_rule.ipv6_dip_5t_rule.pkt_frwd),	 0,    sizeof(tpm_pkt_frwd_t));
	}
	if (rule_action != NULL)
	{
		memcpy(&(tpm_add_acl_rule.ipv6_dip_5t_rule.rule_action), rule_action, sizeof(tpm_rule_action_t));
	}
	else
	{
		memset(&(tpm_add_acl_rule.ipv6_dip_5t_rule.rule_action), 0, sizeof(tpm_rule_action_t));
	}


	tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_IPV6_DIP_5T_RULE;

	osSemTake(tpmApiSemId, GL_SUSPEND);
	if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
	{
		printf("%s: IOCTL failed\n\r", __FUNCTION__);
		osSemGive(tpmApiSemId);
		return ERR_GENERAL;
	}
	osSemGive(tpmApiSemId);

	*rule_idx = tpm_add_acl_rule.rule_idx;

	return TPM_RC_OK;
}


tpm_error_code_t tpm_del_ipv6_dip_5t_rule(uint32_t owner_id, uint32_t rule_idx)
{

    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id      = owner_id;
    tpm_del_acl_rule.rule_idx      = rule_idx;

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_IPV6_DIP_5T_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
tpm_error_code_t tpm_add_ipv6_l4_ports_5t_rule(uint32_t owner_id,
											tpm_dir_t src_dir,
											uint32_t rule_num,
											uint32_t *rule_idx,
											tpm_parse_fields_t parse_rule_bm,
											tpm_parse_flags_t parse_flags_bm,
											tpm_l4_ports_key_t *l4_key,
											tpm_pkt_frwd_t *pkt_frwd,
											tpm_pkt_mod_t *pkt_mod,
											tpm_pkt_mod_bm_t pkt_mod_bm,
											tpm_rule_action_t *rule_action)
{
    int                      fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.add_acl_cmd    = MV_TPM_IOCTL_ADD_IPV6_L4_PORTS_5T_RULE;
    tpm_add_acl_rule.owner_id       = owner_id;
    tpm_add_acl_rule.src_dir        = src_dir;
    tpm_add_acl_rule.rule_num       = rule_num;
    tpm_add_acl_rule.parse_rule_bm  = parse_rule_bm;
    tpm_add_acl_rule.ipv6_l4_ports_acl_rule.parse_flags_bm = parse_flags_bm;
    tpm_add_acl_rule.ipv6_l4_ports_acl_rule.pkt_mod_bm     = pkt_mod_bm;

    if (l4_key != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.l4_key),    l4_key,    sizeof(tpm_l4_ports_key_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.l4_key),    0,    sizeof(tpm_l4_ports_key_t));
    }
    if (pkt_mod != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.pkt_mod),    pkt_mod,    sizeof(tpm_pkt_mod_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.pkt_mod),    0,    sizeof(tpm_pkt_mod_t));
    }
    if (pkt_frwd != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.pkt_frwd),    pkt_frwd,    sizeof(tpm_pkt_frwd_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.pkt_frwd),    0,    sizeof(tpm_pkt_frwd_t));
    }
    if (rule_action != NULL)
    {
        memcpy(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.rule_action), rule_action, sizeof(tpm_rule_action_t));
    }
    else
    {
        memset(&(tpm_add_acl_rule.ipv6_l4_ports_acl_rule.rule_action), 0, sizeof(tpm_rule_action_t));
    }

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *rule_idx = tpm_add_acl_rule.rule_idx;

    return TPM_RC_OK;
}

tpm_error_code_t tpm_del_ipv6_l4_ports_5t_rule(uint32_t owner_id, uint32_t rule_idx)
{

    int                      fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id      = owner_id;
    tpm_del_acl_rule.rule_idx      = rule_idx;

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_IPV6_L4_PORTS_5T_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_add_ctc_cm_acl_rule()
*
* DESCRIPTION:      Creates a new CTC CnM ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* src_port           - The packet originating source port, could be any UNI port:
* precedence         - precedence of this CnM rule, from 0 to 7
* l2_parse_rule_bm
* ipv4_parse_rule_bm
*                    - Bitmap containing the significant flags for parsing fields of the packet.
* l2_key
* ipv4_key
*                    - Information to create a parsing key for the rule.
*                      Some pointers may be NULL depending on the parse_rule_bm.
* pkt_frw            - Information for packet forwarding decision.
* pkt_act            - Action associated to the rule
*
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct precedence of each rule.
*
*******************************************************************************/
tpm_error_code_t tpm_add_ctc_cm_acl_rule(uint32_t owner_id,
					 tpm_src_port_type_t src_port,
					 uint32_t precedence,
					 tpm_parse_fields_t l2_parse_rule_bm,
					 tpm_parse_fields_t ipv4_parse_rule_bm,
					 tpm_l2_acl_key_t *l2_key,
					 tpm_ipv4_acl_key_t  *ipv4_key,
					 tpm_pkt_frwd_t *pkt_frwd,
					 tpm_pkt_action_t pkt_act,
					 uint32_t pbits)
{
    int 		     fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
	return ERR_GENERAL;

    tpm_add_acl_rule.owner_id	   = owner_id;
    tpm_add_acl_rule.src_port	   = src_port;
    tpm_add_acl_rule.ctc_cm_acl_rule.precedence = precedence;
    tpm_add_acl_rule.ctc_cm_acl_rule.l2_parse_rule_bm = l2_parse_rule_bm;
    tpm_add_acl_rule.ctc_cm_acl_rule.ipv4_parse_rule_bm = ipv4_parse_rule_bm;
    tpm_add_acl_rule.ctc_cm_acl_rule.pkt_act = pkt_act;
    tpm_add_acl_rule.ctc_cm_acl_rule.p_bits = pbits;
    if (l2_key != NULL)
    {
	memcpy(&(tpm_add_acl_rule.ctc_cm_acl_rule.l2_key),	    l2_key,	 sizeof(tpm_l2_acl_key_t));
    }
    else
    {
	memset(&(tpm_add_acl_rule.ctc_cm_acl_rule.l2_key),	    0,	    sizeof(tpm_l2_acl_key_t));
    }
    if (ipv4_key != NULL)
    {
	memcpy(&(tpm_add_acl_rule.ctc_cm_acl_rule.ipv4_key),	    ipv4_key,	 sizeof(tpm_ipv4_acl_key_t));
    }
    else
    {
	memset(&(tpm_add_acl_rule.ctc_cm_acl_rule.ipv4_key),	    0,	    sizeof(tpm_ipv4_acl_key_t));
    }
    if (pkt_frwd != NULL)
    {
	memcpy(&(tpm_add_acl_rule.ctc_cm_acl_rule.pkt_frwd),    pkt_frwd,	 sizeof(tpm_pkt_frwd_t));
    }
    else
    {
	memset(&(tpm_add_acl_rule.ctc_cm_acl_rule.pkt_frwd),    0,	  sizeof(tpm_pkt_frwd_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_CTC_CM_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
	printf("%s: IOCTL failed\n\r", __FUNCTION__);
	osSemGive(tpmApiSemId);
	return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_add_ctc_cm_ipv6_acl_rule()
*
* DESCRIPTION:      Creates a new CTC IPv6 CnM ACL.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* src_port           - The packet originating source port, could be any UNI port:
* precedence         - precedence of this CnM rule, from 0 to 7
* ipv6_parse_rule_bm
*                    - Bitmap containing the significant flags for parsing fields of the packet.
* ipv6_key
*                    - Information to create a parsing key for the rule.
*                      Some pointers may be NULL depending on the parse_rule_bm.
* pkt_frw            - Information for packet forwarding decision.
* pkt_act            - Action associated to the rule
*
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct precedence of each rule.
*
*******************************************************************************/
tpm_error_code_t tpm_add_ctc_cm_ipv6_acl_rule(uint32_t owner_id,
					      tpm_src_port_type_t src_port,
					      uint32_t precedence,
					      tpm_parse_fields_t ipv6_parse_rule_bm,
					      tpm_ipv6_acl_key_t  *ipv6_key,
					      tpm_pkt_frwd_t *pkt_frwd,
					      tpm_pkt_action_t pkt_act,
					      uint32_t pbits)
{
    int 		     fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
	return ERR_GENERAL;

    tpm_add_acl_rule.owner_id	   = owner_id;
    tpm_add_acl_rule.src_port	   = src_port;
    tpm_add_acl_rule.ctc_cm_acl_rule.precedence = precedence;
    tpm_add_acl_rule.ctc_cm_acl_rule.ipv6_parse_rule_bm = ipv6_parse_rule_bm;
    tpm_add_acl_rule.ctc_cm_acl_rule.pkt_act = pkt_act;
    tpm_add_acl_rule.ctc_cm_acl_rule.p_bits = pbits;
    if (ipv6_key != NULL)
    {
	memcpy(&(tpm_add_acl_rule.ctc_cm_acl_rule.ipv6_key),	    ipv6_key,	 sizeof(tpm_ipv6_acl_key_t));
    }
    else
    {
	memset(&(tpm_add_acl_rule.ctc_cm_acl_rule.ipv4_key),	    0,	    sizeof(tpm_ipv4_acl_key_t));
    }
    if (pkt_frwd != NULL)
    {
	memcpy(&(tpm_add_acl_rule.ctc_cm_acl_rule.pkt_frwd),    pkt_frwd,	 sizeof(tpm_pkt_frwd_t));
    }
    else
    {
	memset(&(tpm_add_acl_rule.ctc_cm_acl_rule.pkt_frwd),    0,	  sizeof(tpm_pkt_frwd_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_CTC_CM_IPV6_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
	printf("%s: IOCTL failed\n\r", __FUNCTION__);
	osSemGive(tpmApiSemId);
	return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_ctc_cm_set_ipv6_parse_window()
*
* DESCRIPTION:      Set IPv6 CnM rule parse window
*     Two sets of IPv6 parse window:
*       - first 24 bytes from IPv6 Header, include fields like: NH, TC, SIP
*       - second 24 bytes from IPv6 Header, include fields like: L4 ports and DIP
*
* INPUTS:
* owner_id           - ID of an application which requests ownership on a group of APIs.
* check_level        - The check level determines to correct bad tpm rule or not in the API call
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns API_OWNERSHIP_SUCCESS. On error, see tpm_api_ownership_error_t.
*
* COMMENTS: none
*
*******************************************************************************/
tpm_error_code_t tpm_ctc_cm_set_ipv6_parse_window(uint32_t owner_id,
				     		  tpm_ctc_cm_ipv6_parse_win_t ipv6_parse_window)
{
	int			 fd;
	tpm_ioctl_ipv6_parse_window_t  ipv6_parse_window_ioctl;

	fd = getTpmDrvFd();
	if (fd < 0)
	    return ERR_GENERAL;

	ipv6_parse_window_ioctl.owner_id      = owner_id;
	ipv6_parse_window_ioctl.ipv6_parse_window      = ipv6_parse_window;

	osSemTake(tpmApiSemId, GL_SUSPEND);
	if (ioctl(fd, MV_TPM_IOCTL_SET_IPV6_CM_PARSE_WIN_SECTION, &ipv6_parse_window_ioctl))
	{
	    printf("%s: IOCTL failed\n\r", __FUNCTION__);
	    osSemGive(tpmApiSemId);
	    return ERR_GENERAL;
	}
	osSemGive(tpmApiSemId);

	return TPM_RC_OK;
}

/*******************************************************************************
* tpm_del_ctc_cm_acl_rule()
*
* DESCRIPTION:	    Deletes an existing CTC CnM rule.
*
* INPUTS:
* owner_id	     - APP owner id  should be used for all API calls.
* src_port	     - The packet originating source port, could be any UNI port:
* precedence	     - precedence of this CnM rule, from 0 to 7
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct precedence of each rule.
*
*******************************************************************************/
tpm_error_code_t tpm_del_ctc_cm_acl_rule(uint32_t owner_id,
					 tpm_src_port_type_t src_port,
					 uint32_t precedence)
{

    int 		     fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
	return ERR_GENERAL;

    tpm_del_acl_rule.owner_id	   = owner_id;
    tpm_del_acl_rule.precedence	   = precedence;
    tpm_del_acl_rule.src_port	   = src_port;

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_CTC_CM_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
	printf("%s: IOCTL failed\n\r", __FUNCTION__);
	osSemGive(tpmApiSemId);
	return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_rule_self_check()
*
* DESCRIPTION:      Performs TPM self check
*
* INPUTS:
* owner_id           - ID of an application which requests ownership on a group of APIs.
* check_level        - The check level determines to correct bad tpm rule or not in the API call
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns API_OWNERSHIP_SUCCESS. On error, see tpm_api_ownership_error_t.
*
* COMMENTS: none
*
*******************************************************************************/
tpm_error_code_t tpm_rule_self_check(uint32_t owner_id, tpm_self_check_level_enum_t check_level)
{
    int fd;
    tpm_ioctl_tpm_check_t tpm_rule_check;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;
    tpm_rule_check.owner_id = owner_id;
    tpm_rule_check.check_level = check_level;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_TPM_CHECK_SECTION, &tpm_rule_check)) {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_flush_vtu()
*
* DESCRIPTION:      Flush VTU on the Switch.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*
*******************************************************************************/
tpm_error_code_t tpm_flush_vtu(uint32_t owner_id)
{
    int fd;
    tpm_ioctl_flush_vtu_t tpm_flush_vtu;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;
    tpm_flush_vtu.owner_id = owner_id;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_FLUSH_VTU_SECTION, &tpm_flush_vtu)) {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_flush_atu()
*
* DESCRIPTION:      Flush ATU on the Switch.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* flush_type         - FLUSH all or FLUSH all dynamic
* db_num             - ATU DB Num, only 0 should be used, since there is only one ATU DB right now.
*
* OUTPUTS:
*  None.
*
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*
*******************************************************************************/
tpm_error_code_t tpm_flush_atu(uint32_t owner_id, tpm_flush_atu_type_t flush_type, uint16_t db_num)
{
    int fd;
    tpm_ioctl_flush_atu_t tpm_flush_atu;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;
    tpm_flush_atu.owner_id = owner_id;
    tpm_flush_atu.flush_type = flush_type;
    tpm_flush_atu.db_num = db_num;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_FLUSH_ATU_SECTION, &tpm_flush_atu)) {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_get_pnc_lu_entry()
*
* DESCRIPTION: The API get least used PnC rule by in specific PnC range
*
* INPUTS:   owner_id        - API owner id  should be used for all API calls
*           api_type        - TPM API type
*           lu_num          - The required number of least used PnC entries
*           lu_reset        - Should the API reset the counters after after reading the LU Counters
*
*
* OUTPUTS:
*           valid_num       - Number of valid least used PnC entries
*           count_array     - Array of the returned least used PnC entry rule_idx  and hit_counter.
*           unrelated_num   - Number of unrelated  least used PnC entries
*                             (PnC entries which were not part of this API_type)
* RETURNS:
* On success - TPM_RC_OK
* On error different types are returned according to the case - see tpm_error_code_t.
*
* COMMENTS:
*           None
*
*******************************************************************************/
tpm_error_code_t tpm_get_pnc_lu_entry(uint32_t			owner_id,
				      tpm_api_type_t 		 api_type,
				      uint16_t 				 lu_num,
				      uint8_t 				 lu_reset,
				      uint16_t 		        *valid_num,
				      tpm_api_entry_count_t *count_array,
				      uint16_t 		        *unrelated_num)
{
    int fd;
    tpm_ioctl_age_count_t tpm_age_count;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_age_count.owner_id 	       = owner_id;
    tpm_age_count.age_count_cmd    = MV_TPM_IOCTL_GET_LU_ENTRIES;
    tpm_age_count.api_type         = api_type;
    tpm_age_count.lu_reset         = lu_reset;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_AGE_COUNT_SECTION, &tpm_age_count)) {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

	*valid_num     = tpm_age_count.valid_num;
	*unrelated_num = tpm_age_count.unrelated_num;

	memcpy(count_array, &tpm_age_count.count_array[0], sizeof(tpm_api_entry_count_t)*(*valid_num));

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_get_pnc_all_hit_counters
*
* DESCRIPTION: The API returns all PnC hit counters per API type lower than a given threshold
*
* INPUTS:   owner_id            - APP owner id  should be used for all API calls
*           api_type            - TPM API group type
*           high_thresh_pkts    - High threashold watermark, counters lower than will be returned
*           counters_reset      - Reset API group type counters after read (0-false, 1-true)
*           valid_counters      - The count_array size (entry number, not byte count)
*
* OUTPUTS:
*           valid_counters      - The valid number of entries copied to count_array
*           count_array         - The PnC entries for the API type lower than high_thresh_pkts
*
* RETURNS:
* On success - TPM_RC_OK
* On error different types are returned according to the case - see tpm_error_code_t.
*
* COMMENTS:
*           None
*
*******************************************************************************/
tpm_error_code_t tpm_get_pnc_all_hit_counters(uint32_t                owner_id,
					      tpm_api_type_t          api_type,
					      uint32_t                high_thresh_pkts,
					      uint8_t                 counters_reset,
					      uint16_t               *valid_counters,
					      tpm_api_entry_count_t  *count_array)
{
    int fd;
    tpm_ioctl_pnc_hit_cnt_t tpm_hit_cnt;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_hit_cnt.owner_id 	     = owner_id;
    tpm_hit_cnt.age_count_cmd    = MV_TPM_IOCTL_GET_ALL_HIT_COUNTERS;
    tpm_hit_cnt.api_type         = api_type;
    tpm_hit_cnt.high_thresh_pkts = high_thresh_pkts;
    tpm_hit_cnt.counter_reset    = counters_reset;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_AGE_COUNT_SECTION, &tpm_hit_cnt)) {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

	*valid_counters     = tpm_hit_cnt.valid_counters;

	memcpy(count_array, &tpm_hit_cnt.count_array[0], sizeof(tpm_api_entry_count_t)*(*valid_counters));

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_set_pnc_counter_mask()
*
* DESCRIPTION: The API set the ability to mask or unmask a specific API entry from being LU scanned.
*
* INPUTS:   owner_id     - API owner id  should be used for all API calls
*           api_type     - TPM API type
*           rule_idx     - The rule index of the requested entry.
*           lu_rule_mask - The least used scanner mask
*                            0:enable_scanner, 1: mask_from_scanner
*
* OUTPUTS:
*
* RETURNS:
* On success - TPM_RC_OK
* On error different types are returned according to the case - see tpm_error_code_t.
*
* COMMENTS:
*           None
*
*******************************************************************************/
tpm_error_code_t tpm_set_pnc_counter_mask(uint32_t		owner_id,
					  tpm_api_type_t 	api_type,
					  uint32_t 			rule_idx,
					  uint32_t 			lu_rule_mask)
{
    int fd;
    tpm_ioctl_age_count_t tpm_age_count;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_age_count.owner_id 	       = owner_id;
    tpm_age_count.age_count_cmd    = MV_TPM_IOCTL_SET_LU_COUNT_MASK;
    tpm_age_count.api_type         = api_type;
    tpm_age_count.rule_idx         = rule_idx;
    tpm_age_count.lu_rule_mask     = lu_rule_mask;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_AGE_COUNT_SECTION, &tpm_age_count)) {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_get_pnc_hit_count()
*
* DESCRIPTION: The API get the hit counter according to rule_idx
*
* INPUTS:   owner_id     - API owner id  should be used for all API calls
*           api_type     - TPM API type
*           rule_idx     - The rule index of the requested entry.
*           hit_reset    - Should the API reset the hit counters after after reading
*
* OUTPUTS:
*           hit_count    - The number of hits of the rule.
* RETURNS:
* On success - TPM_RC_OK
* On error different types are returned according to the case - see tpm_error_code_t.
*
* COMMENTS:
*           None
*
*******************************************************************************/
tpm_error_code_t tpm_get_pnc_hit_count(uint32_t 	owner_id,
				       tpm_api_type_t 	api_type,
				       uint32_t 		rule_idx,
				       uint8_t      	hit_reset,
				       uint32_t        *hit_count)
{
    int fd;
    tpm_ioctl_age_count_t tpm_age_count;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_age_count.owner_id 	       = owner_id;
    tpm_age_count.age_count_cmd    = MV_TPM_IOCTL_GET_HIT_COUNT;
    tpm_age_count.api_type         = api_type;
    tpm_age_count.rule_idx         = rule_idx;
    tpm_age_count.hit_reset        = hit_reset;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_AGE_COUNT_SECTION, &tpm_age_count)) {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

	*hit_count = tpm_age_count.hit_count;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_set_pnc_lu_threshold()
*
* DESCRIPTION: The API set the theshold packets number for least used scanner
*
* INPUTS:   owner_id       - API owner id  should be used for all API calls
*           api_type       - TPM API type
*           lu_thresh_pkts - The least_used theshold to be used for the lu_scanner, in number of packets.
*                            Hit_counts above this threshold are not returned by the scanner,
*                            even if they are the lowest amoung their peers.
* OUTPUTS:
*            None
* RETURNS:
* On success - TPM_RC_OK
* On error different types are returned according to the case - see tpm_error_code_t.
*
* COMMENTS:
*           None
*
*******************************************************************************/
tpm_error_code_t tpm_set_pnc_lu_threshold(uint32_t 		owner_id,
					  tpm_api_type_t 	api_type,
					  uint32_t 			lu_thresh_pkts)
{
    int fd;
    tpm_ioctl_age_count_t tpm_age_count;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_age_count.owner_id 	       = owner_id;
    tpm_age_count.age_count_cmd    = MV_TPM_IOCTL_SET_LU_THESHOLD;
    tpm_age_count.api_type         = api_type;
    tpm_age_count.lu_thresh_pkts   = lu_thresh_pkts;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_AGE_COUNT_SECTION, &tpm_age_count)) {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;

}

/*******************************************************************************
* tpm_reset_pnc_age_group()
*
* DESCRIPTION: This API resets the hit counters of all the PnC entries of a specific
*              API. It may reset other PnC entries as well, if they share the same hit_group.
*
* INPUTS:   owner_id       - API owner id  should be used for all API calls
*           api_type       - TPM API type
*
* OUTPUTS:
*            None
* RETURNS:
* On success - TPM_RC_OK
* On error different types are returned according to the case - see tpm_error_code_t.
*
* COMMENTS:
*           None
*
*******************************************************************************/
tpm_error_code_t tpm_reset_pnc_age_group(uint32_t 	owner_id,
					 tpm_api_type_t api_type)
{
    int fd;
    tpm_ioctl_age_count_t tpm_age_count;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_age_count.owner_id 	      = owner_id;
    tpm_age_count.age_count_cmd   = MV_TPM_IOCTL_RESET_COUNT_GROUP;
    tpm_age_count.api_type        = api_type;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_AGE_COUNT_SECTION, &tpm_age_count)) {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_add_cpu_wan_loopback()
*
* DESCRIPTION: The API allows the CPU to set the target for packets that will be
*              loopbacked to the WAN port to a HWF queue, by means of packet_modification.
*              It returns to the CPU the mod_idx to put in the Tx_Descriptor, so that the
*              packet will be forwarded to the correct target. The cpu_loopback mechanism is
*              done by use of GMAC1, it is assumed that GMAC0&GMAC1 are both connected to the Internal Switch.
*
* INPUTS:   owner_id    - owner id
*           pkt_frwd    - packet forwarding info: target tcont/LLID number, 0-7
*                         target queue, 0-7, target gem port ID
*
* OUTPUTS:
*           mod_idx     - Returned by HW modification, to fill Tx description
*
* RETURNS:
* On success - TPM_RC_OK
* On error different types are returned according to the case - see tpm_error_code_t.
*
* COMMENTS:
*           None
*
*******************************************************************************/
tpm_error_code_t tpm_add_cpu_wan_loopback(uint32_t 		owner_id,
					  tpm_pkt_frwd_t       *pkt_frwd,
					  uint32_t			   *mod_idx)
{
    int fd;
    tpm_ioctl_cpu_lpbk_t tpm_cpu_lpbk;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_cpu_lpbk.owner_id 	  		= owner_id;
    tpm_cpu_lpbk.cpu_lpbk_cmd 		= MV_TPM_IOCTL_ADD_CPU_WAN_LPBK;
    tpm_cpu_lpbk.pkt_frwd.trg_port  = pkt_frwd->trg_port;
    tpm_cpu_lpbk.pkt_frwd.trg_queue = pkt_frwd->trg_queue;
    tpm_cpu_lpbk.pkt_frwd.gem_port  = pkt_frwd->gem_port;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_CPU_LPBK_SECTION, &tpm_cpu_lpbk)) {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *mod_idx = tpm_cpu_lpbk.mod_idx;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_del_cpu_wan_loopback()
*
* DESCRIPTION: The API delete CPU egress loopback modification and PnC rules for
*              specific Tcont/queue/gem_port
*
* INPUTS:   owner_id    - owner id
*           pkt_frwd    - packet forwarding info: target tcont/LLID number, 0-7
*                         target queue, 0-7, target gem port ID
*
* OUTPUTS:
*           NONE
*
* RETURNS:
* On success - TPM_RC_OK
* On error different types are returned according to the case - see tpm_error_code_t.
*
* COMMENTS:
*           None
*
*******************************************************************************/
tpm_error_code_t tpm_del_cpu_wan_loopback(uint32_t 	  owner_id,
					  tpm_pkt_frwd_t *pkt_frwd)
{
    int fd;
    tpm_ioctl_cpu_lpbk_t tpm_cpu_lpbk;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;
    tpm_cpu_lpbk.owner_id 	  		= owner_id;
    tpm_cpu_lpbk.cpu_lpbk_cmd 		= MV_TPM_IOCTL_DEL_CPU_WAN_LPBK;
    tpm_cpu_lpbk.pkt_frwd.trg_port  = pkt_frwd->trg_port;
    tpm_cpu_lpbk.pkt_frwd.trg_queue = pkt_frwd->trg_queue;
    tpm_cpu_lpbk.pkt_frwd.gem_port  = pkt_frwd->gem_port;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_CPU_LPBK_SECTION, &tpm_cpu_lpbk)) {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_add_mac_learn_rule()
*
* DESCRIPTION:      Add MAC leanr ACL rule.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* rule_num           - Entry index to be added in the current ACL
* queue              - queue on target port
* src_mac_addr       - MAC address to learn
* rule_idx           - Unique rule identification number. Equals to stream_num for API's that are operating in table mode.
* OUTPUTS:
*  None.
*
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*
*******************************************************************************/
tpm_error_code_t tpm_add_mac_learn_rule(uint32_t owner_id, tpm_l2_acl_key_t *src_mac_addr)
{
    int fd;
    tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_add_acl_rule.owner_id = owner_id;
    if (src_mac_addr != NULL) {
        memcpy(&(tpm_add_acl_rule.l2_acl_rule.l2_key), src_mac_addr, sizeof(tpm_l2_acl_key_t));
    }

    tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_ADD_MAC_LEARN_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    //*rule_idx = tpm_add_acl_rule.rule_idx;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_del_mac_learn_rule()
*
* DESCRIPTION:      Deletes an existing MAC learn ACL rule.
*                   Any of the existing entries may be deleted.
* INPUTS:
* owner_id           - API owner id  should be used for all API calls.
* src_mac_addr       - MAC learn addr
*
* OUTPUTS:
*  None.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
* It is APIs caller responsibility to maintain the correct number of each rule.
*
*******************************************************************************/
tpm_error_code_t tpm_del_mac_learn_rule (uint32_t owner_id, tpm_l2_acl_key_t *src_mac_addr)
{

    int fd;
    tpm_ioctl_del_acl_rule_t tpm_del_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_del_acl_rule.owner_id = owner_id;
    memcpy(&tpm_del_acl_rule.l2_key, src_mac_addr, sizeof(tpm_l2_acl_key_t));

    tpm_del_acl_rule.del_acl_cmd = MV_TPM_IOCTL_DEL_MAC_LEARN_ACL_RULE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_DEL_ACL_SECTION, &tpm_del_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_mac_learn_default_rule_act_set()
*
* DESCRIPTION:      Set the action for mac learn default rule
*
* INPUTS:
* owner_id           - ID of an application which requests ownership on a group of APIs.
* mac_conf           - default rule conf, 3 options: trap to CPU, drop packet, frwd to GMAC1
* OUTPUTS:
*
* RETURNS:
* On success, the function returns API_OWNERSHIP_SUCCESS. On error, see tpm_api_ownership_error_t.
*
* COMMENTS: none
*
*******************************************************************************/
tpm_error_code_t tpm_mac_learn_default_rule_act_set(uint32_t owner_id, tpm_unknown_mac_conf_t mac_conf)
{
	int fd;
	tpm_ioctl_add_acl_rule_t tpm_add_acl_rule;

	fd = getTpmDrvFd();
	if (fd < 0)
	    return ERR_GENERAL;

	tpm_add_acl_rule.owner_id = owner_id;
	tpm_add_acl_rule.l2_acl_rule.mac_conf = mac_conf;

	tpm_add_acl_rule.add_acl_cmd = MV_TPM_IOCTL_SET_MAC_LEARN_DEFAULT_ACTION;

	osSemTake(tpmApiSemId, GL_SUSPEND);
	if (ioctl(fd, MV_TPM_IOCTL_ADD_ACL_SECTION, &tpm_add_acl_rule))
	{
	    printf("%s: IOCTL failed\n\r", __FUNCTION__);
	    osSemGive(tpmApiSemId);
	    return ERR_GENERAL;
	}
	osSemGive(tpmApiSemId);

	return TPM_RC_OK;
}

/*******************************************************************************
* tpm_mac_learn_entry_num_get()
*
* DESCRIPTION:      Get the mac learn entry number currently in system
*
* INPUTS:
* None
* OUTPUTS:
* entry_count          - current MAC learn entry count in MAC_LEARN range, not including default one
* RETURNS:
* On success, the function returns API_OWNERSHIP_SUCCESS. On error, see tpm_api_ownership_error_t.
*
* COMMENTS: none
*
*******************************************************************************/
tpm_error_code_t tpm_mac_learn_entry_num_get(uint32_t *entry_count)
{
    int                 fd;
    tpm_ioctl_ctrl_acl_rule_t tpm_ctrl_acl_rule;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_ctrl_acl_rule.ctrl_acl_cmd = MV_TPM_IOCTL_GET_MAC_LEARN_ENTRY_NUM;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_CTRL_ACL_SECTION, &tpm_ctrl_acl_rule))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    *entry_count = tpm_ctrl_acl_rule.entry_count;

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_xlate_uni_2_switch_port()
*
* DESCRIPTION: The API translates TPM logic UNI port into Switch port.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* uni_port           - TPM logic port that need to be translated.
*
* OUTPUTS:
* switch_port      - switch port.
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*           None
*
*******************************************************************************/
tpm_error_code_t tpm_xlate_uni_2_switch_port (uint32_t		  owner_id,
						     tpm_src_port_type_t  uni_port,
						     uint32_t		 *switch_port)
{
	int fd;
	tpm_ioctl_sw_phy_t tpm_sw_phy;

	fd = getTpmDrvFd();
	if (fd < 0)
	    return ERR_GENERAL;

	tpm_sw_phy.owner_id	  = owner_id;

	/*add this port only for convenience, not use is kernel convert function*/
	tpm_sw_phy.port = TPM_SRC_PORT_UNI_0;

	tpm_sw_phy.extern_port_id = uni_port;

	tpm_sw_phy.sw_phy_cmd = MV_TPM_IOCTL_UNI_2_SW_PORT;

	osSemTake(tpmApiSemId, GL_SUSPEND);
	if (ioctl(fd, MV_TPM_IOCTL_SW_PHY_SECTION, &tpm_sw_phy))
	{
	    printf("%s: IOCTL failed\n\r", __FUNCTION__);
	    osSemGive(tpmApiSemId);
	    return ERR_GENERAL;
	}
	osSemGive(tpmApiSemId);

	*switch_port = tpm_sw_phy.switch_port_id;

	return TPM_RC_OK;

}

/*******************************************************************************
* tpm_tm_set_gmac0_ingr_rate_lim()
*
* DESCRIPTION:      Configures the ingress rate limit of US for MC only
*
* INPUTS:
*       owner_id          - APP owner id  should be used for all API calls.
*       rate_limit_val    - ingress rate limit value
*       bucket_size       - bucket size value
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success, the function returns TPM_RC_OK. On error different types are returned
*       according to the case - see tpm_error_code_t.
*
*******************************************************************************/
tpm_error_code_t tpm_tm_set_gmac0_ingr_rate_lim(uint32_t owner_id,
						uint32_t rate_limit_val,
						uint32_t bucket_size)
{
    int               fd;
    tpm_ioctl_tm_tm_t tpm_tm_tm;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_tm_tm.owner_id       = owner_id;
    tpm_tm_tm.rate_limit_val = rate_limit_val;
    tpm_tm_tm.bucket_size    = bucket_size;

    tpm_tm_tm.pp_tm_cmd = MV_TPM_IOCTL_TM_SET_GMAC0_INGR_RATE_LIMIT;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_PP_TM_SECTION, &tpm_tm_tm))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}

/*******************************************************************************
* tpm_sw_port_add_vid_set_egrs_mode
*
* DESCRIPTION:
*       The API adds a VID to the list of the allowed VIDs per UNI port,
*       and sets the egress mode for the port.
*
* INPUTS:
*       owner_id   - APP owner id should be used for all API calls.
*       src_port   - Source port in UNI port index, UNI0, UNI1...UNI4.
*       vid        - vlan id
*       eMode      - egress mode
*
* OUTPUTS:
*       None.
*
* RETURNS:
*       On success - TPM_RC_OK.
*       On error different types are returned according to the case see tpm_error_code_t.
*
* COMMENTS:
*       MEMBER_EGRESS_UNMODIFIED - 0
*       NOT_A_MEMBER             - 1
*       MEMBER_EGRESS_UNTAGGED   - 2
*       MEMBER_EGRESS_TAGGED     - 3
*
*******************************************************************************/
tpm_error_code_t tpm_sw_port_add_vid_set_egrs_mode (uint32_t            owner_id,
                                                    tpm_src_port_type_t port,
                                                    uint16_t            vid,
                                                    uint8_t             eMode)
{
    int                        fd;
    tpm_ioctl_sw_vlan_filter_t tpm_sw_mac_vlan;

    fd = getTpmDrvFd();
    if (fd < 0)
        return ERR_GENERAL;

    tpm_sw_mac_vlan.owner_id    = owner_id;
    tpm_sw_mac_vlan.port        = port;
    tpm_sw_mac_vlan.vid         = vid;
    tpm_sw_mac_vlan.egress_mode = eMode;

    tpm_sw_mac_vlan.sw_vlan_cmd = MV_TPM_IOCTL_SW_PORT_ADD_VID_SET_EGRESS_MODE;

    osSemTake(tpmApiSemId, GL_SUSPEND);
    if (ioctl(fd, MV_TPM_IOCTL_SW_VLAN_SECTION, &tpm_sw_mac_vlan))
    {
        printf("%s: IOCTL failed\n\r", __FUNCTION__);
        osSemGive(tpmApiSemId);
        return ERR_GENERAL;
    }
    osSemGive(tpmApiSemId);

    return TPM_RC_OK;
}
/*******************************************************************************
* tpm_hot_swap_profile()
*
* DESCRIPTION:      Swap profile and update all the ACL rules according to
*                   the new profile
*
* INPUTS:
* owner_id          - APP owner id  should be used for all API calls.
* profile_id        - the new profile that system is swapping to
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPM_DB_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*
*******************************************************************************/
tpm_error_code_t tpm_hot_swap_profile(uint32_t owner_id,
				    tpm_eth_complex_profile_t profile_id)
{
	int fd;
	tpm_ioctl_hot_swap_profile_t  swap_profile;
	
	fd = getTpmDrvFd();
	if (fd < 0)
	    return ERR_GENERAL;
	
	swap_profile.owner_id   = owner_id;
	swap_profile.profile_id = profile_id;
	
	osSemTake(tpmApiSemId, GL_SUSPEND);
	if (ioctl(fd, MV_TPM_IOCTL_HOT_SWAP_PROFILE_SECTION, &swap_profile)) {
	    printf("%s: IOCTL failed\n\r", __FUNCTION__);
	    osSemGive(tpmApiSemId);
	    return ERR_GENERAL;
	}
	osSemGive(tpmApiSemId);
	
	return TPM_RC_OK;
}

/*******************************************************************************
* tpm_set_gmac_loopback()
*
* DESCRIPTION: The API enable/disable loopback mode of gmac.
*
* INPUTS:
* owner_id           - APP owner id  should be used for all API calls.
* gmac                 -
* enable               - 1 for enable, 0 for disable
*
* OUTPUTS:
*
* RETURNS:
* On success, the function returns TPM_RC_OK. On error different types are returned
* according to the case - see tpm_error_code_t.
*
* COMMENTS:
*           None
*
*******************************************************************************/
tpm_error_code_t tpm_set_gmac_loopback (uint32_t	  owner_id,
						tpm_gmacs_enum_t  gmac,
						uint8_t		  enable)
{
	int fd;
	tpm_ioctl_set_gmac_loopback_t  set_gmac_loopback;

	fd = getTpmDrvFd();
	if (fd < 0)
	    return ERR_GENERAL;

	set_gmac_loopback.owner_id   = owner_id;
	set_gmac_loopback.gmac = gmac;
	set_gmac_loopback.enable = enable;

	osSemTake(tpmApiSemId, GL_SUSPEND);
	if (ioctl(fd, MV_TPM_IOCTL_SET_GMAC_LPBK_SECTION, &set_gmac_loopback)) {
	    printf("%s: IOCTL failed\n\r", __FUNCTION__);
	    osSemGive(tpmApiSemId);
	    return ERR_GENERAL;
	}
	osSemGive(tpmApiSemId);

	return TPM_RC_OK;
}
