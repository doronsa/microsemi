/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "globals.h"
#include "errorCode.h"
#include "OsGlueLayer.h"

#include "tpm_types.h"
#include "tpm_internal_types.h"
#include "tpm_mng_if.h"

tpm_init_t      tpm_init;
int             tpmDrvFd = -1;
GL_SEMAPHORE_ID tpmApiSemId = NULL;


int32_t tpm_init_wantech_get(int pon_type)
{
    int32_t rc = TPM_OK;

    tpm_init.pon_type = pon_type;

	return rc;
}

int32_t tpm_init_omci_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_omci_etype_param(&(tpm_init.omci_etype));
    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.omci_etype = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }
    return rc;
}
/*
int32_t tpm_init_debport_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_debug_port_params(&(tpm_init.deb_port_valid),
                                       &(tpm_init.deb_port));
    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    return rc;
}
*/
int32_t tpm_init_igmp_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_igmp_snoop_params(&(tpm_init.igmp_snoop),
                                       &(tpm_init.igmp_cpu_queue));

    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.igmp_snoop     = MV_TPM_UN_INITIALIZED_INIT_PARAM;
        tpm_init.igmp_cpu_queue = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }

    return rc;
}
/* remove from US - implemented in Kernel - by HW auto-detection */
#if 0
int32_t tpm_init_eth_port_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_eth_ports_params(&(tpm_init.eth_port_conf[0]),
                                      TPM_MAX_NUM_ETH_PORTS);
    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    return rc;
}
#endif

int32_t tpm_init_gmac_conn_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_gmac_conn_params(&(tpm_init.num_tcont_llid));
    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.num_tcont_llid = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }
    return rc;
}


int32_t tpm_init_gmac_mh_en_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_gmac_mh_en_params(&(tpm_init.gmac0_mh_en),
                                   &(tpm_init.gmac1_mh_en));
    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.gmac0_mh_en      = MV_TPM_UN_INITIALIZED_INIT_PARAM;
        tpm_init.gmac1_mh_en      = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }
    return rc;
}

int32_t tpm_init_gmac_pool_bufs(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_gmac_pool_bufs_params(&(tpm_init.gmac_bp_bufs),
                                       TPM_MAX_NUM_GMACS);
    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    return rc;
}



int32_t tpm_init_gmac_rx_get(void)
{
    int32_t  rc = TPM_OK;
    int      app_rc;
    uint32_t rx, i;

    app_rc = get_gmac_rxq_params(&(tpm_init.gmac_rx[0]),
                                    TPM_MAX_NUM_GMACS,
                                    TPM_MAX_NUM_RX_QUEUE);
    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    return rc;
}

int32_t tpm_init_gmac_tx_get(void)
{
    int32_t  rc = TPM_OK;
    int      app_rc;
    uint32_t tx, i;

    app_rc = get_gmac_tx_params(&(tpm_init.gmac_tx[0]),
                                    TPM_MAX_NUM_TX_PORTS,
                                    TPM_MAX_NUM_TX_QUEUE);
    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    return rc;
}

int32_t tpm_init_pnc_config_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;
    int     i=0;

    app_rc = get_pnc_range_params(&(tpm_init.pnc_range[0]),
                                      TPM_MAX_NUM_RANGES);
    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    return rc;
}

int32_t tpm_init_ds_mh_config_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_ds_mh_config_params(&(tpm_init.ds_mh_set_conf));
    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.ds_mh_set_conf = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }

    return rc;
}

int32_t tpm_init_validation_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_validation_enabled_config_params(&(tpm_init.validation_en));

    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.validation_en = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }

    return rc;
}

int32_t tpm_init_vlan_etypes_get (void)
{
    int32_t rc = TPM_OK;
    int     app_rc, i;



    app_rc = get_vlan_etypes_params(&(tpm_init.vlan_etypes[0]),
                                    TPM_NUM_VLAN_ETYPE_REGS);

    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.vlan_etypes[0] = 0x8100;
    }

    return rc;
}

int32_t tpm_init_mod_config_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_modification_params(&(tpm_init.mod_config));
    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    return rc;
}

/*
int32_t tpm_init_sw_gwy_init_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_sw_gwy_init_params(&(tpm_init.sw_gwy_init));

    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.sw_gwy_init = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }
    return rc;
}
*/

/*
int32_t tpm_init_default_ping_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_default_ping_enabled_config_params(&(tpm_init.default_ping_en));

    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.default_ping_en = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }
    return rc;
}

int32_t tpm_init_system_type_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_system_type_param(&(tpm_init.system_type));

    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.system_type = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }

    return rc;
}
*/

int32_t tpm_init_cfg_pnc_parse_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_cfg_pnc_parse_param(&(tpm_init.cfg_pnc_parse));

    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.cfg_pnc_parse = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }
    return rc;
}

int32_t tpm_init_trace_debug_info_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_trace_debug_info_param(&(tpm_init.trace_debug_info));

    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.trace_debug_info = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }
    return rc;
}

int32_t tpm_init_wifi_vitual_uni_info_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_wifi_vitual_uni_enable_params(&(tpm_init.virt_uni_info.enabled),
                                               &(tpm_init.virt_uni_info.uni_port));

    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.virt_uni_info.enabled = MV_TPM_UN_INITIALIZED_INIT_PARAM;
        tpm_init.virt_uni_info.uni_port    = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }

    return rc;
}
/*
int32_t tpm_init_double_tag_support_get(void)
{
    int32_t rc = TPM_OK;
    int     app_rc;

    app_rc = get_double_tag_support_params(&(tpm_init.dbl_tag));

    if (app_rc == TPM_FAIL)
    {
        rc = TPM_FAIL;
    }
    else if (app_rc == TPM_NOT_FOUND)
    {
        tpm_init.dbl_tag = MV_TPM_UN_INITIALIZED_INIT_PARAM;
    }

    return rc;
}
*/
int32_t tpm_init_default_tag_tpid_get(void)
{
	int32_t rc = TPM_OK;
	int app_rc;

#if 0
	app_rc = get_default_vlan_tpid_params(&(tpm_init.vlan1_tpid), &(tpm_init.vlan2_tpid));

	if (app_rc == TPM_FAIL)
		rc = TPM_FAIL;
	else if (app_rc == TPM_NOT_FOUND) {
		tpm_init.vlan1_tpid = 0x8100;
		tpm_init.vlan2_tpid = 0x8100;
	}
#endif

	app_rc = get_default_vlan_tpid_params(&tpm_init.tpid_opt.opt_num, tpm_init.tpid_opt.opt);

	if (app_rc == TPM_FAIL)
		rc = TPM_FAIL;
	else if (app_rc == TPM_NOT_FOUND) {
		tpm_init.tpid_opt.opt_num = 2;

		tpm_init.tpid_opt.opt[0].v1_tpid = 0x8100;
		tpm_init.tpid_opt.opt[0].v2_tpid = 0x8100;

		tpm_init.tpid_opt.opt[1].v1_tpid = 0x8100;
		tpm_init.tpid_opt.opt[1].v2_tpid = MV_TPM_UN_INITIALIZED_INIT_PARAM;
	}

	return rc;
}

int32_t tpm_init_ety_dsa_enable_get_para(void)
{
	int32_t rc = TPM_OK;
	int app_rc;

	app_rc = get_ety_dsa_enable(&tpm_init.ety_dsa_enable);

	if (app_rc == TPM_FAIL)
		rc = TPM_FAIL;
	else if (app_rc == TPM_NOT_FOUND)
		tpm_init.ety_dsa_enable = TPM_ETY_DSA_DISABLE;

	return rc;
}

/*******************************************************************************
* module_tpmSetup()
*
* DESCRIPTION:
*
* INPUTS:
*
*
* OUTPUTS:
*
*
* RETURNS:
*
*
* COMMENTS:
*
*******************************************************************************/
int32_t module_tpmSetup(int pon_type)
{
    int32_t             rc;
    tpm_config_mode_t   mode;

    /* Verify TPM configuration mode */
    if (get_config_mode_param (&mode) != TPM_OK)
        mode = TPM_CFG_MODE_KERNEL;

    /*the following commented part is moved to tpmKernelExist() for multiprocess*/
    /* Initialized as sem_full */
    /*
    if (osSemCreate(&tpmApiSemId, "/TpmApiSem", 1, SEM_Q_FIFO, TRUE) != IAMBA_OK)
    {
        printf("module_tpmSetup: Failed to create TPM API semaphore\n\r");                                                   
        return ERROR; 
    }	
    */
    if (mode == TPM_CFG_MODE_APPL)
    {
        /* TPM configuration is processed by user space application */
        /*printf("!!!!!!!!!!! TPM configuration is processed by user space application\n");*/

        tpm_init_validation_get();
        //tpm_init_system_type_get();

        tpm_init_vlan_etypes_get();
        tpm_init_wantech_get(pon_type);
        tpm_init_wifi_vitual_uni_info_get();
        //tpm_init_double_tag_support_get();
        tpm_init_default_tag_tpid_get();
        //tpm_init_default_ping_get();
        //tpm_init_sw_gwy_init_get();
        tpm_init_cfg_pnc_parse_get();
        tpm_init_trace_debug_info_get();
        tpm_init_omci_get();
        //tpm_init_debport_get();
        tpm_init_igmp_get();
    /*    tpm_init_eth_port_get(); = HW auto-detect */
        tpm_init_gmac_conn_get();
        tpm_init_gmac_rx_get();
        tpm_init_gmac_tx_get();
        tpm_init_gmac_mh_en_get();
        tpm_init_gmac_pool_bufs();
        tpm_init_pnc_config_get();
        tpm_init_ds_mh_config_get();
        tpm_init_mod_config_get();
		tpm_init_ety_dsa_enable_get_para();

        rc = tpm_module_init(&tpm_init);
        if (rc != TPM_OK)
        {
            return (rc);
        }
    }
    else
    {
        /* TPM configuration is processed by Kernel */
        /*printf("!!!!!!!!!!! TPM configuration is processed by Kernel\n");*/

        rc = tpm_module_setup(NULL);
        if (rc != TPM_OK)
        {
            return (rc);
        }
    }

    return (TPM_OK);
}

/*******************************************************************************
* getTpmDrvFd()
*
* DESCRIPTION:   Get TPM driver file descriptor
*
* INPUTS:
*
*
* OUTPUTS:
*
*
* RETURNS:
*
*
* COMMENTS:
*
*******************************************************************************/
int getTpmDrvFd(void)
{
    if (NULL == tpmApiSemId)
    {
        /* Initialized as sem_full */
        if (osSemCreate(&tpmApiSemId, "/TpmApiSem", 1, SEM_Q_FIFO, TRUE) != IAMBA_OK)
        {
            printf("Failed to create TPM API semaphore\n\r");                                                    
            return -1; 
        }   
        
    }
    if (tpmDrvFd >= 0)
    {
        return tpmDrvFd;
    }
    
    tpmDrvFd = open("/dev/tpm", O_RDWR);
    if (tpmDrvFd < 0)
    {
        printf("Failed to open /dev/tpm\n\r");
        return(-1);
    }

    return(tpmDrvFd);
}


/*******************************************************************************
* ponKernelExist()
*
* DESCRIPTION:   Verify whether the TPM driver exists
*
* INPUTS:
*
*
* OUTPUTS:
*
*
* RETURNS:
*
*
* COMMENTS:
*
*******************************************************************************/
void tpmKernelExist(void)
{
    FILE    *fptr;
    uint8_t *filename = "/proc/devices";
    uint8_t line[120];
    uint8_t *token;
    uint8_t *search  = " \n\r";

    if (NULL == tpmApiSemId)
	{
		/* Initialized as sem_full */
		if (osSemCreate(&tpmApiSemId, "/TpmApiSem", 1, SEM_Q_FIFO, TRUE) != IAMBA_OK)
		{
			printf("Failed to create TPM API semaphore\n\r");													 
			return -1; 
		}	
		
	}
    
    if ((fptr = fopen(filename, "r")) == 0)
    {
        printf("%s: failed to open %s\n", __FUNCTION__, filename);
        return;
    }

    while (1)
    {
        if (fgets(line, sizeof(line), fptr) != 0)
        {
            if (strtok(line, search) != NULL)
            {
                if ((token = strtok(NULL, search)) != NULL)
                {
                    if (strcmp("tpm", token) == 0)
                    {
                        tpmDrvFd = open("/dev/tpm", O_RDWR);
                        if (tpmDrvFd < 0)
                        {
                            printf("Failed to open /dev/tpm\n\r");
                            fclose(fptr);
                            return;
                        }

                        break;
                    }
                }
            }
        }
        else
            break;
    }

    fclose(fptr);
}
