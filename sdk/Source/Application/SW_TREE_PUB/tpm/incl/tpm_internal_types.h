/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
alternative licensing terms.  Once you have made an election to distribute the
File under one of the following license alternatives, please (i) delete this
introductory statement regarding license alternatives, (ii) delete the two
license alternatives that you have not elected to use and (iii) preserve the
Marvell copyright notice above.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/


#ifndef _TPM_INT_TYPES_H_
#define _TPM_INT_TYPES_H_


#ifdef __cplusplus
extern "C" {
#endif






typedef enum {
	TPM_INVALID_SECTION = -1,
	TPM_PNC_MAC_LEARN_ACL,
	TPM_DS_LOAD_BALANCE_ACL,
	TPM_CPU_LOOPBACK_ACL,
	TPM_L2_PRIM_ACL,
	TPM_L3_TYPE_ACL,
	TPM_IPV4_ACL,
	TPM_IPV4_MC,
	TPM_IPV6_GEN_ACL,
	TPM_IPV6_DIP_ACL,
	TPM_IPV6_MC_ACL,
	TPM_IPV6_NH_ACL,
	TPM_L4_ACL,
	TPM_CNM_MAIN_ACL,
	TPM_MAX_NUM_API_SECTIONS	/* Equals to number of entries in enum */
} tpm_api_sections_t;


/*************************************************************/
/*               DEFINITIONS                                 */
/*************************************************************/

#define     TPM_OK                          (0)
#define     TPM_FAIL                        (1)
#define     TPM_NOT_FOUND                   (2)

/* Structure for fields that may need to be parsed for internal functions (e.g. creating hardcoded pnc entries),
   these fields are not exposed to the API's */
typedef struct {
	uint8_t ipv4_ver;
	uint8_t ipv4_ver_mask;	/* 0-MASK ,  1-PARSE */
	uint8_t ipv4_ihl;
	uint8_t ipv4_ihl_mask;	/* 0-MASK ,  1-PARSE */
	uint16_t ipv4_totlen;
	uint16_t ipv4_totlen_mask;	/* 16-bits */
	uint16_t ipv4_ident;
	uint16_t ipv4_flags;	/* MF Flag at bit 0 */
	uint16_t ipv4_flags_mask;	/* 0-MASK ,  1-PARSE */
	uint8_t ipv4_frag_offset;
	uint8_t ipv4_frag_offset_mask;	/* 0-MASK ,  1-PARSE */
	uint8_t ipv4_ttl;
} tpm_ipv4_add_key_t;

typedef struct {
	uint8_t ipv6_hopl;
	uint16_t ipv6_totlen;
	uint16_t ipv6_totlen_mask;	/* 16-bits */
} tpm_ipv6_add_key_t;

typedef struct {
	uint8_t tcp_flags;
	uint8_t tcp_flags_mask;	/* Bit7-0 - signal each of the TCP flags */
} tpm_tcp_key_t;

#ifdef __cplusplus
}
#endif
#endif				/* _TPM_INT_TYPES_H_ */
