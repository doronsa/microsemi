/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/********************************************************************************
* tpm_print.h
*
* DESCRIPTION:
*               Traffic Processor Manager = TPM
*
* DEPENDENCIES:
*               None
*
* CREATED BY:   OctaviaP
*
* DATE CREATED:
*
* FILE REVISION NUMBER:
*               $Revision: 1.2 $
*
*
*******************************************************************************/

#ifndef _TPM_PRINT_H_
#define _TPM_PRINT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "tpm_internal_types.h"

#ifdef __cplusplus
}
#endif

/********************************************************************************/
/*                                Print Utils                                   */
/********************************************************************************/
#define BuildEnumString(param) { param, #param"\0" }

typedef struct {
	int enumPar;
	char enumString[128];
} db_enum_string_t;

char *db_mac_to_str(uint8_t *addr, char *str);
char *db_ipv4_to_str(uint8_t *ipaddr, char *str);
char *db_ipv6_to_str(uint8_t *ipaddr, char *str);

/* TODO - following functions should use db functions, instead of accessing DB directly */
void tpm_print_etherports(void);
void tpm_print_tx_modules(void);
void tpm_print_rx_modules(void);
void tpm_print_gmac_config(void);
void tpm_print_gmac_func(void);
void tpm_print_igmp(void);
void tpm_print_misc(void);
void tpm_print_owners(void);
void tpm_print_vlan_etype(void);
void tpm_print_mac_key(tpm_mac_key_t *mac_key);
void tpm_print_cpu_lpbk_entry(void);
void tpm_print_tcam_lu_entry(uint32_t owner_id, uint32_t api_group, uint32_t lu_num, uint32_t lu_reset);
void tpm_print_pnc_all_hit_counters(uint32_t owner_id, tpm_api_type_t api_type, uint32_t high_thresh_pkts,
				    uint8_t counters_reset, uint16_t valid_counters,
				    tpm_api_entry_count_t *count_array);

void tpm_print_vlan_key(tpm_vlan_key_t *vlan_key);
void tpm_print_ipv4_key(tpm_ipv4_acl_key_t *ipv4_key, tpm_ipv4_add_key_t *ipv4_add_key);
void tpm_print_l2_key(tpm_l2_acl_key_t *l2_key);
void tpm_print_l3_key(tpm_l3_type_key_t *l3_key);

void tpm_print_valid_api_sections(void);
void tpm_print_full_api_section(tpm_api_sections_t api_section);
void tpm_print_pnc_shadow_range(uint32_t valid_only, uint32_t start, uint32_t end);
void tpm_print_valid_pnc_ranges(void);
void tpm_print_init_tables(void);
void tpm_print_api_dump_all(void);
int tpm_print_pnc(void);
int  tpm_tcam_hw_hits(unsigned int print_pnc);
void tpm_tcam_hw_record(int port);
int  tpm_age_pnc_dump(void);
int  tpm_age_pnc_dump_live(void);
void tpm_print_pnc_field_desc(void);
void tpm_print_mc_vlan_cfg_all(void);
void tpm_print_section_free_szie(tpm_api_type_t api_type);
void tpm_print_gpon_omci_channel(void);
void tpm_print_epon_oam_channel(void);
void tpm_print_busy_apis(void);
void tpm_init_eth_cmplx_setup_error_print(uint32_t hwEthCmplx, bool sysfs_call);

tpm_error_code_t tpm_trace_status_print(void);
tpm_error_code_t tpm_trace_set(uint32_t level);
tpm_error_code_t tpm_trace_module_set(uint32_t  module, uint32_t  flag);


#endif /* _TPM_PRINT_H_*/


