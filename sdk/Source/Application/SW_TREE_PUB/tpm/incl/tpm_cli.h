/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/
/********************************************************************************
*      tpm_cli.h
*
* DESCRIPTION:
*
*
* CREATED BY:
*
* DATE CREATED: June 29, 2010
*
* DEPENDENCIES:
*
*
* FILE REVISION NUMBER:
*       $Revision: 1.16 $
*******************************************************************************/


#ifndef __TPM_CLI_H__
#define __TPM_CLI_H__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "clish/shell.h"


/********************************************************************************/
/*                      TPM CLI init functions                                 */
/********************************************************************************/
bool_t  tpm_cli_init                        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                      TPM CLI MIB REset functions                             */
/********************************************************************************/
bool_t  tpm_cli_mib_reset                   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                      TPM CLI show DB data functions                          */
/********************************************************************************/
bool_t  tpm_cli_print_vlan_table            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_vlan_table_by_name    (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_ipv6_key_table        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_ipv6_key_table_by_name(const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_ipv4_key_table        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_ipv4_key_table_by_name(const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_l3key_table           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_l3key_table_by_name   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_l2key_table           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_l2key_table_by_name   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_l2key_mac             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_l2key_vlan            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_l2key_pppoe           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_l2key_ety             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_l2key_gem             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_mod_table             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_mod_table_by_name     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_frwd_table            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_frwd_table_by_name    (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                      TPM CLI add DB functions                                */
/********************************************************************************/
bool_t  tpm_cli_add_vlan                    (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_l2key_ety               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_l2key_gem               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_l2key_pppoe             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_l2key_mac_address       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_l2key_vlan              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_pkt_frwd_data           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_mod_pppoe               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_mod_dscp                (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_mod_protocol            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_mod_ipv4_addr           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_mod_ipv4_ports          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_mod_ipv6_addr           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_mod_ipv6_ports          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_mod_vlan                (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_mod_mac_address         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_l3key_ety               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_l3key_pppoe             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_ipv4_key_dscp           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_ipv4_key_protocol       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_ipv4_key_ports          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_ipv4_key_addr           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_ipv6_key_dscp           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_ipv6_key_protocol       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_ipv6_key_ports          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_ipv6_key_addr           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                      TPM CLI add rule functions                              */
/********************************************************************************/
bool_t  tpm_cli_add_l2_prim_rule            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_l3_prim_rule            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_ipv4_prim_rule          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
#ifdef OLD_TPM_APIS
bool_t  tpm_cli_add_ipv6_prim_rule          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
#endif /*OLD_TPM_APIS*/
#ifdef OLD_TPM_APIS
bool_t  tpm_cli_add_ipv4_dscp_rule          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_ipv6_dscp_rule          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
#endif /*OLD_TPM_APIS*/

/********************************************************************************/
/*                      TPM CLI MC functions                                    */
/********************************************************************************/
bool_t  tpm_cli_add_ipv4_mc_stream          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_update_ipv4_mc_stream       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
#ifdef OLD_TPM_APIS
bool_t  tpm_cli_add_ipv6_mc_stream          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

bool_t  tpm_cli_update_ipv6_mc_stream       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
#endif /* OLD_TPM_APIS*/

/********************************************************************************/
/*                              TPM CLI get functions                           */
/********************************************************************************/
bool_t  tpm_cli_get_section_free_size       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_get_next_valid_rule         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_get_ownerid                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_get_omci_channel            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_get_oam_channel             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_get_pm_counters             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_clear_pm_counters           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                      TPM CLI delete functions                                */
/********************************************************************************/
bool_t  tpm_cli_erase_section               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_l2_prim_rule            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_l3_acl_rule             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_ipv4_acl_rule           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
#ifdef OLD_TPM_APIS
bool_t  tpm_cli_del_ipv6_acl_rule           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_ipv4_dscp_rule          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_ipv6_dscp_rule          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
#endif /*OLD_TPM_APIS*/

bool_t  tpm_cli_del_vid                     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_ipv4_mc                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
#ifdef OLD_TPM_APIS
bool_t  tpm_cli_del_ipv6_mc                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
#endif /*OLD_TPM_APIS*/

bool_t  tpm_cli_del_oam_channel             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_omci_channel            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_static_mac              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                      TPM CLI delete DB data functions                        */
/********************************************************************************/
bool_t  tpm_cli_del_vlan_entry              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_mod_entry               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_frwd_entry              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_l2_key_entry            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_l3_key_entry            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_ipv4_key_entry          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_ipv6_key_entry          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                      TPM CLI owner functions                                 */
/********************************************************************************/
bool_t  tpm_cli_create_owner                (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_ownership               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                      TPM CLI set functions                                   */
/********************************************************************************/
bool_t  tpm_cli_set_pkt_tagging             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_vid                     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_vid_filter              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_port_flooding           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_add_static_mac              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_max_macs                (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_queue_weight            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_pcl_uni_rate            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_pcl_tc_rate             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_uni_rate_limit          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_wan_up_rate_limit       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_wan_up_q_rate_limit     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_wan_down_rate_limit     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_wan_down_q_rate_limit   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_wan_sched               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_sched            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_autoneg 			(const clish_shell_t    *instance,
                               		     	 const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_restart_autoneg 	(const clish_shell_t    *instance,
                               		     	 const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_admin 			(const clish_shell_t    *instance,
                               		         const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_flow_control		(const clish_shell_t    *instance,
                               		     	 const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_speed 			(const clish_shell_t    *instance,
											 const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_loopback         (const clish_shell_t    *instance,
                               			     const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_duplex           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_default_vlan     (const clish_shell_t    *instance,
                               				 const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_default_pri      (const clish_shell_t    *instance,
                               				 const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_mac_aging_time       (const clish_shell_t    *instance,
                               				 const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_mac_learn        (const clish_shell_t    *instance,
                               				 const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_port_mirror      (const clish_shell_t    *instance,
                               				 const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_mtu_size         (const clish_shell_t    *instance,
                               				 const lub_argv_t       *argv);
bool_t  tpm_sw_cli_set_uni_isolate_vector   (const clish_shell_t    *instance,
                               				 const lub_argv_t       *argv);
bool_t  tpm_sw_cli_add_group_vid            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_del_group_vid            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_clear_dynamic_mac        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_autoneg          (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_admin            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_link             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_flow_control     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_flow_control_state (const clish_shell_t    *instance,
                                               const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_duplex           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_duplex_state     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_speed            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_speed_state      (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_loopback         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_port_flooding        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_default_vlan     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_default_pri      (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_mac_aging_time       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_mac_learn        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_port_mirror      (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_mtu_size         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_sw_cli_get_uni_isolate_vector   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_omci_channel            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_oam_channel             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
//bool_t  tpm_cli_igmp_config                 (const clish_shell_t    *instance,
//                                             const lub_argv_t       *argv);

bool_t tpm_cli_set_port_igmp_frwd_mode      (const clish_shell_t *instance,
                                             const lub_argv_t *argv);
bool_t tpm_cli_set_igmp_cpu_rx_queue        (const clish_shell_t *instance,
                                             const lub_argv_t *argv);


/********************************************************************************/
/*                  TPM CLI debug trace functions                               */
/********************************************************************************/
bool_t  tpm_cli_print_trace                 (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_trace_level             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_set_trace_module            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_del_trace_module            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                  TPM CLI debug print functions                               */
/********************************************************************************/
bool_t  tpm_cli_print_misc                  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_igmp                  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_owners                (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_vlan_etype            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_init_tables           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_eth_ports             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_tx_modules            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_rx_modules            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_gmac_func             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_gmac_conf             (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_api_full_range        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_api_valid_range       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_shadow_mod_range      (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_shadow_pnc_range      (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_print_valid_pnc_range       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                  TPM CLI debug Modification Z2 functions                     */
/********************************************************************************/
bool_t  tpm_cli_mod2_print_jump_area_range  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_mod2_print_jump_area        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_mod2_print_main_area_range  (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_mod2_print_main_area        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_mod2_print_config           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_mod2_print_last_pattern     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

/********************************************************************************/
/*                  TPM CLI CPU Modification functions                          */
/********************************************************************************/
bool_t  tpm_cli_mod_add                     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_mod_get                     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_mod_del                     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t  tpm_cli_mod_inv                     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);


/********************************************************************************/
/*                  TPM CLI switch functions                                    */
/********************************************************************************/
bool_t tpm_sw_cli_add_static_mac            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_sw_cli_del_static_mac            (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_sw_cli_set_port_max_macs         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_sw_cli_set_port_flooding         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_sw_cli_set_port_tagged           (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_sw_cli_set_port_untagged         (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_sw_cli_port_add_vid              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_sw_cli_port_del_vid              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_sw_cli_port_set_vid_filter       (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_sw_cli_set_q_weight              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_sw_cli_set_ingr_police_rate_per_port(const clish_shell_t    *instance,
                                                const lub_argv_t       *argv);
bool_t tpm_sw_cli_set_tc_ingr_police_rate   (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_sw_cli_set_egr_rate_limit        (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);

bool_t appl_shell        (void);


/********************************************************************************/
/*                  TPM CLI rate limit functions                                */
/********************************************************************************/
bool_t tpm_cli_set_queue_sched              (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_cli_set_queue_rate               (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
bool_t tpm_cli_set_rate                     (const clish_shell_t    *instance,
                                             const lub_argv_t       *argv);
#endif /*__TPM_CLI_H__*/
