/*******************************************************************************
Copyright (C) Marvell International Ltd. and its affiliates

This software file (the "File") is owned and distributed by Marvell
International Ltd. and/or its affiliates ("Marvell") under the following
licensing terms.

********************************************************************************
Marvell Commercial License Option

If you received this File from Marvell and you have entered into a commercial
license agreement (a "Commercial License") with Marvell, the File is licensed
to you under the terms of the applicable Commercial License.

*******************************************************************************/

/********************************************************************************/
/*                      TPM CLI init functions                                 */
/********************************************************************************/
    {"tpm_cli_mib_reset",                   tpm_cli_mib_reset},
/********************************************************************************/
/*                      TPM CLI MIB REset functions                             */
/********************************************************************************/
    {"tpm_cli_init",                        tpm_cli_init},
/********************************************************************************/
/*                      TPM CLI owner functions                                 */
/********************************************************************************/
    {"tpm_cli_create_owner",                tpm_cli_create_owner},
    {"tpm_cli_set_ownership",               tpm_cli_set_ownership},
/********************************************************************************/
/*                      TPM CLI add DB functions                                */
/********************************************************************************/
  {"tpm_cli_add_vlan",                      tpm_cli_add_vlan},
  {"tpm_cli_add_pkt_frwd_data",             tpm_cli_add_pkt_frwd_data},
  {"tpm_cli_add_mod_vlan",                  tpm_cli_add_mod_vlan},
  {"tpm_cli_add_mod_ipv6_ports",            tpm_cli_add_mod_ipv6_ports},
  {"tpm_cli_add_mod_ipv6_addr",             tpm_cli_add_mod_ipv6_addr},
  {"tpm_cli_add_mod_ipv4_ports",            tpm_cli_add_mod_ipv4_ports},
  {"tpm_cli_add_mod_ipv4_addr",             tpm_cli_add_mod_ipv4_addr},
  {"tpm_cli_add_mod_protocol",              tpm_cli_add_mod_protocol},
  {"tpm_cli_add_mod_dscp",                  tpm_cli_add_mod_dscp},
  {"tpm_cli_add_mod_pppoe",                 tpm_cli_add_mod_pppoe},
  {"tpm_cli_add_mod_mac_address",           tpm_cli_add_mod_mac_address},
  {"tpm_cli_add_l2key_mac_address",         tpm_cli_add_l2key_mac_address},
  {"tpm_cli_add_l2key_vlan",                tpm_cli_add_l2key_vlan},
  {"tpm_cli_add_l2key_ety",                 tpm_cli_add_l2key_ety},
  {"tpm_cli_add_l2key_pppoe",               tpm_cli_add_l2key_pppoe},
  {"tpm_cli_add_l2key_gem",                 tpm_cli_add_l2key_gem},
  {"tpm_cli_add_l3key_ety",                 tpm_cli_add_l3key_ety},
  {"tpm_cli_add_l3key_pppoe",               tpm_cli_add_l3key_pppoe},
  {"tpm_cli_add_ipv4_key_dscp",             tpm_cli_add_ipv4_key_dscp},
  {"tpm_cli_add_ipv4_key_protocol",         tpm_cli_add_ipv4_key_protocol},
  {"tpm_cli_add_ipv4_key_ports",            tpm_cli_add_ipv4_key_ports},
  {"tpm_cli_add_ipv4_key_addr",             tpm_cli_add_ipv4_key_addr},
  {"tpm_cli_add_ipv6_key_dscp",             tpm_cli_add_ipv6_key_dscp},
  {"tpm_cli_add_ipv6_key_protocol",         tpm_cli_add_ipv6_key_protocol},
  {"tpm_cli_add_ipv6_key_ports",            tpm_cli_add_ipv6_key_ports},
  {"tpm_cli_add_ipv6_key_addr",             tpm_cli_add_ipv6_key_addr},
/********************************************************************************/
/*                      TPM CLI add rule functions                              */
/********************************************************************************/
  {"tpm_cli_add_l2_prim_rule",              tpm_cli_add_l2_prim_rule},
  {"tpm_cli_add_l3_prim_rule",              tpm_cli_add_l3_prim_rule},
  {"tpm_cli_add_ipv4_prim_rule",            tpm_cli_add_ipv4_prim_rule},
#ifdef OLD_TPM_APIS
  {"tpm_cli_add_ipv4_dscp_rule",            tpm_cli_add_ipv4_dscp_rule},
  {"tpm_cli_add_ipv6_prim_rule",            tpm_cli_add_ipv6_prim_rule},
  {"tpm_cli_add_ipv6_dscp_rule",            tpm_cli_add_ipv6_dscp_rule},
#endif /*OLD_TPM_APIS*/
/********************************************************************************/
/*                      TPM CLI MC functions                                    */
/********************************************************************************/
  {"tpm_cli_add_ipv4_mc_stream",            tpm_cli_add_ipv4_mc_stream},
  {"tpm_cli_update_ipv4_mc_stream",         tpm_cli_update_ipv4_mc_stream},
#ifdef OLD_TPM_APIS
  {"tpm_cli_add_ipv6_mc_stream",            tpm_cli_add_ipv6_mc_stream},
  {"tpm_cli_update_ipv6_mc_stream",         tpm_cli_update_ipv6_mc_stream},
#endif  /*OLD_TPM_APIS*/
/********************************************************************************/
/*                      TPM CLI set functions                                   */
/********************************************************************************/
  {"tpm_cli_set_pkt_tagging",               tpm_cli_set_pkt_tagging},
  {"tpm_cli_add_vid",                       tpm_cli_add_vid},
  {"tpm_cli_set_vid_filter",                tpm_cli_set_vid_filter},
  {"tpm_cli_add_static_mac",                tpm_cli_add_static_mac},
  {"tpm_cli_set_max_macs",                  tpm_cli_set_max_macs},
  {"tpm_cli_set_port_flooding",             tpm_cli_set_port_flooding},
  {"tpm_cli_set_queue_weight",              tpm_cli_set_queue_weight},
  {"tpm_cli_set_pcl_uni_rate",              tpm_cli_set_pcl_uni_rate},
  {"tpm_cli_set_pcl_tc_rate",               tpm_cli_set_pcl_tc_rate},
  {"tpm_cli_set_uni_rate_limit",            tpm_cli_set_uni_rate_limit},
  {"tpm_cli_set_wan_up_rate_limit",         tpm_cli_set_wan_up_rate_limit},
  {"tpm_cli_set_wan_up_q_rate_limit",       tpm_cli_set_wan_up_q_rate_limit},
  {"tpm_cli_set_wan_down_rate_limit",       tpm_cli_set_wan_down_rate_limit},
  {"tpm_cli_set_wan_down_q_rate_limit",     tpm_cli_set_wan_down_q_rate_limit},
  {"tpm_cli_set_wan_sched",                 tpm_cli_set_wan_sched},
  {"tpm_sw_cli_set_uni_sched",              tpm_sw_cli_set_uni_sched},
  {"tpm_sw_cli_set_uni_autoneg",            tpm_sw_cli_set_uni_autoneg},
  {"tpm_sw_cli_set_uni_restart_autoneg",    tpm_sw_cli_set_uni_restart_autoneg},
  {"tpm_sw_cli_set_uni_admin",              tpm_sw_cli_set_uni_admin},
  {"tpm_sw_cli_set_uni_flow_control",       tpm_sw_cli_set_uni_flow_control},
  {"tpm_sw_cli_set_uni_speed",              tpm_sw_cli_set_uni_speed},
  {"tpm_sw_cli_set_uni_loopback",           tpm_sw_cli_set_uni_loopback},
  {"tpm_sw_cli_set_uni_duplex",             tpm_sw_cli_set_uni_duplex},
  {"tpm_sw_cli_set_uni_default_vlan",       tpm_sw_cli_set_uni_default_vlan},
  {"tpm_sw_cli_set_uni_default_pri",        tpm_sw_cli_set_uni_default_pri},
  {"tpm_sw_cli_set_mac_aging_time",         tpm_sw_cli_set_mac_aging_time},
  {"tpm_sw_cli_set_uni_mac_learn",          tpm_sw_cli_set_uni_mac_learn},
  {"tpm_sw_cli_set_uni_port_mirror",        tpm_sw_cli_set_uni_port_mirror},
  {"tpm_sw_cli_set_uni_mtu_size",           tpm_sw_cli_set_uni_mtu_size},
  {"tpm_sw_cli_set_uni_isolate_vector",     tpm_sw_cli_set_uni_isolate_vector},
  {"tpm_sw_cli_add_group_vid",              tpm_sw_cli_add_group_vid},
  {"tpm_sw_cli_del_group_vid",              tpm_sw_cli_del_group_vid},
  {"tpm_sw_cli_clear_dynamic_mac",          tpm_sw_cli_clear_dynamic_mac},
  {"tpm_sw_cli_get_uni_autoneg",            tpm_sw_cli_get_uni_autoneg},
  {"tpm_sw_cli_get_uni_admin",              tpm_sw_cli_get_uni_admin},
  {"tpm_sw_cli_get_uni_link",               tpm_sw_cli_get_uni_link},
  {"tpm_sw_cli_get_uni_flow_control",       tpm_sw_cli_get_uni_flow_control},
  {"tpm_sw_cli_get_uni_flow_control_state", tpm_sw_cli_get_uni_flow_control_state},
  {"tpm_sw_cli_get_uni_duplex",             tpm_sw_cli_get_uni_duplex},
  {"tpm_sw_cli_get_uni_duplex_state",       tpm_sw_cli_get_uni_duplex_state},
  {"tpm_sw_cli_get_uni_speed",              tpm_sw_cli_get_uni_speed},
  {"tpm_sw_cli_get_uni_speed_state",        tpm_sw_cli_get_uni_speed_state},
  {"tpm_sw_cli_get_uni_loopback",           tpm_sw_cli_get_uni_loopback},
  {"tpm_sw_cli_get_port_flooding",          tpm_sw_cli_get_port_flooding},
  {"tpm_sw_cli_get_uni_default_vlan",       tpm_sw_cli_get_uni_default_vlan},
  {"tpm_sw_cli_get_uni_default_pri",        tpm_sw_cli_get_uni_default_pri},
  {"tpm_sw_cli_get_mac_aging_time",         tpm_sw_cli_get_mac_aging_time},
  {"tpm_sw_cli_get_uni_mac_learn",          tpm_sw_cli_get_uni_mac_learn},
  {"tpm_sw_cli_get_uni_port_mirror",        tpm_sw_cli_get_uni_port_mirror},
  {"tpm_sw_cli_get_uni_mtu_size",           tpm_sw_cli_get_uni_mtu_size},
  {"tpm_sw_cli_get_uni_isolate_vector",     tpm_sw_cli_get_uni_isolate_vector},
  {"tpm_cli_set_omci_channel",              tpm_cli_set_omci_channel},
  {"tpm_cli_set_oam_channel",               tpm_cli_set_oam_channel},
  //{"tpm_cli_igmp_config",                   tpm_cli_igmp_config},
  {"tpm_cli_set_port_igmp_frwd_mode",       tpm_cli_set_port_igmp_frwd_mode},
  {"tpm_cli_set_igmp_cpu_rx_queue",         tpm_cli_set_igmp_cpu_rx_queue},
/********************************************************************************/
/*                      TPM CLI get functions                                   */
/********************************************************************************/
  {"tpm_cli_get_section_free_size",         tpm_cli_get_section_free_size},
  {"tpm_cli_get_next_valid_rule",           tpm_cli_get_next_valid_rule},
  {"tpm_cli_get_ownerid",                   tpm_cli_get_ownerid},
  {"tpm_cli_get_omci_channel",              tpm_cli_get_omci_channel},
  {"tpm_cli_get_oam_channel",               tpm_cli_get_oam_channel},
  {"tpm_cli_get_pm_counters",               tpm_cli_get_pm_counters},
  {"tpm_cli_clear_pm_counters",             tpm_cli_clear_pm_counters},

/********************************************************************************/
/*                      TPM CLI delete functions                                */
/********************************************************************************/
  {"tpm_cli_erase_section",                 tpm_cli_erase_section},
  {"tpm_cli_del_l2_prim_rule",              tpm_cli_del_l2_prim_rule},
  {"tpm_cli_del_l3_acl_rule",               tpm_cli_del_l3_acl_rule},
  {"tpm_cli_del_ipv4_acl_rule",             tpm_cli_del_ipv4_acl_rule},
#ifdef OLD_TPM_APIS
  {"tpm_cli_del_ipv6_acl_rule",             tpm_cli_del_ipv6_acl_rule},
  {"tpm_cli_del_ipv4_dscp_rule",            tpm_cli_del_ipv4_dscp_rule},
  {"tpm_cli_del_ipv6_dscp_rule",            tpm_cli_del_ipv6_dscp_rule},
#endif /*OLD_TPM_APIS*/
  {"tpm_cli_del_vid",                       tpm_cli_del_vid},
  {"tpm_cli_del_ipv4_mc",                   tpm_cli_del_ipv4_mc},
#ifdef OLD_TPM_APIS
  {"tpm_cli_del_ipv6_mc",                   tpm_cli_del_ipv6_mc},
#endif /*OLD_TPM_APIS*/
  {"tpm_cli_del_oam_channel",               tpm_cli_del_oam_channel},
  {"tpm_cli_del_omci_channel",              tpm_cli_del_omci_channel},
  {"tpm_cli_del_static_mac",                tpm_cli_del_static_mac},
/********************************************************************************/
/*                      TPM CLI delete DB data functions                        */
/********************************************************************************/
  {"tpm_cli_del_vlan_entry",                tpm_cli_del_vlan_entry},
  {"tpm_cli_del_mod_entry",                 tpm_cli_del_mod_entry},
  {"tpm_cli_del_frwd_entry",                tpm_cli_del_frwd_entry},
  {"tpm_cli_del_l2_key_entry",              tpm_cli_del_l2_key_entry},
  {"tpm_cli_del_l3_key_entry",              tpm_cli_del_l3_key_entry},
  {"tpm_cli_del_ipv4_key_entry",            tpm_cli_del_ipv4_key_entry},
  {"tpm_cli_del_ipv6_key_entry",            tpm_cli_del_ipv6_key_entry},
/********************************************************************************/
/*                      TPM CLI show DB data functions                          */
/********************************************************************************/
  {"tpm_cli_print_vlan_table",              tpm_cli_print_vlan_table},
  {"tpm_cli_print_vlan_table_by_name",      tpm_cli_print_vlan_table_by_name},
  {"tpm_cli_print_ipv4_key_table",          tpm_cli_print_ipv4_key_table},
  {"tpm_cli_print_ipv4_key_table_by_name",  tpm_cli_print_ipv4_key_table_by_name},
  {"tpm_cli_print_ipv6_key_table",          tpm_cli_print_ipv6_key_table},
  {"tpm_cli_print_ipv6_key_table_by_name",  tpm_cli_print_ipv6_key_table_by_name},
  {"tpm_cli_print_l3key_table",             tpm_cli_print_l3key_table},
  {"tpm_cli_print_l3key_table_by_name",     tpm_cli_print_l3key_table_by_name},
  {"tpm_cli_print_l2key_table",             tpm_cli_print_l2key_table},
  {"tpm_cli_print_l2key_table_by_name",     tpm_cli_print_l2key_table_by_name},
  {"tpm_cli_print_l2key_mac",               tpm_cli_print_l2key_mac},
  {"tpm_cli_print_l2key_vlan",              tpm_cli_print_l2key_vlan},
  {"tpm_cli_print_l2key_pppoe",             tpm_cli_print_l2key_pppoe},
  {"tpm_cli_print_l2key_ety",               tpm_cli_print_l2key_ety},
  {"tpm_cli_print_l2key_gem",               tpm_cli_print_l2key_gem},
  {"tpm_cli_print_mod_table",               tpm_cli_print_mod_table},
  {"tpm_cli_print_mod_table_by_name",       tpm_cli_print_mod_table_by_name},
  {"tpm_cli_print_frwd_table",              tpm_cli_print_frwd_table},
  {"tpm_cli_print_frwd_table_by_name",      tpm_cli_print_frwd_table_by_name},
/********************************************************************************/
/*                      TPM CLI debug functions                                 */
/********************************************************************************/
  {"tpm_cli_print_trace",                   tpm_cli_print_trace},
  {"tpm_cli_set_trace_level",               tpm_cli_set_trace_level},
  {"tpm_cli_set_trace_module",              tpm_cli_set_trace_module},
  {"tpm_cli_del_trace_module",              tpm_cli_del_trace_module},
  {"tpm_cli_print_misc",                    tpm_cli_print_misc},
  {"tpm_cli_print_igmp",                    tpm_cli_print_igmp},
  {"tpm_cli_print_owners",                  tpm_cli_print_owners},
  {"tpm_cli_print_vlan_etype",              tpm_cli_print_vlan_etype},
  {"tpm_cli_print_init_tables",             tpm_cli_print_init_tables},
  {"tpm_cli_print_eth_ports",               tpm_cli_print_eth_ports},
  {"tpm_cli_print_tx_modules",              tpm_cli_print_tx_modules},
  {"tpm_cli_print_rx_modules",              tpm_cli_print_rx_modules},
  {"tpm_cli_print_gmac_func",               tpm_cli_print_gmac_func},
  {"tpm_cli_print_gmac_conf",               tpm_cli_print_gmac_conf},
  {"tpm_cli_print_api_full_range",          tpm_cli_print_api_full_range},
  {"tpm_cli_print_api_valid_range",         tpm_cli_print_api_valid_range},
  {"tpm_cli_print_shadow_mod_range",        tpm_cli_print_shadow_mod_range},
  {"tpm_cli_print_shadow_pnc_range",        tpm_cli_print_shadow_pnc_range},
  {"tpm_cli_print_valid_pnc_range",         tpm_cli_print_valid_pnc_range},
/********************************************************************************/
/*                      TPM CLI switch functions                                */
/********************************************************************************/
  {"add_static_mac",                        tpm_sw_cli_add_static_mac},
  {"port_add_vid",                          tpm_sw_cli_port_add_vid},
  {"del_static_mac",                        tpm_sw_cli_del_static_mac},
  {"port_del_vid",                          tpm_sw_cli_port_del_vid},
  {"set_port_max_macs",                     tpm_sw_cli_set_port_max_macs},
  {"set_port_flooding",                     tpm_sw_cli_set_port_flooding},
  {"set_port_tagged",                       tpm_sw_cli_set_port_tagged},
  {"set_port_untagged",                     tpm_sw_cli_set_port_untagged},
  {"port_set_vid_filter",                   tpm_sw_cli_port_set_vid_filter},
  {"set_q_weight",                          tpm_sw_cli_set_q_weight},
  {"set_ingr_police_rate_per_port",         tpm_sw_cli_set_ingr_police_rate_per_port},
  {"set_tc_ingr_police_rate",               tpm_sw_cli_set_tc_ingr_police_rate},
  {"set_egr_rate_limit",                    tpm_sw_cli_set_egr_rate_limit},
  /********************************************************************************/
  /*                      Shell                                                   */
  /********************************************************************************/
  {"appl_shell",                            appl_shell},
  /********************************************************************************/
  /*                  TPM CLI debug Modification Z2 functions                     */
  /********************************************************************************/
  {"tpm_cli_mod2_print_jump_area_range",    tpm_cli_mod2_print_jump_area_range},
  {"tpm_cli_mod2_print_jump_area",          tpm_cli_mod2_print_jump_area},
  {"tpm_cli_mod2_print_main_area_range",    tpm_cli_mod2_print_main_area_range},
  {"tpm_cli_mod2_print_main_area",          tpm_cli_mod2_print_main_area},
  {"tpm_cli_mod2_print_config",             tpm_cli_mod2_print_config},
  {"tpm_cli_mod2_print_last_pattern",       tpm_cli_mod2_print_last_pattern},
  /********************************************************************************/
  /*                  TPM CLI CPU Modification functions                          */
  /********************************************************************************/
  {"tpm_cli_mod_add",                       tpm_cli_mod_add},
  {"tpm_cli_mod_get",                       tpm_cli_mod_get},
  {"tpm_cli_mod_del",                       tpm_cli_mod_del},
  {"tpm_cli_mod_inv",                       tpm_cli_mod_inv},
  /********************************************************************************/
  /*                      TPM CLI rate limit functions                            */
  /********************************************************************************/
  {"tpm_cli_set_queue_sched",               tpm_cli_set_queue_sched},
  {"tpm_cli_set_queue_rate",                tpm_cli_set_queue_rate},
  {"tpm_cli_set_rate",                      tpm_cli_set_rate},

