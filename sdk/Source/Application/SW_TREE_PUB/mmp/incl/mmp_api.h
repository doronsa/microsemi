/************************************************************************
* Copyright (C) 2010-2012, Marvell Technology Group Ltd.
* All Rights Reserved.
* 
* This is UNPUBLISHED PROPRIETARY SOURCE CODE of Marvell Technology Group;
* the contents of this file may not be disclosed to third parties, copied
* or duplicated in any form, in whole or in part, without the prior
* written permission of Marvell Technology Group.
* 
* mmp_api.h
*
* Description:
*       Marvell Media Processing Engine API
*
*************************************************************************/

#ifndef _MMP_API_H_
#define _MMP_API_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "mmp_types.h"


/*
 * mmp_init
 *
 * Description:
 *       Initialize MMP engine.
 *
 * Parameters:
 *       mmp_config - MMP engine configuration profile in XML format.
 *
 * Returns:
 *        MMP_OK              - On success.
 *        MMP_CONFIG_ERROR    - On configuration error.
 *        MMP_OUT_OF_MEMORY   - On out of memory error.
 *        MMP_TDM_INIT_FAILED - On TDM unit initialization failure.
 */
int32_t mmp_init(const char* mmp_config);


/*
 * mmp_shutdown
 *
 * Description:
 *       Shutdown MMP engine.
 *
 * Parameters:
 *       None.
 *
 * Returns:
 *       MMP_OK - The function always succeeds.
 *
 */
int32_t mmp_shutdown(void);


/*
 * mmp_open_phone_line
 *
 * Description:
 *
 * Activate a local phone (FXS, FXO or Speakerphone) line.
 * Line Echo Cancellation (LEC) is enabled by default.
 *
 * Parameters:
 * 
 * line_id - ID of of a phone line to open matching the value of id attribute 
 *           in the corresponding "PhoneLineSettings/Line" XML configuration 
 *           section. The valid range is 0..63.
 *
 * Returns:
 * 
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 * 
 */
int32_t mmp_open_phone_line(int32_t line_id);


/*
 * mmp_open_ip_line
 *
 * Description:
 *
 * Create an IP connection suitable for voice, voice-band-data or
 * fax relay communication. By default, IP line is created in VoIP mode 
 * with codec G711a, payload type 8 and packetization time 10ms.
 *
 * Parameters:
 *
 * local_ip    - Local IP address to use for network communication with
 *               remote party.
 * local_port  - Local network port to use for network communication with
 *               remote party.
 * remote_ip   - IP address of the remote party.
 * remote_port - Network port of the remote party.
 * media_path  - media path for the line
 * line_id     - Returned ID of the new IP line.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK. 
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_open_ip_line(mmp_ipaddr_t     local_ip,
                         uint16_t         local_port,
                         mmp_ipaddr_t     remote_ip,
                         uint16_t         remote_port,
                         mmp_media_path_t media_path,
                         int32_t*         line_id);


/*
 * mmp_open_playback_line
 *
 * Description:
 *
 * Create a playback line.
 *
 * Parameters:
 *
 * codec          - Voice codec to use.
 * rx_frames_num  - [DEPRECATED] Number of frames to receive in the receive callback.
 * line_id        - Returned ID of the playback line.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK. 
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_open_playback_line(mmp_codec_type_t codec, 
                               int32_t          rx_frames_num,
                               int32_t         *line_id);


/*
 * mmp_set_ip_params
 *
 * Description:
 *
 * Modify IP parameters of an existing IP line.
 * The existing VoIP, FoIP or VBD sessions are terminated.
 *
 * Parameters:
 *
 * local_ip    - Local IP address to use for network communication with
 *               remote party. If zero is provided, the local IP address will be 
 *               set according to the remote peer address.
 * local_port  - Local network port to use for network communication with
 *               remote party. If zero is provided an available will be 
 *               chosen by MMP automatically.
 * remote_ip   - IP address of the remote party. A zero address and port will 
 *               cause MMP to accept the first call arriving.
 * remote_port - Network port of the remote party. A zero address and port will 
 *               cause MMP to accept the first call arriving.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_ip_params(int32_t       line_id,
                          mmp_ipaddr_t  local_ip,
                          uint16_t      local_port,
                          mmp_ipaddr_t  remote_ip,
                          uint16_t      remote_port);

/* 
 * mmp_bind_to_device
 *
 * Description:
 *
 * Bind IP line to a physical network interface.
 *
 * Parameters:
 * 
 * line_id - IP line id.
 * dev_name - Network interface name (for example "eth0")
 */
int32_t mmp_bind_to_device(int32_t line_id,
                           char   *dev_name);
/*
 * mmp_set_tos
 *
 * Description:
 *
 * Modify the current default ToS setting used when creating IP lines
 * with mmp_open_ip_line API. The existing IP lines are not affected.
 *
 * Parameters:
 *
 * tos - New default ToS setting.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK. 
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_tos(uint8_t tos);

/*
 * mmp_get_tos
 *
 * Description:
 *
 * Retrieve the current default ToS setting used for new IP lines created
 * using mmp_open_ip_line API.
 *
 * Parameters:
 *
 * tos - Pointer to the returned current ToS setting.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_tos(uint8_t *tos);

/*
 * mmp_set_voip_mode
 *
 * Description: 
 *
 * Switch IP or phone line to VoIP mode or change VoIP parameters.
 * When changing to VOIP from FOIP, the current FOIP session is terminated.
 *
 * When changing from VOIP to VOIP, the current ssrc and timestamp attributtes
 * are preserved.
 * 
 * Parameters:
 *
 * line_id           - ID of an IP line.
 * params            - Parameters for the VoIP session.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INVALID_IP_ADDR
 *    MMP_BIND_ERROR
 *    MMP_INACTIVE_LINE
 *
 */
int32_t mmp_set_voip_mode(int32_t                  line_id,
                          const mmp_voip_params_t *params);

/*
 * mmp_set_foip_mode
 *
 * Description:
 * 
 * Switch IP or phone line to FoIP (Fax T.38) mode.
 * When changing to FOIP from VOIP or VBD, the current session is terminated.
 *
 * Parameters:
 *
 * line_id - ID of an IP line.
 * params  - Parameters for the FoIP session.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *
 */
int32_t mmp_set_foip_mode(int32_t                  line_id,
                          const mmp_foip_params_t *params);

 
/*
 * mmp_set_vbd_mode
 *
 * Description:
 * 
 * Switch IP or phone line to VBD mode.
 * When changing to FOIP from VOIP or VBD, the current session is terminated.
 *
 * Parameters:
 * 
 * line_id - ID of an IP line.
 * params  - Parameters for the VBD session. 
 *
 * Returns:
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INVALID_PARAM2
 *
 */
int32_t mmp_set_vbd_mode(int32_t           line_id,
                         mmp_vbd_params_t *params);


/*
 * mmp_close_line
 *
 * Description:
 *
 * Close an active media line.
 *
 * Parameters:
 *
 * line_id - ID of a phone, IP or playback line.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INACTIVE_LINE
 *
 */
int32_t mmp_close_line(int32_t line_id);

/*
 * mmp_create_call
 *
 * Description:
 *
 * Create a media context which can be used for bridging two or more media lines.
 * Media lines can be added or removed from the call using mmp_join_call and
 * mmp_leave_call, respectively.
 *
 * Parameters:
 * call_id - Returned ID of a new voice call
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 */
int32_t mmp_create_call(int32_t *call_id);


/*
 * mmp_join_call
 *
 * Description:
 *
 * Join a voice call. Up to six participating media lines are allowed.
 *
 * Parameters:
 *
 * call_id  - ID of a call to join.
 * line_id  - ID of a media line joining the call.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_join_call(int32_t call_id,
                      int32_t line_id);


/*
 * mmp_leave_call
 *
 * Description: Leave a voice call.
 *
 * Parameters:
 *
 * call_id - ID of a call to leave.
 * line_id - ID of a voice line leaving the call.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_leave_call(int32_t call_id,
                       int32_t line_id);


/*
 * mmp_destroy_call
 *
 * Description:
 *
 * Destroy a media context. All participating media lines are removed from the call
 * automatically, their state and parameters being preserved.
 *
 * Parameters:
 *
 * call_id - ID of a call to leave
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_destroy_call(int32_t call_id);


/*
 * mmp_tonegen_set_tone
 *
 * Description:
 *
 * Update Universal Tone Generation tone entry.
 *
 * Parameters:
 *
 * line_id         - ID of a media line.
 * mpath           - Media path direction to start the tone at.
 * tone_profile_id - Tone profile id (0 ... 9).
 * tone_id         - Tone ID in the tone profile (0 ... 23).
 * tone            - Generated tone parameters.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INACTIVE_LINE
 *    MMP_INVALID_PROF_ID
 *    MMP_INVALID_TONE_ID
 *    MMP_INVALID_PARAM3
 *
 */
int32_t mmp_tonegen_set_tone(int16_t         profile_id,
                             int16_t         tone_id,
                             mmp_utg_tone_t *tone);


/*
 * mmp_tone_start
 *
 * Description:
 *
 * Start playing a tone on a media line.
 *
 * Parameters:
 *
 * line_id         - ID of a media line.
 * mpath           - Media path direction to start the tone at.
 * tone_profile_id - Tone profile id (0 ... 9).
 * tone_id         - Tone ID in the tone profile (0 ... 23).
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INACTIVE_LINE
 *    MMP_INVALID_PROF_ID
 *    MMP_INVALID_TONE_ID
 *    MMP_INVALID_DIR
 *
 */
int32_t mmp_tone_start( int32_t          line_id,
                        mmp_media_path_t mpath,
                        int32_t          tone_profile_id,
                        int32_t          tone_id);



/*
 * mmp_tone_stop
 *
 * Description:
 *
 * Stop the tone currently playing tone on a media line.
 *
 * Parameters:
 *
 * line_id - ID of a media line.
 *
 * Returns: 
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INACTIVE_LINE
 *
 */
int32_t mmp_tone_stop(int32_t line_id);


/*
 * mmp_tone_detection_enable
 *
 * Description: 
 *
 * Activate tone detection on an Phone or IP lines.
 * Detected tones are reported to application via MMP_EVENT_TYPE_TONE_DETECTION 
 * events.
 *
 * Parameters:
 *
 * line_id         - ID of a media line
 * mpath           - Media direction in the line (forward, return or both).
 * tone_profile_id - Tones profile from the XML configuration
 *                   "ToneDetection/Profile" section.
 * tone_id         - Tone from the tone profile specified by the tones profile
 * enable          - true - Enable tone detection.
 *                   false - Disable tone detection.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INACTIVE_LINE
 *    MMP_INVALID_PROF_ID
 *
 */
int32_t mmp_tone_detection_enable(int32_t          line_id,
                                  mmp_media_path_t mpath,
                                  int32_t          tone_profile_id,
                                  bool             enable);


/*
 * mmp_lec_set_params
 *
 * Description:
 *
 * Set the LEC parameters.
 *
 * Parameters:
 *
 * line_id        - ID of a phone line.
 * mmp_lec_params - LEC parameters.
 *
 * Returns:
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INACTIVE_LINE
 *    MMP_INVALID_PARAM
 *
 */

int32_t mmp_lec_set_params(int32_t           line_id,
                           mmp_lec_params_t *lec_params);


/*
 * mmp_lec_enable
 *
 * Description:
 *
 * Enable or disable line echo cancellation (LEC) on an phone line.
 * This API is DEPRECATED, mmp_set_lec_params must be used instead.
 *
 * Parameters:
 *
 * line_id -  ID of a phone line.
 * enable  -  true:  Enable LEC.
 *            false: Disable LEC.
 * 
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INACTIVE_LINE
 */
int32_t mmp_lec_enable(int32_t line_id,
                       bool    enable);


/*
 * mmp_lec_get_params
 *
 * Description:
 *
 * Get the current LEC parameters of a phone line.
 *
 * Parameters:
 *
 * line_id         - ID of a phone line.
 * mmp_lec_params  - Returned LEC parameters.
 *
 * Returns:
 * On success, the function returns MMP_OK. 
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INACTIVE_LINE
 *    MMP_INVALID_PARAM
 *
 */
int32_t mmp_lec_get_params(int32_t           line_id,
                           mmp_lec_params_t *lec_params);

/*
 * mmp_caller_id_start
 *
 * Description:
 *
 * Initiate caller ID generation/detection on a phone line.
 *
 * Parameters:
 * line_id        - ID of a phone line.
 * cid_profile_id - Caller ID generation/detection sequence defined in 
 *                  XML configuration under "Telephony/CallerID" section.
 * msg            - Message to use for caller ID generation. Must be NULL
 *                  for caller ID detection.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INVALID_PROF_ID
 *    MMP_INACTIVE_LINE
 *
 */
int32_t mmp_caller_id_start(int32_t        line_id,
                            int32_t        cid_profile_id,
                            mmp_cid_msg_t *msg);


/*
 * mmp_caller_id_stop
 *
 * Description:
 * 
 * Cancel caller ID generation/detection sequence on a phone interface.
 *
 * Parameters:
 *
 * line_id - ID of a phone line.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INACTIVE_LINE
 *
 */
int32_t mmp_caller_id_stop(int32_t line_id);


/*
 * mmp_agc_enable
 *
 * Description:
 * 
 * Enables Automatic Gain Control (AGC) on a media line.
 * Additional AGC parameters are available under "SpeechEnhancement/AGC" XML
 * configuration section.
 *
 * Parameters:
 *
 * line_id        - ID of a media line.
 * mpath          - Media path to set the gain.
 * agc_profile_id - AGC profile ID.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INACTIVE_LINE
 *    MMP_INVALID_PROF_ID
 *
 */
int32_t mmp_agc_enable(int32_t          line_id,
                       mmp_media_path_t mpath,
                       int32_t          agc_profile_id);


/*
 * mmp_set_gain
 *
 * Description:
 *
 * Enable constant digital gain in the receive/transmit path of a media line.
 *
 * Parameters:
 *
 * line_id - ID of a media line.
 * mpath   - Media path to set the gain. 
 * gain_db - Gain in dB (range: -32db ... + 32db).
 *           0db value is disabling gain control.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns:
 *    MMP_INVALID_LINEID
 *    MMP_API_EXEC_FAIL
 *    MMP_NOT_INITIALIZED
 *    MMP_INVALID_PARAM3
 *
 */
int32_t mmp_set_gain(int32_t          line_id,
                     mmp_media_path_t mpath,
                     int16_t          gain_db);


/*
 * mmp_set_rtcp_params:
 *
 * Description:
 *
 * Set RTCP parameters on IP line in VoIP or VBD mode.
 *
 * Parameters:
 *
 * line_id - ID of an IP line.
 * params  - RTCP parameters.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_rtcp_params(int32_t                  line_id,
                            const mmp_rtcp_params_t *params);


/*
 * mmp_get_rtcp_params:
 *
 * Description:
 *
 * Retrieve current RTCP parameters of an IP line in VoIP or VBD mode.
 *
 * Parameters:
 *
 * line_id - ID of an IP line.
 * params  - Returned RTCP parameters.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_rtcp_params(int32_t            line_id,
                            mmp_rtcp_params_t *params);

/*
 * mmp_rtcp_enable:
 *
 * Description: 
 *
 * Set RTCP mode on IP line in VOIP mode.
 * This API is DEPRECATED, mmp_set_rtcp_params must be used instead.
 *
 * Parameters:
 *
 * line_id        - ID of a media line.
 * mode           - RTCP mode to set.
 * local_port     - Local network port to use with RTCP.
 * remote_port    - Remote network port to use with RTCP.
 * canonical_name - RTCP canonical name.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_rtcp_enable(int32_t         line_id,
                        mmp_rtcp_mode_t mode,
                        uint16_t        local_port,
                        uint16_t        remote_port,
                        char*           canonical_name);

/*
 * mmp_get_line_stats
 *
 * Description: 
 *
 * Retrieve line statistics.
 *
 * Parameters:
 *
 * line_id - ID of a media line.
 * stats   - Returned RTCP statistics.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_line_stats(int32_t           line_id,
                           mmp_line_stats_t *stats);

/*
 * mmp_get_call_stats
 *
 * Description: 
 *
 * Retrieve call statistics.
 *
 * Parameters:
 *
 * call_id - ID of a call.
 * stats   - Returned CALL statistics.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_call_stats(int32_t          call_id,
                            mmp_call_stats_t* stats);


/*
 * mmp_playback_line_write
 *
 * Description:
 * 
 * Send voice bit-stream to a playback line.
 *
 * Parameters:
 *
 * line_id         - ID of a playback line.
 * data            - Pointer to application data.
 * size            - Size in bytes of the application data.
 * remaining_space - Returned remaining size of data hat can be written.
 *
 * Returns:
 *
 * On success, the function returns the number of bytes written
 * On error, the function returns an error code " 0
 *
 */
int32_t mmp_playback_line_write(int32_t   line_id, 
                                uint8_t  *data,
                                uint32_t  size,
                                uint32_t *remaining_space);


/*
 * mmp_playback_line_read
 *
 * Description: 
 *
 * Receive voice bit-stream from a playback line.
 *
 * Parameters:
 *
 * line_id            - ID of a playback line.
 * data               - Pointer to application data.
 * size               - Size in bytes of the application data.
 * remaining_data_len - Returned remaining size of data that can be read.
 *
 * Returns:
 *
 * On success, the function returns the number of bytes read
 * On error, the function returns an error code " 0
 *
 */
int32_t mmp_playback_line_read(int32_t   line_id, 
                               uint8_t  *data,
                               uint32_t  size,
                               uint32_t *remaining_data_len);

/*
 * mmp_set_voice_quality_alarm
 *
 * Description: 
 *
 * Enable Voice Quality Monitoring according to G.107 specification
 * requirements.
 *
 * Parameters:
 *
 * line_id - ID of IP line in VoIP mode.
 * params  - Params for the voice quality alarm.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_voice_quality_alarm(int32_t                           line_id,
                                    mmp_voice_quality_alarm_params_t *params);


/*
 * mmp_set_call_params
 *
 * Description: 
 *
 * Set call parameters
 *
 * Parameters:
 *
 * call_id   - ID of call
 * params    - Pointer to call parameters struct
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_call_params(int32_t            call_id, 
                            mmp_call_params_t *params);


/*
 * mmp_get_call_params()
 *
 * Description: Get call parameters
 *
 * Parameters:
 * call_id - ID of call
 * params  - Pointer to parameters struct
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_call_params(int32_t            call_id, 
                            mmp_call_params_t *params);

/*
 * mmp_set_jb_params
 *
 * Description: 
 *
 * Set jitter buffer operation mode on a IP line.
 *
 * Parameters:
 *
 * line_id - ID of a remote voice line.
 * params  - Jitter buffer configuration parameters
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_jb_params(int32_t         line_id,
                          mmp_jb_params_t *params);


/*
 * mmp_get_jb_params
 *
 * Description: 
 *
 * Get the current jitter buffer operation mode on a IP line.
 *
 * Parameters:
 * line_id - ID of an IP line.
 * mode    - Returned jitter buffer operation mode.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_jb_params(int32_t          line_id,
                          mmp_jb_params_t *params);


/*
 * mmp_get_jb_stats
 *
 * Description: 
 *
 * Get jitter buffer statistics.
 *
 * Parameters:
 *
 * line_id - ID of an IP line.
 * stats   - Returned jitter buffer statistics.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_jb_stats(int32_t         line_id,
                         mmp_jb_stats_t *stats);


/*
 * mmp_rfc2833_enable
 *
 * Description:
 *
 * Enable DTMF signaling transport on an IP line.
 *
 * Parameters:
 *
 * line_id - ID of an IP line.
 * mode    - DTMF transport mode.
 * rx_pt   - Incoming RTP payload type for DTMF.
 * tx_pt   - Outgoing RTP payload type for DTMF.
 *
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_rfc2833_enable(int32_t            line_id,
                           mmp_rfc2833_mode_t mode,
                           uint8_t            rx_pt,
                           uint8_t            tx_pt);








/*
 * mmp_silence_suppression_enable
 *
 * Description:
 *
 * Enable or disable silence suppression for a remote voice line.
 * If enabled, Voice Activity Detector (VAD) is used to detect silent intervals
 * and the associated noise levels. Silence Identifier (SID) frames are sent
 * periodically to the remote side at time intervals specified in "VoIP/DTXPeriod"
 * XML configuration section.
 *
 * Parameters:
 *
 * line_id - ID of a remote voice line.
 * mode    - Silence suppression mode.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_silence_suppression_enable(int32_t                  line_id,
                                       mmp_silence_suppr_mode_t mode);


/*
 * mmp_get_call_id
 *
 * Description: 
 *
 * Return the media call ID associated with a media line.
 *
 * Parameters:
 *
 * line_id - ID of a media line.
 * call_id - Returned ID of an associated call or -1 if the line is not
 *           participating in any call.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_call_id(int32_t  line_id,
                        int32_t *call_id);


/*
 * mmp_get_active_calls_num
 *
 * Description:
 *
 * Retrieve the number of currently active media calls.
 *
 * Parameters:
 *
 * None.
 *
 * Returns:
 *
 * The number of currently active calls or 0 if there are no calls.
 *
 */
int32_t mmp_get_active_calls_num(void);


/*
 * mmp_get_active_calls
 *
 * Description:
 *
 * Retrieve the list of currently active media calls.
 *
 * Parameters:
 *
 * calls - Application supplied array to receive the call IDs.
 * size  - The number of elements in the array.
 *         Upon return, size will contain the actual number calls.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_active_calls(int32_t  calls[],
                             int32_t *size);

/* 
 * mmp_get_lines
 *
 * Description:
 *
 * Retrieve the list of currently active media lines.
 *
 * Parameters:
 *
 * call_id - ID of a media call of the participating lines, or -1 for all active 
 *           media lines regardless of whether they are participating in media 
 *           calls or not.
 * lines   - Application supplied array to receive the media line IDs.
 * size    - The number of elements in the lines array.
 *           Upon return, size will contain the actual number of media line IDs 
 *           in the lines array.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_lines(int32_t  call_id,
                      int32_t  lines[],
                      int32_t *size);


/*
 * mmp_get_line_info
 *
 * Description: 
 *
 * Retrieve information of an active media line.
 *
 * Parameters:
 *
 * line_id   - ID of a media line.
 * line_info - Application supplied structure to receieve the info.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_line_info(int32_t          line_id,
                          mmp_line_info_t *line_info);


/*
 * mmp_register_events
 *
 * Description: 
 *
 * Register application callbacks to receive MMP events.
 *
 * Parameters:
 * event_mask    - Mask of the events the applications is interested in.
 * event_handler - Application callback function.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_register_events(mmp_event_mask_t     event_mask,
                            mmp_event_handler_t *event_handler);


/*
 * mmp_get_version
 *
 * Description:
 *
 * Retrieve MMP SDK version.
 *
 * Parameters:
 *
 * None.
 *
 * Returns:
 *
 * On success, the function returns a string with MMP version.
 * On error, the function returns NULL.
 *
 */
const char* mmp_get_version(void);


/*
 * mmp_enable_media_path
 *
 * Description:
 *
 * Enable or disable media processing in the specified direction of a media line.
 *
 * Parameters:
 *
 * line_id - ID of a media line.
 * path    - Media direction.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_enable_media_path(int32_t          line_id,
                              mmp_media_path_t path);


/*
 * mmp_set_ssrc
 *
 * Description:
 *
 * Set RTP session synchronization source for an IP line in VoIP or VBD mode.
 *
 * Parameters:
 *
 * line_id - ID of an IP line.
 * ssrc    - Session synchronization source.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 * 
 */
int32_t mmp_set_ssrc(int32_t  line_id,
                     uint32_t ssrc);


/*
 * mmp_get_ssrc
 *
 * Description:
 *
 * Retrieve RTP session synchronization source of an IP line in VoIP or VBD mode.
 *
 * Parameters:
 *
 * line_id - ID of a media line.
 * ssrc    - Returned session synchronization source.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_ssrc(int32_t   line_id,
                     uint32_t *ssrc);


/*
 * mmp_set_loopback
 *
 * Description:
 *
 * Create or destroy a loopback between two media lines.
 *
 * Parameters:
 * 
 * line_id1       - ID of line1.
 * line_id2       - ID of line2
 * loopback_point - Where to do loopback.
 * status         - true enable the loopback,
 *                  false - disable the loopback.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_loopback(int32_t              line_id1,
                         int32_t              line_id2,
                         mmp_loopback_point_t loopback_point,
                         bool                 status);



/*
 * mmp_get_stats
 *
 * Description:
 *
 * Retrieve MMP SDK system statistics.
 *
 * Parameters:
 *
 * 
 * stats - Returned MMP SDK system statistics.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_stats(mmp_stats_t *stats);








/*
 * mmp_set_log_level
 *
 * Description:
 *
 * Set log level.
 *
 * Parameters:
 * 
 * id        - Logical unit ID for which to set the logging level.
 * log_level - Logging level (0 ..7).
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_log_level(mmp_log_unit_t id,
                          int32_t log_level);


/*
 * mmp_get_log_level
 *
 * Description: 
 *
 * Get log level
 *
 * Parameters:
 * 
 * id        - Logical unit ID for which to get the current logging level.
 * log_level - Returned logging level.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_log_level(mmp_log_unit_t  id,
                          int32_t        *log_level);


/*
 * mmp_set_rfc2198_params
 *
 * Description:
 *
 * Set rfc2198 parameters
 *
 * Parameters:
 * 
 * line_id - ID of an IP line.
 * params  - RFC2198 parameters.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_rfc2198_params(int32_t               line_id,
                               mmp_rfc2198_params_t *params);


/*
 * mmp_get_rfc2198_params
 *
 * Description: 
 *
 * Retrieve the current RFC2198 parameters of an IP line.
 *
 * Parameters:
 * 
 * line_id - ID of an IP line.
 * params  - Returned RFC2198 parameters.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_rfc2198_params(int32_t               line_id,
                               mmp_rfc2198_params_t *params);


/*
 * mmp_set_packet_capture_params
 *
 * Description:
 *
 * Set packet capture parameters.
 *
 * Parameters:
 *
 * params - Packet capture parameters.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_packet_capture_params(mmp_packet_capture_params_t *params);



/*
 * mmp_get_packet_capture_params
 *
 * Description:
 *
 * Get current packet capture parameters.
 *
 * Parameters:
 *
 * params - Returned packet capture parameters.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_packet_capture_params(mmp_packet_capture_params_t *params);



/*
 * mmp_set_capture_point_status
 *
 * Description:
 *
 * Enable or disable a capture point.
 *
 * Parameters:
 *
 * id          - ID of a capture point.
 * status      - true - Enable the capture point, false - Disable the capture point.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_capture_point_status(mmp_capture_point_t id,
                                     bool                status);



/*
 * mmp_get_capture_point_status
 *
 * Description:
 *
 * Get current capture point status.
 *
 * Parameters:
 *
 * id        - ID of a capture point.
 * status    - Returned status.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 */
int32_t mmp_get_capture_point_status(mmp_capture_point_t id,
                                     bool               *status);



/*
 * mmp_set_capture_line_status
 *
 * Description:
 *
 * Enable or disable capture for specified line.
 *
 * Parameters:
 *
 * line_id          - line id 
 * status           - true - Enable the capture on line, false - Disable capture on line.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_capture_line_status(int32_t line_id,  bool status);


/*
 * mmp_get_capture_line_status
 *
 * Description:
 *
 * Get current capture status of line.
 *
 * Parameters:
 *
 * line_id        - line id
 * status         - Returned status.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 */
int32_t mmp_get_capture_line_status(int32_t line_id,  bool* status);


/*
 * mmp_set_log_params
 *
 * Description:
 *
 * Set the logging parameters.
 *
 * Parameters:
 *
 * params - Logging parameters.
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_log_params(mmp_log_params_t *params);



/*
 * mmp_get_log_params
 *
 * Description:
 *
 * Get current logging parameters
 *
 * Parameters:
 *
 * params - Returned logging parameters
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_get_log_params(mmp_log_params_t *params);


/*
 * mmp_set_rtp_params
 *
 * Description:
 *
 * Set rtp parameters for rtp tx stream
 *
 * Parameters:
 *
 * params - Rtp parameters
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_set_rtp_params(int32_t line_id, const mmp_rtp_params_t* rtp_stream);



/*
 * mmp_cas_event_start
 *
 * Description:
 *
 * Start sending of cas event over rfc2833 trasport
 *
 * Parameters:
 *
 * line_id    - line id 
 * params     - event parameters
 *
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_cas_event_start(int32_t line_id, const mmp_cas_event_t* params);



/*
 * mmp_cas_event_stop
 *
 * Description:
 *
 * cancel sending of cas event over rfc2833
 *
 * Parameters:
 *
 * line_id    - line id
 * 
 * Returns:
 *
 * On success, the function returns MMP_OK.
 * On error, the function returns MMP_FAIL.
 *
 */
int32_t mmp_cas_event_stop(int32_t line_id);



#ifdef __cplusplus
}
#endif


#endif
