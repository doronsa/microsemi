/*******************************************************************************
*               Copyright 2007, Marvell Technology Group Ltd.                    
*                                                                                
* THIS CODE CONTAINS CONFIDENTIAL INFORMATION OF MARVELL. NO RIGHTS ARE GRANTED  
* HEREIN UNDER ANY PATENT, MASK WORK RIGHT OR COPYRIGHT OF MARVELL OR ANY THIRD  
* PARTY. MARVELL RESERVES THE RIGHT AT ITS SOLE DISCRETION TO REQUEST THAT THIS  
* CODE BE IMMEDIATELY RETURNED TO MARVELL. THIS CODE IS PROVIDED "AS IS".      
* MARVELL MAKES NO WARRANTIES, EXPRESS, IMPLIED OR OTHERWISE, REGARDING ITS      
* ACCURACY, COMPLETENESS OR PERFORMANCE. MARVELL COMPRISES MARVELL TECHNOLOGY    
* GROUP LTD. (MTGL) AND ITS SUBSIDIARIES, MARVELL INTERNATIONAL LTD. (MIL),      
* MARVELL TECHNOLOGY, INC. (MTI), MARVELL SEMICONDUCTOR, INC. (MSI), MARVELL     
* ASIA PTE LTD. (MAPL), MARVELL JAPAN K.K. (MJKK).                               
******************************************************************************** 
*      libmmpcli.h                      
*                                                                                
* DESCRIPTION:                                                                   
*                                                                                
*                                                                                
* CREATED BY:  yuval elmaliah                                                    
*                                                                                
* DATE CREATED: October 20, 2010     
*                                                                                
* DEPENDENCIES:                                                                  
*                                                                                
*                                                                                
* FILE REVISION NUMBER:                                                          
*       $Revision: 1 $                                                           
*******************************************************************************/

#ifndef __LIBMMPCLI_H__
#define __LIBMMPCLI_H__


#ifdef __cplusplus
extern "C" {
#endif

#include <clish/shell.h>



/*******************************************************************************
* xxx:
*
* DESCRIPTION: 
*
* INPUTS:
*
*
*
* RETURNS:
*
*
* COMMENTS:
*
*
*******************************************************************************/
  int libmmpcli_init(const char* configuration_file);



/*******************************************************************************
* xxx:
*
* DESCRIPTION: 
*
* INPUTS:
*
*
*
* RETURNS:
*
*
* COMMENTS:
*
*
*******************************************************************************/
  void libmmpcli_shutdown(void);



/********************************************************************************/
/*                            MMP CLI functions                                 */
/********************************************************************************/
bool_t mmp_cli_cmd_enter_mmp_view               (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_leave_mmp_view               (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_open_phone_line              (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_caller_id_start              (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_caller_id_stop               (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_tone_start                   (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_tone_stop                    (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_tone_detection_enable        (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_lec_enable                   (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_close_line                   (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_set_gain                     (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_open_ip_line                 (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_set_ip_params                (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_bind_to_device               (const clish_shell_t* instance,
                                                 const lub_argv_t *argv);
bool_t mmp_cli_cmd_set_voip_mode                (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_set_rtcp_mode                (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_open_playback_line           (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_set_voice_quality_alarm      (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_set_jb_params                 (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_get_jb_params                  (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_rfc2833_enable               (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_silence_suppression_enable   (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_create_call                  (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_join_call                    (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_leave_call                   (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_destroy_call                 (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_set_call_params				(const clish_shell_t* instance,
												 const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_call_params				(const clish_shell_t* instance,
												 const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_active_calls_num         (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_get_active_calls             (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_get_call_id                  (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_set_foip_mode                (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_set_vbd_mode                 (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_version                      (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_show                 (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_reset                (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_message_type         (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_date                 (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_calling_line_identity(const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_called_line_identity (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_calling_party_name   (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_first_line_identity  (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_redirected_number    (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_complementary_calling_line_identity(const clish_shell_t   *instance,
                                                               const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_reason_for_absence   (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_reason_for_absence_of_party(const clish_shell_t   *instance,
                                                       const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_call_type            (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_visual_indicator     (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_network_message_system_status(const clish_shell_t   *instance,
                                                         const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_type_of_forwarded_call(const clish_shell_t   *instance,
                                                  const lub_argv_t      *argv);
bool_t mmp_cli_cmd_cid_msg_type_of_calling_user (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_init                         (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_shutdown                     (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_enable_media_path            (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_set_ssrc                     (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_get_line_stats               (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_get_call_stats               (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_get_ssrc                     (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_get_jb_stats                  (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_get_line_info                (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_agc_enable                   (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_set_loopback(const clish_shell_t* instance,
                                const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_stats(const clish_shell_t* instance,
                             const lub_argv_t *argv);
bool_t mmp_cli_cmd_set_tos                       (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_tos                       (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_lec_set_params                (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_lec_get_params                (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_set_log_level                 (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_log_level                 (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_set_rfc2198_params           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_rfc2198_params           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_set_packet_capture_params           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_packet_capture_params           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_set_capture_point_status           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_capture_point_status           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_set_capture_line_status           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_capture_line_status           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_set_log_params           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_log_params           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_set_rtcp_params           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_rtcp_params           (const clish_shell_t* instance,
                                                  const lub_argv_t *argv);
bool_t mmp_cli_cmd_get_lines                     (const clish_shell_t* instance,
                                                   const lub_argv_t *argv);
bool_t mmp_cli_cmd_cas_event_start           (const clish_shell_t* instance,
                                                   const lub_argv_t *argv);
bool_t mmp_cli_cmd_cas_event_stop           (const clish_shell_t* instance,
                                                   const lub_argv_t *argv);
bool_t mmp_cli_cmd_set_rtp_params          (const clish_shell_t* instance,
                                                   const lub_argv_t *argv);



bool_t mmp_cli_cmd_foip_params_show                  (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_new                   (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_delete                (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_t38_version       (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_max_bit_rate      (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_fill_bit_removal  (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_transcoding_mmr   (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_transcoding_jbig  (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_enable_ecm        (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_t30sig_redundancy(const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_ec                (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_image_redundancy  (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_drmm              (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_v34_enable        (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_modem_type        (const clish_shell_t* instance, const lub_argv_t *argv);
bool_t mmp_cli_cmd_foip_params_set_op_mode            (const clish_shell_t* instance, const lub_argv_t *argv);


/********************************************************************************/
/*                            SLIC CLI functions                                */
/********************************************************************************/
bool_t mmp_cli_cmd_slic_ring_start              (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);
bool_t mmp_cli_cmd_slic_ring_stop               (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);

bool_t mmp_cli_cmd_slic_set_gain               (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);

/********************************************************************************/
/*                            MISCELLANEOUS CLI functions                                */
/********************************************************************************/

bool_t mmp_cli_cmd_execution_time               (const clish_shell_t   *instance,
                                                 const lub_argv_t      *argv);



#ifdef __cplusplus
}
#endif

#endif
