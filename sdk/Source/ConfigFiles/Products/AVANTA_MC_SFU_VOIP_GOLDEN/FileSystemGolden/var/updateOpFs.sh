#!/bin/sh

SERVERIP=$1


if [ ! ${1} ];
then
	echo "Usage: $0 TFTP_SERVER_IP"
	exit 1
fi

echo "downloading rootfs.tar from " ${SERVERIP}
cd /tmp
mkdir opfs
tftp -g -r rootfs.tar ${SERVERIP}
if [ ! -f "rootfs.tar" ]; then
  echo "downloading rootfs.tar failed"
  exit 0
fi
cd /tmp/opfs
tar --exclude=app --exclude=var -xf /tmp/rootfs.tar

echo "erasing mtd block 5-7"
flash_erase -j /dev/mtd5 0 0
flash_erase -j /dev/mtd6 0 0
flash_erase -j /dev/mtd7 0 0

echo "mounting mtd blocks 5-7"
mkdir -p /var/mnt/root
mkdir -p /var/mnt/app
mkdir -p /var/mnt/var
mount -t jffs2 /dev/mtdblock5 /var/mnt/root
mount -t jffs2 /dev/mtdblock6 /var/mnt/app
mount -t jffs2 /dev/mtdblock7 /var/mnt/var

echo "copying rootfs"
cp -af /tmp/opfs/* /var/mnt/root/
mkdir /var/mnt/root/var
mkdir /var/mnt/root/app

rm -rf /tmp/opfs/*
tar -xf /tmp/rootfs.tar

echo "copying app partition"
cp -af /tmp/opfs/app/* /var/mnt/app/

echo "copying var partition"
cp -af /tmp/opfs/var/* /var/mnt/var/

sync
umount /var/mnt/root
umount /var/mnt/app
umount /var/mnt/var
sync
echo "Operational FS successfully written to flash"

