#!/bin/sh
#user init script ver 1.0
echo "user init script ver 1.1"
#start DB parsing and init system
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/var/lib

#SYSTEM_IPv4_ADDR=192.168.0.50/24
#SYSTEM_IPv6_ADDR=2001:db8:0:1::129/64
#INTERFACE_DEV=eth0

#echo "user init script ver 1.0" 

#echo "user network configuration"
#ifconfig ${INTERFACE_DEV} ${SYSTEM_IPv4_ADDR}
#ip -6 addr add ${SYSTEM_IPv6_ADDR} dev ${INTERFACE_DEV}

#echo "Mounting Development file system:"
#echo "mount -o nolock,timeo=5 devhost:/export/work /var/work"
#mount -o nolock,timeo=5 devhost:/export/work /var/work

#boot sequence
MAX_RETRY=3
mount -t jffs2 /dev/mtdblock7 /home
if [ -f "/home/Boot_Retry" ];
then
  bootretry=$(cat /home/Boot_Retry)
  if [ $bootretry -gt $MAX_RETRY ];
  then
    echo "System update"
    /var/updateOpFs.sh 192.168.0.100
    fw_setenv op_valid true
    echo "Rebooting"
    reboot -f
  else
    bootretry_new=$(( bootretry++ ))
    echo $bootretry > /home/Boot_Retry
    fw_setenv op_valid true
    echo ""
    echo ""
    echo "Golden Linux will reboot in 5 seconds. To stop hit any key"
    read -t 5 input
    if [[ $? -ne 0 ]];
    then
      reboot -f
    else
      umount /home
    fi  
  fi
else 
    touch /home/Boot_Retry
    echo 0 > /home/Boot_Retry
    fw_setenv op_valid true
    echo ""
    echo ""
    echo "Golden Linux will reboot in 5 seconds. To stop hit any key"
    read -t 5 input
    if [[ $? -ne 0 ]];
    then
      reboot -f
    else
      umount /home
    fi  
fi

#cd /var/web_root/ 
#./DBFileParser -init

exit 0
