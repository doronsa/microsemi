export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/var/lib
if [ $1 -eq 0 ];
then
	echo "Stoping Web Server"
	/bin/kill $(pidof lighttpd)
else
	if [[ $2 ]];
	then
		if [[ $2 -eq 1 ]];
		then
			echo "Starting Web Server (force HTTPS redirect)"
			/usr/bin/lighttpd -f /var/lighttpd_force_ssl.conf -m /var/lib/
			exit 0
		fi
	fi
	echo "Starting Web Server"
	/usr/bin/lighttpd -f /var/lighttpd.conf -m /var/lib/
fi

