#!/bin/sh

SERVERIP=$1

if [ ! ${1} ];
then
	echo "Usage: $0 TFTP_SERVER_IP"
	exit 1
fi

cd /tmp/
tftp -g -r pds104_sg.pem ${SERVERIP}
if [ -f /var/pds104_sg.pem ];
then
	rm /var/pds104_sg.pem
fi
mv /tmp/pds104_sg.pem /var/
rm /var/lighttpd_ssl.conf
cp -f /var/lighttpd_ssl_sg.conf /var/lighttpd_ssl.conf
