HOST_NAME=$(hostname)
if [ $1 -eq 0 ];
then
	echo "Stoping DHCPv6"
   /bin/kill $(pidof dhclient)
else
	echo "Starting DHCPv6"
	echo "send host-name ${HOST_NAME};" > /tmp/dhclient.conf
   /usr/bin/dhclient -6 -sf /var/dhclient-script -cf /tmp/dhclient.conf eth0 &
fi

