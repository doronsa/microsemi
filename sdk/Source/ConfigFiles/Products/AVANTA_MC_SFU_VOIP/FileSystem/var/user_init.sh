#!/bin/sh
#user init script ver 1.0
echo "user init script ver 1.1"
#start DB parsing and init system
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/var/lib

#SYSTEM_IPv4_ADDR=192.168.0.50/24
#SYSTEM_IPv6_ADDR=2001:db8:0:1::129/64
#INTERFACE_DEV=eth0

#echo "user init script ver 1.0" 

#echo "user network configuration"
#ifconfig ${INTERFACE_DEV} ${SYSTEM_IPv4_ADDR}
#ip -6 addr add ${SYSTEM_IPv6_ADDR} dev ${INTERFACE_DEV}

#echo "Mounting Development file system:"
#echo "mount -o nolock,timeo=5 devhost:/export/work /var/work"
#mount -o nolock,timeo=5 devhost:/export/work /var/work

#boot sequence
fw_setenv op_valid true 
if [ -f "/var/Boot_Retry" ];
then
    echo "File /var/Boot_Retry exists"
else 
    echo "File /var/Boot_Retry does not exist"
    touch /var/Boot_Retry
fi
echo 0 > /var/Boot_Retry

mkdir /tmp/lua_exec_result
cd /var/web_root/ 
./DBFileParser -init

exit 0
