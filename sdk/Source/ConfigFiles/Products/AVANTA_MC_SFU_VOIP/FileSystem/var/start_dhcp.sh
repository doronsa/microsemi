HOST_NAME=$(hostname)
if [ $1 -eq 0 ];
then
	echo "stoping DHCP"
   /bin/kill $(pidof dhclient)
else
	echo "Starting DHCP"
	echo "send host-name ${HOST_NAME};" > /tmp/dhclient.conf
   /usr/bin/dhclient -4 -sf /var/dhclient-script -cf /tmp/dhclient.conf eth0 &
fi

