#!/bin/sh

SERVERIP=$1

if [ ! ${1} ];
then
	echo "Usage: $0 TFTP_SERVER_IP"
	exit 1
fi

cd /tmp/
tftp -g -r ca.crt ${SERVERIP}
tftp -g -r pds104.pem ${SERVERIP}
if [ -f /var/ca.crt ];
then
	rm /var/ca.crt
fi

if [ -f /var/pds104.pem ];
then
	rm /var/pds104.pem
fi
mv /tmp/ca.crt /var/
mv /tmp/pds104.pem /var/
rm /var/lighttpd_ssl.conf
cp -f /var/lighttpd_ssl_ca.conf /var/lighttpd_ssl.conf
