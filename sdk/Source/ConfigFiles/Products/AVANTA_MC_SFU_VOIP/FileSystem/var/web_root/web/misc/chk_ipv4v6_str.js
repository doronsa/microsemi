﻿// used by ipv6 address validation function in order to find the number of ':' 
function substr_count (haystack, needle, offset, length) 
{    
	pos = 0, cnt = 0;
	haystack += '';
	needle += '';
	
	if (isNaN(offset))
	{
		offset = 0;
	}

	if (isNaN(length))
	{
		length = 0;
	}

	offset--;  

	while ((offset = haystack.indexOf(needle, offset+1)) != -1)
	{
		if (length > 0 && (offset+needle.length) > length)
		{
			return false; 
		}
		else
		{
			cnt++; 
		} 
	}  
	return cnt; 
}

// Check if a single char is between lower to upper char range 
function isBound(charCheck, charLower, charUpper)
{
	if(charCheck >= charLower && charCheck <= charUpper)
		return 1;
		
	return 0;
}

// Test if the input is a valid ipv6 address 
function ipv6_address_validation_filter(address_field) 
{
	// Test for empty address
	if (address_field.length<3)
	{
		return address_field == "::"; 
	}

	// Check if part is in IPv4 format 
	if (address_field.indexOf('.')>0)
	{
		lastcolon = address_field.lastIndexOf(':');
		if (!(lastcolon && test_ipv4(address_field.substr(lastcolon + 1), false)))
			return false;

		// replace IPv4 part with dummy 
		address_field = address_field.substr(0, lastcolon) + ':0:0'; 
	}

	// Check uncompressed 
	if (address_field.indexOf('::')<0)
	{
		var match = address_field.match(/^(?:[a-f0-9]{1,4}:){7}[a-f0-9]{1,4}$/i); 
		return match != null; 
	}

	// Check colon-count for compressed format 
	if (substr_count(address_field, ':'))
	{
		var match = address_field.match(/^(?::|(?:[a-f0-9]{1,4}:)+):(?:(?:[a-f0-9]{1,4}:)*[a-f0-9]{1,4})?$/i); 
		return match != null; 
	}

	// Not a valid IPv6 address 
	return false; 
}


// Test for ipv6 address validation
function validate_iPv6_address(address_field)
{
	if(address_field.value.length == 0)
		return 'OK';
		
	// first IP address filter
	var regExp="(::|(([a-fA-F0-9]{1,4}):){7}(([a-fA-F0-9]{1,4}))|(:(:([a-fA-F0-9]{1,4})){1,6})|((([a-fA-F0-9]{1,4}):){1,6}:)|((([a-fA-F0-9]{1,4}):)(:([a-fA-F0-9]{1,4})){1,6})|((([a-fA-F0-9]{1,4}):){2}(:([a-fA-F0-9]{1,4})){1,5})|((([a-fA-F0-9]{1,4}):){3}(:([a-fA-F0-9]{1,4})){1,4})|((([a-fA-F0-9]{1,4}):){4}(:([a-fA-F0-9]{1,4})){1,3})|((([a-fA-F0-9]{1,4}):){5}(:([a-fA-F0-9]{1,4})){1,2}))";

	if (address_field.value.search(regExp)==-1)
	{
		return 'Invalid IPv6 address. Please enter adress as 1234::6F:1';
	}

	// second IP address filter
	if (!(ipv6_address_validation_filter(address_field.value)))
	{
		return 'Invalid IPv6 address. Please enter adress as 1234::6F:1';
	}

	return 'OK';
}   


// Test for IPv6 Prefix validation
function IPv6_prefix_validation(prefix_field,bAllowBlank)
{   			
	if((prefix_field.value.length == 0) && (bAllowBlank == true))
		return 'OK';

	if ((isNaN(prefix_field.value))||(prefix_field.value<1)||(prefix_field.value>128)||(prefix_field.value=='')||(prefix_field.value.indexOf(' ')!=-1))
	{
		return 'Please enter an IPv6 Prefix number in the range of 1-128';
	}
	
	return 'OK';
}   

// Test for a valid dotted IPv4 address
function test_ipv4(address_field_value, bAllowBlank)
{
	var ipaddr_splt;
	
	if((address_field_value.length == 0) && (bAllowBlank == true))
		return true;
	
	// split IPv4 parts by '.'
	ipaddr_splt = address_field_value.split("."); 
	if (ipaddr_splt.length !=4)
		return false;

	// check all part of IPv4
	for (var e in ipaddr_splt)
	{
		if ((isNaN(ipaddr_splt[e]))||(ipaddr_splt[e]<0)||(ipaddr_splt[e]>255)||(ipaddr_splt[e]=='')||(ipaddr_splt[e].indexOf(' ')!=-1))
			return false;
	}
	
	return true;
}

// IPv4 Address Validation
function validate_ipv4_address(address_field)
{
	if(test_ipv4(address_field.value, true))
		return 'OK';

	return 'Invalid IPv4 address. Please enter IPv4 address as 192.168.0.50';
}

// IPv4 mask Validation
function Ipv4_mask_validation(address_field,bAllowBlank)
{
	var ipaddr_splt;
	
	if((address_field.value.length == 0) && (bAllowBlank == true))
		return 'OK';

	// split IPv4 parts by '.' //
	ipaddr_splt = address_field.value.split("."); 
	if (ipaddr_splt.length !=4)
	{
		return 'Invalid IPv4 mask. Please enter IPv4 mask as 255.255.255.0';
	}

	// check all part of IPv4 //
	for (var e in ipaddr_splt)
	{
		if ((isNaN(ipaddr_splt[e]))||(ipaddr_splt[e]<0)||(ipaddr_splt[e]>255)||(ipaddr_splt[e]=='')||(ipaddr_splt[e].indexOf(' ')!=-1))
		{
			return 'Invalid IPv4 mask. Please enter IPv4 mask as 255.255.255.0';
		}
	}
				
	var correct_range = {128:1,192:1,224:1,240:1,248:1,252:1,254:1,255:1,0:1};
			
	for (var i = 0; i <= 3; i ++) 
	{
		if (!(parseInt(ipaddr_splt[i],10) in correct_range)) 
		{
			return 'Mask is not valid';
		}
	}
	
	if ((ipaddr_splt[0] == 0) || (ipaddr_splt[0] != 255 && ipaddr_splt[1] != 0) || (ipaddr_splt[1] != 255 && ipaddr_splt[2] != 0) || (ipaddr_splt[2] != 255 && ipaddr_splt[3] != 0)) 
	{
		return 'Mask is not valid';  
	}
	
	return 'OK';
}

// validates if the user meant to write an IPv4 address
function hints_for_IPv4_address(address_field)
{
	for (var i in address_field.value)
	{
		if( (!isBound(address_field.value[i], '0', '9')) && ('.' != address_field.value[i]) )
			return 0;
	}
	return 1;
}

// validates if the user meant to write an IPv6 address
function hints_for_IPv6_address(address_field)
{
	for (var i in address_field.value)
	{
		if( !isBound(address_field.value[i], '0', '9') && !isBound(address_field.value[i], 'A', 'F') && 
			!isBound(address_field.value[i], 'a', 'f') && (':' != address_field.value[i]))
			return 0;   
	}
	return 1;
}

// check IP address
function validate_IPv4_IPv6_address(address_field)
{
	//IPv4 Address validation
	msg = validate_ipv4_address(address_field);
	if (msg == 'OK')
		return 'OK';

	//if we here - address is not IPV4. lets check IPV6 address validation
	msg = validate_iPv6_address(address_field);
	if (msg == 'OK')
		return 'OK';

	//if we here - address is not IPV4 or IPV6 address //
	//lets find hints for the type that the user mean to type //
	if (hints_for_IPv6_address(address_field))  // find ':' - ipv6 sign //
	{
		return 'Please enter valid IPV6 address';
	}
	else if (hints_for_IPv4_address(address_field))  // find '.' - ipv4 sign //
	{
		return 'Please enter valid IPV4 address';
	}
	else // no special char was found //
	{
		return 'Please enter valid IPV4 or IPV6 address';   	
	}   						
}

// check if host name valid name
function validate_hostname(address_field)
{
	if(address_field.value.length == 0)
		return 'OK';

	var match = address_field.value.match(/^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$/i); 
	if(match != null)
		return 'OK';
		
	return 'Please enter valid host name';  	
}

// check IP address depend on the expected IPV type: IPV4,IPV6 or IPV4V6 (both).
function Validate(Type,field)
{
	var msg;
	
	if( field.value )   						
		field.value = field.value.replace(/^\s+|\s+$/g,"");// the function trims a string (remove spaced before and after a string)
	else
		field.value = "";   	 
	 
	if (Type == 'IPV4')
	{
		return validate_ipv4_address(field);
	}
	else if (Type == 'IPV6')
	{
		return validate_iPv6_address(field);
	}
	else if (Type == 'IPV4V6')
	{
		return validate_IPv4_IPv6_address(field);
	}
	else if (Type == 'HOSTNAME')
	{
		return validate_hostname(field);
	}
	else if (Type == 'IPV4V6HOSTNAME')
	{
		msg = validate_hostname(field);
		if (msg == 'OK')
			return msg;

		return validate_IPv4_IPv6_address(field);
	}
	else
	{
		return 'Unknown IPV TYPE';
	}
	
	return 'OK';
}

function Check_result(resultMsg, field, headerMsg)
{
	if(resultMsg != "OK")
	{
		alert (headerMsg + ' Error: ' + resultMsg);
		field.focus();
		return false;
	}   	
	
	return true;
}

// Validate Names and Passwords for spaces and paranteses and empty fileds.
function Validate_names_pass(from_elem, to_elem) 
{
	var idx_0=-1;
	var idx_1=-1;
	var e;
	var s;
	var pl;
	var result;
	
	for (var i=0;i<document.cfg_form.elements.length;i++)
	{
		//Go over all elements in form
		if(document.cfg_form.elements[i].name == from_elem) 
			idx_0 = i;
		if(document.cfg_form.elements[i].name == to_elem)
			idx_1 = i;  		
	}
	if( (idx_0 == -1)||(idx_1 == -1))
		return false

	for (var i=idx_0;i<=idx_1;i++)
	{
		//Go over all elements in the required range
		e = document.cfg_form.elements[i]; //Element
		s = e.value; //Element value
		if ((s.indexOf('{') != -1)||(s.indexOf('}') != -1)) 
		{
			//Check for {} in all fields in form
			alert ('Curly braces are not allowd');
			e.focus();
			return false;
		}
			
		if (e.value=='') 
		{ 
			//Check for empty fields in form
			alert('Empty field is invalid');
			e.focus();
			return false;
		}
			
		// Check for all spaces 	
		var iSpace;
		var bTest = true;
		for (iSpace=0; iSpace<s.length; iSpace++) 
		{
			//check field characters
			if (s.charAt(iSpace)!=' ') 
			{
				//if not a space - break loop
				bTest = false;
				break;
			}
		}
			
		if (bTest) 
		{
			//If only spaces found
			alert ('Fields with spaces only are not allowd');
			e.focus();
			return false;
		}   	
	}
	return true;
}