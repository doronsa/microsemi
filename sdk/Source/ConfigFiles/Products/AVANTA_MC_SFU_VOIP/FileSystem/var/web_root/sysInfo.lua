local key =io.read("*a")
local result=""
local resultLines={}
local matchedLines={}
local matchedWords={}
local finalResult=""

local myExecError = {"error"}

function myExec(cmd, name)
    local lines = {}
    local tmpfile = '/tmp/lua_exec_result/' .. name
    os.execute(cmd..' > '..tmpfile)
    local fhandle = io.open(tmpfile)
    if not fhandle then return myExecError end

    local k = 1
    for line in fhandle:lines() do
      lines[k] = line
      k = k + 1
    end
    fhandle:close()
    return lines
end
 
function getMatchingLines(lineTable, matchStr)
    local k = 1
    local matchedLines = {}
    for i,line in ipairs(lineTable) do
        if string.find(line, matchStr) then
            matchedLines[k] = line
            k = k + 1
        end
    end
    return matchedLines
end
 
function getWords(line, separator)
    
   local words = {} 
    local k = 1
    for word in line:gmatch(separator) do
        words[k] = word
        k = k+1
    end
    return words
end
 
function getWordsWithIndex(lineTable, index, wordSeparator)
    local matchedWords = {}
    local k = 1
    for i,line in ipairs(lineTable) do
        local words = getWords(line, wordSeparator)
        matchedWords[k] = words[index]
        k = k + 1
    end
    return matchedWords
end

if key == "IPV4_DHCP_EN" then
    -- todo find based on running process
    resultLines=myExec("/var/web_root/DBFileParser -get " .. key, key)
    if resultLines[1] == "0" then
        finalResult = "No"
    elseif resultLines[1] == "1" then
        finalResult = "Yes"
    else
        finalResult="Unknown"
    end
elseif key == "IPV4_ADDR" then
    resultLines = myExec("ip -4 addr show eth0", key)
    matchedLines = getMatchingLines(resultLines, "inet ")
    matchedWords = getWordsWithIndex(matchedLines, 2, "%S+")
    finalResult=table.concat(matchedWords, "\n")
elseif key == "IPV4_MASK" then
    resultLines = myExec("ip -4 addr show eth0", key)
    matchedLines = getMatchingLines(resultLines, "inet ")
    matchedWords = getWordsWithIndex(matchedLines, 2, "%S+")
    finalResult=table.concat(matchedWords, "\n")
elseif key == "IPV4_DGW" then
    resultLines = myExec("ip -4 route show", key)
    matchedLines = getMatchingLines(resultLines, "default via")
    matchedWords = getWordsWithIndex(matchedLines, 3, "%S+")
    finalResult=table.concat(matchedWords, "\n")
elseif key == "IP_DNS0" then
    resultLines = myExec("cat /etc/resolv.conf", key)
    matchedLines = getMatchingLines(resultLines, "nameserver")
    matchedWords = getWordsWithIndex(matchedLines, 2, "%S+")
    finalResult=matchedWords[1]
elseif key == "IP_DNS1" then
    resultLines = myExec("cat /etc/resolv.conf", key)
    matchedLines = getMatchingLines(resultLines, "nameserver")
    matchedWords = getWordsWithIndex(matchedLines, 2, "%S+")
    finalResult=matchedWords[2]
elseif key == "IPV6_DHCP_EN" then
    -- todo find based on running process
    resultLines=myExec("/var/web_root/DBFileParser -get " .. key, key)
    if resultLines[1] == "0" then
        finalResult = "No"
    elseif resultLines[1] == "1" then
        finalResult = "Yes"
    else
        finalResult="Unknown"
    end
elseif key == "IPV6_ADDR" then
    resultLines = myExec("ip -6 addr show eth0", key)
    matchedLines = getMatchingLines(resultLines, "inet6")
    matchedWords = getWordsWithIndex(matchedLines, 2, "%S+")
    finalResult=table.concat(matchedWords, "\n")
elseif key == "IPV6_DGW" then
    resultLines = myExec("ip -6 route show", key)
    matchedLines = getMatchingLines(resultLines, "default via")
    matchedWords = getWordsWithIndex(matchedLines, 3, "%S+")
    finalResult=table.concat(matchedWords, "\n") 
else
  result="Unknown Key"
  finalResult=result .. key
end

print(finalResult)
