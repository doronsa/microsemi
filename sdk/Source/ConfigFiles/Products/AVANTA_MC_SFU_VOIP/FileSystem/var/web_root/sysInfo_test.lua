local myExecError = {"error"}

function myExec(cmd, name)
    local lineTable = {}
    local tmpfile = '/tmp/lua_exec_result/' .. name
    os.execute(cmd..' > '..tmpfile)
    local fhandle = io.open(tmpfile)
    if not fhandle then return myExecError end

    local k = 1
    for line in fhandle:lines() do
      lineTable[k] = line
      k = k + 1
    end
    fhandle:close()
    return lineTable
 end
 
 
function getMatchingLines(lineTable, matchStr)
    local k = 1
    local matchedLines = {}
    for i,line in ipairs(lineTable) do
        if string.find(line, matchStr) then
            matchedLines[k] = line
            k = k + 1
        end
    end
    return matchedLines
end
 
function getWords(line, separator)
    
   local words = {} 
    local k = 1
    for word in line:gmatch(separator) do
        words[k] = word
        k = k+1
   --     print(word)
    end
    return words
end
 
function getWordsWithIndex(lineTable, index, wordSeparator)
    local matchedWords = {}
    local k = 1
    for i,line in ipairs(lineTable) do
        local words = getWords(line, wordSeparator)
        matchedWords[k] = words[index]
        k = k + 1
    end
    return matchedWords
end
 
local resultLines = myExec("ip -6 addr show eth0", "test")


local matchedLines = getMatchingLines(resultLines, "inet6")
local matchedWords = getWordsWithIndex(matchedLines, 2, "%S+")

local str = table.concat(matchedWords, "\n")

print(str)

