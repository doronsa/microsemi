#!/bin/sh

CONF_FILE=snmpd.conf
SNMP_SYS_DESC="PDS104 system"
DEFauthtype=MD5
DEFauthpassphrase=12345678
DEFprivtype=DES
DEFprivpassphrase=87654321

PROD_SNMP_OID=""
SNMPv2EN=0
SNMPv3EN=0
MIB2_SYS_CNTCT=""
MIB2_SYS_NAME=""
MIB2_SYS_LOC=""
V2_TRAP_PASS=""
V2_GET_PASS=""
V2_SET_PASS=""
TRAP_POE_EN=""

V3_USER=""
V3_AUTHEN_PASS=""
V3_ENCR_PASS=""
V3_ENCR_LVL=""
V3_TRAP_USER=""
V3_TRAP_AUTEN_PASS=""
V3_TRAP_ENC_PASS=""
V3_TRAP_ENCR_LVL=""
V3_BOOT_CNTR=""

V3_AUTH_TYPE=""
V3_ENCR_TYPE=""
V3_USER_CREATE_STR=""
V3_USER_SECURITY="auth"

V3_TRAP_AUTH_TYPE=""
V3_TRAP_ENCR_TYPE=""
V3_TRAP_CREATE_STR=""
V3_TRAP_AUTH_PRIV=""

TRAP_IP0=""
TRAP_IP1=""

GetDBData()
{
	cd /var/web_root/

	SNMPv2EN="$(./DBFileParser -get SNMPV2_EN)"
	SNMPv3EN="$(./DBFileParser -get SNMPV3_EN)"
	PROD_SNMP_OID="$(./DBFileParser -get PROD_SNMP_OID)"
	
	MIB2_SYS_CNTCT="$(./DBFileParser -get MIB2_SYS_CNTCT)"	
	MIB2_SYS_NAME="$(./DBFileParser -get MIB2_SYS_NAME)"
	MIB2_SYS_LOC="$(./DBFileParser -get MIB2_SYS_LOC)"
	
	V2_TRAP_PASS="$(./DBFileParser -get V2_TRAP_PASS)"
	V2_GET_PASS="$(./DBFileParser -get V2_GET_PASS)"
	V2_SET_PASS="$(./DBFileParser -get V2_SET_PASS)"
	TRAP_POE_EN="$(./DBFileParser -get TRAP_POE_EN)"

	V3_USER="$(./DBFileParser -get V3_USER)"
	V3_AUTHEN_PASS="$(./DBFileParser -get V3_AUTHEN_PASS)"
	V3_ENCR_PASS="$(./DBFileParser -get V3_ENCR_PASS)"
	V3_ENCR_LVL="$(./DBFileParser -get V3_ENCR_LVL)"
	V3_TRAP_USER="$(./DBFileParser -get V3_TRAP_USER)"
	V3_TRAP_AUTEN_PASS="$(./DBFileParser -get V3_TRAP_AUTEN_PASS)"
	V3_TRAP_ENC_PASS="$(./DBFileParser -get V3_TRAP_ENC_PASS)"
	V3_TRAP_ENCR_LVL="$(./DBFileParser -get V3_TRAP_ENCR_LVL)"
	V3_BOOT_CNTR="$(./DBFileParser -get V3_BOOT_CNTR)"
	
	TRAP_IP0="$(./DBFileParser -get TRAP_IP0)"
	TRAP_IP1="$(./DBFileParser -get TRAP_IP1)"

	cd ..
}

SaveConfigFile()
{
	rm  $CONF_FILE


###### System section
	echo "#############################################################" >> $CONF_FILE
	echo "# SECTION: System Information Setup" >> $CONF_FILE

	echo "sysObjectID ${PROD_SNMP_OID}" >> $CONF_FILE
	echo "sysContact '${MIB2_SYS_CNTCT}'" >> $CONF_FILE
	echo "sysName '${MIB2_SYS_NAME}'" >> $CONF_FILE
	echo "sysLocation '${MIB2_SYS_LOC}'" >> $CONF_FILE
	echo "sysDescr '${SNMP_SYS_DESC}'" >> $CONF_FILE
	echo "engineIDType 3" >> $CONF_FILE
	echo "engineIDNic eth0" >> $CONF_FILE
	
####### Access Control Setup
	echo "#############################################################" >> $CONF_FILE
	echo "# SECTION: Access Control Setup" >> $CONF_FILE
	
	if [ ${SNMPv2EN} -eq "1" ];
	then
		echo "SNMPv2 is enabled"
		echo "rocommunity ${V2_GET_PASS}" >> $CONF_FILE
		echo "rwcommunity ${V2_SET_PASS}" >> $CONF_FILE
		
	fi
	
	if [ ${SNMPv3EN} -eq "1" ];
	then
		echo "SNMPv3 is enabled"

		 #disable the below 6 lines after debug is done
		 echo "createUser NoAuthUser" >> $CONF_FILE
		 echo "createUser MD5User ${DEFauthtype} '${DEFauthpassphrase}'"  >> $CONF_FILE
		 echo "createUser MD5DESUser ${DEFauthtype} '${DEFauthpassphrase}' ${DEFprivtype} '${DEFprivpassphrase}'"  >> $CONF_FILE
		 echo "rouser NoAuthUser noauth"  >> $CONF_FILE
		 echo "rouser MD5User auth"  >> $CONF_FILE
		 echo "rwuser MD5DESUser priv"  >> $CONF_FILE
		 #disable the above lines after debug is done
 
		V3_USER_CREATE_STR="createUser ${V3_USER}"
		case ${V3_ENCR_LVL} in
			*MD5*) V3_AUTH_TYPE="MD5" ;;
			*SHA*) V3_AUTH_TYPE="SHA" ;;
		esac
		case ${V3_ENCR_LVL} in
			*DES*) V3_ENCR_TYPE="DES" ;;
			*AES*) V3_ENCR_TYPE="AES" ;;
		esac
		
		
		if [[ "${V3_AUTH_TYPE}" != "" ]];
		then
			V3_USER_SECURITY="auth"
			V3_USER_CREATE_STR="${V3_USER_CREATE_STR}"" ${V3_AUTH_TYPE} '${V3_AUTHEN_PASS}'"
			if [[ "${V3_ENCR_TYPE}" != "" ]];
			then
				V3_USER_SECURITY="priv"
				V3_USER_CREATE_STR="${V3_USER_CREATE_STR}"" ${V3_ENCR_TYPE} '${V3_ENCR_PASS}'"
			fi
		else
			V3_USER_SECURITY="noauth"
		fi
		
		echo "${V3_USER_CREATE_STR}" >> $CONF_FILE
		echo "rwuser ${V3_USER} ${V3_USER_SECURITY}"  >> $CONF_FILE
	fi
	
##### Trap Section
	echo "#############################################################" >> $CONF_FILE
	echo "# SECTION: Trap Destinations" >> $CONF_FILE
	
	
	echo "trapcommunity ${V2_TRAP_PASS}" >> $CONF_FILE
	echo "authtrapenable  1" >> $CONF_FILE
	
	if [ ${SNMPv2EN} -eq "1" ];
	then	
		echo "trap2sink ${TRAP_IP0}" >> $CONF_FILE
		echo "trap2sink ${TRAP_IP1}" >> $CONF_FILE
	fi
	
	if [ ${SNMPv3EN} -eq "1" ];
	then
		V3_USER_CREATE_STR="createUser ${V3_TRAP_USER}"
		V3_TRAP_CREATE_STR="-v 3 -u ${V3_TRAP_USER}"		
		case ${V3_TRAP_ENCR_LVL} in
			*MD5*) V3_TRAP_AUTH_TYPE="MD5" ;;
			*SHA*) V3_TRAP_AUTH_TYPE="SHA" ;;
		esac
		case ${V3_TRAP_ENCR_LVL} in
			*DES*) V3_TRAP_ENCR_TYPE="DES" ;;
			*AES*) V3_TRAP_ENCR_TYPE="AES" ;;
		esac
		
		
		if [[ "${V3_AUTH_TYPE}" != "" ]];
		then
			V3_USER_CREATE_STR="${V3_USER_CREATE_STR}"" ${V3_TRAP_AUTH_TYPE} '${V3_TRAP_AUTEN_PASS}'"
			V3_TRAP_CREATE_STR="${V3_TRAP_CREATE_STR}"" -a ${V3_TRAP_AUTH_TYPE} -A '${V3_TRAP_AUTEN_PASS}'"
			V3_TRAP_AUTH_PRIV="-l authNoPriv"
			if [[ "${V3_TRAP_ENCR_TYPE}" != "" ]];
			then
				V3_USER_CREATE_STR="${V3_USER_CREATE_STR}"" ${V3_TRAP_ENCR_TYPE} '${V3_TRAP_ENC_PASS}'"
				V3_TRAP_CREATE_STR="${V3_TRAP_CREATE_STR}"" -x ${V3_TRAP_ENCR_TYPE} -X '${V3_TRAP_ENC_PASS}'"
				V3_TRAP_AUTH_PRIV="-l authPriv"
			fi
		else
			V3_TRAP_AUTH_PRIV="-l noAuthNoPriv"
		fi
		
		echo "${V3_USER_CREATE_STR}" >> $CONF_FILE
		V3_TRAP_CREATE_STR="${V3_TRAP_CREATE_STR}"" ${V3_TRAP_AUTH_PRIV}"			
		echo "trapsess ${V3_TRAP_CREATE_STR} ${TRAP_IP0}" >> $CONF_FILE
		echo "trapsess ${V3_TRAP_CREATE_STR} ${TRAP_IP1}" >> $CONF_FILE	
	fi
		
	sync
}

SendWarmStart()
{
	if [ ${SNMPv2EN} -eq "1" ];
	then
		snmptrap -v 2c -c ${V2_TRAP_PASS} ${TRAP_IP0}  '' 1.3.6.1.6.3.1.1.5.2
		snmptrap -v 2c -c ${V2_TRAP_PASS} ${TRAP_IP1}  '' 1.3.6.1.6.3.1.1.5.2
	fi
	
	if [ ${SNMPv3EN} -eq "1" ];
	then
		snmptrap ${V3_TRAP_CREATE_STR} ${TRAP_IP0} '' 1.3.6.1.6.3.1.1.5.2
		snmptrap ${V3_TRAP_CREATE_STR} ${TRAP_IP1} '' 1.3.6.1.6.3.1.1.5.2
	fi	
}

if [ $1 -eq 0 ];
then
   echo "Stoping SNMP"
   /bin/kill -s 2 $(pidof snmpd)
else
	echo "Starting SNMP"
	export MIBS=ALL
	GetDBData
	SaveConfigFile
	/usr/bin/snmpd -c $CONF_FILE >/dev/null
# Enable if required. Currently doesn't make sense
#	SendWarmStart
fi

