system build how to /ver 1.0 doron sandroy/
------------------------------
build kernel:

1. export the CROSS_COMPILE:
   export CROSS_COMPILE=/"local install"/SDK_2.6.25-RC78/armv5-marvell-linux-uclibcgnueabi-soft_i686/bin/arm-marvell-linux-uclibcgnueabi-
   export ARCH=arm
2. cd Source/Kernel/linux_feroceon-KW2/
3. copy the configer on the begin
   cp ../../ConfigFiles/Products/AVANTA_MC_SFU_VOIP/Kernel/.config .
   make mrproper ARCH=arm
4. make -j6

-------------------------------
uboot build:

1. cd Source/Uboot/4.1.6/u-boot-2009.08
2. export the CROSS_COMPILE:
   export CROSS_COMPILE=/"local install"/SDK_2.6.25-RC78/armv5-marvell-linux-uclibcgnueabi-soft_i686/bin/arm-marvell-linux-uclibcgnueabi-
   export ARCH=arm
3. ./create_all_images.sh
4. all need find in: Source/Uboot/4.1.6/images/LE/RD-88F6601-MC-2L/SPI_333mhz

-------------------------------

build the File System:

1. add exe files to file system
   copy the files to :Source/ConfigFiles/Products/AVANTA_MC_SFU_VOIP/FileSystem/bin
2. add script or edit init script :S69_init_card.sh
   Source/ConfigFiles/Products/AVANTA_MC_SFU_VOIP/FileSystem/init.d/
3. add directory go to : Output/Products/AVANTA_MC_SFU_VOIP/FileSystem
   and add to the rootfs.tar.
4. build all run:
   go to SDK_2.6.25-RC78/Tools
   ./sdkBuild.sh AVANTA_MC_SFU_VOIP
5. the img is on : Output/Products/AVANTA_MC_SFU_VOIP/RecoveryImage
   
   
   
