Burn SPI-FLASH Uboot
--------------------
setenv ipaddr 192.168.0.50
setenv serverip 192.168.0.40
ping 192.168.0.40
bubt uboot_spi.bin



boot args command for burn the file system and kernel to the flash
---------------------------------------------------------------------
1st copy the Linux image file to TFTP Server and type the commands bellow in uBoot


setenv ipaddr 192.168.0.50
setenv serverip 192.168.0.40

setenv bootcmd_spi 'tftpboot ${loadaddr} ${image_name}; sf protect off; sf erase 0x100000 0xF00000; sf write ${loadaddr} 0x100000 0xF00000;reset'
setenv bootcmd 'setenv bootargs ${console} root=/dev/mtdblock2 rootfstype=squashfs ${mtdParts} ${mvNetConfig} ${mvPhoneConfig}; sf read ${loadaddr} 0x100000 0x300000; bootm ${loadaddr}'
setenv mtdParts mtdparts=spi_flash:1M@0(uboot),3M@0x100000(uImg)ro,10M@0x400000(rootFs)ro,5M@0xE00000(vars)rw

setenv image_name sdk_unified.img
save
run bootcmd_spi





