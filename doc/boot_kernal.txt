Starting kernel ...

Uncompressing Linux............................................................................................................................................................................. done, booting the kernel.
Linux version 2.6.32.11 (doronsa@doron) (gcc version 4.6.3 20120201 (prerelease) (Linaro GCC branch-4.6.3. Marvell GCC 201206-1216.caf91d97) ) #4 Sun Oct 5 14:52:00 IDT 2014
CPU: Feroceon 88FR131 [56251311] revision 1 (ARMv5TE), cr=00053977
CPU: VIVT data cache, VIVT instruction cache
Machine: Feroceon-KW2
Using UBoot passing parameters structure
Memory policy: ECC disabled, Data cache writeback
Built 1 zonelists in Zone order, mobility grouping on.  Total pages: 32512
Kernel command line: console=ttyS0,115200 root=/dev/mtdblock2 rootfstype=squashfs mtdparts=spi_flash:1M@0(uboot),3M@0x100000(uImg)ro,10M@0x400000(rootFs)ro,1M@0xE00000(vars)rw mv_net_config=4,(00:50:43:11:11:11,0:1:2:3),mtu=1500
PID hash table entries: 512 (order: -1, 2048 bytes)
Dentry cache hash table entries: 16384 (order: 4, 65536 bytes)
Inode-cache hash table entries: 8192 (order: 3, 32768 bytes)
Memory: 128MB = 128MB total
Memory: 122708KB available (4988K code, 1952K data, 116K init, 0K highmem)
Hierarchical RCU implementation.
NR_IRQS:208
Console: colour dummy device 80x30
Calibrating delay loop... 198.65 BogoMIPS (lpj=993280)
Mount-cache hash table entries: 512
CPU: Testing write buffer coherency: ok
NET: Registered protocol family 16
No L2-Cache.

CPU Interface
-------------
SDRAM_CS0 ....base 00000000, size 128MB
SDRAM_CS1 ....no such
SDRAM_CS2 ....no such
SDRAM_CS3 ....no such
DEVICE_CS0 ....no such
DEVICE_CS1 ....no such
DEVICE_CS2 ....no such
DEVICE_CS3 ....no such
PEX0_MEM ....no such
PEX0_IO ....no such
PEX1_MEM ....no such
PEX1_IO ....no such
INTER_REGS ....base f1000000, size   1MB
NAND_NOR_CS ....no such
SPI_CS0 ....base f6000000, size  32MB
SPI_CS1 ....no such
SPI_CS2 ....no such
SPI_CS3 ....no such
SPI_CS4 ....no such
SPI_CS5 ....no such
SPI_CS6 ....no such
SPI_CS7 ....no such
SPI_B_CS0 ....no such
BOOT_ROM_CS ....no such
DEV_BOOTCS ....no such
CRYPT1_ENG ....no such
CRYPT2_ENG ....no such
PNC_BM ....base f5000000, size   1MB
ETH_CTRL ....base f5100000, size   1MB
PON_CTRL ....base f5200000, size   1MB
NFC_CTRL ....no such

  Marvell Development Board (LSP Version KW2_LSP_3.3.1_p12_PQ_uPON_2.6.25_RC78)-- RD-88F6601-MC2L  Soc: 88F6601 A0 LE

 Detected Tclk 200000000 and SysClk 200000000

Warning PCI-E is Powered Off
bio: create slab <bio-0> at 0
vgaarb: loaded
Switching to clocksource kw_clocksource
NET: Registered protocol family 2
IP route cache hash table entries: 1024 (order: 0, 4096 bytes)
TCP established hash table entries: 4096 (order: 3, 32768 bytes)
TCP bind hash table entries: 4096 (order: 2, 16384 bytes)
TCP: Hash tables configured (established 4096 bind 4096)
TCP reno registered
NET: Registered protocol family 1
RPC: Registered udp transport module.
RPC: Registered tcp transport module.
RPC: Registered tcp NFSv4.1 backchannel transport module.
rtc mv_rtc: rtc core: registered kw-rtc as rtc0
RTC registered
(tpm_init_ioctl_mempools:3564) init tpm_ioctl_mpools.mpool_s, number_of_entries=21, entry_size=268 pool=0xc7894000
(tpm_init_ioctl_mempools:3594) init tpm_ioctl_mpools.mpool_m, number_of_entries=2, entry_size=836 pool=0xc789c800
(tpm_init_ioctl_mempools:3624) init tpm_ioctl_mpools.mpool_l, number_of_entries=2, entry_size=1616 pool=0xc7829000
(tpm_init_ioctl_mempools:3654) init tpm_ioctl_mpools.mpool_h, number_of_entries=1, entry_size=3620 pool=0xc789d000
= SW Module SYS FS Init ended successfully =
init_tpm_pkt_frwd_db: Clearing DB
init_tpm_vlan_db: Clearing DB
init_tpm_mod_db: Clearing DB
init_tpm_l2_key_db: Clearing DB
init_tpm_l3_key_db: Clearing DB
init_tpm_ipv4_key_db: Clearing DB
init_tpm_ipv6_key_db: Clearing DB
init_tpm_ipv6_dip_key_db: Clearing DB
init_tpm_ipv6_gen_key_db: Clearing DB
init_tpm_ipv6_l4_ports_key_db: Clearing DB
= SW Module SYS FS Init for tpm ended successfully =
= TPM Module Init ended successfully =
squashfs: version 4.0 (2009/01/31) Phillip Lougher
JFFS2 version 2.2. (NAND) © 2001-2006 Red Hat, Inc.
msgmni has been set to 239
alg: No test for stdrng (krng)
io scheduler noop registered
io scheduler anticipatory registered (default)
Serial: 8250/16550 driver, 2 ports, IRQ sharing disabled
serial8250.0: ttyS0 at MMIO 0xf1012000 (irq = 33) is a 16550A
console [ttyS0] enabled
serial8250.1: ttyS1 at MMIO 0xf1012100 (irq = 34) is a 16550A
brd: module loaded
loop: module loaded
SPI Serial flash detected @ 0xf6000000, 16384KB (256sec x 64KB)
mtd: bad character after partition (r)
4 cmdlinepart partitions found on MTD device spi_flash
Creating 4 MTD partitions on "spi_flash":
0x000000000000-0x000000100000 : "uboot"
0x000000100000-0x000000400000 : "uImg"
0x000000400000-0x000000e00000 : "rootFs"
0x000000e00000-0x000000f00000 : "vars"
mv_eth_probe: port_mask=0x7, cpu_mask=0x1
0 - Base 0x00000000 , Size = 0x08000000.
12 - Base 0xf1000000 , Size = 0x00100000.
14 - Base 0xf6000000 , Size = 0x02000000.
27 - Base 0xf5000000 , Size = 0x00100000.
28 - Base 0xf5100000 , Size = 0x00100000.
29 - Base 0xf5200000 , Size = 0x00100000.
mvPncVirtBase = 0xc8c00000
  o 3 Giga ports supported
  o Giga PON port is #2: - 8 TCONTs supported
  o SKB recycle supported (Enabled)
  o NETA acceleration mode 4
  o PnC supported (Enabled)
  o HWF supported
  o PMT supported
  o RX Queue support: 8 Queues * 128 Descriptors
  o TX Queue support: 8 Queues * 3072 Descriptors
  o Receive checksum offload supported
  o Transmit checksum offload supported
  o Driver ERROR statistics enabled
  o Driver INFO statistics enabled
  o Driver DEBUG statistics enabled
  o Switch support enabled

  o Loading Switch QuarterDeck driver
qdLoadDriver Called.
driverConfig dev = 0xc7819df4
****  dev->deviceId 374.
    o Device ID     : 0x176
    o No. of Ports  : 6
    o CPU Port      : 255
    o Disable disconnected Switch Port #0 and force link down
Force Link DOWN - Failed
  o Loading network interface(s)

  o Port 0 is connected to Linux netdevice
        giga p=0: mtu=1500, mac=c7819e74
    o eth0, ifindex = 2, GbE port = 0

  o Port 1 is connected to Linux netdevice
        giga p=1: mtu=1500, mac=c7819e74
    o eth1, ifindex = 3, GbE port = 1

  o Port 2 is connected to Linux netdevice
        pon p=2: mtu=1500, mac=c7819e74
    o eth2, ifindex = 4, GbE port = 2

Intel(R) PRO/1000 Network Driver - version 7.3.21-k5-NAPI
Copyright (c) 1999-2006 Intel Corporation.
e1000e: Intel(R) PRO/1000 Network Driver - 1.0.2-k2
e1000e: Copyright (c) 1999-2008 Intel Corporation.
e100: Intel(R) PRO/100 Network Driver, 3.5.24-k2-NAPI
e100: Copyright(c) 1999-2006 Intel Corporation
PPP generic driver version 2.4.2
NET: Registered protocol family 24
mice: PS/2 mouse device common for all mice
i2c /dev entries driver
Software Watchdog Timer: 0.07 initialized. soft_noboot=0 soft_margin=60 sec (nowayout= 0)
Linux telephony interface: v1.00
Loading Marvell spi device
Loading Marvell vpapi device
Loading Marvell tdm device
cpuidle: using governor ladder
TCP cubic registered
NET: Registered protocol family 10
IPv6 over IPv4 tunneling driver
NET: Registered protocol family 17
802.1Q VLAN Support v1.8 Ben Greear <greearb@candelatech.com>
All bugs added by David S. Miller <davem@redhat.com>
= CUST Module SYS FS Init ended successfully =
MVCUST: misc device cust registered with minor: 60

MV_CUST module inserted - V2.6.25

= MC MAC LEARN Module SYS FS Init ended successfully =
rtc mv_rtc: setting system clock to 1999-11-30 00:00:00 UTC (943920000)
VFS: Mounted root (squashfs filesystem) readonly on device 31:2.
Freeing init memory: 116K
Initializing random number generator... read-only file system detected...done
Starting network...
run-parts: /etc/network/if-pre-up.d: No such file or directory
ip: RTNETLINK answers: File exists
[RCS]: Unlock /dev/mtd3 flash memory

Notice: Serial SPI flash (spi_flash) unlock per sector is not supported!
        Unlocking the whole device.[RCS]: Mounting /dev/mtdblock3 VAR folder
Loading config file /var/mongoose.conf
Mongoose web server v.5.4 serving [/var/web_root] on port 8585

Welcome to Buildroot
buildroot login:

