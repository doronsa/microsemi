setenv serverip 10.0.0.3
setenv ipaddr 10.0.0.10
setenv gatewayip 10.0.0.138
setenv rcvrip 10.0.0.10
setenv rootpath /tftpboot/FileSystem
bubt u-boot-rd88f6601mc2l_333rd_A-MC_ddr2_spi.bin 

Dual image support for Avanta MC boards, SFU product - file system reduction
----------------------------------------------------------------------------
For 16MB and 32MB MC boards, SFU product with file system reduction image, use the following mtdParts definition


**** work on small img 7.2 m ******
setenv mtdParts 'mtdparts=spi_flash:1M@0(uboot),3584K@0x100000(uImg),3M@0x480000(rootFs),512K@0x780000(vars),3584K@0x800000(uImgB),3M@0xB80000(rootFsB),5M@0xE80000(varsB)'

setenv bootcmd 'setenv bootargs ${console} root=/dev/mtdblock2 rootfstype=squashfs ${mtdParts} ${mvNetConfig} ${mvPhoneConfig}; sf read ${loadaddr} 0x100000 0x380000; bootm ${loadaddr}'

setenv bootcmd_spi 'tftpboot ${loadaddr} ${image_name}; sf protect off; sf erase 0x100000 0x800000; sf write ${loadaddr} 0x100000 0x800000 ;reset'

setenv image_name sdk_unified.img
***********************************
setenv bootcmd_b 'setenv bootargs ${console} root=/dev/mtdblock2 rootfstype=squashfs ${mtdParts} ${mvNetConfig} ${mvPhoneConfig}; sf read ${loadaddr} 0x800000 0x380000; bootm ${loadaddr}'

**** work on small img 7.2 m ******
0x000000800000-0x000000b80000 : "uImgB"
0x000000b80000-0x000000e80000 : "rootFsB"
0x000000e80000-0x000000f00000 : "varsB"

setenv bootcmd_spi_b 'tftpboot ${loadaddr} ${image_name}; sf protect off; sf erase 0x800000 0x800000; sf write ${loadaddr} 0x800000 0x800000;reset'

****************************************************************************
build for big img

setenv bootcmd_b 'setenv bootargs ${console} root=/dev/mtdblock2 rootfstype=squashfs ${mtdParts_b} ${mvNetConfig} ${mvPhoneConfig}; sf read ${loadaddr} 0x800000 0x380000; bootm ${loadaddr}'

setenv bootcmd_spi_b 'tftpboot ${loadaddr} ${image_name_b}; sf protect off; sf erase 0x800000 0xD00000; sf write ${loadaddr} 0x800000 0xD00000;reset'

setenv mtdParts 'mtdparts=spi_flash:1M@0(uboot),3M@0x100000(uImg),3M@0x480000(rootFs),512K@0x780000(vars),3M@0x800000(uImgB),10M@0xB80000(rootFsB),2@0xE80000(varsB)'

setenv image_name_b sdk_unified_big.img
-----------------------------------
test

setenv bootcmd_spi_b 'tftpboot ${loadaddr} ${image_name_b}; sf protect off; sf erase 0x800000 0x280000; sf write ${loadaddr} 0x800000 0x280000;reset'

setenv bootcmd_b 'setenv bootargs ${console} root=/dev/mtdblock2 rootfstype=squashfs ${mtdParts_b} ${mvNetConfig} ${mvPhoneConfig}; sf read ${loadaddr} 0x800000 0x380000; bootm ${loadaddr}'

setenv mtdParts 'mtdparts=spi_flash:1M@0(uboot),3M@0x100000(uImg),3M@0x480000(rootFs),512K@0x780000(vars),3M@0x800000(uImgB),5M@0xB80000(rootFsB),2@0xE80000(varsB)'

1600000
1000000
800000 
e80000

0x000000800000-0x000000b80000 : "uImgB"
0x000000b80000-0x000001080000 : "rootFsB"
0x000000e80000-0x000001180000 : "varsB"
		      1200000



***************************
setenv bootcmd_spi 'tftpboot ${loadaddr} ${image_name}; sf protect off; sf erase 0x200000 0xF00000; sf write ${loadaddr} 0x100000 0xF00000;reset'
setenv bootcmd 'setenv bootargs ${console} root=/dev/mtdblock2 rootfstype=squashfs ${mtdParts} ${mvNetConfig} ${mvPhoneConfig}; sf read ${loadaddr} 0x200000 0x300000; bootm ${loadaddr}'
setenv mtdParts mtdparts=spi_flash:1M@0(uboot),3M@0x200000(uImg)ro,10M@0x400000(rootFs)ro,2M@0xE00000(vars)rw


***************************

// Non file system reduction image - 15M
----------------------------------------
setenv bootcmd_spi 'tftpboot ${loadaddr} ${image_name}; sf protect off; sf erase 0x100000 0xF00000; sf write ${loadaddr} 0x100000 0xF00000;reset'
setenv bootcmd 'setenv bootargs ${console} root=/dev/mtdblock2 rootfstype=squashfs ${mtdParts} ${mvNetConfig} ${mvPhoneConfig}; sf read ${loadaddr} 0x100000 0x300000; bootm ${loadaddr}'
setenv mtdParts mtdparts=spi_flash:1M@0(uboot),3M@0x100000(uImg)ro,10M@0x400000(rootFs)ro,2M@0xE00000(vars)rw


-------------------------
mtdParts=mtdparts=spi_flash:1M@0(uboot),3M@0x100000(uImg)ro,10M@0x400000(rootFs)ro,5M@0xE00000(vars)rw
