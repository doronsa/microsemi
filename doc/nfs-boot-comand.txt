setenv rootpath /tftpboot/FileSystem
setenv ipaddr 10.0.0.10
setenv serverip 10.0.0.4
setenv gatewayip 10.0.0.138
setenv netmask 255.255.255.0
setenv hostname pds104
setenv ip ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}:eth0:off
setenv bootcmd_flash 'setenv bootargs ${console} root=/dev/mtdblock2 rootfstype=squashfs ${mtdParts} ${mvNetConfig} ${mvPhoneConfig}; sf read ${loadaddr} 0x100000 0x300000; bootm ${loadaddr}'
setenv bootargs_nfs 'root=/dev/nfs rw nfsroot=10.0.0.4:/tftpboot/FileSystem mem=128M'
setenv bootcmd_nfs 'setenv bootargs ${bootargs_nfs} ${ip} ${console} ${mvNetConfig}; tftpboot ${loadaddr} uImage; bootm ${loadaddr}'
setenv bootcmd 'run bootcmd_nfs'
saveenv
